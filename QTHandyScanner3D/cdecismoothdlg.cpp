#include "cdecismoothdlg.h"
#include "ui_cdecismoothdlg.h"
#include "cglwidget.h"

#include "cdecimsmooththread.h"
#include "cmeshdumpthread.h"
#include "messages.h"

#include <QFile>

std::string decsm_mesh_dump_str("./decsm.dump");

CDeciSmoothDlg::CDeciSmoothDlg(CGLWidget * glWidget, QWidget *parent) :
    QDialog(parent), _glWidget(glWidget), _init_tri_cnt(0), _very_first_use(false),
    ui(new Ui::CDeciSmoothDlg)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);

    connect(ui->previewBtn, SIGNAL(clicked()), this, SLOT(onPreviewBtn()));
    connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));

    _thread = new CDecimSmoothThread;

    // user pressed cancel button
    connect(ui->cancelBtn,  SIGNAL(clicked()), _thread, SLOT(stop_process()));
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

    // user closed dialg
    connect(this,  SIGNAL(finished(int)), _thread, SLOT(stop_process()));
    // working with a thread
    connect(_thread, SIGNAL(send_back(int)), this, SLOT(onMakeProgressStep(int)));
    connect(_thread, SIGNAL(send_result(CTriangularMesh*)), this, SLOT(onCompleteProcessing(CTriangularMesh*)));
    connect(_thread, SIGNAL(send_result(CTriangularMesh*)), _glWidget, SLOT(on_new_fused_mesh_computed(CTriangularMesh*)));
    connect(_thread, SIGNAL(finished()), this, SLOT(onStopProcessing()));

    //// working with mesh dumping thread
    _info = new QMessageBox(this);
    _ithread = new CMeshDumpThread;
    connect(_ithread, SIGNAL(done_dumping()), this, SLOT(onFinishDumping()));

    connect(ui->sldDecimProc, SIGNAL(valueChanged(int)), this, SLOT(decimproc_changed(int )));
    connect(ui->sldSmooth, SIGNAL(valueChanged(int)), this, SLOT(smoothdeg_changed(int )));
    connect(ui->sldIter,   SIGNAL(valueChanged(int)), this, SLOT(iterations_changed(int )));
}

CDeciSmoothDlg::~CDeciSmoothDlg()
{
    delete ui;
    delete _thread;
    delete _info;
    delete _ithread;
}

void CDeciSmoothDlg::onMakeProgressStep(int val)
{
    ui->progressBar->setValue(val);
}

void CDeciSmoothDlg::onCompleteProcessing(CTriangularMesh * m)
{
   if(m == NULL)
   {
       QMessageBox::warning(this, tr("Warning"),wrn_FailedMesh);
       return;
   }

   if(_on_apply)
   {
       close_dialog();
       return;
   }

   _is_result_valid = true;
   ui->progressBar->setValue(100);
}

void CDeciSmoothDlg::onStopProcessing()
{
    ui->applyBtn     ->setEnabled(true);
    ui->previewBtn   ->setEnabled(true);
    ui->sldDecimProc->setEnabled(true);
    ui->sldIter->setEnabled(true);
    ui->sldSmooth->setEnabled(true);

    _in_process = false;
}

void CDeciSmoothDlg::close_dialog()
{
    _on_apply = false;

    QFile tmp(decsm_mesh_dump_str.c_str());
    tmp.remove();

    close();
}

CTriangularMesh * CDeciSmoothDlg::restore_mesh()
{

    std::cout << "restore_mesh()" << std::endl;
    //// _glWidget->discard_mesh_of_current_object();



    std::ifstream ifile;
    ifile.open(decsm_mesh_dump_str.c_str(), std::ios::in | std::ios::binary);
    CTriangularMesh * mesh = new CTriangularMesh;
    mesh->load(ifile);
    ifile.close();
    _glWidget->on_new_fused_mesh_computed(mesh);

    return mesh;
}

void CDeciSmoothDlg::onCancelBtn()
{
    if(!_in_process)
    {
        if(_is_result_valid)
                restore_mesh();
        close_dialog();
    }

       _in_process = false;
       _on_apply = false;
}


void CDeciSmoothDlg::onPreviewBtn()
{
    CTriangularMesh * mesh = _glWidget->get_fused_mesh();
    if(mesh == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NoFusedMesh);
        return;
    }

    ui->progressBar->setValue(0);
    ui->applyBtn        ->setEnabled(false);
    ui->previewBtn      ->setEnabled(false);
    ui->sldDecimProc    ->setEnabled(false);
    ui->sldIter         ->setEnabled(false);
    ui->sldSmooth       ->setEnabled(false);
    _in_process = true;

    if (_very_first_use && !_on_apply)
    {
        _ithread->start_process(mesh, decsm_mesh_dump_str);
        _info->setText("Dumping mesh to external file ... ");
        _info->setStandardButtons(0);
        _info->show();
        return;
    }

    start_process_loc();


}
void CDeciSmoothDlg::start_process_loc()
{

    CTriangularMesh * mm;
    if(_very_first_use)
    {
        mm = _glWidget->get_fused_mesh();
        _very_first_use = false;
    }
    else
        mm = restore_mesh();
    _thread->start_process(mm, ui->sldDecimProc->value(),
                           ui->sldSmooth->value(), ui->sldIter->value());
}

void CDeciSmoothDlg::onApplyBtn()
{
    _on_apply = true;
    if(!_is_result_valid)
        onPreviewBtn();
    else
        close_dialog();
}


void CDeciSmoothDlg::iterations_changed(int v)
{
    _is_result_valid = false;
    QString lbl = QString("Iterations: %1").arg(QString::number(v));
    ui->lblIter->setText(lbl);
}

void CDeciSmoothDlg::smoothdeg_changed(int v)
{
    _is_result_valid = false;
    QString lbl = QString("Smoothing (degree): %1").arg(QString::number(v));
    ui->lblSmooth->setText(lbl);
}

void CDeciSmoothDlg::decimproc_changed(int v)
{
    _is_result_valid = false;
    if(_glWidget->get_fused_mesh() != NULL)
    {
        int alltris =  _init_tri_cnt / 1000;
        int redtris = alltris * v / 100;

        QString lbl = QString("from %1k polygons to %2k, %3%").arg(QString::number(alltris)).arg(QString::number(redtris)).arg(QString::number(v));
        ui->lblDecim->setText(lbl);
    }
}


void CDeciSmoothDlg::showEvent(QShowEvent * event)
{
    if (_glWidget->get_fused_mesh() != NULL)
        _init_tri_cnt = _glWidget->get_fused_mesh()->get_tri_mesh()->_triangles.size();
    _very_first_use = true;
    ui->progressBar->setValue(0);
   _is_result_valid    = false;
    _on_apply           = false;
    _in_process         = false;


    ui->sldDecimProc->setValue(50);
    ui->sldSmooth->setValue(45);
    ui->sldIter->setValue(1);

}

void CDeciSmoothDlg::onFinishDumping()
{
    std::cout << "onFinishDumping" << std::endl;
    _info->setText("Dumping mesh to external file ... done");
    _info->hide();

     start_process_loc();
}
