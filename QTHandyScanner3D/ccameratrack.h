#ifndef CCAMERATRACK_H
#define CCAMERATRACK_H

#include "ccamerapose.h"
#include <vector>
#include "menu_vew_actions.h"

class CCameraTrack : public CGLObject
{
public:
    CCameraTrack(MenuActions *);
    virtual ~CCameraTrack();

    virtual void glDraw();
    void set_visible(bool v);
    virtual void remake_glList();

protected:
    void clean();
    virtual void create_glList();


    std::vector<CCameraPose *> _camerasp;
    MenuActions * _va;
};

#endif // CCAMERATRACK_H
