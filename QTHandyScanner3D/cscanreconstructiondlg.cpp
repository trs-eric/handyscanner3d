#include "cscanreconstructiondlg.h"
#include "ui_cscanreconstructiondlg.h"
#include "cglwidget.h"
#include "cscanreconstructionthread.h"
#include <QMessageBox>
#include <QFileDialog>
#include "settings/psettings.h"

CScanReconstructionDlg::CScanReconstructionDlg(CGLWidget * glWidget, QWidget *parent) :
   _glWidget(glWidget), QDialog(parent), _in_process(false), _thread(NULL), _saving_dir("./"),
    ui(new Ui::CScanReconstructionDlg)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);

    ui->setupUi(this);

    ui->progressBar->setMaximum(100);

    connect(ui->startBtn, SIGNAL(clicked()), this, SLOT(onStartBtn()));
    connect(ui->comboSliceStep, SIGNAL(currentIndexChanged(int)), this, SLOT(angle_step_changed(int)));


    int val= (CGlobalSettings::_scan_step_angle/BGM(0.45) )- 1.0;
    ui->comboSliceStep->setCurrentIndex(val);

    ui->comboLaserUse->setCurrentIndex(0);

    _thread = new CScanReconstructionThread;

    // user pressed cancel button
    //
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

    // user closed dialg
    connect(this,  SIGNAL(finished(int)), _thread, SLOT(stop_process()));

    connect(_thread, SIGNAL(send_back(int)), this, SLOT(onMakeProgressStep(int)));
    connect(_thread, SIGNAL(send_time_elapsed(QString)), this, SLOT(onMakeTimeElapsed(QString)));
    connect(_thread, SIGNAL(send_time_ETA(QString)), this, SLOT(onMakeTimeETA(QString)));

    //connect(_thread, SIGNAL(send_clean_result()), _glWidget, SLOT(clean_current_scan()));
    //connect(_thread, SIGNAL(send_result(std::vector<C3DPoint> *)), _glWidget, SLOT(construct_next_scan(std::vector<C3DPoint> * )));

    connect(_thread, SIGNAL(send_result_left (std::vector<C3DPoint> *)), _glWidget, SLOT(construct_next_scan_left(std::vector<C3DPoint> * )));
    connect(_thread, SIGNAL(send_result_right(std::vector<C3DPoint> *)), _glWidget, SLOT(construct_next_scan_right(std::vector<C3DPoint> * )));

    connect(_thread, SIGNAL(send_tex_left(CRawTexture *)),  _glWidget, SLOT(construct_next_texture_left (CRawTexture *)));
    connect(_thread, SIGNAL(send_tex_right(CRawTexture *)), _glWidget, SLOT(construct_next_texture_right(CRawTexture *)));

    connect(_thread, SIGNAL(send_finished()), this, SLOT(onCompleteProcessing()));

    connect(ui->slidMotorPower, SIGNAL(valueChanged(int)),   this, SLOT(slidMPowchanged(int)));
    ui->slidMotorPower->setValue(100);

    connect(ui->slidMotorStep, SIGNAL(valueChanged(int)),   this, SLOT(slidMStepchanged(int)));
    ui->slidMotorStep->setValue(5);

    //connect(ui->slidPause, SIGNAL(valueChanged(int)),   this, SLOT(slidPausechanged(int)));
    //ui->slidPause->setValue(0);


    ui->chkScanTex->setChecked(true);


    connect(ui->chkSaveImgs, SIGNAL(clicked(bool)), this, SLOT(checkedSaveImg(bool)));

}

void CScanReconstructionDlg::checkedSaveImg(bool v)
{
    if(v)
    {
        const QString saveImageTypes = "Background image PNG (*.png)";
        QString fileName = QFileDialog::getSaveFileName(this,
             tr("Destination path ..."), "", saveImageTypes);

        if (!fileName.isEmpty())
        {
            QFileInfo info_fname(fileName);
            _saving_dir = info_fname.absolutePath();
            /// ofile.open(fname.toLocal8Bit().data()
        }
    }

}

void CScanReconstructionDlg::onMakeTimeElapsed(QString val)
{
    ui->lblTpass->setText(val);
}

void CScanReconstructionDlg::onMakeTimeETA(QString val)
{
    ui->lblETA->setText(val);
}


/*void CScanReconstructionDlg::slidPausechanged(int val)
{
    ui->lblPause->setText(QString::number(val));
}*/

void CScanReconstructionDlg::slidMPowchanged(int val)
{
   int lbl_val = val/2; //(val - ui->slidMotorPower->minimum()) * 100 / (ui->slidMotorPower->maximum() - ui->slidMotorPower->minimum());

    ui->lblMotorPower->setText(QString::number(lbl_val));

   // ui->slidMotorStep->setMaximum(val);
}

void CScanReconstructionDlg::slidMStepchanged(int val)
{
    ui->lblMotorStep->setText(QString::number(val));
}

CScanReconstructionDlg::~CScanReconstructionDlg()
{
    delete ui;
    delete _thread;
}

void CScanReconstructionDlg::onMakeProgressStep(int val)
{
    ui->progressBar->setValue(val);
}


void CScanReconstructionDlg::onStopProcessing()
{
    _thread->stop_process();
    ui->cancelBtn->setEnabled(false);
    ui->cancelBtn->setText("Canceling ...");
}

void CScanReconstructionDlg::close_dialog()
{
    close();
}

void CScanReconstructionDlg::onCancelBtn()
{
    if(!_in_process)
    {
       close_dialog();
       return;
    }

    QMessageBox::StandardButton reply
            = QMessageBox::question(this, "Abort", tr("Are you sure?"),
                                    QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes)
            onStopProcessing();
}

void CScanReconstructionDlg::angle_step_changed(int idx)
{
    switch(idx)
    {
        case cmb045:
            CGlobalSettings::_scan_step_angle=BGM(0.45);
        break;

        case cmb09:
            CGlobalSettings::_scan_step_angle=BGM(0.9);
        break;

        case cmd18:
            CGlobalSettings::_scan_step_angle=BGM(1.8);
        break;
    }
}

void CScanReconstructionDlg::onStartBtn()
{
    ui->startBtn->setEnabled(false);
    ui->slidMotorPower->setEnabled(false);
    ui->slidMotorStep->setEnabled(false);
    //ui->slidPause->setEnabled(false);
    ui->chkScanTex->setEnabled(false);

    _in_process = true;
    // std::vector<C3DPoint> empty_pnts;
    // _glWidget->construct_single_scan(empty_pnts);
    _glWidget->prepaire_new_scan();

    bool useL = true;
    bool useR = true;
    if(ui->comboLaserUse->currentIndex() == cmbLeft)
        useR = false;
    if(ui->comboLaserUse->currentIndex() == cmbRight)
        useL = false;
    _thread->start_process(useL, useR,
                           ui->slidMotorPower->value(), ui->slidMotorStep->value(),
                           0/*ui->slidPause->value()*/, ui->chkScanTex->isChecked(),
                           ui->chkSaveImgs->isChecked(), _saving_dir);
    return;
}

void CScanReconstructionDlg::showEvent(QShowEvent * event)
{
    ui->progressBar->setValue(0);
    _in_process         = false;
    ui->chkSaveImgs->setChecked(false);
}

void CScanReconstructionDlg::onCompleteProcessing()
{

    _in_process = false;

    ui->startBtn->setEnabled(true);
    ui->slidMotorPower->setEnabled(true);
    ui->slidMotorStep->setEnabled(true);
    //ui->slidPause->setEnabled(true);

    ui->cancelBtn->setEnabled(true);
    ui->cancelBtn->setText("Cancel");

    ui->chkScanTex->setEnabled(true);

    close_dialog();
}
