#ifndef __CPROJECTIO_H__
#define __CPROJECTIO_H__

#include <QObject>
#include <QString>


class CGLWidget;

class CProjectIO: public QObject
{
    Q_OBJECT
public:
    explicit CProjectIO(QObject *parent = 0);

    void request_save(CGLWidget *, QString);
    void request_load(CGLWidget *, QString);
    void request_export(CGLWidget *, QString);



signals:
    void save_requested();
    void load_requested();
    void export_requested();

    void finished_save_proj();
    void finished_load_proj(bool);
    void finished_export();

public slots:
    void do_io();

protected:
    enum IOMode {NONE=0, SAVE=1, LOAD=2, EXPORT=3};
    CGLWidget * _w;
    QString _fp;
    IOMode _mode;



};

#endif
