#ifndef __CARRAY3D_H__
#define __CARRAY3D_H__


#include <cmath>
#include <iostream>
#include <fstream>

template <class T> 
class CArray3D
{

public:

    CArray3D() : _data(NULL), _xs(0), _ys(0), _zs(0){};

    CArray3D(int xs, int ys, int zs, T def_val = T(0))
    {
		allocate_data(xs, ys, zs);

        {for(int k = 0; k <_zs ; k++)
            for(int j = 0; j <_ys ; j++)
                for(int i = 0; i <_xs ; i++)
                    _data[k][j][i] = def_val;}
    }


    virtual ~CArray3D()
    {
        clear();
    }

 
    void operator =(const CArray3D<T> & cpyarr) 
    {
		if(_data != NULL) clear();

        _xs = cpyarr._xs;
        _ys = cpyarr._ys;
        _zs = cpyarr._zs;

        
		allocate_data(_xs, _ys, _zs);


		for(int k = 0; k <_zs ; k++)
            for(int j = 0; j <_ys ; j++)
				memcpy(_data[k][j], cpyarr._data[k][j], _xs*sizeof(T));

    }


   // 
   void multiply(const CArray3D<T> & arr1, const CArray3D<T> & arr2)
   {
       if(_xs!= arr1._xs || _xs != arr2._xs || 
          _ys!= arr1._ys || _ys != arr2._ys ||
          _zs!= arr1._zs || _zs != arr2._zs    )
       {
          std::cout << "Error: multiplication failed " << std::endl;
          return;
       }
    
       for(int k = 0; k <_zs ; k++)
           for(int j = 0; j <_ys ; j++)
               for(int i = 0; i <_xs ; i++)
                   _data[k][j][i] = arr1._data[k][j][i]* arr2._data[k][j][i];


   }

   void clean_data(T def_val = T(0))
   {
   
       for(int k = 0; k <_zs ; k++)
           for(int j = 0; j <_ys ; j++)
               for(int i = 0; i <_xs ; i++)
                   _data[k][j][i] = def_val;
   }

   void dump(std::string fname)
   {
	    std::ofstream ofile;
        ofile.open(fname.c_str(), std::ios::out | std::ios::binary);
        if(! ofile.is_open())
        {
            std::cerr << "Error: cannot save file " << std::endl ;
           return; 
        }

		for(int k = 0; k <_zs ; k++)
           for(int j = 0; j <_ys ; j++)
               for(int i = 0; i <_xs ; i++)
					ofile.write((char *)(& _data[k][j][i]), sizeof(T));


		ofile.close();

   }

    T*** _data;

    int _xs;
    int _ys;
    int _zs; 


protected:


    void allocate_data(int xs, int ys, int zs)
    {
           // _data = (T **) new T[ys]; 
        _data = new T**[zs]; 

		{for(int k = 0; k <zs ; k++)
            _data[k] =  new T*[ys];}

		for(int k = 0; k <zs ; k++)
			for(int j = 0; j <ys ; j++)
				 _data[k][j] =  new T[xs]; 

		
		
/*		_data = (T ***) new T[zs]; 

        {for(int k = 0; k <zs ; k++)
            _data[k] = (T**) new T[ys];}

        {for(int k = 0; k <zs ; k++)
            for(int j = 0; j <ys ; j++)
                _data[k][j] =  new T[xs];} 
*/
        _xs = xs;
        _ys = ys;
        _zs = zs;
    }

    void clear()
    {

        if(_data== NULL) return;
        {for(int k = 0; k <_zs ; k++)
            for(int j = 0; j <_ys ; j++)
				delete [] _data[k][j];}

        {for(int k = 0; k <_zs ; k++)
            delete [] _data[k]; }

        delete [] _data; 

       _xs = 0;
       _ys = 0;
       _zs = 0;
       _data = NULL;
   }



};



#endif
