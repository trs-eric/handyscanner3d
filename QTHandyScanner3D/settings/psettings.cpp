#include "psettings.h"
#include <cmath>
#include "graphicutils.h"
#include <iostream>
#include <QSettings>
#include <QFile>
#include <ctime>

#include "opencv_aux_utils.h"

#include "scene_ini_constants.h"

QString g_app_version = QString("2.95");


float CCCDCamera::_focal_legth = 830;
int CCCDCamera::_exposure = 250;
int CCCDCamera::_exposure_tex = 400;

int CCCDCamera::_red_gain = 30;
int CCCDCamera::_gre_gain = 25;
int CCCDCamera::_blu_gain = 30;

void  difference_image_fast_supp_red(QImage * dest, QImage * src, float thres)
{
   //// std::cout<<"!!!!!!!!!!! " << src->bitPlaneCount() << std::endl;

    unsigned width  = src->width();
    unsigned height = src->height();

    cv::Mat mask_c = cv::Mat::zeros(height,width,CV_8UC1);

    for (int y = 0; y < height; y++)
    {

        uchar * oYLine   =  dest->scanLine(y);
        uchar * iYLine   =   src->scanLine(y);
      for (int x = 0; x < width; x++, iYLine+=3, oYLine+=3)
      /// for (int x = 0; x < width; x++, iYLine+=4, oYLine+=4)
      {
          uchar vsrc_r   = *iYLine;
          uchar vsrc_g   = *(iYLine+1);
          uchar vsrc_b   = *(iYLine+2);

          uchar vdest_r   = *oYLine;
          uchar vdest_g   = *(oYLine+1);
          uchar vdest_b   = *(oYLine+2);


          int  red = (vdest_r > vsrc_r)? (vdest_r - vsrc_r) : 0;// (  qRed(vdest) >   qRed(vsrc))? (  qRed(vdest) -   qRed(vsrc)) :  0;// ( qRed(vsrc) -   qRed(vdest));
          int  green =(vdest_g > vsrc_g)? (vdest_g - vsrc_g) : 0;// (qGreen(vdest) > qGreen(vsrc))? (qGreen(vdest) - qGreen(vsrc)) : 0;//(qGreen(vsrc) - qGreen(vdest));
          int  blue  =  (vdest_b > vsrc_b)? (vdest_b - vsrc_b) : 0;; //( qBlue(vdest) >  qBlue(vsrc))? ( qBlue(vdest) -  qBlue(vsrc)) : 0;// ( qBlue(vsrc) -  qBlue(vdest));

          double val =   0.2989 * double(red) +  0.5870 * double(green) + 0.1140 * double(blue);
          mask_c.at<uchar>( y, x) =  (uchar) val;


          *oYLine = red;
          *(oYLine + 1) = green;
          *(oYLine + 2) = blue;

      }
    }

    cv::Mat mask_bw;

    float threas = 0.95f * CGlobalSettings::_lcthresh ;
    threshold(mask_c, mask_bw, (int)threas , 255,  CV_THRESH_BINARY  /* | CV_THRESH_OTSU*/ );


    int erosion_size = 3;
    cv::Mat element = cv::getStructuringElement( cv::MORPH_RECT,
                                           cv::Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                                           cv::Point( erosion_size, erosion_size ) );
    int erosion_size2 = 9;
    cv::Mat element2 = cv::getStructuringElement( cv::MORPH_ELLIPSE,
                                           cv::Size( 2*erosion_size2 + 1, 2*erosion_size2+1 ),
                                           cv::Point( erosion_size2, erosion_size2 ) );

    cv::Mat erod_bw;
    erode( mask_bw, erod_bw, element );

    dilate( erod_bw, mask_bw, element);


    /// apply mask
    for (int y = 0; y < height; y++)
    {
      uchar * oYLine   =  dest->scanLine(y);

      for (int x = 0; x < width; x++, oYLine+=3)
      {

          if( mask_bw.at<uchar>(y,x) == 0)
          {
              *oYLine = 0;
              *(oYLine + 1) = 0;
              *(oYLine + 2) = 0;
          }
      }
    }

}

void  difference_image_fast(QImage * dest, QImage * src, bool dark_bckg )
{

    unsigned width  = src->width();
    unsigned height = src->height();

    for (int y = 0; y < height; y++)
    {
        QRgb * oYLine   = (QRgb*)dest->scanLine(y);
        QRgb * iYLine   = (QRgb*) src->scanLine(y);
      for (int x = 0; x < width; x++, iYLine++, oYLine++)
      {
          QRgb vsrc  = *iYLine;
          QRgb vdest = *oYLine;

          int red , green, blue;

         // if(dark_bckg )
          {
            red =  (  qRed(vdest) >   qRed(vsrc))? (  qRed(vdest) -   qRed(vsrc)) :  0;// ( qRed(vsrc) -   qRed(vdest));
            green =  (qGreen(vdest) > qGreen(vsrc))? (qGreen(vdest) - qGreen(vsrc)) : 0;//(qGreen(vsrc) - qGreen(vdest));
            blue  =  ( qBlue(vdest) >  qBlue(vsrc))? ( qBlue(vdest) -  qBlue(vsrc)) : 0;// ( qBlue(vsrc) -  qBlue(vdest));
          }
         // else
          {
          //    red =  (  qRed(vdest) >   qRed(vsrc))?  0 : ;
          //    green =  (qGreen(vdest) > qGreen(vsrc))? 0: ;
          //    blue  =  ( qBlue(vdest) >  qBlue(vsrc))? 0 : ;

          }

          *oYLine = qRgb( red, green,  blue);
      }
    }
}




QImage * CCCDCamera::defisheye_image(QImage * in)
{

    QSize szold = in->size();
    int xsold   = szold.width();
    int ysold   =  szold.height();
    int cx_corr = xsold * _defish_extend/2;
    int cy_corr = ysold * _defish_extend/2;


    QSize sz = (1 + _defish_extend) * in->size() * _defish_scale;
    int xs = sz.width();
    int ys = sz.height();

    QImage * res_img = new QImage(sz, in->format());

    //std::cout << xs << " " << ys << " " << xsold << " " << ysold << std::endl;
    //std::cout << _defish_extend << " " << cx_corr << " " << cy_corr<< std::endl;

    for(int y = 0; y < ys; y++)
    {
        for(int x =0; x < xs; x++ )
        {

            float magic_dx = 4; // TODO why is this?
            float xx = (float(x)- CGlobalSettings::_delta_x_proc + magic_dx)/_defish_scale - _focal_cx - cx_corr /*- CGlobalSettings::_delta_x_proc*/;
            float yy = float(y)/_defish_scale - _focal_cy - cy_corr + _defish_deltay;
            float r = sqrt(float(xx*xx) + float(yy*yy));

            float rx = _focal_legth*atan(r/_focal_legth) * xx / r +_focal_cx  ;
            float ry = _focal_legth*atan(r/_focal_legth) * yy / r +_focal_cy  ;

            if(rx >= 0 && rx < xsold  && ry >= 0 && ry < ysold )
            {
                //// add nearest interpolation and check result
                int x0 = floor(rx);
                int y0 = floor(ry);
                //int x1 = x0  + 1;
                //int y1 = y0  + 1;

                QRgb p0 = in->pixel(x0, y0);

                res_img->setPixel(x , y , p0);

            }
        }
    }


    QImage * res2_img = new QImage(sz/_defish_scale, in->format());
    for(int y = 0; y < ys/_defish_scale; y++)
    {
        for(int x =0; x < xs/_defish_scale; x++ )
        {
            QRgb p0 = res_img->pixel(2*x,2*y);
            int p0r = qRed(p0);
            int p0g = qGreen(p0);
            int p0b = qBlue(p0);

            QRgb p1 = res_img->pixel(2*x,2*y+1);
            int p1r = qRed(p1);
            int p1g = qGreen(p1);
            int p1b = qBlue(p1);

            QRgb p2 = res_img->pixel(2*x+1,2*y);
            int p2r = qRed(p2);
            int p2g = qGreen(p2);
            int p2b = qBlue(p2);

            QRgb p3 = res_img->pixel(2*x+1,2*y+1);
            int p3r = qRed(p3);
            int p3g = qGreen(p3);
            int p3b = qBlue(p3);

            float nred = ( p0r + p1r + p2r + p3r)/4;
            float ngre = ( p0g + p1g + p2g + p3g)/4;
            float nblu = ( p0b + p1b + p2b + p3b)/4;


            QRgb p =  qRgb(nred, ngre, nblu);
            res2_img->setPixel(x , y , p);
        }
    }

    delete res_img;

    return res2_img;
}



void CCCDCamera::defisheye_image_fast(QImage * in, QImage *& res_img, QImage *& res2_img, int * & defish_map)
{
    QSize szold = in->size();
    const int xsold   =  szold.width();
    const int ysold   =  szold.height();
    const int cx_corr = xsold * _defish_extend/2;
    const int cy_corr = ysold * _defish_extend/2;

    QSize sz = (1 + _defish_extend) * in->size() * _defish_scale;
    int xs = sz.width();
    int ys = sz.height();

    // do we need cleaning ? fill
    if(res_img == NULL)
        res_img = new QImage(sz, in->format());
    //else
    //    res_img->fill(0);

    if( res2_img == NULL)
        res2_img = new QImage(sz/_defish_scale, in->format());

//clock_t msstart,  mffinish;
//msstart = clock();

    if(defish_map == NULL)
    {
        int line_size = xs * ys ;
        defish_map = new int [line_size];

        const float magic_dx = 4;
        const float ax = - CGlobalSettings::_delta_x_proc + magic_dx;
        const float dx = -_focal_cx - cx_corr;

        #pragma omp parallel
        {
            #pragma omp for
            for(int y = 0; y < ys; y++)
            {
                float yy  = float(y)/_defish_scale - _focal_cy - cy_corr + _defish_deltay;
                float yy2 = yy*yy;

                for(int x =0; x < xs; x++)
                {

                    float xx = (float(x)+ax)/_defish_scale + dx ;
                    float r =  sqrt(float(xx*xx) + yy2);
                    float rf = r/_focal_legth;

                    float atan_rf = atan(rf) / rf;

                    float rx =  atan_rf * xx +_focal_cx  ;
                    float ry =  atan_rf * yy +_focal_cy  ;
                    int irx = int(rx);
                    int iry = int(ry);

                    if((irx >= 0) && (irx < xsold) && (iry >= 0) && (iry < ysold) )
                        defish_map[y * xs + x] = xsold * iry + irx;
                    else
                        defish_map[y * xs + x] = 0;
                }
            }
        }

    }

    QRgb * iBits = (QRgb *) in->bits();
    #pragma omp parallel
    {
        #pragma omp for
        for(int y = 0; y < ys; y++)
        {
            QRgb * oYLine   = (QRgb*)res_img->scanLine(y);
            int * def_line = (defish_map + y * xs);
            for(int x =0; x < xs; x++,  oYLine++, def_line++)
            {
                *oYLine = iBits[*def_line];
            }
        }
    }
//mffinish = clock();
//tot1 += (mffinish - msstart);

//msstart = clock();
    int yss = ys/_defish_scale;
    int xss = xs/_defish_scale;

    #pragma omp parallel
    {
        #pragma omp for
        for(int y = 0; y < yss; y++)
        {

            int dy = 2*y;
            QRgb * iYLine     = (QRgb*)res_img->scanLine(dy);
            QRgb * iYLinep1   = (QRgb*)res_img->scanLine(dy+1);
            QRgb * oYLine     = (QRgb*)res2_img->scanLine(y);

            for(int x =0; x < xss; x++,  oYLine++)
            {
                int dx = 2*x;

                QRgb p0 = iYLine[dx];
                int p0r = qRed(p0);
                int p0g = qGreen(p0);
                int p0b = qBlue(p0);

                QRgb p1 = iYLinep1[dx];
                int p1r = qRed(p1);
                int p1g = qGreen(p1);
                int p1b = qBlue(p1);

                QRgb p2 = iYLine[dx+1];
                int p2r = qRed(p2);
                int p2g = qGreen(p2);
                int p2b = qBlue(p2);

                QRgb p3 = iYLinep1[dx+1];
                int p3r = qRed(p3);
                int p3g = qGreen(p3);
                int p3b = qBlue(p3);

                int nred = ( p0r + p1r + p2r + p3r)>>2; // not tested
                int ngre = ( p0g + p1g + p2g + p3g)>>2;
                int nblu = ( p0b + p1b + p2b + p3b)>>2;

                *oYLine =  qRgb(nred, ngre, nblu);
            }
        }
    }

//mffinish = clock();
//tot2 += (mffinish - msstart);

}


void CCCDCamera::difference_image_fast(QImage * dest, QImage * src)
{
    unsigned width  = src->width();
    unsigned height = src->height();
    #pragma omp parallel for
    for (int y = 0; y < height; y++)
    {
        QRgb * oYLine   = (QRgb*)dest->scanLine(y);
        QRgb * iYLine   = (QRgb*)src->scanLine(y);
      for (int x = 0; x < width; x++, iYLine++, oYLine++)
      {
          QRgb vsrc  = *iYLine;
          QRgb vdest = *oYLine;

          int red   = (  qRed(vdest) >   qRed(vsrc))? (  qRed(vdest) -   qRed(vsrc)) : 0;
          int green = (qGreen(vdest) > qGreen(vsrc))? (qGreen(vdest) - qGreen(vsrc)) : 0;
          int blue  = ( qBlue(vdest) >  qBlue(vsrc))? ( qBlue(vdest) -  qBlue(vsrc)) : 0;

          *oYLine = qRgb( red, green,  blue);
      }
    }
}

void CCCDCamera::difference_image(QImage * dest, QImage * src)
{
    unsigned width  = src->width();
    unsigned height = src->height();
    for (int y = 0; y < height; y++)
    {      
      for (int x = 0; x < width; x++ )
      {

          QRgb vsrc  =  src->pixel(x,y);
          QRgb vdest = dest->pixel(x,y);

          int red   = (  qRed(vdest) >   qRed(vsrc))? (  qRed(vdest) -   qRed(vsrc)) : 0;
          int green = (qGreen(vdest) > qGreen(vsrc))? (qGreen(vdest) - qGreen(vsrc)) : 0;
          int blue  = ( qBlue(vdest) >  qBlue(vsrc))? ( qBlue(vdest) -  qBlue(vsrc)) : 0;

          QRgb dp = qRgb( red, green,  blue);
          dest->setPixel(x,y, dp);
      }
    }
}

QImage * CCCDCamera::processed_image(QImage * in)
{
    QSize sz = in->size();

    int xs = sz.width()/2;
    int ys = sz.height()/2;

    QImage * res_img = new QImage(sz/2, in->format());
    for(int y = 0; y < ys; y++)
    {
        for(int x =0; x < xs; x++ )
        {

            // camera is rotated
            QRgb p00 = in->pixel(2*x,   2*y);
            QRgb p10 = in->pixel(2*x+1, 2*y);
            QRgb p01 = in->pixel(2*x,  2*y+1);
            QRgb p11 = in->pixel(2*x+1,2*y+1);


            int r  = qRed (p10);
            int g1 = qGreen(p00);
            int g2 = qGreen(p11);
            int b  = qBlue  (p01);

            QRgb value = qRgb(r, (g1+g2)/2, b);
            if(((x + CGlobalSettings::_delta_x_proc) < xs) &&
               ((x + CGlobalSettings::_delta_x_proc) >= 0))
                res_img->setPixel(x + CGlobalSettings::_delta_x_proc , y, value);
        }
    }
    return res_img;
}

void CCCDCamera::processed_image_fast(QImage * in, QImage *& res_img)
{
    QSize sz = in->size();

    int xs = sz.width()/2;
    int ys = sz.height()/2;

    if(res_img == NULL)
        res_img = new QImage(sz/2, in->format());


    #pragma omp parallel for
    for(int y = 0; y < ys; y++)
    {
        QRgb * rYLine   = (QRgb*)res_img->scanLine(y);
        QRgb * iYLine   = (QRgb*)in->scanLine(2*y);
        QRgb * iYLinep1 = (QRgb*)in->scanLine(2*y+1);

        for(int x =0; x < xs; x++ )
        {
            int dbx = 2*x;

            // camera is rotated
            QRgb p00 = iYLine  [dbx  ];
            QRgb p10 = iYLine  [dbx+1];
            QRgb p01 = iYLinep1[dbx  ];
            QRgb p11 = iYLinep1[dbx+1];

            int r  = qRed  (p10);
            int g1 = qGreen(p00);
            int g2 = qGreen(p11);
            int b  = qBlue (p01);

            QRgb value = qRgb(r, (g1+g2)/2, b);
            int nx = (x + CGlobalSettings::_delta_x_proc);
            if((nx < xs) && (nx >= 0))
                rYLine[nx] = value;
        }
    }

}


float CGlobalSettings::_gauss_sigma_max = 5.0;
float CGlobalSettings::_gauss_sigma = 0.65;
int   CGlobalSettings::_laser_corrwnd_sz = 3;

float CGlobalSettings::_lcthresh = 30; //16
float CGlobalSettings::_ignore_read_thresh = 170; //16

float CGlobalSettings::_min_opt_dist = 1.0;
float CGlobalSettings::_d=180;
float CGlobalSettings::_h= 80;
float CGlobalSettings::_alpha_x=BGM(63);
float CGlobalSettings::_alpha_z=BGM(86);
/// MTP H 86 grad V 63 grad !!! right values !!
float CGlobalSettings::_defish_alpha_x=BGM(63.64);
float CGlobalSettings::_defish_alpha_z=BGM(79.37);
float CGlobalSettings::_delta_x=0;
float CGlobalSettings::_delta_y=0;
float CGlobalSettings::_betta = BGM(30);           // real BGM(35);
float CGlobalSettings::_scan_step_angle = BGM(0.45); // BGM(0.45);
float CGlobalSettings::_delta_x_proc = 8;
float CGlobalSettings::_maximal_radius_pnts=100;

int CGlobalSettings::_left_led_bright  = 20;
int CGlobalSettings::_right_led_bright = 20;

QString CGlobalSettings::_cam_xml_name = QString("./camcalib.xml");

CGlobalSettings::CGlobalSettings()
{

    _ini_name =  QString("./QTRubicon3D.ini");
    load_from_INI();

    g_callibration_map.load(CGlobalSettings::_cam_xml_name );
    g_callibration_map.set_globals_for_focus(_webc_focus);

/*
    /// send exposure to device
    FT_STATUS ftStatus = connectToModule();
    if (ftStatus == FT_OK)
    {
        initCameraRegs();
        writeCameraReg(CCCDCamera::regExposure, CCCDCamera::_exposure);

        writeCameraReg(CCCDCamera::regRed,    CCCDCamera::_red_gain);
        writeCameraReg(CCCDCamera::regGreen1, CCCDCamera::_gre_gain);
        writeCameraReg(CCCDCamera::regGreen2, CCCDCamera::_gre_gain);
        writeCameraReg(CCCDCamera::regBlue,   CCCDCamera::_blu_gain);
    }
    disconnectFromModule();
*/
}

CGlobalSettings::~CGlobalSettings()
{
    save_to_INI();
}

void CGlobalSettings::save_to_INI()
{
    QSettings settings(_ini_name, QSettings::IniFormat);
    settings.setValue("exposure", CCCDCamera::_exposure);
    settings.setValue("exposure_tex", CCCDCamera::_exposure_tex);
    settings.setValue("lcthresh", double(_lcthresh));
    settings.setValue("laser_corrwnd_sz", _laser_corrwnd_sz);


    settings.setValue("delta_x_proc", double(_delta_x_proc));

    settings.setValue("redgain", CCCDCamera::_red_gain);
    settings.setValue("gregain", CCCDCamera::_gre_gain);
    settings.setValue("blugain", CCCDCamera::_blu_gain);

    settings.setValue("gausssigm", double(_gauss_sigma));


    settings.setValue("webc_exposeure", _webc_exposeure);
    settings.setValue("webc_focus", _webc_focus);
    settings.setValue("webc_brightness", _webc_brightness);
    settings.setValue("webc_contrast", _webc_contrast);
    settings.setValue("webc_gamma", _webc_gamma);
    settings.setValue("webc_gain", _webc_gain);
    settings.setValue("webc_shap", _webc_shap);
    settings.setValue("webc_hue", _webc_hue);
    settings.setValue("webc_satu", _webc_satu);
    settings.setValue("webc_wb_red", _webc_wb_red);
    settings.setValue("webc_wb_blue", _webc_wb_blue);

    settings.setValue(ini_meta_angle_correction, double(g_meta_angle_correction));
    settings.setValue(ini_marks_x, g_marks_x);
    settings.setValue(ini_marks_y, g_marks_y);
    settings.setValue(ini_single_cell_width, double(g_single_cell_width));

    settings.setValue("use_acc_delta", g_use_acc_delta);
    settings.setValue("acc_delta_roll", double(g_acc_delta_roll));
    settings.setValue("acc_delta_pitch", double(g_acc_delta_pitch));

}

void CGlobalSettings::load_from_INI()
{
    QFile inif(_ini_name);
    if(!inif.exists()) return;


    QSettings settings(_ini_name, QSettings::IniFormat);
    CCCDCamera::_exposure = settings.value("exposure",CCCDCamera::_exposure).toInt();
    CCCDCamera::_exposure_tex = settings.value("exposure_tex",CCCDCamera::_exposure_tex).toInt();
    _lcthresh       = settings.value("lcthresh", _lcthresh).toFloat();
    _laser_corrwnd_sz = settings.value("laser_corrwnd_sz",_laser_corrwnd_sz).toInt();

    _delta_x_proc   = settings.value("delta_x_proc").toFloat();

    CCCDCamera::_red_gain = settings.value("redgain",CCCDCamera::_red_gain).toInt();
    CCCDCamera::_gre_gain = settings.value("gregain",CCCDCamera::_gre_gain).toInt();
    CCCDCamera::_blu_gain = settings.value("blugain",CCCDCamera::_blu_gain).toInt();

    _gauss_sigma = settings.value("gausssigm", double(_gauss_sigma)).toFloat();


    _webc_exposeure = settings.value("webc_exposeure", _webc_exposeure).toInt();
    _webc_focus = settings.value("webc_focus", _webc_focus).toInt();

    _webc_brightness = settings.value("webc_brightness", _webc_brightness).toInt();
    _webc_contrast = settings.value("webc_contrast", _webc_contrast).toInt();
    _webc_gamma = settings.value("webc_gamma", _webc_gamma).toInt();

    _webc_shap = settings.value("webc_shap", _webc_shap).toInt();
    _webc_hue = settings.value("webc_hue", _webc_hue).toInt();
    _webc_satu = settings.value("webc_satu", _webc_satu).toInt();


    _webc_wb_red = settings.value("webc_wb_red", _webc_wb_red).toInt();
    _webc_wb_blue = settings.value("webc_wb_blue", _webc_wb_blue).toInt();
    _webc_gain = settings.value("webc_gain", _webc_gain).toInt();

    g_meta_angle_correction = settings.value(ini_meta_angle_correction, g_meta_angle_correction).toFloat();
    g_marks_x = settings.value(ini_marks_x, g_marks_x).toInt();
    g_marks_y = settings.value(ini_marks_y, g_marks_y).toInt();
    g_single_cell_width = settings.value(ini_single_cell_width, g_single_cell_width).toFloat();

    g_use_acc_delta = settings.value("use_acc_delta", g_use_acc_delta).toBool();
    g_acc_delta_roll = settings.value("acc_delta_roll", double(g_acc_delta_roll)).toFloat();
    g_acc_delta_pitch = settings.value("acc_delta_pitch", double(g_acc_delta_pitch)).toFloat();
}

float get_webc_fovx(float focus)
{
    return -0.033367*focus + 57.7633 + 0.5;
}

float get_webc_fovy(float focus)
{
    return  -0.021901*focus + 44.9267 + 0.2;
}


Elas::parameters CGlobalHSSettings::_disparity_parameters;
QString CGlobalHSSettings::_left_img_prefix   = "left_";
QString CGlobalHSSettings::_right_img_prefix  = "right_";
QString CGlobalHSSettings::_rot_angles_file   = "rotmap.txt";
QString CGlobalHSSettings::_current_data_path = "./sfacerpUfXL/";
int CGlobalHSSettings::glob_max_frames = 24;
float CGlobalHSSettings::global_ignore_disparity_for_matching=30;
float CGlobalHSSettings::global_ignore_disparity_for_icp = 130.0;
float CGlobalHSSettings::global_ignore_disparity_offset = 125.0;
static int CGlobalHSSettings::global_spcl_decim_step = 10;

/*QString CGlobalHSSettings::_current_data_path = "./sfacerpUfXXL2/";
int CGlobalHSSettings::glob_max_frames = 100;
float CGlobalHSSettings::global_ignore_disparity_for_matching=60;
float CGlobalHSSettings::global_ignore_disparity_for_icp = 130.0;
float CGlobalHSSettings::global_ignore_disparity_offset = 125.0;
static int CGlobalHSSettings::global_spcl_decim_step = 10;*/


cv::Ptr<cv::FeatureDetector> CGlobalHSSettings::_feature_detector = cv::FeatureDetector::create("PyramidFAST");
cv::Ptr<cv::DescriptorExtractor> CGlobalHSSettings::_feature_extractor = cv::DescriptorExtractor::create("ORB");
///cv::Ptr<cv::DescriptorExtractor> CGlobalHSSettings::_feature_extractor = cv::DescriptorExtractor::create("SIFT");


cv::Mat CGlobalHSSettings::_stereo_Q = (cv::Mat_<double>(4,4) << 1., 0., 0., -6.3939643859863281e+002,
                                          0., 1., 0., -4.7951318359375000e+002,
                                          0., 0., 0., 1.9322238394618969e+003, 0.,
                                          0., 2.3800047808414698e-001, 0. );

cv::Mat CGlobalHSSettings::_camL_K = (cv::Mat_<double>(3,3) << 1.9323147802365747e+003, 0., 6.3949586966917070e+002,
                                     0.,  1.9322613618745359e+003, 4.7951334959687784e+002,
                                     0.,  0., 1. );

/*

cv::Mat g_cam_intrinsics = (cv::Mat_<double>(3,3) << 1.2064409469046241e+003, 0., 6.4277506535158716e+002,
                            0.,  1.2085632964489064e+003, 4.6724734379688294e+002,
                            0.,  0., 1. );

cv::Mat g_cam_distortion =  (cv::Mat_<double>(1,5) << 1.4065502014329997e-001,
                                            -6.0903189642532984e-001, 0,0,
                                            7.8849940942535335e-001);
*/
/*cv::Mat g_cam_intrinsics = (cv::Mat_<double>(3,3) << 1241.704557991638, 0., 635.6836944614682,
                            0.,  1244.223050607849, 469.0849146347448,
                            0.,  0., 1. );

cv::Mat g_cam_distortion =  (cv::Mat_<double>(1,5) << 0.1419638469628867,
                                           -0.5310298816929245, 0,0,
                                            0.540899321978098);*/



cv::Mat g_cam_intrinsics = (cv::Mat_<double>(3,3) << 1249.258726236803, 0.,  637.0345200066868,
                            0.,  1251.470707840956, 467.9631925717999,
                            0.,  0., 1. );

cv::Mat g_cam_distortion =  (cv::Mat_<double>(1,5) << 0.1711709484267146,
                                            -0.6959952159276204, 0,0,
                                            0.9245082710787145);
CCalibrationMap g_callibration_map;

std::string g_rune_pattern_fname = ("Rubi.tpat");

double convert_real_coord_to_gl_x(double rx)
{
    return   rx * CGlobalHSSettings::glob_gl_scale + CGlobalHSSettings::glob_gl_offx;
}

double convert_real_coord_to_gl_y(double ry)
{
    // disparity version
    return -ry * CGlobalHSSettings::glob_gl_scale + CGlobalHSSettings::glob_gl_offy;
    // disparity version
    // return   ry * CGlobalHSSettings::glob_gl_scale + CGlobalHSSettings::glob_gl_offy;
}

double convert_real_coord_to_gl_z(double rz)
{
    return  -rz * CGlobalHSSettings::glob_gl_scale + CGlobalHSSettings::glob_gl_offz;
}

float CGlobalHSSettings::glob_gl_scale     =  0.04f;//1./8.0;
float CGlobalHSSettings::glob_gl_offx      =  0.f;   // 0
float CGlobalHSSettings::glob_gl_offy      =  2.f;   // 0
float CGlobalHSSettings::glob_gl_offz      =  11.f;  //  7

float CGlobalHSSettings::gcolor_sr  = 0.2000f;
float CGlobalHSSettings::gcolor_sg  = 0.2235f;
float CGlobalHSSettings::gcolor_sb  = 0.2313f;

float CGlobalHSSettings::gcolor_cr  = 0.32549f;
float CGlobalHSSettings::gcolor_cg  = 0.36862f;
float CGlobalHSSettings::gcolor_cb  = 0.4000f;

float CGlobalHSSettings::gcolor_mr = 0.882;
float CGlobalHSSettings::gcolor_mg = 0.803;
float CGlobalHSSettings::gcolor_mb = 0.666;

float CGlobalHSSettings::gl_light_Ambient[4]  = {0.1f, 0.1f, 0.1f, 1.0f};
float CGlobalHSSettings::gl_light_Diffuse[4]  = {0.7f, 0.7f, 0.7f, 1.0f};
float CGlobalHSSettings::gl_light_Position[4] = {-10.0f, 5.0f, 73.0f, 1.0f};


float CGlobalHSSettings::gl_mat_Specularity[4] = {.05f, .05f, .05f, 1.0f};
float CGlobalHSSettings::gl_mat_Shiness = 64.f;

float CGlobalHSSettings::gpoint_size = 2.0;

float CGlobalHSSettings::gl_cam_alpha  = 0.38f;

int CGlobalHSSettings::_computing_threads = cv::getNumberOfCPUs();

float CGlobalHSSettings::gl_table_rot_center_x = 0;
float CGlobalHSSettings::gl_table_rot_center_y = 0;
float CGlobalHSSettings::gl_table_rot_center_z = 0;

int CGlobalSettings::_webc_exposeure=-3;
int CGlobalSettings::_webc_focus=68;
int CGlobalSettings::_webc_brightness = 0;
int CGlobalSettings::_webc_contrast = 0;
int CGlobalSettings::_webc_gamma = 0;
int CGlobalSettings::_webc_shap = 1;
int CGlobalSettings::_webc_hue = 0;
int CGlobalSettings::_webc_satu = 0;


int CGlobalSettings::_webc_wb_red=100;
int CGlobalSettings::_webc_wb_blue=2000;
int CGlobalSettings::_webc_gain=20;

bool CGlobalFRSettings::_use = false;
float CGlobalFRSettings::_enhancement = 0.3;
float CGlobalFRSettings::_transition  = 1.0f;
QColor CGlobalFRSettings::_rc_bck_colorL = QColor(153,85,53);
float CGlobalFRSettings::_rc_shadowL[3] = {100.f, 57.f, -46.f};
QColor CGlobalFRSettings::_rc_bcklight_colorL = QColor(213,123,71);
float CGlobalFRSettings::_rc_backlightL[3] = {96.f, 26.f, -31.f};
float CGlobalFRSettings::_rc_toplightL[3] = {96.f, 54.f, -34.f};
float CGlobalFRSettings::_rc_speclightL[3] = {0.f, 0.f, 0.f};
QColor CGlobalFRSettings::_rc_bck_colorR =  QColor(87,99,121);
float CGlobalFRSettings::_rc_shadowR[3] = {100.f, 6.f, 23.f};
QColor CGlobalFRSettings::_rc_bcklight_colorR = QColor(184,235,242);
float CGlobalFRSettings::_rc_backlightR[3] = {70.f, 23.f, -43.f};
float CGlobalFRSettings::_rc_toplightR[3] = {53.f, 29.f, -11.f};
float CGlobalFRSettings::_rc_speclightR[3] = {0.f, 0.f, 0.f};


CArray2D< std::list<aux_pix_data *> > * g_polar_map = NULL;

void clean_g_polar_map()
{
    if(g_polar_map != NULL)
    {
        int psx = g_polar_map->_xs;
        int psy = g_polar_map->_ys;
        for (int y = 0; y < psy; y++)
        {
            for (int x = 0; x < psx; x++)
            {
                if(g_polar_map->_data[y][x].size()>0)
                {
                    std::list<aux_pix_data *>::iterator itL  = g_polar_map->_data[y][x].begin();

                    for ( ; itL  != g_polar_map->_data[y][x].end()  ; itL++ )
                        delete (*itL) ;
                    g_polar_map->_data[y][x].clear();
                }
            }
        }
        g_polar_map == NULL;
    }
}

QString cmd_sp_laser_off = QString("0");
QString cmd_sp_laser_on = QString("1");
QString cmd_sp_laser_pos = QString("3");

QString cmd_sp_motoleft = QString("4");
QString cmd_sp_motostop = QString("5");
QString cmd_sp_motoright = QString("6");

QString cmd_sp_moto_step_right= QString("7");

C3DCorrector g_3d_corrector;

float g_camera_laser_dist = 105;
float g_laser_depth_offset = -10;
float g_meta_angle_correction = -160;

int g_marks_x  = 13;
int g_marks_y = 12;
float g_single_cell_width = 10;


bool g_use_acc_delta = false;
float g_acc_delta_roll = 0;
float g_acc_delta_pitch = 0;
bool g_use_ray_tracing = true;
