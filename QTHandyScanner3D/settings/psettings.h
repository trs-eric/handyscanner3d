#ifndef PSETTINGS_H
#define PSETTINGS_H

#include <QImage>
#include <QColor>

#include "libelasomp/elas.h"

#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include "carray2d.h"
#include "graphicutils.h"
#include "c3dcorrector.h"
#include "ccalibrationmap.h"

#if __GNUC__
#if __x86_64__ || __ppc64__
#define SYSBITS 64
#else
#define SYSBITS 32
#endif
#endif

extern QString g_app_version;

struct CCCDCamera
{
    static float _focal_legth;
    //old image center before defishing and extension
    static const float _focal_cx    = 486;
    static const float _focal_cy    = 648;

    static const float _defish_scale = 2.0;
    static const float _defish_extend = 0.15;
    static const int  _defish_deltay  = 80;
    //static const int  _defish_deltay  = 0;
    static QImage * processed_image(QImage * );
    static void processed_image_fast(QImage * in, QImage *& res_img);

    static QImage * defisheye_image(QImage * );
    static void defisheye_image_fast(QImage * in, QImage *& aux, QImage *& out, int * &);

    static void difference_image(QImage * , QImage * );
    static void difference_image_fast(QImage * , QImage * );

    // exposure for laser images
    static int _exposure;
    // exposure for texture images
    static int _exposure_tex;


    // colors settings
    static int _red_gain;
    static int _gre_gain;
    static int _blu_gain;

    static const int regExposure = 9;
    static const int regRed = 45;
    static const int regGreen1 = 43;
    static const int regGreen2 = 46;
    static const int regBlue = 44;
};

struct CGlobalSettings
{
    // gaussian sigma edge
    static float _gauss_sigma_max;
    static float _gauss_sigma;

    static int _laser_corrwnd_sz;
    // line color threshold
    static float _lcthresh;

    static float _ignore_read_thresh;

    // optimizing point distance
    static float _min_opt_dist;

    // camera to table rotation ceneter distance (mm)
    static float _d;
    // camera elevation (mm)
    static float _h;

    // horizontal camera FOV (rad)
    static float _alpha_x;
    // vertical camera FOV (rad)
    static float _alpha_z;

    // horizontal camera FOV (rad) - after defishis
    static float _defish_alpha_x;
    // vertical camera FOV (rad) - after defishis
    static float _defish_alpha_z;


    // image center and rotation center horizontal offset (pix)
    static float _delta_x_proc;

    // image center and rotation center horizontal offset (pix)
    static float _delta_x;
    // image center and rotation center vertical offset (pix)
    static float _delta_y;
    // laser angle (rad)
    static float _betta;

    // scanning angle step (rad)
    static float _scan_step_angle;

    // maximal points radius to be in a cloud
    static float _maximal_radius_pnts;

    // left led brightness
    static int _left_led_bright;

    // right led brightness
    static int _right_led_bright;



    //////// web camera settings
    static int _webc_exposeure;
    static int _webc_focus;
    static int _webc_brightness;
    static int _webc_contrast;
    static int _webc_gamma;

    static int _webc_shap;
    static int _webc_hue;
    static int _webc_satu;


    static int _webc_wb_red;
    static int _webc_wb_blue;
    static int _webc_gain;

    ///////////////////////

    // ini file name
    QString _ini_name;

    // cam calib name
    static QString _cam_xml_name;

    CGlobalSettings();
    virtual ~CGlobalSettings();
protected:
    void save_to_INI();
    void load_from_INI();


};

float get_webc_fovx(float focus);
float get_webc_fovy(float focus);

struct CGlobalFRSettings
{
    static bool _use;
    static float _enhancement;
    static float _transition;

    static QColor _rc_bck_colorL;
    static float _rc_shadowL[3];
    static QColor _rc_bcklight_colorL;
    static float _rc_backlightL[3];
    static float _rc_toplightL[3];
    static float _rc_speclightL[3];

    static  QColor _rc_bck_colorR;
    static float _rc_shadowR[3];
    static QColor _rc_bcklight_colorR;
    static float _rc_backlightR[3];
    static float _rc_toplightR[3];
    static float _rc_speclightR[3];

};

struct CGlobalHSSettings
{
    // aux constants for offline test
    static QString _current_data_path;
    static QString _left_img_prefix;
    static QString _right_img_prefix;
    static QString _rot_angles_file;
    static int glob_max_frames;


    static Elas::parameters _disparity_parameters;

    static float global_ignore_disparity_for_matching;
    static float global_ignore_disparity_for_icp;
    static float global_ignore_disparity_offset;

    static cv::Ptr<cv::FeatureDetector> _feature_detector;
    static cv::Ptr<cv::DescriptorExtractor> _feature_extractor;

    static int global_spcl_decim_step;

    static cv::Mat _stereo_Q;
    static cv::Mat _camL_K;

    // OpenGL rendering settings
    static  float glob_gl_scale;
    static float glob_gl_offx;
    static float glob_gl_offy;
    static float glob_gl_offz;

    // OpenGL backgorund colors
    static float gcolor_sr;
    static float gcolor_sg;
    static float gcolor_sb;

    static float gcolor_cr;
    static float gcolor_cg;
    static float gcolor_cb;

    // GLMesh default color
    static float gcolor_mr;
    static float gcolor_mg;
    static float gcolor_mb;

    // GLCloud point size
    static float gpoint_size;


    static float gl_light_Position[4];
    static float gl_light_Ambient[4];
    static float gl_light_Diffuse[4];

    static float gl_mat_Specularity[4];
    static float gl_mat_Shiness;

    static float gl_cam_alpha;

    static int _computing_threads;

    static float gl_table_rot_center_x;
    static float gl_table_rot_center_y;
    static float gl_table_rot_center_z;

};

double convert_real_coord_to_gl_x(double rx);
double convert_real_coord_to_gl_y(double ry);
double convert_real_coord_to_gl_z(double rz);

void  difference_image_fast(QImage * dest, QImage * src, bool dark_bckg = true);

void  difference_image_fast_supp_red(QImage * dest, QImage * src, float thres);

extern CArray2D< std::list<aux_pix_data *> > * g_polar_map;
void clean_g_polar_map();

extern QString cmd_sp_laser_off;
extern QString cmd_sp_laser_on;
extern QString cmd_sp_laser_pos;
extern QString cmd_sp_motoleft;
extern QString cmd_sp_motostop;
extern QString cmd_sp_motoright;
extern QString cmd_sp_moto_step_right;


extern C3DCorrector g_3d_corrector;

extern cv::Mat g_cam_intrinsics;
extern cv::Mat g_cam_distortion;
extern std::string g_rune_pattern_fname;

extern CCalibrationMap g_callibration_map;

extern float g_laser_depth_offset;
extern float g_camera_laser_dist;
extern float g_meta_angle_correction;

extern int g_marks_x;
extern int g_marks_y;
extern float g_single_cell_width;

extern bool g_use_acc_delta;
extern float g_acc_delta_roll;
extern float g_acc_delta_pitch;

extern bool g_use_ray_tracing;

#endif // PSETTINGS_H
