#ifndef CGLCAMERA_H
#define CGLCAMERA_H

#include "cglobject.h"
#include "cdimreference.h"

class CGLCamera : public CGLObject, public CDimReference
{
public:

    CGLCamera();
    virtual ~CGLCamera();

    virtual void glDraw();
    virtual void set_view();
    virtual void init() = 0;
    virtual void recompute_tpos() = 0;
    void set_view_point( GLfloat x, GLfloat y, GLfloat z) {_cx = x; _cy = y; _cz =z; }

    // orientation
    GLfloat _cx, _cy, _cz;

    // up_vector
    GLfloat _ux, _uy, _uz;

protected:
    virtual void update_pos() = 0;
    virtual void create_glList();


};

#endif // CGLCAMERA_H
