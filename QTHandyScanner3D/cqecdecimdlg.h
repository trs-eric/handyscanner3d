#ifndef CQECDECIMDLG_H
#define CQECDECIMDLG_H

#include <QDialog>
#include <QMessageBox>

class CGLWidget;
class CTriangularMesh;
class CQECDecimThread;
class CMeshDumpThread;

namespace Ui {
class CQECDecimDlg;
}

class CQECDecimDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CQECDecimDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CQECDecimDlg();

protected:
    void showEvent(QShowEvent * event);
    CTriangularMesh * restore_mesh();

private:
    Ui::CQECDecimDlg *ui;
    CGLWidget *_glWidget;


    bool _in_process;
    bool _on_apply;
    bool _is_result_valid;

    CQECDecimThread * _thread;
    QMessageBox * _info;
    CMeshDumpThread * _ithread;

private slots:
    void onCancelBtn();
    void onPreviewBtn();
    void onApplyBtn();
    void onFinishDumping();

    void onMakeProgressStep(int val);
    void onCompleteProcessing(CTriangularMesh * );
    void onStopProcessing();

    void close_dialog();
};

#endif // CQECDECIMDLG_H
