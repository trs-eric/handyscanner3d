#include "cmeshdumpthread.h"

#include "ctriangularmesh.h"

CMeshDumpThread::CMeshDumpThread(QObject *parent) :
    QThread(parent)
{
}

CMeshDumpThread::~CMeshDumpThread()
{
    wait();
}

void CMeshDumpThread::start_process(CTriangularMesh * mesh, std::string & fname)
{
    _mesh = mesh;
    _fname = fname;
    start();
}

void CMeshDumpThread::run()
{
    if(_mesh != NULL)
    {
        std::ofstream dfile;
        dfile.open(_fname.c_str(), std::ios::out | std::ios::binary);
        _mesh->save(dfile);
        dfile.close();
    }

    emit done_dumping();
    std::cout << "dumping finished" << std::endl;

}
