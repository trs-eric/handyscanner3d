#include "cmovrotscadlg.h"
#include "ui_cmovrotscadlg.h"

#include <QMessageBox>

#include "cglwidget.h"
#include "ctriangularmesh.h"
#include "messages.h"

CMovRotScaDlg::CMovRotScaDlg(CGLWidget * glWidget, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CMovRotScaDlg), _glWidget(glWidget), _ref_mesh(NULL), _valid_res(false)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);


    ui->lockS->setChecked(true);
    ui->valSY->setEnabled(false);
    ui->valSZ->setEnabled(false);

    connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));
    connect(ui->prevBtn,    SIGNAL(clicked()), this, SLOT(onPreviewBtn()));


    connect(this, SIGNAL(send_result(CTriangularMesh*)), _glWidget, SLOT(on_mesh_computed(CTriangularMesh*)));


    connect(ui->valTX, SIGNAL(valueChanged(double)), this, SLOT(onValTXChanged(double)));
    connect(ui->valTY, SIGNAL(valueChanged(double)), this, SLOT(onValTYChanged(double)));
    connect(ui->valTZ, SIGNAL(valueChanged(double)), this, SLOT(onValTZChanged(double)));

    connect(ui->valRX, SIGNAL(valueChanged(double)), this, SLOT(onValRXChanged(double)));
    connect(ui->valRY, SIGNAL(valueChanged(double)), this, SLOT(onValRYChanged(double)));
    connect(ui->valRZ, SIGNAL(valueChanged(double)), this, SLOT(onValRZChanged(double)));

    connect(ui->valSY, SIGNAL(valueChanged(double)), this, SLOT(onValSYChanged(double)));
    connect(ui->valSZ, SIGNAL(valueChanged(double)), this, SLOT(onValSZChanged(double)));

    connect(ui->valSX, SIGNAL(valueChanged(double)), this, SLOT(onValSXChanged(double)));
    connect(ui->lockS, SIGNAL(clicked(bool)), this, SLOT(onLockScale(bool)));

}


void CMovRotScaDlg::init_gui()
{
   CTriMesh * tmesh = _ref_mesh->get_tri_mesh();

    ui->valTX->setValue(tmesh->_trans.x);
    ui->valTY->setValue(tmesh->_trans.y);
    ui->valTZ->setValue(tmesh->_trans.z);

    ui->valRX->setValue(tmesh->_rotat.x);
    ui->valRY->setValue(tmesh->_rotat.y);
    ui->valRZ->setValue(tmesh->_rotat.z);

    ui->valSX->setValue(100*tmesh->_scale.x);
    ui->valSY->setValue(100*tmesh->_scale.y);
    ui->valSZ->setValue(100*tmesh->_scale.z);


}

void CMovRotScaDlg::init_param()
{
    _valid_res = true;

   _ref_mesh =  new CTriangularMesh(*_glWidget->get_mesh_of_current_object());

}

CMovRotScaDlg::~CMovRotScaDlg()
{
    delete ui;
}

void CMovRotScaDlg::onLockScale(bool val)
{
   if(val)
    {
        ui->valSY->setEnabled(false);
        ui->valSZ->setEnabled(false);
        ui->valSY->setValue(ui->valSX->value());
        ui->valSZ->setValue(ui->valSX->value());

        _valid_res = false;

    }
    else
    {
        ui->valSY->setEnabled(true);
        ui->valSZ->setEnabled(true);
    }

}

void CMovRotScaDlg::onApplyBtn()
{
/*
    CPointCloud * rpnts = _glWidget->get_raw_points_of_current_object();
    if(rpnts != NULL)
    {
        rpnts->rotate(BGM(ui->valRX->value()), BGM(ui->valRY->value()), BGM(ui->valRZ->value()));

        rpnts->translate(ui->valTX->value(), ui->valTY->value(), ui->valTZ->value());
        rpnts->scale(ui->valSX->value()/100, ui->valSY->value()/100, ui->valSZ->value()/100);

        restore_transforms();
    }
*/

    //int ret = QMessageBox::warning(this, tr("Warning"), wrn_TexDel, QMessageBox::Ok | QMessageBox::Cancel);
    //if(ret == QMessageBox::Cancel)
    //    return;

   if(!_valid_res)
   {
        CTriangularMesh * res_mesh = make_transforms();
      //  res_mesh->remove_mesh_texture();
        emit send_result(res_mesh);
   }

    delete _ref_mesh;_ref_mesh=NULL;
    close();
}

void CMovRotScaDlg::onCancelBtn()
{
    emit send_result(_ref_mesh);
    close();
}

CTriangularMesh * CMovRotScaDlg::make_transforms()
{
    CTriangularMesh * res_mesh =  new CTriangularMesh(*_ref_mesh);
    CTriMesh * tmesh = res_mesh->get_tri_mesh();

    if(tmesh != NULL)
    {

        tmesh->translate( -tmesh->_mass_center.x - tmesh->_trans.x,
                          -tmesh->_mass_center.y - tmesh->_trans.y,
                          -tmesh->_mass_center.z - tmesh->_trans.z);

        tmesh->rotate_rev( BGM(-tmesh->_rotat.x) ,
                           BGM(-tmesh->_rotat.y) ,
                           BGM(-tmesh->_rotat.z) );

        tmesh->scale(  (ui->valSX->value()/100.)/tmesh->_scale.x,
                       (ui->valSY->value()/100.)/tmesh->_scale.y,
                       (ui->valSZ->value()/100.)/tmesh->_scale.z );
        tmesh->_scale.x = (ui->valSX->value()/100.);
        tmesh->_scale.y = (ui->valSY->value()/100.);
        tmesh->_scale.z = (ui->valSZ->value()/100.);

        tmesh->rotate( BGM(ui->valRX->value() ) ,
                       BGM(ui->valRY->value() ) ,
                       BGM(ui->valRZ->value() ) );
        tmesh->_rotat.x = ui->valRX->value();
        tmesh->_rotat.y = ui->valRY->value();
        tmesh->_rotat.z = ui->valRZ->value();


        tmesh->translate(  ui->valTX->value() + tmesh->_mass_center.x,
                           ui->valTY->value() + tmesh->_mass_center.y,
                           ui->valTZ->value() + tmesh->_mass_center.z);
        tmesh->_trans.x = ui->valTX->value();
        tmesh->_trans.y = ui->valTY->value();
        tmesh->_trans.z = ui->valTZ->value();

        _valid_res = true;
    }
    return res_mesh;
}

void CMovRotScaDlg::onPreviewBtn()
{
    CTriangularMesh * res_mesh = make_transforms();
    emit send_result(res_mesh);
}

void CMovRotScaDlg::showEvent(QShowEvent * event)
{
    init_param();
    init_gui();
}

void CMovRotScaDlg::restore_transforms()
{
/*    CPointCloud * rpnts = _glWidget->get_raw_points_of_current_object();
    if(rpnts != NULL)
    {
        rpnts->_tx = _tx;
        rpnts->_ty = _ty;
        rpnts->_tz = _tz;

        rpnts->_rotx = _rotx;
        rpnts->_roty = _roty;
        rpnts->_rotz = _rotz;

        rpnts->_rx = _rx;
        rpnts->_ry = _ry;
        rpnts->_rz = _rz;
    }
*/
}

void CMovRotScaDlg::onValTXChanged(double tx)
{

    _valid_res = false;

/*    CPointCloud * rpnts = _glWidget->get_raw_points_of_current_object();
    if(rpnts != NULL)
    {
        rpnts->_tx = tx;
        _glWidget->udate_gl();
    }
*/
}

void CMovRotScaDlg::onValTYChanged(double ty)
{

    _valid_res = false;

/*    CPointCloud * rpnts = _glWidget->get_raw_points_of_current_object();
    if(rpnts != NULL)
    {
        rpnts->_ty = ty;
        _glWidget->udate_gl();
    }
*/
}

void CMovRotScaDlg::onValTZChanged(double tz)
{

    _valid_res = false;

/*    CPointCloud * rpnts = _glWidget->get_raw_points_of_current_object();
    if(rpnts != NULL)
    {
        rpnts->_tz = tz;
        _glWidget->udate_gl();
    }
*/
}

void CMovRotScaDlg::onValRXChanged(double rx)
{

    _valid_res = false;

/*    CPointCloud * rpnts = _glWidget->get_raw_points_of_current_object();
    if(rpnts != NULL)
    {
        rpnts->_rotx = rx;
        _glWidget->udate_gl();
    }
*/
}

void CMovRotScaDlg::onValRYChanged(double ry)
{

    _valid_res = false;

/*    CPointCloud * rpnts = _glWidget->get_raw_points_of_current_object();
    if(rpnts != NULL)
    {
        rpnts->_roty = ry;
        _glWidget->udate_gl();
    }
*/
}

void CMovRotScaDlg::onValRZChanged(double rz)
{

    _valid_res = false;

/*    CPointCloud * rpnts = _glWidget->get_raw_points_of_current_object();
    if(rpnts != NULL)
    {
        rpnts->_rotz = rz;
        _glWidget->udate_gl();
    }
*/
}

void CMovRotScaDlg::onValSXChanged(double sx)
{

    _valid_res = false;

/*    CPointCloud * rpnts = _glWidget->get_raw_points_of_current_object();
    if(rpnts != NULL)
    {
        rpnts->_rx = _rx * sx /100;
        _glWidget->udate_gl();
    }
*/
    if(ui->lockS->isChecked())
    {
        ui->valSY->setValue(sx);
        ui->valSZ->setValue(sx);
    }

}

void CMovRotScaDlg::onValSYChanged(double sy)
{

    _valid_res = false;
/*
 * CPointCloud * rpnts = _glWidget->get_raw_points_of_current_object();
    if(rpnts != NULL)
    {
        rpnts->_ry = _ry * sy /100;
        _glWidget->udate_gl();
    }
*/
}

void CMovRotScaDlg::onValSZChanged(double sz)
{
    _valid_res = false;
/*    CPointCloud * rpnts = _glWidget->get_raw_points_of_current_object();
    if(rpnts != NULL)
    {
        rpnts->_rz = _rz * sz /100;
        _glWidget->udate_gl();
    }
    */
}
