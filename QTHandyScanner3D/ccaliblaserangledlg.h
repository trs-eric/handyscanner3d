#ifndef CCALIBLASERANGLEDLG_H
#define CCALIBLASERANGLEDLG_H

#include <QDialog>


#include "opencv_aux_utils.h"
#include "./driver/driver_aux_util.h"

namespace Ui {
class CCalibLaserAngleDlg;
}

class CCalibLaserAngleDlg : public QDialog
{
    Q_OBJECT
public slots:
    void start_calib();
    void close_dlg();
    void capture_new_frame();
    void update_main_wnd();

public:
    explicit CCalibLaserAngleDlg(cv::VideoCapture * cam, PVOID dh, QWidget *parent);
    ~CCalibLaserAngleDlg();

protected:
    void showEvent(QShowEvent * event);
    void hideEvent(QHideEvent * event);


signals:
    void update_main_preview(cv::Mat & frame);
    void stop_main_preview();
    void start_main_preview();

private:
    Ui::CCalibLaserAngleDlg *ui;


    cv::VideoCapture * _current_cam;
    PVOID _dev_handle;
    void single_timer_shoot();

    cv::Mat _bckg;
    std::vector<cv::Point3f> _boardPoints;

    std::vector<float> _laser_init_cands;

    int _good_frames;
    int _max_good_frames;
    int _total_tries;
    float _last_angle ;

    void check_stop_condition();
    float convert_langle(int v);

    void  calculate_laser_init_angle();

    QTimer * _timer;
};

#endif // CCALIBLASERANGLEDLG_H
