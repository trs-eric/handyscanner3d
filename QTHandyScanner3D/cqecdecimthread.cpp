#include "cqecdecimthread.h"

namespace vcg {
namespace tri {

typedef	SimpleTempData<CMeshO::VertContainer, math::Quadric<double> > QuadricTemp;


class QHelper
        {
        public:
      QHelper(){}
      static void Init(){}
      static math::Quadric<double> &Qd(CVertexO &v) {return TD()[v];}
      static math::Quadric<double> &Qd(CVertexO *v) {return TD()[*v];}
      static CVertexO::ScalarType W(CVertexO * /*v*/) {return 1.0;}
      static CVertexO::ScalarType W(CVertexO & /*v*/) {return 1.0;}
      static void Merge(CVertexO & /*v_dest*/, CVertexO const & /*v_del*/){}
      static QuadricTemp* &TDp() {static QuadricTemp *td; return td;}
      static QuadricTemp &TD() {return *TDp();}
        };

typedef BasicVertexPair<CVertexO> VertexPair;

class MyTriEdgeCollapse: public vcg::tri::TriEdgeCollapseQuadric< CMeshO, VertexPair , MyTriEdgeCollapse, QHelper > {
                        public:
            typedef  vcg::tri::TriEdgeCollapseQuadric< CMeshO, VertexPair,  MyTriEdgeCollapse, QHelper> TECQ;
            inline MyTriEdgeCollapse(  const VertexPair &p, int i, BaseParameterClass *pp) :TECQ(p,i,pp){}
};
} // end namespace tri
} // end namepsace vcg


using namespace vcg;


CQECDecimThread::CQECDecimThread(QObject *parent) :
    QThread(parent), _inmesh(NULL)
{

    _tar_num    = 0;
    _perc       = 0;
    _qual       = 0;
    _pbound     = false;
    _boundw     = 0;
    _pnorm      = false;
    _ptopo      = false;
    _optimpos   = false;
    _psimp      = false;
    _weisimp    = false;
    _postsimp   = false;

  _abort = false;
}

CQECDecimThread::~CQECDecimThread()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
    wait();
}


void CQECDecimThread::start_process(CTriangularMesh * mesh, int tar_num,
int perc, float qual, bool pbound, float boundw, bool pnorm,
bool ptopo, bool optimpos, bool psimp, bool weisimp, bool postsimp)
{
    _abort = false;

    _inmesh = mesh;
    _tar_num = tar_num;
    _perc = perc;
    _qual = qual;
    _pbound = pbound;
    _boundw = boundw;
    _pnorm = pnorm;
    _ptopo = ptopo;
    _optimpos = optimpos;
    _psimp = psimp;
    _weisimp = weisimp;
    _postsimp = postsimp;

   /// myLapThread = this;
    start();

}

/*
bool lapSmoothMeshlabCB(const int pos, const char * str )
{
    myLapThread->emit_send_back(pos);
}*/


void CQECDecimThread::stop_process()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
}

void CQECDecimThread::emit_send_back(int i)
{
     emit send_back(i);
}

void CQECDecimThread::QuadricSimplification(CMeshO &m,
                           int  TargetFaceNum,
                           tri::TriEdgeCollapseQuadricParameter &pp)
{
    math::Quadric<double> QZero;
    QZero.SetZero();
    tri::QuadricTemp TD(m.vert,QZero);
    tri::QHelper::TDp()=&TD;

    if(pp.PreserveBoundary && !false)
    {
        pp.FastPreserveBoundary=true;
        pp.PreserveBoundary = false;
    }

    if(pp.NormalCheck) pp.NormalThrRad = M_PI/4.0;

    LocalOptimization<CMeshO> DeciSession(m, &pp);
    DeciSession.Init<tri::MyTriEdgeCollapse >();
    DeciSession.SetTargetSimplices(TargetFaceNum);
    DeciSession.SetTimeBudget(0.1f); // this allow to update the progress bar 10 time for sec...

    int faceToDel=m.fn-TargetFaceNum;
    while( DeciSession.DoOptimization() && m.fn>TargetFaceNum )
    {
        emit send_back((100-100*(m.fn-TargetFaceNum)/(faceToDel)));
        if (_abort)
        {
            emit send_back(0);
            return;
        }
        //// std::cout  << (100-100*(m.fn-TargetFaceNum)/(faceToDel)) << " " <<  "Simplifying..." << std::endl;
    };
    DeciSession.Finalize<tri::MyTriEdgeCollapse >();
}

void CQECDecimThread::run()
{
    /////////////////////////////////////////////////////
    ///// convert input CTrianleMesh to vcgMesh /////////
    /////////////////////////////////////////////////////

    CMeshO  cm;
    CTriMesh * tri_mesh = _inmesh->get_tri_mesh();

    int n_vert = tri_mesh->_points.size();
    CMeshO::VertexIterator v_iter =  tri::Allocator<CMeshO>::AddVertices(cm, n_vert);
    for (unsigned int i = 0; i < n_vert; i++, v_iter++)
    {
        (*v_iter).P()[0] = tri_mesh->_points[i]._x;
        (*v_iter).P()[1] = tri_mesh->_points[i]._y;
        (*v_iter).P()[2] = tri_mesh->_points[i]._z;

        (*v_iter).N()[0] = tri_mesh->_points[i]._nx;
        (*v_iter).N()[1] = tri_mesh->_points[i]._ny;
        (*v_iter).N()[2] = tri_mesh->_points[i]._nz;

    }

    if (_abort)
    {
        emit send_back(0);
        return;
    }

    unsigned int n_face = tri_mesh->_triangles.size();
    tri::Allocator<CMeshO>::AddFaces(cm, n_face);
    for (unsigned int f=0; f < n_face; f++)
    {
        cm.face[f].V(0) = &(cm.vert[tri_mesh->_triangles[f].pnt_index[0]]);
        cm.face[f].V(1) = &(cm.vert[tri_mesh->_triangles[f].pnt_index[1]]);
        cm.face[f].V(2) = &(cm.vert[tri_mesh->_triangles[f].pnt_index[2]]);
    }

    if (_abort)
    {
        emit send_back(0);
        return;
    }

    std::cout << "conversion TrianleMesh to vcgMesh is finished " << std::endl;

    //////////////////////////////////////////////////////////////////////////////
    /* original code
     * m.updateDataMask( MeshModel::MM_VERTFACETOPO | MeshModel::MM_VERTMARK);
            tri::UpdateFlags<CMeshO>::FaceBorderFromVF(m.cm);

            int TargetFaceNum = par.getInt("TargetFaceNum");
            if(par.getFloat("TargetPerc")!=0) TargetFaceNum = m.cm.fn*par.getFloat("TargetPerc");

            tri::TriEdgeCollapseQuadricParameter pp;
            pp.QualityThr=lastq_QualityThr =par.getFloat("QualityThr");
            pp.PreserveBoundary=lastq_PreserveBoundary = par.getBool("PreserveBoundary");
            pp.BoundaryWeight = pp.BoundaryWeight * par.getFloat("BoundaryWeight");
            pp.PreserveTopology=lastq_PreserveTopology = par.getBool("PreserveTopology");
            pp.QualityWeight=lastq_QualityWeight = par.getBool("QualityWeight");
            pp.NormalCheck=lastq_PreserveNormal = par.getBool("PreserveNormal");
            pp.OptimalPlacement=lastq_OptimalPlacement = par.getBool("OptimalPlacement");
            pp.QualityQuadric=lastq_PlanarQuadric = par.getBool("PlanarQuadric");
            lastq_Selected = par.getBool("Selected");

            QuadricSimplification(m.cm,TargetFaceNum,lastq_Selected,pp,  cb);

            if(par.getBool("AutoClean"))
            {
                int nullFaces=tri::Clean<CMeshO>::RemoveFaceOutOfRangeArea(m.cm,0);
                if(nullFaces) Log( "PostSimplification Cleaning: Removed %d null faces", nullFaces);
                int deldupvert=tri::Clean<CMeshO>::RemoveDuplicateVertex(m.cm);
                if(deldupvert) Log( "PostSimplification Cleaning: Removed %d duplicated vertices", deldupvert);
                int delvert=tri::Clean<CMeshO>::RemoveUnreferencedVertex(m.cm);
                if(delvert) Log( "PostSimplification Cleaning: Removed %d unreferenced vertices",delvert);
                m.clearDataMask(MeshModel::MM_FACEFACETOPO );
                tri::Allocator<CMeshO>::CompactVertexVector(m.cm);
                tri::Allocator<CMeshO>::CompactFaceVector(m.cm);
            }

            m.UpdateBoxAndNormals();
       */
            std::cout << "1" << std::endl;

              cm.vert.EnableVFAdjacency();
              cm.face.EnableVFAdjacency();
              tri::UpdateTopology<CMeshO>::VertexFace(cm);
              cm.vert.EnableMark();

              tri::UpdateFlags<CMeshO>::FaceBorderFromVF(cm);


              std::cout << "2" << std::endl;

       int TargetFaceNum = float(cm.fn) * float(_perc)/100;
        tri::TriEdgeCollapseQuadricParameter pp;
        pp.QualityThr           = _qual;
        pp.PreserveBoundary     = _pbound;
        pp.BoundaryWeight       = pp.BoundaryWeight * _boundw;
        pp.PreserveTopology     = _ptopo;
        pp.QualityWeight        = _weisimp;
        pp.NormalCheck          = _pnorm;
        pp.OptimalPlacement     = _optimpos;
        pp.QualityQuadric       = _psimp;


        std::cout << "3"<< std::endl;
        QuadricSimplification(cm, TargetFaceNum, pp);
        if (_abort)
        {
            emit send_back(0);
            return;
        }

        if(_postsimp)
        {
                int nullFaces=tri::Clean<CMeshO>::RemoveFaceOutOfRangeArea(cm,0);
                int deldupvert=tri::Clean<CMeshO>::RemoveDuplicateVertex(cm);
                int delvert=tri::Clean<CMeshO>::RemoveUnreferencedVertex(cm);
                // m.clearDataMask(MeshModel::MM_FACEFACETOPO ); eq to
                cm.face.DisableFFAdjacency();
                tri::Allocator<CMeshO>::CompactVertexVector(cm);
                tri::Allocator<CMeshO>::CompactFaceVector(cm);
        }
        /// m.UpdateBoxAndNormals();


    ///////////////////////////////////////////////
    //// convert vcgMesh to CTriangularMesh
    ///////////////////////////////////////////////

    CTriMesh * rmesh = new CTriMesh ;

    //// insert new points
    int n_overt = cm.vert.size();
    std::vector<C3DPoint> pnts;
    for (unsigned int i = 0; i < n_overt; i++)
    {
        C3DPoint xpp;
        xpp._x  = cm.vert[i].P()[0];
        xpp._y  = cm.vert[i].P()[1];
        xpp._z  = cm.vert[i].P()[2];
        pnts.push_back(xpp);
    }

    if (_abort)
    {
        emit send_back(0);
        return;
    }

    //// insert new triangles
    unsigned int n_oface = cm.face.size();
    rmesh->_triangles.resize(n_oface);
    for (unsigned int f = 0; f < n_oface; f++)
    {
        C3DPointIdx tri;
        tri.pnt_index[0] = cm.face[f].V(0)-(&*(cm.vert.begin()));
        tri.pnt_index[1] = cm.face[f].V(1)-(&*(cm.vert.begin()));
        tri.pnt_index[2] = cm.face[f].V(2)-(&*(cm.vert.begin()));
        rmesh->_triangles[f] = tri;
   }

    if (_abort)
    {
        emit send_back(0);
        return;
    }

    std::vector< std::vector <int> *> v(n_overt, NULL);
    ///////// update mesh normals per face
    std::vector<C3DPointIdx>::iterator itt = rmesh->_triangles.begin();
    for(int fidx = 0 ; itt != rmesh->_triangles.end(); itt++, fidx++)
    {
        if(v[itt->pnt_index[0]] == NULL)
        {
              v[ itt->pnt_index[0] ] = new std::vector <int>;
              v[ itt->pnt_index[0] ]-> push_back(fidx);
        }
        else
        {
              v[ itt->pnt_index[0] ]->push_back(fidx);
        }
        C3DPoint pnt0 = pnts[ itt->pnt_index[0] ];

        if(v[itt->pnt_index[1]] == NULL)
        {
              v[ itt->pnt_index[1] ] = new std::vector <int>;
              v[ itt->pnt_index[1] ]-> push_back(fidx);
        }
        else
              v[ itt->pnt_index[1] ]->push_back(fidx);
        C3DPoint pnt1 = pnts[ itt->pnt_index[1] ];

        if(v[itt->pnt_index[2]] == NULL)
        {
              v[ itt->pnt_index[2] ] = new std::vector <int>;
              v[ itt->pnt_index[2] ]-> push_back(fidx);
        }
        else
              v[ itt->pnt_index[2] ]->push_back(fidx);
        C3DPoint pnt2 = pnts[ itt->pnt_index[2] ];
        itt->tri_normal = compute_normal( pnt2, pnt1, pnt0);
    }

    if (_abort)
    {
        emit send_back(0);
        return;
    }


    //////// update mesh normals per verteces
    int pnts_count  = pnts.size();
    rmesh->_points.resize(pnts_count);
    std::vector<C3DPoint>::iterator itv = pnts.begin();
    for(int p = 0; itv != pnts.end(); itv++, p++)
    {
        C3DPoint xpp;
        xpp._x  = itv->_x;
        xpp._y  = itv->_y;
        xpp._z  = itv->_z;


        float fnx =0;
        float fny =0;
        float fnz =0;

        if(v[p] != NULL)
        {
          std::vector<int>::iterator fit = v[p]->begin();
          for( ; fit != v[p]->end(); fit++)
          {

              fnx += rmesh->_triangles[(*fit)].tri_normal._x;
              fny += rmesh->_triangles[(*fit)].tri_normal._y;
              fnz += rmesh->_triangles[(*fit)].tri_normal._z;

              xpp._faceidx.push_back(*fit);
          }
          normalize3(fnx, fny, fnz);
        }

        xpp._nx = fnx;
        xpp._ny = fny;
        xpp._nz = fnz;

        rmesh->_points[p]= xpp;
    }
    std::vector< std::vector <int> *>::iterator cit = v.begin();
    for( ; cit != v.end(); cit++)
        if((*cit) != NULL) delete *cit;


    ////////////// send result back to user //////////////////
    CTriangularMesh * outmesh =  new CTriangularMesh;
    outmesh -> set_mesh(rmesh);
    emit send_result(outmesh);
    emit send_back(100);

}

