#ifndef CDPCLCONSTRUCTIONDLG_H
#define CDPCLCONSTRUCTIONDLG_H

#include <QDialog>
#include "graphicutils.h"

class CGLWidget;
class CTriangularMesh;
class CDPCLConstructionThread ;

namespace Ui {
class CDPCLConstructionDlg;
}

class CDPCLConstructionDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CDPCLConstructionDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CDPCLConstructionDlg();
protected:
    void showEvent(QShowEvent * event);

private:
    Ui::CDPCLConstructionDlg *ui;

    CGLWidget *_glWidget;

    bool _in_process;
    bool _on_apply;
    bool _is_result_valid;
    CDPCLConstructionThread * _thread;

private slots:
    void onCancelBtn();
    void onPreviewBtn();
    void onApplyBtn();

    void onMakeProgressStep(int val);
    void onCompleteProcessing(std::vector<C3DPoint>*);
    void onStopProcessing();

    void close_dialog();

};

#endif // CDPCLCONSTRUCTIONDLG_H
