#ifndef C3DCORRECTOR_H
#define C3DCORRECTOR_H

#include "opencv_aux_utils.h"
#include "carray2d.h"

struct board_corners
{
    //// corner coordinates
    float x00;
    float y00;
   /// float z00;

    float x01;
    float y01;
   ///  float z01;

    float x11;
    float y11;
   /// float z11;

    float x10;
    float y10;
   /// float z10;


    /// plane parameters
    /// Ax + By + Cz + D = 0
    float pA;
    float pB;
    float pC;
    float pD;

} ;

class C3DCorrector
{

public:

    C3DCorrector();
    void set_grid_sz(int xs, int ys);


    void set_up_corners(std::vector<cv::Point2f>);
    void set_up_correction_level(CArray2D<cv::Point3f> v, int k = 0);
   // void set_up_lblmap(CArray2D<int>  );
   // void set_up_board_plain(float A, float B, float C, float D);

    //// deprovated version
   //void do_correction(float & x, float & y, float & z, float u, float v);


    void compute_parametric(float x, float y, float & px, float & py, int layer);
    void do_correction2d(float & x, float & y, float & z);
    void do_correction2d_aux(float & x, float & y, float & z, float & deform_x, float & deform_y, float & deform_z, int k);
    void do_correction3d(float & x, float & y, float & z);

    void set_ext_corners( board_corners );

    void save_calib_data(QString );
    void load_calib_data(QString );

protected:
    std::vector< CArray2D<cv::Point3f> >  _deform_map;

   ///CArray2D<int > _lbl_map;

    /// single deformation layer (used for 2d version only)
   /// CArray2D<cv::Point3f>  _deflay;


    std::vector<cv::Point2f> _corners;

    std::vector<board_corners> _ext_corners;

    int _xs;
    int _ys;

   /* float _pA;
    float _pB;
    float _pC;
    float _pD;*/
};

#endif // C3DCORRECTOR_H
