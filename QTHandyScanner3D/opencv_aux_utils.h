#ifndef OPENCV_AUX_UTILS_H
#define OPENCV_AUX_UTILS_H

#include <vector>

#include <QString>

#include <opencv2/features2d/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/contrib/contrib.hpp>


std::vector<cv::Point2f> findGridMarkers(QString & imgname);

float compute_board_distance(std::vector<cv::Point2f>  & corners,  float cellw,
                                int xs,  int ys, float fovx, float fovy);


cv::SimpleBlobDetector::Params contruct_blob_detector_pset();


// plain  equeation Ax + By + Cz + D = 0
void compute_plain_SVD_RANSAC(std::vector<cv::Point3f>  & points,
                              float & A, float & B, float & C, float &D,
                             int max_terations, float tolerance);

struct CFrameDesc
{
    enum CFrameType {BKG = 0, TEX, FR };

    CFrameType _typ;
    float _angle;
    float _lpos;

    CFrameDesc() :  _typ(CFrameDesc::FR), _angle(0), _lpos(0){}
    CFrameDesc(CFrameType t, float a, float l) : _typ(t), _angle(a), _lpos(l){}


    CFrameDesc(const CFrameDesc & c)
    {
        _typ   = c._typ;
        _angle = c._angle;
        _lpos  = c._lpos;
    }

    void operator =(const CFrameDesc & c)
    {
        _typ   = c._typ;
        _angle = c._angle;
        _lpos  = c._lpos;

    }
};

bool parse_description_video_file(QString & name, std::map<int, CFrameDesc> & frame_map);


#endif // OPENCV_AUX_UTILS_H
