#ifndef CCROPDISKTHREAD_H
#define CCROPDISKTHREAD_H

#include <QThread>
#include <QMutex>

class CPointCloud;

class CCropDiskThread : public QThread
{

    Q_OBJECT
public:
    explicit CCropDiskThread(QObject *parent = 0);
    ~CCropDiskThread();

    void start_process(CPointCloud *, bool, float,  bool, float, bool, float, bool, bool);
signals:
    void send_back(const int val);
    void send_result(CPointCloud *);

public slots:
    void stop_process();

protected:
    void run();

private:
    bool    _abort;
    QMutex  _mutex;


    bool _crop_top;
    float _crop_top_val;
    bool _crop_bot;
    float _crop_bot_val;
    bool _crop_cyl;
    float _crop_cyl_val;

    bool _fill_top;
    bool _fill_bottom;
    CPointCloud * _in_cloud;
};

#endif // CCROPDISKTHREAD_H
