#include "cfusemeshdlg.h"
#include "ui_cfusemeshdlg.h"
#include "cfusemeshthread.h"
#include "cglwidget.h"
#include "settings/psettings.h"
#include <QMessageBox>
#include "messages.h"

CFuseMeshDlg::CFuseMeshDlg(CGLWidget * glWidget, QWidget *parent) :
    QDialog(parent), _glWidget(glWidget),
    ui(new Ui::CFuseMeshDlg)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);

    ui->edtDecimPntStep->setText(QString::number(4));
    ui->edtSideLen->setText(QString::number(2));


    /// connect(ui->previewBtn, SIGNAL(clicked()), this, SLOT(onPreviewBtn()));
    connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));

    _thread = new CFuseMeshThread;


    // user pressed cancel button
    connect(ui->cancelBtn,  SIGNAL(clicked()), _thread, SLOT(stop_process()));
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

    // user closed dialg
    connect(this,  SIGNAL(finished(int)), _thread, SLOT(stop_process()));


     connect(_thread, SIGNAL(send_back(int)), this, SLOT(onMakeProgressStep(int)));
     connect(_thread, SIGNAL(send_result_fuse(CTriangularMesh*)), this, SLOT(onCompleteProcessing(CTriangularMesh*)));
     connect(_thread, SIGNAL(finished()), this, SLOT(onStopProcessing()));
     connect(_thread, SIGNAL(send_result_fuse(CTriangularMesh*)), _glWidget, SLOT(on_new_fused_mesh_computed(CTriangularMesh*)));

}

CFuseMeshDlg::~CFuseMeshDlg()
{
    delete ui;
    delete _thread;
}


void CFuseMeshDlg::onMakeProgressStep(int val)
{
    ui->progressBar->setValue(val);
}

void CFuseMeshDlg::onCompleteProcessing(CTriangularMesh * m)
{
    if(m == NULL)
    {
        QMessageBox::warning(this, tr("Warning"),wrn_FailedMesh);
        return;
    }
    if(_on_apply)
    {
        close_dialog();
        return;
    }
    _is_result_valid = true;
    ui->progressBar->setValue(100);

}

void CFuseMeshDlg::onStopProcessing()
{
    ui->applyBtn    ->setEnabled(true);
    ///ui->previewBtn  ->setEnabled(true);
    ui->edtDecimPntStep->setEnabled(true);
    ui->edtSideLen->setEnabled(true);

    _in_process = false;
}


void CFuseMeshDlg::close_dialog()
{
    _on_apply = false;
    close();
}

void CFuseMeshDlg::onCancelBtn()
{
    if(!_in_process)
        close_dialog();
    _in_process = false;
    _on_apply = false;
}

void CFuseMeshDlg::onPreviewBtn()
{
    if(g_polar_map == NULL)
    {
       QMessageBox::warning(this, tr("Warning"),wrn_NoPMAP);
       return;
    }

    ui->applyBtn    ->setEnabled(false);
    ////ui->previewBtn  ->setEnabled(false);
    ui->edtDecimPntStep->setEnabled(false);
    ui->edtSideLen->setEnabled(false);

    _in_process = true;
    _thread->start_process( ui->edtDecimPntStep->text().toInt(),
                            ui->edtSideLen->text().toFloat());


}


void CFuseMeshDlg::onApplyBtn()
{
    _on_apply = true;
    if(!_is_result_valid)
        onPreviewBtn();
    else
        close_dialog();
}

void CFuseMeshDlg::showEvent(QShowEvent * event)
{
     ui->progressBar->setValue(0);
    _is_result_valid    = false;
    _on_apply           = false;
    _in_process         = false;
}
