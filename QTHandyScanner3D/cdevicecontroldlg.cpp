#include "cdevicecontroldlg.h"
#include "ui_cdevicecontroldlg.h"
#include <QMessageBox>
// image viewer example
// http://qt-project.org/doc/qt-4.8/widgets-imageviewer.html
// pixel manipulation
// http://qt-project.org/doc/qt-4.8/qimage.html
extern "C"
{
#include "driver_aux_utils.h"
}

CDeviceControlDlg::CDeviceControlDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CDeviceControlDlg)
{
    ui->setupUi(this);

    connect(ui->tstMotorBtn, SIGNAL(clicked()), this, SLOT(test_motor()));
    connect(ui->tstSnapBtn,  SIGNAL(clicked()), this, SLOT(test_snapshot()));
}

CDeviceControlDlg::~CDeviceControlDlg()
{
    delete ui;
}

void CDeviceControlDlg::test_motor()
{
  int i;
  u16 data;
  int result;
  FT_STATUS ftStatus;

  ftStatus = connectToModule();
  if (ftStatus != FT_OK)
  {
    disconnectFromModule();
    QMessageBox::information(this, tr("Information"), tr("failure"));
    return;
  }

  pwmSetPrescaler(10);
  pwmSetBrightness(PWM_LED1_BRIGHTNESS, 20);
  pwmSetBrightness(PWM_LED2_BRIGHTNESS, 5);
  pwmSetBrightness(PWM_LASER1_BRIGHTNESS, 0);
  pwmSetBrightness(PWM_LASER2_BRIGHTNESS, 50);

  initCameraRegs();
  // testSnapshot();

  stepMotorTest();

  disconnectFromModule();

  QMessageBox::information(this, tr("Information"), tr("success"));

}

void CDeviceControlDlg::test_snapshot()
{
    int i;
    u16 data;
    int result;
    FT_STATUS ftStatus;

    ftStatus = connectToModule();
    if (ftStatus != FT_OK)
    {
      disconnectFromModule();
      QMessageBox::information(this, tr("Information"), tr("failure"));
      return;
    }

    pwmSetPrescaler(10);
    pwmSetBrightness(PWM_LED1_BRIGHTNESS, 20);
    pwmSetBrightness(PWM_LED2_BRIGHTNESS, 5);
    pwmSetBrightness(PWM_LASER1_BRIGHTNESS, 0);
    pwmSetBrightness(PWM_LASER2_BRIGHTNESS, 50);

    initCameraRegs();
    testSnapshot();

    disconnectFromModule();

    QMessageBox::information(this, tr("Information"), tr("success"));
}
