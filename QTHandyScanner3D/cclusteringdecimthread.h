#ifndef CCLUSTERINGDECIMTHREAD_H
#define CCLUSTERINGDECIMTHREAD_H

#include <QThread>
#include <QMutex>

class CPointCloud;
class CNormalCloud;
class CTriangularMesh;
class CTriMesh;

class CClusteringDecimThread : public QThread
{
    Q_OBJECT
public:
    explicit CClusteringDecimThread(QObject *parent = 0);
    ~CClusteringDecimThread();

    void start_process(CTriangularMesh *, float);
    bool is_abort(){return _abort;}
    void emit_send_back(int i);

signals:
    void send_back(const int val);
    void send_result(CNormalCloud *);
    void send_result(CTriangularMesh *);

protected:
    void run();

public slots:
    void stop_process();

private:
    bool    _abort;
    QMutex  _mutex;

    CTriangularMesh * _inmesh;
    float _csize;

    void undo_transform(CTriMesh * rmesh);
    void redo_transform(CTriMesh * rmesh);

};

#endif // CCLUSTERINGDECIMTHREAD_H
