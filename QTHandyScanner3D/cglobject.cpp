#include "cglobject.h"

const GLuint nonGLlist = 0;

CGLObject::CGLObject()
{
    _tx = 0.0f;
    _ty = 0.0f;
    _tz = 0.0f;

    _rotx = 0.0f;
    _roty = 0.0f;
    _rotz = 0.0f;

    _irotx = 0.0f;
    _iroty = 0.0f;
    _irotz = 0.0f;

    _xs = 0.0f;
    _ys = 0.0f;
    _zs = 0.0f;

    _rx = 1.0f;
    _ry = 1.0f;
    _rz = 1.0f;

    _color[0] = 0.0f;
    _color[1] = 0.0f;
    _color[2] = 0.0f;

    _oglid = nonGLlist;

    isVisible = true;
}

CGLObject::CGLObject(
        GLfloat		rotx,	GLfloat		roty,	GLfloat		rotz,
        GLfloat		tx, 	GLfloat		ty,		GLfloat		tz,
        GLfloat		xs,		GLfloat		ys, 	GLfloat		zs,
        GLfloat		rx,		GLfloat		ry,		GLfloat		rz,
        GLfloat		red,	GLfloat		green,	GLfloat		blue) :
    _tx (tx), _ty (ty), _tz (tz), _rotx (rotx), _roty (roty),
    _rotz(rotz), _xs (xs), _ys (ys), _zs (zs), _rx (rx), _ry (ry), _rz(rz), isVisible(true)
{
    _color[0] = red;
    _color[1] = green;
    _color[2] = blue;

    _irotx = 0.0f;
    _iroty = 0.0f;
    _irotz = 0.0f;

    _oglid = nonGLlist ;
}


CGLObject::~CGLObject()
{
    delete_glLists();
}

void CGLObject::delete_glLists()
{
    if(_oglid == nonGLlist) return;

    glDeleteLists(_oglid,1);
    _oglid = nonGLlist;
}

void CGLObject::remake_glList()
{
    delete_glLists();
}

void CGLObject::glDraw()
{


    if(_oglid == nonGLlist)
        create_glList();

    if(!isVisible) return;

     if(_oglid == nonGLlist)
         return;

    glPushMatrix();
        glTranslatef(_tx, _ty, _tz);
        glRotatef(_roty, 0, 1, 0);
        glRotatef(_rotz, 0, 0, 1);
        glRotatef(_rotx, 1, 0, 0);

        glRotatef(_iroty, 0, 1, 0);
        glRotatef(_irotz, 0, 0, 1);
        glRotatef(_irotx, 1, 0, 0);
        glScalef(_rx, _ry, _rz);
        glCallList(_oglid);
    glPopMatrix();
}
