#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QtMultimedia/QSound>

QT_BEGIN_NAMESPACE
class QAction;
class QActionGroup;
class QMenu;
class QToolBar;
QT_END_NAMESPACE

class CGLWidget;
class CDisparityCompDlg;
class CDisparityCompClassicDlg;
class CComputeSparsePCLDlg;
class CMCubesMeshDlg;
class CCalibStereoDlg;
class CMeshConstructionDlg;
class CFuseMeshDlg;
class CTexturingDlg;
//class CLaplacianSmDlg;
//class CQECDecimDlg;
class CDeciSmoothDlg;
class CGLSettingsDlg;
class CDPCLConstructionDlg;
class CLaserSanOfflineDlg;
class CLaserScanOnlineDlg;
class CPCLColoringDlg;
class CProjectIO;
class IOWaitDlg;
class CSelectPCLHelpDlg;
class CGLAlignmentCloudDlg;
class CMeshArtifRemoveDlg;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void closeEvent(QCloseEvent *event);
    void keyPressEvent(QKeyEvent *event);


private slots:
    void new_scene();
    void about();
    void tobeimplented();
    void scanning();
    void device_calibration();
    void transform_mesh();
    void set_rend_mode_rawp();
    void set_rend_mode_wire();
    void set_rend_mode_flat();
    void set_rend_mode_smoo();
    void set_rend_mode_tex();
    void set_view_camera();
    void set_view_shad_points();
    void change_gl_settings();
    void change_view_scene_mode();
    void meshRemoveDlg();

    void delaunay_mesh_test();
    void mesh_decimation();
    void optimizePntsDlg();
    void normalsEstDlg();
    void meshConstDlg();
    void fusemeshDlg();
    void cropDiskDlg();
    void pclColorDlg();
    void import_scene();
    void export_scene();

    void load_project();
    void save_project();
    void save_as_project();
    void import_photogrammetry();
    void disparityCompDlg();
    void disparityCompClassicDlg();
    void computeSparcePCLDlg();
    void computeDensePCLDlg();
    void computeMCubeMeshDlg();
    void computeTexture();
    void computeLaplacianSmooth();
    void computeQECDecimation();
    void computeDeciSmooth();

    void calibrationStereoDlg();
    void text_clicked();

    void field_btn_sound();

    void alignCloudDlg();

    void laserScanOfflineDlg();
    void laserScanOnlineDlg();
    void dockLocationChanged(Qt::DockWidgetArea area);

    void proj_save_finished();
    void proj_load_finished(bool);
    void export_finished();

public:
    void myinitFieldButtons();
    void updateFieldButton();
    void setStatusBarInfo(QString );

private:
    void createActions();
    void createMenus();
    void myinitMenuItems();
    void createFieldButtons();

    void createToolBars();
    void createStatusBar();
    void createAuxDialogs();
    void initButtonView(QString , QPushButton * btn, int icosz);


    // menu
    QMenu *fileMenu;
    QMenu *workMenu;
    QMenu *viewMenu;
    QMenu *helpMenu;

    // File menu actions
    QAction *newAct;
    QAction *openAct;
    QAction *saveAct;
    QAction *saveAsAct;
    QAction *exportAct;
    QAction *exitAct;



    /*QAction *pptAct;

    QAction *optimAct;
    QAction *pnormAct;
    QAction *meshActS;
    QAction *meshActD;
    QAction *meshOpt;
    QAction *meshLSmooth;



    QAction *movAct;
    QAction *rotAct;
    QAction *cropAct;
    QAction *erasAct;
    QAction *aligAct;
*/

    QAction * meshAct;
    QAction * fuseMeshAct;

    QAction * calibAct;
    QAction * dispAct;
    QAction * dispClasAct;
    QAction * aboutAct;

    QAction * alignAct;
    QAction * laserScanOffAct;
    QAction * laserScanOnlAct;
    QAction * scanOffAct;

    // QAction * mcubemeshAct;
    // QAction * lapSmAct;
    // QAction * qecDecAct;
    QAction * decismAct;
    QAction * texAct;
    QAction * dpclConstAct;

    QActionGroup * visGroup;
    QAction * viewrpAct;
    QAction * viewwrAct;
    QPushButton * btn_vw_wr;
    QAction * viewflAct;
    QPushButton * btn_vw_fl;
    QAction * viewsmAct;
    QPushButton * btn_vw_sm;
    QAction * viewtxAct;
    QPushButton * btn_vw_tx;

    QAction * viewshadpAct;
    QAction * viewcamAct;

    QAction * viewGLSet;
    QAction * viewRemAct;

    QAction * pclColAct;

    QAction * viewSelMode;

    QString _field_btn_style;

    // toolbars
    //QToolBar * mainToolBar;
    QToolBar * toolsToolBar;
    QLabel   * sbSceneMessageLbl;

    // OpenGL scene widget
    QWidget *centralWidget;
    CGLWidget *glWidget;


    // CPointOptimizeDlg * _poptDlg;
    // CNormalEstimationDlg *_normeDlg;
    // CConvexMeshConstructionDlg * _cvxMeshCDlg;
    // CClusteringDecimDlg * _clDesimDlg;
    // CScanReconstructionDlg * _scanDlg;
    // CMovRotScaDlg * _mrsDlg;
    // CCropDiskDlg * _cropDlg;

    CDisparityCompDlg * _dispDlg;
   // CDisparityCompClassicDlg * _dispClasDlg;
    CComputeSparsePCLDlg * _compSPCLDlg;
    CDPCLConstructionDlg * _compDPCLDlg;
    //CMCubesMeshDlg * _compMCubeMeshDlg;
    CCalibStereoDlg * _calibDlg;
    CMeshConstructionDlg * _meshCDlg;
    CFuseMeshDlg * _fusemeshDlg;
    CTexturingDlg * _texDlg;
    //CLaplacianSmDlg * _lapSmDlg;
    //CQECDecimDlg * _qecDecDlg;
    CDeciSmoothDlg * _decsmDlg;
    CGLSettingsDlg * _glSetDlg;
    CLaserSanOfflineDlg * _lasScanOffDlg;
    CLaserScanOnlineDlg * _lasScanOnlDlg;
    CPCLColoringDlg * _pclColorDlg;
    CSelectPCLHelpDlg * _selPCLHelpDlg;
    CGLAlignmentCloudDlg * _alignCldDlg;
    CMeshArtifRemoveDlg * _martifRemDlg;

    bool _is_actul_scene;
    QString _appName;
    QString _current_project;
    QString _title;


    QThread * _proj_io_thread;
    CProjectIO * _proj_io;
    IOWaitDlg * _iowaitDlg;



private:
    Ui::MainWindow *ui;
    QSound * _splay;
protected:
    void resizeEvent(QResizeEvent * event);

};

#endif // MAINWINDOW_H
