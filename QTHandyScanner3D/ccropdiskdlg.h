#ifndef CCROPDISKDLG_H
#define CCROPDISKDLG_H

#include <QDialog>

class CGLWidget;
class CCropDiskThread;
class CPointCloud;

namespace Ui {
class CCropDiskDlg;
}

class CCropDiskDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CCropDiskDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CCropDiskDlg();

protected:
    void showEvent(QShowEvent * event);

private:
    Ui::CCropDiskDlg *ui;
    CGLWidget *_glWidget;
    CPointCloud * _orig_cloud;

    bool _in_process;
    bool _on_apply;
    bool _is_result_valid;
    CCropDiskThread * _thread;

private slots:
    void onCancelBtn();
    void onPreviewBtn();
    void onApplyBtn();

    void onMakeProgressStep(int val);
    void onCompleteProcessing(CPointCloud * );
    void onStopProcessing();

    void close_dialog();

};

#endif // CCROPDISKDLG_H
