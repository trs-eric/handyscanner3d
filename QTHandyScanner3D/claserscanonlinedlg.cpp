#include "claserscanonlinedlg.h"
#include "ui_claserscanonlinedlg.h"

#include "cglwidget.h"

#include <QMediaService>
#include <QMediaRecorder>
#include <QCameraViewfinder>
#include <QFileDialog>
#include <QMediaMetaData>
#include <QMessageBox>
#include <QGridLayout>
#include <ctime>

#include "messages.h"
#include "settings/psettings.h"
#include "img2space/edgedetection.h"
#include "ccapturesequencedlg.h"

#include <QThread>
#include "cusbcamerasettings.h"
#include <QCamera>
#include "opencv_aux_utils.h"
#include "ccalibcameramatrixdlg.h"

#include "runetag/runetag.h"
#include "ccaliblaserangledlg.h"

CLaserScanOnlineDlg::CLaserScanOnlineDlg(CGLWidget * glWidget, QWidget *parent) :
   _glWidget(glWidget),  QDialog(parent),
    ui(new Ui::CLaserScanOnlineDlg),
    _scale_factor(0.75), _current_camera_idx(0), _current_cam(NULL), _timer_period(50), _cam_setdlg(NULL),
    _board_distance (0),_roll (0), _pitch(0),  _dev_handle (NULL), _pat_detector(NULL), _valid_tt_marks(false),
    _update_tt_marks(false), _valid_tt_dots(false), _laser_calib_dlg(NULL)
{
    ui->setupUi(this);

    setWindowFlags(Qt::WindowMaximizeButtonHint | Qt::WindowMinimizeButtonHint | Qt::WindowCloseButtonHint);

    _tex_label = new QLabel;
    _tex_label->setBackgroundRole(QPalette::Base);
    _tex_label->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    _tex_label->setScaledContents(true);


    ui->scrollArea->setBackgroundRole(QPalette::Dark);
    ui->scrollArea->setWidget(_tex_label);
    ui->scrollArea->setWidgetResizable(false);

    connect(ui->btnHundr, SIGNAL(clicked()), this, SLOT(set_zoom_100()));
    connect(ui->btnHalf,  SIGNAL(clicked()), this, SLOT(set_zoom_50()));
    connect(ui->btnQaut, SIGNAL(clicked()), this, SLOT(set_zoom_25()));

    for(int cam_idx = 0; cam_idx < 3 ; cam_idx++ )
    {
        cv::VideoCapture camera(cam_idx);

        if (!camera.isOpened())
            break;
        else
        {
            QString cam_name = QString("Web camera %1").arg(QString::number(cam_idx+1));
            ui->cmbCamList->addItem(cam_name);
            _web_camera_list.push_back(cam_idx);
            _current_camera_idx = cam_idx;
        }
    }

    connect(ui->cmbCamList, SIGNAL(currentIndexChanged(int)), this, SLOT(set_camera(int)));
    connect(ui->btnCapture, SIGNAL(clicked()), this, SLOT(grab_image()));

    _timer = new QTimer(this);
    connect(_timer, SIGNAL(timeout()), this, SLOT(update_form_image()));

    ui->slidOpas->setValue(100);
    connect(ui->slidOpas, SIGNAL(sliderReleased()), this, SLOT(change_opacity()));

    ui->sldColorThres->setValue(CGlobalSettings::_lcthresh);
    ui->lblColorThres->setText(QString::number(CGlobalSettings::_lcthresh));
    connect(ui->sldColorThres,   SIGNAL(valueChanged(int)), this, SLOT(edge_thresh_changed(int )));

    ui->sldCorrWndSz->setValue(CGlobalSettings::_laser_corrwnd_sz);
    ui->lblCorrWndsSz->setText(QString::number(CGlobalSettings::_laser_corrwnd_sz));
    connect(ui->sldCorrWndSz,   SIGNAL(valueChanged(int)), this, SLOT(corwndsz_changed(int)));

    connect(ui->sldExLED,   SIGNAL(valueChanged(int)), this, SLOT(eLED_changed(int)));

    connect(ui->chkEdge,   SIGNAL(clicked()), this, SLOT(change_find_edges()));
    connect(ui->btnCaptureSeq, SIGNAL(clicked()), this, SLOT(grab_sequence()));


    ui->cmbTurnTbl->addItem("45",45);
    ui->cmbTurnTbl->addItem("60",60);
    ui->cmbTurnTbl->addItem("90",90);
    ui->cmbTurnTbl->setCurrentIndex(0);

   connect(ui->btnCameraSet, SIGNAL(clicked()), this, SLOT(cam_settings_dlg()));

   ui->cmbCamList->setEnabled(false);

   //////// RUNE tag
   _pat_detector = NULL;

   unsigned short min_ellipse_contour_points=10;
   unsigned short max_ellipse_contour_points=10000;
   float min_ellipse_area=100.0f;
   float max_ellipse_area=10000.0f;
   float min_roundness=0.3f;
   float max_mse = 0.3f;
   float size_compensation=-1.5f;

    _ellipseDetector = new cv::runetag::EllipseDetector( min_ellipse_contour_points,
          max_ellipse_contour_points,
          min_ellipse_area,
          max_ellipse_area,
          min_roundness,
          max_mse,
          size_compensation);

    connect(ui->btnDetTurnMarks, SIGNAL(clicked()), this, SLOT(update_tbl_marks()));
    connect(ui->btnCamCalib, SIGNAL(clicked()), this, SLOT(cam_callibration()));

    connect(ui->btnLaserCalibr, SIGNAL(clicked()), this, SLOT(laser_callibration()));

}

void CLaserScanOnlineDlg::laser_callibration()
{
 /*    i2c_laser_OFF();

    CCalibLaserAngleDlg dlg(_current_cam,  _dev_handle, this);

    connect(&dlg, SIGNAL(update_main_preview(cv::Mat & )), this, SLOT(on_update_main_preview(cv::Mat & )));

    emit on_stop_preview();
    dlg.exec();

    disconnect(&dlg, SIGNAL(update_main_preview(cv::Mat & )), 0, 0);
    emit on_start_preview();*/

    if(_laser_calib_dlg == NULL)
    {
        _laser_calib_dlg = new CCalibLaserAngleDlg(_current_cam,  _dev_handle, this);
        connect(_laser_calib_dlg, SIGNAL(update_main_preview(cv::Mat & )), this, SLOT(on_update_main_preview(cv::Mat & )));
        connect(_laser_calib_dlg, SIGNAL(stop_main_preview()), this, SLOT(on_stop_preview()));
        connect(_laser_calib_dlg, SIGNAL(start_main_preview()), this, SLOT(on_start_preview()));
    }

    _in_calibration_mode = true;
    _laser_calib_dlg->show();


}

void CLaserScanOnlineDlg::cam_callibration()
{
    _in_calibration_mode = true;
    CCalibCameraMatrixDlg dlg(_current_cam,  _dev_handle, this);


    connect(&dlg, SIGNAL(stop_main_preview()), this, SLOT(on_stop_preview()));
    connect(&dlg, SIGNAL(start_main_preview()), this, SLOT(on_start_preview()));
    connect(&dlg, SIGNAL(update_main_preview(cv::Mat & )), this, SLOT(on_update_main_preview(cv::Mat & )));

    dlg.exec();

    disconnect(&dlg, SIGNAL(stop_main_preview()), 0, 0);
    disconnect(&dlg, SIGNAL(start_main_preview()), 0, 0);
    disconnect(&dlg, SIGNAL(update_main_preview(cv::Mat & )), 0, 0);

    _in_calibration_mode = false;
}

void CLaserScanOnlineDlg::update_tbl_marks()
{
    _update_tt_marks = true;
}

void CLaserScanOnlineDlg::init_device()
{

    ////// device initialisation
    _dev_handle = NULL;

    wchar_t **devList = NULL;
    UINT nCount = 0;
    HRESULT hr = driv::GetDeviceList(&devList, &nCount);

    if(devList != NULL)
    {
        bool found_dev = false;
        for (int i = 0; i < nCount; i++)
        {
            driv::OpenDevice(devList[i], &_dev_handle);

            BYTE data_buf[8] = { 0 };

            // fill in register address of ChipID
            data_buf[0] = 0x30;
            data_buf[1] = 0x0A;
            driv::I2C_Write(_dev_handle, 0x78, 2, data_buf);
            driv::I2C_Read(_dev_handle, 0x78, 2, data_buf);


            if((data_buf[0] == 0x56) && (data_buf[1] == 0x40) )
            {
                found_dev = true;
                break;
            }
        }
        driv::FreeDeviceList(devList);

        if(!found_dev)
        {
            std::cout << "Wrong I2C device (not OV 56 40)" << std::endl;
            _dev_handle = NULL;
            return;
        }

        ui->btnTestLaserOn->setEnabled(true);
        ui->btnTestLaserOFF->setEnabled(true);
        connect(ui->btnTestLaserOn, SIGNAL(clicked()), this, SLOT(i2c_laser_ON()));
        connect(ui->btnTestLaserOFF, SIGNAL(clicked()), this, SLOT(i2c_laser_OFF()));

        ui->btnMotoLeft->setEnabled(true);
        ui->btnMotoRight->setEnabled(true);

        connect(ui->btnMotoRight, SIGNAL(pressed()), this, SLOT(i2c_motor_right_pressed()));
        connect(ui->btnMotoRight, SIGNAL(released()), this, SLOT(i2c_motor_stop()));

        connect(ui->btnMotoLeft, SIGNAL(pressed()), this, SLOT(i2c_motor_left_pressed()));
        connect(ui->btnMotoLeft, SIGNAL(released()), this, SLOT(i2c_motor_stop()));


        connect(ui->btnTurnTbl, SIGNAL(clicked()), this, SLOT(i2c_turn_table()));

       driv::set_M0(_dev_handle, 1);
       driv::set_M1(_dev_handle, 2);

        ui->sldExLED->setEnabled(true);
    }
}

void CLaserScanOnlineDlg::free_device()
{
    if(_dev_handle != NULL)
    {
        driv::CloseDevice(_dev_handle);
        _dev_handle = NULL;
    }
}

void CLaserScanOnlineDlg::i2c_motor_left_pressed()
{
    driv::motor_dir_change(_dev_handle,2); // to the left
    driv::motor_rotate_signal(_dev_handle, 1); // start
}


void CLaserScanOnlineDlg::i2c_motor_stop()
{
    driv::motor_rotate_signal(_dev_handle, 2); // stop
}

void CLaserScanOnlineDlg::i2c_motor_right_pressed()
{
    driv::motor_dir_change(_dev_handle,1); // to the left
    driv::motor_rotate_signal(_dev_handle, 1); // start
}

void CLaserScanOnlineDlg::update_acc()
{
    if(_dev_handle != NULL)
    {

        driv::read_accelerometer_roll_pitch(_dev_handle,  _roll,  _pitch);
        if(!g_use_acc_delta)
        {
            ui->edtRoll->setText(QString::number(floor(_roll)));
            ui->edtPitch->setText(QString::number(floor(_pitch)));
        }
        else
        {
           float nroll = floor(_roll + g_acc_delta_roll);
           float npitch = floor(_pitch + g_acc_delta_pitch);

           ui->edtRoll->setText(QString("%1*").arg(QString::number(nroll)));
           ui->edtPitch->setText(QString("%1*").arg(QString::number(npitch)));
        }


        float andat = driv::get_laser_angle(_dev_handle);
        QString adata = QString::number(andat);//+  QString(" (grad)");
        ui->edtLaserAngle->setText(adata);
    }
}

void CLaserScanOnlineDlg::cam_settings_dlg()
{

    ui->chkEdge->setChecked(false);
    if(_cam_setdlg != NULL) delete _cam_setdlg;
    _cam_setdlg = new CUSBCameraSettings(_current_cam,  this);
    _cam_setdlg->show();
}

void CLaserScanOnlineDlg::i2c_laser_ON()
{
    if(_dev_handle == NULL) return;
    driv::laser_turn_on(_dev_handle);
}


void CLaserScanOnlineDlg::i2c_laser_OFF()
{
    if(_dev_handle == NULL) return;
    driv::laser_turn_off(_dev_handle);
}


void CLaserScanOnlineDlg::i2c_turn_table()
{
    _valid_tt_dots = false;

    if(_dev_handle == NULL) return;

    int angle = ui->cmbTurnTbl->currentData().toInt();
    driv::turn_table_request(_dev_handle, angle);

}

bool CLaserScanOnlineDlg::check_callibration()
{
    /// DELTO this line for release return true;
   if(  g_callibration_map.settings_for_focus(CGlobalSettings::_webc_focus))
       return true;
   else
       return false;
}


void CLaserScanOnlineDlg::grab_sequence()
{
    bool callibrated =   check_callibration(); // DELTO
    if(!callibrated )
    {
        QString msg = "Cannot proceed. Perform callibration for current camera focus first.";
        QMessageBox::information(this, tr("Information"), msg);
        return;
    }

    float depth_to_cancel  = 75;
    if(!_valid_tt_marks)
    {
        cv::Mat frame;
        bool bSuccess = _current_cam->read(frame);
        do_find_runetag_in_frame(frame, true);

        if(!_valid_tt_marks)
        {
            QString msg = "Markers was not detected. Use GYRO information?";
            QMessageBox::StandardButton reply = QMessageBox::question(this, "Warning", msg,
                                           QMessageBox::Yes|QMessageBox::No);
            if (reply == QMessageBox::No) return;
            _rt_x_incline = -BGM(_pitch);
            _rt_y_incline = -BGM(_roll);
            _rt_rotCX = -10;
            _rt_rotCY = 250;
            _rt_rotCZ = 120;
            depth_to_cancel = 750;
        }
    }


    _valid_tt_dots = false;


    CCaptureSequenceDlg dlg (_current_cam,
                             IBGM(_rt_y_incline),-IBGM(_rt_x_incline),
                             _rt_rotCX, _rt_rotCY, _rt_rotCZ, depth_to_cancel,  _dev_handle,
                             ui->sldExLED->value()*10,
                             this);

    connect(&dlg, SIGNAL(stop_main_preview()), this, SLOT(on_stop_preview()));
    connect(&dlg, SIGNAL(start_main_preview()), this, SLOT(on_start_preview()));
    connect(&dlg, SIGNAL(update_main_preview(cv::Mat & )), this, SLOT(on_update_main_preview(cv::Mat & )));
    connect(this, SIGNAL(update_child_lpos()), &dlg, SLOT(read_and_update_lblA()));

    dlg.exec();

    disconnect(&dlg, SIGNAL(stop_main_preview()), 0, 0);
    disconnect(&dlg, SIGNAL(start_main_preview()), 0, 0);
    disconnect(&dlg, SIGNAL(update_main_preview(cv::Mat & )), 0, 0);
    disconnect(this, SIGNAL(update_child_lpos()), 0, 0);




    _timer->start(_timer_period);
}

void CLaserScanOnlineDlg::on_stop_preview()
{
     _timer->stop();
}


void CLaserScanOnlineDlg::on_start_preview()
{
    _timer->start(_timer_period);
    _in_calibration_mode  = false;
}

void CLaserScanOnlineDlg::change_find_edges()
{
    _timer->stop();

    i2c_laser_OFF();
    _bckg = QImage();
    _corners.clear();

    _timer->start(500);
}

void CLaserScanOnlineDlg::eLED_changed(int v)
{
    ui->lblELEDval->setText(QString::number(v*10));

    if(_dev_handle == NULL) return;
    driv::set_ELED_brightness(_dev_handle, v*10);
}

void CLaserScanOnlineDlg::corwndsz_changed(int v)
{
    CGlobalSettings::_laser_corrwnd_sz = v;
    ui->lblCorrWndsSz->setText(QString::number( CGlobalSettings::_laser_corrwnd_sz ));
}

void CLaserScanOnlineDlg::edge_thresh_changed(int val)
{
    CGlobalSettings::_lcthresh = val;
    ui->lblColorThres->setText(QString::number(CGlobalSettings::_lcthresh));
}

void CLaserScanOnlineDlg::grab_image()
{
   QString fileName = QFileDialog::getSaveFileName(this,
             tr("Grab camera image..."), "", "Image (*.png)");

    if (fileName.isEmpty()) return;

    cv::Mat frame;
    bool bSuccess = _current_cam->read(frame);
    if (!bSuccess) return;

    QImage input_img((uchar*)frame.data, frame.cols, frame.rows,  QImage::Format_RGB888);
    input_img = input_img.rgbSwapped();
    input_img.save(fileName);
}

void CLaserScanOnlineDlg::set_zoom_100()
{
    _scale_factor = 1.0;
    update_form_image();
}

void CLaserScanOnlineDlg::set_zoom_50()
{
    _scale_factor = 0.5;
    update_form_image();
}
void CLaserScanOnlineDlg::set_zoom_25()
{
    _scale_factor = 0.75;
    update_form_image();
}


void CLaserScanOnlineDlg::draw_mark( QImage & texture, float cx, float cy)
{
    int cr_len = 10;

    for( int ix = -cr_len ; ix <= cr_len; ix++)
    {
        QRgb p = qRgb(0,255, 0 );
        texture.setPixel(ix+cx, cy, p);

        if(_scale_factor < 1.0)
        {
            texture.setPixel(ix+cx, cy-1, p);
            texture.setPixel(ix+cx, cy+1, p);
        }
    }

    for( int iy = -cr_len ; iy <= cr_len; iy++)
    {
        QRgb p = qRgb(0, 255, 0);
        texture.setPixel(cx, iy+cy, p);
        if(_scale_factor < 1.0)
        {
            texture.setPixel(cx-1, iy+cy, p);
            texture.setPixel(cx+1, iy+cy, p);
        }
    }
}

void CLaserScanOnlineDlg::on_update_main_preview(cv::Mat & frame)
{
    return update_from_matimage(frame);
}

void CLaserScanOnlineDlg::do_find_runetag_in_frame(cv::Mat & frame, bool salient)
{

    bool callibrated = check_callibration();
    if(!callibrated )
    {
        QString msg = "Cannot proceed. Perform callibration for current camera focus first.";
        QMessageBox::information(this, tr("Information"), msg);
        return;
    }

    cv::Mat distortion = cv::Mat::zeros(1,5,CV_32F);

    if(_pat_detector != NULL);
        delete _pat_detector;
    _pat_detector = new cv::runetag::MarkerDetector( g_cam_intrinsics );
    _pat_detector->addModelsFromFile(g_rune_pattern_fname);

    std::cout << g_cam_intrinsics << std::endl;
    std::cout << g_cam_distortion << std::endl;

    float err = 0;
    bool res = do_find_runetag( frame,
                                g_cam_intrinsics,
                                distortion,
                                _pat_detector,  _ellipseDetector, _RT, _tbl_markers,
                                & err);
    _rt_error = err;

    if(res)
    {

        compute_cam_angles_and_rotation_center(_RT, _rt_x_incline, _rt_y_incline,
                _rt_rotCX, _rt_rotCY, _rt_rotCZ);

        _valid_tt_marks = true;
        _valid_tt_dots = true;

        return;
    }


    cv::Mat cnt_frame = cv::Mat::zeros( frame.size(), frame.type() );
    float alpha = 2.0;
    for( int y = 0; y < frame.rows; y++ )
    {
        for( int x = 0; x < frame.cols; x++ )
        {
            for( int c = 0; c < 3; c++ )
            {
                cnt_frame.at<cv::Vec3b>(y,x)[c] = cv::saturate_cast<uchar>( alpha*( frame.at<cv::Vec3b>(y,x)[c] ) );
            }
        }
    }

    res = do_find_runetag( cnt_frame, g_cam_intrinsics,
                                distortion, _pat_detector,  _ellipseDetector, _RT, _tbl_markers,
                          & err);
    _rt_error = err;

    if(res)
    {

        compute_cam_angles_and_rotation_center(_RT, _rt_x_incline, _rt_y_incline,
                _rt_rotCX, _rt_rotCY, _rt_rotCZ);

        _valid_tt_marks = true;
        _valid_tt_dots = true;

        return;
    }
    else
    {
        _valid_tt_marks = false;
        _valid_tt_dots = false;
        if(!salient)
               QMessageBox::information(this, tr("Information"), "Marker not found");
    }
}

void CLaserScanOnlineDlg::update_from_matimage(cv::Mat & frame)
{

    //// undistort always
    cv::Mat input_image_undistorted;
    cv::undistort(frame, input_image_undistorted, g_cam_intrinsics, g_cam_distortion);
    frame = input_image_undistorted;

    if(_update_tt_marks)
    {
       do_find_runetag_in_frame(frame);
       _update_tt_marks = false;

       if(_valid_tt_marks)
       {
            g_use_acc_delta = true;
            g_acc_delta_pitch = -IBGM(_rt_x_incline) - _pitch;
            g_acc_delta_roll  = IBGM(_rt_y_incline) - _roll;
        }
    }

    if(_valid_tt_marks)
    {
        cv::Mat ddistortion = cv::Mat::zeros(1,5,CV_32F);
        draw_detected_runetag(frame, _tbl_markers, _RT, g_cam_intrinsics, ddistortion, _valid_tt_dots);


        int deg = IBGM(_rt_x_incline) * 100;
        ui->lblPitchDM->setText(QString::number(-deg/100.));
        if(fabs(deg/100.) < 25)
            ui->lblPitchDM->setStyleSheet("QLabel {  color : red; }");
        else
            ui->lblPitchDM->setStyleSheet("QLabel {  color: #b1b1b1; }");

        deg = IBGM(_rt_y_incline) * 100;
        ui->lblRollDM->setText(QString::number(deg/100.));
        if(fabs(deg/100.)>1)
            ui->lblRollDM->setStyleSheet("QLabel {  color : red; }");
        else
            ui->lblRollDM->setStyleSheet("QLabel {   color: #b1b1b1;}");


        int ival = _rt_rotCX * 100;
        ui->lblRCX->setText(QString::number(ival / 100.));

        ival =  _rt_rotCY * 100;
        ui->lblRCY->setText(QString::number(ival / 100.));

        ival =  _rt_rotCZ * 100;
        ui->lblRCZ->setText(QString::number(ival / 100.));

        ival = _rt_error * 1000;
        ui->lblDetectErr->setText(QString::number(ival / 1000.));
    }
    else if(! _in_calibration_mode)
    {

        float ew = 0.6 * frame.cols;
        float eh = 0.6 * ew;

        float x0 = frame.cols/2;
        float y0 = frame.rows - eh/2  + 25;


        cv::ellipse(frame,cv::RotatedRect(cv::Point2f(x0,y0),cv::Size(ew,eh),0),cv::Scalar(0,255,0));

        y0 -= 50;
        cv::line(frame,cv::Point2f(x0 - 20 ,y0 - 10), cv::Point2f(x0+20 ,y0 + 10), cv::Scalar(0,255,0));
        cv::line(frame,cv::Point2f(x0 - 20 ,y0 + 10), cv::Point2f(x0+20 ,y0 - 10), cv::Scalar(0,255,0));
    }

    QImage input_img((uchar*)frame.data, frame.cols, frame.rows,  QImage::Format_RGB888);
    input_img = input_img.rgbSwapped();

    QImage texture(input_img.width(), input_img.height(), QImage::Format_ARGB32);
    texture.fill(QColor(0,0,0,0));

    QString bkg_name ("bckg_tmp.jpg");
    if(ui->chkEdge->isChecked() )
    {
        if (input_img.width() != _bckg.width() ||
        input_img.height() != _bckg.height() ||
                _bckg.width() == 0 ||
                _bckg.height() == 0 )
        {
            _bckg = input_img.copy();
            std::cout << _bckg.width() << " "<< _bckg.height() << std::endl;
             _timer->start(_timer_period);

             _bckg.save(bkg_name);

            i2c_laser_ON();
            return;
        }
    }

    /////////////////
    if(ui->chkEdge->isChecked())
    {

         QImage bckg = _bckg;//.convertToFormat(QImage::Format_RGB32);

         QImage diff = input_img;//.convertToFormat(QImage::Format_RGB32);


         difference_image_fast_supp_red(&diff, &bckg, CGlobalSettings::_ignore_read_thresh);
         /// difference_image_fast(&diff, &bckg, false);

         std::vector<CIPixel> pix = get_edge_points_lines_hills_1D(&diff);

         if(ui->sldCorrWndSz->value() > 0)
             pix = do_laser_points_correction(pix);

         if(pix.size() > 2)
         {
             std::vector<CIPixel>::iterator it = pix.begin();
             for( ; it != pix.end(); it++)
             {
                 QRgb p = qRgba(0, 255, 0,255);
                 texture.setPixel(it->_u, it->_v, p);

                 if(_scale_factor < 1.0)
                 {
                     texture.setPixel(it->_u+1., it->_v, p);
                     texture.setPixel(it->_u-1., it->_v, p);
                 }
            }
         }
    }

    /////////////////
    QPixmap bakcg_map = QPixmap::fromImage(input_img);
    QPixmap forwd_map = QPixmap::fromImage(texture);


    QPixmap finalPixmap(bakcg_map.size());
    finalPixmap.fill(Qt::transparent);
    QPainter pd(&finalPixmap);
    pd.setBackgroundMode(Qt::TransparentMode);
    pd.setBackground(QBrush(Qt::transparent));
    pd.eraseRect(bakcg_map.rect());

    float opacity = float(ui->slidOpas->value())/100.0;
    pd.setOpacity(opacity);
    pd.drawPixmap(0, 0, bakcg_map);
    pd.setOpacity(1);
    pd.drawPixmap(0, 0, forwd_map);
    pd.end();

    _tex_label->setPixmap(finalPixmap/*QPixmap::fromImage(input_img)*/);
    _tex_label->resize(_scale_factor * _tex_label->pixmap()->size());
}

bool CLaserScanOnlineDlg::check_device_alive()
{
    wchar_t **devList = NULL;
    UINT nCount = 0;
    driv::GetDeviceList(&devList, &nCount);

    bool res =false;
    if(devList != NULL)
    {
        res = true;
        driv::FreeDeviceList(devList);
    }
    return res;
}

void CLaserScanOnlineDlg::update_form_image()
{
    _timer->stop();
    if(_current_cam == NULL) return;

    if((_dev_handle != NULL) )
    {
        if(!check_device_alive())
        {
            QMessageBox::warning(this, tr("Device"), wrn_DeviceCannotOpen);
            _dev_handle = NULL;
            close();
            return;
        }
    }

    cv::Mat frame;
    bool bSuccess =  _current_cam->read(frame); /// DELTO true;//
  // frame = cv::imread("table.jpg");

    /*
    g_cam_intrinsics = (cv::Mat_<double>(3,3) << 1.2713346891893113e+003, 0., 6.2587195652199807e+002,
                               0.,  1.2713346891893113e+003, 4.5938413487296373e+002,
                               0.,  0., 1. );

    g_cam_distortion =  (cv::Mat_<double>(1,5) << 1.1463561308700512e-001,
                                               -2.5236495887536681e-001, 0,0,
                                               0);

    g_cam_intrinsics = (cv::Mat_<double>(3,3) <<1.2166547637059464e+003, 0., 6.5360008214042273e+002,
                               0.,  1.2166547637059464e+003, 4.6998918872457807e+002,
                               0.,  0., 1. );

    g_cam_distortion =  (cv::Mat_<double>(1,5) << 1.1567279716461285e-001,
                                               -2.3631627458188340e-001, 0,0,
                                               0);

    g_cam_intrinsics = (cv::Mat_<double>(3,3) <<1.2334870099856778e+003, 0., 6.2959126420087966e+002,
                               0.,  1.2339146881385284e+003, 4.7117813759436910e+002,
                               0.,  0., 1. );

    g_cam_distortion =  (cv::Mat_<double>(1,5) << 1.0049666165347637e-001,
                                               -2.2379873930215580e-001, 0,0,
                                               0);*/


    if (!bSuccess)
    {
         _timer->start(_timer_period);
        return;
    }
    emit update_child_lpos();

    update_from_matimage(frame);

    // roll pitch
    update_acc();

    _timer->start(_timer_period);
}


void CLaserScanOnlineDlg::adjust_scroll_bar(QScrollBar *scrollBar, float factor)
{
    scrollBar->setValue(int(factor * scrollBar->value()
                            + ((factor - 1) * scrollBar->pageStep()/2)));
}



CLaserScanOnlineDlg::~CLaserScanOnlineDlg()
{
    delete ui;
    if(_pat_detector != NULL)
        delete _pat_detector;
    if(_ellipseDetector != NULL)
        delete _ellipseDetector;
    if(_laser_calib_dlg != NULL)
        delete _laser_calib_dlg;
}

void CLaserScanOnlineDlg::camera_on()
{
    camera_off();

    _current_cam = new cv::VideoCapture(_current_camera_idx);

      if (!_current_cam ->isOpened())
      {
            std::cout << "ATTENTION cannot open" << std::endl;
            delete _current_cam;
            _current_cam = NULL;
            return;
      }


}

void CLaserScanOnlineDlg::camera_off()
{
    _timer->stop();

    if(_current_cam != NULL)
    {
        _current_cam->release();
        delete _current_cam ;
        _current_cam = NULL;
    }
}


void CLaserScanOnlineDlg::hideEvent(QHideEvent * event)
{
    if(_dev_handle != NULL)
    {
         driv::set_RGBLed_X(_dev_handle, 0x21, 15, 0, 0);
         driv::set_RGBLed_X(_dev_handle, 0x22, 15, 0, 0);
         driv::set_RGBLed_X(_dev_handle, 0x23, 15, 0, 0);

         driv::laser_turn_off(_dev_handle);
    }

    ui->sldExLED->setValue(0);


   camera_off();

   if(_cam_setdlg != NULL) _cam_setdlg ->hide();
   free_device();

   _valid_tt_marks = false;
   _valid_tt_dots = false;
}




void CLaserScanOnlineDlg::showEvent(QShowEvent * event)
{   
    set_camera( _current_camera_idx );
    update_form_image();

    init_device();

    if(_dev_handle == NULL)
    {
        QMessageBox::warning(this, tr("Device"), wrn_DeviceCannotOpen);
        ui->btnTestLaserOn->setEnabled(false);
        ui->btnTestLaserOFF->setEnabled(false);

         ui->btnMotoLeft->setEnabled(false);
        ui->btnMotoRight->setEnabled(false);

        ui->cmbTurnTbl->setEnabled(false);
        ui->btnTurnTbl->setEnabled(false);
        ui->sldExLED->setEnabled(false);

    }

    if(_dev_handle != NULL)
    {
         driv::init_accelerometer(_dev_handle);
         driv::motor_stop(_dev_handle);
         driv::laser_turn_off(_dev_handle);

         driv::set_RGBLed_X(_dev_handle, 0x21, 0, 15, 0);
         driv::set_RGBLed_X(_dev_handle, 0x22, 0, 15, 0);
         driv::set_RGBLed_X(_dev_handle, 0x23, 0, 15, 0);

    }

    _in_calibration_mode = false;



}


void CLaserScanOnlineDlg::change_opacity()
{
}

void CLaserScanOnlineDlg::set_camera(int c)
{
    _timer->stop();
    ui->chkEdge->setChecked(false);

    std::cout << "setup camera " << c << std::endl;
    camera_off();
    _current_camera_idx = c;
    camera_on();

    update_form_image();
    _timer->start(_timer_period);

}
