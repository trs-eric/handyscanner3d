#include "ctriangularmesh.h"
#include <fstream>
#include <sstream>
#include "cimportmanager.h"
#include <QFileInfo>
#include <qopenglext.h>
#include <QGLWidget>
#include "settings/psettings.h"

#include "./libcoldet/coldet.h"
#include "cglcamera.h"

CTriangularMesh::CTriangularMesh() : _mesh(NULL), _rmode(MESH_SMOOTH), _use_glob_coloring(true)
{
    _color[0] = 0.8;
    _color[1] = 0.8;
    _color[2] = 0.8;

   // _rx = 0.05f;
   // _ry = 0.05f;
   // _rz = 0.05f;

    _wire_glid  = nonGLlist;
    _flat_glid  = nonGLlist;
    _smooth_glid = nonGLlist;
    _tex_glid    = nonGLlist;
    _tex_matid = nonGLlist;
    _oglid = nonGLlist;

}


void CTriangularMesh::clear_selection()
{
    std::vector<C3DPoint>::iterator it = _mesh->_points.begin();
    for( ; it != _mesh->_points.end(); it++)
            it->_selected = false;
    delete_glLists();
}

CollisionModel3D * CTriangularMesh::construct_collision_model()
{
    CollisionModel3D* collision_model = newCollisionModel3D();

    float aux_v[9];
    std::vector<C3DPointIdx>::iterator itt = _mesh->_triangles.begin();
    int t = 0;
    for( ; itt != _mesh->_triangles.end(); itt++, t++ )
    {

            C3DPoint pnt0 =  _mesh->_points[ itt->pnt_index[0] ];
            aux_v[0] = pnt0._x;
            aux_v[1] = pnt0._y;
            aux_v[2] = pnt0._z;


            C3DPoint pnt1 =  _mesh->_points[ itt->pnt_index[1] ];
            aux_v[3] = pnt1._x;
            aux_v[4] = pnt1._y;
            aux_v[5] = pnt1._z;

            C3DPoint pnt2 =  _mesh->_points[ itt->pnt_index[2] ];
            aux_v[6] = pnt2._x;
            aux_v[7] = pnt2._y;
            aux_v[8] = pnt2._z;

            collision_model ->addTriangle(&aux_v[0],&aux_v[3],&aux_v[6]);


    }
    collision_model->finalize();
    return collision_model;
}

void CTriangularMesh::process_selected_faces_old()
{
    // make clean version
    // add iteration number
    // interface blocking
    // aux coloring - boundary and intern at wire mode

    if(_mesh == NULL) return;

    std::vector<int> inner_v;
    std::vector<int> outer_v;

    std::set<int> _sel_faces;
    //// find internal and outer vertices
    std::vector<C3DPoint>::iterator it = _mesh->_points.begin();
    for(int vi = 0 ; it != _mesh->_points.end(); it++, vi++)
    {

        if(it->_selected)
        {
            bool is_intern = true;
            std::vector<int>::iterator itf = it-> _faceidx.begin();
            for(  ; itf != it-> _faceidx.end(); itf++)
            {
                bool is_face_intern = true;
                for (int p = 0; p < 3; p++)
                {
                    if(!_mesh->_points[ _mesh->_triangles[*itf].pnt_index[p]]._selected)
                        is_face_intern = false;
                }
                if(!is_face_intern)
                {
                    is_intern = false;
                    break;
                }

                _sel_faces.insert(*itf);
            }

            if(is_intern)
            {
                inner_v.push_back(vi);
                it->_r = 0;
                it->_g = 1;
                it->_b = 0;
            }
            else
            {
                outer_v.push_back(vi);
                it->_r = 0;
                it->_g = 0;
                it->_b = 1;

            }

        }
    }

    /////// make selection flat
    float N_count = outer_v.size();

    std::vector<int>::iterator iti = inner_v.begin();
    for( ; iti != inner_v.end(); iti++)
    {
        /*
        C3DPoint sum_pi;
        C3DPoint sum_e_pi;
        float e_total = 0;

        C3DPoint pi = _mesh->_points[*iti];

        std::vector<int>::iterator ito = outer_v.begin();
        for( ; ito != outer_v.end(); ito++)
        {

            C3DPoint po = _mesh->_points[*ito];
            sum_pi._x += po._x;
            sum_pi._y += po._y;
            sum_pi._z += po._z;

            float e_i = sqrt( (pi._x - po._x) * (pi._x - po._x) +
                              (pi._y - po._y) * (pi._y - po._y) +
                              (pi._z - po._z) * (pi._z - po._z) );

            e_total +=  e_i;

            sum_e_pi._x += e_i * po._x;
            sum_e_pi._y += e_i * po._y;
            sum_e_pi._z += e_i * po._z;

        }


        std::cout << _mesh->_points[*iti]._x << " " << _mesh->_points[*iti]._y << " " << _mesh->_points[*iti]._z << " || ";

        _mesh->_points[*iti]._x = (sum_pi._x - (sum_e_pi._x / e_total))/(N_count -1);
        _mesh->_points[*iti]._y = (sum_pi._y - (sum_e_pi._y / e_total))/(N_count -1);
        _mesh->_points[*iti]._z = (sum_pi._z - (sum_e_pi._z / e_total))/(N_count -1);

         std::cout << _mesh->_points[*iti]._x << " " << _mesh->_points[*iti]._y << " "  << _mesh->_points[*iti]._z << std::endl;
        */

        C3DPoint pi = _mesh->_points[*iti];

        std::vector<float> dists;
        float e_total = 0;

        std::vector<int>::iterator ito = outer_v.begin();
        for( ; ito != outer_v.end(); ito++)
        {

            C3DPoint po = _mesh->_points[*ito];
            float dist =  sqrt  ( (pi._x - po._x) * (pi._x - po._x) +
                               (pi._y - po._y) * (pi._y - po._y) +
                               (pi._z - po._z) * (pi._z - po._z) );

            dist =  fabs(log(dist)*log(dist)*log(dist));
            dists.push_back(dist);
            e_total += dist;
        }

        C3DPoint rp;
        float sum_weights = 0;
        std::vector<float>::iterator dit = dists.begin();
        for(int i =0 ; dit != dists.end(); dit++, i++)
        {
            C3DPoint pi = _mesh->_points[outer_v[i]];
            float wei = *dit;

            rp._x += wei * pi._x;
            rp._y += wei * pi._y;
            rp._z += wei * pi._z;
            rp._nx += wei * pi._nx;
            rp._ny += wei * pi._ny;
            rp._nz += wei * pi._nz;


            sum_weights += wei;

           // std::cout << wei << std::endl;
        }
     /*   _mesh->_points[*iti]._x = rp._x / sum_weights;
        _mesh->_points[*iti]._y = rp._y / sum_weights;
        _mesh->_points[*iti]._z = rp._z / sum_weights;*/
        _mesh->_points[*iti]._nx = rp._nx / sum_weights;
        _mesh->_points[*iti]._ny = rp._ny / sum_weights;
        _mesh->_points[*iti]._nz = rp._nz / sum_weights;
    }

    float gpho = 0;
    float gc = 0;

    std::vector<C3DPoint>::iterator itv = _mesh->_points.begin();
    for( ; itv != _mesh->_points.end(); itv++ )
    {
        std::vector<int>::iterator itf =  itv->_faceidx.begin();
        for(  ; itf != itv->_faceidx.end(); itf++)
        {

            C3DPoint pnt0 =  _mesh->_points[ _mesh->_triangles[*itf].pnt_index[0]];
            C3DPoint pnt1 =  _mesh->_points[ _mesh->_triangles[*itf].pnt_index[1]];
            C3DPoint pnt2 =  _mesh->_points[ _mesh->_triangles[*itf].pnt_index[2]];

            gpho +=   sqrt(  (pnt0._x - pnt1._x)*(pnt0._x - pnt1._x) +
                        (pnt0._y - pnt1._y)*(pnt0._y - pnt1._y) +
                        (pnt0._z - pnt1._z)*(pnt0._z - pnt1._z) );

            gpho +=   sqrt(  (pnt0._x - pnt2._x)*(pnt0._x - pnt2._x) +
                        (pnt0._y - pnt2._y)*(pnt0._y - pnt2._y) +
                        (pnt0._z - pnt2._z)*(pnt0._z - pnt2._z) );

            gpho +=   sqrt(  (pnt2._x - pnt1._x)*(pnt2._x - pnt1._x) +
                        (pnt2._y - pnt1._y)*(pnt2._y - pnt1._y) +
                        (pnt2._z - pnt1._z)*(pnt2._z - pnt1._z) );

            gc +=3;
        }
    }
    gpho /= gc;


    //////////////////////////////////////

for(int iter = 0; iter < 5; iter++)
 {
    std::map<int,C3DPoint> res_vert;
    std::vector<int>::iterator itv = inner_v.begin();
    for( ; itv != inner_v.end(); itv++)
    {

        //// compute fancy normal
         C3DPoint v_normal;

        std::vector<int>::iterator itf =  _mesh->_points[*itv]._faceidx.begin();
        for(  ; itf != _mesh->_points[*itv]._faceidx.end(); itf++)
        {

            C3DPoint pnt0 =  _mesh->_points[ _mesh->_triangles[*itf].pnt_index[0]];
            C3DPoint pnt1 =  _mesh->_points[ _mesh->_triangles[*itf].pnt_index[1]];
            C3DPoint pnt2 =  _mesh->_points[ _mesh->_triangles[*itf].pnt_index[2]];
            C3DPoint tri_normal = compute_normal( pnt2, pnt1, pnt0);
            float area = compute_area(pnt2, pnt1, pnt0);

            v_normal._nx += 1 * tri_normal._x;
            v_normal._ny += 1 * tri_normal._y;
            v_normal._nz += 1 * tri_normal._z;
        }
        float v_normal_len = sqrt(v_normal._nx * v_normal._nx +
                                  v_normal._ny * v_normal._ny +
                                  v_normal._nz * v_normal._nz);

        v_normal._nx /= v_normal_len;
        v_normal._ny /= v_normal_len;
        v_normal._nz /= v_normal_len;

        std::set<int> _q_i;
        itf =  _mesh->_points[*itv]._faceidx.begin();
        for(  ; itf != _mesh->_points[*itv]._faceidx.end(); itf++)
        {
            for (int p = 0; p < 3; p++)
            {
                int n_v_idx = _mesh->_triangles[*itf].pnt_index[p];
                if(  (*itv) !=  n_v_idx)
                    _q_i.insert(n_v_idx);
            }
        }


        float sum = 0;
        float norm = 0;
        float sigma_c = 0.1 * gpho;
        float sigma_s = 0.5 * gpho;

        float r_x = 0;
        float r_y = 0;
        float r_z = 0;

        float n_x = 0;
        float n_y = 0;
        float n_z = 0;

        float nei_sz = _q_i.size();
        C3DPoint v = _mesh->_points[*itv];


        std::set<int>::iterator itn = _q_i.begin();
        for(; itn != _q_i.end(); itn++)
        {
            C3DPoint qi = _mesh->_points[*itn];

            float t = sqrt((qi._x - v._x)*(qi._x - v._x) +
                           (qi._y - v._y)*(qi._y - v._y) +
                           (qi._z - v._z)*(qi._z - v._z) );

            /*
            float ll = sqrt(( v._x - qi._x)*( v._x - qi._x) +
                            ( v._y - qi._y)*( v._y - qi._y) +
                            ( v._z - qi._z)*( v._z - qi._z));

            float h =  v_normal._nx  * ( v._x - qi._x)  +
                       v_normal._ny  * ( v._y - qi._y)  +
                       v_normal._nz  * ( v._z - qi._z)  ;

            float wc = exp(- t * t / (2* sigma_c * sigma_c));
            float ws = exp(- h * h / (2 * sigma_s * sigma_s));

            sum  += (wc * ws)*h;
            norm += (wc * ws);*/

            float w_i =  1 / t;

            r_x += w_i * qi._x;
            r_y += w_i * qi._y;
            r_z += w_i * qi._z;



            n_x += w_i * qi._nx;
            n_y += w_i * qi._ny;
            n_z += w_i * qi._nz;

            sum += w_i;


        }

         C3DPoint rv (v);

         rv._x =  r_x / sum ;//  + v_normal._nx *  sum /norm;
         rv._y =  r_y / sum;//  + v_normal._ny *  sum /norm;
         rv._z =  r_z / sum;//  + v_normal._nz *  sum /norm;
         rv._nx = n_x / sum;
         rv._ny = n_y / sum;
         rv._nz = n_z / sum;

         res_vert[*itv] = rv;

         std::cout << gpho << " | " << v._x << " " << v._y << " " << v._z << " | " << rv._x << " " << rv._y << " " << rv._z  << " | " << v_normal._nx << " " << v_normal._ny << " " << v_normal._nz << " | "<< sum  << std::endl;

    }

    std::map<int,C3DPoint>::iterator itm = res_vert.begin();
    for( ; itm != res_vert.end(); itm++ )
         _mesh->_points[itm->first] = itm->second;
}

    //////////////////////////////////////

    /// TODO recalculate face normals
    std::set<int>::iterator ittf = _sel_faces.begin();
    for( ; ittf != _sel_faces.end(); ittf++ )
    {
        C3DPoint pnt0 =  _mesh->_points[ _mesh->_triangles[*ittf].pnt_index[0]];
        C3DPoint pnt1 =  _mesh->_points[ _mesh->_triangles[*ittf].pnt_index[1]];
        C3DPoint pnt2 =  _mesh->_points[ _mesh->_triangles[*ittf].pnt_index[2]];
         _mesh->_triangles[*ittf].tri_normal = compute_normal( pnt2, pnt1, pnt0);
    }

    delete_glLists();
}


void CTriangularMesh::process_selected_faces()
{
    // aux coloring - boundary and intern at wire mode
    if(_mesh == NULL) return;

    float gpho = 0;
    float gc = 0;

    std::vector<C3DPoint>::iterator itv = _mesh->_points.begin();
    for( ; itv != _mesh->_points.end(); itv++ )
    {
        std::vector<int>::iterator itf =  itv->_faceidx.begin();
        for(  ; itf != itv->_faceidx.end(); itf++)
        {

            C3DPoint pnt0 =  _mesh->_points[ _mesh->_triangles[*itf].pnt_index[0]];
            C3DPoint pnt1 =  _mesh->_points[ _mesh->_triangles[*itf].pnt_index[1]];
            C3DPoint pnt2 =  _mesh->_points[ _mesh->_triangles[*itf].pnt_index[2]];

            gpho +=   sqrt(  (pnt0._x - pnt1._x)*(pnt0._x - pnt1._x) +
                        (pnt0._y - pnt1._y)*(pnt0._y - pnt1._y) +
                        (pnt0._z - pnt1._z)*(pnt0._z - pnt1._z) );

            gpho +=   sqrt(  (pnt0._x - pnt2._x)*(pnt0._x - pnt2._x) +
                        (pnt0._y - pnt2._y)*(pnt0._y - pnt2._y) +
                        (pnt0._z - pnt2._z)*(pnt0._z - pnt2._z) );

            gpho +=   sqrt(  (pnt2._x - pnt1._x)*(pnt2._x - pnt1._x) +
                        (pnt2._y - pnt1._y)*(pnt2._y - pnt1._y) +
                        (pnt2._z - pnt1._z)*(pnt2._z - pnt1._z) );

            gc +=3;
        }
    }
    gpho /= gc;
    ///////////////////////////////////////////
    std::cout << "ro=" << gpho <<std::endl;

    std::vector<int> inner_v;
    std::vector<int> outer_v;

    std::set<int> _sel_faces;
    //// find internal and outer vertices
    std::vector<C3DPoint>::iterator it = _mesh->_points.begin();
    for(int vi = 0 ; it != _mesh->_points.end(); it++, vi++)
    {

        if(it->_selected)
        {
            bool is_intern = true;
            std::vector<int>::iterator itf = it-> _faceidx.begin();
            for(  ; itf != it-> _faceidx.end(); itf++)
            {
                bool is_face_intern = true;
                for (int p = 0; p < 3; p++)
                {
                    if(!_mesh->_points[ _mesh->_triangles[*itf].pnt_index[p]]._selected)
                        is_face_intern = false;
                }
                if(!is_face_intern)
                {
                    is_intern = false;
                    break;
                }
                _sel_faces.insert(*itf);
            }

            if(is_intern)
            {
                inner_v.push_back(vi);
               // it->_r = 0;
               // it->_g = 1;
               // it->_b = 0;
            }
            else
            {
                outer_v.push_back(vi);
               // it->_r = 0;
               // it->_g = 0;
               // it->_b = 1;

            }
        }
    }

    /////// make selection flat
    float N_count = outer_v.size();

    float max_dist = 0;
    float min_dist = 100000;

    std::vector<float> w_dists;
    std::map<int, float> map_w_dists;

    std::vector<C3DPoint> inner_p_bckp;

    std::vector<int>::iterator iti = inner_v.begin();
    for( ; iti != inner_v.end(); iti++)
    {
        C3DPoint pi = _mesh->_points[*iti];

        std::vector<float> dists;

        float d_min = 10000;
        std::vector<int>::iterator ito = outer_v.begin();
        for( ; ito != outer_v.end(); ito++)
        {

            C3DPoint po = _mesh->_points[*ito];
            float dist =     ( (pi._x - po._x) * (pi._x - po._x) +
                               (pi._y - po._y) * (pi._y - po._y) +
                               (pi._z - po._z) * (pi._z - po._z) );

            if(sqrt(dist) < d_min ) d_min =  sqrt(dist);

            dist = 1/ dist;// fabs(log(dist)*log(dist)*log(dist));
            dists.push_back(dist);

        }


        d_min = pow(d_min, 0.75);
        d_min +=  gpho/6;

        if(d_min > max_dist) max_dist = d_min;
        if(d_min < min_dist) min_dist = d_min;
        w_dists.push_back(d_min);

        map_w_dists[*iti] = d_min;

        C3DPoint rp;
        float sum_weights = 0;
        std::vector<float>::iterator dit = dists.begin();
        for(int i =0 ; dit != dists.end(); dit++, i++)
        {
            C3DPoint pi = _mesh->_points[outer_v[i]];
            float wei = *dit;

            rp._x += wei * pi._x;
            rp._y += wei * pi._y;
            rp._z += wei * pi._z;
            rp._nx += wei * pi._nx;
            rp._ny += wei * pi._ny;
            rp._nz += wei * pi._nz;


            sum_weights += wei;

        }
        _mesh->_points[*iti]._x = rp._x / sum_weights;
        _mesh->_points[*iti]._y = rp._y / sum_weights;
        _mesh->_points[*iti]._z = rp._z / sum_weights;
        _mesh->_points[*iti]._nx = rp._nx / sum_weights;
        _mesh->_points[*iti]._ny = rp._ny / sum_weights;
        _mesh->_points[*iti]._nz = rp._nz / sum_weights;


        inner_p_bckp.push_back(_mesh->_points[*iti]);
    }

    /////////// smooth weights
    /* std::vector<float> sm_w_dists;
     float max_val_w = 0;
     float min_val_w = 100000;

     iti = inner_v.begin();
    for( ; iti != inner_v.end(); iti++)
    {
        std::set<int> _w_i;
        std::vector<int>::iterator itf =  _mesh->_points[*iti]._faceidx.begin();
        for(  ; itf != _mesh->_points[*iti]._faceidx.end(); itf++)
        {
            for (int p = 0; p < 3; p++)
            {
                int n_v_idx = _mesh->_triangles[*itf].pnt_index[p];
                if(  (*iti) !=  n_v_idx)
                {
                    std::map<int, float>::iterator itm  = map_w_dists.find(n_v_idx);
                    if(itm != map_w_dists.end())
                        _w_i.insert(n_v_idx);
                }
            }
        }

        float sum = 0;
        std::set<int>::iterator itn = _w_i.begin();
        for(; itn != _w_i.end(); itn++)
            sum += map_w_dists[*itn];

        float sm_weight = sum / float(_w_i.size());
         sm_w_dists.push_back(sm_weight);
         if(sm_weight >max_val_w ) max_val_w = sm_weight;
         if(sm_weight <min_val_w ) min_val_w = sm_weight;

    }

    {
        std::vector<float>::iterator its = sm_w_dists.begin();
        for(int c = 0; its != sm_w_dists.end(); its++, c++)
            w_dists[c] = (*its) ;

        max_dist = max_val_w;
        min_dist = min_val_w;

          its =  w_dists.begin();
        for(int c = 0; its !=  w_dists.end(); its++, c++)
        {

            w_dists[c] = (*its) ;


        }
    }*/

    //////////////////////

    float smooth_measure = 10e14;
    float dhei_best = -30;
    for(int dhei = -30; dhei <= 30; dhei++)
    {

        // setup height
        float height = float(dhei) * gpho/4;
        std::vector<int>::iterator iti = inner_v.begin();
        for(int c = 0 ; iti != inner_v.end(); iti++, c++)
        {
            C3DPoint pi = _mesh->_points[*iti];

            float n_p = height * ( w_dists[c]  )/(max_dist - min_dist);

            _mesh->_points[*iti]._x += n_p *  _mesh->_points[*iti]._nx ;
            _mesh->_points[*iti]._y += n_p *  _mesh->_points[*iti]._ny ;
            _mesh->_points[*iti]._z += n_p *  _mesh->_points[*iti]._nz ;
        }


        float cur_smooth_measure = 0;

        std::vector<int>::iterator ito =  outer_v.begin();
        for( ; ito != outer_v.end(); ito++ )
        {
            C3DPoint po = _mesh->_points[*ito];

            std::vector<int>::iterator itf =_mesh->_points[*ito]. _faceidx.begin();
            for(  ; itf != _mesh->_points[*ito]._faceidx.end(); itf++)
            {
                for (int p = 0; p < 3; p++)
                {
                    C3DPoint pi =_mesh->_points[ _mesh->_triangles[*itf].pnt_index[p]];

                    if(pi._selected &&  (_mesh->_triangles[*itf].pnt_index[p] != *ito))
                    {

                        float vx = pi._x - po._x;
                        float vy = pi._y - po._y;
                        float vz = pi._z - po._z;

                        float vlen = sqrt(vx*vx + vy*vy + vz*vz);
                        vx /= vlen;
                        vy /= vlen;
                        vz /= vlen;

                        cur_smooth_measure += fabs(vx*po._nx + vy*po._ny + vz *po._nz);
                    }
                }
            }
        }
        std::cout << "current measure " << dhei << " "  <<  cur_smooth_measure << std::endl;

        if(cur_smooth_measure < smooth_measure)
        {
            smooth_measure = cur_smooth_measure;
            dhei_best = dhei;
        }

        /// reset mesh
        iti = inner_v.begin();
        for(int c = 0 ; iti != inner_v.end(); iti++, c++)
              _mesh->_points[*iti] = inner_p_bckp[c];
    }


    std::cout << "BEST HEIGHT " <<  dhei_best << std::endl;

    //// make best smooth


    float height = float(dhei_best) * gpho/4;
    iti = inner_v.begin();
    for(int c = 0 ; iti != inner_v.end(); iti++, c++)
    {
        C3DPoint pi = _mesh->_points[*iti];

        float n_p = height * ( w_dists[c]  )/(max_dist - min_dist);

        _mesh->_points[*iti]._x += n_p *  _mesh->_points[*iti]._nx ;
        _mesh->_points[*iti]._y += n_p *  _mesh->_points[*iti]._ny ;
        _mesh->_points[*iti]._z += n_p *  _mesh->_points[*iti]._nz ;
    }




    for(int iter = 0; iter < 5; iter++)
    {
        std::map<int,C3DPoint> res_vert;
        std::vector<int>::iterator itv = inner_v.begin();
        for( ; itv != inner_v.end(); itv++)
        {

            std::set<int> _q_i;
            std::vector<int>::iterator itf =  _mesh->_points[*itv]._faceidx.begin();
            for(  ; itf != _mesh->_points[*itv]._faceidx.end(); itf++)
            {
                for (int p = 0; p < 3; p++)
                {
                    int n_v_idx = _mesh->_triangles[*itf].pnt_index[p];
                    if(  (*itv) !=  n_v_idx)
                        _q_i.insert(n_v_idx);
                }
            }

            float sum = 0;

            float r_x = 0;
            float r_y = 0;
            float r_z = 0;

            float n_x = 0;
            float n_y = 0;
            float n_z = 0;

            C3DPoint v = _mesh->_points[*itv];
            std::set<int>::iterator itn = _q_i.begin();
            for(; itn != _q_i.end(); itn++)
            {
                C3DPoint qi = _mesh->_points[*itn];

                float t = sqrt((qi._x - v._x)*(qi._x - v._x) +
                               (qi._y - v._y)*(qi._y - v._y) +
                               (qi._z - v._z)*(qi._z - v._z) );

                if(t < 0.1 ) t = 0.1;

                float w_i =  1 / t;

                r_x += w_i * qi._x;
                r_y += w_i * qi._y;
                r_z += w_i * qi._z;
                n_x += w_i * qi._nx;
                n_y += w_i * qi._ny;
                n_z += w_i * qi._nz;

                sum += w_i;
            }

             C3DPoint rv (v);

             rv._x =  r_x / sum ;
             rv._y =  r_y / sum;
             rv._z =  r_z / sum;
             rv._nx = n_x / sum;
             rv._ny = n_y / sum;
             rv._nz = n_z / sum;

             res_vert[*itv] = rv;
        }

        std::map<int,C3DPoint>::iterator itm = res_vert.begin();
        for( ; itm != res_vert.end(); itm++ )
             _mesh->_points[itm->first] = itm->second;
    }

    ///iterations
    /*{

        std::map<int,C3DPoint> res_vert;
        std::vector<int>::iterator itv = inner_v.begin();
        for( ; itv != inner_v.end(); itv++)
        {
            float sum = 0;

            float r_x = 0;
            float r_y = 0;
            float r_z = 0;

            float n_x = 0;
            float n_y = 0;
            float n_z = 0;

            C3DPoint v = _mesh->_points[*itv];

            float sigma = gpho;
            std::vector<int>::iterator itn = inner_v.begin();
            for(; itn != inner_v.end(); itn++)
            {
                C3DPoint qi = _mesh->_points[*itn];

                if(*itn == *itv) continue;

                float t = sqrt((qi._x - v._x)*(qi._x - v._x) +
                               (qi._y - v._y)*(qi._y - v._y) +
                               (qi._z - v._z)*(qi._z - v._z) );

                float w_i =  1/t;

                r_x += w_i * qi._x;
                r_y += w_i * qi._y;
                r_z += w_i * qi._z;
                n_x += w_i * qi._nx;
                n_y += w_i * qi._ny;
                n_z += w_i * qi._nz;

                sum += w_i;
            }

            C3DPoint rv (v);

            rv._x =  r_x / sum ;
            rv._y =  r_y / sum;
            rv._z =  r_z / sum;
            rv._nx = n_x / sum;
            rv._ny = n_y / sum;
            rv._nz = n_z / sum;

            res_vert[*itv] = rv;
        }

        std::map<int,C3DPoint>::iterator itm = res_vert.begin();
        for( ; itm != res_vert.end(); itm++ )
             _mesh->_points[itm->first] = itm->second;
    }*/



    //////////////////////////////////////

    /// TODO recalculate face normals
    /*std::set<int>::iterator ittf = _sel_faces.begin();
    for( ; ittf != _sel_faces.end(); ittf++ )
    {
        float n_x = 0;
        float n_y = 0;
        float n_z = 0;

        for(int p = 0; p < 3; p++)
        {
            C3DPoint pnt =  _mesh->_points[ _mesh->_triangles[*ittf].pnt_index[p]];

            n_x += pnt._nx;
            n_y += pnt._ny;
            n_z += pnt._nz;

        }
        float nlen = sqrt(n_x*n_x + n_y*n_y + n_z*n_z);
        n_x /= nlen;
        n_y /= nlen;
        n_z /= nlen;

        _mesh->_triangles[*ittf].tri_normal._x = n_x;
        _mesh->_triangles[*ittf].tri_normal._y = n_y;
        _mesh->_triangles[*ittf].tri_normal._z = n_z;
    }*/
    std::set<int>::iterator ittf = _sel_faces.begin();
    for( ; ittf != _sel_faces.end(); ittf++ )
    {
        C3DPoint pnt0 =  _mesh->_points[ _mesh->_triangles[*ittf].pnt_index[0]];
        C3DPoint pnt1 =  _mesh->_points[ _mesh->_triangles[*ittf].pnt_index[1]];
        C3DPoint pnt2 =  _mesh->_points[ _mesh->_triangles[*ittf].pnt_index[2]];
         _mesh->_triangles[*ittf].tri_normal = compute_normal( pnt2, pnt1, pnt0);
    }

    /*std::vector<int>::iterator*/ iti = inner_v.begin();
    for( ; iti != inner_v.end(); iti++)
    {
            C3DPoint pnt =  _mesh->_points[*iti];

            float nx = 0;
            float ny = 0;
            float nz = 0;

            std::vector<int>::iterator itf = _mesh->_points[*iti]._faceidx.begin();
            for(  ; itf != _mesh->_points[*iti]._faceidx.end(); itf++)
            {
                C3DPoint tnorm = _mesh->_triangles[*itf].tri_normal;
                nx += tnorm._x;
                ny += tnorm._y;
                nz += tnorm._z;
            }
            float len = sqrt(nx*nx + ny*ny + nz*nz);
             _mesh->_points[*iti]._nx = nx / len;
             _mesh->_points[*iti]._ny = ny / len;
            _mesh->_points[*iti]._nz = nz / len;



    }

    delete_glLists();
}

void CTriangularMesh::update_section( std::vector<cv::Point2f> & v, CGLCamera * cam )
{


    if(_mesh == NULL) return;

    GLdouble projection[16];
    glGetDoublev(GL_PROJECTION_MATRIX, projection);

    GLdouble modelview[16];
    glGetDoublev(GL_MODELVIEW_MATRIX, modelview);

    GLint viewport[4];
    glGetIntegerv( GL_VIEWPORT, viewport );

    //GLdouble screen_coords[3];

    CollisionModel3D * collision_model = construct_collision_model();

    cv::Point3f camera_normal(cam->_cx - cam->_tx, cam->_cy - cam->_ty, cam->_cz - cam->_tz );
    float cvlen = std::sqrt(camera_normal.x * camera_normal.x +
                            camera_normal.y * camera_normal.y +
                            camera_normal.z * camera_normal.z);
    camera_normal.x /= cvlen;
    camera_normal.y /= cvlen;
    camera_normal.z /= cvlen;



    cv::Point3f camera_origin(cam->_tx, cam->_ty, cam->_tz);

    float test_origin[3];
    float test_direction[3];

    float _angle_threshold = -cos(BGM(85.0));

    std::vector<C3DPoint>::iterator it = _mesh->_points.begin();
    for( ; it != _mesh->_points.end(); it++)
    {
        if (v.size() < 3) // clear selection
        {
            it->_selected = false;
            continue;
        }
        // previous ray selection
        if(it->_selected) continue;

        GLdouble px = 0;
        GLdouble py = 0;
        GLdouble pz = 0;
        /// compute projection
        gluProject(it->_x, it->_y, it->_z, modelview, projection, viewport, &px, &py, &pz);

        // if(points  projects into RIO)
        bool is_in_ROI = false;
        if (v.size() >=3 )
             is_in_ROI =( cv::pointPolygonTest(v, cv::Point2f(px,py), false) > 0);

        if(is_in_ROI) it->_selected = true;
        //else  it->_selected = false; // can be removed (!)


        if(! it->_selected) continue;

        if(g_use_ray_tracing)
        {
            /////////////////////////////////////////////////////////
            /// step 1 back face culling - skip point with wrong normal
            /////////////////////////////////////////////////////////
            C3DPoint pnt =  *it;
            float pnlen = std::sqrt(pnt._nx * pnt._nx + pnt._ny * pnt._ny + pnt._nz * pnt._nz);

            float cos_alpha = (pnt._nx * camera_normal.x +
                               pnt._ny * camera_normal.y +
                               pnt._nz * camera_normal.z)/pnlen;

             if(cos_alpha > _angle_threshold)
             {
                 it->_selected = false;
                continue;
             }



            /////////////////////////////////////////////////////////
            /// step 2 Collision test
            /////////////////////////////////////////////////////////

            Vector3f view_direction;
            view_direction.x = -pnt._x + camera_origin.x ;
            view_direction.y = -pnt._y + camera_origin.y ;
            view_direction.z = -pnt._z + camera_origin.z ;
             float vlen = std::sqrt (view_direction.x * view_direction.x  +
                                     view_direction.y * view_direction.y  +
                                     view_direction.z * view_direction.z  );
            view_direction.x /= vlen;
            view_direction.y /= vlen;
            view_direction.z /= vlen;


            test_origin[0] = pnt._x + 0.01 * view_direction.x;
            test_origin[1] = pnt._y + 0.01 * view_direction.y;
            test_origin[2] = pnt._z + 0.01 * view_direction.z;

            test_direction[0] = view_direction.x;
            test_direction[1] = view_direction.y;
            test_direction[2] = view_direction.z;

            bool collide  = collision_model->rayCollision(test_origin, test_direction);
            if(collide)
                 it->_selected = false;
       }

     }


    delete collision_model;

    delete_glLists();
}

CTriangularMesh::~CTriangularMesh()
{
    delete_mesh();
    delete_glLists();
}

void CTriangularMesh::delete_glList_tex()
{
    if(_tex_glid != nonGLlist)
    {
        glDeleteLists(_tex_glid ,1);
        _tex_glid  = nonGLlist;
    }

    if(_tex_matid != nonGLlist)
    {
        glDeleteTextures(1,  &_tex_matid);
        _tex_matid = nonGLlist;
    }
}

void CTriangularMesh::remake_glList()
{
    delete_glLists();
}

void CTriangularMesh::delete_glLists()
{
    //std::cout << "CTriangularMesh::delete_glLists()" <<std::endl;

    if(_wire_glid != nonGLlist)
    {
        glDeleteLists(_wire_glid ,1);
        _wire_glid  = nonGLlist;
    }

    if(_flat_glid != nonGLlist)
    {
        glDeleteLists(_flat_glid ,1);
        _flat_glid  = nonGLlist;
    }

    if(_smooth_glid != nonGLlist)
    {
        glDeleteLists(_smooth_glid ,1);
        _smooth_glid  = nonGLlist;
    }

    delete_glList_tex();

    _oglid = nonGLlist;

}

void CTriangularMesh::load_tex_matid()
{
    if(_mesh->_texture == NULL) return;

    std::cout << "load_tex_matid" << std::endl;

    glEnable(GL_TEXTURE_2D);
    glGenTextures(1, &_tex_matid);
    QImage tximg = QGLWidget::convertToGLFormat(*_mesh->_texture);
    glBindTexture(GL_TEXTURE_2D, _tex_matid);

    glTexImage2D(GL_TEXTURE_2D, 0, 3, tximg.width(), tximg.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, tximg.bits());
    gluBuild2DMipmaps(GL_TEXTURE_2D, 3, tximg.width(), tximg.height(), GL_RGBA, GL_UNSIGNED_BYTE, tximg.bits());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,  GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,  GL_REPEAT);

    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glDisable(GL_TEXTURE_2D);

/*

    int nx = _mesh->_texture->width();
    int ny =  _mesh->_texture->height();
    int byte_count = nx * ny * 4;
    int raw_byte_count = nx * 4;

    uchar * new_bits = new uchar[byte_count];
    uchar * old_bits = _mesh->_texture->bits();

    // OpenGL spec: has to flip image vertically before texture creation
    // std::cout << nx << " " << ny << " " << byte_count  << " " << raw_byte_count << std::endl;
    for( int r = 0; r < ny; r++)
    {
        memcpy ( new_bits + raw_byte_count * r ,
                 old_bits + raw_byte_count * (ny - r - 1),  raw_byte_count );
    }
    // memcpy ( new_bits, old_bits , byte_count);


    glGenTextures(1, &_tex_matid);
    glBindTexture(GL_TEXTURE_2D, _tex_matid);


    /// glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    // glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    // glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_MIRRORED_REPEAT);
    // glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_MIRRORED_REPEAT);

    ////glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    //glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

     // , GL_MIRRORED_REPEAT, or
    ///
    ////// light texture
    ///glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    //glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR_MIPMAP_LINEAR);
    //glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    //glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    delete [] new_bits;
*/
}


void CTriangularMesh::create_glList_wire()
{
    float cr0 = (_use_glob_coloring)? CGlobalHSSettings::gcolor_mr: _color[0] ;
    float cg0 = (_use_glob_coloring)? CGlobalHSSettings::gcolor_mg: _color[1] ;
    float cb0 = (_use_glob_coloring)? CGlobalHSSettings::gcolor_mb: _color[2] ;

    _wire_glid = glGenLists(1);
    glNewList(_wire_glid, GL_COMPILE_AND_EXECUTE);

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glEnable(GL_COLOR_MATERIAL);
    ///std::cout<<"wire mode " << (int)glIsEnabled(GL_COLOR_MATERIAL)<<std::endl;

    glBegin(GL_TRIANGLES);

    std::cout << "::" << _mesh->_triangles.size() << std::endl;

    std::vector<C3DPointIdx>::iterator itt = _mesh->_triangles.begin();
    for(; itt != _mesh->_triangles.end(); itt++)
    {
        C3DPoint pnt0 = _mesh->_points[ itt->pnt_index[0] ];
        C3DPoint pnt1 = _mesh->_points[ itt->pnt_index[1] ];
        C3DPoint pnt2 = _mesh->_points[ itt->pnt_index[2] ];
        float cr, cg, cb;
        if(pnt0._selected && pnt1._selected && pnt2._selected)
        {
            cr = 1.0;
            cg = 0;
            cb = 0;
        }
        else
        {
            cr = cr0;
            cg = cg0;
            cb = cb0;
        }


        C3DPoint tri_normal =  itt->tri_normal;
        ///std::cout << "create_glList_wire " << tri_normal._x << " " <<   tri_normal._y << " " << tri_normal._z << std::endl;
       // glColor3f(_color[0] , _color[1] ,  _color[2] );
         glColor3f(cr, cg,  cb);
        //glColor3f(pnt0._r, pnt0._g,  pnt0._b);
        glNormal3f (tri_normal._x, tri_normal._y, tri_normal._z );
        //glNormal3f (pnt0._nx, pnt0._ny, pnt0._nz );
        glVertex3f( pnt0._x, pnt0._y, pnt0._z);

        // glColor3f(_color[0] , _color[1] ,  _color[2] );
        glColor3f(cr, cg, cb);
        //glColor3f(pnt1._r, pnt1._g,  pnt1._b);
        glNormal3f (tri_normal._x, tri_normal._y, tri_normal._z );
        //glNormal3f (pnt1._nx, pnt1._ny, pnt1._nz );
        glVertex3f( pnt1._x, pnt1._y, pnt1._z);

        //glColor3f(_color[0] , _color[1] ,  _color[2] );
          glColor3f(cr, cg, cb);
        //glColor3f(pnt2._r, pnt2._g,  pnt2._b);
        glNormal3f (tri_normal._x, tri_normal._y, tri_normal._z );
        //glNormal3f (pnt2._nx, pnt2._ny, pnt2._nz );
        glVertex3f( pnt2._x, pnt2._y, pnt2._z);
    }
   glEnd();
   
   glColor3f(1.f, 1.f, 1.f);
   float spec [] = {0.f, 0.f, 0.f, 1.f};
   glMaterialfv (GL_FRONT, GL_SPECULAR, spec);
   glMaterialf (GL_FRONT, GL_SHININESS, 0);
   glDisable(GL_COLOR_MATERIAL);

   
   
   
   glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

   glEndList();
}

void CTriangularMesh::create_glList_flat()
{
    float cr0 = (_use_glob_coloring)? CGlobalHSSettings::gcolor_mr: _color[0] ;
    float cg0 = (_use_glob_coloring)? CGlobalHSSettings::gcolor_mg: _color[1] ;
    float cb0 = (_use_glob_coloring)? CGlobalHSSettings::gcolor_mb: _color[2] ;

    _flat_glid = glGenLists(1);
    glNewList(_flat_glid, GL_COMPILE_AND_EXECUTE);

     glEnable(GL_COLOR_MATERIAL);
    glMaterialfv (GL_FRONT, GL_SPECULAR, CGlobalHSSettings::gl_mat_Specularity);
    glMaterialf (GL_FRONT, GL_SHININESS, CGlobalHSSettings::gl_mat_Shiness);

    glBegin(GL_TRIANGLES);
    ////std::cout<<"flat mode " << (int)glIsEnabled(GL_COLOR_MATERIAL)<<std::endl;

    std::vector<C3DPointIdx>::iterator itt = _mesh->_triangles.begin();
    for(; itt != _mesh->_triangles.end(); itt++)
    {
        C3DPoint pnt0 = _mesh->_points[ itt->pnt_index[0] ];
        C3DPoint pnt1 = _mesh->_points[ itt->pnt_index[1] ];
        C3DPoint pnt2 = _mesh->_points[ itt->pnt_index[2] ];

        float cr, cg, cb;
        if(pnt0._selected && pnt1._selected && pnt2._selected)
        {
            cr = 1.0;
            cg = 0;
            cb = 0;
        }
        else
        {
            cr = cr0;
            cg = cg0;
            cb = cb0;
        }



        C3DPoint tri_normal =  itt->tri_normal;
        ///std::cout << "create_glList_flat " << tri_normal._x << " " <<   tri_normal._y << " " << tri_normal._z << std::endl;

        //glColor3f(_color[0] , _color[1] ,  _color[2] );
        //glColor3f(pnt0._r, pnt0._g,  pnt0._b);
        glColor3f(cr, cg, cb);
        glNormal3f (tri_normal._x, tri_normal._y, tri_normal._z );
        //glNormal3f (pnt0._nx, pnt0._ny, pnt0._nz );
        glVertex3f( pnt0._x, pnt0._y, pnt0._z);

        //glColor3f(_color[0] , _color[1] ,  _color[2] );
        //glColor3f(pnt1._r, pnt1._g,  pnt1._b);
        glColor3f(cr, cg, cb);
        glNormal3f (tri_normal._x, tri_normal._y, tri_normal._z );
        //glNormal3f (pnt1._nx, pnt1._ny, pnt1._nz );
        glVertex3f( pnt1._x, pnt1._y, pnt1._z);

        //glColor3f(_color[0] , _color[1] ,  _color[2] );
        //glColor3f(pnt2._r, pnt2._g,  pnt2._b);
        glColor3f(cr, cg, cb);
        glNormal3f (tri_normal._x, tri_normal._y, tri_normal._z );
        //glNormal3f (pnt2._nx, pnt2._ny, pnt2._nz );
        glVertex3f( pnt2._x, pnt2._y, pnt2._z);
    }
   glEnd();
   // set defaut values back
   glColor3f(1.f, 1.f, 1.f);
   float spec [] = {0.f, 0.f, 0.f, 1.f};
   glMaterialfv (GL_FRONT, GL_SPECULAR, spec);
   glMaterialf (GL_FRONT, GL_SHININESS, 0);


   ///////// debug
   /*glBegin(GL_LINES);
   std::vector<C3DPoint>::iterator	 itp = _mesh->_points.begin();
   for( ; itp !=  _mesh->_points.end(); itp++)
   {
      glColor3f(1, 0, 0);
      glVertex3f(itp->_x, itp->_y, itp->_z);
      glColor3f(0, 1, 0);
      glVertex3f(itp->_x+.1*itp->_nx, itp->_y+.1*itp->_ny, itp->_z+.1*itp->_nz);

   }
   glEnd();*/
   ////////////////////
   ///

   glDisable(GL_COLOR_MATERIAL);

   glEndList();

}

void CTriangularMesh::create_glList_smooth()
{

    float cr0 = (_use_glob_coloring)? CGlobalHSSettings::gcolor_mr: _color[0] ;
    float cg0 = (_use_glob_coloring)? CGlobalHSSettings::gcolor_mg: _color[1] ;
    float cb0 = (_use_glob_coloring)? CGlobalHSSettings::gcolor_mb: _color[2] ;

    _smooth_glid = glGenLists(1);
    glNewList(_smooth_glid, GL_COMPILE_AND_EXECUTE);


  glEnable(GL_COLOR_MATERIAL);
   glMaterialfv (GL_FRONT, GL_SPECULAR, CGlobalHSSettings::gl_mat_Specularity);
    glMaterialf (GL_FRONT, GL_SHININESS, CGlobalHSSettings::gl_mat_Shiness);

   /// std::cout<<"smooth mode " << (int)glIsEnabled(GL_COLOR_MATERIAL)<<std::endl;

    glBegin(GL_TRIANGLES);
    std::vector<C3DPointIdx>::iterator itt = _mesh->_triangles.begin();
    for(; itt != _mesh->_triangles.end(); itt++)
    {
        C3DPoint pnt0 = _mesh->_points[ itt->pnt_index[0] ];
        C3DPoint pnt1 = _mesh->_points[ itt->pnt_index[1] ];
        C3DPoint pnt2 = _mesh->_points[ itt->pnt_index[2] ];

        float cr, cg, cb;
        if(pnt0._selected && pnt1._selected && pnt2._selected)
        {
            cr = 1.0;
            cg = 0;
            cb = 0;
        }
        else
        {
            cr = cr0;
            cg = cg0;
            cb = cb0;
        }


        ///std::cout << "create_glList_smooth " << pnt0._nx << " " <<   pnt0._ny << " " << pnt0._nz<< std::endl;
        ///C3DPoint tri_normal =  itt->tri_normal;

        //glColor3f(_color[0] , _color[1] ,  _color[2] );
        //glColor3f(pnt0._r, pnt0._g,  pnt0._b);
        //glNormal3f (tri_normal._x, tri_normal._y, tri_normal._z );
        glColor3f(cr, cg, cb);
        glNormal3f (pnt0._nx, pnt0._ny, pnt0._nz );
        //glNormal3f (1, 0, 0);
        glVertex3f( pnt0._x, pnt0._y, pnt0._z);

        //glColor3f(_color[0] , _color[1] ,  _color[2] );
        //glColor3f(pnt1._r, pnt1._g,  pnt1._b);
        //glNormal3f (tri_normal._x, tri_normal._y, tri_normal._z );
        glColor3f(cr, cg, cb);
        glNormal3f (pnt1._nx, pnt1._ny, pnt1._nz );
        //glNormal3f (1, 0, 0);
        glVertex3f( pnt1._x, pnt1._y, pnt1._z);

        //glColor3f(_color[0] , _color[1] ,  _color[2] );
        //glColor3f(pnt2._r, pnt2._g,  pnt2._b);
        //glNormal3f (tri_normal._x, tri_normal._y, tri_normal._z );
        glColor3f(cr, cg, cb);
        glNormal3f (pnt2._nx, pnt2._ny, pnt2._nz );
        //glNormal3f (1, 0, 0);
        glVertex3f( pnt2._x, pnt2._y, pnt2._z);
    }
   glEnd();

   // set defaut values back
   glColor3f(1.f, 1.f, 1.f);
   float spec [] = {0.f, 0.f, 0.f, 1.f};

 glMaterialfv (GL_FRONT, GL_SPECULAR, spec);
 glMaterialf (GL_FRONT, GL_SHININESS, 0);
   glDisable(GL_COLOR_MATERIAL);





   glEndList();

}

void CTriangularMesh::delete_mesh()
{
    if(_mesh)
    {
        delete _mesh;
        _mesh = NULL;
    }
}


void CTriangularMesh::set_mesh(CTriMesh * m)
{   if(_mesh != NULL)
    {
        delete_mesh();
        delete_glLists();
    }
    _mesh = m;
}

void CTriangularMesh::create_glList_tex()
{
    if(_mesh->_texture == NULL)
    {
        //create_glList_smooth();
        _tex_glid = _smooth_glid;
        return;
    }
    load_tex_matid();

    _tex_glid = glGenLists(1);
    glNewList(_tex_glid, GL_COMPILE_AND_EXECUTE);
   glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, _tex_matid);

    glDisable(GL_COLOR_MATERIAL);

    /// std::cout<<"tex mode " << (int)glIsEnabled(GL_COLOR_MATERIAL)<<std::endl;

    glBegin(GL_TRIANGLES);
    std::vector<C3DPointIdx>::iterator itt = _mesh->_triangles.begin();
    for(; itt != _mesh->_triangles.end(); itt++)
    {
        C3DPoint pnt0 = _mesh->_points[ itt->pnt_index[0] ];

        C3DPoint pnt1 = _mesh->_points[ itt->pnt_index[1] ];

        C3DPoint pnt2 = _mesh->_points[ itt->pnt_index[2] ];


        glNormal3f (pnt0._nx, pnt0._ny, pnt0._nz );
        glTexCoord2f(_mesh->_tex_coords[itt->tex_index[0]].x,
                     _mesh->_tex_coords[itt->tex_index[0]].y);
        // glTexCoord2f(pnt0._tex_x, pnt0._tex_y);
        //glColor3f(1., 1.,  1.);
        glVertex3f( pnt0._x, pnt0._y, pnt0._z);

        glNormal3f (pnt1._nx, pnt1._ny, pnt1._nz );
        glTexCoord2f(_mesh->_tex_coords[itt->tex_index[1]].x,
                     _mesh->_tex_coords[itt->tex_index[1]].y);
        // glTexCoord2f(pnt1._tex_x, pnt1._tex_y);
        //glColor3f(1., 1.,  1.);
        glVertex3f( pnt1._x, pnt1._y, pnt1._z);


        glNormal3f (pnt2._nx, pnt2._ny, pnt2._nz );
        glTexCoord2f(_mesh->_tex_coords[itt->tex_index[2]].x,
                     _mesh->_tex_coords[itt->tex_index[2]].y);
        // glTexCoord2f(pnt2._tex_x, pnt2._tex_y);
        //glColor3f(1., 1.,  1.);
        glVertex3f( pnt2._x, pnt2._y, pnt2._z);

    }
   glEnd();

   glColor3f(1, 1, 1);
   glDisable(GL_TEXTURE_2D);

   glEndList();
}


CTriangularMesh::CTriangularMesh(const CTriangularMesh & src)
{
    _mesh = new CTriMesh(*src._mesh);
    _rmode = src._rmode;

    _color[0] = .8f;
    _color[1] = .8f;
    _color[2] = .8f;

    _rx = 0.05f;
    _ry = 0.05f;
    _rz = 0.05f;

    _wire_glid   = nonGLlist;
    _flat_glid   = nonGLlist;
    _smooth_glid = nonGLlist;
    _tex_glid    = nonGLlist;
    _tex_matid   = nonGLlist;

    _use_glob_coloring = src._use_glob_coloring;
}

void CTriangularMesh::set_correct_glid()
{
    switch(_rmode)
    {
    case MESH_WIRE:
        _oglid = _wire_glid;
        break;
    case MESH_FLAT:
        _oglid = _flat_glid;
        break;
    case MESH_SMOOTH:
        _oglid = _smooth_glid;
        break;
    case MESH_TEX:
        _oglid = _tex_glid;
        break;
    }
}

void  CTriangularMesh::set_rendering_mode(RenderingMode rm)
{
    _rmode = rm;
    set_correct_glid();
}

void CTriangularMesh::create_glList()
{
    delete_glLists();

    create_glList_wire();
    create_glList_flat();
    create_glList_smooth();
    create_glList_tex();
    set_correct_glid();

   // std::cout << " CTriangularMesh::create_glList() " << _oglid << std::endl;
}


void CTriangularMesh::save(std::ofstream & ofile)
{
    // rmode
    ofile.write(reinterpret_cast<const char *>(&_rmode), sizeof(RenderingMode));

    ofile.write(reinterpret_cast<const char *>(&_use_glob_coloring), sizeof(bool));
    ofile.write(reinterpret_cast<const char *>(&_color[0]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_color[1]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_color[2]), sizeof(float));

    bool have_mesh = (_mesh == NULL)? false : true;
    ofile.write(reinterpret_cast<const char *>(&_mesh), sizeof(bool));
    if(have_mesh) _mesh->save(ofile);


}

void CTriangularMesh::load(std::ifstream & ifile)
{
    // rmode
    ifile.read(reinterpret_cast<char *>(&_rmode), sizeof(RenderingMode));
    ifile.read(reinterpret_cast<char *>(&_use_glob_coloring), sizeof(bool));
    ifile.read(reinterpret_cast<const char *>(&_color[0]), sizeof(float));
    ifile.read(reinterpret_cast<const char *>(&_color[1]), sizeof(float));
    ifile.read(reinterpret_cast<const char *>(&_color[2]), sizeof(float));

    delete_mesh();

    bool have_mesh = false;
    ifile.read(reinterpret_cast<char *>(&have_mesh), sizeof(bool));
    if(have_mesh)
    {
        _mesh = new CTriMesh;
        _mesh->load(ifile);
    }
}

bool CTriangularMesh::m_import_obj(QString &file)
{
    if(file.isEmpty()) return false;
    delete_mesh();
    _mesh = load_obj_mesh(file.toStdString());

    return true;
}

bool CTriangularMesh::m_export(QString & fname)
{
    if(fname.isEmpty()) return false;

    bool res = false;
    // off file format
   // if(fname.endsWith(".off", Qt::CaseInsensitive))
    //    res = m_export_off(fname);
    // obj file format
  //  if(fname.endsWith(".obj", Qt::CaseInsensitive))
        res = m_export_obj(fname);

    return res;

}

bool CTriangularMesh::m_export_xyz(QString & fname)
{
    std::ofstream ofile;
    ofile.open(fname.toLocal8Bit().constData(), std::ios::out );
    if(! ofile.is_open())
                return false;

    // output vertices ony
    std::vector<C3DPoint>::iterator itv =  _mesh->_points.begin();
    for(  ; itv != _mesh->_points.end(); itv++ )
        ofile <<  itv->_x <<  " " << itv->_y<< " " << itv->_z << " " /*<< itv->_nx << " " << itv->_ny << " " << itv->_nz << " " */ << itv->_r <<  " " << itv->_g<< " " << itv->_b /*<< " 0"*/ <<std::endl;

    return true;
}

bool CTriangularMesh::m_export_off(QString & fname)
{
    std::ofstream ofile;
    ofile.open(fname.toLocal8Bit().constData(), std::ios::out );
    if(! ofile.is_open())
                return false;

    ofile << "OFF" << std::endl;
    ofile << _mesh->_points.size() << " " << _mesh->_triangles.size() << " " << 0 << std::endl;

    std::vector<C3DPoint>::iterator vit = _mesh->_points.begin();
    for( ; vit != _mesh->_points.end(); vit++ )
        ofile << vit->_x << " " << " " << vit->_y<< " " << vit->_z << std::endl;

    std::vector<C3DPointIdx>::iterator fit = _mesh->_triangles.begin();
    for( ; fit != _mesh->_triangles.end(); fit++ )
        ofile << 3 << " " << fit->pnt_index[0] << " " << fit->pnt_index[1] << " " << fit->pnt_index[2]  << std::endl;

    return true;
}

void CTriangularMesh::remove_mesh_texture()
{
    if(is_textured())
    {
        _mesh->remove_texture();
        delete_glList_tex();
    }
}

void CTriangularMesh::replace_mesh_texture(QImage * tex)
{
    if(is_textured())
    {
        _mesh->replace_texture(tex);
        delete_glList_tex();
    }
}

bool CTriangularMesh::is_textured()
{
    if(_mesh != NULL)
        if(_mesh->_texture != NULL)
            return true;
    return false;
}


QString CTriangularMesh::get_texture_name()
{
    if(!is_textured()) return QString("empty");

    return _mesh->_texture_name.c_str();
}

bool CTriangularMesh::m_export_obj(QString & fname)
{
    std::ofstream ofile;

    ///std::cout <<fname.toLocal8Bit().data() << std::endl;

    ofile.open(fname.toLocal8Bit().data(), std::ios::out );
    if(! ofile.is_open())
                return false;

    ///std::cout << "opened" << std::endl;

    QFileInfo info_fname(fname);
    QString working_dir = info_fname.absolutePath();
    QString base_name = info_fname.baseName();

    if(_mesh->_texture != NULL)
    {
        QString mtlfile =  base_name + ".mtl";
        std::string imgfilename = base_name.toStdString()  + ".png";
        ofile << "mtllib " <<  mtlfile.toStdString()  << std::endl << std::endl;

        mtlfile  = working_dir + "/"  + mtlfile  ;

        // std::cout << mtlfile.toLocal8Bit().data() << std::endl;

         // export mtl file
        std::ofstream mtlfilestrem;
        mtlfilestrem.open(mtlfile.toLocal8Bit().data(), std::ios::out );

        mtlfilestrem << "newmtl 01___Default" << std::endl;
        mtlfilestrem << "Ka 0.200000 0.200000 0.200000" << std::endl;
        mtlfilestrem << "Kd 0.000000 0.000000 1.000000" << std::endl;
        mtlfilestrem << "map_Ka " << imgfilename/*_mesh->_texture_name*/ << std::endl;
        mtlfilestrem << "map_Kd " << imgfilename/*_mesh->_texture_name*/ << std::endl;
        mtlfilestrem.close();


        // save image
        std::string imagfile = working_dir.toStdString() + "/"  + imgfilename/*_mesh->_texture_name*/;
        uchar * bits = _mesh->_texture->bits();
        _mesh->_texture->save(imagfile.c_str());

    }
    // output vertices
    std::vector<C3DPoint>::iterator vit = _mesh->_points.begin();
    for( ; vit != _mesh->_points.end(); vit++ )
        ofile <<  "v " << vit->_x << " " <<  vit->_y<< " " << vit->_z << std::endl;

    // output texture coordinates
    if(_mesh->_texture != NULL)
    {
        std::vector<Vector2f>::iterator itt = _mesh->_tex_coords.begin();
        for( ; itt != _mesh->_tex_coords.end(); itt++ )
            ofile <<  "vt " << itt->x << " " << " " << itt->y <<std::endl;
    }

    // output normals
    vit = _mesh->_points.begin();
    for( ; vit != _mesh->_points.end(); vit++ )
        ofile <<  "vn " << vit->_nx << " " << vit->_ny<< " " << vit->_nz << std::endl;


    // output faces
    std::vector<C3DPointIdx>::iterator fit = _mesh->_triangles.begin();
    for( ; fit != _mesh->_triangles.end(); fit++ )
    {
        ofile << "f ";
        for (int i = 0 ; i < 3; i++ )
        {
            ofile <<  fit->pnt_index[i]+1 << "/";
            if(_mesh->_texture != NULL) // if textured add coords
               ofile << fit->tex_index[i]+1;
            ofile <<  "/" << fit->pnt_index[i]+1 << " ";
        }
        ofile << std::endl;
    }
    ofile.close();

    return true;
}

