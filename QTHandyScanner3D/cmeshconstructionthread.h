#ifndef CMESHCONSTRUCTIONTHREAD_H
#define CMESHCONSTRUCTIONTHREAD_H

#include <QThread>
#include <QMutex>
#include "graphicutils.h"

///class CNormalCloud;
class CTriangularMesh;
class CMeshConstructionThread : public QThread
{
    Q_OBJECT
public:
    explicit CMeshConstructionThread(QObject *parent = 0);
    ~CMeshConstructionThread();


    void start_process( int, int, float, float,  std::vector<C3DPoint> *);
    bool is_abort(){return _abort;}
    void emit_send_back(int i);


signals:
    void send_back(const int val);
    void send_result(CTriangularMesh *);

protected:
    void run();


public slots:
    void stop_process();

private:
    bool    _abort;
    QMutex  _mutex;

    ///std::vector<C3DPoint>  * _normals;
    int	_octree_d;
    int _solvdiv;
    float _sampl_per_node;
    float _surf_offset;
    std::vector<C3DPoint> * _dpcl;


};

#endif // CMESHCONSTRUCTIONTHREAD_H
