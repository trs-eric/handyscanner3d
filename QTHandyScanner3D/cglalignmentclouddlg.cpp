#include "cglalignmentclouddlg.h"
#include "ui_cglalignmentclouddlg.h"
#include "cglwidget.h"

#include "settings/psettings.h"
#include "cframe.h"

CGLAlignmentCloudDlg::CGLAlignmentCloudDlg(CGLWidget * glWidget, QWidget *parent) :
    _glWidget(glWidget), QDialog(parent),
    ui(new Ui::CGLAlignmentCloudDlg)
{
    ui->setupUi(this);

    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(close_dlg()));
    /// connect(ui->previewBtn, SIGNAL(clicked()), this, SLOT(onPreviewBtn()));
    connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));


    connect(ui->edtXshift,   SIGNAL(valueChanged(double)), this, SLOT(onPreviewBtn()));
    connect(ui->edtYshift,   SIGNAL(valueChanged(double)), this, SLOT(onPreviewBtn()));
    connect(ui->edtZshift,   SIGNAL(valueChanged(double)), this, SLOT(onPreviewBtn()));

    connect(ui->edtPitch,   SIGNAL(valueChanged(double)), this, SLOT(onPreviewBtn()));
    connect(ui->edtRoll,   SIGNAL(valueChanged(double)), this, SLOT(onPreviewBtn()));

    ui->previewBtn->setVisible(false);
}


CGLAlignmentCloudDlg::~CGLAlignmentCloudDlg()
{
    delete ui;
}

void CGLAlignmentCloudDlg::close_dlg()
{
    restore_scene();
    close();
}

void CGLAlignmentCloudDlg::showEvent(QShowEvent *)
{
    std::vector<CGLCompoundObject*> cgllist = _glWidget->get_scene()->get_list_of_all_cglobjects();
    if(cgllist.size()==0)   return;

    std::vector<CGLCompoundObject*>::iterator it = cgllist.begin();
    for(int c = 0 ; it != cgllist.end(); it++ )
    {
        if((*it)->is_unique_object()) continue;

        CPointCloud * rp = (*it)->get_raw_points();

      _orig_rpnts.push_back(  rp->get_points_cpy() );
    }

    ui->edtXshift->blockSignals(true);
    ui->edtYshift->blockSignals(true);
    ui->edtZshift->blockSignals(true);
    ui->edtPitch->blockSignals(true);
    ui->edtRoll->blockSignals(true);
    ui->edtXshift->setValue(0);
    ui->edtYshift->setValue(0);
    ui->edtZshift->setValue(0);
    ui->edtPitch->setValue(0);
    ui->edtRoll->setValue(0);
    ui->edtXshift->blockSignals(false);
    ui->edtYshift->blockSignals(false);
    ui->edtZshift->blockSignals(false);
    ui->edtPitch->blockSignals(false);
    ui->edtRoll->blockSignals(false);




}

void CGLAlignmentCloudDlg::hideEvent(QHideEvent *)
{
    _orig_rpnts.clear();
}

void CGLAlignmentCloudDlg::restore_scene()
{

    std::vector<CGLCompoundObject*> cgllist = _glWidget->get_scene()->get_list_of_all_cglobjects();

    if(cgllist.size()==0)   return;

    std::vector<CGLCompoundObject*>::iterator it = cgllist.begin();
    for( int p = 0; it != cgllist.end(); it++)
    {
        if((*it)->is_unique_object()) continue;

        CPointCloud * rp = (*it)->get_raw_points();

        std::vector<C3DPoint> * vp = rp->get_points_direct_access();
         for(int i = 0; i < vp->size(); i++)
         {
              C3DPoint pp = _orig_rpnts[p][i];
                 (*vp)[i] = pp;
         }
         p++;
    }
    _glWidget->on_rawpnts_updated();

}

void CGLAlignmentCloudDlg::onPreviewBtn()
{

    std::vector<CGLCompoundObject*> cgllist = _glWidget->get_scene()->get_list_of_all_cglobjects();

    if(cgllist.size()==0)
       return;

    //// rotation axis

    double rotCX =  CGlobalHSSettings::gl_table_rot_center_x;
    double rotCY =  CGlobalHSSettings::gl_table_rot_center_y;
    double rotCZ =  CGlobalHSSettings::gl_table_rot_center_z;



    ///// rotation around y axis
    double y_rot_angle = BGM(ui->edtRoll->value());
    double r_mat_00 =  cos(y_rot_angle);
    double r_mat_02 =  sin(y_rot_angle);
    double r_mat_20 = -sin(y_rot_angle);
    double r_mat_22 =  cos(y_rot_angle);

    ///// rotation around x axis
    double x_rot_angle = BGM(ui->edtPitch->value());
    double mat_11 =  cos(x_rot_angle);
    double mat_12 = -sin(x_rot_angle);
    double mat_21 =  sin(x_rot_angle);
    double mat_22 =  cos(x_rot_angle);

    std::vector<CGLCompoundObject*>::iterator it = cgllist.begin();
    for(int p = 0 ; it != cgllist.end(); it++, p++)
    {
        if((*it)->is_unique_object()) continue;


        CPointCloud * rp = (*it)->get_raw_points();



        double rangle = global_keyframes_sequence[p]->_glob_rot_vec.at<double>(0,1);


        cv::Mat rotY = (cv::Mat_<double>(3,1) << 0, rangle, 0);

        cv::Mat rotM_Y;
        cv::Rodrigues(rotY, rotM_Y);

       ///  std::cout << "rangle " << rangle << std::endl;

        cv::Mat shiftV = (cv::Mat_<double>(3,1) << ui->edtXshift->value() * CGlobalHSSettings::glob_gl_scale,
                                                   ui->edtYshift->value() * CGlobalHSSettings::glob_gl_scale,
                                                   ui->edtZshift->value() * CGlobalHSSettings::glob_gl_scale);

        cv::Mat rshiftV = rotM_Y * shiftV;


        float dx = rshiftV.at<double>(0,0);
        float dy = rshiftV.at<double>(1,0);
        float dz = rshiftV.at<double>(2,0);

        //// back rotation around Z
        double obj_rot_angle = rangle;
        double rmat_00 =  cos(obj_rot_angle);
        double rmat_01 = -sin(obj_rot_angle);
        double rmat_10 =  sin(obj_rot_angle);
        double rmat_11 =  cos(obj_rot_angle);

        double iobj_rot_angle = -rangle;
        double irmat_00 =  cos(iobj_rot_angle);
        double irmat_01 = -sin(iobj_rot_angle);
        double irmat_10 =  sin(iobj_rot_angle);
        double irmat_11 =  cos(iobj_rot_angle);



        std::vector<C3DPoint> * vp = rp->get_points_direct_access();
         for(int i = 0; i < vp->size(); i++)
         {
              C3DPoint pp = _orig_rpnts[p][i];
              float x0 = pp._x - rotCX;
              float y0 = pp._y - rotCY;
              float z0 = pp._z - rotCZ;

              float ix0 = x0 * rmat_00 + z0 * rmat_01;
              float iy0 = y0;
              float iz0 = x0 * rmat_10 + z0 * rmat_11;

              // rotate around y
              float rx = ix0 * r_mat_00 + iy0 * r_mat_02;
              float ry = ix0 * r_mat_20 + iy0 * r_mat_22;
              float rz = iz0 ;

              // rotate around x
              float rrx = rx;
              float rry = ry * mat_11 + rz * mat_12;
              float rrz = ry * mat_21 + rz * mat_22;


              float fx = rrx * irmat_00 + rrz * irmat_01;
              float fy = rry;
              float fz = rrx * irmat_10 + rrz * irmat_11;



             pp._x = fx + rotCX + dx;
             pp._y = fy + rotCY + dy;
             pp._z = fz + rotCZ + dz;

             (*vp)[i] = pp;

         }

    }

     _glWidget->on_rawpnts_updated();
}


void CGLAlignmentCloudDlg::fix_cloud_normals()
{
    std::vector<CGLCompoundObject*> cgllist = _glWidget->get_scene()->get_list_of_all_cglobjects();

    if(cgllist.size()==0)
       return;

    ///// rotation around y axis
    double y_rot_angle = BGM(ui->edtRoll->value());
    double r_mat_00 =  cos(y_rot_angle);
    double r_mat_02 =  sin(y_rot_angle);
    double r_mat_20 = -sin(y_rot_angle);
    double r_mat_22 =  cos(y_rot_angle);

    ///// rotation around x axis
    double x_rot_angle = BGM(ui->edtPitch->value());
    double mat_11 =  cos(x_rot_angle);
    double mat_12 = -sin(x_rot_angle);
    double mat_21 =  sin(x_rot_angle);
    double mat_22 =  cos(x_rot_angle);

    std::vector<CGLCompoundObject*>::iterator it = cgllist.begin();
    for(int p = 0 ; it != cgllist.end(); it++, p++)
    {
        if((*it)->is_unique_object()) continue;


        CPointCloud * rp = (*it)->get_raw_points();



        double rangle = global_keyframes_sequence[p]->_glob_rot_vec.at<double>(0,1);

        //// back rotation around Z
        double obj_rot_angle = rangle;
        double rmat_00 =  cos(obj_rot_angle);
        double rmat_01 = -sin(obj_rot_angle);
        double rmat_10 =  sin(obj_rot_angle);
        double rmat_11 =  cos(obj_rot_angle);

        double iobj_rot_angle = -rangle;
        double irmat_00 =  cos(iobj_rot_angle);
        double irmat_01 = -sin(iobj_rot_angle);
        double irmat_10 =  sin(iobj_rot_angle);
        double irmat_11 =  cos(iobj_rot_angle);


         std::vector<C3DPoint> * vp = rp->get_points_direct_access();
         for(int i = 0; i < vp->size(); i++)
         {
              C3DPoint pp = _orig_rpnts[p][i];
              float x0 = pp._nx;
              float y0 = pp._ny;
              float z0 = pp._nz;

              float ix0 = x0 * rmat_00 + z0 * rmat_01;
              float iy0 = y0;
              float iz0 = x0 * rmat_10 + z0 * rmat_11;

              // rotate around y
              float rx = ix0 * r_mat_00 + iy0 * r_mat_02;
              float ry = ix0 * r_mat_20 + iy0 * r_mat_22;
              float rz = iz0 ;

              // rotate around x
              float rrx = rx;
              float rry = ry * mat_11 + rz * mat_12;
              float rrz = ry * mat_21 + rz * mat_22;


              float fx = rrx * irmat_00 + rrz * irmat_01;
              float fy = rry;
              float fz = rrx * irmat_10 + rrz * irmat_11;

             (*vp)[i]._nx = fx;
             (*vp)[i]._ny = fy;
             (*vp)[i]._nz = fz;


         }

    }
     _glWidget->on_rawpnts_updated();


}

void CGLAlignmentCloudDlg::onApplyBtn()
{
    onPreviewBtn();

    fix_cloud_normals();


    close();
}


