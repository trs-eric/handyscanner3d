#ifndef MESSAGES_H
#define MESSAGES_H

#include <QObject>
#include <QString>

const QString msg_newScene = QObject::tr("Create a new scene");

const QString err_WrongRUBVer = QObject::tr("Wrong version of RUB file.");

const QString err_cannotLoadFile = QObject::tr("Cannot load requested file");

const QString err_cannotSaveFile = QObject::tr("Cannot save requested file");

const QString msg_PointsBeforeAfter = QObject::tr("Points before : %s / Points after : %s");

const QString wrn_NothingOptim = QObject::tr("There are no points to optimize.");

const QString wrn_NothingNormal = QObject::tr("There are no optimum points.");

const QString wrn_NothingMesh = QObject::tr("There are no mesh normals to compute closed mesh.");

const QString wrn_FailedMesh = QObject::tr("For given parameters a mesh cannot be constructed.");

const QString wrn_CannotExportCObject = QObject::tr("Cannot export anything: current object is undefined.");

const QString wrn_CannotExportMesh = QObject::tr("Cannot find surface to be exported.");

const QString wrn_CannotExportOptimXYZ = QObject::tr("Cannot find optim points to be exported.");

const QString wrn_CannotExportRawXYZ = QObject::tr("Cannot find raw points to be exported.");

const QString wrn_DiscardSceneChanges = QObject::tr("Discard current changes in the scene?");

const QString err_DeviceConnection = QObject::tr("Device connection failure");

const QString err_computeProcImage = QObject::tr("Compute processed image first.");

const QString err_loadRawImage = QObject::tr("Load raw image first.");

const QString wrn_RemoveScan = QObject::tr("Are you sure this scan has to be deleted?");

const QString wrn_NoRawPoints = QObject::tr("There are no raw points.");

const QString wrn_NoMesh = QObject::tr("There are no mesh in the selected scan.");

const QString wrn_NoFusedMesh = QObject::tr("There is no fused mesh to work with.");


const QString wrn_TexDel = QObject::tr("Pressing this button removes mesh texture.");

const QString wrn_NoSPCL = QObject::tr("Cannot start since sparce PCL was not constructed properly.");

const QString wrn_NoDPCL = QObject::tr("Cannot start since dense PCL was not constructed properly.");

const QString sbInfo = QObject::tr("%1k face");

const QString wrn_FailedFindKMatrix = QObject::tr("Cannot construct proper camera matrix. Texturing might fail.");

const QString wrn_NoPMAP = QObject::tr("Cannot start since polar map was not constructed properly.");


const QString wrn_CannotExportFuse = QObject::tr("Cannot export: there is no fused mesh to work with.");

const QString err_RenderNotSupported = QObject::tr("Your graphic card does not support the render features.");

const QString wrn_WrongWebResMode = QObject::tr("Your camera does not support selected resolution.");

const QString wrn_SerialPortCannotOpen = QObject::tr("Cannot open serial port");

const QString wrn_DeviceCannotOpen = QObject::tr("Cannot access externla device");





#endif // MESSAGES_H
