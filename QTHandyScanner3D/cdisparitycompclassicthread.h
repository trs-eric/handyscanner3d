#ifndef CDISPARITYCOMPCLASSICTHREAD_H
#define CDISPARITYCOMPCLASSICTHREAD_H

#include <QThread>
#include <QMutex>

#include "disparity/disparity_utils.h"
#include "graphicutils.h"
#include "ccamerapose.h"

#include <opencv2/features2d/features2d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/contrib/contrib.hpp>
#include "opencv2/nonfree/features2d.hpp"

class CDisparityCompClassicThread : public QThread
{
    Q_OBJECT
public:
    explicit CDisparityCompClassicThread(QObject *parent = 0);
    ~CDisparityCompClassicThread();

    void start_process(QImage * left_im, QImage * right_im, int num_disp, int lbias,
                       int SADWindowSize, int preFilterCap,
                       int uniquenessRatio, int speckleWindowSize,
                       int speckleRange , int disp12MaxDiff ,
                       bool fullDP, int P1, int P2 );

    bool is_abort(){return _abort;}
    void emit_send_back(int i);

signals:
    void send_back(const int val);
    void send_result(std::vector<C3DPoint> * cpoints);
    void send_result(CGLObject * );


protected:
    void run_old();
    void run(); // homo
    void run_icp(); // icp


    //uax function for current run
   std::vector<C3DPoint> * construct_pcl_from_depthmap(cv::Mat & img_L1,cv::Mat & disp_L1,
                cv::Mat & depth, cv::Mat & R, cv::Mat & T);
    cv::Mat get_back_rotation_matrix();
    cv::Mat get_forward_rotation_matrix();
    double compute_av_repo_error(cv::Mat & P2,
                                 cv::Mat & K,
                                 std::vector<cv::KeyPoint> & pt_all_1L,
                                 std::vector<cv::KeyPoint> & pt_all_2L,
                                 std::vector<cv::DMatch> & matches,
                                  cv::Mat& img3d1);
    double find_camP2_Tz_iter(int iter_num,
                              double Tx,
                              double Ty,
                              double initTz,
                              cv::Mat & K,
                              std::vector<cv::KeyPoint> & pt_all_1L,
                              std::vector<cv::KeyPoint> & pt_all_2L,
                              std::vector<cv::DMatch> & matches,
                               cv::Mat& img3d1);


public slots:
     void stop_process();

private:
    bool    _abort;
    QMutex  _mutex;

    QImage * _left_im;
    QImage * _right_im;
    int _num_disp;
    int _lbias;
    int _SADWindowSize;
    int _preFilterCap;
    int _uniquenessRatio;
    int _speckleWindowSize;
    int _speckleRange;
    int _disp12MaxDiff;
    bool _fullDP;
    int _P1;
    int _P2;

    void clean_dispmap_sides(int xs, int ys, cv::Mat & disp);

    std::vector<C3DPoint> * compute_pcloud(cv::StereoSGBM & sbm,
                                                     cv::Mat & Q,
                                                     QImage * left_im,
                                                     QImage * right_im,
                                                     CArray2D<int> & left_map0,
                                                     cv::Mat & disp,
                                                     int idx);


    void compute_correspondance(cv::SurfFeatureDetector & detector,
                                cv::SurfDescriptorExtractor & extractor,
                                cv::FlannBasedMatcher & matcher, QImage * left_im_0,
                                                     QImage * left_im_1,
                                std::vector< cv::DMatch > & good_matches,
                                std::vector<cv::KeyPoint> & keypoints_1,
                                std::vector<cv::KeyPoint> & keypoints_2);

   void fix_affine_transform2(CArray2D<int> & left_map0,
                                                           CArray2D<int> & left_map1,
                                                           std::vector<C3DPoint> *  final_res_0,
                                                           std::vector<C3DPoint> *  final_res_1,
                                                           std::vector< cv::DMatch > & good_matches,
                                                           std::vector<cv::KeyPoint> & keypoints_1,
                                                           std::vector<cv::KeyPoint> & keypoints_2,
                                                            cv::Mat & disp0,
                                                            cv::Mat & disp1);
   void fix_affine_transform(CArray2D<int> & left_map0,
                                                           CArray2D<int> & left_map1,
                                                           std::vector<C3DPoint> *  final_res_0,
                                                           std::vector<C3DPoint> *  final_res_1,
                                                           std::vector< cv::DMatch > & good_matches,
                                                           std::vector<cv::KeyPoint> & keypoints_1,
                                                           std::vector<cv::KeyPoint> & keypoints_2,
                                                            cv::Mat & disp0,
                                                            cv::Mat & disp1);

};

#endif // CDISPARITYCOMPCLASSICTHREAD_H
