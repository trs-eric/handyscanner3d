#ifndef CLAPLACIANSMDLG_H
#define CLAPLACIANSMDLG_H

#include <QDialog>
#include <QThread>
#include <QMessageBox>

class CGLWidget;
class CTriangularMesh;
class CLaplacianSmoothThread;
class CMeshDumpThread;

// TODO Make it external ?
class CMeshDumpThread;
namespace Ui {
class CLaplacianSmDlg;
}

class CLaplacianSmDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CLaplacianSmDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CLaplacianSmDlg();

protected:
    void showEvent(QShowEvent * event);
    CTriangularMesh * restore_mesh();

private:
    Ui::CLaplacianSmDlg *ui;
    CGLWidget *_glWidget;


    bool _in_process;
    bool _on_apply;
    bool _is_result_valid;

    CLaplacianSmoothThread * _thread;
    QMessageBox * _info;
    CMeshDumpThread * _ithread;

private slots:
    void onCancelBtn();
    void onPreviewBtn();
    void onApplyBtn();
    void onFinishDumping();

    void onMakeProgressStep(int val);
    void onCompleteProcessing(CTriangularMesh * );
    void onStopProcessing();

    void close_dialog();

};

#endif // CLAPLACIANSMDLG_H
