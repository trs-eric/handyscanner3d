#include "cconvexmeshconstructiondlg.h"
#include "ui_cconvexmeshconstructiondlg.h"

#include "cconvexmeshconstructionthread.h"

#include "cglwidget.h"
#include "messages.h"

#include <QMessageBox>

CConvexMeshConstructionDlg::CConvexMeshConstructionDlg(CGLWidget * glWidget, QWidget *parent) :
    _glWidget(glWidget), QDialog(parent),
    ui(new Ui::CConvexMeshConstructionDlg)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);

    ui->progressBar->setMaximum(100);

    ui->vertDistThres->setText(QString::number(2));
    ui->horizDistTreas->setText(QString::number(2));
    ui->chckCloseShape->setChecked(true);
    ui->chckGenTex->setChecked(true);

    connect(ui->previewBtn, SIGNAL(clicked()), this, SLOT(onPreviewBtn()));
    connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));


    _thread = new CConvexMeshConstructionThread;

    // user pressed cancel button
    connect(ui->cancelBtn,  SIGNAL(clicked()), _thread, SLOT(stop_process()));
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

    // user closed dialg
    connect(this,  SIGNAL(finished(int)), _thread, SLOT(stop_process()));

    // working with a thread
    connect(_thread, SIGNAL(send_back(int)), this, SLOT(onMakeProgressStep(int)));
    connect(_thread, SIGNAL(send_result(CTriangularMesh*)), this, SLOT(onCompleteProcessing(CTriangularMesh*)));

   // connect(_thread, SIGNAL(send_result(CNormalCloud*)), _glWidget, SLOT(on_normal_pnts_computed(CNormalCloud*)));
    connect(_thread, SIGNAL(send_result(CTriangularMesh*)), _glWidget, SLOT(on_mesh_computed(CTriangularMesh*)));

    connect(_thread, SIGNAL(finished()), this, SLOT(onStopProcessing()));
}

void CConvexMeshConstructionDlg::onMakeProgressStep(int val)
{
    ui->progressBar->setValue(val);
}

void CConvexMeshConstructionDlg::onCompleteProcessing(CTriangularMesh * m)
{
    if(m == NULL)
    {
        QMessageBox::warning(this, tr("Warning"),wrn_FailedMesh);
        return;
    }
    if(_on_apply)
    {
        close_dialog();
        return;
    }

    _is_result_valid = true;
    ui->progressBar->setValue(100);

}

void CConvexMeshConstructionDlg::onStopProcessing()
{
    ui->applyBtn->setEnabled(true);
    ui->previewBtn->setEnabled(true);
    ui->vertDistThres->setEnabled(true);
    ui->horizDistTreas->setEnabled(true);
    ui->chckCloseShape->setEnabled(true);
    ui->chckGenTex->setEnabled(true);

    _in_process = false;
}

void CConvexMeshConstructionDlg::close_dialog()
{
    _on_apply = false;
    close();
}


void CConvexMeshConstructionDlg::onCancelBtn()
{
    if(!_in_process)
        close_dialog();

    _in_process = false;
    _on_apply = false;
}

void CConvexMeshConstructionDlg::onPreviewBtn()
{
    CPointCloud * pcld = _glWidget->get_raw_points_of_current_object();
    if(pcld  == NULL)
    {
       QMessageBox::warning(this, tr("Warning"),wrn_NoRawPoints);
       return;
    }

    ui->applyBtn    ->setEnabled(false);
    ui->previewBtn  ->setEnabled(false);
    ui->vertDistThres->setEnabled(false);
    ui->horizDistTreas->setEnabled(false);
    ui->chckCloseShape->setEnabled(false);
    ui->chckGenTex->setEnabled(false);

    _in_process = true;
    _thread->start_process(pcld, ui->vertDistThres->text().toFloat(),
                           ui->horizDistTreas->text().toFloat(),
                           ui->chckCloseShape->isChecked(),
                           ui->chckGenTex->isChecked());
}

void CConvexMeshConstructionDlg::onApplyBtn()
{
    _on_apply = true;
    if(!_is_result_valid)
        onPreviewBtn();
    else
        close_dialog();
}

void CConvexMeshConstructionDlg::showEvent(QShowEvent * event)
{
    ui->progressBar->setValue(0);
    _is_result_valid    = false;
    _on_apply           = false;
    _in_process         = false;
}


/*
void CConvexMeshConstructionDlg::process_2_scan_lines_bckp()
{
    CPointCloud * pcld = _glWidget->get_raw_poins_of_current_object();

     if(pcld != NULL)
     {
         std::cout << "raw points are found" << std::endl;

         std::vector<C3DPoint> pnts = pcld->get_points_cpy();
         std::vector<int> pnts_neighbours(pnts.size());


         ///////////// find left and right scan-lines
         int mid_idx = 0; // index of the second scanline
         int cur_angle = pnts[0]._angle;
         std::vector<C3DPoint>::iterator it =  pnts.begin();
         for( ; it != pnts.end(); it++ )
         {
             if( it->_angle == cur_angle)
                 mid_idx++;

             // if(it->_angle == 0)
             /{
               //it-> _x = 5;
               //it -> _z = 0;
             //}
             //else
             //{
                // it->  _x = 0;
                // it -> _z = 0;
             //}
            // std::cout << it->_x << " " << it->_y << " " << it->_z << " " << it->_angle  << std::endl;

         }
         //std::cout << pnts.size() << " " << mid_idx << " "<< pnts[0]._x << " " << pnts[0]._y  << std::endl;
         //std::cout << pnts[mid_idx]._x << " " << pnts[mid_idx]._y  << std::endl;

         ////////////////////////////////////////////////

         ///////////// compute neighbour pairs
         int pnts_num = pnts.size();
         for( int i = 0; i < mid_idx; i++ )
         {
             C3DPoint lpnt = pnts[i];

             int ridx  = mid_idx;
             C3DPoint rpnt = pnts[ridx];
             float pdist = compute_sq_dist(lpnt, rpnt);

             for(int n = mid_idx + 1; n < pnts_num; n++)
             {
                 C3DPoint rpnt = pnts[n];
                 float cdist = compute_sq_dist(lpnt, rpnt);

                 if(cdist < pdist)
                 {
                     ridx = n;
                     pdist = cdist;
                 }
             }
             pnts_neighbours[i] = ridx;
             ///std::cout << i << ">" << ridx << std::endl;
         }



         for( int j = mid_idx; j < pnts_num; j++ )
         {
             C3DPoint rpnt = pnts[j];

             int lidx  = 0;
             C3DPoint lpnt = pnts[lidx];
             float pdist = compute_sq_dist(lpnt, rpnt);
             for(int n = 0; n < mid_idx; n++)
             {
                 C3DPoint lpnt = pnts[n];
                 float cdist = compute_sq_dist(lpnt, rpnt);

                 if(cdist < pdist)
                 {
                     lidx = n;
                     pdist = cdist;
                  }
              }
              pnts_neighbours[j] = lidx;
              ///std::cout << j << ">" << lidx << std::endl;
         }
         ////////////////////////////////////////////////

         std::vector<int> triangles;
         bool cont = true;
         int left_marker = 0;
         int last_left_marker = mid_idx-1;
         int right_marker = mid_idx;
         int last_right_marker = pnts_num - 1;
         while(cont)
         {
             if( (left_marker  == last_left_marker) ||
                 (right_marker == last_right_marker)) //?
                             break;

             int next_left_marker  = (left_marker + 1 <= last_left_marker)?(left_marker + 1):last_left_marker;
             int next_right_marker = (right_marker + 1 <= last_right_marker)?(right_marker + 1):last_right_marker;

             int new_left_marker = next_left_marker;
             bool lfound = false;
             while(new_left_marker < last_left_marker)
             {
                 if(pnts_neighbours[new_left_marker] >=next_right_marker)
                 {
                     lfound = true;
                     break;
                 }
                 new_left_marker++;
             }

             int new_right_marker = next_right_marker;
             bool rfound = false;
             while(new_right_marker < last_right_marker)
             {
                 if(pnts_neighbours[new_right_marker] >=next_left_marker)
                 {
                     rfound = true;
                     break;
                 }
                 new_right_marker++;
             }

             if(rfound && lfound)
             {

                 if( ( abs(pnts_neighbours[new_left_marker] - next_right_marker)<
                       abs(pnts_neighbours[new_right_marker]- next_left_marker))
                   )
                 {
                    // new_left_marker  = new_left_marker;
                     new_right_marker = pnts_neighbours[new_left_marker] ;
                 }
                 else
                 {
                     new_left_marker  = pnts_neighbours[new_right_marker];
                     //new_right_marker = new_right_marker;
                 }
             }
             else
             {
                 new_left_marker  = last_left_marker;
                 new_right_marker = last_right_marker;
             }

             std::cout << " L0 " << left_marker      <<
                          " L1 " << new_left_marker  << " "<< compute_sq_dist(pnts[left_marker], pnts[new_left_marker]) <<
                          " R0 " << right_marker     <<
                          " R1 " << new_right_marker << " "<< compute_sq_dist(pnts[right_marker], pnts[new_right_marker]) << std::endl;


             add_triangles(left_marker, new_left_marker,
                                right_marker,  new_right_marker,
                                pnts, pnts_neighbours, triangles);

             left_marker  = new_left_marker;
             right_marker = new_right_marker;
         }
         std::cout << "done"<< std::endl;



     CTriMesh * rmesh = new CTriMesh ;
     rmesh->_points = pnts;

     std::vector<int>::iterator ittr = triangles.begin();
     for ( ; ittr != triangles.end(); ittr++)
     {
         C3DPointIdx tri;
         tri.pnt_index[0] = *ittr; ittr++;
         tri.pnt_index[1] = *ittr; ittr++;
         tri.pnt_index[2] = *ittr;

         rmesh->_triangles.push_back(tri);
     }

     ///////// update mesh normals per face
     std::vector<C3DPointIdx>::iterator itt = rmesh->_triangles.begin();
     for(int fidx = 0 ; itt != rmesh->_triangles.end(); itt++, fidx++)
     {
         rmesh->_points[ itt->pnt_index[0] ]._faceidx.push_back(fidx);
         C3DPoint pnt0 = rmesh->_points[ itt->pnt_index[0] ];

         rmesh->_points[ itt->pnt_index[1] ]._faceidx.push_back(fidx);
         C3DPoint pnt1 = rmesh->_points[ itt->pnt_index[1] ];

         rmesh->_points[ itt->pnt_index[2] ]._faceidx.push_back(fidx);
         C3DPoint pnt2 = rmesh->_points[ itt->pnt_index[2] ];

         itt->tri_normal = compute_normal( pnt2, pnt1, pnt0);
     }

     //////// update mesh normals per vertec
     std::vector<C3DPoint>::iterator itv = rmesh->_points.begin();
     for(; itv != rmesh->_points.end(); itv++)
     {
         itv->_nx =0;
         itv->_ny =0;
         itv->_nz =0;

         std::vector<int>::iterator fit = itv->_faceidx.begin();
         for( ; fit != itv->_faceidx.end(); fit++)
         {

             itv->_nx += rmesh->_triangles[(*fit)].tri_normal._x;
             itv->_ny += rmesh->_triangles[(*fit)].tri_normal._y;
             itv->_nz += rmesh->_triangles[(*fit)].tri_normal._z;
         }
         normalize3(itv->_nx, itv->_ny, itv->_nz);
     }

    CTriangularMesh * mmesh = new CTriangularMesh;
     mmesh->set_mesh(rmesh);

     _glWidget->on_mesh_computed(mmesh);

     std::cout << "mesh is done"<< std::endl;



     }
     else
         std::cout << "no raw points" << std::endl;
}
*/
//void CConvexMeshConstructionDlg::onComputeBtn()
//{
//    process_2_scan_lines();
//}

CConvexMeshConstructionDlg::~CConvexMeshConstructionDlg()
{
    delete ui;
    delete _thread;
}
