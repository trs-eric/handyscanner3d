#include "claserframepreviewdlg.h"
#include "ui_claserframepreviewdlg.h"
#include <QVBoxLayout>
#include <QMessageBox>
#include <QFileDialog>
#include <QScrollBar>
#include <QGLWidget>
#include "settings/psettings.h"
#include "img2space/edgedetection.h"
#include <fstream>

CLaserFramePreviewDlg::CLaserFramePreviewDlg(QWidget *parent) :  _scale_factor(0.5),
    QDialog(parent), _bg(NULL), _img(NULL),
    ui(new Ui::CLaserFramePreviewDlg)
{
    ui->setupUi(this);

    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint | Qt::WindowMinimizeButtonHint |
                   Qt::WindowMaximizeButtonHint );

    this->setModal(false);

    _tex_label = new QLabel;
    _tex_label->setBackgroundRole(QPalette::Base);
    _tex_label->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    _tex_label->setScaledContents(true);

    ui->scrollArea->setBackgroundRole(QPalette::Dark);
    ui->scrollArea->setWidget(_tex_label);
    ui->scrollArea->setWidgetResizable(false);

    QHBoxLayout *dialog_layout = new QHBoxLayout(this);
    dialog_layout->setMargin(0);
    dialog_layout->setSpacing(0);
    dialog_layout->addWidget(ui->scrollArea);
    setLayout(dialog_layout);


    connect(ui->btnHundr, SIGNAL(clicked()), this, SLOT(set_zoom_100()));
    connect(ui->btnHalf,  SIGNAL(clicked()), this, SLOT(set_zoom_50()));
    connect(ui->btnQaut, SIGNAL(clicked()), this, SLOT(set_zoom_25()));


    ui->slidOpas->setValue(100);
    connect(ui->slidOpas, SIGNAL(sliderReleased()), this, SLOT(change_opacity()));

    connect(ui->chkDif, SIGNAL(clicked()), this, SLOT(show_diff()));

}

void CLaserFramePreviewDlg::show_diff()
{
    update_image_from_video(_img, false, _bg, _isdiff,
                            _prev_draw_edges, _prev_edge_correct, _use_meta, _meta_angle);
}

void CLaserFramePreviewDlg::change_opacity()
{
    update_image_from_video(_img, false, _bg, _isdiff,
                            _prev_draw_edges, _prev_edge_correct, _use_meta, _meta_angle);
}

void CLaserFramePreviewDlg::set_zoom_100()
{
    _scale_factor = 1.0;
    update_image_from_video(_img, false, _bg, _isdiff,
                            _prev_draw_edges, _prev_edge_correct, _use_meta, _meta_angle);
}

void CLaserFramePreviewDlg::set_zoom_50()
{
    _scale_factor = 0.5;
    update_image_from_video(_img, false, _bg, _isdiff,
                            _prev_draw_edges, _prev_edge_correct, _use_meta, _meta_angle);
}

void CLaserFramePreviewDlg::set_zoom_25()
{
    _scale_factor = 0.75;
    update_image_from_video(_img, false, _bg, _isdiff,
                            _prev_draw_edges, _prev_edge_correct, _use_meta, _meta_angle);
}

CLaserFramePreviewDlg::~CLaserFramePreviewDlg()
{
    delete _tex_label;
    if(_img != NULL)
         delete _img;

    delete ui;
}


void CLaserFramePreviewDlg::update_image_from_video(QImage * img, bool clean_img,
    QImage * bg, bool is_diff, bool draw_edges, bool edge_correct, bool usemeta, float meta_angle )
{
    _isdiff = is_diff;
    _use_meta = usemeta;
    _meta_angle = meta_angle;
   _prev_draw_edges   = draw_edges;
   _prev_edge_correct = edge_correct;

    if(clean_img)
        if(_img != NULL)
        {
            delete _img;
        }
    _img = img;
    _bg = bg;


    if(_img->width() == 0) return;

    int txs = _img->width();
    int tys = _img->height();

    QImage texture(txs, tys, QImage::Format_ARGB32);
    texture.fill(QColor(0,0,0,0));

   if(_use_meta)
    {
        QString txt = QString("Angle: %1").arg(QString::number(_meta_angle));
        ui->lblMetaAngle->setText(txt);
        ui->lblMetaAngle->setVisible(true);
    }
    else
        ui->lblMetaAngle->setVisible(false);


    QImage diff(*_img);
    if(!is_diff)
         difference_image_fast_supp_red(&diff, _bg, CGlobalSettings::_ignore_read_thresh);

    if(draw_edges)
    {
        std::vector<CIPixel> pix = get_edge_points_lines_hills_1D(&diff);
        if(edge_correct)
            pix = do_laser_points_correction(pix);


        for( std::vector<CIPixel>::iterator it = pix.begin(); it != pix.end(); it++)
        {
            QRgb p = qRgba(0, 255, 0,255);
            texture.setPixel(it->_u, it->_v, p);

            if(_scale_factor < 1.0)
            {
                texture.setPixel(it->_u+1., it->_v, p);
                texture.setPixel(it->_u-1., it->_v, p);
            }
       }
    } // draw_edges

    QPixmap bakcg_map = (ui->chkDif->isChecked())? QPixmap::fromImage(diff): QPixmap::fromImage(*_img);
    QPixmap forwd_map = QPixmap::fromImage(texture);

    QPixmap finalPixmap(bakcg_map.size());
    finalPixmap.fill(Qt::transparent);

    QPainter pd(&finalPixmap);
    pd.setBackgroundMode(Qt::TransparentMode);
    pd.setBackground(QBrush(Qt::transparent));
    pd.eraseRect(bakcg_map.rect());

    float opacity = float(ui->slidOpas->value())/100.0;
    pd.setOpacity(opacity);
    pd.drawPixmap(0, 0, bakcg_map);
    pd.setOpacity(1);
    pd.drawPixmap(0, 0, forwd_map);
    pd.end();

    _tex_label->setPixmap(finalPixmap);
    _tex_label->resize(_scale_factor * _tex_label->pixmap()->size());

}

void CLaserFramePreviewDlg::adjust_scroll_bar(QScrollBar *scrollBar, float factor)
{
    scrollBar->setValue(int(factor * scrollBar->value()
                            + ((factor - 1) * scrollBar->pageStep()/2)));
}

void CLaserFramePreviewDlg::clean_data()
{
   // _corners.clear();
}

void CLaserFramePreviewDlg::hideEvent(QHideEvent *)
{
    emit close_preview();
}


//////////////////////////////////////////////
//////////////////////////////////////////////
//////////////////////////////////////////////

void CLaserFramePreviewDlg::update_image(QString & image_name, QString & bckg_name,
                                         QString & bckg_name2, bool draw_edges, bool edge_correct, bool draw_corners, bool is_diff,
                                         bool has_marks, bool is_round_marks, int marks_x,
                                                                    int marks_y, int corrwndsz,
                                         bool usemeta, float meta_angle, int laser_pos)
{
    scale_image(image_name, bckg_name, bckg_name2, draw_edges, edge_correct, draw_corners, is_diff,
                  has_marks, is_round_marks, marks_x, marks_y, corrwndsz, usemeta, meta_angle, laser_pos );

    update();

}

void CLaserFramePreviewDlg::draw_mark( QImage & texture, float cx, float cy)
{
    int cr_len = 10;

    for( int ix = -cr_len ; ix <= cr_len; ix++)
    {
        QRgb p = qRgb(0, 255, 0);
        texture.setPixel(ix+cx, cy, p);

        if(_scale_factor < 1.0)
        {
            texture.setPixel(ix+cx, cy-1, p);
            texture.setPixel(ix+cx, cy+1, p);
        }
    }

    for( int iy = -cr_len ; iy <= cr_len; iy++)
    {
        QRgb p = qRgb(0, 255, 0);
        texture.setPixel(cx, iy+cy, p);
        if(_scale_factor < 1.0)
        {
            texture.setPixel(cx-1, iy+cy, p);
            texture.setPixel(cx+1, iy+cy, p);
        }
    }

}



void CLaserFramePreviewDlg::scale_image(QString & image_name, QString & bckg_name, QString & bckg_name2,
                                        bool draw_edges, bool edge_correct,
                                        bool draw_corners, bool is_diff,
                                        bool has_marks, bool is_round_marks, int marks_x, int marks_y, int corrwndsz,
                                        bool usemeta, float meta_angle, int laser_pos)
{

    _prev_image_name   = image_name;
    _prev_bckg_name    = bckg_name;
    _prev_bckg_name2   = bckg_name2;
    _prev_draw_edges   = draw_edges;
    _prev_draw_corners = draw_corners;
    _isdiff = is_diff;
    _prev_has_marks = has_marks;
    _prev_is_round_marks = is_round_marks;
    _prev_marks_x = marks_x;
    _prev_marks_y = marks_y;
    _prev_edge_correct = edge_correct;
    _prev_corrwndsz = corrwndsz;
    _use_meta = usemeta;
    _meta_angle = meta_angle;
    _prev_lookfor_laser_onboard_ymax = laser_pos;

    QImage input_img(image_name);

    if(input_img.width() == 0) return;

    QImage diff(input_img);
    diff = diff.convertToFormat(QImage::Format_RGB888);


    int txs = input_img.width();
    int tys = input_img.height();

    QImage texture(txs, tys, QImage::Format_ARGB32);
    texture.fill(QColor(0,0,0,0));

    //QPixmap canvasPixmap(selpixmap.size());
    ///canvasPixmap.fill(Qt::transparent);

    if(_use_meta)
    {
        QString txt = QString("Angle: %1").arg(QString::number(_meta_angle));
        ui->lblMetaAngle->setText(txt);
        ui->lblMetaAngle->setVisible(true);
    }
    else
        ui->lblMetaAngle->setVisible(false);

    QImage bckg(bckg_name);
    bckg = bckg.convertToFormat(QImage::Format_RGB888);

    if(!is_diff)
    {

         difference_image_fast_supp_red(&diff, &bckg, CGlobalSettings::_ignore_read_thresh);


    }
    if(draw_edges)
    {


        std::vector<CIPixel> pix = get_edge_points_lines_hills_1D(&diff);

        if(edge_correct)
            pix = do_laser_points_correction(pix);

        ///std::ofstream pointsf;
        ///pointsf.open ("points.dat");

        std::vector<CIPixel>::iterator it = pix.begin();
        for( ; it != pix.end(); it++)
        {
            QRgb p = qRgba(0, 255, 0,255);
            texture.setPixel(it->_u, it->_v, p);


            if(_scale_factor < 1.0)
            {
                texture.setPixel(it->_u+1., it->_v, p);
                texture.setPixel(it->_u-1., it->_v, p);
            }

           /// pointsf << it->_v <<  " " << it->_u << std::endl;
       }

      /// pointsf.close();


    }

    if(draw_corners && has_marks)
    {
        std::cout << _corners.size()  << std::endl;

        if(_corners.size() == 0)
        {

            /*std::cout <<is_round_marks << " " << marks_x << " " << marks_y << std::endl;
            cv::Size patternsize(marks_x, marks_y);
            QImage bckg2(bckg_name2);

            int xs = bckg2.width();
            int ys = bckg2.height();
            cv::Mat tmp2(ys, xs, CV_8UC4, (uchar*)bckg2.bits(),bckg2.bytesPerLine());
            cv::Mat res = cv::Mat(tmp2.rows, tmp2.cols, CV_8UC3 );
            int from_to[] = { 0,0,  1,1,  2,2 };
            cv::mixChannels( &tmp2, 1, &res, 1, from_to, 3 );
            cv::Mat inimg = cv::Mat(res, cv::Rect(0, 0, xs, ys));

            bool found = false;
             if(is_round_marks)
                found = cv::findCirclesGrid(inimg, patternsize, _corners);
             else
                 found = cv::findChessboardCorners(inimg, patternsize, _corners);

             std::cout << found  << std::endl;*/
             ////////////////
            /* cv::Mat mBckg = imread( bckg_name.toStdString().c_str(), cv::IMREAD_GRAYSCALE );
             cv::SimpleBlobDetector::Params params;
             // Change thresholds
             params.minThreshold = 10;
             params.maxThreshold =200;
             //params.minDistBetweenBlobs =.001;
             // Filter by Area.
             params.filterByArea = true;
             params.minArea = 100;
             // Filter by Circularity
             params.filterByCircularity = true;
             params.minCircularity = 0.7;
             // Filter by Convexity
             params.filterByConvexity = true;
             params.minConvexity = 0.87;
             // Filter by Inertia
             params.filterByInertia = true;
             params.minInertiaRatio = 0.01;
             cv::SimpleBlobDetector detector(params);
             std::vector<cv::KeyPoint> keypoints;
             detector.detect( mBckg, keypoints);

             std::vector<cv::KeyPoint>::iterator it = keypoints.begin();
             for( ; it != keypoints.end(); it++ )
             {
                _corners.push_back(it->pt);
             }
             */

             _corners = findGridMarkers(bckg_name);


        }

        std::vector<cv::Point2f>::iterator it = _corners.begin();
        for( int i = 0; it != _corners.end(); it++, i++ )
        {
           // std::cout << i << " " << it->x << " " << it->y << std::endl;
            draw_mark(texture, it->x , it->y);
        }

        //////////////////


        for(int x = 0 ; x < txs; x++)
        {
            QRgb p = qRgba(0, 0, 255, 255);
            texture.setPixel(x, _prev_lookfor_laser_onboard_ymax, p);

            texture.setPixel(x, _prev_lookfor_laser_onboard_ymax+1, p);
        }


    }


    QPixmap bakcg_map = (ui->chkDif->isChecked())? QPixmap::fromImage(diff): QPixmap::fromImage(input_img);
    QPixmap forwd_map = QPixmap::fromImage(texture);

    QPixmap finalPixmap(bakcg_map.size());
    finalPixmap.fill(Qt::transparent);

    QPainter pd(&finalPixmap);
    pd.setBackgroundMode(Qt::TransparentMode);
    pd.setBackground(QBrush(Qt::transparent));
    pd.eraseRect(bakcg_map.rect());

    float opacity = float(ui->slidOpas->value())/100.0;
    pd.setOpacity(opacity);
    pd.drawPixmap(0, 0, bakcg_map);
    pd.setOpacity(1);
    pd.drawPixmap(0, 0, forwd_map);
    pd.end();

    _tex_label->setPixmap(finalPixmap/*QPixmap::fromImage(texture)*/);
    _tex_label->resize(_scale_factor * _tex_label->pixmap()->size());
  //  adjust_scroll_bar(ui->scrollArea->horizontalScrollBar(), _scale_factor);
  //  adjust_scroll_bar(ui->scrollArea->verticalScrollBar(),   _scale_factor);
  //  update();

}
