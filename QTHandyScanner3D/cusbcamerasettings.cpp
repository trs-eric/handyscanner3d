#include "cusbcamerasettings.h"
#include "ui_cusbcamerasettings.h"

#include <QMessageBox>
#include "messages.h"
#include "settings/psettings.h"


#define CV_WEBCAM_WB_U 17 //CV_CAP_PROP_WHITE_BALANCE_U
#define CV_WEBCAM_WB_V 26 //CV_CAP_PROP_WHITE_BALANCE_V


CUSBCameraSettings::CUSBCameraSettings(cv::VideoCapture * cam,  QWidget *parent) :
    QDialog(parent), _current_cam(cam),  _val_exposure(10^6),
   _val_focus(10^6), _val_brightness(10^6), _val_contrast(10^6), _val_gamma(10^6), _val_gain(10^6),
   _val_sharp(10^6), _val_hue(10^6), _val_satu(10^6), _val_wb_blue(10^6), _val_wb_red(10^6),
    ui(new Ui::CUSBCameraSettings)
{
    ui->setupUi(this);


    connect(ui->btnClose, SIGNAL(clicked()), this, SLOT(close()));


    _resol_list.push_back(0);
    _resol_list.push_back(0);
    ui->cmbResol->addItem("Default: ");

    _resol_list.push_back(640);
    _resol_list.push_back(480);
    ui->cmbResol->addItem("LR: 640x480");

    _resol_list.push_back(1280);
    _resol_list.push_back(960);
    ui->cmbResol->addItem("HR: 1280x960");

    _resol_list.push_back(1280);
    _resol_list.push_back(1024);
    ui->cmbResol->addItem("SXGA: 1280x1024");

    _resol_list.push_back(1600);
    _resol_list.push_back(1200);
    ui->cmbResol->addItem("UXGA : 1600x1200");

    _resol_list.push_back(2048);
    _resol_list.push_back(1536);
    ui->cmbResol->addItem("QXGE: 2048x1536");

    _resol_list.push_back(2592);
    _resol_list.push_back(1944);
    ui->cmbResol->addItem("QSXGA: 2592x1944");

    _resol_list.push_back(1920);
    _resol_list.push_back(1080);
    ui->cmbResol->addItem("TR1: 1920x1080");

    _resol_list.push_back(2304);
    _resol_list.push_back(1536);
    ui->cmbResol->addItem("TR2: 2304x1536");


    _resol_idx =0;
    int def_xs = _current_cam->get(CV_CAP_PROP_FRAME_WIDTH);
    int def_ys = _current_cam->get(CV_CAP_PROP_FRAME_HEIGHT);
    _resol_list[0] = def_xs;
    _resol_list[1] = def_ys;
    QString def_res = QString("Default : %1x%2").arg(QString::number(def_xs)).arg(QString::number(def_ys));
    ui->cmbResol->setItemText(_resol_idx, def_res);

    connect(ui->cmbResol, SIGNAL(currentIndexChanged(int)), this, SLOT(resolution_changed(int)));
    ui->cmbResol->setCurrentIndex(2);

    ui->chkAutoExpos->setVisible(false);
    connect(ui->sldExposure, SIGNAL(valueChanged(int)), this, SLOT(change_exposure()));
    ui->sldExposure->setValue(CGlobalSettings::_webc_exposeure); // def
    QString msg = QString("%1 ").arg(QString::number(CGlobalSettings::_webc_exposeure));
    ui->lblExposure->setText(msg);


    connect(ui->sldBrightness, SIGNAL(valueChanged(int)), this, SLOT(change_brightness()));
    ui->sldBrightness->setValue(CGlobalSettings::_webc_brightness); // def

    connect(ui->sldContrast, SIGNAL(valueChanged(int)), this, SLOT(change_contrast()));
    ui->sldContrast->setValue(CGlobalSettings::_webc_contrast); // def

    connect(ui->sldGamma, SIGNAL(valueChanged(int)), this, SLOT(change_gamma()));
    ui->sldGamma->setValue(CGlobalSettings::_webc_gamma); // def

    connect(ui->sldGain, SIGNAL(valueChanged(int)), this, SLOT(change_gain()));
    ui->sldGain->setValue(CGlobalSettings::_webc_gain);

    connect(ui->sldSharpness, SIGNAL(valueChanged(int)), this, SLOT(change_sharpness()));
    ui->sldSharpness->setValue(CGlobalSettings::_webc_shap);

    connect(ui->sldHue, SIGNAL(valueChanged(int)), this, SLOT(change_hue()));
    ui->sldHue->setValue(CGlobalSettings::_webc_hue);

    connect(ui->sldSaturation, SIGNAL(valueChanged(int)), this, SLOT(change_saturation()));
    ui->sldSaturation->setValue(CGlobalSettings::_webc_satu);


    /// ui->chkAutoWB->setVisible(true);

    connect(ui->sldWBBlue, SIGNAL(valueChanged(int )), this, SLOT(change_wb_blue()));
    ui->sldWBBlue->setValue(CGlobalSettings::_webc_wb_blue);

    connect(ui->sldWBRed, SIGNAL(valueChanged(int )), this, SLOT(change_wb_red()));
    ui->sldWBRed->setValue(CGlobalSettings::_webc_wb_red);

    connect(ui->chkAutoWB, SIGNAL(toggled(bool)), this, SLOT(change_wb_auto()));

    connect(ui->btnDefault, SIGNAL(clicked()), this, SLOT(set_defaults()));

    ui->lblFOVX->setText(QString::number(get_webc_fovx(CGlobalSettings::_webc_focus)));
    ui->lblFOVY->setText(QString::number(get_webc_fovy(CGlobalSettings::_webc_focus)));

    //ui->lblResText->hide();
    ui->cmbResol->setEnabled(false);


    connect(ui->sldFocus, SIGNAL(valueChanged(int)), this, SLOT(change_focus()));

    ui->chkCalib->setChecked(false);
    connect(ui->chkCalib, SIGNAL(clicked(bool)), this, SLOT(change_calib(bool)));
    if(!g_callibration_map.empty())
    {
        ui->chkCalib->setChecked(true);
        ui->sldFocus->setMinimum(0);
        ui->sldFocus->setMaximum(g_callibration_map._map.size() - 1);

        int idx = g_callibration_map.which_focus_indx(CGlobalSettings::_webc_focus);
        ui->sldFocus->setValue(idx);
        change_focus();
    }
    else
        ui->sldFocus->setValue(CGlobalSettings::_webc_focus/4); // def


}

void CUSBCameraSettings::set_defaults()
{
    CGlobalSettings::_webc_brightness = 0;
    ui->sldBrightness->setValue(CGlobalSettings::_webc_brightness); // def

    CGlobalSettings::_webc_contrast = 32;
    ui->sldContrast->setValue(CGlobalSettings::_webc_contrast); // def

    CGlobalSettings::_webc_hue = 0;
    ui->sldHue->setValue(CGlobalSettings::_webc_hue);

    CGlobalSettings::_webc_satu = 50;
    ui->sldSaturation->setValue(CGlobalSettings::_webc_satu);

    CGlobalSettings::_webc_shap = 1;
    ui->sldSharpness->setValue(CGlobalSettings::_webc_shap);

    CGlobalSettings::_webc_gamma = 300;
    ui->sldGamma->setValue(CGlobalSettings::_webc_gamma); // def

    CGlobalSettings::_webc_wb_blue = 4600;
    ui->sldWBBlue->setValue(CGlobalSettings::_webc_wb_blue);

    CGlobalSettings::_webc_gain = 64;
    ui->sldGain->setValue(CGlobalSettings::_webc_gain);

    CGlobalSettings::_webc_focus = 64;
    ui->sldFocus->setValue(CGlobalSettings::_webc_focus/4); // def

    CGlobalSettings::_webc_exposeure = -3;
    ui->sldExposure->setValue(CGlobalSettings::_webc_exposeure);

    ui->chkCalib->setChecked(false);
}

void CUSBCameraSettings::change_wb_auto()
{
    if(ui->chkAutoWB->isChecked())
    {
        _current_cam->set(CV_CAP_PROP_ANDROID_WHITE_BALANCE,1);

    }
    else
    {
        _current_cam->set(CV_CAP_PROP_ANDROID_WHITE_BALANCE,0);
    }

    //QString msg = QString("auto =") + QString::number(_current_cam->get(CV_CAP_PROP_XI_AUTO_WB));
    ///ui->chkAutoWB->setText(msg);
}

void CUSBCameraSettings::change_wb_blue()
{
    double v =  ui->sldWBBlue->value();
    CGlobalSettings::_webc_wb_blue =v;
    if(v == _val_wb_blue) return;

    _current_cam->set(CV_WEBCAM_WB_U,v);
    double real_val = _current_cam->get(CV_WEBCAM_WB_U);

    if(v == real_val)
        _val_wb_blue = v;

    QString msg = QString("%1 (%2)").arg(QString::number(v)).arg(QString::number(real_val));
    ui->lblWBBlue->setText(msg);

}

void CUSBCameraSettings::change_wb_red()
{
    double v =  ui->sldWBRed->value();
    CGlobalSettings::_webc_wb_red =v;
    if(v == _val_wb_red) return;

    _current_cam->set(CV_WEBCAM_WB_V,v);
    double real_val = _current_cam->get(CV_WEBCAM_WB_V);

    if(v == real_val)
        _val_wb_red = v;

    QString msg = QString("%1 (%2)").arg(QString::number(v)).arg(QString::number(real_val));
    ui->lblWBRed->setText(msg);
}

void CUSBCameraSettings::change_saturation()
{
    double v =  ui->sldSaturation->value();

    CGlobalSettings::_webc_satu= v;
    if(v == _val_satu) return;

    _current_cam->set(CV_CAP_PROP_SATURATION, v);
    double real_val = _current_cam->get(CV_CAP_PROP_SATURATION);

    if(v == real_val)
        _val_satu = v;

    QString msg = QString("%1 (%2)").arg(QString::number(v)).arg(QString::number(real_val));
    ui->lblSaturation->setText(msg);
}


void CUSBCameraSettings::change_hue()
{
    double v =  ui->sldHue->value();

    CGlobalSettings::_webc_hue = v;
    if(v == _val_hue) return;

   // _timer->stop();

    _current_cam->set(CV_CAP_PROP_HUE, v);
    double real_val = _current_cam->get(CV_CAP_PROP_HUE );

    if(v == real_val)
        _val_hue = v;

    QString msg = QString("%1 (%2)").arg(QString::number(v)).arg(QString::number(real_val));
    ui->lblHue->setText(msg);

   // _timer->start(_timer_period);

}


void CUSBCameraSettings::change_sharpness()
{
    double v =  ui->sldSharpness->value();

    CGlobalSettings::_webc_shap = v;
    if(v == _val_sharp) return;

    _current_cam->set(CV_CAP_PROP_SHARPNESS,v);
    double real_val = _current_cam->get(CV_CAP_PROP_SHARPNESS);

    if(v == real_val)
        _val_sharp = v;

    QString msg = QString("%1 (%2)").arg(QString::number(v)).arg(QString::number(real_val));
    ui->lblSharp->setText(msg);
}


void CUSBCameraSettings::change_gain()
{
    double v =  ui->sldGain->value();

    CGlobalSettings::_webc_gain = v;
    if(v == _val_gain) return;

    _current_cam->set(CV_CAP_PROP_GAIN,v);
    double real_val = _current_cam->get(CV_CAP_PROP_GAIN);

    if(v == real_val)
        _val_gain = v;

    QString msg = QString("%1 (%2)").arg(QString::number(v)).arg(QString::number(real_val));
    ui->lblGain->setText(msg);
}

void CUSBCameraSettings::change_gamma()
{
    double v = ui->sldGamma->value();
    CGlobalSettings::_webc_gamma= v;

    if(v == _val_gamma) return;

    _current_cam->set(CV_CAP_PROP_GAMMA,v);
    double real_val = _current_cam->get(CV_CAP_PROP_GAMMA);

    if(v == real_val)
        _val_gamma = v;

    QString msg = QString("%1 (%2)").arg(QString::number(v)).arg(QString::number(real_val));
    ui->lblGamma->setText(msg);
}

void CUSBCameraSettings::change_contrast()
{
    double v = ui->sldContrast->value();
    CGlobalSettings::_webc_contrast= v;

    if(v == _val_contrast) return;

    _current_cam->set(CV_CAP_PROP_CONTRAST,v);
    double real_val = _current_cam->get(CV_CAP_PROP_CONTRAST);

    if(v == real_val)
        _val_contrast = v;

    QString msg = QString("%1 (%2)").arg(QString::number(v)).arg(QString::number(real_val));
    ui->lbContrast->setText(msg);
}

void CUSBCameraSettings::change_brightness()
{
    double v = ui->sldBrightness->value();
    CGlobalSettings::_webc_brightness= v;

    if(v == _val_brightness) return;

    _current_cam->set(CV_CAP_PROP_BRIGHTNESS,v);
    double real_val = _current_cam->get(CV_CAP_PROP_BRIGHTNESS);

    if(v == real_val)
        _val_brightness = v;

    QString msg = QString("%1 (%2)").arg(QString::number(v)).arg(QString::number(real_val));
    ui->lblBrightness->setText(msg);
}

void CUSBCameraSettings::change_calib(bool checked)
{
    if(g_callibration_map.empty())
    {
        QString msg = QString("There is no callibration info");
        QMessageBox::information(this, tr("Information"), msg);
        ui->chkCalib->setChecked(false);
        return;
    }

    if(checked) // show calib info only
    {
        ui->sldFocus->setMinimum(0);
        ui->sldFocus->setMaximum(g_callibration_map._map.size() - 1);

        int idx = g_callibration_map.which_focus_indx(CGlobalSettings::_webc_focus);
        ui->sldFocus->setValue(idx);

    }
    else // show all calib
    {
        ui->sldFocus->setMinimum(0);
        ui->sldFocus->setMaximum(25);
        ui->sldFocus->setValue(CGlobalSettings::_webc_focus/4); // def
    }

}

void CUSBCameraSettings::change_focus()
{

    std::cout << "void CUSBCameraSettings::change_focus()" << std::endl;
    if(!ui->chkCalib->isChecked())
    {
        double v = ui->sldFocus->value()*4;
        CGlobalSettings::_webc_focus= v;

        if(v == _val_focus) return;

        _current_cam->set(CV_CAP_PROP_FOCUS,v);
        double real_val = _current_cam->get(CV_CAP_PROP_FOCUS);

        if(v == real_val)
            _val_focus = v;

        QString msg = QString("%1 (%2)").arg(QString::number(v)).arg(QString::number(real_val));
        ui->lblFocus->setText(msg);

        ui->lblFOVX->setText(QString::number(get_webc_fovx(CGlobalSettings::_webc_focus)));
        ui->lblFOVY->setText(QString::number(get_webc_fovy(CGlobalSettings::_webc_focus)));
    }
    else
    {
        double idx = ui->sldFocus->value();

        int v = g_callibration_map.set_globals_for_num(idx);
        CGlobalSettings::_webc_focus= v;


        if(v == _val_focus) return;


        _current_cam->set(CV_CAP_PROP_FOCUS,v);
        double real_val = _current_cam->get(CV_CAP_PROP_FOCUS);

        if(v == real_val)
            _val_focus = v;

        QString msg = QString("%1 (%2)").arg(QString::number(v)).arg(QString::number(real_val));
        ui->lblFocus->setText(msg);

        ui->lblFOVX->setText(QString::number(get_webc_fovx(CGlobalSettings::_webc_focus)));
        ui->lblFOVY->setText(QString::number(get_webc_fovy(CGlobalSettings::_webc_focus)));
    }


}

void CUSBCameraSettings::change_exposure()
{
    int v =  ui->sldExposure->value();
    CGlobalSettings::_webc_exposeure = v;

    if(v == _val_exposure) return;
    _current_cam->set(CV_CAP_PROP_EXPOSURE,v);
    double real_val = _current_cam->get(CV_CAP_PROP_EXPOSURE);

    if(real_val == v)
        _val_exposure = v;

    QString msg = QString("%1 (%2)").arg(QString::number(v)).arg(QString::number(real_val));
    ui->lblExposure->setText(msg);
}


void CUSBCameraSettings::resolution_changed(int nres_idx)
{
    if(nres_idx == _resol_idx) return;

    int nxs = _resol_list[2*nres_idx];
    int nys = _resol_list[2*nres_idx + 1];

    _current_cam->set(CV_CAP_PROP_FRAME_WIDTH, nxs);
    _current_cam->set(CV_CAP_PROP_FRAME_HEIGHT, nys);

    if((_current_cam->get(CV_CAP_PROP_FRAME_WIDTH) == nxs) &&
        (_current_cam->get(CV_CAP_PROP_FRAME_HEIGHT) == nys) )
    {
        _resol_idx = nres_idx;
    }
    else
    {
        _current_cam->set(CV_CAP_PROP_FRAME_WIDTH, _resol_list[2*_resol_idx]);
        _current_cam->set(CV_CAP_PROP_FRAME_HEIGHT, _resol_list[2*_resol_idx+1]);
        ui->cmbResol->setCurrentIndex(_resol_idx);

        QString msg = QString("Camera does not support selected resolution: %1x%2").arg(QString::number(nxs)).arg(QString::number(nys));
        QMessageBox::warning(this, tr("Warning"),msg);
    }

}

CUSBCameraSettings::~CUSBCameraSettings()
{
    delete ui;
}
