#ifndef CDECIMSMOOTHTHREAD_H
#define CDECIMSMOOTHTHREAD_H

#include <QThread>
#include <QMutex>
#include "graphicutils.h"
#include "ctriangularmesh.h"
#include "./meshlab/mesho.h"

class CDecimSmoothThread : public QThread
{
    Q_OBJECT
public:
    explicit CDecimSmoothThread(QObject *parent = 0);
    ~CDecimSmoothThread();

    void start_process(CTriangularMesh *, int, float, int);
    bool is_abort(){return _abort;}
    void emit_send_back(int i);

signals:
    void send_back(const int val);
    void send_result(CTriangularMesh *);

protected:
    void run();
    void  QuadricSimplification(CMeshO &m,
                               int  TargetFaceNum,
                               vcg::tri::TriEdgeCollapseQuadricParameter &pp);

public slots:
    void stop_process();


private:
    bool    _abort;
    QMutex  _mutex;

    CTriangularMesh * _inmesh;
    int     _perc;
    float _degree;
    int     _iter;
};

#endif // CDECIMSMOOTHTHREAD_H
