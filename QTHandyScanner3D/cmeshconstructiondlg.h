#ifndef CMESHCONSTRUCTIONDLG_H
#define CMESHCONSTRUCTIONDLG_H

#include <QDialog>

class CGLWidget;
class CTriangularMesh;
class CMeshConstructionThread ;
namespace Ui {
class CMeshConstructionDlg;
}

class CMeshConstructionDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CMeshConstructionDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CMeshConstructionDlg();

protected:
    void showEvent(QShowEvent * event);
    void init_meshlist();

private:
    Ui::CMeshConstructionDlg *ui;


    CGLWidget *_glWidget;

    bool _in_process;
    bool _on_apply;
    bool _is_result_valid;
    CMeshConstructionThread * _thread;

    enum MeshQuality{LOW = 1, MEDIUM, HIGH};

private slots:
    void change_quality(int v);
    void onCancelBtn();
    void onPreviewBtn();
    void onApplyBtn();

    void onMakeProgressStep(int val);
    void onCompleteProcessing(CTriangularMesh * );
    void onStopProcessing();

    void close_dialog();



};

#endif // CMESHCONSTRUCTIONDLG_H
