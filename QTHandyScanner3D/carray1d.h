#ifndef __CARRAY1D_H__
#define __CARRAY1D_H__


#include <cmath>
#include <iostream>
#include <fstream>

template <class T> 
class CArray1D
{

public:

    CArray1D() { init_data(); }

    CArray1D(int xs)
    {
		init_data();
        allocate_data(xs);
    }

    CArray1D(int xs, T def_val )
    {
        init_data();
        allocate_data(xs);
        for(int i = 0; i <_xs ; i++)
            _data[i] = def_val;
    }

    virtual ~CArray1D()
    {
        clear();
    }

    CArray1D(const CArray1D<T>& cpyarr)
	{
		init_data();
	    _xs = cpyarr._xs;
        allocate_data(_xs);
        memcpy(_data, cpyarr._data, _xs*sizeof(T));
	}

    CArray1D(const CArray1D<T> * cpyarr)
	{
	   init_data();
	   _xs = cpyarr->_xs;
       allocate_data(_xs);
       memcpy(_data, cpyarr._data, _xs*sizeof(T));
	}
 
    void operator =(const CArray1D<T> & cpyarr)
    {
        if(_data != NULL) clear();
	
        _xs = cpyarr._xs;
        allocate_data(_xs);
       memcpy(_data, cpyarr._data, _xs*sizeof(T));
    }



   void clean_data(T def_val = T(0))
   {
        for(int i = 0; i <_xs ; i++)
            _data[i] = def_val;
   }


   int _xs;
    T* _data;

protected:

    void allocate_data(int xs)
    {

        _data = new T[xs];
        _xs = xs;
    }

	void init_data()
	{
		_data = NULL;
        _xs = 0;
	}

    void clear()
    {	
        if(_data != NULL) delete [] _data;
        init_data();
   }



};



#endif
