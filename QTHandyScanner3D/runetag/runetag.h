#ifndef RUNETAG_H
#define RUNETAG_H


/// RUNETAG block
#include "runetag.hpp"
#include "auxrenderer.hpp"
#include "ellipsefitter.hpp"
#include "markerpose.hpp"
#include "coding.h"

void compute_cam_angles_and_rotation_center(cv::runetag::Pose & RT,
        double & c_camera_x_incline, double & c_camera_y_incline,
        double & rotCX, double & rotCY, double & rotCZ);

void draw_detected_runetag( cv::Mat& frame,
                                const cv::runetag::MarkerDetected& m,
                                const cv::runetag::Pose& p,
                                const cv::Mat& intrinsics,
                                const cv::Mat& distortion,
                                bool valid_dots = true);


bool do_find_runetag(cv::Mat & frame,
                     cv::Mat & cam_intrinsics,
                    cv::Mat & distortion,
                     cv::runetag::MarkerDetector * _pat_detector,
                     cv::runetag::EllipseDetector * _ellipseDetector,
                     cv::runetag::Pose & RT,
                     cv::runetag::MarkerDetected & tf,
                     float * av_error);

#endif // RUNETAG_H
