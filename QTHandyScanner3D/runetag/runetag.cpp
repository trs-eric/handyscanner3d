#include "runetag/runetag.h"

#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/contrib/contrib.hpp>

#include  "graphicutils.h"
void  compute_cam_angles_and_rotation_center
(
        cv::runetag::Pose & RT,
        double & c_camera_x_incline,
        double & c_camera_y_incline,
        double & rotCX, double & rotCY, double & rotCZ)
{
    cv::Mat vRot =  RT.R;
    cv::Mat vTra =  RT.t;

    cv::Mat mRot;
    cv::Rodrigues(vRot, mRot);


    cv::Mat vecZ = (cv::Mat_<double>(3,1) << 0, 0, 1); // vertical from disc ceneter
    cv::Mat vecZR = mRot * vecZ;

    double v0L = std::sqrt(vecZR.at<double>(1,0)*vecZR.at<double>(1,0)  +  vecZR.at<double>(2,0)*vecZR.at<double>(2,0));
    double theta = std::acos(vecZR.at<double> (2,0)/v0L ) ;
    double beta = atan2(vecZR.at<double>(1,0), vecZR.at<double>(0,0));
    std::cout << "ANGLE theta " << IBGM(theta)  <<  std::endl; /// pi - theta is
    std::cout << "ANGLE beta " << IBGM(beta)   <<  std::endl;


     c_camera_x_incline =  cPI/2 - theta;
     c_camera_y_incline =  beta-cPI/2   ;



    //float mat_10 = 0;
    double mat_11 =  cos(c_camera_x_incline);
    double mat_12 = -sin(c_camera_x_incline);

    //float mat_20 =  0;
    double mat_21 =  sin(c_camera_x_incline);
    double mat_22 =  cos(c_camera_x_incline);

    // camera_y_incline
    double r_mat_00 =  cos(c_camera_y_incline);
    double r_mat_02 =  sin(c_camera_y_incline);

    double r_mat_20 = -sin(c_camera_y_incline);
    double r_mat_22 =  cos(c_camera_y_incline);


   std::cout << "Current camera R" << vRot << std::endl;
   std::cout << "Current camera T" << vTra << std::endl;

    double rx0 = vTra.at<double>(0,0);
    double ry0 = vTra.at<double>(2,0);
    double rz0 = vTra.at<double>(1,0);

    // rotate back for camera inclanation y
    double nx1 = rx0 * r_mat_00 + rz0 * r_mat_02;
    double ny1 = ry0;
    double nz1 = rx0 * r_mat_20 + rz0 * r_mat_22;

    // rotate back for camera inclanation x
     rotCX = nx1 ;
     rotCY = ny1 * mat_11 + nz1 * mat_12;
     rotCZ = ny1 * mat_21 + nz1 * mat_22;


     c_camera_x_incline = -c_camera_x_incline;
     c_camera_y_incline = -c_camera_y_incline;


     /*  double nx1 = rx0 ;
    double ny1 = ry0 * mat_11 + rz0 * mat_12;
    double nz1 = ry0 * mat_21 + rz0 * mat_22;

        // rotate back for camera inclanation x
    rotCX = nx1 * r_mat_00 + nz1 * r_mat_02;
    rotCY = ny1;
    rotCZ = nx1 * r_mat_20 + nz1 * r_mat_22;*/


    std::cout << "c_camera_x_incline " << IBGM(c_camera_x_incline) <<  " c_camera_y_incline " << IBGM(c_camera_y_incline) <<  std::endl;
    std::cout << "ROTATION CENTER " << rotCX << " " << rotCY << " " << rotCZ << std::endl;


}

void draw_detected_runetag( cv::Mat& frame,
                                const cv::runetag::MarkerDetected& m,
                                const cv::runetag::Pose& p,
                                const cv::Mat& intrinsics,
                                const cv::Mat& distortion,
                                 bool valid_dots)
{

    std::vector< cv::Point3f> circle_vertices;
    int szi = 60;
    float radii = 72;
    float x0 = 0;
    float y0 = 0;

    for(int i = 0; i < szi; i++)
    {
        float x = x0 + radii * cos(2.0 * float(i) * M_PI/float(szi));
        float y = y0 + radii * sin(2.0 * float(i)  * M_PI/float(szi));
        float z = 0;

        circle_vertices.push_back(cv::Point3f(x,y,z));
    }


    std::vector< cv::Point3f > marker_dots;
    std::vector< bool > valid_slots;

    //// std::cout << " slots count " << m.getNumSlots()  << std::endl;


    for( unsigned int i=0; i< m.getNumSlots(); ++i )
    {
        try
        {
            cv::Point2d ptcenter = cv::runetag::ellipseCenter( m.associatedModel()->modelEllipseAtSlot(i) );
            marker_dots.push_back( cv::Point3f((float)ptcenter.x, (float)ptcenter.y, 0 ) );
            valid_slots.push_back( m.getSlot(i).value() );
        }catch( cv::runetag::DigitalMarkerModel::MarkerModelOperationException )
        {
        }

    }

    std::vector< cv::Point3f> rotation_axis;
    float axis_length = 30;
    rotation_axis.push_back(cv::Point3f(0,0,0));
    rotation_axis.push_back(cv::Point3f(0,0,-axis_length ));

    std::vector< cv::Point2f > circle_vertices_img;
    std::vector< cv::Point2f > marker_dots_img;
    std::vector< cv::Point2f > rotation_axis_img;


    cv::projectPoints( circle_vertices, p.R, p.t, intrinsics, distortion, circle_vertices_img );
    cv::projectPoints( marker_dots, p.R, p.t, intrinsics, distortion, marker_dots_img );
    cv::projectPoints( rotation_axis, p.R, p.t, intrinsics, distortion, rotation_axis_img );

    // Render marker dots
    if(valid_dots)
    {
        for( size_t i=0; i<marker_dots_img.size(); ++i )
            cv::circle(frame, marker_dots_img[i],3,valid_slots[i]?CV_RGB(0,255,0):CV_RGB(255,0,0),-1,CV_AA );
    }


    cv::Scalar colorL = CV_RGB(0,0,255);
    for( size_t i=0; i<circle_vertices_img.size(); ++i )
         cv::line( frame, circle_vertices_img[i], circle_vertices_img[(i+1)%circle_vertices_img.size()], colorL, 2, CV_AA );

    cv::Scalar colorA = CV_RGB(0,0,255);
    cv::line( frame, rotation_axis_img[0], rotation_axis_img[1], colorA, 2, CV_AA );


    /// compute axis coordinates from camera point of view
    /*
    cv::Mat p0_axis =  (cv::Mat_<double>(4,1) << 0, 0, 0, 1);
    cv::Mat p1_axis =  (cv::Mat_<double>(4,1) << 0, 0, -axis_length, 1);

    cv::Mat PRot;
    cv::Rodrigues(p.R, PRot);
    cv::Mat cPose = (cv::Mat_<double>(3,4) << PRot.at<double>(0,0), PRot.at<double>(0,1), PRot.at<double>(0,2), p.t.at<double>(0,0),
                                              PRot.at<double>(1,0), PRot.at<double>(1,1), PRot.at<double>(1,2), p.t.at<double>(1,0),
                                              PRot.at<double>(2,0), PRot.at<double>(2,1), PRot.at<double>(2,2), p.t.at<double>(2,0));

    cv::Mat p0_axis_nc = cPose * p0_axis;
    cv::Mat p1_axis_nc = cPose * p1_axis;


    cv::Mat oPose = (cv::Mat_<double>(3,4) << 1, 0, 0, 0,
                                              0, 1, 0, 0,
                                              0, 0, 1, 0);

    cv::Mat p0_axis_nc_mat = (cv::Mat_<double>(4,1) << p0_axis_nc.at<double>(0,0),
                                                       p0_axis_nc.at<double>(1,0),
                                                       p0_axis_nc.at<double>(2,0),
                                                     1);

    cv::Mat axis_uv0 = intrinsics * oPose * p0_axis_nc_mat;
    axis_uv0 /= axis_uv0.at<double>(2,0);

    cv::Mat p1_axis_nc_mat = (cv::Mat_<double>(4,1) << p1_axis_nc.at<double>(0,0),
                                                       p1_axis_nc.at<double>(1,0),
                                                       p1_axis_nc.at<double>(2,0),
                                                     1);
    //std::cout << "axis beg " << p0_axis_nc_mat << std::endl;
    //std::cout << "axis end " << p1_axis_nc_mat << std::endl;

    cv::Mat axis_uv1 = intrinsics * oPose * p1_axis_nc_mat;
    axis_uv1 /= axis_uv1.at<double>(2,0);


    std::cout << "axis beg " << axis_uv0.t() << std::endl;
    std::cout << "axis end " << axis_uv1.t() << std::endl;


    std::cout << "rotation_axis_img beg " << rotation_axis_img[0] << std::endl;
    std::cout << "rotation_axis_img end " << rotation_axis_img[1] << std::endl;
    */

}



bool do_find_runetag(cv::Mat & frame,
                     cv::Mat & cam_intrinsics,
                     cv::Mat & distortion,
                     cv::runetag::MarkerDetector * _pat_detector,
                     cv::runetag::EllipseDetector * _ellipseDetector,
                     cv::runetag::Pose & RT,
                     cv::runetag::MarkerDetected & tf,
                     float * av_error)
{
    std::vector< cv::RotatedRect > foundEllipses;
    _ellipseDetector->detectEllipses(frame, foundEllipses );
    std::cout << "Elipses  " << foundEllipses.size() << " found." << std::endl << std::endl;

    std::cout << "> Detecting RUNE tags" << std::endl;
    std::vector< cv::runetag::MarkerDetected > tags_found;
    _pat_detector->dbgimage = frame.clone();
    _pat_detector->detectMarkers( foundEllipses, tags_found);
    std::cout << "Tags:  " << tags_found.size() << " found." << std::endl << std::endl;

    if( tags_found.size() == 1)
    {
        std::cout << "> Estimating tags poses" << std::endl;
        bool poseok;
        std::cout << "  Tag IDX:" << tags_found[0].associatedModel()->getIDX();

        unsigned int flags=0;
        flags |= cv::runetag::FLAG_REPROJ_ERROR;

        tf = tags_found[0];

        RT = cv::runetag::findPose( tags_found[0], cam_intrinsics, distortion, &poseok,  CV_ITERATIVE, flags, av_error);

        if( poseok )
           return true;
        else
            return false;
    }
    else
        return false;
}
