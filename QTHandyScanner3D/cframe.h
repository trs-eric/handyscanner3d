#ifndef CFRAME_H
#define CFRAME_H

#include <vector>
#include <map>

// opencv headers
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include "graphicutils.h"

/*
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/contrib/contrib.hpp>
#include "opencv2/nonfree/features2d.hpp"
*/

// abstraction for all data gathered by a single shot

#include "ciostate.h"
class CFrame : public CIOState
{

public:
    CFrame(int id = -1);
    ~CFrame();

    // unique identification (=image number in a sequence or video)
    int _id;



    cv::Mat _img_L;
    cv::Mat _img_R;

    /// for offline work
    QString _imgL_str;
    QString _imgR_str;

    // global Euler angles in radians
    cv::Mat _glob_rot_vec;
    double _glob_rot_alpha;
    double _glob_rot_betta;

    cv::Mat _img_disparity;

    cv::Mat _img_3d;

    cv::Mat _descriptors;

    std::vector<cv::KeyPoint> _keypoints;

    std::map< int, std::vector<cv::DMatch>  *> _matches;


    CArray2D<aux_pix_data *> * _merged_data;

    // 3d coordinate of the camera
    cv::Mat _global_pos;
    // position changes wrt previous frame
    cv::Mat _relat_pos;
    //
    bool _valid_coord;


    bool load_tex_from_video_hd();

    bool load_L_images_from_hd();
    bool load_LR_images_from_hd();
    bool load_left_image_from_hd();

    bool compute_disparity_map();
    bool compute_depth_image();
    bool compute_keypoints_with_desc(bool filter_keypoints = true);

    void dump_unused_data();

    void match_features(CFrame *);

    cv::Mat get_neg_angular_difference_as_rotation_matrix(CFrame *);
    cv::Mat get_pos_angular_difference_as_rotation_matrix(CFrame *);
    cv::Mat get_global_rotation_matrix_forw();
    cv::Mat get_global_rotation_matrix_bakw();

    std::vector<C3DPoint> * construct_spcl_from_depth_img(bool usedecim=true);
    std::vector<C3DPoint> * construct_spcl_from_depth_img_with_norms(bool usedecim=true);
    int construct_dpcl_from_depth_img(std::list<C3DPoint> * , int decim_step);
    int construct_dpcl_from_depth_img_with_norms(std::vector<C3DPoint> * , int decim_step, int nsz);


    CTriMesh * construct_meshcast_from_depth_img(int decim_step);

    void adjust_angular_range();
    static bool cmp_angular_dist(const CFrame *, const CFrame *);
    double get_angular_distance(CFrame *);

    cv::Point3f  find_3d_offset_with_merged_data(CFrame * prev_frame);
    aux_pix_data * find_best_neighbour_merged_data(int u, int v);

    void fill_interpolated_merged_data();
    cv::Point3f  find_3d_offset_with_merged_data_icp(CFrame * prev_frame, cv::Point3f  in);
    cv::Point3f  find_3d_offset_with_merged_data_icp_x(CFrame * prev_frame, cv::Point3f  in);
    cv::Point3f  find_3d_offset_with_merged_data_icp_y(CFrame * prev_frame, cv::Point3f in);
    cv::Point3f  find_2d_offset_with_correlation(CFrame * prev_frame);
    cv::Point3f  compute_bb_center_with_RANSAC();
    float compute_farest_dist();
    cv::Point3f find_2d_offset_with_correlation_init( CFrame * prev_frame,
            CArray2D<float> * & cmap, CArray2D<float> * & pmap,
            CArray2D<float> * & cmap_x,  CArray2D<float> * & pmap_x,
            float & fwidth, int & rwidth, int & rheight,  float & p_c_bb_min_x_diff);
    cv::Point3f  find_2d_offset_with_correlation_step( CArray2D<float> * cmap, CArray2D<float> * pmap,
            CArray2D<float> * cmap_x, CArray2D<float> * pmap_x, float fwidth,
            int rwidth, int rheight,  float iny, float p_c_bb_min_x_diff, int & best_count);

    cv::Point3f  find_3d_offset_with_homography(CFrame * prev_frame,
                                                double & repro_error);
    cv::Point3f find_3d_offset_for_camera_pose(CFrame * prev_frame);
    cv::Point3f improve_z_offset_iter(int iteration_number, cv::Point3f & offT,
                                     CFrame * prev_frame, double & repro_error);
    double compute_average_reproj_error(cv::Mat & camP, cv::Mat & camK,
                                       CFrame * prev, double cutoff_min, double cautoff_max);


    void udate_global_and_relat_pose(CFrame *, cv::Point3f & realT);

    void save(std::ofstream & ofile);
    void load(std::ifstream & ifile);

    QImage get_as_image();

protected:

    void filter_out_far_keypoints();

     std::vector<cv::DMatch> * knn_match(CFrame *);
     std::vector<cv::DMatch> * flann_match(CFrame *);
     void filter_out_matches_wrt_epipolar_sence(CFrame *);

     std::vector<cv::Point3f> get_neghbour_3d_points(int x, int y, int decimstep);
       std::vector<cv::Point3f> get_neghbour_3d_glpoints(std::vector<C3DPoint>&, int, int, int, int, int, int);

     C3DPoint convert_point_coord_real_to_gl( cv::Mat & Rglob, int x, int y );
};

extern std::vector<CFrame *> global_keyframes_sequence;
//extern std::map<CFrame *,CFrame *> global_keyframes_sequence_corr;

extern int global_keyframes_for_processing;
extern bool global_no_more_keyframes;

// extern std::map< std::pair<int, int>, std::vector<cv::DMatch> * >

#endif // CFRAME_H
