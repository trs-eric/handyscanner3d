#include "meshconstructionutils.h"

#include "../MeshLab_133src/vcglib/vcg/complex/algorithms/pointcloud_normal.h"
#include "../MeshLab_133src/meshlab/src/plugins_experimental/edit_ocme/src/ocme/vcg_mesh.h"
#include "../MeshLab_133src/vcglib/vcg/complex/complex.h"
//#include "../MeshLab_133src/vcglib/apps/pivoting/cmesh.h"
#include "../MeshLab_133src/vcglib/vcg/complex/complex.h"
using namespace vcg;

bool myCallBackPos(const int pos, const char * str )
{
    std::cout << pos << " " << str<< std::endl;
}


void compute_normals_meshlab(std::vector<C3DPoint> * points, int neighbors, bool flip)
{
    tri::PointCloudNormal<vcgMesh>::Param p;

    p.fittingAdjNum = neighbors;
    p.smoothingIterNum = 2; // par.getInt("smoothIter");
  //  p.viewPoint = par.getPoint3f("viewPos");
    p.useViewPoint = false; // par.getBool("flipFlag");
    /**/
    vcgMesh mm;
    ///mm.vert.resize( points->size());

    std::vector<C3DPoint>::iterator it = points->begin();
    for( int i = 0 ; it != points->end(); it++, i++)
    {

       vcgVertex vertex;
       vertex.P() = vcg::Point3f(it->_x, it->_y, it->_z);
       mm.vert.push_back(vertex);
    }
    mm.vn = points->size();

    std::cout << "complete conversion" << std::endl;

    tri::PointCloudNormal<vcgMesh>::Compute(mm, p, myCallBackPos);

    std::vector<vcgVertex>::iterator itv = mm.vert.begin();

    for ( int i= 0; itv != mm.vert.end(); itv++, i++)
    {
        C3DPoint normal;
        normal._x = itv->P().X();
        normal._y = itv->P().Y();
        normal._z = itv->P().Z();

        normal._nx = itv->N().X();
        normal._ny = itv->N().Y();
        normal._nz = itv->N().Z();
         //// std::cout <<  normal._nx << " " <<  normal._ny << " " <<  normal._nz << std::endl;

         float len = std::sqrt(normal._nx*normal._nx + normal._ny*normal._ny +normal._nz*normal._nz);
        normal._nx = (flip)?normal._nx/len:-(normal._nx/len);
        normal._ny = (flip)?normal._ny/len:-(normal._ny/len);
         normal._nz = (flip)?normal._nz/len:-(normal._nz/len);

        (*points)[i] = normal;

    }

}


void compute_normals_meshlab(std::list<C3DPoint> * points, int neighbors, int iter, bool flip,
                     CallBackPos info)
{
    tri::PointCloudNormal<vcgMesh>::Param p;

    p.fittingAdjNum = neighbors;
    p.smoothingIterNum = iter;
    p.useViewPoint = false;

    vcgMesh mm;

    std::list<C3DPoint>::iterator it = points->begin();
    for( int i = 0 ; it != points->end(); it++, i++)
    {

       vcgVertex vertex;
       vertex.P() = vcg::Point3f(it->_x, it->_y, it->_z);
       mm.vert.push_back(vertex);
    }
    mm.vn = points->size();


    tri::PointCloudNormal<vcgMesh>::Compute(mm, p, info);

    std::vector<vcgVertex>::iterator itv = mm.vert.begin();
    std::list<C3DPoint>::iterator its = points->begin();

    for (  ; itv != mm.vert.end() && its != points->end(); itv++, its++)
    {
        C3DPoint normal;
        normal._x = itv->P().X();
        normal._y = itv->P().Y();
        normal._z = itv->P().Z();

        normal._nx = itv->N().X();
        normal._ny = itv->N().Y();
        normal._nz = itv->N().Z();

        float len = std::sqrt(normal._nx*normal._nx + normal._ny*normal._ny +normal._nz*normal._nz);
        normal._nx = (flip)?normal._nx/len:-(normal._nx/len);
        normal._ny = (flip)?normal._ny/len:-(normal._ny/len);
        normal._nz = (flip)?normal._nz/len:-(normal._nz/len);

        (*its) = normal;
    }

}

void compute_normals(std::vector<C3DPoint> * points, int neighbors, bool flip)
{
/*
  std::vector<CVertex> vertex_list; /// (points->size());

    std::vector<C3DPoint>::iterator it = points->begin();
    for( int i = 0 ; it != points->end(); it++, i++)
    {

        CVertex vertex;
        vertex.P() = vcg::Point3f(it->_x, it->_y, it->_z);// = it->_x;
         vertex_list.push_back(vertex);
    }

    std::cout << "finished in " << std::endl;

   bool is_succeeded = false;
   NormalExtrapolation<std::vector<CVertex> >::ExtrapolateNormals
        (vertex_list.begin(), vertex_list.end(), neighbors);


   std::cout << points->size() << " " << vertex_list.size() << std::endl;
   /// std::vector<C3DPoint> normals;
   std::cout << "computed " << std::endl;

 //  if(!is_succeeded) return; // exit right away

   std::vector<CVertex>::iterator itv = vertex_list.begin();
   for ( int i= 0; itv != vertex_list.end(); itv++, i++)
   {
       C3DPoint normal;
       normal._x = itv->P().X();
       normal._y = itv->P().Y();
       normal._z = itv->P().Z();

       normal._nx = itv->N().X();
       normal._ny = itv->N().Y();
       normal._nz = itv->N().Z();
     //  std::cout <<  normal._nx << " " <<  normal._ny << " " <<  normal._nz << std::endl;

        float len = std::sqrt(normal._nx*normal._nx + normal._ny*normal._ny +normal._nz*normal._nz);
       normal._nx = (flip)?normal._nx/len:-(normal._nx/len);
      normal._ny = (flip)?normal._ny/len:-(normal._ny/len);
        normal._nz = (flip)?normal._nz/len:-(normal._nz/len);

       (*points)[i] = normal;

   }
   std::cout << "result" << std::endl;
*/
}

