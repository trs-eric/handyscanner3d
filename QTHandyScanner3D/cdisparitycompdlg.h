#ifndef CDISPARITYCOMPDLG_H
#define CDISPARITYCOMPDLG_H

#include <QDialog>
#include "graphicutils.h"

class CGLWidget;
class CDisparityCompThread;
class CPointCloud;

namespace Ui {
class CDisparityCompDlg;
}

class CDisparityCompDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CDisparityCompDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CDisparityCompDlg();

protected:
    void showEvent(QShowEvent * event);


    bool _in_process;
    bool _on_apply;
    bool _is_result_valid;
    CDisparityCompThread * _thread;


private:
    Ui::CDisparityCompDlg *ui;

    CGLWidget *_glWidget;

private slots:
    void onCancelBtn();
    void onPreviewBtn();
    void onApplyBtn();
    void onBrowseBtn();

    void onMakeProgressStep(int val);
    void onCompleteProcessing(std::vector<C3DPoint> *  );
    void onStopProcessing();

    void close_dialog();

};

#endif // CDISPARITYCOMPDLG_H
