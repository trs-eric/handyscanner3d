#include "cdisparitycompthread.h"
#include "cpointcloud.h"
#include "disparity/disparity_utils.h"


#include "./settings/psettings.h"
/*#include "./libelasomp/image.h"
#include "./libelasomp/elas.h"
*/
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>


#include "normals/normal_utils.h"

CDisparityCompThread::CDisparityCompThread(QObject *parent) :
    QThread(parent), _camera_image (NULL), _usecol(true), _left_im(NULL), _right_im(NULL), _lbias(0),
    _thresh_border(0), _num_disp(0), _thresColor(0),
  _thresGrad(0), _gamma(0), _r(0), _eps(0),
  _gamma_c(0), _gamma_d(0), _r_median(0), _postproc(false), _disp_cutoff(300), _decim_step(1),
  _smooth_disp(false)
{
    _abort = false;
}

CDisparityCompThread::~CDisparityCompThread()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
    wait();
}

void CDisparityCompThread::start_process(QImage * cam_img, bool usecol,
                                         double disp_cutoff, int decim_step,
                                         bool smdisp)
{
    _abort = false;


    _camera_image = cam_img;
    _usecol = usecol;
    _disp_cutoff = disp_cutoff;
    _decim_step  = decim_step;
    _smooth_disp = smdisp;

    start();
}

void CDisparityCompThread::start_process(QImage * left_im, QImage * right_im,
                       int lbias, double thresh_border, int num_disp, double thresColor,
                       double thresGrad, double gamma, int r, double eps,
                       double gamma_c, double gamma_d, int r_median, bool postproc)
{
    _abort = false;


    _left_im = left_im;
    _right_im = right_im;
    _lbias = lbias;
    _thresh_border = thresh_border;
    _num_disp = num_disp;
    _thresColor = thresColor;
    _thresGrad = thresGrad;
    _gamma = gamma;
    _r = r;
    _eps = eps;
    _gamma_c = gamma_c;
    _gamma_d = gamma_d;
    _r_median = r_median;
    _postproc = postproc;

    start();


}

void CDisparityCompThread::stop_process()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
}


void CDisparityCompThread::emit_send_back(int i)
{
     emit send_back(i);
}

cv::Mat offsetImageWithPadding( cv::Mat & originalImage,
                               int offsetX, int offsetY, cv::Scalar backgroundColour)
{
        cv::Mat padded = cv::Mat(originalImage.rows + 2 * abs(offsetY), originalImage.cols + 2 * abs(offsetX), CV_8UC3, backgroundColour);
        originalImage.copyTo(padded(cv::Rect(abs(offsetX), abs(offsetY), originalImage.cols, originalImage.rows)));
        return cv::Mat(padded,cv::Rect(abs(offsetX) + offsetX, abs(offsetY) + offsetY, originalImage.cols, originalImage.rows));
}


void test_get_neghbour_3d_glpoints(std::vector<C3DPoint> & gl_points,
                      cv::Mat & imdisp,
   double disp_cutoff, int x, int y, int xs, int ys, int decimstep, int nsz,
                                   std::vector<cv::Point3f> & res)
{
   res.clear();

    for (int j = y - nsz * decimstep;
         j <= y+ nsz *decimstep; j+=decimstep )
    {
        for (int i = x - nsz * decimstep;
             i <= x + nsz * decimstep; i+=decimstep)
        {

            if( (j >= 0) && (j < ys) && (i >=0) && (i < xs))
            {
                 int cdisp = imdisp.at<float>(j,i);
                 if(cdisp > disp_cutoff )
                 {
                        C3DPoint p = gl_points[j * xs + i];
                        cv::Point3f pp;
                        pp.x = p._x;
                        pp.y = p._y;
                        pp.z = p._z;
                        res.push_back(pp);
                 }
            }
        }
    }
}

void CDisparityCompThread::run()
{

    std::cout << "extract images" << std::endl;

    int xs = _camera_image->width()/2;
    int ys = _camera_image->height();

    int x_offset = 0;
    cv::Mat tmp(ys, xs, CV_8UC4, (uchar*)_camera_image->bits(),_camera_image->bytesPerLine());
    cv::Mat left_img_tmp= cv::Mat(tmp.rows, tmp.cols, CV_8UC3 );
    int from_to[] = { 0,0,  1,1,  2,2 };
    cv::mixChannels( &tmp, 1, &left_img_tmp, 1, from_to, 3 );
    // cv::Mat left_img;
    // img(cv::Rect(50,30,img.cols-50,img.rows-30)).copyTo(imgTranslated(cv::Rect(0,0,img.cols-50,img.rows-30)));
    cv::Mat left_img= cv::Mat(left_img_tmp, cv::Rect(0, 0, xs, ys));
    //offsetImageWithPadding(left_img_tmp, x_offset, 0, cv::Scalar(0,0,0));

    cv::Mat tmp2(ys, 2*xs, CV_8UC4, (uchar*)_camera_image->bits(),_camera_image->bytesPerLine());
    cv::Mat res = cv::Mat(tmp2.rows, tmp2.cols, CV_8UC3 );
    ///int from_to[] = { 0,0,  1,1,  2,2 };
    cv::mixChannels( &tmp2, 1, &res, 1, from_to, 3 );
    // cut right part
    cv::Mat right_img= cv::Mat(res, cv::Rect(xs, 0, xs, ys));


  /*

    cv::Mat left_img;
    left_img = cv::imread("./realrect/imgU_left_vcorr_s.bmp", CV_LOAD_IMAGE_COLOR);
    cv::Mat right_img;
    right_img = cv::imread("./realrect/imgU_right_vcorr_s.bmp", CV_LOAD_IMAGE_COLOR)
    int xs = left_img.cols;
    int ys = left_img.rows; */

    std::cout << "computing disparity" << std::endl;


    cv::Mat imgL_g, imgR_g;
    cv::cvtColor(left_img, imgL_g, CV_BGR2GRAY);
    cv::cvtColor(right_img, imgR_g, CV_BGR2GRAY);

/*
    cv::Size size(imgL_g.cols/2, imgL_g.rows/2);
    xs /= 2; ys /= 2 ;
    cv::Mat imgL_gr;//dst image
    cv::resize(imgL_g, imgL_gr, size);
    cv::Mat imgR_gr;//dst image
    cv::resize(imgR_g, imgR_gr, size);
   imgL_gr.copyTo(imgL_g);
   imgR_gr.copyTo(imgR_g);

*/
    //cv::imshow("L", imgL_g);
    //cv::imshow("R", imgR_g);

    int width  = imgL_g.cols;
    int height = imgL_g.rows;
    int fbytes = width*height*sizeof(float);

    const int32_t dims[3] = {width, height, width}; // bytes per line = width
    float* L_data = (float*)malloc(fbytes);
    float* R_data = (float*)malloc(fbytes);
//MIDDLEBURY
    Elas::parameters disp_params= Elas::parameters(Elas::MIDDLEBURY);
    disp_params.disp_min = 0;
    disp_params.disp_max = 450;
    disp_params.ipol_gap_width = 7;
    disp_params.beta = 0.02;
    disp_params.lr_threshold  = 4;

    if(_smooth_disp)
    {
        disp_params.filter_median         = 1;
        disp_params.filter_adaptive_mean  = 1;
    }

    Elas elas(disp_params);


    elas.process(imgL_g.data, imgR_g.data, L_data, R_data, dims);

    cv::Mat resmat = cv::Mat(width*height, 1, CV_32F, L_data).clone();
    cv::Mat _img_disparity = resmat.reshape(0, height);

    /////////////////////
    /**/QString filname =QString("disp_float.raw");
    std::ofstream dispf;
    dispf.open (filname.toStdString().c_str(), std::ios::out| std::ios::binary);
    for (int32_t i=0; i<xs*ys; i++)
    {
      float val = L_data[i];
      dispf.write((char*)&val,  sizeof(float));
    }
    dispf.close();
    ////////////////////////

    free(L_data);
    free(R_data);


     /////////////// compute reprojection
    std::cout << "reprojecting points" << std::endl;

    cv::Mat _img_3d;
    cv::Mat stereo_Q = (cv::Mat_<double>(4,4) <<1., 0., 0., -5.9075617551803589e+002,
                        0., 1., 0.,  -4.7898372668027878e+002,
                        0., 0., 0., 1.2722967642764891e+003,
                        0., 0., 3.3887706509959209e-001, 0. );


    /*cv::Mat stereo_Q = (cv::Mat_<double>(4,4) << 1., 0., 0., -1.1408561706542969e+003,
     * 0.,  -9.6806149291992188e+002, 0., 0., 0., 2.5427033261434640e+003, 0.,
                        0., 3.3847110953139725e-001, 0.  );*/
    cv::reprojectImageTo3D(_img_disparity, _img_3d,  stereo_Q);


    /////////////// compute possible gl_points
    std::cout << "computing gl coordinates" << std::endl;

    std::vector<C3DPoint> gl_points(xs*ys);
    for (int y = 0; y < ys; y+= _decim_step )
    for (int x = 0; x < xs; x+= _decim_step )
    {
        int cdisp = _img_disparity.at<float>(y,x);
        if( cdisp > _disp_cutoff  )
        {
            cv::Point3f pp = _img_3d.at<cv::Point3f>(y,x);

            C3DPoint p;
            p._x =   pp.x * CGlobalHSSettings::glob_gl_scale + CGlobalHSSettings::glob_gl_offx;
            p._y = - pp.y * CGlobalHSSettings::glob_gl_scale + CGlobalHSSettings::glob_gl_offy;
            p._z = - pp.z * CGlobalHSSettings::glob_gl_scale + CGlobalHSSettings::glob_gl_offz;

            gl_points[y*xs +x ] = p;
        }
    }

    ///////////////////////////////


      int _neigh_count = 4;
      std::vector<C3DPoint> *  final_res = new std::vector<C3DPoint>;
      for (int y = 0; y < ys; y+=_decim_step)
      {
            emit send_back(float(y)*100/float(ys));

            #pragma omp parallel for  num_threads(2)
            for (int x = 0; x < xs; x+=_decim_step )
            {
                float disp_1L = _img_disparity.at<float>(y, x);
                if(disp_1L  >= _disp_cutoff   )
                {
                    std::vector<cv::Point3f> neigh_pnts;
                    test_get_neghbour_3d_glpoints(gl_points, _img_disparity,
                                               _disp_cutoff, x, y, xs, ys, _decim_step, _neigh_count,
                                                   neigh_pnts);
                    if(neigh_pnts.size() > 5)
                   {
                          Vector3f n   = compute_lse_plane_normal(neigh_pnts);
                           /// normal postprocessing
                            if(n.z < 0.)
                           {
                               n.x = -n.x;
                               n.y = -n.y;
                               n.z = -n.z;
                           }
                           ////////////////////////

                           C3DPoint p = gl_points[y*xs +x ];

                           p._nx = n.x;
                           p._ny = n.y;
                           p._nz = n.z;

                            if(_usecol)
                            {
                                cv::Vec3b rgbv = left_img.at<cv::Vec3b>(y,x);
                                p._r =  float(rgbv.val[2]) / 256.0;
                                p._g =  float(rgbv.val[1]) / 256.0;
                                p._b =  float(rgbv.val[0]) / 256.0;
                            }
                            else
                            {
                                p._r = 0.7;
                                p._g = 0.7;
                                p._b = 0.7;
                            }

                            #pragma omp critical
                            {
                                final_res->push_back(p);
                            }
                    }

                }


            }
        }
        std::cout << "getting final result" << std::endl;

        delete _camera_image ;
        _camera_image = NULL;

        emit send_result(final_res);
        emit send_back(100);
}

void CDisparityCompThread::run_old()
{

    //////////////////////////////////////////////////////////////////////////////////
    std::cout << _left_im << " " << _right_im << " " << " " << _lbias << " " <<
                 _thresh_border << " " <<  _num_disp << " " <<
                 _thresColor << " "  <<_thresGrad << " " << _gamma <<
                 " " <<  _r << " " << _eps  << " " << _gamma_c <<
                 " " << _gamma_d << " " <<  _r_median << " " <<  _postproc << std::endl;
    ////////////////////////////////////////////////////////////////////////////////////

    int xs = _left_im->width();
    int ys = _left_im->height();

    std::cout << xs << " " << ys << std::endl;

    if(_lbias > 0)
        horisontal_shift(_left_im, _lbias,_thresh_border*255.0);
    CArray1D<double3> * Il = convert_to_array((QRgb*)_left_im->scanLine(0), xs, ys);
    CArray1D<double3> * Ir = convert_to_array((QRgb*)_right_im->scanLine(0), xs, ys);

    // % Mirror image
    // Il_1 = flipdim(Ir,2);
    // Ir_1 = flipdim(Il,2);
    // normolize to 255
    CArray1D<double3> * Il_flip  = new CArray1D<double3> (xs*ys);
    CArray1D<double3> * Ir_flip  = new CArray1D<double3> (xs*ys);

    for(int y = 0, off = 0; y < ys; y++)
    {
        for(int x =0; x < xs; x++, off++)
        {
                double3 v = Il->_data[off] / 255.0;
                Il->_data[off] = v;
                double3 v2 = Ir->_data[off] / 255.0;
                Ir->_data[off] = v2;

                Il_flip->_data[y*xs + xs - x - 1] = v2;
                Ir_flip->_data[y*xs + xs - x - 1] =  v;
        }
    }


    CArray1D<double> * Il_g = convert_to_gray( Il, xs, ys);
    CArray1D<double> * Ir_g = convert_to_gray( Ir, xs, ys);

    CArray1D<double> * fx_l = gradient_norm(Il_g, xs, ys);
    CArray1D<double> * fx_r = gradient_norm(Ir_g, xs, ys);

    CArray1D<double> * fx_l_flip  = new CArray1D<double> (xs*ys);
    CArray1D<double> * fx_r_flip  = new CArray1D<double> (xs*ys);

    for(int y = 0; y < ys; y++)
    {
        int y_xs = y * xs;
        for(int x =0; x < xs; x++)
        {
               fx_l_flip->_data[y_xs + x] = fx_r->_data[y_xs + xs - x - 1];
               fx_r_flip->_data[y_xs + x] = fx_l->_data[y_xs + xs - x - 1];
        }
    }


    std::vector<CArray1D<double> *> dispVol(_num_disp, NULL) ;
    std::vector<CArray1D<double> *> dispVoll(_num_disp, NULL) ;

    CArray1D<double> * pcolor  = new CArray1D<double>(xs *ys,0);
    CArray1D<double> * pgrad = new CArray1D<double>(xs*ys,0);

    CArray1D<double> * pl_color  = new CArray1D<double>(xs*ys,0);
    CArray1D<double> * pl_grad = new CArray1D<double>(xs*ys,0);

    CArray1D<double3> tmp(xs*ys);
    CArray1D<double>  tmp2(xs*ys);
    CArray1D<double3> tmp_l(xs*ys);
    CArray1D<double>  tmp2_l(xs*ys);

    //////////////// first big loop
    for (int d = 0 ; d < _num_disp; d++)
    {

        tmp.clean_data(double3(_thresh_border));
        tmp2.clean_data(_thresh_border);
        tmp_l.clean_data(double3(_thresh_border));
        tmp2_l.clean_data(_thresh_border);


         for(int y = 0; y < ys; y++)
         {
             int y_xs = y * xs;
             for(int x =d, x1 =0 ; x < xs; x++, x1++)
             {
                 tmp._data[y_xs+x] = Ir->_data[y_xs+x1];
                 tmp2._data[y_xs+x] = fx_r->_data[y_xs+x1];
                 tmp_l._data[y_xs+x] = Ir_flip->_data[y_xs+x1];
                 tmp2_l._data[y_xs+x] = fx_r_flip->_data[y_xs+x1];
             }
         }

         for(int y = 0; y < ys; y++)
         {
             int y_xs = y * xs;
             for(int x =0; x < xs; x++)
             {
                {
                     double3 diff = tmp._data[y_xs+x] - Il->_data[y_xs+x];
                     diff.r = std::abs(diff.r);
                     diff.g = std::abs(diff.g);
                     diff.b = std::abs(diff.b);

                     double nsum =  (diff.r + diff.g + diff.b)/3.0;
                     if(nsum > _thresColor) nsum = _thresColor;
                     pcolor->_data[y_xs+x] = nsum;
               }

                 {
                      double nsum = std::abs (tmp2._data[y_xs+x] - fx_l->_data[y_xs+x]);
                      if(nsum > _thresGrad) nsum = _thresGrad;
                      pgrad->_data[y_xs+x] = nsum;
                }

                 {
                      double3 diff = tmp_l._data[y_xs+x] - Il_flip->_data[y_xs+x];
                      diff.r = std::abs(diff.r);
                      diff.g = std::abs(diff.g);
                      diff.b = std::abs(diff.b);

                      double nsum =  (diff.r + diff.g + diff.b)/3.0;
                      if(nsum > _thresColor) nsum = _thresColor;
                      pl_color->_data[y_xs+x] =  nsum;
                }

                {
                       double nsum = std::abs (tmp2_l._data[y_xs+x] - fx_l_flip->_data[y_xs+x]);
                       if(nsum > _thresGrad) nsum = _thresGrad;
                       pl_grad->_data[y_xs+x] = nsum;
                }
             }
         }

         CArray1D<double> * p  = new CArray1D<double>(xs * ys,0);
         CArray1D<double> * pl = new CArray1D<double>(xs * ys,0);
         for(int y = 0; y < ys; y++)
         {
             int y_xs = y * xs;
             for(int x =0; x < xs; x++)
             {
                 p->_data[y_xs+x]  = _gamma * pcolor->_data[y_xs+x]   + (1.0 - _gamma) * pgrad->_data[y_xs+x];
                 pl->_data[y_xs+x] = _gamma * pl_color->_data[y_xs+x] + (1.0 - _gamma) * pl_grad->_data[y_xs+x];
             }
        }
         dispVol[d] = p;
         dispVoll[d] = pl;

    } // end for d
   delete pcolor;
   delete pl_color;
   delete pgrad;
   delete pl_grad;

   emit send_back(20);


    CArray1D<double> * N = NULL;
    CArray1D<double3> * mean_I = NULL;
    std::vector< CArray2D<double> > * arrSigma = NULL;
    init_guidedfilter_inputs(Il, _r, _eps, N, mean_I, arrSigma, xs, ys);


    CArray1D<double> * Nf = NULL;
    CArray1D<double3> * mean_If = NULL;
    std::vector< CArray2D<double> > * arrSigmaf = NULL;
    init_guidedfilter_inputs(Il_flip, _r, _eps, Nf, mean_If, arrSigmaf, xs, ys);



   for (int d = 0 ; d < _num_disp; d++)
   {

        CArray1D<double> * p  =  dispVol[d];
        CArray1D<double> * p1 = dispVoll[d];

        CArray1D<double> *  q =   guidedfilter_color_fast(Il, p, _r, _eps, N, mean_I, arrSigma, xs,ys);
        CArray1D<double> *  q1 =  guidedfilter_color_fast(Il_flip, p1, _r, _eps, Nf, mean_If, arrSigmaf,xs,ys);

        delete p;
        dispVol[d] = q;

        // flip it
        for(int y = 0; y < ys; y++)
            for(int x =0; x < xs; x++)
                    p1->_data[y*xs+xs - x - 1] = q1->_data[y*xs+x];
        delete q1;
        dispVoll[d]= p1;

        emit send_back(20 + (d * 40 / _num_disp));

        if (_abort)
        {
            emit send_back(0);
            return;
        }


    }
   clear_guidedfilter_inputs(N, mean_I, arrSigma);
   clear_guidedfilter_inputs(Nf, mean_If, arrSigmaf);



   ///////////////////// end of second big loop

        double huge_const = 1000000;
        CArray1D<double> min_vals_left(xs*ys, huge_const );
        CArray1D<double> min_vals_right(xs*ys, huge_const );

        CArray1D<double> labels_left (xs*ys, 2*_num_disp);
        CArray1D<double> labels_right (xs*ys, 2*_num_disp);
       for (int d = 0 ; d < _num_disp; d++)
       {
            CArray1D<double> * p  =  dispVol[d];
            CArray1D<double> * p1 = dispVoll[d];

            if( (p == NULL) || (p1 == NULL) ) continue;

            for(int y = 0, off=0; y < ys; y++)
            {
                for(int x =0; x < xs; x++, off++)
                {
                    if( p->_data[off] < min_vals_left._data[off])
                    {
                        min_vals_left._data[off] = p->_data[off];
                        labels_left._data[off] = d;
                    }

                    if( p1->_data[off] < min_vals_right._data[off])
                    {
                        min_vals_right._data[off] = p1->_data[off];
                        labels_right._data[off] = d;
                    }
                }
            }

            if (_abort)
           {
                emit send_back(0);
                return;
            }

        }

      CArray1D<double> * final_labels = new CArray1D<double>(xs*ys, -1);
       for(int y = 0; y < ys; y++)
       {
           for(int x =0; x < xs; x++)
           {
               int index_r = x - labels_left._data[y*xs+x];
               if(index_r<0) index_r = 0;
               if( std::abs(labels_left._data[y*xs+x]- labels_right._data[y*xs+index_r])< 1.0)
                   final_labels ->_data[y*xs+x] = labels_left._data[y*xs+x];
           }
       }

       emit send_back(60);


       CArray1D<double> * res = final_labels;
       if(_postproc)
            res =  fillPixelsReference(Il, final_labels, xs, ys);

       //// use parabollic fitting

       for(int y = 0; y < ys; y++)
       {
           for(int x =0; x < xs; x++)
           {
               int depth = res->_data[y*xs+x];

               if(depth < 2)
                   continue;
               if(depth > (_num_disp - 2) )
                   continue;

               double f_di =  dispVol[depth]->_data[y*xs+x];
               double f_di_p1 =  dispVol[depth+1]->_data[y*xs+x];
               double f_di_m1 =  dispVol[depth-1]->_data[y*xs+x];

               // if descriminat is too small, dont make parabola
               double dep_add = (f_di_m1 - f_di_p1)/(2*(f_di_m1 - 2*f_di + f_di_p1));
               double ndepth = depth  + ((abs(dep_add) < 2)? dep_add:0);

               res->_data[y*xs+x] = ndepth ;
           }
       }

       for (int d = 0 ; d < _num_disp; d++)
       {
            CArray1D<double> * p  =  dispVol[d];
            CArray1D<double> * p1 = dispVoll[d];
            delete p;
            delete p1;
       }

       ////////////////////////////////////////////////////////////////////////////
       ////////////////////////////////////////////////////////////////////////////
       /**std::ofstream all_ch;
       all_ch.open ("all_chs.raw", std::ios::out| std::ios::binary);

       for (int y = 0; y < ys; y++)
         for (int x = 0; x < xs; x++ )
          {

             double color = res->_data[y*xs+x];
             all_ch.write((char*)&color,  sizeof(double));
          }
      all_ch.close();*/
      ////////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////


    //   simple visualisation
    double factor_xz =  10.0f;
    double factor_z = 1.5f;

      std::vector<C3DPoint> *  final_res = new std::vector<C3DPoint>;
      for (int y = 0; y < ys; y++)
      {
            for (int x = 0; x < xs; x++ )
            {

                C3DPoint p;
                p._x = double(x - xs/2) / factor_xz;
                p._y = double(ys - y)/factor_xz;
                p._z = res->_data[y*xs+x]*factor_z;

                QRgb c = _left_im->pixel(x,y);
                p._r = (float)qRed(c)/255.0f;
                p._g = (float)qGreen(c)/255.0f;
                p._b = (float)qBlue(c)/255.0f;

                p._nx = 0;
                p._ny = 0;
                p._nz = -1;

                if(res->_data[y*xs+x] != -1)
                    final_res->push_back(p);
            }
        }


        delete _left_im;
        delete _right_im;
        delete Il;
        delete Ir;
        delete Il_flip;
        delete Ir_flip;
        delete Il_g;
        delete Ir_g;
        delete fx_l;
        delete fx_r;
        delete fx_l_flip;
        delete fx_r_flip;

        delete res;

        emit send_result(final_res);
        emit send_back(100);


}



CArray1D<double> *  CDisparityCompThread::fillPixelsReference(CArray1D<double3> * Il,
                                       CArray1D<double> * final_labels, int xs, int ys)
{
    std::cout<< "fillPixelsReference " << xs << " " << ys << std::endl;
    CArray1D<double>  occPix (xs*ys);
    for(int y = 0; y < ys; y++)
        for(int x =0; x < xs; x++)
        {
            if(final_labels->_data[y*xs+x] < 0)
                occPix._data[y*xs+x] = 1;
            else
                occPix._data[y*xs+x] = 0;
        }

    CArray1D<double>  fillVals(ys, _num_disp);
    CArray1D<double>  curCol(ys, _num_disp);
    CArray1D<double> * final_labels_filled  = new CArray1D<double> (xs*ys);

    for(int x = 0; x < xs; x++)
    {
        for(int y = 0; y < ys; y++)
            curCol._data[y] = final_labels->_data[y*xs+x];

        for(int y = 0; y < ys; y++)
            if(curCol._data[y] == -1.0)
                curCol._data[y] = fillVals._data[y];

        // fillVals(curCol~=-1) = curCol(curCol~=-1);
        for(int y = 0; y < ys; y++)
            if(curCol._data[y] != -1.0)
                fillVals._data[y] = curCol._data[y];

        // final_labels_filled(:,col) = curCol;
         for(int y = 0; y < ys; y++)
             final_labels_filled->_data[y*xs+x] = curCol._data[y];
    }
    send_back(65);

    std::cout<< "3" << std::endl;

    CArray1D<double> * final_labels_filled1  = new CArray1D<double> (xs*ys);
    fillVals.clean_data(_num_disp);
    for(int x = xs - 1; x >= 0; x--)
    {
        for(int y = 0; y < ys; y++)
            curCol._data[y] = final_labels->_data[y*xs+x];
        for(int y = 0; y < ys; y++)
            if(curCol._data[y] == -1.0)
                curCol._data[y] = fillVals._data[y];
        for(int y = 0; y < ys; y++)
            if(curCol._data[y] != -1.0)
                fillVals._data[y] = curCol._data[y];
         for(int y = 0; y < ys; y++)
             final_labels_filled1->_data[y*xs+x] = curCol._data[y];
    }

    std::cout<< "4" << std::endl;

    for(int y = 0, off=0; y < ys; y++)
    {
        for(int x =0; x < xs; x++, off++)
        {
            double flf  = final_labels_filled  -> _data[off];
            double flf1 = final_labels_filled1 -> _data[off];
            final_labels->_data[off]  = (flf < flf1)? flf : flf1 ;
        }
    }
    delete final_labels_filled;
    delete final_labels_filled1;


    send_back(70);
    if (_abort)
    {
        emit send_back(0);
        return;
    }

     CArray1D<double> *  final_labels_smoothed = weightedMedian(Il,final_labels,
                                                                _r_median,_gamma_c,_gamma_d, xs, ys);

    for(int y = 0, off=0; y < ys; y++)
    {
        for(int x =0; x < xs; x++,off++)
            if( occPix._data[off] == 1)
                final_labels->_data[off] = final_labels_smoothed->_data[off];
    }
    delete final_labels_smoothed;

    send_back(95);

    return final_labels;

}

CArray1D<double> *  CDisparityCompThread::weightedMedian(CArray1D<double3> * Il,
                                   CArray1D<double> * disp_img, int winsize, double gamma_c, double gamma_p, int xs, int ys)
{

    CArray1D<double> * redIl = new CArray1D<double> (xs*ys);
    CArray1D<double> * greIl = new CArray1D<double> (xs*ys);
    CArray1D<double> * bluIl = new CArray1D<double> (xs*ys);

    for(int y = 0, off=0; y < ys; y++)
    {
        for(int x =0; x < xs; x++, off++)
        {
            redIl->_data[off] = Il->_data[off].r;
            greIl->_data[off] = Il->_data[off].g;
            bluIl->_data[off] = Il->_data[off].b;
        }
    }

    CArray1D<double> * redIl_smoo = medfilt2(redIl, xs, ys);
    CArray1D<double> * greIl_smoo = medfilt2(greIl, xs, ys);
    CArray1D<double> * bluIl_smoo = medfilt2(bluIl, xs, ys);

    delete redIl;
    delete greIl;
    delete bluIl;

    int radius = winsize/2;

    CArray1D<double> * medianFiltered = new CArray1D<double> (xs*ys);


    CArray2D<double>  weights(winsize, winsize, 0);
    int wxs =0, wys = 0;

    for(int y = 0; y < ys; y++)
    {
        for(int x =0; x < xs; x++)
        {
            filtermask(&weights, &wxs, &wys,  redIl_smoo, greIl_smoo, bluIl_smoo, x, y, winsize,gamma_c,gamma_p, xs, ys);


            int left   = std::max(0,      x-radius);
            int right  = std::min(xs-1,   x+radius);
            int bottom = std::max(0,      y-radius);
            int top    = std::min(ys - 1, y+radius);

            double maxDispVal = disp_img->_data[y*xs+x];
            for(int v = bottom; v <= top; v++)
                for(int u =left; u <= right; u++)
                    if(disp_img->_data[v*xs+u]> maxDispVal )
                        maxDispVal = disp_img->_data[v*xs+u];

            /// compute histogramm
            CArray1D<double> hist(maxDispVal + 1, 0);
            double hist_sum = 0;
            for(int v = bottom, vw = 0; v <= top; v++, vw++)
            {
                for(int u =left, uw = 0; u <= right; u++, uw++)
                {
                    hist._data[(int) disp_img->_data[v*xs+u] ] += weights._data[vw][uw];
                    hist_sum  += weights._data[vw][uw];
                }
            }
            double hist_sum_div_2 = hist_sum/2;

            double hist_cumsum = hist._data[0];
            for(int h = 1; h < hist._xs; h++)
            {
                hist_cumsum += hist._data[h];
                if(hist_cumsum > hist_sum_div_2 )
                {
                    medianFiltered->_data[y*xs+x] = h;
                    break; // return
                }
            } // h


        } // x

      send_back(70 + (y * 25 / ys));
        if (_abort)
        {
            emit send_back(0);
            return;
        }


    } // y

    delete redIl_smoo;
    delete greIl_smoo;
    delete bluIl_smoo;

    return medianFiltered;
}
