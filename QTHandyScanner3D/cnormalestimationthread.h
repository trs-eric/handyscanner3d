#ifndef CNORMALESTIMATIONTHREAD_H
#define CNORMALESTIMATIONTHREAD_H

#include <QThread>
#include <QMutex>

class CPointCloud;
class CNormalCloud;

class CNormalEstimationThread : public QThread
{
    Q_OBJECT
public:
    explicit CNormalEstimationThread(QObject *parent = 0);
    ~CNormalEstimationThread();

    void start_process(CPointCloud *, int, bool);
    bool is_abort(){return _abort;}
    void emit_send_back(int i);

signals:
    void send_back(const int val);
    void send_result(CNormalCloud *);

public slots:
    void stop_process();

protected:
    void run();

private:
    bool    _abort;
    QMutex  _mutex;
    int _ncount;
    bool _flip;
    CPointCloud * _in_cloud;
};

#endif // CNORMALESTIMATIONTHREAD_H
