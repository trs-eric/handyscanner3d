#ifndef CGLSCENE_H
#define CGLSCENE_H

#include<vector>
#include <QString>

#include "cglcompoundobject.h"
#include "ccamerapose.h"
#include "menu_vew_actions.h"

class CCameraTrack;
class CGLSampleTrivTable;
class CGLCamera;
class QWidget;
class CSceneExplorerManager;
class CGLWidget;
class RadianceScalingRendererPlugin;

class CGLScene
{
public:
    CGLScene(QWidget * treew, MenuActions * va, CGLWidget * glw);
    virtual ~CGLScene();

     CSceneExplorerManager * get_se_manager(){return _se_manager;}

    void GLRenderScene();
    void GLRenderBackground();
    void GLSetupLight();
    void render_selection_frame();

    bool ImportScene(QString &fname);
    void ExportScene(QString &fname);
    CGLCompoundObject * ConstructSingleScan(std::vector<C3DPoint> & pnts);
    bool ConstructNextScan(std::vector<C3DPoint> & pnts);

    void PrepaireNewScan();
    void ConstructNextScanRight(std::vector<C3DPoint> & pnts);
    void ConstructNextScanLeft(std::vector<C3DPoint> & pnts);
    void ConstructNextTextureRight(CRawTexture *);
    void ConstructNextTextureLeft (CRawTexture *);
    void CleanCurrentScan();

    void UpdateCameraTrack();
    void UpdateTable();
    void ResetCameraPos();

    void ConstructNextMeshCast(int, CTriMesh *);
    CGLCompoundObject * ConstructEmptyScan();


    bool load_from_rub_file(QString &fname);
    void load_scene_gl_settings_from_rub_file(std::ifstream & ifile);
    void finilize_loading_from_rub();

    void save_into_rub_file(QString &fname);
    void save_scene_gl_settings_into_rub_file(std::ofstream & ofile);

    inline CGLCamera * get_dim_reference(){return _camera;}
    void update_referece_dependant_glRepo();

    void set_raw_poins_of_current_object(CPointCloud *);
    CPointCloud * get_raw_poins_of_current_object();
    void discard_raw_points_of_current_object();
    void update_all_raw_points();
    void delete_selected_points();

    void set_optim_poins_of_current_object(CPointCloud *);
    CPointCloud * get_optim_points_of_current_object();
    void discard_optim_points_of_current_object();

    void set_normal_poins_of_current_object(CNormalCloud *);
    CNormalCloud * get_normals_of_current_object();
    void discard_normal_points_of_current_object();

    void set_mesh_of_current_object(CTriangularMesh *);
    CTriangularMesh * get_mesh_of_current_object();
    CTriangularMesh * get_fused_mesh();
    void discard_mesh_of_current_object();
    void update_mesh_of_current_object();


    void set_dpcl_of_current_object(std::vector<C3DPoint> * pnts);
    std::vector<C3DPoint> * get_dpcl_of_current_object();

    std::vector<C3DPoint> * get_merged_dpcl_of_scene();

    void change_current_cobject(CGLCompoundObject *);
    void chainge_view_mode_of_current_object(CGLCompoundObject::ViewingMode vm);
    void chainge_mesh_rmode_of_current_object(CTriangularMesh::RenderingMode rm);
    void set_view_camera_path(bool);
    void set_view_shad_points(bool v);

    void update_selection_state(QTreeWidgetItem *, int);
    void update_surface_state(QTreeWidgetItem *, int);
    void update_scan_visability_state(QTreeWidgetItem *, int);
    bool is_romove_action(QTreeWidgetItem * , int );
    void update_remove_scan_state(QTreeWidgetItem * , int );
    void on_single_click_treew_switch(QTreeWidgetItem *, int);

    std::vector<CGLCompoundObject*> get_list_of_valid_meshes();
    CGLCompoundObject* get_current_object(){return _currentCObject;}

    std::vector<CGLCompoundObject*> get_list_of_all_cglobjects() {return _cglobjects;}

    void create_new_cobject(CTriangularMesh *);

    void create_unique_cobject(CTriangularMesh *);
    void delete_unique_cobject();

    void create_new_cglobject(std::vector<C3DPoint> *pcl, CTriangularMesh *mesh, float, float, float);

    void add_new_globj(CGLObject* obj){ _gl_objects.push_back(obj);};

    void delete_all_scans(){DeleteObjects();}

     RadianceScalingRendererPlugin * get_radiance_render(){return _fancy_render;}

     void set_selection_frame(int x, int y, int w, int h);
     void set_empty_selection();
     void extend_poly_selection(cv::Point2f p){_selection_poly.push_back(p);}
     void set_selection_mode(bool v);
     void set_selection_poly(bool v){_sel_poly = v;}
     void update_selection_points(CPointCloud::SelectMode sm);
     bool is_there_selected_raw_points();
     void process_selected_faces();

public:

    bool _disable_raw_points_render;
    bool _disable_decim_surface_render;
    bool _disable_fused_surface_render;


protected:
    void CreateScene();
    void DeleteScene();
    void DeleteObjects();

    std::vector<CGLCompoundObject *> _cglobjects;
    CGLCompoundObject * _currentCObject;

    // temporal references to left and right scan
    CGLCompoundObject * _cobj_left;
    CGLCompoundObject * _cobj_right;

    CGLSampleTrivTable * _table;
    CCameraTrack * _ctrack;

    CGLCamera * _camera;

    QWidget * _treew;
    MenuActions * _va;

    std::vector<CGLObject*> _gl_objects;

    CSceneExplorerManager * _se_manager;

    CGLWidget * _glw;
    RadianceScalingRendererPlugin * _fancy_render;

    GLint _selection_frame[4]; // x0, y0, w, h
    bool _do_selection;

    bool _sel_poly;
    std::vector<cv::Point2f> _selection_poly;

};

#endif // CGLSCENE_H
