#ifndef CLASERSCANONLINEDLG_H
#define CLASERSCANONLINEDLG_H

#include <QDialog>
#include <QLabel>
#include <QScrollBar>
#include <QTimer>


#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <QtSerialPort/QSerialPort>
#include "graphicutils.h"

#include "./driver/driver_aux_util.h"

#include "cdevcontrolthread.h"

/// RUNETAG block
#include "runetag.hpp"
#include "auxrenderer.hpp"
#include "ellipsefitter.hpp"
#include "markerpose.hpp"
#include "coding.h"

class CGLWidget;
class CCalibLaserAngleDlg;
class CUSBCameraSettings;
namespace Ui {
class CLaserScanOnlineDlg;
}

class CLaserScanOnlineDlg : public QDialog
{
    Q_OBJECT

public slots:
  void on_stop_preview();
private slots:

    void set_camera(int );
    void update_form_image();
    void update_acc();
    void grab_image();
    void change_opacity();
    void corwndsz_changed(int );
    void edge_thresh_changed(int );
    void eLED_changed(int v);
    void change_find_edges();
    void grab_sequence();
    void cam_callibration();
    void laser_callibration();

    void i2c_laser_OFF();
    void i2c_laser_ON();
    void i2c_motor_left_pressed();
    void i2c_motor_right_pressed();
    void i2c_motor_stop();
    void i2c_turn_table();

    void cam_settings_dlg();


    void on_start_preview();
    void on_update_main_preview(cv::Mat & frame);

    void update_tbl_marks();

signals:
    void update_child_lpos();

public:
    explicit CLaserScanOnlineDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CLaserScanOnlineDlg();
    void update_from_matimage(cv::Mat & frame);

private slots:
    void set_zoom_100();
    void set_zoom_50();
    void set_zoom_25();

protected:
    void showEvent(QShowEvent * event);
    void hideEvent(QHideEvent * event);
    void camera_on();
    void camera_off();
    QString serial_backinfo();

    void init_device();
    void free_device();
    bool check_device_alive();
    void do_find_runetag_in_frame(cv::Mat & frame,  bool salient=false);
    bool check_callibration();



private:
    void draw_mark( QImage & texture, float cx, float cy);
    std::vector<cv::Point2f> _corners;

    Ui::CLaserScanOnlineDlg *ui;
    QLabel * _tex_label;

    float _scale_factor;

    void adjust_scroll_bar(QScrollBar *scrollBar, float factor);

    CGLWidget *_glWidget;

    std::vector<int> _web_camera_list;
    int _current_camera_idx;
    cv::VideoCapture * _current_cam;
    QTimer * _timer;
    int _timer_period;

    QImage _bckg;

    PVOID _dev_handle;
    QThread * _dev_ctrl_thread;
    CDevControlThread * _dev_ctrl;

     CUSBCameraSettings * _cam_setdlg;
     float _board_distance;
     float _roll;
     float _pitch;

     cv::runetag::MarkerDetector  *  _pat_detector;
     cv::runetag::EllipseDetector *  _ellipseDetector;

     bool _valid_tt_marks;
     bool _valid_tt_dots;

     bool _update_tt_marks;
     cv::runetag::Pose _RT;
     cv::runetag::MarkerDetected _tbl_markers;
     double _rt_x_incline;
     double _rt_y_incline;
     double _rt_rotCX;
     double _rt_rotCY;
     double _rt_rotCZ;
     double _rt_error;

     CCalibLaserAngleDlg * _laser_calib_dlg;
     bool _in_calibration_mode;

};

#endif // CLASERSCANONLINEDLG_H
