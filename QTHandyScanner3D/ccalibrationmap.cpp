#include "ccalibrationmap.h"
#include "settings/psettings.h"
#include <QFile>

CCalibrationMap::CCalibrationMap()
{
}

void CCalibrationMap::set_callibration_data(int focus, cv::Mat & cm, cv::Mat & dc)
{
    _map[focus] = FocusData(focus, cm, dc);
    g_cam_intrinsics = cm;
    g_cam_distortion = dc;
    save();
}

bool CCalibrationMap::settings_for_focus(int focus)
{
    std::map<int, FocusData>::iterator it = _map.find(focus);
    if( it !=  _map.end())
        return true;
    else
        return false;
}


bool CCalibrationMap::empty()
{
    return (_map.size() == 0);
}


bool CCalibrationMap::set_globals_for_focus(int focus)
{
    std::map<int, FocusData>::iterator it = _map.find(focus);
    if( it !=  _map.end())
    {
        g_cam_intrinsics = it->second._cm;
        g_cam_distortion = it->second._dc;
    }
}

int CCalibrationMap::which_focus_indx(int focus)
{
    std::map<int, FocusData>::iterator it = _map.begin();
    for( int i = 0 ; it !=  _map.end() ; i++, it++)
        if(it->first == focus)
            return i;
    return 0;
}

int CCalibrationMap::set_globals_for_num(int idx)
{
    std::map<int, FocusData>::iterator it = _map.begin();
    for( int i = 0 ; it !=  _map.end() ; i++, it++)
    {
        if(i == idx )
        {
            g_cam_intrinsics = it->second._cm;
            g_cam_distortion = it->second._dc;
            return it->first;
        }
    }

    return 0;

}

void CCalibrationMap::save()
{
    QFile(CGlobalSettings::_cam_xml_name).remove();

    cv::FileStorage fs(CGlobalSettings::_cam_xml_name.toStdString(), cv::FileStorage::WRITE);

    std::map<int, FocusData>::iterator it = _map.begin();
    for ( ; it != _map.end(); it++ )
        it->second.save(fs);


    fs << "focuslist" << "[";
    for (  it = _map.begin(); it != _map.end(); it++ )
        fs << it->first;
    fs << "]";
    fs.release();
}


void CCalibrationMap::load(QString & f)
{
     if(!QFile(f).exists()) return;

    cv::FileStorage fs(f.toStdString(), cv::FileStorage::READ);
    cv::FileNode flist  = fs["focuslist"];
    cv::FileNodeIterator it = flist.begin();

    std::vector<int> fvals;
    for(int i = 0; it != flist.end(); it++, i++)
    {
        fvals.push_back((int)(*it));
    }

     std::vector<int>::iterator itf = fvals.begin();
     for( ; itf != fvals.end(); itf++ )
     {
         int cfocus = *itf;

         cv::Mat cm;
         QString cm_nm = QString("CM_%1").arg(QString::number(cfocus));
         fs[cm_nm.toStdString()] >> cm;
         g_cam_intrinsics = cm;

         cv::Mat dc;
         QString dc_nm = QString("DC_%1").arg(QString::number(cfocus));
         fs[dc_nm.toStdString()] >> dc;
         g_cam_distortion = dc;

         _map[cfocus] = FocusData(cfocus, cm, dc);
     }

}


void FocusData::save( cv::FileStorage & fs)
{
    QString cm_nm = QString("CM_%1").arg(QString::number(_focus));
    fs << cm_nm.toStdString() << _cm;

    QString dc_nm = QString("DC_%1").arg(QString::number(_focus));
    fs << dc_nm.toStdString() << _dc;

}


