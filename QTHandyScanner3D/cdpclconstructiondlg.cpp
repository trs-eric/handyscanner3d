#include "cdpclconstructiondlg.h"
#include "ui_cdpclconstructiondlg.h"

#include <QMessageBox>

#include "cdpclconstructionthread.h"
#include "cframe.h"
#include "messages.h"
#include "cglwidget.h"

CDPCLConstructionDlg::CDPCLConstructionDlg(CGLWidget * glWidget, QWidget *parent) :
    QDialog(parent), _glWidget(glWidget),
    ui(new Ui::CDPCLConstructionDlg)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);

    // default ui settings
    ui->progressBar->setMaximum(100);
    ui->edtDecimDPCL->setText(QString::number(5));
    ui->edtNeigh->setText(QString::number(1));


    ///connect(ui->previewBtn, SIGNAL(clicked()), this, SLOT(onPreviewBtn()));
    connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));

    _thread = new CDPCLConstructionThread;

    // user pressed cancel button
    connect(ui->cancelBtn,  SIGNAL(clicked()), _thread, SLOT(stop_process()));
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

    // working with a thread
    connect(_thread, SIGNAL(send_back(int)), this, SLOT(onMakeProgressStep(int)));
    connect(_thread, SIGNAL(send_dpcl(std::vector<C3DPoint>*)), this, SLOT(onCompleteProcessing(std::vector<C3DPoint>*)));

    /// adding new point cloud as separate compound object
    ///connect(_thread, SIGNAL(send_dpcl(std::vector<C3DPoint>*)), _glWidget, SLOT(construct_new_scan(std::vector<C3DPoint>*)));
    connect(_thread, SIGNAL(send_dpcl(std::vector<C3DPoint>*)), _glWidget, SLOT(on_dpcl_computed(std::vector<C3DPoint>*)));
    connect(_thread, SIGNAL(send_newcam()), _glWidget, SLOT(on_new_campos()));

    connect(_thread, SIGNAL(finished()), this, SLOT(onStopProcessing()));


}

CDPCLConstructionDlg::~CDPCLConstructionDlg()
{
    delete ui;
    delete _thread;
}

void CDPCLConstructionDlg::onMakeProgressStep(int val)
{
    ui->progressBar->setValue(val);
}

void CDPCLConstructionDlg::onCompleteProcessing(std::vector<C3DPoint>*)
{
   /* if(m == NULL)
    {
        QMessageBox::warning(this, tr("Warning"),wrn_FailedMesh);
        return;
    }*/

    if(_on_apply)
    {
        close_dialog();
        return;
    }

    _is_result_valid = true;
    ui->progressBar->setValue(100);
}

void CDPCLConstructionDlg::onStopProcessing()
{
    ui->applyBtn    ->setEnabled(true);
    ///ui->previewBtn  ->setEnabled(true);
    ui->edtDecimDPCL->setEnabled(true);
    ui->edtNeigh->setEnabled(true);

    _in_process = false;
}

void CDPCLConstructionDlg::close_dialog()
{
    _on_apply = false;
    close();
}

void CDPCLConstructionDlg::onCancelBtn()
{
    if(!_in_process)
        close_dialog();

    _in_process = false;
    _on_apply = false;
}

void CDPCLConstructionDlg::onPreviewBtn()
{
    if(global_keyframes_sequence.size() == 0)
    {
       QMessageBox::warning(this, tr("Warning"),wrn_NoSPCL);
       return;
    }

    ui->applyBtn    ->setEnabled(false);
    ///ui->previewBtn  ->setEnabled(false);
    ui->edtDecimDPCL->setEnabled(false);
    ui->edtNeigh    ->setEnabled(false);



    _in_process = true;
   _thread->start_process( ui->edtDecimDPCL->text().toInt(),ui->edtNeigh->text().toInt());


}

void CDPCLConstructionDlg::onApplyBtn()
{
    _on_apply = true;
    if(!_is_result_valid)
        onPreviewBtn();
    else
        close_dialog();
}

void CDPCLConstructionDlg::showEvent(QShowEvent * event)
{
    ui->progressBar->setValue(0);
    _is_result_valid    = false;
    _on_apply           = false;
    _in_process         = false;
}


