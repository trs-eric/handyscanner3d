#ifndef CDISPARITYCOMPCLASSICDLG_H
#define CDISPARITYCOMPCLASSICDLG_H

#include <QDialog>

#include "graphicutils.h"

class CGLWidget;
class CDisparityCompClassicThread;
class CPointCloud;

namespace Ui {
class CDisparityCompClassicDlg;
}

class CDisparityCompClassicDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CDisparityCompClassicDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CDisparityCompClassicDlg();

protected:
    void showEvent(QShowEvent * event);


    bool _in_process;
    bool _on_apply;
    bool _is_result_valid;
    CDisparityCompClassicThread * _thread;

private:
    Ui::CDisparityCompClassicDlg *ui;

    CGLWidget *_glWidget;

private slots:
    void onCancelBtn();
    void onPreviewBtn();
    void onApplyBtn();

    void onMakeProgressStep(int val);
    void onCompleteProcessing(std::vector<C3DPoint> *  );
    void onStopProcessing();

    void close_dialog();
};

#endif // CDISPARITYCOMPCLASSICDLG_H
