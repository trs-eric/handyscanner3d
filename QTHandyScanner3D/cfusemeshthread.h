#ifndef CFUSEMESHTHREAD_H
#define CFUSEMESHTHREAD_H

#include <QThread>
#include <QMutex>
#include "carray2d.h"

class CTriangularMesh;
class CTriMesh;
struct aux_pix_data;
class CFuseMeshThread : public QThread
{
    Q_OBJECT
public:
    explicit CFuseMeshThread(QObject *parent = 0);
    ~CFuseMeshThread();

    void start_process(int , float  );
    bool is_abort(){return _abort;}

signals:
    void send_back(const int val);
    void send_result_fuse(CTriangularMesh *);

protected:
    void run();
    CTriMesh * construct_mesh_from_single_shot(CArray2D<aux_pix_data *> & merged_data, int decimstep);
    CTriMesh * construct_mesh_from_single_shot_round(CArray2D<aux_pix_data *> & merged_data, int decimstep);

    float compute_dist_pnt(aux_pix_data * p0, aux_pix_data *p1);
    void fill_gaps_in_merged_data_triv(CArray2D<aux_pix_data *> * merged_data);
    CArray2D<aux_pix_data *> * simplify_polar_map(CArray2D< std::list<aux_pix_data *> > * polar_map);

public slots:
    void stop_process();

private:
    bool    _abort;
    QMutex  _mutex;

    int _decim_step;
    float _max_side_len;
};

#endif // CFUSEMESHTHREAD_H
