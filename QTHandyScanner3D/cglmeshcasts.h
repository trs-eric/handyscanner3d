#ifndef CGLMESHCASTS_H
#define CGLMESHCASTS_H

#include <vector>
#include <map>
#include <opencv2/core/core.hpp>
#include "ctriangularmesh.h"

class CGLMeshCasts
{
public:
    CGLMeshCasts();
    virtual ~CGLMeshCasts();
    void add_new_meshcast(int, CTriMesh *);
    void glDraw();

    void set_rendering_mode(CTriangularMesh::RenderingMode rm);
    CTriangularMesh::RenderingMode get_rendering_mode();
protected:
    std::map<int, CTriangularMesh*> _mcasts;

    std::map<int, cv::Point3f> _colormap;
    int _cmap_size;

    void init_color_map();
};

#endif // CGLMESHCASTS_H
