/****************************************************************************
* Render Radiance Scaling                                                   *
* Meshlab's plugin                                                          *
*                                                                           *
* Copyright(C) 2010                                                         *
* Vergne Romain, Dumas Olivier                                              *
* INRIA - Institut Nationnal de Recherche en Informatique et Automatique    *
*                                                                           *
* All rights reserved.                                                      *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License (http://www.gnu.org/licenses/gpl.txt)          *
* for more details.                                                         *
*                                                                           *
****************************************************************************/
#include "radianceScalingRenderer.h"
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <QGLWidget>
#include <QTextStream>
#include <QResource>

using namespace std;

#include "../MeshLab_133src/vcglib/wrap/gl/space.h"
//#include <vcglib/wrap/gl/space.h>
//using namespace vcg;

#include "settings/psettings.h"

#define GL_TEST_ERR							\
  {									\
   GLenum eCode;							\
   if((eCode=glGetError())!=GL_NO_ERROR)				\
     std::cerr << "OpenGL error : " <<  gluErrorString(eCode) << " in " <<  __FILE__ << " : " << __LINE__ << std::endl;	\
 }

RadianceScalingRendererPlugin::RadianceScalingRendererPlugin()
  : _supported(false), _turned(false),
    _fbo(NULL),
    _buffPass(NULL),
    _rsPass(NULL),
    _depthTex(NULL),
    _gradTex(NULL),
    _normTex(NULL),
    _colorTex(NULL),
    _convexLS(NULL),
    _concavLS(NULL)
{
    _enhancement = 0.5;
    _transition = 0.5;
    _invert = false;
    _convexLSfilename = QString(":/rsrender/render_radiance_scaling/litSpheres/s1.png");
    _concavLSfilename = QString( ":/rsrender/render_radiance_scaling/litSpheres/s2.png");

}

RadianceScalingRendererPlugin::~RadianceScalingRendererPlugin()
{
    Finalize();
}

int
isExtensionSupported(const char *extension)
{
  const GLubyte *extensions = NULL;
  const GLubyte *start;
  GLubyte *where, *terminator;

  /* Extension names should not have spaces. */
  where = (GLubyte *) strchr(extension, ' ');
  if (where || *extension == '\0')
    return 0;
  extensions = glGetString(GL_EXTENSIONS);
  /* It takes a bit of care to be fool-proof about parsing the
     OpenGL extensions string. Don't be fooled by sub-strings,
     etc. */
  start = extensions;
  for (;;) {
    where = (GLubyte *) strstr((const char *) start, extension);
    if (!where)
      break;
    terminator = where + strlen(extension);
    if (where == start || *(where - 1) == ' ')
      if (*terminator == ' ' || *terminator == '\0')
        return 1;
    start = terminator;
  }
  return 0;
}

void RadianceScalingRendererPlugin::Init(  QGLWidget *gla)
{

   gla->makeCurrent();
    GLenum out = glewInit();
    std::cout << "glewInit "<< out << std::endl;

    if(!isExtensionSupported("GL_EXT_gpu_shader4"))
    {
        std::cout << "GL_EXT_gpu_shader4 is not supported" << std::endl;
        _supported = false;
        return;
    }

  GL_TEST_ERR

  /* if(GLEW_ARB_vertex_program)
          std::cout << "GLEW_ARB_vertex_program" << std::endl;
  if(GLEW_ARB_fragment_program)
            std::cout << "GLEW_ARB_fragment_program" << std::endl;
   else
      std::cout << "NOOOO GL_ARB_draw_buffers" << std::endl;

  if(GLEW_ARB_texture_float)
            std::cout << "GLEW_ARB_texture_float" << std::endl;
  if(GLEW_EXT_framebuffer_object)
            std::cout << "GLEW_EXT_framebuffer_object" << std::endl;*/


  if(!GLEW_ARB_vertex_program   ||
     !GLEW_ARB_fragment_program ||
     !GLEW_ARB_texture_float    ||
     !GLEW_ARB_draw_buffers     ||
     !GLEW_EXT_framebuffer_object)
  {
      std::cout << "dont found required by redned extensions" << std::endl;
    _supported = false;
    return;
  }
  else
  {
      std::cout << "everething ok" << std::endl;

  }
    GL_TEST_ERR

    _supported = true;

    createLit(_convexLSfilename,0);
    createLit(_concavLSfilename,1);


    initFBOs();
    initShaders();
    GL_TEST_ERR
}


void RadianceScalingRendererPlugin::turn(bool on)
{
    if(!_supported) _turned = false;
    else _turned = on;
}

void RadianceScalingRendererPlugin::starting_rendering()
{
    checkViewport();

    // first pass: buffers
    _fbo->bind();
    glDrawBuffers(3,FramebufferObject::buffers(0));
//     glClearColor(1.0,0.0,0.0,1.0);
     glClearColor(CGlobalHSSettings::gcolor_sr, CGlobalHSSettings::gcolor_sg, CGlobalHSSettings::gcolor_sb, 1.0f);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    vcg::glColor(vcg::Color4b::LightGray);
    _buffPass->enable();

}

void RadianceScalingRendererPlugin::ending_rendering()
{
    _buffPass->disable();

    // second pass: descriptor, radiance scaling, lighting
    FramebufferObject::unbind();
    swapToScreenMode();
    _rsPass->enable();
    drawQuad();
    _rsPass->disable();
    swapToWorldMode();
}



void RadianceScalingRendererPlugin::Render(/*QAction *, MeshDocument &md, QMap<int,RenderMode>&rm, QGLWidget **/)
{

    ////////  http://qt.apidoc.info/5.1.1/qtopengl/framebufferobject2-glwidget-cpp.html
    checkViewport();

     // first pass: buffers
     _fbo->bind();
     glDrawBuffers(3,FramebufferObject::buffers(0));
    // glClearColor(1.0,0.0,0.0,0.0);
     glClearColor(50./255.,50./255.,66./255.,1.0);

     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      vcg::glColor(vcg::Color4b::LightGray);
     _buffPass->enable();
    /* foreach(MeshModel *mp,md.meshList) {
         QMap<int,RenderMode>::iterator it = rm.find(mp->id());
         if((mp->visible) && (it != rm.end()))
             mp->render(it.value().drawMode,it.value().colorMode,it.value().textureMode);
     }*/

     glEnable(GL_COLOR_MATERIAL);

     glPushMatrix();
         glTranslatef(0,0, -10);
         glRotatef(45, 0,1, 0);
         glRotatef(30, 1,0, 0);

         glScalef(2, 2, 2);
         // glCallList(_oglid);


         // Белая сторона - ЗАДНЯЯ
         glBegin(GL_POLYGON);
         glColor3f(   .2,  .4, 0.8 );
         glNormal3f(   0.0,  0.0, 1.0 );
         glVertex3f(  0.5, -0.5, 0.5 );
         glVertex3f(  0.5,  0.5, 0.5 );
         glVertex3f( -0.5,  0.5, 0.5 );
         glVertex3f( -0.5, -0.5, 0.5 );
         glEnd();
    glPopMatrix();
    glDisable(GL_COLOR_MATERIAL);

     _buffPass->disable();

     // second pass: descriptor, radiance scaling, lighting
     FramebufferObject::unbind();
     swapToScreenMode();
     _rsPass->enable();
     drawQuad();
     _rsPass->disable();
     swapToWorldMode();
}


void RadianceScalingRendererPlugin::Finalize()
{
  cleanShaders();
  cleanFBOs();

  if(_convexLS!=NULL)
  {
    delete _convexLS;
    _convexLS = NULL;
  }

  if(_concavLS!=NULL)
  {
    delete _concavLS;
    _concavLS = NULL;
  }
}


void RadianceScalingRendererPlugin::createLit(const QString &filename,int type) {
  QImage t;
  QImage b;

  if(!b.load(filename)) {
    return;
  }

  t = QGLWidget::convertToGLFormat(b);

  if(type==0)
  {
    if(_convexLS!=NULL)
    {
      delete _convexLS;
      _convexLS = NULL;
    }
    _convexLS = new UbyteTexture2D(TextureFormat(GL_TEXTURE_2D,t.width(),t.height(),3,GL_RGBA,GL_UNSIGNED_BYTE),TextureParams(GL_LINEAR,GL_LINEAR),t.bits());
  }
  else
  {
    if(_concavLS!=NULL)
    {
      delete _concavLS;
      _concavLS = NULL;
    }
    _concavLS = new UbyteTexture2D(TextureFormat(GL_TEXTURE_2D,t.width(),t.height(),3,GL_RGBA,GL_UNSIGNED_BYTE),TextureParams(GL_LINEAR,GL_LINEAR),t.bits());
  }
}

void RadianceScalingRendererPlugin::initShaders(bool reload)
{
  if(!reload)
  {
    string path = ":/rsrender/render_radiance_scaling/shaders/";
    delete _buffPass;
    delete _rsPass;
    _buffPass = new GPUProgram(path+"01_buffer.vs",path+"01_buffer.fs");
    _rsPass   = new GPUProgram(path+"02_rs.vs"  ,path+"02_rs.fs");
    GL_TEST_ERR

    _rsPass->enable();
    _rsPass->addUniform("sw");
    _rsPass->addUniform("sh");
    _rsPass->addUniform("enhancement");
    _rsPass->addUniform("transition");
    _rsPass->addUniform("enabled");
    _rsPass->addUniform("invert");
    _rsPass->addUniform("twoLS");
    _rsPass->addUniform("display");
    _rsPass->addUniform("grad");
    _rsPass->addUniform("norm");
    _rsPass->addUniform("colormap");
    _rsPass->addUniform("convexLS");
    _rsPass->addUniform("concavLS");
    _rsPass->disable();

    GL_TEST_ERR
  }
  else
  {
    _buffPass->reload();
    _rsPass->reload();
    GL_TEST_ERR
  }
 // check _w and _h here
  ///std::cout << "screen params " << _w << " " << _h << std::endl;

  float sw  = 1.0f/(float)_w;
  float sh  = 1.0f/(float)_h;

  _rsPass->enable();
  _rsPass->setUniform1f("sw",sw);
  _rsPass->setUniform1f("sh",sh);
  _rsPass->setUniform1f("enhancement", _enhancement); // (+) slider
  _rsPass->setUniform1f("transition", _transition ); // (+) slider
  _rsPass->setUniform1i("enabled", true ); // (+) endable radial scal render
  _rsPass->setUniform1i("display", 1); // mode 1 ftom [0,1,2,3]
  _rsPass->setUniform1i("invert",_invert);
  _rsPass->setUniform1i("twoLS", true );
  _rsPass->setUniformTexture("convexLS",0,_convexLS->format().target(),_convexLS->id());
  _rsPass->setUniformTexture("concavLS",1,_concavLS->format().target(),_concavLS->id());
  _rsPass->setUniformTexture("grad",2,_gradTex->format().target(),_gradTex->id());
  _rsPass->setUniformTexture("norm",3,_normTex->format().target(),_normTex->id());
  _rsPass->setUniformTexture("colormap",4,_colorTex->format().target(),_colorTex->id());

  _rsPass->disable();
  GL_TEST_ERR
}

void RadianceScalingRendererPlugin::initFBOs()
{
    int v[4];
    glGetIntegerv(GL_VIEWPORT,v);
    _w = v[2];
    _h = v[3];

    if(_fbo==NULL)
    {
        GLenum filter = GL_LINEAR;
        GLenum format = GL_RGBA16F_ARB;
        _fbo = new FramebufferObject();
        _depthTex = new FloatTexture2D(TextureFormat(GL_TEXTURE_2D,_w,_h,GL_DEPTH_COMPONENT24,GL_DEPTH_COMPONENT,GL_FLOAT),TextureParams(filter,filter));
        _gradTex  = new FloatTexture2D(TextureFormat(GL_TEXTURE_2D,_w,_h,format,GL_RGBA,GL_FLOAT),TextureParams(filter,filter));
        _normTex  = new FloatTexture2D(_gradTex->format(),_gradTex->params());
        _colorTex  = new UbyteTexture2D(TextureFormat(GL_TEXTURE_2D,_w,_h,3,GL_RGBA,GL_UNSIGNED_BYTE),TextureParams(GL_LINEAR,GL_LINEAR));//new FloatTexture2D(_gradTex->format(),_gradTex->params());
    }

  _fbo->bind();
  _fbo->unattachAll();
  _depthTex->bind();
  _fbo->attachTexture(_depthTex->format().target(),_depthTex->id(),GL_DEPTH_ATTACHMENT_EXT);
  _gradTex->bind();
  _fbo->attachTexture(_gradTex->format().target(),_gradTex->id(),GL_COLOR_ATTACHMENT0_EXT);
  _normTex->bind();
  _fbo->attachTexture(_normTex->format().target(),_normTex->id(),GL_COLOR_ATTACHMENT1_EXT);
  _colorTex->bind();
  _fbo->attachTexture(_colorTex->format().target(),_colorTex->id(),GL_COLOR_ATTACHMENT2_EXT);
  _fbo->isValid();

  FramebufferObject::unbind();
}

void RadianceScalingRendererPlugin::cleanShaders()
{
  if(_buffPass!=NULL)
  {
        delete _buffPass;
        delete _rsPass;

        _buffPass = NULL;
        _rsPass   = NULL;
  }
}

void RadianceScalingRendererPlugin::cleanFBOs()
{
  if(_fbo!=NULL)
  {
    delete _fbo;
    delete _depthTex;
    delete _gradTex;
    delete _normTex;
    delete _colorTex;

    _fbo      = NULL;
    _depthTex = NULL;
    _gradTex  = NULL;
    _normTex  = NULL;
    _colorTex = NULL;
  }
}
