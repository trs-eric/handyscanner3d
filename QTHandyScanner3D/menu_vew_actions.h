#ifndef MENU_VEW_ACTIONS_H
#define MENU_VEW_ACTIONS_H

#include <QAction>
#include <QPushButton>

class MainWindow;

struct MenuActions
{
    QAction * view_raw_point_Act;
    QAction * view_mesh_wire_Act;
    QPushButton * fbtn_vw_wr;
    QAction * view_mesh_flat_Act;
    QPushButton * fbtn_vw_fl;
    QAction * view_mesh_smoo_Act;
    QPushButton * fbtn_vw_sm;
    QAction * view_mesh_tex_Act;
    QPushButton * fbtn_vw_tx;
    QAction * view_shad_pnt_Act;
    QAction * view_camara_path_Act;

    MainWindow * _main_window;

    MenuActions () :view_raw_point_Act(NULL),
                    view_mesh_wire_Act(NULL),
                    fbtn_vw_wr(NULL),
                    view_mesh_flat_Act(NULL),
                    fbtn_vw_fl(NULL),
                    view_mesh_smoo_Act(NULL),
                    fbtn_vw_sm(NULL),
                    view_mesh_tex_Act(NULL),
                    fbtn_vw_tx(NULL),
                    view_shad_pnt_Act(NULL),
                    view_camara_path_Act(NULL),
                    _main_window(NULL)
    {

    }
};


#endif // MENU_VEW_ACTIONS_H
