#include "ccomputesparsepcldlg.h"
#include "ui_ccomputesparsepcldlg.h"
#include "cspclconstrthread.h"
#include "cframeprepthread.h"

#include "cglwidget.h"
#include "cframe.h"

#include "settings/psettings.h"
#include <QMessageBox>

CComputeSparsePCLDlg::CComputeSparsePCLDlg(CGLWidget * glWidget, QWidget *parent) :
    _glWidget(glWidget), QDialog(parent),
    ui(new Ui::CComputeSparsePCLDlg)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);


    _framep_thread = new CFramePrepThread;
    _spcl_thread = new CSPCLConstrThread;


    connect(ui->computeBtn, SIGNAL(clicked()), this, SLOT(onComputeBtn()));

   ///connect(_spcl_thread, SIGNAL(send_spcl(std::vector<C3DPoint> * )), _glWidget, SLOT(construct_new_scan(std::vector<C3DPoint> *)));
    connect(_spcl_thread, SIGNAL(send_spcl(std::vector<C3DPoint> * )), _glWidget, SLOT(construct_next_scan(std::vector<C3DPoint> *)));
    ///connect(_spcl_thread, SIGNAL(send_meshcast(int, CTriMesh*)), _glWidget, SLOT(construct_next_meshcast(int, CTriMesh*)));

   connect(_spcl_thread, SIGNAL(send_resulting_info(double, double, double, double)), this, SLOT(onInfoComplete(double, double, double, double)));

   //// connect(_framep_thread,  SIGNAL(completed()), _spcl_thread, SLOT(stop_process()));

    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));



}

CComputeSparsePCLDlg::~CComputeSparsePCLDlg()
{
    delete ui;
    delete _framep_thread;
    delete _spcl_thread;
}


void CComputeSparsePCLDlg::onCancelBtn()
{
    CGlobalHSSettings::glob_max_frames = global_keyframes_sequence.size();

   close_dialog();
}

void CComputeSparsePCLDlg::close_dialog()
{
    close();
}


void CComputeSparsePCLDlg::onComputeBtn()
{

    // create new scan
    _glWidget->prepaire_new_scan();


    // setup gloabl variables
    CGlobalHSSettings::_current_data_path = ui->edtPath->text();
    CGlobalHSSettings::glob_max_frames = ui->edtFrameCnt->text().toInt();
    CGlobalHSSettings::global_ignore_disparity_for_matching=ui->edtIgnoreDispMatch->text().toInt();
    CGlobalHSSettings::global_ignore_disparity_offset = ui->edtIgnoreDispSPCL->text().toInt();
    CGlobalHSSettings::global_spcl_decim_step = ui->edtSPCLDecimStep->text().toInt();
    //CGlobalHSSettings::gloabal_normals_neighb_sz = ui->edtNeigh->text().toInt();


    ui->computeBtn->setEnabled(false);
    ui->edtFrameCnt         -> setEnabled(false);
    ui->edtIgnoreDispMatch  -> setEnabled(false);
    ui->edtPath             -> setEnabled(false);
    ui->edtSPCLDecimStep    -> setEnabled(false);
    ui->edtIgnoreDispSPCL   -> setEnabled(false);
    //ui->edtNeigh            ->setEnabled(false);

    _framep_thread->start_process(&_syncro_mutex, &_wait_frames_cond);
    _spcl_thread->start_process(&_syncro_mutex, &_wait_frames_cond);

}

void CComputeSparsePCLDlg::onInfoComplete(double total_time, double fps,
                                          double total_rep_err, double rep_err_av)
{

    ui->computeBtn->setEnabled(true);
    ui->edtFrameCnt         -> setEnabled(true);
    ui->edtIgnoreDispMatch  -> setEnabled(true);
    ui->edtPath             -> setEnabled(true);
    ui->edtSPCLDecimStep    -> setEnabled(true);
    ui->edtIgnoreDispSPCL   -> setEnabled(true);
    //ui->edtNeigh            -> setEnabled(true);

    /// QString im_1L_str  = QString("./sfacerpUfXL/left_00%1.png").arg(img_idx, 2, 10, QLatin1Char('0'));

    QString msg  = QString("total time: %1<br>fps: %2<br>total repro-error: %3 px<br>average repro-error: %4 px").arg(QString::number(total_time)).arg(QString::number(fps)).arg(QString::number(total_rep_err)).arg(QString::number(rep_err_av));




    QMessageBox::information(this, tr("Information"), msg);

    close_dialog();
}

void CComputeSparsePCLDlg::showEvent(QShowEvent * event)
{
    ui->edtPath->setText(CGlobalHSSettings::_current_data_path);
    ui->edtFrameCnt->setText(QString::number(CGlobalHSSettings::glob_max_frames));
    ui->edtIgnoreDispMatch->setText(QString::number(CGlobalHSSettings::global_ignore_disparity_for_matching));
    ui->edtIgnoreDispSPCL->setText(QString::number(CGlobalHSSettings::global_ignore_disparity_offset));
    ui->edtSPCLDecimStep->setText(QString::number(CGlobalHSSettings::global_spcl_decim_step));

 /*
    ui->edtPath->setText(QString("./sfacerpUfXXL2/"));
    ui->edtFrameCnt->setText(QString::number(100));
    ui->edtIgnoreDispMatch->setText(QString::number(60));
    ui->edtIgnoreDispSPCL->setText(QString::number(125));
    ui->edtSPCLDecimStep->setText(QString::number(10));

    ui->edtPath->setText(QString("./sfacerpUfXL/"));
    ui->edtFrameCnt->setText(QString::number(24));
    ui->edtIgnoreDispMatch->setText(QString::number(30));
    ui->edtIgnoreDispSPCL->setText(QString::number(125));
    ui->edtSPCLDecimStep->setText(QString::number(10));*/

}
