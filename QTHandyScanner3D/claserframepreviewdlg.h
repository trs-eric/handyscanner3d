#ifndef CLASERFRAMEPREVIEWDLG_H
#define CLASERFRAMEPREVIEWDLG_H

#include <QDialog>
#include <QLabel>
#include <QScrollBar>

#include "opencv_aux_utils.h"

namespace Ui {
class CLaserFramePreviewDlg;
}

class CLaserFramePreviewDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CLaserFramePreviewDlg(QWidget *parent = 0);
    ~CLaserFramePreviewDlg();

    void update_image(QString & image_name, QString & bckg_name,  QString & bckg_name2,
                      bool draw_edges, bool edge_correct, bool draw_corners, bool is_diff,
                      bool has_marks, bool is_round_marks, int marks_x,
                                                 int marks_y, int corrwndsz, bool usemeta,
                      float meta_angle, int laser_pos);

    void update_image_from_video(QImage * img, bool clean_img,
                                 QImage * bg, bool is_diff,
                                 bool draw_edges, bool edge_correct,
                                 bool usemeta, float meta_angle );

    void clean_data();

private slots:
    void set_zoom_100();
    void set_zoom_50();
    void set_zoom_25();
    void change_opacity();
    void show_diff();

private:
    Ui::CLaserFramePreviewDlg *ui;

    void hideEvent(QHideEvent *);
    QLabel * _tex_label;

    void scale_image(QString & image_name, QString & bckg_name,  QString & bckg_name2,
                     bool draw_edges, bool edge_correct, bool draw_corners, bool is_diff,
                     bool has_marks, bool is_round_marks, int marks_x,
                                                int marks_y, int corrwndsz, bool usemeta,
                     float meta_angle, int laser_pos);

    void adjust_scroll_bar(QScrollBar *scrollBar, float factor);

    void draw_mark( QImage & texture, float cx, float cy);

    std::vector<cv::Point2f> _corners;

    QImage * _img;
    QImage * _bg;

    QString _prev_image_name;
    QString _prev_bckg_name;
    QString _prev_bckg_name2;
    bool    _prev_draw_edges;
    bool    _prev_draw_corners;
    bool _isdiff;

    bool _prev_has_marks;
    bool _prev_edge_correct;
    bool _prev_is_round_marks;
    int _prev_marks_x;
    int _prev_marks_y;
    int _prev_corrwndsz;
    int _prev_lookfor_laser_onboard_ymax;

    float _scale_factor;

    bool _use_meta;
    float _meta_angle;

signals:
    void close_preview();
};

#endif // CLASERFRAMEPREVIEWDLG_H
