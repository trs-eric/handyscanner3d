#include "cspclconstrthread.h"

#include <iostream>
#include <ctime>

#include "cframe.h"
#include <opencv2/highgui/highgui.hpp>

#include "./settings/psettings.h"

//#include <opencv2/core/core.hpp>
//#include <opencv2/features2d/features2d.hpp>
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/video/tracking.hpp>
//#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
//#include <opencv2/nonfree/nonfree.hpp>


// http://nghiaho.com/?page_id=846

CSPCLConstrThread::CSPCLConstrThread(QObject *parent) :
    QThread(parent), _syncro_mutex(NULL)
{
   // _mutex_abort.lock();
  //  _abort = false;
   // _mutex_abort.unlock();
}

CSPCLConstrThread::~CSPCLConstrThread()
{
  //  _mutex_abort.lock();
  // _abort = true;
   //_mutex_abort.unlock();
    wait();
}

void CSPCLConstrThread::start_process(QMutex * smutex, QWaitCondition * wcond)
{
   //  _abort = false;
    _syncro_mutex = smutex;
    _wait_cond_suffice_keyframes = wcond;

    start();
}

/*void CSPCLConstrThread::stop_process()
{
    _mutex_abort.lock();
    _abort = true;
    _mutex_abort.unlock();
}*/

/*
std::vector<C3DPoint> * CSPCLConstrThread::construct_spcl_for_frame(CFrame * f)
{

    /// cv::Mat rvec_glob_L1 = -f->_glob_rot_vec;
    /// cv::Mat Rglob;
    /// cv::Rodrigues(rvec_glob_L1, Rglob);



    double theta_x =  -f->_glob_rot_vec.at<double>(0,0);
    double theta_y =  -f->_glob_rot_vec.at<double>(0,1);
    double theta_z =  -f->_glob_rot_vec.at<double>(0,2);

    cv::Mat rotX = (cv::Mat_<double>(3,1) << theta_x, 0, 0);
    cv::Mat rotY = (cv::Mat_<double>(3,1) << 0, theta_y, 0);
    cv::Mat rotZ = (cv::Mat_<double>(3,1) << 0, 0, theta_z);

    cv::Mat rotM_X, rotM_Y, rotM_Z;
    cv::Rodrigues(rotX, rotM_X);
    cv::Rodrigues(rotY, rotM_Y);
    cv::Rodrigues(rotZ, rotM_Z);

    cv::Mat Rglob;
     Rglob =  rotM_Y * rotM_Z * rotM_X   ;

    /// table version
   /// Rglob =  rotM_X *  rotM_Y * rotM_Z ; // ORDERING!

    int xs = f->_img_L.cols;
    int ys = f->_img_L.rows;

    std::vector<C3DPoint> *  final_res = new std::vector<C3DPoint>;

    for (int y = 0; y < ys; y+= CGlobalHSSettings::global_spcl_decim_step )
    for (int x = 0; x < xs; x+= CGlobalHSSettings::global_spcl_decim_step )
    {
        // int cdisp = f->_img_disparity.at<short>(y,x);
        int cdisp = f->_img_disparity.at<float>(y,x);
        if((cdisp > CGlobalHSSettings::global_ignore_disparity_offset) && (cdisp < CGlobalHSSettings::_disparity_parameters.disp_max) )
        {
            cv::Point3f pp = f->_img_3d.at<cv::Point3f>(y,x);

            cv::Mat pp_mat = (cv::Mat_<double>(3,1) << pp.x, pp.y, pp.z);

            cv::Mat pp_mat_glob = Rglob * pp_mat + f->_global_pos;

            C3DPoint p;
            p._x =   pp_mat_glob.at<double>(0,0)  * CGlobalHSSettings::glob_gl_scale + CGlobalHSSettings::glob_gl_offx;
            p._y = - pp_mat_glob.at<double>(1,0)  * CGlobalHSSettings::glob_gl_scale + CGlobalHSSettings::glob_gl_offy;
            p._z = - pp_mat_glob.at<double>(2,0)  * CGlobalHSSettings::glob_gl_scale + CGlobalHSSettings::glob_gl_offz;

            cv::Vec3b rgbv = f->_img_L.at<cv::Vec3b>(y,x);
             p._r = float(rgbv.val[2]) / 256.0;
             p._g = float(rgbv.val[1]) / 256.0;
             p._b = float(rgbv.val[0]) / 256.0;

            final_res->push_back(p);
        }
    }
    return final_res;

}
*/

bool icp_sort_x_coord(const cv::Point3f & a, const cv::Point3f & b)
{
    return (a.x < b.x);
}

bool icp_sort_y_coord(const cv::Point3f & a, const cv::Point3f & b)
{
    return (a.y < b.y);
}

bool icp_sort_z_coord(const cv::Point3f & a, const cv::Point3f & b)
{
    return (a.z < b.z);
}

cv::Point3f CSPCLConstrThread::find_3d_offset_with_icp(CFrame * prev_frame, CFrame * curr_frame,
                                                       cv::Point3f & initT, double & repro_error)
{

    clock_t msstart = clock();

    int xs = prev_frame->_img_L.cols;
    int ys = prev_frame->_img_L.rows;

    cv::Mat backR = curr_frame->get_neg_angular_difference_as_rotation_matrix(prev_frame) ;
    cv::Mat forwR = curr_frame->get_pos_angular_difference_as_rotation_matrix(prev_frame) ;


    double bR00 = backR.at<double>(0,0);
    double bR01 = backR.at<double>(0,1);
    double bR02 = backR.at<double>(0,2);
    double bR10 = backR.at<double>(1,0);
    double bR11 = backR.at<double>(1,1);
    double bR12 = backR.at<double>(1,2);
    double bR20 = backR.at<double>(2,0);
    double bR21 = backR.at<double>(2,1);
    double bR22 = backR.at<double>(2,2);

    ///////////////////////////////////////////////////////////////////////////////////////
    ////// Step 1. Compute unrotated and translated points
    //////          with known rotation and assumed offset
    ///////////////////////////////////////////////////////////////////////////////////////
    cv::Mat rotPoints = cv::Mat_<cv::Point3f>(xs, ys);
    for(int y = 0; y < ys; y ++)
    {
        for(int x = 0; x < xs; x ++)
        {
            float disp_2L = curr_frame->_img_disparity.at<float>(y,x);
            if(disp_2L  >= CGlobalHSSettings::global_ignore_disparity_for_icp)
            {
                cv::Point3f  pp = curr_frame->_img_3d.at<cv::Point3f>(y,x);

               cv::Point3f  pres;
               pres.x = bR00 * pp.x + bR01 * pp.y + bR02 * pp.z + initT.x;
               pres.y = bR10 * pp.x + bR11 * pp.y + bR12 * pp.z + initT.y;
               pres.z = bR20 * pp.x + bR21 * pp.y + bR22 * pp.z + initT.z;

                rotPoints.at<cv::Point3f>(y,x) =  pres;
            }
        }
    }


    ///////////////////////////////////////////////////////////////////////////////////////
    ////// Step 2. Compute closest projection point, define search region,
    //////          put offset into vector with best neighbour
    ///////////////////////////////////////////////////////////////////////////////////////

    int global_icp_search_step = 2;
    std::vector<cv::Point3f> icp_off;

    cv::Mat camL_K_inv = CGlobalHSSettings::_camL_K.inv();

    cv::Mat forwR_camL_K_inv = forwR * camL_K_inv;

    cv::Mat T = (cv::Mat_<double>(3,1) << -initT.x, -initT.y, -initT.z);



    for(int y = 0; y < ys; y +=global_icp_search_step)
    {
        for(int x = 0; x < xs; x +=global_icp_search_step)
        {


            float disp_1L = prev_frame->_img_disparity.at<float>(y,x);
            if(disp_1L  >= CGlobalHSSettings::global_ignore_disparity_for_icp)
            {

                cv::Mat uv = (cv::Mat_<double>(3,1) << x, y, 1);
                cv::Point3f p3d = prev_frame->_img_3d.at<cv::Point3f>(y,x);
                cv::Mat xyz = (cv::Mat_<double>(3,1) << p3d.x, p3d.y, p3d.z);
                cv::Mat p3d_rot = (forwR * xyz + T);

                cv::Point3f p3_1L = prev_frame->_img_3d.at<cv::Point3f>(y,x);
                //p3_1L.x = p3d_rot.at<double>(0,0);
                //p3_1L.y = p3d_rot.at<double>(1,0);
                //p3_1L.z = p3d_rot.at<double>(2,0);

                cv::Mat uvp = CGlobalHSSettings::_camL_K * p3d_rot;

                double uvp_x = uvp.at<double> (0,0)/uvp.at<double> (2,0);
                double uvp_y = uvp.at<double> (1,0)/uvp.at<double> (2,0);

                /// if central point projection in image plane
                if((uvp_x > 0) && (uvp_y > 0) && (uvp_x < xs) && (uvp_y < ys))
                {

                        //// if central disparity is good enough
                       ///std::cout << "correpondance " << x << " "<< y << "--" << " " << uvp_x << " " << uvp_y  << std::endl;

                        bool found_nn = false;
                        cv::Point3f  off;
                        off.x = 1000000; off.y = 1000000; off.z = 1000000;

                        double radius = 2 * repro_error;

                        int x_min = (0>uvp_x - radius)? 0: uvp_x - radius;
                        int x_max = (uvp_x + radius<xs)? uvp_x + radius: xs-1;
                        int y_min = (0>uvp_y - radius)? 0: uvp_y - radius;
                        int y_max = (uvp_y + radius<ys)? uvp_y+radius: ys-1;

                        ////////
                        //// search region
                        for(int j = y_min; j <= y_max; j ++)
                        {
                            for(int i = x_min; i < x_max; i++)
                            {
                                float disp_2L = curr_frame->_img_disparity.at<float>(j,i);
                                if(disp_2L  >= CGlobalHSSettings::global_ignore_disparity_for_icp)
                                {

                                        cv::Point3f p3_2L = rotPoints.at<cv::Point3f>(j,i);
                                        cv::Point3f  coff =p3_1L-p3_2L;
                                        if(  (coff.x * coff.x + coff.y * coff.y + coff.z * coff.z) <
                                             ( off.x * off.x  +  off.y *  off.y +  off.z * off.z)  )
                                        {
                                              found_nn = true;
                                              off = coff;
                                              ///std::cout << "2: " << x << " " << y << " " << off << " " << std::endl;
                                        }
                                }
                            }
                        }

                        if(found_nn)
                        {
                            ///std::cout << "2: " << x << " " << y << " "<< off  << std::endl;
                            icp_off.push_back(off);
                        }



                }
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    ////// Step 4. find proper correction                                               ///
    //////                                                                              ///
    ///////////////////////////////////////////////////////////////////////////////////////


    std::sort(icp_off.begin(), icp_off.end(),icp_sort_z_coord);
    std::vector<cv::Point3f> icp_after_z;
    int vsz = icp_off.size();
    double cutoff_min = double(vsz) * 0.2;
    double cutoff_max = double(vsz) * 0.8;
    for(int i = cutoff_min; i <  cutoff_max; i++ )
        icp_after_z.push_back(icp_off[i]);

    std::sort(icp_after_z.begin(), icp_after_z.end(),icp_sort_x_coord);
    std::vector<cv::Point3f> icp_after_zx;
    int vszZX = icp_after_z.size();
    cutoff_min = double(vszZX) * 0.2;
    cutoff_max = double(vszZX) * 0.8;
    for(int i = cutoff_min; i <  cutoff_max; i++ )
        icp_after_zx.push_back(icp_after_z[i]);

    std::sort(icp_after_zx.begin(), icp_after_zx.end(),icp_sort_y_coord);
    std::vector<cv::Point3f> icp_after_xyz;
    int vszXYZ = icp_after_zx.size();
      cutoff_min = double(vszXYZ) * 0.2;
      cutoff_max = double(vszXYZ) * 0.8;
    for(int i = cutoff_min; i <  cutoff_max; i++ )
        icp_after_xyz.push_back(icp_after_zx[i]);

    int res_sz = icp_after_xyz.size();
    cv::Point3f roff;
    int vcount = 0;
    for(int i = 0; i <  res_sz; i++ )
    {
        roff += icp_after_xyz[i];
        vcount ++;
    }

    double Tx = roff.x / double(vcount);
    double Ty = roff.y / double(vcount);
    double Tz = roff.z / double(vcount);
    std::cout << "======" << Tx << " " << Ty << " " << Tz << std::endl;

     initT.x += Tx;
     initT.y += Ty;
     initT.z += Tz;



    clock_t mffinish = clock();
    int sec_conv = (mffinish - msstart);
    std::cout << "22222: step 22222 1,2,3 perfromance - time clocks: " << sec_conv << " secs: " << ((float)sec_conv )/CLOCKS_PER_SEC << std::endl;
}

/// range of random steps isnot clear
/// make it even more faster and more tries
///

cv::Point3f CSPCLConstrThread::improve_camera_pose_with_rg(CFrame * prev_frame, CFrame * curr_frame,
                                                           cv::Point3f & inT, double & in_repro_error)
{

     clock_t msstart = clock();

     cv::Point3f resT = inT;
     cv::Point3f cT   = inT;

     double c_error = in_repro_error;

     bool found_better_solution = false;
     int max_random_tries = 5000;

     cv::Mat forwR = curr_frame->get_pos_angular_difference_as_rotation_matrix(prev_frame) ;

     double cutoff_min = 0.3;
     double cutoff_max = 0.7;
     for (int i = 0; i < max_random_tries; i++)
     {
         double rnd = 2*double(rand())/double(RAND_MAX) - 1; // -1 .. +1
         double scale_x = 0.05; // %
         double rand_x = resT.x * scale_x * rnd;

                rnd = 2*double(rand())/double(RAND_MAX) - 1; // -1 .. +1
         double scale_y = 0.05; // %
         double rand_y = resT.y * scale_y * rnd;

                rnd = 2*double(rand())/double(RAND_MAX) - 1; // -1 .. +1
         double scale_z = 0.1; // %
         double rand_z = resT.z * scale_z * rnd;

         cT.x = resT.x + rand_x; //[-2% +2%]
         cT.y = resT.y + rand_y; //[-2% +2%]
         cT.z = resT.z + rand_z; //[-5% +5%]

         cv::Mat camTd = (cv::Mat_<double>(3,1) << cT.x, cT.y, cT.z);

         cv::Mat poseTd = -forwR * camTd;

         cv::Mat P2d = (cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2),  poseTd.at<double>(0,0) ,
                                               forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2),   poseTd.at<double>(1,0) ,
                                               forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2),   poseTd.at<double>(2,0));


         double proj_error = curr_frame -> compute_average_reproj_error(P2d, CGlobalHSSettings::_camL_K,
                                                          prev_frame, cutoff_min, cutoff_max);

        /// std::cout << "random solution " << proj_error << std::endl;
         if(proj_error < c_error)
         {
           /// std::cout << "found better solution, repo err: "<< proj_error << std::endl;
            found_better_solution = true;
            resT = cT;
            c_error = proj_error;
         }

     }


     clock_t mffinish = clock();
     int sec_conv = (mffinish - msstart);
     std::cout << "22222: RANDOM SEARCH  perfromance - time clocks: " << sec_conv << " secs: " << ((float)sec_conv )/CLOCKS_PER_SEC << std::endl;

     if(found_better_solution)
     {
         in_repro_error = c_error;
         return resT;
     }else
         return inT;

}


void CSPCLConstrThread::run()
{
    std::cout << "2: CSPCLConstrThread::run()" << std::endl;


    cv::Mat global_frame_pos    = (cv::Mat_<double>(3,1) << 0,0,0);
    cv::Mat relative_frame_pos  = (cv::Mat_<double>(3,1) << 0,0,0);

    clock_t msstart;

    bool is_zero_frame = true;
    CFrame * prev_frame = NULL;
    CFrame * curr_frame = NULL;
    int current_frame_idx = 0;
    float total_repo_error = 0;
    while(true)
    {
        //std::cout << "wait for the next frame ... " << std::endl;
        // wait for two frames
        _syncro_mutex->lock();
        if (global_keyframes_for_processing == 0 && !global_no_more_keyframes )
                       _wait_cond_suffice_keyframes->wait(_syncro_mutex);
        _syncro_mutex->unlock();
        //std::cout << "done!" << std::endl;

        // if I am awaik and no frames came then just exiting
        _syncro_mutex->lock();
        if (global_keyframes_for_processing == 0)
        {
            std::cout << "2: there is nothing todo" << std::endl;
            _syncro_mutex->unlock();

            clock_t mffinish = clock();
            int sec_conv = (mffinish - msstart);

            double time_total = ((float)sec_conv )/CLOCKS_PER_SEC;
            double fps =  double(CGlobalHSSettings::glob_max_frames)/time_total;
            double repo_err_av = total_repo_error/double(CGlobalHSSettings::glob_max_frames);

            std::cout << " 2st thread result - time clocks: " << sec_conv << " secs: " << time_total << std::endl;
            std::cout << "total repo error: " << total_repo_error << " " << repo_err_av  << std::endl;

            emit send_resulting_info(time_total, fps, total_repo_error, repo_err_av);

            return;
        }
        _syncro_mutex->unlock();


        if(is_zero_frame)
        {
            msstart  = clock();

            std::cout << "2: processing zero frame" << std::endl;
            is_zero_frame = false;
            prev_frame = global_keyframes_sequence[current_frame_idx];

            current_frame_idx++;

            _syncro_mutex->lock();
            global_keyframes_for_processing--;
            _syncro_mutex->unlock();

            continue;
        }


        curr_frame = global_keyframes_sequence[current_frame_idx];


        clock_t msstart = clock();
        std::cout << "2: -------- processing " << current_frame_idx << " frame -------- " << std::endl;

        double repro_error = 0;
        cv::Point3f realT = curr_frame->find_3d_offset_with_homography(prev_frame, repro_error);
        ///  realT  = improve_camera_pose_with_rg(prev_frame, curr_frame, realT, repro_error);

        std::cout << "OFFSET IN LOCAL COORDS " << realT << std::endl;
        ////////////////////////////////////////////////
        ///  cv::Point3f realT =
         /* find_3d_offset_with_icp(prev_frame, curr_frame, realT, repro_error);
          {
          cv::Mat camT = (cv::Mat_<double>(3,1) << realT.x, realT.y, realT.z);
          cv::Mat forwR = curr_frame->get_pos_angular_difference_as_rotation_matrix(prev_frame);
          cv::Mat poseT = -forwR * camT;

          cv::Mat P2 = ( cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2),  poseT.at<double>(0,0) ,
                                                  forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2),   poseT.at<double>(1,0) ,
                                                  forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2),   poseT.at<double>(2,0));
         std::cout << "and now repo error is " <<  compute_average_reproj_error(P2, CGlobalHSSettings::_camL_K, prev_frame, curr_frame) << std::endl;
         }*/
         ///////////////////////////////////////////////

        total_repo_error += repro_error;
        std::cout << "2: reprojection - error" << repro_error << std::endl;

        ///// construct point cloud
        global_frame_pos.copyTo(prev_frame->_global_pos);
        ///std::vector<C3DPoint> * spcl = prev_frame->construct_spcl_from_depth_img_with_norms();
        std::vector<C3DPoint> * spcl = prev_frame->construct_spcl_from_depth_img();

        /// CTriMesh * tmesh = prev_frame->construct_meshcast_from_depth_img(CGlobalHSSettings::global_spcl_decim_step);



        curr_frame->udate_global_and_relat_pose(prev_frame,  realT);
        curr_frame->_global_pos.copyTo(global_frame_pos);

        prev_frame->_valid_coord = true;
        curr_frame->_valid_coord = true;
         emit send_spcl(spcl);
        ///emit send_meshcast(prev_frame->_id, tmesh);

        //////////////////////////////////////////////////
        ///// compute raltive pose and update global pose
        /// cv::Mat rvec_glob_L1 = -prev_frame->_glob_rot_vec;
        /// cv::Mat Rglob;
        /// cv::Rodrigues(rvec_glob_L1, Rglob);
/*
        double theta_x =  -prev_frame->_glob_rot_vec.at<double>(0,0);
        double theta_y =  -prev_frame->_glob_rot_vec.at<double>(0,1);
        double theta_z =  -prev_frame->_glob_rot_vec.at<double>(0,2);

        cv::Mat rotX = (cv::Mat_<double>(3,1) << theta_x, 0, 0);
        cv::Mat rotY = (cv::Mat_<double>(3,1) << 0, theta_y, 0);
        cv::Mat rotZ = (cv::Mat_<double>(3,1) << 0, 0, theta_z);

        cv::Mat rotM_X, rotM_Y, rotM_Z;
        cv::Rodrigues(rotX, rotM_X);
        cv::Rodrigues(rotY, rotM_Y);
        cv::Rodrigues(rotZ, rotM_Z);

        cv::Mat Rglob;
        Rglob =  rotM_Y  * rotM_Z * rotM_X   ; /// ORDERING


        cv::Mat camT = (cv::Mat_<double>(3,1) << realT.x, realT.y, realT.z);
        cv::Mat relative_pose = Rglob * camT;
        std::cout << " OFFSET IN GLOBAL COORDS computed " << relative_pose.t() << std::endl;



      //  double rx = relative_pose.at<double>(0,0);
      //  double ry = relative_pose.at<double>(1,0);
      //  double rz = relative_pose.at<double>(2,0);

    //    relative_pose.at<double>(0,0) = 0;
      //  relative_pose.at<double>(1,0) = 0;
     // relative_pose.at<double>(2,0) = 0;


        relative_pose.copyTo(curr_frame->_relat_pos);


        ///// TEMPORAL FIX, WHYYYYY
        ///curr_frame->_relat_pos.at<double>(1,0) = 1.25* curr_frame->_relat_pos.at<double>(1,0);

        std::cout << " OFFSET IN GLOBAL COORDS " << relative_pose.t() << std::endl;

        global_frame_pos += relative_pose;
        global_frame_pos.copyTo(curr_frame->_global_pos);

        std::cout << "computed gT: " << global_frame_pos.t() << std::endl;




        ////////////////////////////////////////////////////////////////////
*/
        clock_t mffinish = clock();
        int sec_conv = (mffinish - msstart);
        std::cout << "2: single spcl iter - time clocks: " << sec_conv << " secs: " << ((float)sec_conv )/CLOCKS_PER_SEC << std::endl;

        current_frame_idx++;
        prev_frame->dump_unused_data();
        prev_frame = curr_frame;

        _syncro_mutex->lock();
        global_keyframes_for_processing--;
        _syncro_mutex->unlock();
    }



}
