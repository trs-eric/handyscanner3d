#include "cnormalestimationthread.h"
#include "cpointcloud.h"
#include "cnormalcloud.h"
#include "meshconstructionutils.h"

CNormalEstimationThread::CNormalEstimationThread(QObject *parent) :
        QThread(parent), _in_cloud(NULL), _ncount(10), _flip(false)
{
    _abort = false;
}

CNormalEstimationThread::~CNormalEstimationThread()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
    wait();
}

void CNormalEstimationThread::start_process(CPointCloud * in, int ncnt, bool flip)
{
    _abort = false;
    _in_cloud = in;
    _ncount = ncnt;
    _flip = flip;
    start();
}

void CNormalEstimationThread::stop_process()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
}
void CNormalEstimationThread::emit_send_back(int i)
{
     emit send_back(i);
}

void CNormalEstimationThread::run()
{


    std::vector<C3DPoint> inpnts = _in_cloud->get_points_cpy();
    std::vector<C3DPoint> normals =  compute_normals(inpnts, _ncount, _flip, this);

    if(normals.size() == 0) // if not succeed
    {
        emit send_back(0);
        return;
    }

    CNormalCloud * ncld = new CNormalCloud ;
    ncld->set_normals(normals);
    emit send_result(ncld);
    emit send_back(100);

}
