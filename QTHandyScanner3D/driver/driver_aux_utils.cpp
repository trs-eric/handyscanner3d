#include "driver_aux_util.h"

#include <unistd.h>
#include <iostream>
#include <cmath>
 #include <fstream>

#include <QElapsedTimer>
namespace driv
{

// camera
BYTE cam_i2c_addr = 0x78;


// AS5600
BYTE tbl_angl_addr_wr =  0x6C;
BYTE tbl_angl_addr_rd =  0x6D;


// novel angle measure
// SC18IS602B
BYTE spi_conv_addr_wr =  0x5E;
BYTE spi_conv_addr_rd =  0x5F;
//  AS5047P
BYTE func_id = 0x01;


// ADXL345
BYTE accel_addr_wr =  0xA6;
BYTE accel_addr_rd =  0xA7;

BYTE avr_addr_wr =  0x20;
BYTE avr_addr_rd =  0x21;

//
//PCA9536
BYTE tbl_moto_addr_wr =  0x82;
BYTE tbl_moto_addr_rd =  0x83;

// PCF8574
BYTE expander_addr_wr = 0x40;
BYTE expander_addr_rd = 0x41;

BYTE laser_i2c_status = 0x00;
BYTE motor_i2c_status = 0x00;
bool vmotor_on   = true; // p0
bool vmotor_left = true; // p1
bool vmotor_step = true; // p2
unsigned int motor_sleep_usecs = 1000000;
int table_rot_angle = 45; // one turn is  45 grad


/// BYTE laser_i2c_addr_wr = 0x40;
/// BYTE laser_i2c_addr_rd = 0x41;


void set_RGBLed_X(PVOID hDev, BYTE cmd, int r, int g, int b)
{
    if(hDev == NULL) return;
    BYTE data_buf[8] = { 0 };
    data_buf[0] = cmd;

    data_buf[1] = r;
    data_buf[2] = g;
    data_buf[3] = b;

    driv::I2C_Write(hDev, avr_addr_wr, 4, data_buf);
}

void motor_rotate_signal(PVOID hDev, int sig)
{
    if(hDev == NULL) return;

    BYTE data_buf[8] = { 0 };
    data_buf[0] = 0x45;
    data_buf[1] = sig;

    driv::I2C_Write(hDev, avr_addr_wr, 2, data_buf);
}

void motor_dir_change(PVOID hDev, int dir)
{
    if(hDev == NULL) return;

    BYTE data_buf[8] = { 0 };
    data_buf[0] = 0x43;

    data_buf[1] = dir;
    driv::I2C_Write(hDev, avr_addr_wr, 2, data_buf);

}

void turn_table_request(PVOID hDev, int val)
{
     if(hDev == NULL) return;

    BYTE data_buf[8] = { 0 };
    data_buf[0] = 0x51;

    data_buf[1] =        val & 0xff;
    data_buf[2] = (val >> 8) & 0xff;

    driv::I2C_Write(hDev, avr_addr_wr, 3, data_buf);
}

int turn_table_status(PVOID hDev)
{
    if(hDev == NULL) return;

    BYTE data_buf[8] = { 0 };
    BYTE out_buf[8] = { 0 };

    data_buf[0] = 0x52;
    driv::I2C_Write(hDev, avr_addr_wr, 1, data_buf);
    driv::I2C_Read(hDev, avr_addr_wr, 1, out_buf);

    return out_buf[0];
}

void set_M0(PVOID hDev, int v)
{
    if(hDev == NULL) return;

    BYTE data_buf[8] = { 0 };
    data_buf[0] = 0x41;

    data_buf[1] = v;
    driv::I2C_Write(hDev, avr_addr_wr, 2, data_buf);

}


void set_M1(PVOID hDev, int v)
{
     if(hDev == NULL) return;

    BYTE data_buf[8] = { 0 };
    data_buf[0] = 0x42;

    data_buf[1] = v;
    driv::I2C_Write(hDev, avr_addr_wr, 2, data_buf);

}

void set_ELED_brightness(PVOID hDev, int v)
{
    if(hDev == NULL) return;

    BYTE data_buf[8] = { 0 };
    data_buf[0] = 0x31;
    data_buf[1] = v;

    driv::I2C_Write(hDev, avr_addr_wr, 2, data_buf);
}

int get_laser_angle(PVOID hDev)
{
     if(hDev == NULL) return 0;

     BYTE data_buf[8] = { 0 };
     BYTE out_buf[8] = { 0 };

     data_buf[0] = 0x11;
     driv::I2C_Write(hDev, avr_addr_wr, 1, data_buf);
     driv::I2C_Read(hDev, avr_addr_rd, 2, out_buf);
     int angle = (((int)out_buf[1]) << 8) | out_buf[0];

     return angle;
}

void laser_turn_on(PVOID hDev)
{
    if(hDev == NULL) return;
    //// laser_i2c_status = 0x08;
    //// update_expander_status(hDev);

    BYTE data_buf[8] = { 0 };
    data_buf[0] = 0x61;
    data_buf[1] = 1; // on
    driv::I2C_Write(hDev, avr_addr_wr, 2, data_buf);
}

void laser_turn_off(PVOID hDev)
{
    if(hDev == NULL) return;
    //// laser_i2c_status = 0x00;
    //// update_expander_status(hDev);


    BYTE data_buf[8] = { 0 };
    data_buf[0] = 0x61;
    data_buf[1] = 2; // off
    driv::I2C_Write(hDev, avr_addr_wr, 2, data_buf);
}

void update_motor_status()
{
    BYTE on     = (vmotor_on)?   0x01 : 0x00;
    BYTE dir    = (vmotor_left)? 0x02 : 0x00;
    BYTE step   = (vmotor_step)? 0x04 : 0x00;
    motor_i2c_status =0x00;
    motor_i2c_status = on | dir | step;
}



void motor_step(PVOID hDev)
{
    update_motor_status();
    update_expander_status(hDev);
    vmotor_step = !vmotor_step;

    struct timespec tw = {0, motor_sleep_usecs*1000};
    struct timespec tr;
    nanosleep (&tw, &tr);
}

void motor_stop(PVOID hDev)
{
    if(hDev == NULL) return;

     vmotor_on   = false;
     vmotor_left = false;
     vmotor_step = false;

    update_motor_status();
    update_expander_status(hDev);
}



void motor_left(PVOID hDev)
{
    if(hDev == NULL) return;
    vmotor_on   = true;
    vmotor_left = true;
    vmotor_step = true;

    update_motor_status();

    for(int i = 0; i < 100; i++)
        motor_step(hDev);
}

void motor_right(PVOID hDev)
{
    if(hDev == NULL) return;
    vmotor_on   = true;
    vmotor_left = false;
    vmotor_step = true;

    update_motor_status();

    for(int i = 0; i < 100; i++)
        motor_step(hDev);

}

void update_expander_status(PVOID hDev)
{
    BYTE data_buf[8] = { 0 };
    data_buf[0] = 0x60; // FIX P5 P6 high


    data_buf[0] |= laser_i2c_status;
    data_buf[0] |= motor_i2c_status;

    HRESULT hr = driv::I2C_Write(hDev, expander_addr_wr, 1, data_buf);
}


void init_accelerometer(PVOID hDev)
{

    BYTE data_buf[8] = { 0 };

/// https://github.com/labjack/I2C-AppNotes/tree/master/Accelerometer-ADXL345


    /// ADXL345_DATA_FORMAT
    data_buf[0] = 0x31;  // FORMAT register
    data_buf[1] = 0x0B; //Set bits 3(full range) and 1 0 on (+/- 16g-range)
    driv::I2C_Write(hDev, accel_addr_wr,  2, data_buf);

    ///  ADXL345_POWER_CTL
    data_buf[0] = 0x2D; // clear POWER register
    data_buf[1] = 0x00;
    driv::I2C_Write(hDev, accel_addr_wr, 2, data_buf);

    ///  ADXL345_POWER_CTL
    data_buf[0] = 0x2D; // POWER register
    data_buf[1] = 0x08;
    driv::I2C_Write(hDev, accel_addr_wr, 2, data_buf);
}

void read_accelerometer_gdata(PVOID hDev, int & x, int & y, int & z)
{
    BYTE data_buf[8] = { 0 };
    data_buf[0] = 0x32;  //first axis-acceleration-data register on the ADXL345
    driv::I2C_Write(hDev, accel_addr_wr,  1, data_buf);

    BYTE out_buf[8] = { 0 };

    driv::I2C_Read(hDev, accel_addr_rd, 6, out_buf);

    short xx = (((int)out_buf[1]) << 8) | out_buf[0];
    short yy = (((int)out_buf[3]) << 8) | out_buf[2];
    short zz = (((int)out_buf[5]) << 8) | out_buf[4];

    x = xx;
    y = yy;
    z = zz;
}

void read_accelerometer_roll_pitch(PVOID hDev, float & roll, float& pitch)
{
    int x = 0;
    int y = 0;
    int z = 0;
    read_accelerometer_gdata(hDev, x, y, z);

    double x_Buff = double(x) * 0.00390625 ;
    double y_Buff = double(y) * 0.00390625 ;
    double z_Buff = double(z) * 0.00390625 ;
    roll =  std::atan2((y_Buff) , std::sqrt(x_Buff * x_Buff + z_Buff * z_Buff)) * 57.295779;
    pitch = -std::atan2((x_Buff) , std::sqrt(y_Buff * y_Buff + z_Buff * z_Buff)) * 57.295779;
}

int get_as5600_angle_pos(PVOID hDev)
{
    BYTE data_buf[8] = { 0 };

    int raw_ang_lo = 0x0d;
    data_buf[0] = raw_ang_lo;
    driv::I2C_Write(hDev, tbl_angl_addr_wr,  1, data_buf);

    BYTE out_buf[8] = { 0 };
    driv::I2C_Read(hDev, tbl_angl_addr_rd, 1, out_buf);
    int lo_raw = out_buf[0];

    int raw_ang_hi = 0x0c;
    data_buf[0] = raw_ang_hi;
    driv::I2C_Write(hDev, tbl_angl_addr_wr,  1, data_buf);

    driv::I2C_Read(hDev, tbl_angl_addr_rd, 1, out_buf);
    int hi_raw = out_buf[0];

    hi_raw = hi_raw << 8;
    hi_raw = hi_raw | lo_raw;

    return hi_raw;
}

void table_rot(PVOID hDev)
{
    int init_angl_pos = get_as5600_angle_pos(hDev);

    BYTE data_buf[8] = { 0 };

    data_buf[0] = 0x03; //setup conf reg to output
    data_buf[1] = 0x00; //
    driv::I2C_Write(hDev, tbl_moto_addr_wr,  2, data_buf);

    // clean output register
    data_buf[0] = 0x01;
    data_buf[1] = 0x00;
    driv::I2C_Write(hDev, tbl_moto_addr_wr,  2, data_buf);

    clock_t msstart  = clock();

     while(true)
    {
        data_buf[0] = 0x01; //setup output reg
        data_buf[1] = 0x07; // 1 1 1
        driv::I2C_Write(hDev, tbl_moto_addr_wr,  2, data_buf);

        {
            struct timespec tw = {0, 1000*1000}; // sleep 10 millsec
            struct timespec tr;
            nanosleep (&tw, &tr);
        }
        data_buf[0] = 0x01;  // setup output reg
        data_buf[1] = 0x03;  //0 1 1
        driv::I2C_Write(hDev, tbl_moto_addr_wr,  2, data_buf);

        {
            struct timespec tw = {0, 1000*1000}; //sleep  10 millsec
            struct timespec tr;
            nanosleep (&tw, &tr);
        }

        clock_t mffinish = clock();
        int sec_conv = (mffinish - msstart);

        // > 100 millisec
        if( ((float)sec_conv )*1000./CLOCKS_PER_SEC > 100.)
        {
            msstart  = mffinish;
            int cur_angl_pos = get_as5600_angle_pos(hDev);

            int difference = 0;
            if(cur_angl_pos > init_angl_pos)
                difference = cur_angl_pos - init_angl_pos;
            else
                difference = (4096 - init_angl_pos) + cur_angl_pos;

            float angle = float(difference) * 360.f / 4096.0f;
            if(angle >= table_rot_angle)
                    break;
        }
    }

    // clean output register
    data_buf[0] = 0x01;
    data_buf[1] = 0x00;
    driv::I2C_Write(hDev, tbl_moto_addr_wr,  2, data_buf);
}


void get_nano_measure(PVOID hDev, double & t1, double & t2)
{
    BYTE * data_buf = new BYTE[8];//{ 0 };

    data_buf[0] = 0x03; //setup conf reg to output
    data_buf[1] = 0x00; //
    driv::I2C_Write(hDev, tbl_moto_addr_wr,  2, data_buf);


    for(int i = 0; i < 200; i++)
    {
        data_buf[0] = 0x01; //setup output reg
        data_buf[1] = 0x07; // 1 1 1
        driv::I2C_Write(hDev, tbl_moto_addr_wr,  2, data_buf);

        {
        struct timespec tw = {0, 10000*1000};
        struct timespec tr;
        nanosleep (&tw, &tr);
        }
        data_buf[0] = 0x01;  // setup output reg
        data_buf[1] = 0x03;  //0 1 1
        driv::I2C_Write(hDev, tbl_moto_addr_wr,  2, data_buf);

        {
        struct timespec tw = {0, 10000*1000};
        struct timespec tr;
        nanosleep (&tw, &tr);
        }
    }

    // set all low
    data_buf[0] = 0x01;
    data_buf[1] = 0x00;
    driv::I2C_Write(hDev, tbl_moto_addr_wr,  2, data_buf);

    ///////////////////////////////////

    int raw_ang_lo = 0x0d;
    data_buf[0] = raw_ang_lo;
    driv::I2C_Write(hDev, tbl_angl_addr_wr,  1, data_buf);

    BYTE out_buf[8] = { 0 };
    driv::I2C_Read(hDev, tbl_angl_addr_rd, 1, out_buf);
    int lo_raw = out_buf[0];

    int raw_ang_hi = 0x0c;
    data_buf[0] = raw_ang_hi;
    driv::I2C_Write(hDev, tbl_angl_addr_wr,  1, data_buf);

    driv::I2C_Read(hDev, tbl_angl_addr_rd, 1, out_buf);
    int hi_raw = out_buf[0];

    hi_raw = hi_raw << 8;
    hi_raw = hi_raw | lo_raw;

    t1 = hi_raw;


    delete [] data_buf;

    /*
    QElapsedTimer timer;
    qint64 nanoSec = 0;
    timer.start();
    //something happens here
    //nanoSec = timer.nsecsElapsed();
    //printing the result(nanoSec)

    //something else happening here
    BYTE data_buf[8] = { 0 };

    int N = 100;
    for (int t = 0; t < N; t++)
    {
        timer.restart();

        driv::I2C_Write(hDev, expander_addr_wr, 1, data_buf);

        nanoSec += timer.nsecsElapsed();
    }

    t1 = double(nanoSec)/ N;*/
}


void sci_writeRegister(PVOID hDev, BYTE reg_addr, BYTE val)
{
    BYTE data_buf[8] = { 0 };
    data_buf[0] = reg_addr;
    data_buf[1] = val;
    driv::I2C_Write(hDev, spi_conv_addr_wr, 2, data_buf);

    struct timespec tw = {0, 10000*1000}; // sleep 10 millsec
    struct timespec tr;
    nanosleep (&tw, &tr);

    /*do
    {
        WIRE.beginTransmission(device_address);
        WIRE.write(reg_addr);
        WIRE.write(val);
    } while ( WIRE.endTransmission(1) == 2) ;
    delay(10);*/
}

void sci_pinMode_input(PVOID hDev, BYTE pin,  BYTE & reg_f7_gpio_d)
{
    reg_f7_gpio_d &= (((BYTE)~(0x03))<<(pin<1)); //clear the two control bit of the pin
    reg_f7_gpio_d |= (0x02<<(pin<<1)); //10 for input
    sci_writeRegister(hDev, 0xF7, reg_f7_gpio_d);

    /*
    reg_f7_gpio_d &= (((uint8_t)~(0x03))<<(pin<1));
    if (i_o == INPUT)
        reg_f7_gpio_d |= (0x02<<(pin<<1));
    else
        reg_f7_gpio_d |= (0x01<<(pin<<1));
    WriteRegister(0xF7, reg_f7_gpio_d);
    */
}

void sci_GPIOEnable(PVOID hDev, BYTE pin, BYTE & reg_f6_gpio_e)
{
    reg_f6_gpio_e |= ( 0x01 << pin );
    sci_writeRegister(hDev, 0xf6, reg_f6_gpio_e);
}

void sci_SSEnable(PVOID hDev, BYTE pin, BYTE & reg_f6_gpio_e)
{
    reg_f6_gpio_e &= (BYTE)(~(0x01 << pin ));
    sci_writeRegister(hDev, 0xf6, reg_f6_gpio_e);
}

void sci_digitalWrite(PVOID hDev, BYTE pin, int value,  BYTE & reg_f4_gpio_w)
{
    if ( value==0 ) {
        reg_f4_gpio_w &= (((BYTE)~(0x01))<<pin);
    } else {
        reg_f4_gpio_w |= (0x01<<pin);
    }

    sci_writeRegister( hDev, 0xF4, reg_f4_gpio_w);
}

int test_angle_measure(PVOID hDev)
{
    /*
BYTE dut_buf[8] = { 0 };
    BYTE out_buf[8] = { 0 };
    out_buf[0] = 0x06;
    driv::I2C_Write(hDev, 0x0A, 1, out_buf);


    struct timespec tw = {0, 10000*1000}; // sleep 10 millsec
    struct timespec tr;
    nanosleep (&tw, &tr);

    out_buf[0] = 0xFF;
    out_buf[1] = 0xFF;
    out_buf[2] = 0xFF;
    out_buf[3] = 0xFF;
*/
    int sz = 10;
    BYTE * dbuf  =  new BYTE[sz](10);
    int r = driv::I2C_Read(hDev, 0x0B, 1, dbuf);


    std::ofstream img;
    img.open ("i2cout.txt");
    for (int x = 0; x < sz; x++ )
        img << (int)dbuf[x] << std::endl;
    img << "result i2c read " << r << std::endl;
    img.close();


    //int res = out_buf[0]   | (out_buf[1] << 8)  | (out_buf[2] << 16) | (out_buf[3] << 24);

    return 11;

    /*

    BYTE data_buf[8] = { 0 };
    data_buf[0] = 0x01;
    data_buf[1] = 0x02;
    data_buf[2] = 0x03;
    driv::I2C_Write(hDev, 0x0B, 1, data_buf);
*/



   // reset
    BYTE reg_f0_config = 0x00; //configuration register 0xF0
    BYTE reg_f4_gpio_w = 0x00; //gpio write register
    BYTE reg_f6_gpio_e = 0x00; //gpio enable register
    BYTE reg_f7_gpio_d = 0xAA; //gpio direction

    sci_writeRegister(hDev, 0xf0, reg_f0_config);
    sci_writeRegister(hDev, 0xf4, reg_f4_gpio_w);
    sci_writeRegister(hDev, 0xf6, reg_f6_gpio_e);
    sci_writeRegister(hDev, 0xf7, reg_f7_gpio_d);

    sci_pinMode_input(hDev, 0, reg_f7_gpio_d);
    sci_pinMode_input(hDev, 1, reg_f7_gpio_d);
    sci_pinMode_input(hDev, 2, reg_f7_gpio_d);
    sci_pinMode_input(hDev, 3, reg_f7_gpio_d);

    sci_GPIOEnable(hDev, 0, reg_f6_gpio_e);
    sci_GPIOEnable(hDev, 1, reg_f6_gpio_e);
    sci_GPIOEnable(hDev, 2, reg_f6_gpio_e);
    sci_GPIOEnable(hDev, 3, reg_f6_gpio_e);

    for(int i = 0; i < 20; i++)
    {
        int val = (i%2);
        sci_digitalWrite(hDev, 0, val,  reg_f4_gpio_w);

        struct timespec tw = {0, 900000*1000}; // sleep 1 sec
        struct timespec tr;
        nanosleep (&tw, &tr);

    }

    return 0;

    //  i2cspi.begin(0);
    //sci_SSEnable(hDev, 0, reg_f6_gpio_e);
    //sci_SSEnable(hDev, 2, reg_f6_gpio_e);

/*
    BYTE out_buf[8] = { 0 };
    out_buf[0] = 0xf5;
    driv::I2C_Write(hDev, spi_conv_addr_wr, 2, out_buf);

    struct timespec tw = {0, 100000*1000}; // sleep 10 millsec
    struct timespec tr;
    nanosleep (&tw, &tr);

    out_buf[0] = 0x00;
    driv::I2C_Read(hDev, spi_conv_addr_rd, 2, out_buf);

    int res = out_buf[0] | (out_buf[1] << 8);


    return res;
*/


    /*



    BYTE data_buf[8] = { 0 };

    data_buf[0] = 0xF5; // set SSO
    driv::I2C_Write(hDev, spi_conv_addr_wr, 2, data_buf);

    data_buf[0] = 0x00;
    driv::I2C_Read(hDev, spi_conv_addr_rd, 2, data_buf); //small test check GPO
    int rres =  data_buf[0] | (data_buf[1] << 8) ;
    return rres;

    data_buf[0] = func_id; // get SS0 device

    // SPI Command Frame - ANGLE reg (wo correction) + read + parity
    unsigned short cmd =  0x3FFC | 0x4000 | 0x8000; 0x3FFE

    data_buf[2] = cmd >>8; //    // high bt
    data_buf[1] = (cmd <<8)>>8; // low bt
    driv::I2C_Write(hDev, spi_conv_addr_wr,  3, data_buf);


    BYTE out_buf[8] = { 0 };
    driv::I2C_Read(hDev, spi_conv_addr_rd, 4, out_buf);
    //int lo_raw = out_buf[1];
    //BYTE hi_raw = out_buf[0] << 8;
    int res =  out_buf[0] | (out_buf[1] << 8) | (out_buf[2] << 16) | (out_buf[3] << 24);
*/

   /**/
}

}
