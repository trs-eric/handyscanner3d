#ifndef __CAMAPI_H__
#define __CAMAPI_H__


/*
	GetDeviceList retreives a list of supported devices present in the system.
	
	Parameters:

		devList		A pointer to list of wide char strings. On successful return, it contains 
					a list of pointers to device path strings. The caller just need provide 
					a pointer to variable typed wchar_t**, and GetDeviceList will allocate 
					memory for the device path strings. devList must be freed with 
					FreeDeviceList() after finish using the device path strings.

		pnCount		A pointer to UINT variable provided by the caller. On successful return,
					it contains the number of supported devices that has been found.

	Return value:

		Return S_OK on success
		Return error code on fail conditions
*/
HRESULT GetDeviceList(wchar_t*** devList, UINT *pnCount);

/*
	FreeDeviceList frees devList returned by GetDeviceList()

	Parameters:

		devList		Pointer to device path strings returned by GetDeviceList()

	Return value:

		Return S_OK on success
		Return E_POINTER if devList is NULL
*/
HRESULT FreeDeviceList(wchar_t** devList);

/*
	OpenDevice opens a device specified by devPath, and return device handle in phDev

	Parameters:

		devPath		One of the device path string returned by GetDeviceList()

		phDev		On successful return, contains device handle that is opened

	Return value:
		
		Return S_OK on success
		Return error code on fail conditions
*/
HRESULT OpenDevice(wchar_t* devPath, PVOID *phDev);

/*
	CloseDevice closes a device previously opened with OpenDevice()

	Parameters:

		hDev		Device handle to be closed, must be a handle returned from OpenDevice()

	Return value:

		Return S_OK on success
		Return error code on fail conditions
*/
HRESULT CloseDevice(PVOID hDev);

/*
	I2C_Read performs read operation on the I2C bus to the target device

	Parameters:

		hDev		Device handle to be operated on
		
		bySlaveAddr	I2C slave address of the target I2C device

		uLength		Data length to be read from target I2C device

		pData		Data buffer to receive data, caller is responsible to allocate memory

	Return value:

		Return S_OK on success
		Return error code on fail conditions
*/
HRESULT I2C_Read(PVOID hDev, BYTE bySlaveAddr, UINT uLength, LPBYTE pData);

/*
	I2C_Write performs write operation on the I2C bus to the target device
	
	Parameters:

		hDev		Device handle to be operated on
		
		bySlaveAddr	I2C slave address of the target I2C device

		uLength		Data length to be written to target I2C device

		pData		Data to be sent to target device, caller is responsible to allocate memory

	Return value:

		Return S_OK on success
		Return error code on fail conditions
*/
HRESULT I2C_Write(PVOID hDev, BYTE bySlaveAddr, UINT uLength, LPBYTE pData);


#endif // __CAMAPI_H__
