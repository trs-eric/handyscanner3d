#ifndef __DRIVER_AUX_UTILS__
#define __DRIVER_AUX_UTILS__

#include "./WinTypes.h"

namespace driv
{
extern "C"
{
    #include "./CamAPI.h"
}
   // extern BYTE cam_i2c_addr;
   // extern BYTE laser_i2c_addr_wr;
   // extern BYTE laser_i2c_addr_rd;

    extern bool vmotor_on;
    extern bool vmotor_left;
    extern bool vmotor_step;
    extern unsigned int motor_sleep_usecs;
    extern int table_rot_angle;

    void laser_turn_on(PVOID hDev);
    void laser_turn_off(PVOID hDev);
    void update_expander_status(PVOID hDev);
    void update_motor_status();

    void motor_stop(PVOID hDev);
    void motor_left(PVOID hDev);
    void motor_right(PVOID hDev);

    void motor_step(PVOID hDev);

    void table_rot(PVOID hDev);

    void init_accelerometer(PVOID hDev);
    void read_accelerometer_gdata(PVOID hDev, int & x, int & y, int & z);
    void read_accelerometer_roll_pitch(PVOID hDev, float & roll, float& pitch);


    void get_nano_measure(PVOID hDev, double & t1, double & t2);
    int test_angle_measure(PVOID hDev);

    int  get_laser_angle(PVOID hDev);
    void set_ELED_brightness(PVOID hDev, int v);
    void set_M0(PVOID hDev, int v);
    void set_M1(PVOID hDev, int v);
    void turn_table_request(PVOID hDev, int v);
    int  turn_table_status(PVOID hDev);
    void motor_dir_change(PVOID hDev, int dir);
    void motor_rotate_signal(PVOID hDev, int sig);
    void set_RGBLed_X(PVOID hDev, BYTE cmd, int r, int g, int b);


}
#endif
