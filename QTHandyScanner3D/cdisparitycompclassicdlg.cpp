#include "cdisparitycompclassicdlg.h"
#include "ui_cdisparitycompclassicdlg.h"


#include "cglwidget.h"
#include "cdisparitycompclassicthread.h"
#include "cpointcloud.h"


CDisparityCompClassicDlg::CDisparityCompClassicDlg(CGLWidget * glWidget, QWidget *parent) :
    _glWidget(glWidget), QDialog(parent),
    ui(new Ui::CDisparityCompClassicDlg)
{

    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);

    ui->limgEdt->setText(QString("imgU_left_1.bmp"));
    ui->rimgEdt->setText(QString("imgU_right_1.bmp"));
    ui->maxDispEdt->setText(QString::number(256));
    ui->shiftLeftEdt->setText(QString::number(0));//-40

    ui->SADWinSzEdt->setText(QString::number(3));
    ui->filterCapEdt->setText(QString::number(63));
    ui->uniqnessEdt->setText(QString::number(10));
    ui->speckleWinSzEdt->setText(QString::number(100));
    ui->speckleRangeEdt->setText(QString::number(32));
    ui->Disp12MaxDiffEdt->setText(QString::number(1));
    ui->FullDpEdt->setText(QString::number(0));
    ui->P1Edt->setText(QString::number(216));
    ui->P2Edt->setText(QString::number(864));

    connect(ui->previewBtn, SIGNAL(clicked()), this, SLOT(onPreviewBtn()));
    connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));

    _thread = new CDisparityCompClassicThread;


    // user pressed cancel button
      connect(ui->cancelBtn,  SIGNAL(clicked()), _thread, SLOT(stop_process()));
      connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

      // user closed dialg
      connect(this,  SIGNAL(finished(int)), _thread, SLOT(stop_process()));

      // working with a thread
      connect(_thread, SIGNAL(send_back(int)), this, SLOT(onMakeProgressStep(int)));
      connect(_thread, SIGNAL(send_result(std::vector<C3DPoint> * )), this, SLOT(onCompleteProcessing(std::vector<C3DPoint> * )));
      connect(_thread, SIGNAL(send_result(std::vector<C3DPoint> * )), _glWidget, SLOT(construct_new_scan(std::vector<C3DPoint> *)));
      connect(_thread, SIGNAL(send_result(CGLObject * )), _glWidget, SLOT(add_new_globj(CGLObject*)));

      connect(_thread, SIGNAL(finished()), this, SLOT(onStopProcessing()));



}

void CDisparityCompClassicDlg::onMakeProgressStep(int val)
{
    ui->progressBar->setValue(val);
}

void CDisparityCompClassicDlg::onCompleteProcessing(std::vector<C3DPoint> * )
{
    if(_on_apply)
    {
        close_dialog();
        return;
    }

    _is_result_valid = true;
    ui->progressBar->setValue(100);
}
void CDisparityCompClassicDlg::onStopProcessing()
{
    ui->applyBtn->setEnabled(true);
    ui->previewBtn->setEnabled(true);

    ui->limgEdt->setEnabled(true);
    ui->rimgEdt->setEnabled(true);
    ui->maxDispEdt->setEnabled(true);
    ui->shiftLeftEdt->setEnabled(true);

    ui->SADWinSzEdt->setEnabled(true);
    ui->filterCapEdt->setEnabled(true);
    ui->uniqnessEdt->setEnabled(true);
    ui->speckleWinSzEdt->setEnabled(true);
    ui->speckleRangeEdt->setEnabled(true);
    ui->Disp12MaxDiffEdt->setEnabled(true);

    ui->FullDpEdt->setEnabled(true);
    ui->P1Edt->setEnabled(true);
    ui->P2Edt->setEnabled(true);


    _in_process = false;
}
CDisparityCompClassicDlg::~CDisparityCompClassicDlg()
{
    delete ui;
    delete _thread;

}

void CDisparityCompClassicDlg::close_dialog()
{
    _on_apply = false;
    close();
}

void CDisparityCompClassicDlg::onCancelBtn()
{
    if(!_in_process)
    {
        _glWidget->discard_raw_points_of_current_object();
        close_dialog();
    }
    _in_process = false;
    _on_apply = false;
}


void CDisparityCompClassicDlg::onPreviewBtn()
{

    ui->applyBtn->setEnabled(false);
    ui->previewBtn->setEnabled(false);
    ui->limgEdt->setEnabled(false);
    ui->rimgEdt->setEnabled(false);
    ui->maxDispEdt->setEnabled(false);
    ui->shiftLeftEdt->setEnabled(false);

    ui->SADWinSzEdt->setEnabled(false);
    ui->filterCapEdt->setEnabled(false);
    ui->uniqnessEdt->setEnabled(false);
    ui->speckleWinSzEdt->setEnabled(false);
    ui->speckleRangeEdt->setEnabled(false);
    ui->Disp12MaxDiffEdt->setEnabled(false);
    ui->FullDpEdt->setEnabled(false);
    ui->P1Edt->setEnabled(false);
    ui->P2Edt->setEnabled(false);

    _in_process = true;

    QImage * left_im = NULL; ///new QImage(ui->limgEdt->text());
    QImage * right_im = NULL; ///new QImage(ui->rimgEdt->text());

    _thread->start_process(left_im, right_im,
                           ui->maxDispEdt->text().toInt(),
                           ui->shiftLeftEdt->text().toInt(),
                            ui->SADWinSzEdt->text().toInt(),
                            ui->filterCapEdt->text().toInt(),
                            ui->uniqnessEdt->text().toInt(),
                            ui->speckleWinSzEdt->text().toInt(),
                            ui->speckleRangeEdt->text().toInt(),
                            ui->Disp12MaxDiffEdt->text().toInt(),
                            ui->FullDpEdt->text().toInt(),
                            ui->P1Edt->text().toInt(),
                            ui->P2Edt->text().toInt()) ;
}

void CDisparityCompClassicDlg::onApplyBtn()
{
    _on_apply = true;
    if(!_is_result_valid)
        onPreviewBtn();
    else
        close_dialog();
}


void CDisparityCompClassicDlg::showEvent(QShowEvent * event)
{
    ui->progressBar->setValue(0);

    _is_result_valid    = false;
    _on_apply           = false;
    _in_process         = false;

}

