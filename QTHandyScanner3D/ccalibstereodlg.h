#ifndef CCALIBSTEREODLG_H
#define CCALIBSTEREODLG_H

#include <QDialog>

namespace Ui {
class CCalibStereoDlg;
}

class CCalibStereoDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CCalibStereoDlg(QWidget *parent = 0);
    ~CCalibStereoDlg();

private:
    Ui::CCalibStereoDlg *ui;
};

#endif // CCALIBSTEREODLG_H
