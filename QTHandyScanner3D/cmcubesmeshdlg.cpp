#include "cmcubesmeshdlg.h"
#include "ui_cmcubesmeshdlg.h"

#include <QMessageBox>

#include "cglwidget.h"
#include "graphicutils.h"
#include "cframe.h"

#include <opencv2/highgui/highgui.hpp>


CMCubesMeshDlg::CMCubesMeshDlg(CGLWidget * glWidget, QWidget *parent) :
     _glWidget(glWidget), QDialog(parent),
    ui(new Ui::CMCubesMeshDlg)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);
    connect(ui->computeBtn, SIGNAL(clicked()), this, SLOT(onComputeBtn()));
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

    connect(this, SIGNAL(send_result(CTriangularMesh*)), _glWidget, SLOT(on_mesh_computed(CTriangularMesh*)));
    connect(this, SIGNAL(send_fpcl(std::vector<C3DPoint> * )), _glWidget, SLOT(construct_new_scan(std::vector<C3DPoint> *)));

    ui->edtODepth->setText(QString::number(global_octo_depth_max));
    ui->edtDim->setText(QString::number(std::pow(2.0,global_octo_depth_max)));

    connect(ui->edtODepth, SIGNAL(textEdited(QString)), this, SLOT(odepth_changed(QString)));

    ///// basic algorithm constant

    // density matrix size
    _matrix_dim_N = std::pow(2,global_octo_depth_max);


    // neighbour size for 256 pix +/-2 pix window
    _nei_windsz_factor = 2.0 / 256;

    _neighb_size_f = float(_matrix_dim_N) * _nei_windsz_factor;
    _neighb_size_i = (int) (_neighb_size_f +0.5);

    // gauss kernel
    _gauss_kernel = NULL;
    _gkernel_size = 0;
    _gsigma = _neighb_size_f/3.0;

    _midle_fill_vlue = 100.0;


}

CMCubesMeshDlg::~CMCubesMeshDlg()
{
    delete ui;
}

void CMCubesMeshDlg::onCancelBtn()
{
   close();
}

void CMCubesMeshDlg::odepth_changed(QString s)
{
    int v = s.toInt();

    global_octo_depth_max = v;
    global_octo_depth_cut = v - 2;
    ui->edtDim->setText(QString::number(std::pow(2.0,global_octo_depth_max)));

    std::cout  << global_octo_depth_max  << " " <<global_octo_depth_cut << std::endl;
}

/*
void CMCubesMeshDlg::set_in_matrix_neighbourhood_improved( CArray3D<uchar> * dmatrix,
                                                           int i, int j, int k, int neighb_sz)
{
    int xs = dmatrix->_xs;
    int ys = dmatrix->_ys;
    int zs = dmatrix->_zs;


    for(int z = k - neighb_sz ; z <= k + neighb_sz; z++)
    {
        for(int y = j - neighb_sz ; y <= j + neighb_sz; y++)
        {
            for(int x = i - neighb_sz ; x <= i + neighb_sz; x++)
            {

                if( z >= 0 && z < zs &&
                    y >= 0 && y < ys &&
                    x >= 0 && x < xs)
                {



                    int distance = (k-z)*(k-z)+(j-y)*(j-y) + (i-x) *(i-x) ;
                    float mult = _midle_fill_vlue * _gauss_kernel[distance];
                    uchar oval = dmatrix->_data[x][y][z];
                    if(oval == 0)
                    {
                           // set value
                            dmatrix->_data[x][y][z] = mult;
                    }
                    else
                    {
                        // multiply values
                        float nval = float(oval) * mult / 100.0;
                        dmatrix->_data[x][y][z] = (uchar) nval;
                    }

                } /// in 3d matrix
            }/// x
        } /// y
    } /// z


}

void CMCubesMeshDlg::set_in_matrix_neighbourhood( CArray3D<uchar> * dmatrix, int i, int j, int k)
{
    int xs = dmatrix->_xs;
    int ys = dmatrix->_ys;
    int zs = dmatrix->_zs;

    int neighb_sz = 6;
    float mid_vlue = 8;

    for(int z = k - neighb_sz ; z <= k + neighb_sz; z++)
    {
        for(int y = j - neighb_sz ; y <= j + neighb_sz; y++)
        {
            for(int x = i - neighb_sz ; x <= i + neighb_sz; x++)
            {

                if( z >= 0 && z < zs &&
                    y >= 0 && y < ys &&
                    x >= 0 && x < xs)
                {

                    int dz = ((z-k)<0)? (k-z) : (z-k);
                    int dy = ((y-j)<0)? (j-y) : (y-j);
                    int dx = ((x-i)<0)? (i-x) : (x-i);

                    int dist = (dz>dy)?dz:dy;
                    dist = (dist>dx)?dist:dx;

                    float add = mid_vlue * float(neighb_sz - dist) / float(neighb_sz);
                    uchar oval = dmatrix->_data[x][y][z];
                    uchar addv = uchar(add);
                    if((oval+addv)<255)
                        dmatrix->_data[x][y][z] = oval + addv;

                } /// in 3d matrix

            }/// x
        } /// y
    } /// z

}
*/


/*

   1.  Q: what is the relation for BB in cese of SPCL (small res) and DPCL (high res)
    2. Q: how to resolve photo-density vs isovalue
*/


void CMCubesMeshDlg::init_gauss_kernel()
{
    if(_gauss_kernel != NULL)
        delete [] _gauss_kernel;

    _gkernel_size = _neighb_size_i*_neighb_size_i*4;
    _gauss_kernel = new double[_gkernel_size];
    for(int i = 0; i < _gkernel_size ; i++)
    {
        double val = i;
        double sqval = std::sqrt(val);
        _gauss_kernel[i] = std::exp( -sqval*sqval/(2*_gsigma*_gsigma));
    }
}

/*
void CMCubesMeshDlg::set_in_octocube_neighbourhood( COctoCube<float> * cube,
                                                    float ix, float iy, float iz, int neighb_sz)
{

    float fstep = cube->get_smallest_cube_side();
    /// std::cout << "FSTEP " << fstep << std::endl;
    for(int z = -neighb_sz ; z <= neighb_sz; z++)
    {
        for(int y = -neighb_sz ; y <= neighb_sz; y++)
        {
            for(int x = -neighb_sz ; x <= neighb_sz; x++)
            {

                    int distance = z * z + y * y + x * x;
                    float add =  _midle_fill_vlue * _gauss_kernel[distance];
                   // uchar addv = uchar(add);

                    float cx = ix + float(x)*fstep;
                    float cy = iy + float(y)*fstep;
                    float cz = iz + float(z)*fstep;

                    cube->add_value(cx,cy,cz,add);
            }/// x
        } /// y
    } /// z
}
*/

void CMCubesMeshDlg::onComputeBtn()
{

    //// phase 1. initialize 3d matrix
    std::vector<C3DPoint> * spnts = _glWidget->get_raw_points_of_current_object()->get_points_direct_access();

    C3DPoint min, max;
    compute_bounding_box(spnts, &min, &max);
    std::cout << "mn: " << min._x << " " << min._y << " " << min._z << std::endl;
    std::cout << "mx: " << max._x << " " << max._y << " " << max._z << std::endl;

    make_bounding_box_sq(&min, &max);
    std::cout << "mn: " << min._x << " " << min._y << " " << min._z << std::endl;
    std::cout << "mx: " << max._x << " " << max._y << " " << max._z << std::endl;

    // expand bb by 10 percents
    // should we do bb the same size
    expand_bounding_box(&min, &max, 0.05);
    std::cout << "mn: " << min._x << " " << min._y << " " << min._z << std::endl;
    std::cout << "mx: " << max._x << " " << max._y << " " << max._z << std::endl;

    COctoCube<avpnt> cube((max._x + min._x)/2.0, // x-center
                         (max._y + min._y)/2.0, // y-center
                         (max._z + min._z)/2.0, // z-center
                         (max._x - min._x)/2.0); // side-half

    ////////////////////////////////////////////////////////////


    //// phase 2. process frames
    unsigned long int total_points = 0;

    std::vector<CFrame *>::iterator itf = global_keyframes_sequence.begin();
    for(int i= 0; itf != global_keyframes_sequence.end(); itf++, i++)
    {
      std::cout << "frame prcoessing : " << i << std::endl;
      (*itf)->load_LR_images_from_hd();
      (*itf)->compute_disparity_map();
      (*itf)->compute_depth_image();


      std::vector<C3DPoint> * pnts  = (*itf)->construct_spcl_from_depth_img(false);
          std::cout << "start filling octocube " << std::endl;
          std::vector<C3DPoint>::iterator it = pnts->begin() ;
          int psz =  (pnts->size())/10;
          total_points +=pnts->size();

          for( int p =0; it != pnts->end(); it++, p++)
          {
              float proc = float(p)  * 1000.0 / float (psz);
              if( (p%psz) ==0  )
                  std::cout << "p = " << proc << " " <<  p << " " << psz << std::endl;
              cube.add_point(it->_x, it->_y, it->_z);
          }
         std::cout << "finish filling octocube" << std::endl;

      delete pnts;
      (*itf)->dump_unused_data();

    }
    std::cout << "total processed points count is " << total_points  << std::endl;

    /////////////////////////////////////////////////////////////
    //// phase 3. get merged points

    std::list<C3DPoint> merged_points = cube.get_points();
    cube.clear_data();

    std::cout << "found " <<  merged_points.size() << " points from " << total_points << std::endl;

    std::vector<C3DPoint> *  final_res = new std::vector<C3DPoint>(merged_points.size());

    std::list<C3DPoint>::iterator itm = merged_points.begin();
    for( int i = 0; itm != merged_points.end(); itm++, i++ )
       (*final_res)[i] = *itm;

     emit send_fpcl(final_res);

    QString msg  = QString("merged points  %1 from %2").arg(QString::number( merged_points.size())).arg(QString::number(total_points));
    QMessageBox::information(this, tr("Information"), msg);


    return;

  /*   /// cube.dump("octomax.raw"); // 1024

    /// phase 2. run marshing cube
    float isovalue = 400;
    CTriMesh * rmesh = new CTriMesh ;
    rmesh->_points.resize(200000);
    rmesh->_points.resize(0);
    std::cout << "points are " << rmesh->_points.size() << std::endl;
/// return;

    run_marching_cubesf(cube, isovalue, &rmesh->_points, min._x, min._y, min._z,
                       (max._x - min._x)/ float(_matrix_dim_N-1),
                       (max._y - min._y)/ float(_matrix_dim_N-1),
                       (max._z - min._z)/ float(_matrix_dim_N-1));
    std::cout << "found vertexs " << rmesh->_points.size() << std::endl;
    ///cube.clean_data();

    __int64 face_count = rmesh->_points.size()/3;
    rmesh->_triangles.resize(face_count);
    for ( __int64 i = 0; i < face_count; i++)
    {
            C3DPointIdx tri;
            tri.pnt_index[0] = i*3;
            tri.pnt_index[1] = i*3+1;
            tri.pnt_index[2] = i*3+2;

            rmesh->_triangles[i] = tri;
    }
    std::cout << "rmesh->_trinagles done" << std::endl;

    std::vector<C3DPointIdx>::iterator itt = rmesh->_triangles.begin();
    for(__int64 fidx = 0 ; itt != rmesh->_triangles.end(); itt++, fidx++)
    {
          rmesh->_points[ itt->pnt_index[0] ]._faceidx.push_back(fidx);
          C3DPoint pnt0 = rmesh->_points[ itt->pnt_index[0] ];

          rmesh->_points[ itt->pnt_index[1] ]._faceidx.push_back(fidx);
          C3DPoint pnt1 = rmesh->_points[ itt->pnt_index[1] ];

          rmesh->_points[ itt->pnt_index[2] ]._faceidx.push_back(fidx);
          C3DPoint pnt2 = rmesh->_points[ itt->pnt_index[2] ];

          itt->tri_normal = compute_normal( pnt2, pnt1, pnt0);
    }
    CTriangularMesh * mmesh = new CTriangularMesh;
    mmesh->set_mesh(rmesh);

    std::cout << "mesh is done" << std::endl;
    emit send_result(mmesh);*/
}
/*
void CMCubesMeshDlg::onComputeBtn_old()
{
    //// phase 1. initialize 3d matrix
    std::vector<C3DPoint> * pnts = _glWidget->get_raw_points_of_current_object()->get_points_direct_access();

    C3DPoint min, max;
    compute_bounding_box(pnts, &min, &max);
    std::cout << "mn: " << min._x << " " << min._y << " " << min._z << std::endl;
    std::cout << "mx: " << max._x << " " << max._y << " " << max._z << std::endl;

    make_bounding_box_sq(&min, &max);
    std::cout << "mn: " << min._x << " " << min._y << " " << min._z << std::endl;
    std::cout << "mx: " << max._x << " " << max._y << " " << max._z << std::endl;

    // expand bb by 10 percents
    // should we do bb the same size
    expand_bounding_box(&min, &max, 0.05);
    std::cout << "mn: " << min._x << " " << min._y << " " << min._z << std::endl;
    std::cout << "mx: " << max._x << " " << max._y << " " << max._z << std::endl;


    CArray3D<uchar> _density_matrix(_matrix_dim_N, _matrix_dim_N,_matrix_dim_N);

   // std::vector<C3DPoint>	 test_points;
   // test_points.resize(4000000);
    //std::cout << "test_points is " << test_points.size() << std::endl;

   // std::vector<C3DPoint>	 test_points2;
   // test_points2.resize(8000000);
   // std::cout << "test_points2 is " << test_points2.size() << std::endl;

    init_gauss_kernel();

    std::vector<C3DPoint>::iterator it = pnts->begin() ;
    float factor_x = float(_matrix_dim_N) / (max._x - min._x);
    float factor_y = float(_matrix_dim_N) / (max._y - min._y);
    float factor_z = float(_matrix_dim_N) / (max._z - min._z);

    //// compute neighbourhood size
     for( ; it != pnts->end(); it++)
    {
        int i = (it->_x - min._x) * factor_x;
        int j = (it->_y - min._y) * factor_y;
        int k = (it->_z - min._z) * factor_z;

        /// set_in_matrix_neighbourhood(&_density_matrix, i, j, k);
        set_in_matrix_neighbourhood_improved( &_density_matrix, i, j, k, _neighb_size_i);

    }
    _density_matrix.dump("matrix.raw");
    std::cout << "matrix is done " << std::endl;
  //  return;

    /// phase 2. run marshing cube
    uchar isovalue = 220;
    CTriMesh * rmesh = new CTriMesh ;
     rmesh->_points.resize(200000);
     rmesh->_points.resize(0);
    std::cout << "points are " << rmesh->_points.size() << std::endl;
/// return;

    run_marching_cubes(_density_matrix, isovalue, &rmesh->_points, min._x, min._y, min._z,
                       (max._x - min._x)/ float(_matrix_dim_N-1),
                       (max._y - min._y)/ float(_matrix_dim_N-1),
                       (max._z - min._z)/ float(_matrix_dim_N-1));
    std::cout << "found vertexs " << rmesh->_points.size() << std::endl;
    _density_matrix.clean_data();


    __int64 face_count = rmesh->_points.size()/3;
    rmesh->_triangles.resize(face_count);
    for ( __int64 i = 0; i < face_count; i++)
    {
            C3DPointIdx tri;
            tri.pnt_index[0] = i*3;
            tri.pnt_index[1] = i*3+1;
            tri.pnt_index[2] = i*3+2;

            rmesh->_triangles[i] = tri;
    }
    std::cout << "rmesh->_trinagles done" << std::endl;

    std::vector<C3DPointIdx>::iterator itt = rmesh->_triangles.begin();
    for(__int64 fidx = 0 ; itt != rmesh->_triangles.end(); itt++, fidx++)
    {
          rmesh->_points[ itt->pnt_index[0] ]._faceidx.push_back(fidx);
          C3DPoint pnt0 = rmesh->_points[ itt->pnt_index[0] ];

          rmesh->_points[ itt->pnt_index[1] ]._faceidx.push_back(fidx);
          C3DPoint pnt1 = rmesh->_points[ itt->pnt_index[1] ];

          rmesh->_points[ itt->pnt_index[2] ]._faceidx.push_back(fidx);
          C3DPoint pnt2 = rmesh->_points[ itt->pnt_index[2] ];

          itt->tri_normal = compute_normal( pnt2, pnt1, pnt0);
    }
    CTriangularMesh * mmesh = new CTriangularMesh;
    mmesh->set_mesh(rmesh);

    std::cout << "mesh is done" << std::endl;
    emit send_result(mmesh);


}
*/

/* std::cout << "found verteces " << vertexlist.size() << std::endl;


__int64 vsz = vertexlist.size();
std::cout << vsz << std::endl;
.resize(vsz);
std::cout << "resized "<< std::endl;

std::vector<vertex>::iterator itv = vertexlist.begin();
for( __int64 c = 0 ; itv != vertexlist.end(); itv++,c++ )
{
    C3DPoint xpp;
    xpp._x = min._x + itv->x * (max._x - min._x)/ float(_matrix_dim_N-1) ;
    xpp._y = min._y + itv->y * (max._y - min._y)/ float(_matrix_dim_N-1) ;
    xpp._z = min._z + itv->z * (max._z - min._z)/ float(_matrix_dim_N-1) ;
    xpp._nx = itv->normal_x;
    xpp._ny = itv->normal_y;
    xpp._nz = itv->normal_z;
    rmesh->_points[c] = xpp;

    if(!(c%100))
        std::cout << c << std::endl;
}
std::cout << "rmesh->_points done" << std::endl;
*/
