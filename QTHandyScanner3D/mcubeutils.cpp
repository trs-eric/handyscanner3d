#include "mcubeutils.h"


vertex interpolate(uchar isolevel, vertex p1, vertex p2, int valp1, int valp2)
{

        if(isolevel == valp1)
            return p1;
        if(isolevel == valp2)
            return p2;
        if(valp1 == valp2)
            return p1;

      vertex p;
      double diff = (double)(isolevel - valp1) / (valp2 - valp1);
      p.x = p1.x + diff * (p2.x - p1.x);
      p.y = p1.y + diff * (p2.y - p1.y);
      p.z = p1.z + diff * (p2.z - p1.z);

      p.normal_x = p1.normal_x + diff * (p2.normal_x - p1.normal_x);
      p.normal_y = p1.normal_y + diff * (p2.normal_y - p1.normal_y);
      p.normal_z = p1.normal_z + diff * (p2.normal_z - p1.normal_z);

      return p;
  }

vertex interpolatef(float isolevel, vertex p1, vertex p2, float valp1, float valp2)
{
    if(std::abs(isolevel - valp1) < 0.001) // TODO check how this error influences quality
        return p1;
    if(std::abs(isolevel - valp2) < 0.001)
        return p2;
    if(std::abs(valp1 - valp2) < 0.001)
        return p1;

    vertex p;
    float diff = (isolevel - valp1) / (valp2 - valp1);
    p.x = p1.x + diff * (p2.x - p1.x);
    p.y = p1.y + diff * (p2.y - p1.y);
    p.z = p1.z + diff * (p2.z - p1.z);

    p.normal_x = p1.normal_x + diff * (p2.normal_x - p1.normal_x);
    p.normal_y = p1.normal_y + diff * (p2.normal_y - p1.normal_y);
    p.normal_z = p1.normal_z + diff * (p2.normal_z - p1.normal_z);

    return p;
}

void process_cubef(cubef & icube, float & isovalue, std::vector<C3DPoint> * res,
                  float minx, float miny, float minz, float scalex, float scaley, float scalez)
{
    uchar cubeindex = 0;
    if(icube.val[0] > isovalue) cubeindex |= 1;
    if(icube.val[1] > isovalue) cubeindex |= 2;
    if(icube.val[2] > isovalue) cubeindex |= 4;
    if(icube.val[3] > isovalue) cubeindex |= 8;
    if(icube.val[4] > isovalue) cubeindex |= 16;
    if(icube.val[5] > isovalue) cubeindex |= 32;
    if(icube.val[6] > isovalue) cubeindex |= 64;
    if(icube.val[7] > isovalue) cubeindex |= 128;

    // Cube is entirely in/out of the surface
    if(edgeTable[cubeindex] == 0 || edgeTable[cubeindex] == 255)
        return;

     vertex vertlist[12];
     // Find the vertices where the surface intersects the cube
     if(edgeTable[cubeindex] & 1)
         vertlist[0] = interpolatef(isovalue,icube.p[0],icube.p[1],icube.val[0],icube.val[1]);
     if(edgeTable[cubeindex] & 2)
              vertlist[1] = interpolatef(isovalue,icube.p[1],icube.p[2],icube.val[1],icube.val[2]);
     if(edgeTable[cubeindex] & 4)
              vertlist[2] = interpolatef(isovalue,icube.p[2],icube.p[3],icube.val[2],icube.val[3]);
     if(edgeTable[cubeindex] & 8)
              vertlist[3] = interpolatef(isovalue,icube.p[3],icube.p[0],icube.val[3],icube.val[0]);
     if(edgeTable[cubeindex] & 16)
              vertlist[4] = interpolatef(isovalue,icube.p[4],icube.p[5],icube.val[4],icube.val[5]);
     if(edgeTable[cubeindex] & 32)
              vertlist[5] = interpolatef(isovalue,icube.p[5],icube.p[6],icube.val[5],icube.val[6]);
     if(edgeTable[cubeindex] & 64)
              vertlist[6] = interpolatef(isovalue,icube.p[6],icube.p[7],icube.val[6],icube.val[7]);
     if(edgeTable[cubeindex] & 128)
              vertlist[7] = interpolatef(isovalue,icube.p[7],icube.p[4],icube.val[7],icube.val[4]);
     if(edgeTable[cubeindex] & 256)
              vertlist[8] = interpolatef(isovalue,icube.p[0],icube.p[4],icube.val[0],icube.val[4]);
     if(edgeTable[cubeindex] & 512)
              vertlist[9] = interpolatef(isovalue,icube.p[1],icube.p[5],icube.val[1],icube.val[5]);
     if(edgeTable[cubeindex] & 1024)
              vertlist[10] = interpolatef(isovalue,icube.p[2],icube.p[6],icube.val[2],icube.val[6]);
     if(edgeTable[cubeindex] & 2048)
              vertlist[11] = interpolatef(isovalue,icube.p[3],icube.p[7],icube.val[3],icube.val[7]);

    for(int i = 0; triTable[cubeindex][i] != -1; i++)
    {
         vertex v = vertlist[triTable[cubeindex][i]];

         C3DPoint xpp;
         xpp._x = minx + v.x * scalex ;
         xpp._y = miny + v.y * scaley ;
         xpp._z = minz + v.z * scalez ;

         xpp._nx = v.normal_x;///ln;
         xpp._ny = v.normal_y;///ln;
         xpp._nz = v.normal_z;///ln;
         res->push_back(xpp);
    }

}

void process_cube(cube & icube, uchar & isovalue, std::vector<C3DPoint> * res,
                  float minx, float miny, float minz, float scalex, float scaley, float scalez)
{

       unsigned char cubeindex = 0;
       if(icube.val[0] > isovalue) cubeindex |= 1;
       if(icube.val[1] > isovalue) cubeindex |= 2;
       if(icube.val[2] > isovalue) cubeindex |= 4;
       if(icube.val[3] > isovalue) cubeindex |= 8;
       if(icube.val[4] > isovalue) cubeindex |= 16;
       if(icube.val[5] > isovalue) cubeindex |= 32;
       if(icube.val[6] > isovalue) cubeindex |= 64;
       if(icube.val[7] > isovalue) cubeindex |= 128;

       // Cube is entirely in/out of the surface
       if(edgeTable[cubeindex] == 0 || edgeTable[cubeindex] == 255)
           return;

        vertex vertlist[12];
        // Find the vertices where the surface intersects the cube
        if(edgeTable[cubeindex] & 1)
            vertlist[0] = interpolate(isovalue,icube.p[0],icube.p[1],icube.val[0],icube.val[1]);
        if(edgeTable[cubeindex] & 2)
                 vertlist[1] = interpolate(isovalue,icube.p[1],icube.p[2],icube.val[1],icube.val[2]);
        if(edgeTable[cubeindex] & 4)
                 vertlist[2] = interpolate(isovalue,icube.p[2],icube.p[3],icube.val[2],icube.val[3]);
        if(edgeTable[cubeindex] & 8)
                 vertlist[3] = interpolate(isovalue,icube.p[3],icube.p[0],icube.val[3],icube.val[0]);
        if(edgeTable[cubeindex] & 16)
                 vertlist[4] = interpolate(isovalue,icube.p[4],icube.p[5],icube.val[4],icube.val[5]);
        if(edgeTable[cubeindex] & 32)
                 vertlist[5] = interpolate(isovalue,icube.p[5],icube.p[6],icube.val[5],icube.val[6]);
        if(edgeTable[cubeindex] & 64)
                 vertlist[6] = interpolate(isovalue,icube.p[6],icube.p[7],icube.val[6],icube.val[7]);
        if(edgeTable[cubeindex] & 128)
                 vertlist[7] = interpolate(isovalue,icube.p[7],icube.p[4],icube.val[7],icube.val[4]);
        if(edgeTable[cubeindex] & 256)
                 vertlist[8] = interpolate(isovalue,icube.p[0],icube.p[4],icube.val[0],icube.val[4]);
        if(edgeTable[cubeindex] & 512)
                 vertlist[9] = interpolate(isovalue,icube.p[1],icube.p[5],icube.val[1],icube.val[5]);
        if(edgeTable[cubeindex] & 1024)
                 vertlist[10] = interpolate(isovalue,icube.p[2],icube.p[6],icube.val[2],icube.val[6]);
        if(edgeTable[cubeindex] & 2048)
                 vertlist[11] = interpolate(isovalue,icube.p[3],icube.p[7],icube.val[3],icube.val[7]);

       for(int i = 0; triTable[cubeindex][i] != -1; i++)
       {
            vertex v = vertlist[triTable[cubeindex][i]];

            C3DPoint xpp;
            xpp._x = minx + v.x * scalex ;
            xpp._y = miny + v.y * scaley ;
            xpp._z = minz + v.z * scalez ;


           // float ln = sqrt(v.normal_x*v.normal_x + v.normal_y*v.normal_y+v.normal_z*v.normal_z);
            xpp._nx = v.normal_x;///ln;
            xpp._ny = v.normal_y;///ln;
            xpp._nz = v.normal_z;///ln;
            res->push_back(xpp);
       }
}


void run_marching_cubesf(COctoCube<float> & voxels, float & isovalue, std::vector<C3DPoint> * res,
                        float minx, float miny, float minz, float scalex, float scaley, float scalez,
                          int stepX = 1, int stepY = 1, int stepZ = 1)
{
    int sizeX = voxels.get_cube_side_sz();
    int sizeY = voxels.get_cube_side_sz();
    int sizeZ = voxels.get_cube_side_sz();

    CArray2D<float>  * x0arr = NULL; // voxels.get_slice_at_x(      0);
    CArray2D<float>  * xarr  = NULL; //voxels.get_slice_at_x(  stepX);
    CArray2D<float>  * x1arr = NULL;// voxels.get_slice_at_x(2*stepX);
    CArray2D<float>  * x2arr = NULL; //voxels.get_slice_at_x(3*stepX);



    for(int x = stepX; x < sizeX - 2*stepX; x += stepX)
    {
        std::cout << x << std::endl;
        for(int y = stepY; y < sizeY - 2*stepY; y += stepY)
        {
            for(int z = stepZ; z < sizeZ - 2*stepZ; z += stepZ) // check one more time ranges
            {

                cubef c;

                float n00 =  (x1arr->_data[y][z] - x0arr->_data[y][z]) / -stepX;
                float n01 =  (xarr->_data[y+stepY][z]-xarr->_data[y-stepY][z])/ -stepY;
                float n02 =  (xarr->_data[y][z+stepZ]-xarr->_data[y][z-stepZ]) / -stepZ;
                vertex v0 = {x      ,y      ,z   , n00, n01, n02};

                float n10 = (x2arr->_data[y][z]-xarr->_data[y][z]) / -stepX;
                float n11 = (x1arr->_data[y+stepY][z]-x1arr->_data[y-stepY][z]) / -stepY;
                float n12 = (x1arr->_data[y][z+stepZ]-x1arr->_data[y][z-stepZ]) / -stepZ;
                vertex v1 = {x+stepX,y      ,z    ,   n10, n11, n12};

                float n20 =  (x2arr->_data[y][z+stepZ]-xarr->_data[y][z+stepZ]) / -stepX;
                float n21 =  (x1arr->_data[y+stepY][z+stepZ]-x1arr->_data[y-stepY][z+stepZ]) /-stepY;
                float n22 =  (x1arr->_data[y][z+2*stepZ]-x1arr->_data[y][z]) / -stepZ;
                vertex v2 = {x+stepX, y      ,z+stepZ, n20, n21, n22};


                float n30 = (x1arr->_data[y][z+stepZ]-x0arr->_data[y][z+stepZ]) / -stepX;
                float n31 = (xarr->_data[y+stepY][z+stepZ]-xarr->_data[y-stepY][z+stepZ]) / -stepY;
                float n32 = (xarr->_data[y][z+2*stepZ]- xarr->_data[y][z]) / -stepZ;
                vertex v3 = {x      ,y      ,z+stepZ,  n30, n31, n32};

                float n40 = (x1arr->_data[y+stepY][z]-x0arr->_data[y+stepY][z]) / -stepX;
                float n41 = (xarr->_data[y+2*stepY][z]-xarr->_data[y][z]) / -stepY;
                float n42 = (xarr->_data[y+stepY][z+stepZ]-xarr->_data[y+stepY][z-stepZ]) / -stepZ;
                vertex v4 = {x      ,y+stepY,z , n40, n41, n42};


                float n50 = (x2arr->_data[y+stepY][z]-x1arr->_data[y+stepY][z]) / -stepX;
                float n51 = (x1arr->_data[y+2*stepY][z]-x1arr->_data[y][z]) / -stepY;
                float n52 = (x1arr->_data[y+stepY][z+stepZ]-x1arr->_data[y+stepY][z-stepZ]) / -stepZ;
                vertex v5 = {x+stepX,y+stepY,z,   n50, n51, n52 };

                float n60 = (x2arr->_data[y+stepY][z+stepZ]-xarr->_data[y+stepY][z+stepZ]) / -stepX;
                float n61 = (x1arr->_data[y+2*stepY][z+stepZ]-x1arr->_data[y][z+stepZ]) / -stepY;
                float n62 = (x1arr->_data[y+stepY][z+2*stepZ]-x1arr->_data[y+stepY][z]) / -stepZ;
                vertex v6 = {x+stepX,y+stepY,z+stepZ, n60, n61, n62 };


                float n70 = (x1arr->_data[y+stepY][z+stepZ]-x0arr->_data[y+stepY][z+stepZ])/ -stepX;
                float n71 = (xarr->_data[y+2*stepY][z+stepZ]-xarr->_data[y][z+stepZ]) / -stepY;
                float n72 = (xarr->_data[y+stepY][z+2*stepZ]-xarr->_data[y+stepY][z]) / -stepZ;
                vertex v7 = {x      ,y+stepY,z+stepZ, n70, n71, n72 };

                c.p[0] = v0;
                c.p[1] = v1;
                c.p[2] = v2;
                c.p[3] = v3;
                c.p[4] = v4;
                c.p[5] = v5;
                c.p[6] = v6;
                c.p[7] = v7;

                float vl0 = xarr  ->_data[y      ][z      ];
                float vl1 = x1arr ->_data[y      ][z      ];
                float vl2 = x1arr ->_data[y      ][z+stepZ];
                float vl3 = xarr  ->_data[y      ][z+stepZ];
                float vl4 = xarr  ->_data[y+stepY][z      ];
                float vl5 = x1arr ->_data[y+stepY][z      ];
                float vl6 = x1arr ->_data[y+stepY][z+stepZ];
                float vl7 = xarr  ->_data[y+stepY][z+stepZ];

                c.val[0] = vl0 ;
                c.val[1] = vl1 ;
                c.val[2] = vl2 ;
                c.val[3] = vl3 ;
                c.val[4] = vl4 ;
                c.val[5] = vl5 ;
                c.val[6] = vl6 ;
                c.val[7] = vl7 ;

                process_cubef(c, isovalue, res, minx, miny, minz, scalex, scaley, scalez);

            } // z
        } // y

        /// fix it later
        std::cout << res->size() << " ";
       if(res->size()> 500000)
           return;


        delete x0arr;
        x0arr = xarr;
        xarr  = x1arr;
        x1arr = x2arr;
        //x2arr = voxels.get_slice_at_x(x+2*stepX+1);



    } // x


    delete x0arr;
    delete xarr;
    delete x1arr;
    delete x2arr;


}

void run_marching_cubes(CArray3D<uchar> & voxels, uchar & isovalue, std::vector<C3DPoint> * res,
                        float minx, float miny, float minz, float scalex, float scaley, float scalez,
           int stepX, int stepY, int stepZ)
{
    // Run the processCube function on every cube in the grid
    int sizeX = voxels._zs;
    int sizeY = voxels._ys;
    int sizeZ = voxels._xs;
    /// std::vector<vertex> vertexList;

    for(int x = stepX; x < sizeX - 2*stepX; x += stepX)
    {
        std::cout << x << std::endl;
        for(int y = stepY; y < sizeY - 2*stepY; y += stepY)
        {
            for(int z = stepZ; z < sizeZ - 2*stepZ; z += stepZ) // check one more time ranges
            {

                cube c;

                float n00 = 0.0f;
                float n01 = 0.0f;
                float n02 = 0.0f;
                float n10 = 0.0f;
                float n11 = 0.0f;
                float n12 = 0.0f;
                float n20 = 0.0f;
                float n21 = 0.0f;
                float n22 = 0.0f;
                float n30 = 0.0f;
                float n31 = 0.0f;
                float n32 = 0.0f;
                float n40 = 0.0f;
                float n41 = 0.0f;
                float n42 = 0.0f;
                float n50 = 0.0f;
                float n51 = 0.0f;
                float n52 = 0.0f;
                float n60 = 0.0f;
                float n61 = 0.0f;
                float n62 = 0.0f;
                float n70 = 0.0f;
                float n71 = 0.0f;
                float n72 = 0.0f;

                n00 =  (float)(voxels._data[x+stepX][y][z]-voxels._data[x-stepX][y][z]) / -stepX;
                n01 = (float)(voxels._data[x][y+stepY][z]-voxels._data[x][y-stepY][z]) / -stepY;
                n02 = (float)(voxels._data[x][y][z+stepZ]-voxels._data[x][y][z-stepZ]) / -stepZ;
                vertex v0 = {x      ,y      ,z   , n00, n01, n02};

                n10 =(float)(voxels._data[x+2*stepX][y][z]-voxels._data[x][y][z]) / -stepX;
                n11 =(float)(voxels._data[x+stepX][y+stepY][z]-voxels._data[x+stepX][y-stepY][z]) / -stepY;
                n12 =(float)(voxels._data[x+stepX][y][z+stepZ]-voxels._data[x+stepX][y][z-stepZ]) / -stepZ;
                vertex v1 = {x+stepX,y      ,z    ,   n10, n11, n12};

                n20 =  (float)(voxels._data[x+2*stepX][y][z+stepZ]-voxels._data[x][y][z+stepZ]) / -stepX;
                n21 =  (float)(voxels._data[x+stepX][y+stepY][z+stepZ]-voxels._data[x+stepX][y-stepY][z+stepZ]) /-stepY;
                n22 =  (float)(voxels._data[x+stepX][y][z+2*stepZ]-voxels._data[x+stepX][y][z]) / -stepZ;
                vertex v2 = {x+stepX, y      ,z+stepZ, n20, n21, n22};


                n30 = (float)(voxels._data[x+stepX][y][z+stepZ]-voxels._data[x-stepX][y][z+stepZ]) / -stepX;
                n31 = (float)(voxels._data[x][y+stepY][z+stepZ]-voxels._data[x][y-stepY][z+stepZ]) / -stepY;
                n32 = (float)(voxels._data[x][y][z+2*stepZ]-voxels._data[x][y][z]) / -stepZ;
                vertex v3 = {x      ,y      ,z+stepZ,  n30, n31, n32};

                n40 = (float)(voxels._data[x+stepX][y+stepY][z]-voxels._data[x-stepX][y+stepY][z]) / -stepX;
                n41 = (float)(voxels._data[x][y+2*stepY][z]-voxels._data[x][y][z]) / -stepY;
                n42 = (float)(voxels._data[x][y+stepY][z+stepZ]-voxels._data[x][y+stepY][z-stepZ]) / -stepZ;
                vertex v4 = {x      ,y+stepY,z , n40, n41, n42};


                n50 = (float)(voxels._data[x+2*stepX][y+stepY][z]-voxels._data[x+stepX][y+stepY][z]) / -stepX;
                n51 = (float)(voxels._data[x+stepX][y+2*stepY][z]-voxels._data[x+stepX][y][z]) / -stepY;
                n52 = (float)(voxels._data[x+stepX][y+stepY][z+stepZ]-voxels._data[x+stepX][y+stepY][z-stepZ]) / -stepZ;
                vertex v5 = {x+stepX,y+stepY,z,   n50, n51, n52 };

                n60 = (float)(voxels._data[x+2*stepX][y+stepY][z+stepZ]-voxels._data[x][y+stepY][z+stepZ]) / -stepX;
                n61 = (float)(voxels._data[x+stepX][y+2*stepY][z+stepZ]-voxels._data[x+stepX][y][z+stepZ]) / -stepY;
                n62 = (float)(voxels._data[x+stepX][y+stepY][z+2*stepZ]-voxels._data[x+stepX][y+stepY][z]) / -stepZ;
                vertex v6 = {x+stepX,y+stepY,z+stepZ, n60, n61, n62 };

                /*float n70 =  ((voxels._data[x-stepX][y+stepY][z+stepZ] != isovalue)?0.f:1.f) -
                             ((voxels._data[x+stepX][y+stepY][z+stepZ] != isovalue)?0.f:1.f);

                float n71 =  ((voxels._data[x][y][z+stepZ] != isovalue)?0.f:1.f) -
                             ((voxels._data[x][y+2*stepY][z+stepZ] != isovalue)?0.f:1.f);

                float n72 =  ((voxels._data[x][y+stepY][z] != isovalue)?0.f:1.f) -
                             ((voxels._data[x][y+stepY][z+2*stepZ] != isovalue)?0.f:1.f);*/

                n70 = (float)(voxels._data[x+stepX][y+stepY][z+stepZ]-voxels._data[x-stepX][y+stepY][z+stepZ]) / -stepX;
                n71 = (float)(voxels._data[x][y+2*stepY][z+stepZ]-voxels._data[x][y][z+stepZ]) / -stepY;
                n72 = (float)(voxels._data[x][y+stepY][z+2*stepZ]-voxels._data[x][y+stepY][z]) / -stepZ;

                vertex v7 = {x      ,y+stepY,z+stepZ, n70, n71, n72 };

                c.p[0] = v0;
                c.p[1] = v1;
                c.p[2] = v2;
                c.p[3] = v3;
                c.p[4] = v4;
                c.p[5] = v5;
                c.p[6] = v6;
                c.p[7] = v7;

                uchar vl0 = voxels._data[x      ][y      ][z      ];
                uchar vl1 = voxels._data[x+stepX][y      ][z      ];
                uchar vl2 = voxels._data[x+stepX][y      ][z+stepZ];
                uchar vl3 = voxels._data[x      ][y      ][z+stepZ];
                uchar vl4 = voxels._data[x      ][y+stepY][z      ];
                uchar vl5 = voxels._data[x+stepX][y+stepY][z      ];
                uchar vl6 = voxels._data[x+stepX][y+stepY][z+stepZ];
                uchar vl7 = voxels._data[x      ][y+stepY][z+stepZ];

                c.val[0] = vl0 ;
                c.val[1] = vl1 ;
                c.val[2] = vl2 ;
                c.val[3] = vl3 ;
                c.val[4] = vl4 ;
                c.val[5] = vl5 ;
                c.val[6] = vl6 ;
                c.val[7] = vl7 ;

                process_cube(c, isovalue, res, minx, miny, minz, scalex, scaley, scalez);

            } // z
        } // y

        /// fix it later
        std::cout << res->size() << " ";
        if(res->size()> 100000)
            return;
    } // x


    ///return vertexList;
}


