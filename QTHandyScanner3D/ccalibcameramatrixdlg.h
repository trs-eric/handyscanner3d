#ifndef CCALIBCAMERAMATRIXDLG_H
#define CCALIBCAMERAMATRIXDLG_H

#include <QDialog>

#include <QThread>

#include "opencv_aux_utils.h"
#include "./driver/driver_aux_util.h"

namespace Ui {
class CCalibCameraMatrixDlg;
}

class CCalibCameraMatrixDlg : public QDialog
{
    Q_OBJECT
public slots:
    void capture_and_proc_new_frame();
    void start_calib();

public:
    explicit CCalibCameraMatrixDlg(cv::VideoCapture *, PVOID dh,QWidget *parent = 0);
    ~CCalibCameraMatrixDlg();

signals:
       void stop_main_preview();
       void start_main_preview();
       void update_main_preview(cv::Mat & frame);

protected:
    void hideEvent(QHideEvent * event);
    void showEvent(QShowEvent * event);
    void single_timer_shoot();
    void drawProjectedArea(cv::Mat & frame);

private:
    Ui::CCalibCameraMatrixDlg *ui;

   void init();

    cv::VideoCapture * _current_cam;
    PVOID _dev_handle;

    int _mode;
    bool _blink;
    clock_t _prev_time;
    int _delay;
    int _max_frame_check;
    float _max_reproj_error;
    std::vector<std::vector<cv::Point2f> > _all_img_pnts;

    // Number of items by width and height
    cv::Size _boardSize;
    cv::Size _frame_pix_size;
    float _cell_size;
    int _captured_frames;
    int _max_frame_count;
    void callibrate_and_save();

    void init_frame_series_50();
    void init_frame_series_100();
    void draw_frame_helper(cv::Mat & frame);

    std::vector<cv::Point2f> _frame_corners;

};

class CDotsExtration : public QThread
{
      Q_OBJECT
public:
      void start_thread(cv::Mat , cv::Size );

      std::vector<cv::Point2f> _points;
      bool _found;
      cv::Mat _frame;
      cv::Size _boardSize;

protected:
      void run();
};


#endif // CCALIBCAMERAMATRIX_H
