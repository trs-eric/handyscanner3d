#include "crawtexture.h"

void CRawTexture::load(std::ifstream &ifile)
{
    int tex_title_len = 0;
    ifile.read(reinterpret_cast<char *>(&tex_title_len), sizeof(int));

    // title buf
    char * tex_title_buf = new char [tex_title_len];
    ifile.read(tex_title_buf, tex_title_len);
    std::string nm = std::string(tex_title_buf,tex_title_len);
    _name = QString(nm.c_str());

    delete [] tex_title_buf;

    /// image
    int twidth = 0;
    ifile.read(reinterpret_cast<char *>(&twidth), sizeof(int));

    int theight = 0;
    ifile.read(reinterpret_cast<char *>(&theight), sizeof(int));

    int format = 0;
    ifile.read(reinterpret_cast<char *>(&format), sizeof(int));

    int t_bcount = 0;
    ifile.read(reinterpret_cast<char *>(&t_bcount), sizeof(int));

    unsigned char * idata = new char [t_bcount];
    ifile.read(idata, t_bcount);

    QImage::Format iformat = format;
    if(_img != NULL) delete _img;
    _img = new QImage(idata, twidth, theight, iformat);
}

void CRawTexture::save(std::ofstream &ofile)
{

    int tex_len = _name.length();
    char * tex_title_buf = _name.toStdString().c_str();
    ofile.write(reinterpret_cast<const char *>(&tex_len), sizeof(int));
    ofile.write(tex_title_buf, tex_len);


    /// image
    int twidth = _img->width();
    ofile.write(reinterpret_cast<const char *>(&twidth), sizeof(int));

    int theight = _img->height();
    ofile.write(reinterpret_cast<const char *>(&theight), sizeof(int));

    int format = _img->format();
    ofile.write(reinterpret_cast<const char *>(&format), sizeof(int));

    int t_bcount = _img->byteCount();
    ofile.write(reinterpret_cast<const char *>(&t_bcount), sizeof(int));

    uchar * data = _img->bits();
    ofile.write(data, t_bcount);
}

