#include "render_radiance_scaling/radianceScalingRenderer.h"

#include "cglscene.h"
#include <QMessageBox>
#include <QFileInfo>
#include "cglsampletrivtable.h"
#include "cfreeglcamera.h"
#include "messages.h"
#include "cframe.h"
#include "ccameratrack.h"
#include "csceneexplorermanager.h"
#include "cglwidget.h"

#include "settings/psettings.h"





CGLCamera * gReferencePoint;

QString str_gRUBident = QString("3DSCAN")+g_app_version+QString::number(SYSBITS);
int len_gRUBident = str_gRUBident.length();

//const char gRUBident[6] = "RUB2.5";
extern int global_compound_object_index;
CGLScene::CGLScene(QWidget * treew, MenuActions * va, CGLWidget * glw) : _treew(treew), _va(va), _glw(glw)
{

    _table = NULL;
    _ctrack = NULL;
    _camera = NULL;
    _currentCObject = NULL;

    _cobj_left = NULL;
    _cobj_right = NULL;

    _se_manager = NULL;
    _disable_raw_points_render = false;
    _disable_decim_surface_render = true;
    _disable_fused_surface_render = true;

    _fancy_render = NULL;

    _do_selection = false;
    _sel_poly = true;

    CreateScene();


}

CGLScene::~CGLScene()
{
    DeleteScene();
}

std::vector<CGLCompoundObject*> CGLScene::get_list_of_valid_meshes()
{
    std::vector<CGLCompoundObject*>  res;
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
    {
        CTriangularMesh * vv = (*it)->get_mesh();
        if(vv != NULL)
               res.push_back(*it);
    }
    return res;

}

void CGLScene::CreateScene()
{
    DeleteScene();

    CGlobalHSSettings::gl_table_rot_center_x = 0;
    CGlobalHSSettings::gl_table_rot_center_y = 0;
    CGlobalHSSettings::gl_table_rot_center_z = 0;

    _table  = new CGLSampleTrivTable;


    _ctrack = new CCameraTrack(_va);

    _camera = new CFreeGLCamera;
    gReferencePoint = _camera;

    _se_manager = new CSceneExplorerManager(_treew, this, _va);

     _fancy_render = new RadianceScalingRendererPlugin;
     _fancy_render->Init(_glw);

}

void CGLScene::DeleteScene()
{
    if(_table)
    {
        delete _table;
        _table = NULL;
    }

    if(_ctrack)
    {
        delete _ctrack;
        _ctrack = NULL;
    }

    if(_camera)
    {
        delete _camera;
        _camera = NULL;
    }

    if(_se_manager)
    {
        delete _se_manager;
        _se_manager = NULL;
    }

    if(_fancy_render)
    {
        delete _fancy_render;
        _fancy_render = NULL;
    }
    DeleteObjects();

    global_keyframes_sequence.clear();

    clean_g_polar_map();
}

void CGLScene::DeleteObjects()
{
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
        delete *it;
    _cglobjects.clear();

    if(_se_manager != NULL)
          _se_manager->clean_all();

}


void CGLScene::GLRenderBackground()
{
    //==========================================
        //Gradient background
        glMatrixMode (GL_MODELVIEW);
        glPushMatrix ();
        glLoadIdentity ();
        glMatrixMode (GL_PROJECTION);
        glPushMatrix ();
        glLoadIdentity ();

        glDisable(GL_LIGHTING);
        glDisable(GL_DEPTH_TEST);
        //glEnable(GL_COLOR_MATERIAL);


        float triangleAmount = 20;
        //GLfloat radius = 0.8f; //radius
        GLfloat twicePi = 2.0f * cPI;
        glBegin(GL_TRIANGLE_FAN);
            glColor3f(CGlobalHSSettings::gcolor_cr, CGlobalHSSettings::gcolor_cg, CGlobalHSSettings::gcolor_cb);
            glVertex3f(0, 0, -1.); // center of circle
            for(int i = 0; i <= triangleAmount;i++)
            {
                    glColor3f(CGlobalHSSettings::gcolor_sr, CGlobalHSSettings::gcolor_sg, CGlobalHSSettings::gcolor_sb);
                    glVertex3f(
                            ( std::cos(i *  twicePi / triangleAmount)),
                            ( std::sin(i * twicePi / triangleAmount)),
                                -1.0 );
            }
        glEnd();

        glEnable(GL_LIGHTING);
        glEnable(GL_DEPTH_TEST);
        //glDisable(GL_COLOR_MATERIAL);

        glPopMatrix ();

        glColor3f(0.f, 0.f, 0.f);

        glMatrixMode (GL_MODELVIEW);
        glPopMatrix ();
        //================================

}
void CGLScene::GLSetupLight()
{
    glLightfv(GL_LIGHT0, GL_AMBIENT,  CGlobalHSSettings::gl_light_Ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  CGlobalHSSettings::gl_light_Diffuse);
    glLightfv(GL_LIGHT0, GL_POSITION, CGlobalHSSettings::gl_light_Position);

}

void drawSampleScene()
{
    // make it with triangles
    // make it with gl lists
    glEnable(GL_COLOR_MATERIAL);

    glPushMatrix();
        glTranslatef(0,0, -10);
        glRotatef(45, 0,1, 0);
        glRotatef(30, 1,0, 0);

        glScalef(2, 2, 2);
        // glCallList(_oglid);


        // Белая сторона - ЗАДНЯЯ
        glBegin(GL_POLYGON);
        glColor3f(   .2,  .4, 0.8 );
        glNormal3f(   0.0,  0.0, 1.0 );
        glVertex3f(  0.5, -0.5, 0.5 );
        glVertex3f(  0.5,  0.5, 0.5 );
        glVertex3f( -0.5,  0.5, 0.5 );
        glVertex3f( -0.5, -0.5, 0.5 );
        glEnd();

        // Фиолетовая сторона - ПРАВАЯ
        glBegin(GL_QUADS);
        glColor3f(  1.0,  0.0,  1.0 );
        glNormal3f( 1.0,  0.0, 0.0 );
        glVertex3f( 0.5, -0.5, -0.5 );
        glVertex3f( 0.5,  0.5, -0.5 );
        glVertex3f( 0.5,  0.5,  0.5 );
        glVertex3f( 0.5, -0.5,  0.5 );
        glEnd();

        // Зеленая сторона - ЛЕВАЯ
        glBegin(GL_QUADS);
        glColor3f(   0.0,  1.0,  0.0 );
        glNormal3f( -1.0,  0.0, 0.0 );
        glVertex3f( -0.5, -0.5,  0.5 );
        glVertex3f( -0.5,  0.5,  0.5 );
        glVertex3f( -0.5,  0.5, -0.5 );
        glVertex3f( -0.5, -0.5, -0.5 );
        glEnd();

        // Синяя сторона - ВЕРХНЯЯ
        glBegin(GL_QUADS);
        glColor3f(   0.0,  0.0,  1.0 );
        glNormal3f(   0.0,  1.0, 0.0 );
        glVertex3f(  0.5,  0.5,  0.5 );
        glVertex3f(  0.5,  0.5, -0.5 );
        glVertex3f( -0.5,  0.5, -0.5 );
        glVertex3f( -0.5,  0.5,  0.5 );
        glEnd();

        // Красная сторона - НИЖНЯЯ
        glBegin(GL_QUADS);
        glColor3f(   1.0,  0.0,  0.0 );
        glNormal3f(   0.0, -10.0, 0.0 );
        glVertex3f(  0.5, -0.5, -0.5 );
        glVertex3f(  0.5, -0.5,  0.5 );
        glVertex3f( -0.5, -0.5,  0.5 );
        glVertex3f( -0.5, -0.5, -0.5 );
        glEnd();

   glPopMatrix();

   glDisable(GL_COLOR_MATERIAL);
      glColor3f(0.f, 0.f, 0.f);
}

void CGLScene::process_selected_faces( )
{

    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
    {
        if((*it)->is_unique_object() && !_disable_fused_surface_render)
        {
            if((*it)->get_vmode() == CGLCompoundObject::MESH)
            {
                 (*it)->process_selected_faces();
            }

        }
    }
}

void CGLScene::update_selection_points( CPointCloud::SelectMode sm)
{

    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
    {
        if(!(*it)->is_unique_object() && !_disable_raw_points_render)
        {
            if((*it)->get_vmode() == CGLCompoundObject::RAW_POINTS)
            {
                 (*it)->update_slection_raw_points(_selection_frame[0], _selection_frame[1],
                _selection_frame[2], _selection_frame[3], _selection_poly, sm);
            }
        }

        if((*it)->is_unique_object() && !_disable_fused_surface_render)
        {
            if((*it)->get_vmode() == CGLCompoundObject::MESH)
            {
                 (*it)->update_slection_mesh_faces(_selection_poly, _camera );
            }

        }
    }
}

bool CGLScene::is_there_selected_raw_points()
{
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
    {
        if(!(*it)->is_unique_object())
            if((*it)->get_vmode() == CGLCompoundObject::RAW_POINTS)
            {
                if((*it)->is_there_selected_raw_points())
                    return true;
            }
    }

    return false;
}

void CGLScene::set_selection_frame(int x, int y, int w, int h)
{
    _selection_frame[0] = x;
    _selection_frame[1] = y;
    _selection_frame[2] = w;
    _selection_frame[3] = h;
}

void CGLScene::set_empty_selection()
{
    _selection_frame[0] = 0;
    _selection_frame[1] = 0;
    _selection_frame[2] = 0;
    _selection_frame[3] = 0;

    _selection_poly.clear();
}

void  CGLScene::set_selection_mode(bool v)
{

    _do_selection = v;
    set_selection_frame(0, 0, 0, 0);

    if(!_do_selection)
    {
        std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
        for(; it != _cglobjects.end(); it++)
        {
            (*it)->clear_slected_features();
        }
    }
}

void  CGLScene::render_selection_frame()
{
    glPushAttrib(GL_CURRENT_BIT);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    GLint m_viewport[4];
    glGetIntegerv( GL_VIEWPORT, m_viewport );

    gluOrtho2D(m_viewport[0], m_viewport[2], m_viewport[1],  m_viewport[3]);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);


    float _x = _selection_frame[0];
    float _y = _selection_frame[1];
    float _w = _selection_frame[2];
    float _h = _selection_frame[3];


    if(! _sel_poly) // rectangular frame
    {
        glColor4f(float(0.8), float(.0), float(0), float(1.0));
        glBegin(GL_LINE_LOOP);
            glVertex2f(_x , _y );
            glVertex2f(_x , _y + _h );
            glVertex2f(_x + _w , _y + _h );
            glVertex2f(_x + _w, _y);
        glEnd();

        glEnable(GL_BLEND);
        glColor4f(float(1.0), float(.0), float(0), float(.2));

        glBegin(GL_QUADS);
            glVertex2f(_x, _y);
            glVertex2f(_x, _y + _h);
            glVertex2f(_x + _w, _y + _h);
            glVertex2f(_x + _w, _y);
        glEnd();
    }
    else // polygonal selection
    {
         glColor4f(float(0.8), float(.0), float(0), float(1.0));

        if(_selection_poly.size() == 1)
        {
            glBegin(GL_QUADS);
            glVertex2f(  _selection_poly[0].x - 1,  _selection_poly[0].y - 1);
            glVertex2f(  _selection_poly[0].x + 1,  _selection_poly[0].y - 1);
            glVertex2f( _selection_poly[0].x + 1,  _selection_poly[0].y + 1);
            glVertex2f( _selection_poly[0].x - 1,  _selection_poly[0].y + 1);
            glEnd();
        }

        int sz = _selection_poly.size();
        float avx = 0;
        float avy = 0;
        glBegin(GL_LINE_LOOP);
        for(int i = 0; i < sz; i ++)
        {
            glVertex2f(_selection_poly[i].x, _selection_poly[i].y);
            avx += _selection_poly[i].x;
            avy += _selection_poly[i].y;
        }
        glEnd();

        avx /= sz;
        avy /= sz;

        /*if(sz >= 3)
        {
            glEnable(GL_BLEND);
            glColor4f(float(1.0), float(.0), float(0), float(.2));
            glBegin( GL_TRIANGLE_FAN );
            glVertex2f(avx, avy);
            for(int i = 0; i < sz; i ++)
                glVertex2f(_selection_poly[i].x, _selection_poly[i].y);
            glVertex2f(_selection_poly[0].x, _selection_poly[0].y);

            glEnd();
        }*/
    }


    glDisable(GL_BLEND);
    glColor4f(1.f, 1.f, 1.f,1.0f);


    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);

    glPopAttrib();
}
void CGLScene::GLRenderScene()
{

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(CGlobalHSSettings::gcolor_sr, CGlobalHSSettings::gcolor_sg, CGlobalHSSettings::gcolor_sb, 1.0f);

    GLRenderBackground();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    GLSetupLight();

    if(_camera)
        _camera->set_view();

    bool isTexMode = false;
    if(_cglobjects.size() > 0)
    {
        CTriangularMesh * m = get_fused_mesh();
        if(m != NULL)
        {
            if (m->get_rendering_mode() == CTriangularMesh::MESH_TEX)
                isTexMode = true;
            else
                isTexMode = false;
        }
    }


    if(_fancy_render->turned_on() && !_disable_fused_surface_render && (_cglobjects.size() > 0) &&
           !isTexMode)
    {
        _fancy_render->starting_rendering();

       std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
       for(; it != _cglobjects.end(); it++)
       {
           if((*it)->is_unique_object() && !_disable_fused_surface_render)
                    (*it)->glDraw();
       }

       std::vector<CGLObject*>::iterator git = _gl_objects.begin();
       for(; git != _gl_objects.end(); git ++)
           (*git )->glDraw();

     _fancy_render->ending_rendering();
    }
    else
    {
        std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
        for(; it != _cglobjects.end(); it++)
        {
            if(((*it)->get_vmode() == CGLCompoundObject::RAW_POINTS) &&
                    !_disable_raw_points_render)
                            (*it)->glDraw();
            else if (((*it)->get_vmode() == CGLCompoundObject::MESH) && (!(*it)->is_unique_object()) &&
                    !_disable_decim_surface_render)
                            (*it)->glDraw();
            else if((*it)->is_unique_object() && !_disable_fused_surface_render)
                     (*it)->glDraw();
        }

        std::vector<CGLObject*>::iterator git = _gl_objects.begin();
        for(; git != _gl_objects.end(); git ++)
            (*git )->glDraw();
    }

    if(_table)
          _table->glDraw();

    if(_ctrack)
        _ctrack->glDraw();


    if(_do_selection)
        render_selection_frame();
}

bool CGLScene::load_from_rub_file(QString &fname)
{

    std::ifstream ifile;
    ifile.open(fname.toLocal8Bit().constData(), std::ios::in | std::ios::binary);
    if(!ifile.is_open()) return false;

    char * fformat = new char[len_gRUBident];
    ///ifile.read(reinterpret_cast<char *>(&fformat), sizeof(gRUBident));

    ifile.read(fformat, len_gRUBident);

    QString a =  QString::fromLocal8Bit(fformat,len_gRUBident);

    std::cout << a.toStdString() << std::endl;
    /// std::cout << fformat<< " " << len_gRUBident <<  std::endl;
    /// std::cout << len_gRUBident << std::endl;


    if(QString::fromLocal8Bit(fformat,len_gRUBident) != str_gRUBident)
    {
        delete [] fformat;
        return false;
    }
    delete [] fformat;

    ifile.read(reinterpret_cast<char *>(&_disable_raw_points_render), sizeof(bool));
    ifile.read(reinterpret_cast<char *>(&_disable_decim_surface_render), sizeof(bool));
    ifile.read(reinterpret_cast<char *>(&_disable_fused_surface_render), sizeof(bool));

    load_scene_gl_settings_from_rub_file(ifile);



    int obj_num = 0;
    ifile.read(reinterpret_cast<char *>(&obj_num), sizeof(int));


    global_compound_object_index = 0;
    DeleteObjects();
    _currentCObject = NULL;


    for(int i =0; i < obj_num; i++)
    {
        CGLCompoundObject * cobj = new CGLCompoundObject(_treew, _va, false);
        cobj->load(ifile);


        cobj->update_treew_children();
         cobj->update_menu_interface();
        if(cobj->is_current())
            _currentCObject = cobj;
        _cglobjects.push_back(cobj);
    }


    int frames_num = 0;
    ifile.read(reinterpret_cast<char *>(&frames_num), sizeof(int));


    for(int f =0; f < frames_num ; f++)
    {
        std::cout << "loading frame " << f << std::endl;
        CFrame * fr = new CFrame;
        fr->load(ifile);
        global_keyframes_sequence.push_back(fr);
    }

    double cmx = 0;
    ifile.read(reinterpret_cast< char *>(&cmx), sizeof(double));
    CGlobalHSSettings::_camL_K.at<double>(0,0) = cmx;

    ifile.read(reinterpret_cast< char *>(&cmx), sizeof(double));
    CGlobalHSSettings::_camL_K.at<double>(0,1) = cmx;

    ifile.read(reinterpret_cast< char *>(&cmx), sizeof(double));
    CGlobalHSSettings::_camL_K.at<double>(0,2) = cmx;

    ifile.read(reinterpret_cast< char *>(&cmx), sizeof(double));
    CGlobalHSSettings::_camL_K.at<double>(1,0) = cmx;

    ifile.read(reinterpret_cast< char *>(&cmx), sizeof(double));
    CGlobalHSSettings::_camL_K.at<double>(1,1) = cmx;

    ifile.read(reinterpret_cast< char *>(&cmx), sizeof(double));
     CGlobalHSSettings::_camL_K.at<double>(1,2) = cmx;

    ifile.read(reinterpret_cast< char *>(&cmx), sizeof(double));
    CGlobalHSSettings::_camL_K.at<double>(2,0) = cmx;

    ifile.read(reinterpret_cast< char *>(&cmx), sizeof(double));
    CGlobalHSSettings::_camL_K.at<double>(2,1) = cmx;

    ifile.read(reinterpret_cast< char *>(&cmx), sizeof(double));
    CGlobalHSSettings::_camL_K.at<double>(2,2) = cmx;

    double  fx = 0;
    ifile.read(reinterpret_cast< char *>(&fx), sizeof(double));
    double  cx = 0;
    ifile.read(reinterpret_cast< char *>(&cx), sizeof(double));
    double  fy = 0;
    ifile.read(reinterpret_cast< char *>(&fy), sizeof(double));
    double  cy = 0;
    ifile.read(reinterpret_cast< char *>(&cy), sizeof(double));
    g_cam_intrinsics = (cv::Mat_<double>(3,3) <<fx, 0., cx,
                                0.,  fy, cy,
                                0.,  0., 1. );
    double  d0 = 0;
    ifile.read(reinterpret_cast< char *>(&d0), sizeof(double));
    double  d1 = 0;
    ifile.read(reinterpret_cast< char *>(&d1), sizeof(double));
    double  d2 = 0;
    ifile.read(reinterpret_cast< char *>(&d2), sizeof(double));
    double  d3 = 0;
    ifile.read(reinterpret_cast< char *>(&d3), sizeof(double));
    double  d4 = 0;
    ifile.read(reinterpret_cast< char *>(&d4), sizeof(double));
    g_cam_distortion =  (cv::Mat_<double>(1,5) << d0, d1, d2, d3, d4);


    bool is_map = false;
    ifile.read(reinterpret_cast< char *>(&is_map), sizeof(bool));
    if(is_map)
    {
        int pcx = 0;
        ifile.read(reinterpret_cast< char *>(&pcx), sizeof(int));

        int pcy = 0;
        ifile.read(reinterpret_cast< char *>(&pcy), sizeof(int));

        g_polar_map = new CArray2D< std::list<aux_pix_data *> > (pcx, pcy);

        for (int y = 0; y < pcy; y++)
        {
            for (int x = 0; x < pcx; x++)
            {
                int list_sz = 0;
                ifile.read(reinterpret_cast< char *>(&list_sz), sizeof(int));

                for(int i = 0; i < list_sz; i++)
                {
                    aux_pix_data * curr_px = new aux_pix_data;
                    curr_px->load(ifile);
                    g_polar_map->_data[y][x].push_back(curr_px);

                }
            }
        }


    }

    ifile.close();



    return true;
}

void CGLScene::finilize_loading_from_rub()
{
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
    {
        _se_manager->add_new_cobj(*it);
    }
    _se_manager->update_scene_explorer();
    _se_manager->set_rend_mode_tex();

    this->UpdateTable();
}

void CGLScene::load_scene_gl_settings_from_rub_file(std::ifstream & ifile)
{
    std::cout << "load_scene_gl_settings_from_rub_file(std::ifstream & ifile)" << std::endl;

    int lpr_len = 0;
    ifile.read(reinterpret_cast<char *>(&lpr_len), sizeof(int));
    char * lpr_buf = new char [lpr_len];
    ifile.read(lpr_buf, lpr_len);
    CGlobalHSSettings::_left_img_prefix = QString::fromLocal8Bit(lpr_buf, lpr_len);
    delete [] lpr_buf;


    int rpr_len = 0;
    ifile.read(reinterpret_cast<char *>(&rpr_len), sizeof(int));
    char * rpr_buf = new char [rpr_len];
    ifile.read(rpr_buf, rpr_len);
    CGlobalHSSettings::_right_img_prefix = QString::fromLocal8Bit(rpr_buf, rpr_len);
    delete [] rpr_buf;


    int rm_len = 0;
    ifile.read(reinterpret_cast<char *>(&rm_len), sizeof(int));
    char * rm_buf = new char [rm_len];
    ifile.read(rm_buf, rm_len);
    CGlobalHSSettings::_rot_angles_file= QString::fromLocal8Bit(rm_buf, rm_len);
    delete [] rm_buf;


    int cp_len = 0;
    ifile.read(reinterpret_cast<char *>(&cp_len), sizeof(int));
    char * cp_buf = new char [cp_len];
    ifile.read(cp_buf, cp_len);
    CGlobalHSSettings::_current_data_path = QString::fromLocal8Bit(cp_buf, cp_len);
    delete [] cp_buf;

    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::glob_max_frames ), sizeof(int));
    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::global_ignore_disparity_for_matching), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::global_ignore_disparity_for_icp), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::global_ignore_disparity_offset), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::global_spcl_decim_step), sizeof(int));

    // load background side color
    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::gcolor_sr), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::gcolor_sg), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::gcolor_sb), sizeof(float));

    // load background centercolor
    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::gcolor_cr), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::gcolor_cg), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::gcolor_cb), sizeof(float));

    // load mesh color
    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::gcolor_mr), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::gcolor_mg), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::gcolor_mb), sizeof(float));

    // save light ambient, diffuse, position
    float r, g, b;
    ifile.read(reinterpret_cast< char *>(&r), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&g), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&b), sizeof(float));
    CGlobalHSSettings::gl_light_Ambient[0] = r;
    CGlobalHSSettings::gl_light_Ambient[1] = g;
    CGlobalHSSettings::gl_light_Ambient[2] = b;
    CGlobalHSSettings::gl_light_Ambient[3] = 1.f;

    ifile.read(reinterpret_cast< char *>(&r), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&g), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&b), sizeof(float));
    CGlobalHSSettings::gl_light_Diffuse[0] = r;
    CGlobalHSSettings::gl_light_Diffuse[1] = g;
    CGlobalHSSettings::gl_light_Diffuse[2] = b;
    CGlobalHSSettings::gl_light_Diffuse[3] = 1.f;

    ifile.read(reinterpret_cast< char *>(&r), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&g), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&b), sizeof(float));
    CGlobalHSSettings::gl_light_Position[0] = r;
    CGlobalHSSettings::gl_light_Position[1] = g;
    CGlobalHSSettings::gl_light_Position[2] = b;
    CGlobalHSSettings::gl_light_Position[3] = 1.f;


    // load material specularity and shiness
    float nv = 0;
    ifile.read(reinterpret_cast< char *>(&nv), sizeof(float));
    CGlobalHSSettings::gl_mat_Specularity[0] = nv;
    CGlobalHSSettings::gl_mat_Specularity[1] = nv;
    CGlobalHSSettings::gl_mat_Specularity[2] = nv;
    CGlobalHSSettings::gl_mat_Specularity[3] = 1.f;

    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::gl_mat_Shiness), sizeof(float));

    // load camera opacity
    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::gl_cam_alpha), sizeof(float));

    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_use ), sizeof(bool));
    CGlobalFRSettings::_use = false;
    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_enhancement), sizeof(float));

    float trans = 0;
    ifile.read(reinterpret_cast< char *>(&trans), sizeof(float));
    CGlobalFRSettings::_transition = trans;

    int  tRed  = 0;
    int  tGre  = 0;
    int  tBlu  = 0;
    ifile.read(reinterpret_cast< char *>(&tRed), sizeof(int));
    ifile.read(reinterpret_cast< char *>(&tGre), sizeof(int));
    ifile.read(reinterpret_cast< char *>(&tBlu), sizeof(int));
    CGlobalFRSettings::_rc_bck_colorL = QColor(tRed, tGre, tBlu);
    //std::cout << "col " << tRed<< " " << tGre << std::endl;
    //std::cout << "col2 " << CGlobalFRSettings::_rc_bck_colorL.red() << " " << CGlobalFRSettings::_rc_bck_colorL .green() << std::endl;


    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_shadowL[0]), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_shadowL[1]), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_shadowL[2]), sizeof(float));

    tRed  = 0;
    tGre  = 0;
    tBlu  = 0;
    ifile.read(reinterpret_cast< char *>(&tRed), sizeof(int));
    ifile.read(reinterpret_cast< char *>(&tGre), sizeof(int));
    ifile.read(reinterpret_cast< char *>(&tBlu), sizeof(int));
    CGlobalFRSettings::_rc_bcklight_colorL =  QColor(tRed, tGre, tBlu);

    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_backlightL[0]), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_backlightL[1]), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_backlightL[2]), sizeof(float));

    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_toplightL[0]), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_toplightL[1]), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_toplightL[2]), sizeof(float));

    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_speclightL[0]), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_speclightL[1]), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_speclightL[2]), sizeof(float));

    tRed  = 0;
    tGre  = 0;
    tBlu  = 0;
    ifile.read(reinterpret_cast< char *>(&tRed), sizeof(int));
    ifile.read(reinterpret_cast< char *>(&tGre), sizeof(int));
    ifile.read(reinterpret_cast< char *>(&tBlu), sizeof(int));
    CGlobalFRSettings::_rc_bck_colorR =  QColor(tRed, tGre, tBlu);

    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_shadowR[0]), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_shadowR[1]), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_shadowR[2]), sizeof(float));

    tRed  = 0;
    tGre  = 0;
    tBlu  = 0;
    ifile.read(reinterpret_cast< char *>(&tRed), sizeof(int));
    ifile.read(reinterpret_cast< char *>(&tGre), sizeof(int));
    ifile.read(reinterpret_cast< char *>(&tBlu), sizeof(int));
    CGlobalFRSettings::_rc_bcklight_colorR =  QColor(tRed, tGre, tBlu);

    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_backlightR[0]), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_backlightR[1]), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_backlightR[2]), sizeof(float));

    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_toplightR[0]), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_toplightR[1]), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_toplightR[2]), sizeof(float));

    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_speclightR[0]), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_speclightR[1]), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalFRSettings::_rc_speclightR[2]), sizeof(float));

    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::gl_table_rot_center_x), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::gl_table_rot_center_y), sizeof(float));
    ifile.read(reinterpret_cast< char *>(&CGlobalHSSettings::gl_table_rot_center_z), sizeof(float));
}

void CGLScene::save_into_rub_file(QString &fname)
{

    std::ofstream ofile;
    ofile.open(fname.toLocal8Bit().constData(), std::ios::out | std::ios::binary);
    if(!ofile.is_open()) return;


    /*char * RUBident = new char [len_gRUBident];
    RUBident = str_gRUBident.toStdString().c_str();
    std::cout << RUBident << " " << len_gRUBident << " " << str_gRUBident.toStdString() << std::endl;

    ofile.write(RUBident, len_gRUBident);
    delete [] RUBident ;*/

    std::string ssRUBident = str_gRUBident.toStdString();
    int ssz = str_gRUBident.length();
    ofile.write((char*)&ssRUBident[0], ssz);



    ofile.write(reinterpret_cast<const char *>(&_disable_raw_points_render), sizeof(bool));
    ofile.write(reinterpret_cast<const char *>(&_disable_decim_surface_render), sizeof(bool));
    ofile.write(reinterpret_cast<const char *>(&_disable_fused_surface_render), sizeof(bool));






    save_scene_gl_settings_into_rub_file(ofile);


    int obj_num = _cglobjects.size();
    ofile.write(reinterpret_cast<const char *>(&obj_num ), sizeof(int));

    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
        (*it)->save(ofile);


    int frames_num = global_keyframes_sequence.size();
    ofile.write(reinterpret_cast<const char *>(&frames_num), sizeof(int));


    std::vector<CFrame *>::iterator itf = global_keyframes_sequence.begin();
    for( ; itf != global_keyframes_sequence.end() ; itf++)
        (*itf)->save(ofile);


    double c00 =  CGlobalHSSettings::_camL_K.at<double>(0,0);
    double c01 =  CGlobalHSSettings::_camL_K.at<double>(0,1);
    double c02 =  CGlobalHSSettings::_camL_K.at<double>(0,2);
    double c10 =  CGlobalHSSettings::_camL_K.at<double>(1,0);
    double c11 =  CGlobalHSSettings::_camL_K.at<double>(1,1);
    double c12 =  CGlobalHSSettings::_camL_K.at<double>(1,2);
    double c20 =  CGlobalHSSettings::_camL_K.at<double>(2,0);
    double c21 =  CGlobalHSSettings::_camL_K.at<double>(2,1);
    double c22 =  CGlobalHSSettings::_camL_K.at<double>(2,2);

    ofile.write(reinterpret_cast<const char *>(&c00), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&c01), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&c02), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&c10), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&c11), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&c12), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&c20), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&c21), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&c22), sizeof(double));

    double  fx = g_cam_intrinsics.at<double>(0,0);
    double  cx = g_cam_intrinsics.at<double>(0,2);
    double  fy = g_cam_intrinsics.at<double>(1,1);
    double  cy = g_cam_intrinsics.at<double>(1,2);

    double d0 = g_cam_distortion.at<double>(0,0);
    double d1 = g_cam_distortion.at<double>(0,1);
    double d2 = g_cam_distortion.at<double>(0,2);
    double d3 = g_cam_distortion.at<double>(0,3);
    double d4 = g_cam_distortion.at<double>(0,4);
    ofile.write(reinterpret_cast<const char *>(&fx), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&cx), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&fy), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&cy), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&d0), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&d1), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&d2), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&d3), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&d4), sizeof(double));



    if(g_polar_map != NULL)
    {
        bool is_map = true;
        ofile.write(reinterpret_cast<const char *>(&is_map), sizeof(bool));


        int psx = g_polar_map->_xs;
        ofile.write(reinterpret_cast<const char *>(&psx), sizeof(int));

        int psy = g_polar_map->_ys;
        ofile.write(reinterpret_cast<const char *>(&psy), sizeof(int));

        for (int y = 0; y < psy; y++)
        {
            for (int x = 0; x < psx; x++)
            {
                int list_sz = g_polar_map->_data[y][x].size();
                ofile.write(reinterpret_cast<const char *>(&list_sz), sizeof(int));

                if( list_sz > 0)
                {
                    std::list<aux_pix_data *>::iterator itL  = g_polar_map->_data[y][x].begin();

                    for ( ; itL  != g_polar_map->_data[y][x].end()  ; itL++ )
                        (*itL)->save(ofile) ;
                }
            }
        }
    }
    else
    {
        bool is_map = false;
        ofile.write(reinterpret_cast<const char *>(&is_map), sizeof(bool));
    }

    ofile.close();
}

void CGLScene::save_scene_gl_settings_into_rub_file(std::ofstream & ofile)
{

    int lpr_len = CGlobalHSSettings::_left_img_prefix.length();
    char * lpr_buf = CGlobalHSSettings::_left_img_prefix.toLocal8Bit().constData();
    ofile.write(reinterpret_cast<const char *>(&lpr_len), sizeof(int));
    ofile.write(lpr_buf, lpr_len);

    int rpr_len = CGlobalHSSettings::_right_img_prefix.length();
    char * rpr_buf = CGlobalHSSettings::_right_img_prefix.toLocal8Bit().constData();
    ofile.write(reinterpret_cast<const char *>(&rpr_len), sizeof(int));
    ofile.write(rpr_buf, rpr_len);

    int rm_len = CGlobalHSSettings::_rot_angles_file.length();
    char * rm_buf = CGlobalHSSettings::_rot_angles_file.toLocal8Bit().constData();
    ofile.write(reinterpret_cast<const char *>(&rm_len), sizeof(int));
    ofile.write(rm_buf, rm_len);


    int cp_len = CGlobalHSSettings::_current_data_path.length();
    char * cp_buf = CGlobalHSSettings::_current_data_path.toLocal8Bit().constData();
    ofile.write(reinterpret_cast<const char *>(&cp_len), sizeof(int));
    ofile.write(cp_buf, cp_len);


    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::glob_max_frames ), sizeof(int));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::global_ignore_disparity_for_matching), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::global_ignore_disparity_for_icp), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::global_ignore_disparity_offset), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::global_spcl_decim_step), sizeof(int));

    // save background side color
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gcolor_sr), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gcolor_sg), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gcolor_sb), sizeof(float));

    // save background centercolor
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gcolor_cr), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gcolor_cg), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gcolor_cb), sizeof(float));

    // save mesh color
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gcolor_mr), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gcolor_mg), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gcolor_mb), sizeof(float));

    // save light ambient, diffuse, position
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gl_light_Ambient[0]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gl_light_Ambient[1]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gl_light_Ambient[2]), sizeof(float));

    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gl_light_Diffuse[0]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gl_light_Diffuse[1]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gl_light_Diffuse[2]), sizeof(float));

    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gl_light_Position[0]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gl_light_Position[1]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gl_light_Position[2]), sizeof(float));


    // save material specularity and shiness
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gl_mat_Specularity[0]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gl_mat_Shiness), sizeof(float));

    // save camera opacity
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gl_cam_alpha), sizeof(float));


    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_use ), sizeof(bool));
    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_enhancement ), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_transition ), sizeof(float));

    int  tRed  = CGlobalFRSettings::_rc_bck_colorL.red();
    int  tGre  = CGlobalFRSettings::_rc_bck_colorL.green();
    int  tBlu  = CGlobalFRSettings::_rc_bck_colorL.blue();
    ofile.write(reinterpret_cast<const char *>(&tRed), sizeof(int));
    ofile.write(reinterpret_cast<const char *>(&tGre), sizeof(int));
    ofile.write(reinterpret_cast<const char *>(&tBlu), sizeof(int));

    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_shadowL[0]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_shadowL[1]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_shadowL[2]), sizeof(float));

    tRed  = CGlobalFRSettings::_rc_bcklight_colorL.red();
    tGre  = CGlobalFRSettings::_rc_bcklight_colorL.green();
    tBlu  = CGlobalFRSettings::_rc_bcklight_colorL.blue();
    ofile.write(reinterpret_cast<const char *>(&tRed), sizeof(int));
    ofile.write(reinterpret_cast<const char *>(&tGre), sizeof(int));
    ofile.write(reinterpret_cast<const char *>(&tBlu), sizeof(int));

    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_backlightL[0]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_backlightL[1]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_backlightL[2]), sizeof(float));

    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_toplightL[0]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_toplightL[1]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_toplightL[2]), sizeof(float));

    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_speclightL[0]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_speclightL[1]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_speclightL[2]), sizeof(float));



    tRed  = CGlobalFRSettings::_rc_bck_colorR.red();
    tGre  = CGlobalFRSettings::_rc_bck_colorR.green();
    tBlu  = CGlobalFRSettings::_rc_bck_colorR.blue();
    ofile.write(reinterpret_cast<const char *>(&tRed), sizeof(int));
    ofile.write(reinterpret_cast<const char *>(&tGre), sizeof(int));
    ofile.write(reinterpret_cast<const char *>(&tBlu), sizeof(int));

    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_shadowR[0]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_shadowR[1]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_shadowR[2]), sizeof(float));

    tRed  = CGlobalFRSettings::_rc_bcklight_colorR.red();
    tGre  = CGlobalFRSettings::_rc_bcklight_colorR.green();
    tBlu  = CGlobalFRSettings::_rc_bcklight_colorR.blue();
    ofile.write(reinterpret_cast<const char *>(&tRed), sizeof(int));
    ofile.write(reinterpret_cast<const char *>(&tGre), sizeof(int));
    ofile.write(reinterpret_cast<const char *>(&tBlu), sizeof(int));

    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_backlightR[0]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_backlightR[1]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_backlightR[2]), sizeof(float));

    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_toplightR[0]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_toplightR[1]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_toplightR[2]), sizeof(float));

    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_speclightR[0]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_speclightR[1]), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalFRSettings::_rc_speclightR[2]), sizeof(float));

    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gl_table_rot_center_x), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gl_table_rot_center_y), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&CGlobalHSSettings::gl_table_rot_center_z), sizeof(float));

}

bool CGLScene::ImportScene(QString &fname)
{

    CGLCompoundObject * cobj = new CGLCompoundObject(_treew, _va, true);
    bool result = false;
    if(fname.endsWith(".xyz", Qt::CaseInsensitive))
        result = cobj->import_raw_points(fname);
    else if (fname.endsWith(".obj", Qt::CaseInsensitive))
        result = cobj->import_mesh(fname);

    if(!result)
    {
        delete cobj;
        return false;
    }

    if(_currentCObject != NULL)
    {
        _currentCObject->select_object(false);
        _currentCObject->update_treew_children();
        _currentCObject->update_menu_interface();

    }
    _currentCObject = cobj;

    _cglobjects.push_back(cobj);
    return true;
}

CGLCompoundObject * CGLScene::ConstructSingleScan(std::vector<C3DPoint> & pnts)
{
    CGLCompoundObject * cobj = new CGLCompoundObject(_treew, _va, true);
    cobj->set_raw_points(pnts);
    if(_currentCObject != NULL)
    {
        _currentCObject->select_object(false);
        _currentCObject->update_treew_children();
        _currentCObject->update_menu_interface();

    }
    _currentCObject = cobj;
    _cglobjects.push_back(cobj);
    return cobj;
}

CGLCompoundObject * CGLScene::ConstructEmptyScan()
{
    CGLCompoundObject * cobj = new CGLCompoundObject(_treew, _va, true);
    if(_currentCObject != NULL)
    {
        _currentCObject->select_object(false);
        _currentCObject->update_treew_children();
        _currentCObject->update_menu_interface();
    }
    _currentCObject = cobj;
    _cglobjects.push_back(cobj);
    return cobj;
}

bool CGLScene::ConstructNextScan(std::vector<C3DPoint> & pnts)
{
    if(_currentCObject == NULL || (_cglobjects.back() != _currentCObject))
        return ConstructSingleScan(pnts);
    _currentCObject->add_raw_points(pnts);
    return true;
}


void CGLScene::ConstructNextMeshCast(int fid, CTriMesh * m)
{
    if(_currentCObject == NULL || (_cglobjects.back() != _currentCObject))
        ConstructEmptyScan();
    ///_currentCObject->add_new_meshcast(fid, m);
}

void CGLScene::ResetCameraPos()
{
    _camera->init();
    _camera->set_view_point(CGlobalHSSettings::gl_table_rot_center_x,
                   CGlobalHSSettings::gl_table_rot_center_y,
                   CGlobalHSSettings::gl_table_rot_center_z);
    _camera->recompute_tpos();
}


void CGLScene::UpdateCameraTrack()
{
    std::cout << "UpdateCameraTrack()" << std::endl;
    _ctrack->remake_glList();
}

void CGLScene::UpdateTable()
{
    _table->set_pos( CGlobalHSSettings::gl_table_rot_center_x,
                CGlobalHSSettings::gl_table_rot_center_y,
                CGlobalHSSettings::gl_table_rot_center_z);
}

void CGLScene::PrepaireNewScan()
{
    _cobj_right = NULL;
    _cobj_left  = NULL;
    _currentCObject = NULL;
}

void CGLScene::ConstructNextScanRight(std::vector<C3DPoint> & pnts)
{
   if(_cobj_right == NULL )
    {
       _cobj_right = ConstructSingleScan(pnts);
       _cobj_right ->extend_title("_right");
       _cobj_right ->update_treew_children();
       _cobj_right ->update_menu_interface();
       return;
    }
    _cobj_right->add_raw_points(pnts);
}

void CGLScene::ConstructNextScanLeft(std::vector<C3DPoint> & pnts)
{
    if(_cobj_left == NULL  )
     {
        _cobj_left = ConstructSingleScan(pnts);
        _cobj_left->extend_title("_left");
        _cobj_left->update_treew_children();
        _cobj_right ->update_menu_interface();
        return;
     }
    _cobj_left->add_raw_points(pnts);
}

void CGLScene::ConstructNextTextureLeft(CRawTexture * tex)
{
    if(_cobj_left != NULL)
        _cobj_left->add_raw_texture(tex);
}

void CGLScene::ConstructNextTextureRight(CRawTexture * tex)
{
    if(_cobj_right != NULL)
        _cobj_right->add_raw_texture(tex);
}

void CGLScene::ExportScene(QString &fname)
{    
    CGLCompoundObject * unique_obj = NULL;
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
    {
        if((*it)->is_unique_object())
        {
            unique_obj = *it;
            break;
        }
    }

    if(fname.endsWith(".cloud.xyz.txt", Qt::CaseInsensitive))
    {
        if(_cglobjects.size() == 0)
        {
            QMessageBox::warning(NULL, QObject::tr("Warning"), wrn_CannotExportRawXYZ);
            return;
        }
        int i = 0;
        for(std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin() ;
                    it != _cglobjects.end(); it++)
        {

            if((*it)!= unique_obj)
            {
                QString efname =  fname.mid(0,fname.length()-12) + QString::number(i) + ".cloud.xyz.txt";
                 std::cout << "EXPORT " << i << " " << efname.toStdString() << std::endl;
                (*it)->export_raw_points(efname);
                 i++;
            }
        }
        return;
    }

    /////////////////////////////////////////////////////
    ///// hereafter we export fused mesh
    /////////////////////////////////////////////////////

    if(unique_obj == NULL)
    {
        QMessageBox::warning(NULL, QObject::tr("Warning"), wrn_CannotExportFuse);
        return;
    }

    if(fname.endsWith(".fuse.xyz.txt", Qt::CaseInsensitive))
    {
        unique_obj->export_mesh_xyz(fname);
        return;
    }

    if(fname.endsWith(".obj", Qt::CaseInsensitive))
    {
        unique_obj->export_mesh(fname);
        return;
    }

 /*   ///   else if(fname.endsWith(".objs", Qt::CaseInsensitive))
    {

        QFileInfo info_fname(fname);
        QString working_dir = info_fname.absolutePath();
        QString base_name   = info_fname.baseName();

        std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
        for(; it != _cglobjects.end(); it++)
        {

            QString scan_name = (*it)->get_title();
            ///scan_name.simplified();
            scan_name.replace(" ", "_");

            QString scan_obj_name =  working_dir + "//" + base_name + "_" + scan_name + ".obj";

            ///std::cout << scan_name.toStdString() << std::endl;
            ///std::cout << scan_obj_name.toStdString() << std::endl;
            (*it)->export_mesh(scan_obj_name);
        }

    }*/
}


void CGLScene::change_current_cobject(CGLCompoundObject * nit)
{
    if(_currentCObject != NULL)
    {
        _currentCObject->select_object(false);
        _currentCObject->update_treew_children();
        _currentCObject->update_menu_interface();
    }
    _currentCObject = nit;
    _currentCObject->select_object(true);
    _currentCObject->update_treew_children();
    _currentCObject->update_menu_interface();


}

void CGLScene::update_selection_state(QTreeWidgetItem * item, int col)
{
    if(_se_manager->is_root_raw_pclouds(item) )
    {
        _disable_raw_points_render    = !_disable_raw_points_render;
        _disable_decim_surface_render = true;
        _disable_fused_surface_render = true;
    }

    if(_se_manager->is_root_raw_decim_surfaces(item) )
    {
        _disable_raw_points_render    = true;
        _disable_decim_surface_render = !_disable_decim_surface_render ;
        _disable_fused_surface_render = true;
        if(_disable_decim_surface_render )
            _se_manager->set_rend_mode_smoo();
    }


    if(_se_manager->is_root_fused_surfaces(item) )
    {
        _disable_raw_points_render    = true;
        _disable_decim_surface_render = true;
        _disable_fused_surface_render = !_disable_fused_surface_render;
    }


   //// std::cout << "update_selection_state" << std::endl;
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(int c = 0; it != _cglobjects.end(); it++, c++)
    {
        CGLCompoundObject * cobj = *it;
        if(cobj->_it_raw_pcloud == item)
        {
           /* if(cobj->get_vmode() == CGLCompoundObject::RAW_POINTS)
                cobj->set_view_mode(CGLCompoundObject::NONE);
            else
                cobj->set_view_mode(CGLCompoundObject::RAW_POINTS);*/

            ///int bb = _se_manager->get_view_pcls(c);
            ///std::cout << "before " <<  bb << std::cout;

            _se_manager->update_view_pcls(c);
            //int aa =_se_manager->get_view_pcls(c);
            ////std::cout << "after " <<  aa<< std::cout;

        }

        if(cobj->_it_raw_decim_surface == item)
        {
           /* if(cobj->get_vmode() == CGLCompoundObject::MESH)
                cobj->set_view_mode(CGLCompoundObject::NONE);
            else
                cobj->set_view_mode(CGLCompoundObject::MESH);*/


            _se_manager->update_view_meshs(c);
        }
    }

    it = _cglobjects.begin();
    for(int c = 0; it != _cglobjects.end(); it++, c++)
    {
        CGLCompoundObject * cobj = *it;

        if(cobj->is_unique_object()) continue; //nothing to do

        cobj->set_view_mode(CGLCompoundObject::NONE);
        if((_se_manager->get_view_pcls(c)) && (!_disable_raw_points_render))
            cobj->set_view_mode(CGLCompoundObject::RAW_POINTS);

        if(_se_manager->get_view_meshs(c) && !_disable_decim_surface_render)
        {
            cobj->set_view_mode(CGLCompoundObject::MESH);
        }

    }
    _se_manager->update_scene_explorer();



    /////////////////////// old section
    {std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
    {
        if((*it)->is_update_root_selection_state(item, col) &&
               _currentCObject != (*it)  )
        {          
            change_current_cobject(*it);
        }
        else
        {
            if((*it)->is_update_child_selection_state(item, col) &&
                 _currentCObject != (*it)   )
                change_current_cobject(*it);

            (*it)->update_selection_state(item, col);
        }
    }}
}

void CGLScene::on_single_click_treew_switch(QTreeWidgetItem * item, int col)
{

    /////////////////////// old section
    {if(is_romove_action(item, col))
    {
        QMessageBox::StandardButton reply =
                QMessageBox::warning(_treew, QObject::tr("Warning"),wrn_RemoveScan, QMessageBox::Yes|QMessageBox::No);
         if (reply == QMessageBox::No)
                      return;
        update_remove_scan_state(item, col);
        return;
    }

    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
    {
        if((*it)->is_update_child_selection_state(item, col) &&
             _currentCObject != (*it))
            change_current_cobject(*it);
    }

    update_surface_state(item,col);
    update_scan_visability_state(item,col);
    }
}

bool CGLScene::is_romove_action(QTreeWidgetItem * item, int col)
{
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
        if((*it)->is_remove_state(item, col))
            return true;

    return false;
}

void CGLScene::update_remove_scan_state(QTreeWidgetItem * item, int col)
{

    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
    {
        if((*it)->is_remove_state(item, col))
        {
            CGLCompoundObject * dl = (*it);
            if( dl == _currentCObject)
            {
                _currentCObject->select_object(false);
                _currentCObject=NULL;
            }
            _cglobjects.erase(it);
            delete dl;

            return;
        }

    }

}
void CGLScene::update_scan_visability_state(QTreeWidgetItem * item, int col)
{
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
        (*it)->update_visability_state(item, col);
}

void CGLScene::update_surface_state(QTreeWidgetItem * item, int col)
{
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
        (*it)->update_surface_state(item, col);
}

void CGLScene::update_referece_dependant_glRepo()
{
    // changing camera position some of the scene features
    // has to be colored differently

    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
        (*it)->update_refdependant_glRepo();
}

CPointCloud * CGLScene::get_raw_poins_of_current_object()
{
    if(_currentCObject == NULL)
            return NULL;
    return _currentCObject->get_raw_points();
}

CPointCloud * CGLScene::get_optim_points_of_current_object()
{
    if(_currentCObject == NULL)
            return NULL;
    return _currentCObject->get_optim_points();

}

CNormalCloud * CGLScene::get_normals_of_current_object()
{
    if(_currentCObject == NULL)
            return NULL;
    return _currentCObject->get_normals();
}

CTriangularMesh * CGLScene::get_mesh_of_current_object()
{
    if(_currentCObject == NULL)
            return NULL;
    return _currentCObject->get_mesh();
}

CTriangularMesh *  CGLScene::get_fused_mesh()
{
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
        if((*it)->is_unique_object())
            return (*it)->get_mesh();

    return NULL;
}


std::vector<C3DPoint> * CGLScene::get_dpcl_of_current_object()
{
    if(_currentCObject == NULL)
            return NULL;
    return _currentCObject->get_dpcl();
}

std::vector<C3DPoint> * CGLScene::get_merged_dpcl_of_scene()
{
    if(_cglobjects.size() == 0) return NULL;
    std::vector<C3DPoint> * res = new std::vector<C3DPoint> ();
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
    {
        CPointCloud * rp = (*it)->get_raw_points();
        if(rp != NULL)
        {
            std::vector<C3DPoint> cl =  rp->get_points_cpy();
            res->insert(res->begin(), cl.begin(), cl.end());
        }
    }
    return res;
}

void CGLScene::set_dpcl_of_current_object(std::vector<C3DPoint> * pnts)
{
    if(_currentCObject == NULL) return;
    _currentCObject->set_dpcl(pnts);

    _currentCObject->set_raw_points(*pnts);

}

void CGLScene::set_raw_poins_of_current_object(CPointCloud * pcld)
{
     if(_currentCObject == NULL) return;
     _currentCObject->set_raw_points(pcld);
}


void CGLScene::set_optim_poins_of_current_object(CPointCloud * pcld)
{
     if(_currentCObject == NULL) return;
     _currentCObject->set_optim_points(pcld);
}

void CGLScene::set_normal_poins_of_current_object(CNormalCloud * ncld)
{
    if(_currentCObject == NULL) return;
    _currentCObject->set_normal_points(ncld);
}

void CGLScene::set_mesh_of_current_object(CTriangularMesh * mesh)
{
    if(_currentCObject == NULL) return;
    _currentCObject->set_mesh(mesh);
}

void CGLScene::update_all_raw_points()
{
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
    {
        (*it)->clean_rawp_gllook();
    }
}

void CGLScene::delete_selected_points()
{
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
        if(!(*it)->is_unique_object())
            (*it)->delete_selected_raw_points();

}


/// actually it updates now fused mesh
void CGLScene::update_mesh_of_current_object()
{
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
        if((*it)->is_unique_object())
        {
               (*it)->clean_gllook();
            break;
        }
    _se_manager->set_rend_mode_tex();
    _se_manager->update_scene_explorer();
}

void CGLScene::delete_unique_cobject()
{
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
        if((*it)->is_unique_object())
            break;

    if(it != _cglobjects.end())
    {
        _cglobjects.erase(it);
        delete *it;
    }
}

void CGLScene::create_unique_cobject(CTriangularMesh *mesh)
{
   /// std::cout << "create_new_cobject" << std::endl;
    CGLCompoundObject * cobj = new CGLCompoundObject(_treew, _va, false, "POISSON");
    cobj->set_unique_object();
    cobj->set_mesh(mesh);
    cobj->set_view_mode(CGLCompoundObject::MESH);

    mesh->remake_glList();

    _disable_raw_points_render    = true;
    _disable_decim_surface_render = true;
    _disable_fused_surface_render = false;

    _cglobjects.push_back(cobj);
    _se_manager->add_new_cobj(cobj);

    /// cobj->update_treew_children();
    /// cobj->update_menu_interface();
    /// UpdateCameraTrack();
}

void CGLScene::create_new_cobject(CTriangularMesh * mesh)
{
    ///std::cout << "create_new_cobject" << std::endl;
    CGLCompoundObject * cobj = new CGLCompoundObject(_treew, _va, false, "POISSON");

    cobj->set_mesh(mesh);
    cobj->set_view_mode(CGLCompoundObject::MESH);

    _cglobjects.push_back(cobj);
    _se_manager->add_new_cobj(cobj);

    cobj->update_treew_children();
    cobj->update_menu_interface();
    UpdateCameraTrack();
/*
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(  ; it != _cglobjects.end() ; it++)
               (*it)->set_view_mode(CGLCompoundObject::NONE);*/
}


void CGLScene::create_new_cglobject(std::vector<C3DPoint> *pcl, CTriangularMesh *mesh, float r, float g, float b)
{

    CGLCompoundObject * cobj = new CGLCompoundObject(_treew, _va, false, "POISSON");



    if(mesh != NULL)
    {
        mesh->_color[0] = r;
        mesh->_color[1] = g;
        mesh->_color[2] = b;

        cobj->set_mesh(mesh);
    }

    cobj->set_raw_points(*pcl);
    CPointCloud * rp = cobj->get_raw_points();
    if((rp != NULL))
    {
        rp->_color[0] = r;
        rp->_color[1] = g;
        rp->_color[2] = b;
    }

    delete pcl;

    _cglobjects.push_back(cobj);

    _se_manager->add_new_cobj(cobj);



    //// revisit interface
    cobj->update_treew_children();
    cobj->update_menu_interface();
    UpdateCameraTrack();

}

void CGLScene::chainge_view_mode_of_current_object(CGLCompoundObject::ViewingMode vm)
{
     if(_currentCObject == NULL) return;
     _currentCObject->set_view_mode(vm);
}

void CGLScene::chainge_mesh_rmode_of_current_object(CTriangularMesh::RenderingMode rm)
{
    if(_currentCObject == NULL) return;
    _currentCObject->set_mesh_render_mode(rm);
}

void CGLScene::set_view_shad_points(bool v)
{
    if(_currentCObject == NULL) return;
    _currentCObject->set_shad_raw_points(v);
}

void CGLScene::set_view_camera_path(bool v)
{
    _ctrack->set_visible(v);
}


void CGLScene::CleanCurrentScan()
{
    if(_cobj_right)
    {
        _cobj_right->delete_raw_points();
        _cobj_right->set_view_mode(CGLCompoundObject::RAW_POINTS);
    }

    if(_cobj_left)
    {
        _cobj_left->delete_raw_points();
        _cobj_left->set_view_mode(CGLCompoundObject::RAW_POINTS);
    }

}

void CGLScene::discard_raw_points_of_current_object()
{
   /// discard_optim_points_of_current_object();
    if(_currentCObject == NULL) return;
    _currentCObject->delete_raw_points();
    _currentCObject->set_view_mode(CGLCompoundObject::RAW_POINTS);
}

void CGLScene::discard_optim_points_of_current_object()
{
    discard_normal_points_of_current_object();
    if(_currentCObject == NULL) return;
    _currentCObject->delete_optim_points();
    _currentCObject->set_view_mode(CGLCompoundObject::RAW_POINTS);
}

void CGLScene::discard_normal_points_of_current_object()
{
    discard_mesh_of_current_object();
    if(_currentCObject == NULL) return;
    _currentCObject->delete_normal_points();
    _currentCObject->set_view_mode(CGLCompoundObject::OPTIM_POINTS);
}

void CGLScene::discard_mesh_of_current_object()
{
    if(_currentCObject == NULL) return;
    _currentCObject->delete_mesh();
    _currentCObject->set_view_mode(CGLCompoundObject::NONE);
}
