#-------------------------------------------------
#
# Project created by QtCreator 2014-03-13T19:14:22
#
#-------------------------------------------------

QT       += core gui opengl multimedia  multimediawidgets serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Rubicon3DScanner
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    cglwidget.cpp \
    cglobject.cpp \
    cglsampletrivtable.cpp \
    cglscene.cpp \
    cimportmanager.cpp \
    cexportmanager.cpp \
    cglcompoundobject.cpp \
    cpointcloud.cpp \
    cdimreference.cpp \
    cglcamera.cpp \
    cfreeglcamera.cpp \
    graphicutils.cpp \
    cnormalcloud.cpp \
    ctriangularmesh.cpp \
    commonutils.cpp \
    ciostate.cpp \
    settings/psettings.cpp \
    crawtexture.cpp \
    cdisparitycompdlg.cpp \
    cdisparitycompthread.cpp \
    disparity/disparity_utils.cpp \
    ccalibstereodlg.cpp \
    cdisparitycompclassicdlg.cpp \
    cdisparitycompclassicthread.cpp \
    sfm/sfm.cpp \
    sfmToyLib/AbstractFeatureMatcher.cpp \
    sfmToyLib/BundleAdjuster.cpp \
    sfmToyLib/Common.cpp \
    sfmToyLib/Distance.cpp \
    sfmToyLib/FeatureMatching.cpp \
    sfmToyLib/FindCameraMatrices.cpp \
    sfmToyLib/MultiCameraDistance.cpp \
    sfmToyLib/MultiCameraPnP.cpp \
    sfmToyLib/OFFeatureMatcher.cpp \
    sfmToyLib/RichFeatureMatcher.cpp \
    sfmToyLib/SfMUpdateListener.cpp \
    sfmToyLib/Triangulation.cpp \
    sfmToyLib/GPUSURFFeatureMatcher.cpp \
    sfmToyLib/SURFFeatureMatcher.cpp \
    ccamerapose.cpp \
    ccomputesparsepcldlg.cpp \
    cspclconstrthread.cpp \
    cframeprepthread.cpp \
    cframe.cpp \
    cinputdatamanager.cpp \
    libelasomp/descriptor.cpp \
    libelasomp/elas.cpp \
    libelasomp/filter.cpp \
    libelasomp/matrix.cpp \
    libelasomp/triangle.cpp \
    mcubeutils.cpp \
    coctocube.cpp \
    cmeshconstructiondlg.cpp \
    cmeshconstructionthread.cpp \
    poisson/Factor.cpp \
    poisson/Geometry.cpp \
    poisson/MarchingCubes.cpp \
    poisson/MultiGridOctest.cpp \
    normals/normal_utils.cpp \
    mechconstructionutils.cpp \
    ctexturingdlg.cpp \
    ctexturingthread.cpp \
    ctrianglesgroup.cpp \
    libcoldet/box.cpp \
    libcoldet/box_bld.cpp \
    libcoldet/cdmath3d.cpp \
    libcoldet/coldet.cpp \
    libcoldet/coldet_bld.cpp \
    libcoldet/multiobject.cpp \
    libcoldet/mytritri.cpp \
    libcoldet/sysdep.cpp \
    libcoldet/tritri.c \
    cmeshdumpthread.cpp \
    cdecismoothdlg.cpp \
    cdecimsmooththread.cpp \
    ccameratrack.cpp \
    cglsettingsdlg.cpp \
    cglmeshcasts.cpp \
    cdpclconstructiondlg.cpp \
    cdpclconstructionthread.cpp \
    clasersanofflinedlg.cpp \
    clasersanofflinethread.cpp \
    img2space/edgedetection.cpp \
    img2space/image2spacemodel.cpp \
    csceneexplorermanager.cpp \
    claserframepreviewdlg.cpp \
    cfusemeshdlg.cpp \
    cfusemeshthread.cpp \
    metadata/qexifimageheader.cpp \
    metadata/qmetadata.cpp \
    render_radiance_scaling/framebufferObject.cpp \
    render_radiance_scaling/gpuProgram.cpp \
    render_radiance_scaling/gpuShader.cpp \
    render_radiance_scaling/textureFormat.cpp \
    render_radiance_scaling/textureParams.cpp \
    render_radiance_scaling/radianceScalingRenderer.cpp \
    cprojectio.cpp \
    iowaitdlg.cpp \
    claserscanonlinedlg.cpp \
    ccapturesequencedlg.cpp \
    opencv_aux_utils.cpp \
    driver/driver_aux_utils.cpp \
    cdevcontrolthread.cpp \
    cusbcamerasettings.cpp \
    cpclcoloringdlg.cpp \
    cselectpclhelpdlg.cpp \
    c3dcorrector.cpp \
    runetag/runetag.cpp \
    ccalibcameramatrixdlg.cpp \
    ccalibrationmap.cpp \
    ccaliblaserangledlg.cpp \
    cglalignmentclouddlg.cpp \
    cmeshartifremovedlg.cpp

HEADERS  += mainwindow.h \
    messages.h \
    cglwidget.h \
    cglobject.h \
    cglsampletrivtable.h \
    graphicutils.h \
    cglscene.h \
    cimportmanager.h \
    cexportmanager.h \
    cglcompoundobject.h \
    cpointcloud.h \
    cdimreference.h \
    cglcamera.h \
    cfreeglcamera.h \
    cnormalcloud.h \
    ctriangularmesh.h \
    commonutils.h \
    ciostate.h \
    settings/psettings.h \
    carray2d.h \
    crawtexture.h \
    cdisparitycompdlg.h \
    cdisparitycompthread.h \
    disparity/disparity_utils.h \
    ccalibstereodlg.h \
    carray1d.h \
    cdisparitycompclassicdlg.h \
    cdisparitycompclassicthread.h \
    sfm/sfm.h \
    sfmToyLib/AbstractFeatureMatcher.h \
    sfmToyLib/BundleAdjuster.h \
    sfmToyLib/Common.h \
    sfmToyLib/Distance.h \
    sfmToyLib/FeatureMatching.h \
    sfmToyLib/FindCameraMatrices.h \
    sfmToyLib/IDistance.h \
    sfmToyLib/IFeatureMatcher.h \
    sfmToyLib/MultiCameraDistance.h \
    sfmToyLib/MultiCameraPnP.h \
    sfmToyLib/OFFeatureMatcher.h \
    sfmToyLib/RichFeatureMatcher.h \
    sfmToyLib/SfMUpdateListener.h \
    sfmToyLib/Triangulation.h \
    sfmToyLib/GPUSURFFeatureMatcher.h \
    sfmToyLib/SURFFeatureMatcher.h \
    ccamerapose.h \
    ccomputesparsepcldlg.h \
    cspclconstrthread.h \
    cframeprepthread.h \
    cframe.h \
    cinputdatamanager.h \
    libelasomp/triangle.h \
    libelasomp/descriptor.h \
    libelasomp/elas.h \
    libelasomp/filter.h \
    libelasomp/image.h \
    libelasomp/matrix.h \
    libelasomp/timer.h \
    carray3d.h \
    mcubeutils.h \
    coctocube.h \
    cmeshconstructiondlg.h \
    cmeshconstructionthread.h \
    poisson/Allocator.h \
    poisson/BinaryNode.h \
    poisson/Factor.h \
    poisson/FunctionData.h \
    poisson/Geometry.h \
    poisson/Hash.h \
    poisson/MarchingCubes.h \
    poisson/MultiGridOctest.h \
    poisson/MultiGridOctreeData.h \
    poisson/Octree.h \
    poisson/PoissonParam.h \
    poisson/Polynomial.h \
    poisson/PPolynomial.h \
    poisson/PPolynomialI.h \
    poisson/SparseMatrix.h \
    poisson/Vector.h \
    normals/normal_utils.h \
    meshconstructionutils.h \
    ctexturingdlg.h \
    ctexturingthread.h \
    ctrianglesgroup.h \
    libcoldet/bitmatrix.h \
    libcoldet/box.h \
    libcoldet/cdmath3d.h \
    libcoldet/coldet.h \
    libcoldet/coldetimpl.h \
    libcoldet/multi_impl.h \
    libcoldet/multiobject.h \
    libcoldet/mytritri.h \
    libcoldet/sweep_prune.h \
    libcoldet/sysdep.h \
    cmeshdumpthread.h \
    cdecismoothdlg.h \
    cdecimsmooththread.h \
    meshlab/mesho.h \
    menu_vew_actions.h \
    ccameratrack.h \
    cglsettingsdlg.h \
    cglmeshcasts.h \
    cdpclconstructiondlg.h \
    cdpclconstructionthread.h \
    clasersanofflinedlg.h \
    clasersanofflinethread.h \
    img2space/edgedetection.h \
    img2space/image2spacemodel.h \
    csceneexplorermanager.h \
    claserframepreviewdlg.h \
    scene_ini_constants.h \
    cfusemeshdlg.h \
    cfusemeshthread.h \
    metadata/qmetadata.h \
    metadata/qexifimageheader.h \
    render_radiance_scaling/framebufferObject.h \
    render_radiance_scaling/gpuProgram.h \
    render_radiance_scaling/gpuShader.h \
    render_radiance_scaling/texture2D.h \
    render_radiance_scaling/textureFormat.h \
    render_radiance_scaling/textureParams.h \
    render_radiance_scaling/radianceScalingRenderer.h \
    cprojectio.h \
    iowaitdlg.h \
    claserscanonlinedlg.h \
    ccapturesequencedlg.h \
    opencv_aux_utils.h \
    driver/WinTypes.h \
    driver/CamAPI.h \
    driver/driver_aux_util.h \
    cdevcontrolthread.h \
    cusbcamerasettings.h \
    cpclcoloringdlg.h \
    cselectpclhelpdlg.h \
    c3dcorrector.h \
    runetag/runetag.h \
    ccalibcameramatrixdlg.h \
    ccalibrationmap.h \
    ccaliblaserangledlg.h \
    cglalignmentclouddlg.h \
    cmeshartifremovedlg.h

FORMS    += mainwindow.ui \
    cdisparitycompdlg.ui \
    ccalibstereodlg.ui \
    cdisparitycompclassicdlg.ui \
    ccomputesparsepcldlg.ui \
    cmeshconstructiondlg.ui \
    ctexturingdlg.ui \
    cdecismoothdlg.ui \
    cglsettingsdlg.ui \
    cdpclconstructiondlg.ui \
    clasersanofflinedlg.ui \
    claserframepreviewdlg.ui \
    cfusemeshdlg.ui \
    iowaitdlg.ui \
    claserscanonlinedlg.ui \
    ccapturesequencedlg.ui \
    cusbcamerasettings.ui \
    cpclcoloringdlg.ui \
    cselectpclhelpdlg.ui \
    ccalibcameramatrixdlg.ui \
    ccaliblaserangledlg.ui \
    cglalignmentclouddlg.ui \
    cmeshartifremovedlg.ui

OTHER_FILES += \
    ico_new.png \
    sfmToyLib/CMakeLists.txt

RESOURCES += \
    res.qrc

QMAKE_CXXFLAGS +=-fpermissive #-D__SFM__DEBUG__# -I'../MechLABsrc/vcglib/' -O6

LIBS += # -L. -lftd2xx

win32:RC_ICONS += ./icons/rubitech.ico

QMAKE_LIBS      += -lgomp -lpthread
QMAKE_CXXFLAGS  +=-O3 -msse3 -fopenmp -I../MeshLab_133src/vcglib

INCLUDEPATH += D:/_Programming/_libraries/opencv2.4.10/opencv_buildr/install/include/
LIBS += -LD:/_Programming/_libraries/opencv2.4.10/opencv_buildr/install/x64/mingw/bin/ \
      libopencv_core2410 \
     libopencv_highgui2410 \
     libopencv_imgproc2410 \
    libopencv_features2d2410 \
    libopencv_calib3d2410 \
    libopencv_nonfree2410\
    libopencv_flann2410\
    libopencv_contrib2410\
    libopencv_video2410\
    libopencv_gpu2410

INCLUDEPATH += D:/_Programming/_libraries/glew-1.12-mingw/include
LIBS += -LD:/_Programming/_libraries/glew-1.12-mingw/lib/ \
        libglew32

LIBS += -L. CamAPI.dll

INCLUDEPATH += D:/_Programming/_libraries/runetag1.0/include
LIBS += -LD:/_Programming/_libraries/runetag1.0/lib/ \
        liblibRUNETag \
        libWinNTL
