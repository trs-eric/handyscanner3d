#include "cframeprepthread.h"

#include <QTime>

#include <vector>
#include <iostream>
#include <ctime>


#include "cinputdatamanager.h"
#include "./settings/psettings.h"


CFramePrepThread::CFramePrepThread(QObject *parent) :
    QThread(parent), _syncro_mutex(NULL)
{
}

void CFramePrepThread::start_process(QMutex * smutex, QWaitCondition * wcond)
{
    _syncro_mutex = smutex;
    _wait_cond_suffice_keyframes = wcond;
    start();
}


void CFramePrepThread::run()
{
    //////////////////// cleaning for second run ////////////////////////
    global_keyframes_for_processing = 0;
    global_no_more_keyframes = false;
    std::vector<CFrame *>::iterator it = global_keyframes_sequence.begin();
    for( ; it != global_keyframes_sequence.end(); it++)
        delete *it;
    global_keyframes_sequence.clear();

    _input_data_manager.init_euler_angles_map();

    //////////////////////////////////////////

   // qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));

    std::cout << "1: CFramePrepThread::run()" << std::endl;

    int thread_number = 4;

    clock_t msstart  = clock();

   // #pragma omp parallel for  num_threads(2)
    #pragma omp parallel for ordered schedule(dynamic,1) num_threads(thread_number)
    for (int idx = 0; idx <= CGlobalHSSettings::glob_max_frames; idx++)
    {        
        // user pressed cancel
        if(idx >= CGlobalHSSettings::glob_max_frames) continue;

        CFrame * cframe =  _input_data_manager.get_frame_with_number(idx);

        // process current frame
        cframe->compute_disparity_map();
        cframe->compute_depth_image();
        cframe->compute_keypoints_with_desc();


        int prev_frame_idx = -1;
        //#pragma omp critical
        #pragma omp ordered
        {
            global_keyframes_sequence.push_back(cframe);
            prev_frame_idx = global_keyframes_sequence.size() - 2;
        }

        if(prev_frame_idx>=0)
            cframe->match_features(global_keyframes_sequence[prev_frame_idx]);

        _syncro_mutex->lock();
        std::cout << "1: adding frame " << cframe->_id  << std::endl;
        global_keyframes_for_processing++;
        /// if(global_keyframes_for_processing != 0)
        _wait_cond_suffice_keyframes->wakeAll();
        _syncro_mutex->unlock();



    }

    // inform other threads that no more frames
    // they should expect
    _syncro_mutex->lock();
    _wait_cond_suffice_keyframes->wakeAll();
    global_no_more_keyframes = true;
    _syncro_mutex->unlock();

    clock_t mffinish = clock();
    int sec_conv = (mffinish - msstart);
    std::cout << " 1st thread result - time clocks: " << sec_conv << " secs: " << ((float)sec_conv )/CLOCKS_PER_SEC << std::endl;
}

