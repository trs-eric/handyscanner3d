#ifndef CCONVEXMESHCONSTRUCTIONTHREAD_H
#define CCONVEXMESHCONSTRUCTIONTHREAD_H

#include <QThread>
#include <QMutex>

class CPointCloud;
class CNormalCloud;
class CTriangularMesh;

class CConvexMeshConstructionThread : public QThread
{
    Q_OBJECT
public:
    explicit CConvexMeshConstructionThread(QObject *parent = 0);
    ~CConvexMeshConstructionThread();

    void start_process(CPointCloud *, float, float, bool, bool);
    bool is_abort(){return _abort;}
    void emit_send_back(int i);

signals:
    void send_back(const int val);
    void send_result(CNormalCloud *);
    void send_result(CTriangularMesh *);


protected:
    void run();
    void fillin_texture(QImage *, QImage *, int , int);

public slots:
    void stop_process();

private:
    bool    _abort;
    QMutex  _mutex;

    CPointCloud * _pcloud;
    float _vert_dist_threas;
    float _horiz_dist_threas;
    bool _closed_shape;
    bool _gen_texture;
};

#endif // CCONVEXMESHCONSTRUCTIONTHREAD_H
