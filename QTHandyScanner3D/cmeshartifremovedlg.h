#ifndef CMESHARTIFREMOVEDLG_H
#define CMESHARTIFREMOVEDLG_H

#include <QDialog>
class CGLWidget;

namespace Ui {
class CMeshArtifRemoveDlg;
}

class CMeshArtifRemoveDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CMeshArtifRemoveDlg(CGLWidget * ,QWidget *parent = 0);
    ~CMeshArtifRemoveDlg();
    void hideEvent(QHideEvent * e);
public slots:
    void process_selected_faces();
    void height_changed(int v);
    void deselected_faces();
    void change_selection_mode();

private:


    Ui::CMeshArtifRemoveDlg *ui;
    CGLWidget *_glWidget;
};

#endif // CMESHARTIFREMOVEDLG_H
