#include "opencv_aux_utils.h"
#include <iostream>
#include <fstream>
#include "cimportmanager.h"
#include "img2space/image2spacemodel.h"

bool corners_sort_y (const cv::Point2f & a, const cv::Point2f & b)
{
    return (a.y < b.y);
}

bool corners_sort_x (const cv::Point2f & a, const cv::Point2f & b)
{
    return (a.x < b.x);
}

void proper_sort_corners(std::vector<cv::Point2f>  & corners)
{
    // vertical sort
    std::sort(corners.begin(), corners.end(), corners_sort_y);

    int cx  = corners.size();
    int idx_st = 0;
    int idx_en = 0;

    float vertical_thread = 25;
    for(int c = 1; c <   cx ; c++)
    {
        if( (std::fabs( corners[c].y - corners[idx_st].y) >  vertical_thread ) || (c == cx - 1))
        {


            idx_en = c;
           // std::cout << "SORTING " << idx_st << " " << idx_en << std::endl;
            std::sort(corners.begin() + idx_st, corners.begin() + idx_en, corners_sort_x);

            idx_st = c;
            idx_en = c;
        }
    }
}


cv::SimpleBlobDetector::Params contruct_blob_detector_pset()
{
    cv::SimpleBlobDetector::Params params;
    // Change thresholds
    params.minThreshold = 10;
    params.maxThreshold =200;
    //params.minDistBetweenBlobs =.001;
    // Filter by Area.
    params.filterByArea = true;
    params.minArea = 100;
    // Filter by Circularity
    params.filterByCircularity = true;
    params.minCircularity = 0.7;
    // Filter by Convexity
    params.filterByConvexity = true;
    params.minConvexity = 0.87;
    // Filter by Inertia
    params.filterByInertia = true;
    params.minInertiaRatio = 0.01;

    return params;
}

std::vector<cv::Point2f> findGridMarkers(QString & imgname)
{

    cv::Mat mBckg = imread( imgname.toStdString().c_str(), cv::IMREAD_GRAYSCALE );

    if(mBckg.cols == 0) return;

     cv::SimpleBlobDetector::Params params = contruct_blob_detector_pset();

    cv::SimpleBlobDetector detector(params);
    std::vector<cv::KeyPoint> keypoints;
    detector.detect( mBckg, keypoints);

    std::vector<cv::Point2f> corners;
    std::vector<cv::KeyPoint>::iterator it = keypoints.begin();
    for( ; it != keypoints.end(); it++ )
       corners.push_back(it->pt);

    //filter_out_wrong_corners(corner);

    proper_sort_corners(corners);

    return corners;
}


float compute_board_distance(std::vector<cv::Point2f>  & corners, float cellw,
                                int xs,  int ys, float fovx, float fovy)
{

    if(corners.size() < 10) return 0;

    std::vector <float> pixsz;
    for(int p = 0; p < (corners.size()-1) ; p++ )
    {
        if(corners[p].x < corners[p+1].x)
        {
            float pix_width =  corners[p+1].x - corners[p].x;
            pixsz.push_back(pix_width);
        }
    }
    std::sort(pixsz.begin(), pixsz.end());

    int vsz = pixsz.size();
    double cutoff_min = double(vsz) * 0.2;
    double cutoff_max = double(vsz) * 0.8;

    float av = 0;
    int  av_cnt = 0;
    for(int i = cutoff_min; i <  cutoff_max; i++ )
    {
        av += pixsz[i];
        av_cnt ++;
    }

    float metrix_x = cellw /(av/av_cnt);


    std::vector <float> dists;
    for(int p = 0; p < corners.size() ; p++ )
    {
        float ux = corners[p].x  - float(xs-1)/2.f ;

        float tan_alpha_x = (2.0f * ux/ float(xs-1) ) * (float)tan(fovx / 2.0f);

        float real_x = metrix_x * ux;

        float dist = real_x / tan_alpha_x;
        dists.push_back(fabs(dist));
    }

    int dsz = dists.size();
    double cutoff_mind = double(dsz) * 0.2;
    double cutoff_maxd = double(dsz) * 0.8;

    float dav = 0;
    int  dav_cnt =0;
    for(int i = cutoff_mind; i <  cutoff_maxd; i++ )
    {
        dav += dists[i];
        dav_cnt ++;
    }
    return dav/float(dav_cnt);
}



float point_plain_abs_distance(cv::Point3f &p, float & rA, float & rB, float & rC, float &rD)
{
    float nom = std::fabs(rA * p.x + rB * p.y + rC *p.z + rD);
    float det = std::sqrt(rA*rA + rB*rB + rC *rC);

    return nom/det;
}

void compute_plain_SVD(cv::Mat A, float & rA, float & rB, float & rC, float &rD)
{
    cv::Mat B = (cv::Mat_<double>(4,1) << 1,2,3,4);
    cv::Mat X = (cv::Mat_<double>(4,1) << 0,0,0,0);


   // cv::solve(A, B, X, cv::DECOMP_SVD);
    cv::SVD::solveZ(A,X);

    rA = X.at<double>(0,0);
    rB = X.at<double>(1,0);
    rC = X.at<double>(2,0);
    rD = X.at<double>(3,0);

}

cv::Mat convert_points_to_mat_for_plain_alg(std::vector<cv::Point3f>  & points,
                                         std::vector<bool>  & mask)
{
    int points_sz = 0;
    std::vector<bool>::iterator itm = mask.begin();
    for( ; itm != mask.end(); itm++ )
        if(*itm) points_sz ++;

    cv::Mat A(points_sz,4, CV_64F);
    for(int p = 0, tp = 0; p < points.size(); p++)
    {
        if(mask[p])
        {
            A.at<double>(tp,0) = points[p].x;
            A.at<double>(tp,1) = points[p].y;
            A.at<double>(tp,2) = points[p].z;
            A.at<double>(tp,3) = 1;

            tp++;
        }
    }

    return A;
}


std::vector<bool>  mark_three_random_pnts(int range)
{
     std::vector<bool> res(range, false); /// 3 true, other falss

    int rem = 3;
    while( rem > 0 )
    {
        int idx = rand() % range;
        if( !res[idx] )
        {

            res[idx] = true;
            rem --;


        }
    }

    return res;
}

void compute_plain_SVD_RANSAC(std::vector<cv::Point3f>  & points,
                              float & A, float & B, float & C,
                              float &D,
                              int max_terations,
                              float tolerance)
{


    float threshold = tolerance; // mm

    int best_inliers  = 0;

    float best_A = 0;
    float best_B = 0;
    float best_C = 0;
    float best_D = 0;

    for(int it = 0; it < max_terations; it++)
    {

        std::vector<bool> mask_three = mark_three_random_pnts(points.size());

        cv::Mat A_three_pnts = convert_points_to_mat_for_plain_alg(points, mask_three);

        float A0 = 0;
        float B0 = 0;
        float C0 = 0;
        float D0 = 0;
        compute_plain_SVD(A_three_pnts, A0, B0, C0, D0);

        int inlers_count = 0;
        std::vector<bool> mask_inliers;
        std::vector<cv::Point3f>::iterator itp = points.begin();
        for( ; itp != points.end(); itp++ )
        {

            float pdist = point_plain_abs_distance(*itp, A0, B0, C0, D0);
            if(pdist < threshold)
            {
                mask_inliers.push_back(true);
                inlers_count++;
            }
            else
                mask_inliers.push_back(false);

        }
        std::cout << "iter " << it << " " << A0 << " " << B0 << " " << C0 << " " << D0 << std::endl;
        std::cout << "iter " << it << " inliers " <<inlers_count << std::endl;

        cv::Mat A_inliers = convert_points_to_mat_for_plain_alg(points, mask_inliers);

        float AI = 0;
        float BI = 0;
        float CI = 0;
        float DI = 0;
        compute_plain_SVD(A_inliers, AI, BI, CI, DI);


        int inlers_count_imp = 0;
        itp = points.begin();
        for( ; itp != points.end(); itp++ )
        {
            float pdist = point_plain_abs_distance(*itp, AI, BI, CI, DI);
            if(pdist < threshold)
                inlers_count_imp++;

        }

        if(best_inliers < inlers_count_imp)
        {
            std::cout << "iter (imp) " << it << " " << AI << " " << BI << " " << CI << " " << DI << std::endl;
            std::cout << "iter (imp) " << it << " inliers_imp " <<inlers_count_imp << " " << float(inlers_count_imp *100)/float(points.size()) << " %" << std::endl;

           best_inliers = inlers_count_imp;
           best_A = AI;
           best_B = BI;
           best_C = CI;
           best_D = DI;
        }

    }

    A = best_A;
    B = best_B;
    C = best_C;
    D = best_D;

}



bool parse_description_video_file(QString & name, std::map<int, CFrameDesc> & frame_map)
{

    std::ifstream desc_file (name.toLocal8Bit().constData());
    if (!desc_file.is_open())
        return false;

    frame_map.clear();

    std::string line;
    while ( getline (desc_file,line) )
    {
        std::vector<std::string> strs =  split(line, ' ');

        int frame  = atoi(strs[0].c_str());

        CFrameDesc::CFrameType type;
        if(strs[1] == "f") type = CFrameDesc::FR;
        if(strs[1] == "t") type = CFrameDesc::TEX;
        if(strs[1] == "b") type = CFrameDesc::BKG;

        std::string jsn;
        std::size_t found_in = line.find_first_of("{");
        std::size_t found_out = line.find_first_of("}");
        if(found_in != std::string::npos &&  found_out != std::string::npos)
             jsn = line.substr(found_in, found_out);
        std::map<std::string, float> pp = parse_pairs(jsn);

        float angle = 0;
        std::map<std::string, float>::iterator it = pp.find(std::string("angle_"));
        if(it != pp.end()) angle = it->second;

        float lpos = 0;
        it = pp.find(std::string("lpos"));
        if(it != pp.end()) lpos  = it->second;

        frame_map[frame] = CFrameDesc(type, angle, lpos);

    }
    desc_file.close();

    return true;
}


