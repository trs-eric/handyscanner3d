#include "claplaciansmooththread.h"
#include "ctriangularmesh.h"
// examples are CClusteringDecimThread
// compute_normals_meshlab (meshconstructionutils.h)
#include "../MeshLab_133src/meshlab/src/plugins_experimental/edit_ocme/src/ocme/vcg_mesh.h"
#include "../MeshLab_133src/vcglib/vcg/complex/algorithms/clean.h"
#include "../MeshLab_133src/vcglib/vcg/complex/algorithms/smooth.h"
using namespace vcg;


CLaplacianSmoothThread * myLapThread;

CLaplacianSmoothThread::CLaplacianSmoothThread(QObject *parent) :
    QThread(parent), _inmesh(NULL), _degree(0), _iteration(1)
{
    _abort = false;
}

CLaplacianSmoothThread::~CLaplacianSmoothThread()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
    wait();

}


void CLaplacianSmoothThread::start_process(CTriangularMesh * mesh, float deg, int iter)
{
    _abort = false;

    _inmesh = mesh;
    _degree = deg;
    _iteration = iter;

    myLapThread = this;
    start();

}


bool lapSmoothMeshlabCB(const int pos, const char * str )
{
    myLapThread->emit_send_back(pos);
}

void CLaplacianSmoothThread::stop_process()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
}


void CLaplacianSmoothThread::emit_send_back(int i)
{
     emit send_back(i);
}

void CLaplacianSmoothThread::run()
{
    if(_inmesh == NULL) return;

    /////////////////////////////////////////////////////
    ///// convert input CTrianleMesh to vcgMesh /////////
    /////////////////////////////////////////////////////

    vcgMesh  cm;
    CTriMesh * tri_mesh = _inmesh->get_tri_mesh();

    // float min_x = 0, min_y = 0, min_z = 0;
    // float max_x = 0, max_y = 0, max_z = 0;
    int n_vert = tri_mesh->_points.size();
    vcgMesh::VertexIterator v_iter =  tri::Allocator<vcgMesh>::AddVertices(cm, n_vert);
    for (unsigned int i = 0; i < n_vert; i++, v_iter++)
    {
        (*v_iter).P()[0] = tri_mesh->_points[i]._x;
        (*v_iter).P()[1] = tri_mesh->_points[i]._y;
        (*v_iter).P()[2] = tri_mesh->_points[i]._z;

        (*v_iter).N()[0] = tri_mesh->_points[i]._nx;
        (*v_iter).N()[1] = tri_mesh->_points[i]._ny;
        (*v_iter).N()[2] = tri_mesh->_points[i]._nz;

        //cm.bbox.Add((*v_iter).P()); // update bound box
        // if(min_x > tri_mesh->_points[i]._x) min_x = tri_mesh->_points[i]._x;
        // if(min_y > tri_mesh->_points[i]._y) min_y = tri_mesh->_points[i]._y;
        // if(min_z > tri_mesh->_points[i]._z) min_z = tri_mesh->_points[i]._z;

        // if(max_x < tri_mesh->_points[i]._x) max_x = tri_mesh->_points[i]._x;
        // if(max_y < tri_mesh->_points[i]._y) max_y = tri_mesh->_points[i]._y;
        // if(max_z < tri_mesh->_points[i]._z) max_z = tri_mesh->_points[i]._z;
    }

    if (_abort)
    {
        emit send_back(0);
        return;
    }

    unsigned int n_face = tri_mesh->_triangles.size();
    tri::Allocator<vcgMesh>::AddFaces(cm, n_face);
    for (unsigned int f=0; f < n_face; f++)
    {
        cm.face[f].V(0) = &(cm.vert[tri_mesh->_triangles[f].pnt_index[0]]);
        cm.face[f].V(1) = &(cm.vert[tri_mesh->_triangles[f].pnt_index[1]]);
        cm.face[f].V(2) = &(cm.vert[tri_mesh->_triangles[f].pnt_index[2]]);
    }

    if (_abort)
    {
        emit send_back(0);
        return;
    }

    std::cout << "conversion TrianleMesh to vcgMesh is finished " << std::endl;

    //////////////////////////////////////////////////////////////////////////////
    //// original pugin code
    ////  int iternum = par.getInt("iterations");
    ////  float dthreshold = par.getFloat("AngleDeg");
    ////  tri::Smooth<CMeshO>::VertexCoordPlanarLaplacian(m.cm, iternum, math::ToRad(dthreshold), selection,cb);
    ////  tri::UpdateNormal<CMeshO>::PerVertexNormalizedPerFace(m.cm);


    tri::Smooth<vcgMesh>::VertexCoordPlanarLaplacian(cm, _iteration, BGM(_degree), false, lapSmoothMeshlabCB);
    tri::UpdateNormal<vcgMesh>::PerVertexNormalizedPerFace(cm); // can we drop it?

    ///////////////////////////////////////////////
    //// convert vcgMesh to CTriangularMesh
    ///////////////////////////////////////////////

    CTriMesh * rmesh = new CTriMesh ;

    //// insert new points
    int n_overt = cm.vert.size();
    std::vector<C3DPoint> pnts;
    for (unsigned int i = 0; i < n_overt; i++)
    {
        C3DPoint xpp;
        xpp._x  = cm.vert[i].P()[0];
        xpp._y  = cm.vert[i].P()[1];
        xpp._z  = cm.vert[i].P()[2];
        pnts.push_back(xpp);
    }

    if (_abort)
    {
        emit send_back(0);
        return;
    }

    //// insert new triangles
    unsigned int n_oface = cm.face.size();
    rmesh->_triangles.resize(n_oface);
    for (unsigned int f = 0; f < n_oface; f++)
    {
        C3DPointIdx tri;
        tri.pnt_index[0] = cm.face[f].V(0)-(&*(cm.vert.begin()));
        tri.pnt_index[1] = cm.face[f].V(1)-(&*(cm.vert.begin()));
        tri.pnt_index[2] = cm.face[f].V(2)-(&*(cm.vert.begin()));
        rmesh->_triangles[f] = tri;
   }

    if (_abort)
    {
        emit send_back(0);
        return;
    }

    std::vector< std::vector <int> *> v(n_overt, NULL);
    ///////// update mesh normals per face
    std::vector<C3DPointIdx>::iterator itt = rmesh->_triangles.begin();
    for(int fidx = 0 ; itt != rmesh->_triangles.end(); itt++, fidx++)
    {
        if(v[itt->pnt_index[0]] == NULL)
        {
              v[ itt->pnt_index[0] ] = new std::vector <int>;
              v[ itt->pnt_index[0] ]-> push_back(fidx);
        }
        else
        {
              v[ itt->pnt_index[0] ]->push_back(fidx);
        }
        C3DPoint pnt0 = pnts[ itt->pnt_index[0] ];

        if(v[itt->pnt_index[1]] == NULL)
        {
              v[ itt->pnt_index[1] ] = new std::vector <int>;
              v[ itt->pnt_index[1] ]-> push_back(fidx);
        }
        else
              v[ itt->pnt_index[1] ]->push_back(fidx);
        C3DPoint pnt1 = pnts[ itt->pnt_index[1] ];

        if(v[itt->pnt_index[2]] == NULL)
        {
              v[ itt->pnt_index[2] ] = new std::vector <int>;
              v[ itt->pnt_index[2] ]-> push_back(fidx);
        }
        else
              v[ itt->pnt_index[2] ]->push_back(fidx);
        C3DPoint pnt2 = pnts[ itt->pnt_index[2] ];
        itt->tri_normal = compute_normal( pnt2, pnt1, pnt0);
    }

    if (_abort)
    {
        emit send_back(0);
        return;
    }


    //////// update mesh normals per verteces
    int pnts_count  = pnts.size();
    rmesh->_points.resize(pnts_count);
    std::vector<C3DPoint>::iterator itv = pnts.begin();
    for(int p = 0; itv != pnts.end(); itv++, p++)
    {
        C3DPoint xpp;
        xpp._x  = itv->_x;
        xpp._y  = itv->_y;
        xpp._z  = itv->_z;


        float fnx =0;
        float fny =0;
        float fnz =0;

        if(v[p] != NULL)
        {
          std::vector<int>::iterator fit = v[p]->begin();
          for( ; fit != v[p]->end(); fit++)
          {

              fnx += rmesh->_triangles[(*fit)].tri_normal._x;
              fny += rmesh->_triangles[(*fit)].tri_normal._y;
              fnz += rmesh->_triangles[(*fit)].tri_normal._z;

              xpp._faceidx.push_back(*fit);
          }
          normalize3(fnx, fny, fnz);
        }

        xpp._nx = fnx;
        xpp._ny = fny;
        xpp._nz = fnz;

        rmesh->_points[p]= xpp;
    }
    std::vector< std::vector <int> *>::iterator cit = v.begin();
    for( ; cit != v.end(); cit++)
        if((*cit) != NULL) delete *cit;


    ////////////// send result back to user //////////////////
    CTriangularMesh * outmesh =  new CTriangularMesh;
    outmesh -> set_mesh(rmesh);
    emit send_result(outmesh);
    emit send_back(100);

}

