#include "cmeshconstructiondlg.h"
#include "ui_cmeshconstructiondlg.h"
#include "cglwidget.h"
#include "cmeshconstructionthread.h"
#include "messages.h"

#include <QMessageBox>
#include<QListWidget>

#include "normals/normal_utils.h"
#include "meshconstructionutils.h"

#include "cframe.h"

CMeshConstructionDlg::CMeshConstructionDlg(CGLWidget * glWidget, QWidget *parent) :
    QDialog(parent), _glWidget(glWidget),
    ui(new Ui::CMeshConstructionDlg)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);

    // default edit values
    ui->progressBar->setMaximum(100);

    connect(ui->previewBtn, SIGNAL(clicked()), this, SLOT(onPreviewBtn()));
    connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));

      _thread = new CMeshConstructionThread;

    // user pressed cancel button
    connect(ui->cancelBtn,  SIGNAL(clicked()), _thread, SLOT(stop_process()));
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

    // user closed dialg
    connect(this,  SIGNAL(finished(int)), _thread, SLOT(stop_process()));

    // working with a thread
    connect(_thread, SIGNAL(send_back(int)), this, SLOT(onMakeProgressStep(int)));
    connect(_thread, SIGNAL(send_result(CTriangularMesh*)), this, SLOT(onCompleteProcessing(CTriangularMesh*)));

    /// connect(_thread, SIGNAL(send_result(CTriangularMesh*)), _glWidget, SLOT(on_new_mesh_computed(CTriangularMesh*)));

    connect(_thread, SIGNAL(send_result(CTriangularMesh*)), _glWidget, SLOT(on_new_fused_mesh_computed(CTriangularMesh*)));

    connect(_thread, SIGNAL(finished()), this, SLOT(onStopProcessing()));

   /// for creating new scan
   /// connect(_thread, SIGNAL(send_result(std::vector<C3DPoint> * )), _glWidget, SLOT(construct_next_scan(std::vector<C3DPoint> *)));

    connect(ui->sldQuality, SIGNAL(valueChanged(int)), this, SLOT(change_quality(int )));
    ui->sldQuality->setValue(CMeshConstructionDlg::MEDIUM);
}

void CMeshConstructionDlg::change_quality(int v)
{
    std::cout << v << std::endl;
    switch(v)
    {
        case LOW:
            ui->lblQual->setText("Low");
            break;
        case MEDIUM:
            ui->lblQual->setText("Medium");
            break;
        case HIGH:
            ui->lblQual->setText("High");
            break;
    }
}

CMeshConstructionDlg::~CMeshConstructionDlg()
{
    delete ui;
    delete _thread;
}

void CMeshConstructionDlg::onMakeProgressStep(int val)
{
    ui->progressBar->setValue(val);
}

void CMeshConstructionDlg::onCompleteProcessing(CTriangularMesh * m)
{
    if(m == NULL)
    {
        QMessageBox::warning(this, tr("Warning"),wrn_FailedMesh);
        return;
    }
    if(_on_apply)
    {
        close_dialog();
        return;
    }

    _is_result_valid = true;
    ui->progressBar->setValue(100);

}
void CMeshConstructionDlg::onStopProcessing()
{
    ui->applyBtn    ->setEnabled(true);
    ui->previewBtn  ->setEnabled(true);

    _in_process = false;
}

void CMeshConstructionDlg::close_dialog()
{
    _on_apply = false;
    close();
}

void CMeshConstructionDlg::onCancelBtn()
{
    if(!_in_process)
    {
        //_glWidget->discard_mesh_of_current_object();
        close_dialog();
    }
    _in_process = false;
    _on_apply = false;
}

void CMeshConstructionDlg::onPreviewBtn()
{
    std::vector<C3DPoint> * dpcl = _glWidget->get_merged_dpcl_of_scene();

    if(dpcl == NULL)
    {
       QMessageBox::warning(this, tr("Warning"),wrn_NoDPCL);
       return;
    }

    ui->applyBtn    ->setEnabled(false);
    ui->previewBtn  ->setEnabled(false);

    _in_process = true;

    float octd   = 0;
    float sdiv   = 0;
    float snode  = 0;
    float soffs  = 0;
    switch(ui->sldQuality->value())
    {
        case LOW:
          octd   = 8;
          sdiv   = 8;
          snode  = 1;
          soffs  = 1;
        break;
        case MEDIUM:
            octd   = 10;
            sdiv   = 10;
            snode  = 1;
            soffs  = 1;
        break;
        case HIGH:
            octd   = 12;
            sdiv   = 12;
            snode  = 1;
            soffs  = 1;
        break;
    }

    _thread->start_process(octd, sdiv, snode, soffs, dpcl);
}

void CMeshConstructionDlg::onApplyBtn()
{
    _on_apply = true;
    if(!_is_result_valid)
        onPreviewBtn();
    else
        close_dialog();
}


void CMeshConstructionDlg::showEvent(QShowEvent * event)
{
    ui->progressBar->setValue(0);
    _is_result_valid    = false;
    _on_apply           = false;
    _in_process         = false;
}

