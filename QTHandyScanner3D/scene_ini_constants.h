#ifndef SCENE_INI_CONSTANTS_H
#define SCENE_INI_CONSTANTS_H

#include <QString>

static QString ini_image_name_format= QString("image_name_format");
static QString ini_image_name_has_scan = QString("image_name_has_scan");
static QString ini_first_scan=QString("first_scan");
static QString ini_last_scan=QString("last_scan");
static QString ini_first_image=QString("first_image");
static QString ini_last_image=QString("last_image");
static QString ini_is_difference=QString("is_difference");
static QString ini_has_marks=QString("has_marks");
static QString ini_is_round_marks=QString("is_round_marks");
static QString ini_bckg_image=QString("bckg_image");
static QString ini_bckg_image2=QString("bckg_image2");
static QString ini_marks_x=QString("marks_x");
static QString ini_marks_y=QString("marks_y");

static QString ini_fov_horiz=QString("fov_horiz");
static QString ini_fov_vert=QString("fov_vert");
static QString ini_scan_angle_step=QString("scan_angle_step");
static QString ini_camera_x_incline=QString("camera_x_incline");
static QString ini_camera_y_incline=QString("camera_y_incline");

static QString ini_rot_center_x=QString("rot_center_x");
static QString ini_rot_center_y=QString("rot_center_y");
static QString ini_rot_center_z=QString("rot_center_z");


static QString ini_max_depth_to_cancel=QString("max_depth_to_cancel_pnts");
static QString ini_min_depth_to_cancel=QString("min_depth_to_cancel_pnts");


static QString ini_compute_laser_pos=QString("compute_laser_pos");
static QString ini_laser_betta_angle_init=QString("laser_betta_angle_init");
static QString ini_laser_betta_angle_step=QString("laser_betta_angle_step");
static QString ini_camera_laser_dist=QString("camera_laser_dist");
static QString ini_tex_name_format=QString("tex_name_format");


static QString ini_texture_img_fmt=QString("texture_img_fmt");
static QString ini_texture_img_cnts=QString("texture_img_cnts");

static QString ini_lookfor_laser_onboard_ymin=QString("lookfor_laser_onboard_ymin");
static QString ini_lookfor_laser_onboard_ymax=QString("lookfor_laser_onboard_ymax");
static QString ini_single_cell_width=QString("single_cell_width");
static QString ini_camera_obj_dist=QString("camera_obj_dist");
static QString ini_computing_threads =QString("computing_threads");



static QString ini_use_metadata_angle=QString("use_metadata_angle");
static QString ini_meta_angle_correction=QString("meta_angle_correction");
static QString ini_camera_laser_interchanged =QString("camera_laser_interchanged");

static QString ini_laser_depth_offset =QString("laser_depth_offset");

static QString ini_video_name_format   = QString("video_name_format");
static QString ini_video_desc_format   = QString("video_desc_format");
static QString ini_video_name_has_scan = QString("video_name_has_scan");

static QString ini_is_callibrated = QString("is_callibrated");
static QString ini_cx = QString("cx");
static QString ini_fx = QString("fx");
static QString ini_cy = QString("cy");
static QString ini_fy = QString("fy");

static QString ini_dist1 = QString("dist1");
static QString ini_dist2 = QString("dist2");
static QString ini_dist3 = QString("dist3");
static QString ini_dist4 = QString("dist4");
static QString ini_dist5 = QString("dist5");

#endif // SCENE_INI_CONSTANTS_H
