#ifndef CPHOTOGRAMMETRYTHREAD_H
#define CPHOTOGRAMMETRYTHREAD_H

#include <QThread>
#include <QMutex>

class CPhotogrammetryThread : public QThread
{
    Q_OBJECT
public:
    explicit CPhotogrammetryThread(QObject *parent = 0);
    ~CPhotogrammetryThread();

    void start_process();
    bool is_abort(){return _abort;}
    void emit_send_back(int i);

signals:
    void send_back(const int val);
    void send_result();

protected:
    void run();

public slots:
    void stop_process();

private:
    bool    _abort;
    QMutex  _mutex;

};

#endif // CPHOTOGRAMMETRYTHREAD_H
