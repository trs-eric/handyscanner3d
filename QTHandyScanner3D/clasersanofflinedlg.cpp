#include "clasersanofflinedlg.h"
#include "ui_clasersanofflinedlg.h"

#include "cglwidget.h"
#include "clasersanofflinethread.h"
#include "cpointcloud.h"

#include  <QFileDialog>
#include  <QFileInfo>
#include <QSettings>
#include <QMessageBox>

#include "settings/psettings.h"

#include "claserframepreviewdlg.h"

#include "scene_ini_constants.h"
#include "messages.h"
#include "img2space/image2spacemodel.h"


CLaserSanOfflineDlg::CLaserSanOfflineDlg(CGLWidget * glWidget, QWidget *parent) :
      _glWidget(glWidget), QDialog(parent), _prevDlg(NULL), _bg_img(NULL), _in_avi(NULL),
    ui(new Ui::CLaserSanOfflineDlg)
{
    Qt::WindowFlags flags = windowFlags();

    Qt::WindowFlags helpFlag =
    Qt::WindowContextHelpButtonHint;

    flags = flags & (~helpFlag);
    setWindowFlags(flags);

    ui->setupUi(this);

    ui->edtDecimPntStep->setText(QString::number(4));
    ui->edtSideLen->setText(QString::number(2));

    QString orig_ini("./real_camshot_4okt/description.ini");
    /// read_ini_file(orig_ini);

    connect(ui->btnBrowseINI, SIGNAL(clicked()), this, SLOT(onLoadINIBtn()));

   connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));

    _thread = new CLaserSanOfflineThread;
    // user pressed cancel button
    connect(ui->cancelBtn,  SIGNAL(clicked()), _thread, SLOT(stop_process()));
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

    // user closed dialg
    connect(this,  SIGNAL(finished(int)), _thread, SLOT(stop_process()));

    // working with a thread
    connect(_thread, SIGNAL(send_back(int)), this, SLOT(onMakeProgressStep(int)));


    connect(_thread, SIGNAL(send_result(std::vector<C3DPoint> * )), _glWidget, SLOT(construct_next_scan(std::vector<C3DPoint> *)));
    connect(_thread, SIGNAL(send_new_cam()), _glWidget, SLOT(on_new_campos()));

    connect(_thread, SIGNAL(send_delete_scan()), _glWidget, SLOT(send_clean_all_scans()));
    connect(_thread, SIGNAL(send_clean_scan()), _glWidget, SLOT(send_clean_scan()));
    connect(_thread, SIGNAL(send_data_for_cglobject(std::vector<C3DPoint> *, CTriangularMesh *, float, float, float)), _glWidget, SLOT(on_new_cglobject_computed(std::vector<C3DPoint> *, CTriangularMesh *, float, float, float)));

    connect(_thread, SIGNAL(valid_result()), this, SLOT(onCompleteProcessing( )));
    connect(_thread, SIGNAL(finished()), this, SLOT(onStopProcessing()));
    connect(_thread, SIGNAL(send_wrong_K_matrix()), this, SLOT(onWrongKMatrix()));

    connect(_thread, SIGNAL(send_result_fuse(CTriangularMesh*)), _glWidget, SLOT(on_new_fused_mesh_computed(CTriangularMesh*)));


      //////////////////////

      connect(ui->btnPrevWnd, SIGNAL(clicked()), this, SLOT(previewWindowToggle()) );

      _prevDlg = new CLaserFramePreviewDlg;
      _prevDlg->setVisible(false);

      connect(ui->sldFrameIdx,   SIGNAL(valueChanged(int)), this, SLOT(slider_frame_moved(int)));
      connect(ui->sldFrameIdx,   SIGNAL(sliderReleased()), this, SLOT(refresh_preview()));
      connect(ui->btnFramNext,   SIGNAL(clicked()), this, SLOT(btn_frame_clicked_pos()));
      connect(ui->btnFramPrev,   SIGNAL(clicked()), this, SLOT(btn_frame_clicked_neg()));
      connect(ui->btnFramNext,   SIGNAL(clicked()), this, SLOT(refresh_preview()));
      connect(ui->btnFramPrev,   SIGNAL(clicked()), this, SLOT(refresh_preview()));


        //// frames_count_changed( );
        ui->sldFrameIdx->setValue(50);


    ////////////// edge detection /////////////////////////
    ui->sldColorThres->setValue(CGlobalSettings::_lcthresh);
    ui->lblColorThres->setText(QString::number(CGlobalSettings::_lcthresh));
    connect(ui->sldColorThres,   SIGNAL(valueChanged(int)), this, SLOT(edge_thresh_changed(int )));
    connect(ui->sldColorThres,   SIGNAL(sliderReleased()), this, SLOT(refresh_preview()));

    connect(ui->sldCorrWndSz,   SIGNAL(sliderReleased()), this, SLOT(refresh_preview()));
    connect(ui->sldCorrWndSz,   SIGNAL(valueChanged(int)), this, SLOT(corwndsz_changed()));
    ui->sldCorrWndSz->setValue( CGlobalSettings::_laser_corrwnd_sz );


    ////connect(ui->chkEdge,   SIGNAL(stateChanged(int)), this, SLOT(refresh_preview( )));


    ui->sldColorThres->setValue(CGlobalSettings::_lcthresh);


    connect(_prevDlg, SIGNAL(close_preview()), this, SLOT(updPrevBtn()));

}

void CLaserSanOfflineDlg::updPrevBtn()
{
    ui->btnPrevWnd->setChecked(false);
}


void CLaserSanOfflineDlg::onSaveCalib()
{
    QString fileName = QFileDialog::getSaveFileName(this,
          tr("Save calibration data ..."), "", "Calibration data (*.cdat)");

    if (fileName.isEmpty()) return;
    g_3d_corrector.save_calib_data(fileName);
}

void CLaserSanOfflineDlg::onLoadCalib()
{
    QString fileName = QFileDialog::getOpenFileName(this,
          tr("Load calibration data ..."), "", "Calibration data (*.cdat)");

    if (fileName.isEmpty()) return;
    g_3d_corrector.load_calib_data(fileName);
}

void CLaserSanOfflineDlg::onLoadINIBtn()
{
    QString fileName = QFileDialog::getOpenFileName(this,
          tr("Open scan INI file ..."), "", "Scan parameters(*.ini)");

    if (fileName.isEmpty()) return;

    read_ini_file(fileName);
}

void CLaserSanOfflineDlg::onWrongKMatrix()
{
    QMessageBox::warning(this, tr("Warning"),wrn_FailedFindKMatrix);
}

void CLaserSanOfflineDlg::read_ini_file(QString & fileName)
{
    if(_prevDlg)
    {
        _prevDlg->setVisible(false);
        _prevDlg->clean_data();
    }
    ui->btnPrevWnd->setChecked(false);

    ui->edtININame->setText(fileName);

    QFileInfo info_fname(fileName);
    QSettings settings(fileName , QSettings::IniFormat);

    _scan_dir = info_fname.absolutePath();



   /// settings.value(ini_video_name_format).toString();










    _video_name_format = settings.value(ini_video_name_format).toString();
    _video_desc_format = settings.value(ini_video_desc_format).toString();
    _video_name_has_scan = settings.value(ini_video_name_has_scan).toBool();

   _image_name_format = settings.value(ini_image_name_format).toString();
   _image_name_has_scan = settings.value(ini_image_name_has_scan).toBool();
   _first_scan = settings.value(ini_first_scan).toInt();
   _last_scan = settings.value(ini_last_scan).toInt();
   _is_difference = settings.value(ini_is_difference).toBool();
   _has_marks = settings.value(ini_has_marks).toBool();
   _is_round_marks = settings.value(ini_is_round_marks).toInt();
   _bckg_image= settings.value(ini_bckg_image).toString();
   _bckg_image2 = settings.value(ini_bckg_image2).toString();
   _marks_x = settings.value(ini_marks_x).toInt();
   _marks_y = settings.value(ini_marks_y).toInt();

   _use_metadata_angle = settings.value(ini_use_metadata_angle).toBool();
   _camera_laser_interchanged = settings.value(ini_camera_laser_interchanged).toBool();
   _meta_angle_correction =  settings.value(ini_meta_angle_correction).toFloat();

   init_avi_acc();
   init_bckg_image();
   init_frame_map();



}
void CLaserSanOfflineDlg::init_avi_acc()
{
    _first_image = 0;

    char * vid_name_buf = new char[2048];
    if(_video_name_has_scan)
        std::sprintf(vid_name_buf, _video_name_format.toStdString().c_str(), _first_scan);
    else
        std::sprintf(vid_name_buf, _video_name_format.toStdString().c_str());
    QString _first_avi_name =  _scan_dir + "/" + QString(vid_name_buf);
    delete  [] vid_name_buf;


    clean_avi_acc();

    _in_avi = new  cv::VideoCapture(_first_avi_name.toLocal8Bit().constData());
    if(!_in_avi->isOpened() )
    {
         QString msg = QString("Cannot load %1").arg(_first_avi_name);
         QMessageBox::information(this, tr("Information"), msg);
         clean_avi_acc();
         return;
    }
    int frame_count = _in_avi->get(CV_CAP_PROP_FRAME_COUNT);
    _last_image = frame_count -1;

    ui->sldFrameIdx->setMinimum(_first_image);
    ui->sldFrameIdx->setMaximum(_last_image);

}

void CLaserSanOfflineDlg::clean_avi_acc()
{
    if(_in_avi != NULL)
    {
         _in_avi->release();
          delete _in_avi;
         _in_avi = NULL;
    }

    if(_bg_img != NULL)
    {
        delete _bg_img;
        _bg_img = NULL;
    }
}

void CLaserSanOfflineDlg::init_bckg_image()
{
    if(_in_avi == NULL) return;
    _in_avi->set(CV_CAP_PROP_POS_FRAMES, 1);

    cv::Mat frame;
    _in_avi->read(frame);

    _bg_img = new QImage((uchar*)frame.data, frame.cols, frame.rows,  QImage::Format_RGB888);
    *_bg_img = _bg_img->rgbSwapped();
}

void CLaserSanOfflineDlg::init_frame_map()
{
    char * vid_desc_buf = new char[2048];
    if(_video_name_has_scan)
        std::sprintf(vid_desc_buf, _video_desc_format.toStdString().c_str(), _first_scan);
    else
        std::sprintf(vid_desc_buf, _video_desc_format.toStdString().c_str());
    QString first_desc_name =  _scan_dir + "/" + QString(vid_desc_buf);
    delete [] vid_desc_buf;


    bool res = parse_description_video_file(first_desc_name,_frame_map);
    if (!res)
    {
        QString msg = QString("Cannot load %1").arg(first_desc_name);
        QMessageBox::information(this, tr("Information"), msg);
        return;
    }
}

float CLaserSanOfflineDlg::convert_to_angle(int meta_int_angle)
{
    return   360. * float(meta_int_angle) /16384. + _meta_angle_correction ;
}

void CLaserSanOfflineDlg::edge_thresh_changed(int val)
{
    CGlobalSettings::_lcthresh = val;
    ui->lblColorThres->setText(QString::number(CGlobalSettings::_lcthresh));
}



void CLaserSanOfflineDlg::previewWindowToggle()
{
    if(ui->btnPrevWnd->isChecked())
    {
        _prevDlg->setVisible(true);
        refresh_preview();
    }
    else
        _prevDlg->setVisible(false);
}

void CLaserSanOfflineDlg::onMakeProgressStep(int val)
{
    ui->progressBar->setValue(val);
}

CLaserSanOfflineDlg::~CLaserSanOfflineDlg()
{
    delete ui;

    clean_avi_acc();
    delete _thread;
    delete _prevDlg;
}

void CLaserSanOfflineDlg::onCompleteProcessing()
{
    init_avi_acc();
    init_bckg_image();

    close_dialog();


    /*_is_result_valid = true;
    ui->progressBar->setValue(100);
    */

}


void CLaserSanOfflineDlg::onStopProcessing()
{
    ui->applyBtn->setEnabled(true);
    ui->edtDecimPntStep->setEnabled(true);
    /////ui->edtSideLen->setEnabled(true);
    ////ui->chckRandColors->setEnabled(true);
    ui->btnPrevWnd->setEnabled(true);
    ui->sldFrameIdx->setEnabled(true);
    ///ui->chkEdge->setEnabled(true);
    ui->sldColorThres->setEnabled(true);
    ui->sldCorrWndSz->setEnabled(true);
    ////ui->chkMesh->setEnabled(true);
    ui->btnFramPrev->setEnabled(true);
    ui->btnFramNext->setEnabled(true);
    ui->cmbFrameDecim->setEnabled(true);

    ui->btnBrowseINI->setEnabled(true);

    _in_process = false;
}


void CLaserSanOfflineDlg::close_dialog()
{
    _on_apply = false;
    close();
}




void CLaserSanOfflineDlg::onCancelBtn()
{
    if(!_in_process)
    {
         close_dialog();
    }
    _in_process = false;
    _on_apply = false;
}


void CLaserSanOfflineDlg::onApplyBtn()
{
   /*_on_apply = true;
    if(!_is_result_valid)
        onPreviewBtn();
    else
        close_dialog();
    */

    ui->btnPrevWnd->setChecked(false);
    _prevDlg->setVisible(false);

    ui->applyBtn->setEnabled(false);
    ui->edtDecimPntStep->setEnabled(false);
    ui->edtSideLen->setEnabled(false);
    ui->chckRandColors->setEnabled(false);
    ui->btnPrevWnd->setEnabled(false);
    ui->sldFrameIdx->setEnabled(false);
    /// ui->chkEdge->setEnabled(false);
    ui->sldColorThres->setEnabled(false);
    ui->sldCorrWndSz->setEnabled(false);
    ui->cmbFrameDecim->setEnabled(false);
    ui->btnBrowseINI->setEnabled(false);
    ui->chkMesh->setEnabled(false);
    ui->btnFramPrev->setEnabled(false);
    ui->btnFramNext->setEnabled(false);

    _in_process = true;

    // release video strem
    clean_avi_acc();

    QString ini_name = ui->edtININame->text();
    _thread->start_process(ini_name,
                           ui->cmbFrameDecim->currentText().toInt(),
                          (CGlobalSettings::_laser_corrwnd_sz>0) );

}

void CLaserSanOfflineDlg::showEvent(QShowEvent * event)
{
    ui->progressBar->setValue(0);

    _is_result_valid    = false;
    _on_apply           = false;
    _in_process         = false;

    ui->btnPrevWnd->setChecked(false);
    _prevDlg->setVisible(false);
}

void CLaserSanOfflineDlg::hideEvent(QHideEvent *)
{
    ui->btnPrevWnd->setChecked(false);
    _prevDlg->setVisible(false);
}


void CLaserSanOfflineDlg::slider_frame_moved(int)
{
    QString frm = QString("Frame: %1").arg(ui->sldFrameIdx->value());
    ui->lblFrame->setText(frm);
}

void CLaserSanOfflineDlg::btn_frame_clicked_pos()
{
    int frame = ui->sldFrameIdx->value();
    ui->sldFrameIdx->setValue(++frame);
}

void CLaserSanOfflineDlg::btn_frame_clicked_neg()
{
    int frame = ui->sldFrameIdx->value();
    ui->sldFrameIdx->setValue(--frame);
}


void CLaserSanOfflineDlg::corwndsz_changed()
{
    CGlobalSettings::_laser_corrwnd_sz = ui->sldCorrWndSz->value();
     ui->lblCorrWndsSz->setText(QString::number( ui->sldCorrWndSz->value()));
}

void CLaserSanOfflineDlg::refresh_preview()
{

    if(!ui->btnPrevWnd->isChecked())
        return;

    if(_in_avi == NULL)
        return;

    int current_frame_idx =  ui->sldFrameIdx->value();
    _in_avi->set(CV_CAP_PROP_POS_FRAMES, current_frame_idx);

    cv::Mat frame;
    _in_avi->read(frame);
    if(frame.empty())
    {
        std::cout << "error cannot extaract frame " << std::endl;
        return;
    }

    QImage * img = new QImage((uchar*)frame.data, frame.cols, frame.rows,  QImage::Format_RGB888);
    *img = img->rgbSwapped();

   std::map<int, CFrameDesc>::iterator itm =  _frame_map.find(current_frame_idx);

    bool use_meta = false;
    float meta_angle = 0;
    if(itm != _frame_map.end())
    {
        use_meta = (itm->second._typ == CFrameDesc::FR)?true:false;
        if(use_meta)
               meta_angle = itm->second._angle + _meta_angle_correction;
    }

    std::cout << use_meta << std::endl;

    _prevDlg->update_image_from_video(img, true, _bg_img, _is_difference,  true,
                                      ( CGlobalSettings::_laser_corrwnd_sz>0), use_meta, meta_angle);

}
