#include "disparity_utils.h"

void invert_3by3_matrix(CArray2D<double> * A, CArray2D<double> * invA)
{

    double determinant =  +A->_data[0][0] *(A->_data[1][1] * A->_data[2][2] - A->_data[2][1] * A->_data[1][2])
                        -A->_data[0][1] *(A->_data[1][0] * A->_data[2][2] - A->_data[1][2] * A->_data[2][0])
                        +A->_data[0][2] *(A->_data[1][0] * A->_data[2][1] - A->_data[1][1] * A->_data[2][0]);


    double invdet = 1/determinant;

    invA->_data[0][0] =  (A->_data[1][1] * A->_data[2][2] - A->_data[2][1] * A->_data[1][2])*invdet;
    invA->_data[1][0] = -(A->_data[0][1] * A->_data[2][2] - A->_data[0][2] * A->_data[2][1])*invdet;
    invA->_data[2][0] =  (A->_data[0][1] * A->_data[1][2] - A->_data[0][2] * A->_data[1][1])*invdet;

    invA->_data[0][1] = -(A->_data[1][0] * A->_data[2][2] - A->_data[1][2] * A->_data[2][0])*invdet;
    invA->_data[1][1] =  (A->_data[0][0] * A->_data[2][2] - A->_data[0][2] * A->_data[2][0])*invdet;
    invA->_data[2][1] = -(A->_data[0][0] * A->_data[1][2] - A->_data[1][0] * A->_data[0][2])*invdet;

    invA->_data[0][2] =  (A->_data[1][0] * A->_data[2][1] - A->_data[2][0] * A->_data[1][1])*invdet;
    invA->_data[1][2] = -(A->_data[0][0] * A->_data[2][1] - A->_data[2][0] * A->_data[0][1])*invdet;
    invA->_data[2][2] =  (A->_data[0][0] * A->_data[1][1] - A->_data[1][0] * A->_data[0][1])*invdet;
}

CArray1D<double3> * convert_to_array(QRgb * in, int xs, int ys)
{
    CArray1D<double3> * res = new CArray1D<double3> (xs * ys);
    for(int y = 0,line_dx = 0; y < ys; y++, line_dx +=xs)
    {
        int y_xs = y * xs;
        for(int x =0, idx = line_dx; x < xs; x++, idx++ )
        {
            res->_data[y_xs + x].r = qRed  (in[idx]);
            res->_data[y_xs + x].g = qGreen(in[idx]);
            res->_data[y_xs + x].b = qBlue (in[idx]);
        }
    }
    return res;
}

CArray1D<double> * convert_to_gray(CArray1D<double3> * inI, int xs, int ys)
{

    // 0.2989, 0.5870, 0.1140.
    double redC, greC, bluC;
    *(reinterpret_cast<__int64 *>(&redC)) = 0x3fd321c48cc36668L;
    *(reinterpret_cast<__int64 *>(&greC)) = 0x3fe2c90e8ec38c28L;
    *(reinterpret_cast<__int64 *>(&bluC)) = 0x3fbd307956d60524L;


    CArray1D<double> * res = new CArray1D<double> (xs * ys);
    for(int y = 0, off=0; y < ys; y++)
    {
        for(int x =0; x < xs; x++, off++)
        {
            double r = inI->_data[off].r;
            double g = inI->_data[off].g;
            double b = inI->_data[off].b;


            double color = redC * r + greC * g + bluC * b;
            if(color < 0) color =0;
            if(color > 1) color =1;
            res->_data[off] = color;
        }
    }
    return res;
}


CArray1D<double> *  guidedfilter_color_fast(CArray1D<double3> * inI,
                                       CArray1D<double> * inP, int r, double eps,
CArray1D<double> * N, CArray1D<double3> * mean_I, std::vector< CArray2D<double> > * arrSigma, int xs , int ys)
{



    CArray1D<double> * mean_p = boxfilter_img(inP, xs, ys, r);
    CArray1D<double3> * mean_iIp  = new CArray1D<double3> (xs*ys);
    for(int y = 0, off=0; y < ys; y++)
        for(int x =0; x < xs; x++, off++)
        {
            mean_p->_data[off] /= (float)N->_data[off];
            mean_iIp->_data[off] = inI->_data[off]  * inP->_data[off];
        }


    CArray1D<double3> * mean_Ip = boxfilter_img(mean_iIp,xs,ys, r);
    for(int y = 0, off=0; y < ys; y++)
        for(int x =0; x < xs; x++, off++)
             mean_Ip ->_data[off] = mean_Ip ->_data[off] / N->_data[off];
    delete mean_iIp;

    CArray1D<double3> cov_Ip = CArray1D<double3> (xs * ys);
    for(int y = 0, off = 0; y < ys; y++)
        for(int x =0; x < xs; x++, off++)
             cov_Ip._data[off] = mean_Ip->_data[off] - mean_I->_data[off] * mean_p->_data[off];
    delete mean_Ip;


    CArray1D<double3> * matA = new CArray1D<double3> (xs * ys);
    std::vector< CArray2D<double> >::iterator its = arrSigma->begin();
    for(int y = 0, off=0; y < ys; y++)
    {
        for(int x =0; x < xs; x++, its++, off++)
        {
                matA->_data[off].r =  cov_Ip._data[off].r * its->_data[0][0] +
                                      cov_Ip._data[off].g * its->_data[0][1] +
                                      cov_Ip._data[off].b * its->_data[0][2];

                matA->_data[off].g =  cov_Ip._data[off].r * its->_data[1][0] +
                                      cov_Ip._data[off].g * its->_data[1][1] +
                                      cov_Ip._data[off].b * its->_data[1][2];

                matA->_data[off].b =  cov_Ip._data[off].r * its->_data[2][0] +
                                      cov_Ip._data[off].g * its->_data[2][1] +
                                      cov_Ip._data[off].b * its->_data[2][2];
        }
    }



    CArray1D<double> * matB = new CArray1D<double>(xs*ys) ;
    for(int y = 0, off=0; y < ys; y++)
        for(int x =0; x < xs; x++, off++)
            matB->_data[off] = mean_p->_data[off] - matA->_data[off].r * mean_I->_data[off].r
                                                    - matA->_data[off].g * mean_I->_data[off].g
                                                    - matA->_data[off].b * mean_I->_data[off].b;
    CArray1D<double3> * matA_box = boxfilter_img(matA, xs, ys, r);

    CArray1D<double> * matB_box = boxfilter_img(matB,xs,ys, r);
    CArray1D<double> * resQ = new CArray1D<double> (xs*ys);

    for(int y = 0, off=0; y < ys; y++)
    {
        for(int x =0; x < xs; x++, off++)
        {
            resQ->_data[off] = ( matA_box->_data[off].r * inI->_data[off].r +
                                  matA_box->_data[off].g * inI->_data[off].g +
                                  matA_box->_data[off].b * inI->_data[off].b +
                                  matB_box->_data[off]) / N->_data[off];
        }
    }

   delete matB_box;
   delete matA_box;
   delete matA;
   delete matB;
   delete mean_p;


   return resQ;

}


CArray1D<double> *  gradient_norm(CArray1D<double> * I, int xs, int ys)
{
    CArray1D<double> * res  = new CArray1D<double> (xs*ys);

    for(int y = 0; y < ys; y++)
    {
        int y_xs = y * xs;
        for(int x =1; x < xs-1; x++)
            res->_data[y_xs+x] = 0.5 *(I->_data[y_xs+x+1] - I->_data[y_xs+x-1]) + .5;
    }

    for(int y = 0; y < ys; y++)
    {
        res->_data[y*xs] = I->_data[y*xs + 1] - I->_data[y*xs] + .5;
        res->_data[y*xs+xs -1] = I->_data[y*xs + xs -1] - I->_data[y*xs+xs -2] + .5;
    }

    return res;
}


void  filtermask(CArray2D<double> * weights, int * patch_xs, int * patch_ys,
                               CArray1D<double> * colimg_r, CArray1D<double> * colimg_g,
                               CArray1D<double> * colimg_b, int  xx, int yy,
                               int winsize, double gamma_c, double gamma_p, int xs, int ys)
{
    int radius = (winsize / 2);


     int left   = std::max(0,      xx-radius);
     int right  = std::min(xs-1,   xx+radius);
     int bottom = std::max(0,      yy-radius);
     int top    = std::min(ys - 1, yy+radius);

     *patch_xs = right - left + 1;
     *patch_ys = top - bottom + 1;

     int yy_xs_xx = yy * xs + xx;
     double gamma_csq_inv = -1.0/(gamma_c*gamma_c);
     double gamma_psq_inv = -1.0/(gamma_p*gamma_p);
     for(int yw = 0, y = bottom; y <= top; y++, yw++)
      {
          double dy = y - yy;
          double dy_dy = dy*dy;
          int y_xs = y * xs;
          for(int xw = 0, x =left; x <= right; x++, xw++)
          {
              double _rd = (colimg_r->_data[yy_xs_xx] - colimg_r->_data[y_xs+x]);
              double rd =  _rd * _rd;

              double _gd = (colimg_g->_data[yy_xs_xx] - colimg_g->_data[y_xs+x]);
              double gd = _gd * _gd;

              double _bd =(colimg_b->_data[yy_xs_xx] - colimg_b->_data[y_xs+x]);
              double bd = _bd * _bd;

              double coldiff = std::sqrt(rd + gd + bd) * gamma_csq_inv;

              double dx = x - xx;
              double sdiff = std::sqrt(dx * dx +  dy_dy ) * gamma_psq_inv;

              weights->_data[yw][xw] =  std::exp(coldiff) * std::exp(sdiff);
          }
     }
}


void horisontal_shift(QImage * & img, int bias, int color)
{
    int xs = img->width();
    int ys = img->height();

    QImage * res = new QImage(img->size(), img->format());

    for(int y = 0; y < ys; y++)
    {
           for(int x =0; x < xs; x++ )
           {
               QRgb p;
               if(x >= bias)
                    p = img->pixel(x-bias,y);
               else
                    p = qRgb(color, color, color);

               res->setPixel(x, y, p);
           }
    }
    delete img;
    img = res;
}

void init_guidedfilter_inputs(CArray1D<double3> * inI, int r, double eps,
                      CArray1D<double> * & N, CArray1D<double3> * & mean_I,
                              std::vector< CArray2D<double> > * & arrSigma, int xs, int ys
                              )
{


    CArray1D<double> * ones = new CArray1D<double> (xs*ys, 1);
    N = boxfilter_img(ones, xs, ys, r);
    delete ones;


    CArray1D<double3> * smoo_inI = boxfilter_img(inI, xs, ys, r);
    mean_I = new CArray1D<double3> (xs*ys);
    for(int y = 0, off=0; y < ys; y++)
        for(int x =0; x < xs; x++, off++)
            mean_I->_data[off] = smoo_inI->_data[off] / N->_data[off];
    delete smoo_inI;


    CArray1D<double> * in_var_I_rr = new CArray1D<double> (xs* ys);
    CArray1D<double> * in_var_I_rg = new CArray1D<double> (xs* ys);
    CArray1D<double> * in_var_I_rb = new CArray1D<double> (xs* ys);
    CArray1D<double> * in_var_I_gg = new CArray1D<double> (xs* ys);
    CArray1D<double> * in_var_I_gb = new CArray1D<double> (xs* ys);
    CArray1D<double> * in_var_I_bb = new CArray1D<double> (xs* ys);
    for(int y = 0, off=0; y < ys; y++)
    {
        for(int x =0; x < xs; x++, off++)
        {
            in_var_I_rr->_data[off] = inI->_data[off].r  * inI->_data[off].r;
            in_var_I_rg->_data[off] = inI->_data[off].r  * inI->_data[off].g;
            in_var_I_rb->_data[off] = inI->_data[off].r  * inI->_data[off].b;
            in_var_I_gg->_data[off] = inI->_data[off].g  * inI->_data[off].g;
            in_var_I_gb->_data[off] = inI->_data[off].g  * inI->_data[off].b;
            in_var_I_bb->_data[off] = inI->_data[off].b  * inI->_data[off].b;
        }
    }

    CArray1D<double> * var_I_rr = boxfilter_img(in_var_I_rr, xs, ys, r);
    CArray1D<double> * var_I_rg = boxfilter_img(in_var_I_rg, xs, ys, r);
    CArray1D<double> * var_I_rb = boxfilter_img(in_var_I_rb, xs, ys, r);
    CArray1D<double> * var_I_gg = boxfilter_img(in_var_I_gg, xs, ys, r);
    CArray1D<double> * var_I_gb = boxfilter_img(in_var_I_gb, xs, ys, r);
    CArray1D<double> * var_I_bb = boxfilter_img(in_var_I_bb, xs, ys, r);
    delete in_var_I_rr;
    delete in_var_I_rg;
    delete in_var_I_rb;
    delete in_var_I_gg;
    delete in_var_I_gb;
    delete in_var_I_bb;

    for(int y = 0, off=0; y < ys; y++)
    {
        for(int x =0; x < xs; x++, off++)
        {
            var_I_rr->_data[off] =  var_I_rr->_data[off]/N->_data[off] - mean_I->_data[off].r * mean_I->_data[off].r;
            var_I_rg->_data[off] =  var_I_rg->_data[off]/N->_data[off] - mean_I->_data[off].r * mean_I->_data[off].g;
            var_I_rb->_data[off] =  var_I_rb->_data[off]/N->_data[off] - mean_I->_data[off].r * mean_I->_data[off].b;
            var_I_gg->_data[off] =  var_I_gg->_data[off]/N->_data[off] - mean_I->_data[off].g * mean_I->_data[off].g;
            var_I_gb->_data[off] =  var_I_gb->_data[off]/N->_data[off] - mean_I->_data[off].g * mean_I->_data[off].b;
            var_I_bb->_data[off] =  var_I_bb->_data[off]/N->_data[off] - mean_I->_data[off].b * mean_I->_data[off].b;
        }
    }

    CArray2D<double> Sigma(3, 3);
    CArray2D<double> invSigma(3, 3);
    arrSigma = new std::vector< CArray2D<double> > ;

    for(int y = 0, off=0; y < ys; y++)
    {
        for(int x =0; x < xs; x++, off++)
        {


                Sigma._data[0][0] = var_I_rr->_data[off] + eps;
                Sigma._data[0][1] = var_I_rg->_data[off];
                Sigma._data[0][2] = var_I_rb->_data[off];

                Sigma._data[1][0] = var_I_rg->_data[off];
                Sigma._data[1][1] = var_I_gg->_data[off] + eps;
                Sigma._data[1][2] = var_I_gb->_data[off];

                Sigma._data[2][0] = var_I_rb->_data[off];
                Sigma._data[2][1] = var_I_gb->_data[off];
                Sigma._data[2][2] = var_I_bb->_data[off] + eps;

                invert_3by3_matrix(&Sigma, &invSigma);


                arrSigma->push_back(invSigma);
        }
    }

    delete var_I_rr;
    delete var_I_rg;
    delete var_I_rb;
    delete var_I_gg;
    delete var_I_gb;
    delete var_I_bb;

}

void clear_guidedfilter_inputs( CArray1D<double> * N, CArray1D<double3> * mean_I,
                              std::vector< CArray2D<double> > * arrSigma)
{
    delete N;
    delete mean_I;
    delete arrSigma;
}
