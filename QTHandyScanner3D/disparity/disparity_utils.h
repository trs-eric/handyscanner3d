#ifndef __DISPARITY_UTILS_H__
#define __DISPARITY_UTILS_H__

#include <QImage>

#include "carray1d.h"
#include "carray2d.h"

inline void invert_3by3_matrix(CArray2D<double> * A, CArray2D<double> * invA);

struct double3
{
    double r;
    double g;
    double b;

    double3(double v = 0) : r(v), g(v), b(v){}

    double3(const double3& rhs)
    {
        r = rhs.r;
        g = rhs.g;
        b = rhs.b;
    }

    inline double3 operator-(const double3& rhs)
    {
        double3 res;
        res.r = r - rhs.r;
        res.g = g - rhs.g;
        res.b = b - rhs.b;
        return res;
    }

    inline double3 operator+(const double3& rhs)
    {
        double3 res;
        res.r = r + rhs.r;
        res.g = g + rhs.g;
        res.b = b + rhs.b;
        return res;
    }

    inline double3 operator/(const double v)
    {
        double3 res;
        res.r = r /v;
        res.g = g /v;
        res.b = b /v;
        return res;
    }

    inline double3 operator*(const double v)
    {
        double3 res;
        res.r = r * v;
        res.g = g * v;
        res.b = b * v;
        return res;
    }
    inline double3 operator=(const double3& rhs)
    {
        r = rhs.r;
        g = rhs.g;
        b = rhs.b;
        return (*this);
    }
};



CArray1D<double3> * convert_to_array(QRgb * in, int xs, int ys);

CArray1D<double> * convert_to_gray(CArray1D<double3> * inI, int xs, int ys);


template< class T >
CArray1D<T> * cumulative_sum_y(CArray1D<T> * in, int xs, int ys)
{

    CArray1D<T> * res = new CArray1D<T> (xs * ys);

    /// fill 0th line
    for(int x =0; x < xs; x++)
        res->_data[x] = in->_data[x];

    //fill other
    for(int y = 1; y < ys; y++)
        for(int x =0; x < xs; x++)
            res->_data[y*xs+x] = res->_data[(y-1)*xs + x] + in->_data[y*xs + x];
    return res;
}




template< class T >
CArray1D<T> * cumulative_sum_x(CArray1D<T> * in, int xs, int ys)
{
    CArray1D<T> * res = new CArray1D<T> (xs * ys);

    /// fill 0th column
    for(int y =0; y < ys; y++)
        res->_data[y*xs] = in->_data[y*xs];

    //fill other colums
    for(int y = 0; y < ys; y++)
        for(int x =1; x < xs; x++)
            res->_data[y*xs+x] = res->_data[y*xs +x-1] + in->_data[y*xs + x];
     return res;
}


template< class T >
CArray1D<T> *  boxfilter_img(CArray1D<T> * indata, int xs, int ys, const int r)
{
    const int rp1 = r + 1;

    CArray1D<T> * imCum  = cumulative_sum_y(indata, xs, ys);
    /// delete indata;

    CArray1D<T> * dist = new CArray1D<T> (xs*ys);

    /// %difference over Y axis
    /// imDst(1:r+1, :) = imCum(1+r:2*r+1, :); // lines 0 to r
    for(int y = 0; y < rp1; y++)
        for(int x =0; x < xs; x++)
            dist->_data[y*xs+x] = imCum->_data[(y+r)*xs+x];

    /// imDst(r+2:hei-r, :) = imCum(2*r+2:hei, :) - imCum(1:hei-2*r-1, :); // lines r + 1 to hei - r - 1
    int ys_m_r = ys - r;
    for(int y = rp1, y2=0; y < ys_m_r; y++, y2++)
         for(int x =0; x < xs; x++)
            dist->_data[y*xs+x] = imCum->_data[(y+r)*xs+x] - imCum->_data[y2*xs+x];

    /// imDst(hei-r+1:hei, :) = repmat(imCum(hei, :), [r, 1]) - imCum(hei-r-r:hei-r-1, :); // hei - r to h-1
    int ys_m_1 = ys-1;
    for(int y = ys_m_r; y < ys; y++)
    {
        int y_xs = y*xs;
        for(int x =0; x < xs; x++)
            dist->_data[y_xs+x] = imCum->_data[ys_m_1*xs+x] - imCum->_data[(y-rp1)*xs+x];
    }
    delete imCum;

    // %cumulative sum over X axis
    // imCum = cumsum(imDst, 2);
    imCum = cumulative_sum_x(dist, xs, ys);
    int xs_m_1 = xs - 1;
    int xs_m_r = xs - r;
    for(int y = 0; y < ys; y++)
    {
        int x =0;
        int y_xs = y*xs;
        for(; x < rp1; x++)
                dist->_data[y_xs+x] = imCum->_data[y_xs+x+r];
        for(; x < xs_m_r; x++)
                dist->_data[y_xs+x] = imCum->_data[y_xs+x+r] - imCum->_data[y_xs+x-rp1];
        for(; x < xs; x++)
                dist->_data[y_xs+x] = imCum->_data[y_xs+xs_m_1] - imCum->_data[y_xs+x-rp1];
    }
    delete imCum;
    return dist;
}




CArray1D<double> *  guidedfilter_color_fast(CArray1D<double3> * inI,
                                       CArray1D<double> * inP, int r, double eps,
CArray1D<double> * N, CArray1D<double3> * mean_I,    std::vector< CArray2D<double> > * arrSigma, int xs , int ys );


 CArray1D<double> *  gradient_norm(CArray1D<double> * I, int xs, int ys);

 template< class T >
 CArray1D<T> * medfilt2(CArray1D<T> * inarr, int xs, int ys)
 {

     int radius = 1;
     CArray1D<T> * res = new CArray1D<T>(xs*ys,0);
     for(int y = 0; y < ys-1; y++)
     {
         int top = std::max(y-radius,0);
         int bottom = std::min(y+radius, ys-1);

         for(int x =0; x < xs-1; x++)
         {
             int left = std::max(x - radius,0);
             int right = std::min(x + radius, xs - 1);
             std::vector<T> nlist;
             for(int v = top; v <= bottom; v++)
                 for(int u = left; u <= right; u++)
                     nlist.push_back(inarr->_data[v*xs+u]);
             std::sort(nlist.begin(), nlist.end());
             res->_data[y*xs+x] = nlist[nlist.size()/2];
         }
     }
     return res;
 }


 void  filtermask(CArray2D<double> * weights, int * patch_xs, int * patch_ys,
                                CArray1D<double> * colimg_r, CArray1D<double> * colimg_g,
                                CArray1D<double> * colimg_b, int  xx, int yy,
                                int winsize, double gamma_c, double gamma_p, int xs, int ys);


void horisontal_shift(QImage * & img, int bias, int color);

void init_guidedfilter_inputs(CArray1D<double3> * inI, int r, double eps,
                      CArray1D<double> * & N, CArray1D<double3> * & mean_I,
                              std::vector< CArray2D<double> > * & arrSigma, int xs, int ys
                              );

void clear_guidedfilter_inputs( CArray1D<double> * N, CArray1D<double3> * mean_I,
                              std::vector< CArray2D<double> > * arrSigma);

#endif
