#ifndef CMESHDUMPTHREAD_H
#define CMESHDUMPTHREAD_H

#include <QThread>
class CTriangularMesh;

class CMeshDumpThread: public QThread
{
Q_OBJECT

public:
    explicit CMeshDumpThread(QObject *parent = 0);
    ~CMeshDumpThread();
    void start_process(CTriangularMesh * mesh, std::string & fname);

signals:
    void done_dumping();

protected:
    void run();
    CTriangularMesh * _mesh;
    std::string _fname;
};

#endif // CMESHDUMPTHREAD_H
