#ifndef CEXPORTMANAGER_H
#define CEXPORTMANAGER_H

#include <QString>

class CExportManager
{
public:
    CExportManager();

     static const QString exportFileTypes;
};

#endif // CEXPORTMANAGER_H
