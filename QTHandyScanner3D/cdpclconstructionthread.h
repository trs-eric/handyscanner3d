#ifndef CDPCLCONSTRUCTIONTHREAD_H
#define CDPCLCONSTRUCTIONTHREAD_H

#include <QThread>
#include <QMutex>
#include "graphicutils.h"

class CDPCLConstructionThread : public QThread
{
    Q_OBJECT
public:
    explicit CDPCLConstructionThread(QObject *parent = 0);
    ~CDPCLConstructionThread();

    void start_process(int, int);
    bool is_abort(){return _abort;}

signals:
    void send_back(const int val);
    void send_dpcl(std::vector<C3DPoint> * cpoints);
    void send_newcam();

protected:
    void run();

public slots:
    void stop_process();

private:
    bool    _abort;
    QMutex  _mutex;

    int _decim_step;
    int _neighb_count;


};

#endif // CDPCLCONSTRUCTIONTHREAD_H
