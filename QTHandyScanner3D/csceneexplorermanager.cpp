#include "csceneexplorermanager.h"
#include "cglscene.h"

#include <QWidget>
#include <QTreeWidgetItem>
#include <QPainter>
#include "cglcompoundobject.h"
#include "mainwindow.h"


#define FBTN_WIRE 1
#define FBTN_FLAT 2
#define FBTN_SMOO 3
#define FBTN_TEXT 4


CSceneExplorerManager::CSceneExplorerManager(QWidget * t,  CGLScene * s,
                                             MenuActions * va ) :
    _treew(NULL), _scene(s), _root_raw_videos(NULL), _root_raw_pclouds(NULL),
    _root_raw_textures(NULL), _root_raw_decim_surfaces(NULL),_root_fused_surfaces(NULL),
    _obj_counter(0), _va(va)
{
    _treew = (QTreeWidget*)t;


    _current_btn_raw = FBTN_TEXT;
    _current_btn_surf = FBTN_SMOO;
    initialize_tree();

}

CSceneExplorerManager::~CSceneExplorerManager()
{
    //if(_root_raw_videos) delete _root_raw_videos;
    if(_root_raw_pclouds) delete _root_raw_pclouds;
    if(_root_raw_textures) delete _root_raw_textures;
    if(_root_raw_decim_surfaces) delete _root_raw_decim_surfaces;
    if(_root_fused_surfaces) delete _root_fused_surfaces;

}


void CSceneExplorerManager::update_column_width()
{
    _treew->resizeColumnToContents(0);
}

void CSceneExplorerManager::initialize_tree( )
{
    QString str_raw_videos("RAW VIDEOS");
    QString str_raw_pclouds("RAW POINT CLOUDS");
    QString str_raw_textures("RAW TEXTURES");
    QString str_raw_decim_surfaces("DECIMATED SURFACES");
    QString str_fused_surface("FUSED SURFACE");


   // _root_raw_videos = new QTreeWidgetItem();
   // _root_raw_videos->setText(0, str_raw_videos);
   // _treew->addTopLevelItem(  _root_raw_videos);


    _root_raw_pclouds = new QTreeWidgetItem();
    _root_raw_pclouds->setText(0, str_raw_pclouds);
    _root_raw_pclouds->setIcon(1, QPixmap(":/images/icons/se_eyeopen.png"));
    _treew->addTopLevelItem(  _root_raw_pclouds);

    _root_raw_textures = new QTreeWidgetItem();
    _root_raw_textures->setText(0, str_raw_textures);
    _treew->addTopLevelItem(  _root_raw_textures);

   // _root_raw_decim_surfaces = new QTreeWidgetItem();
   // _root_raw_decim_surfaces->setText(0, str_raw_decim_surfaces);
   // _root_raw_decim_surfaces->setIcon(1, QPixmap(":/images/icons/se_eyeclosed.png"));
   // _treew->addTopLevelItem(  _root_raw_decim_surfaces);



    _root_fused_surfaces = new QTreeWidgetItem();
    _root_fused_surfaces->setText(0, str_fused_surface);
    _root_fused_surfaces->setIcon(1, QPixmap(":/images/icons/se_eyeclosed.png"));
    _treew->addTopLevelItem(_root_fused_surfaces);

    _treew->expandAll();

    update_column_width();


}


void CSceneExplorerManager::add_unique_cobj(CGLCompoundObject * cobj)
{


    QString str_fused_surface("FUSED SURFACE [%1k tgls]");

    QString caption = str_fused_surface.arg((cobj->get_mesh() != NULL)?cobj->get_mesh()->get_tri_mesh()->_triangles.size()/1000:0);

    if(_root_fused_surfaces!= NULL )
        _root_fused_surfaces->setText(0, caption);

    update_scene_explorer();
}

void CSceneExplorerManager::add_new_cobj(CGLCompoundObject * cobj)
{

    if(cobj->is_unique_object())
    {
        add_unique_cobj(cobj);
        return ;
    }

    _obj_counter++;

    QString str_raw_videos("Video %1");
    QString str_raw_pclouds("Cloud %1 [%2k pnts]");
    QString str_raw_textures("Texture %1");
    QString str_raw_decim_surfaces("Surface %1 [%2k tgls]");

/*
    QTreeWidgetItem * raw_video = new QTreeWidgetItem();
    QString str_raw_videos_num = str_raw_videos.arg(_obj_counter);
    raw_video->setText(0, str_raw_videos_num);
    _root_raw_videos->addChild(raw_video);
*/


    QTreeWidgetItem * raw_pcloud = new QTreeWidgetItem();
    QString str_raw_pclouds_num = str_raw_pclouds.arg(_obj_counter).arg((cobj->get_raw_points()!=NULL)?cobj->get_raw_points()->get_points_count()/1000:0);
    raw_pcloud->setText(0, str_raw_pclouds_num);
    raw_pcloud->setIcon(1, QPixmap(":/images/icons/se_eyeopen.png"));
    _root_raw_pclouds->addChild(raw_pcloud);
    cobj->_it_raw_pcloud    = raw_pcloud;

    /* QTreeWidgetItem *  raw_texture = new QTreeWidgetItem();
     QString str_raw_textures_num = str_raw_textures.arg(_obj_counter);
     raw_texture->setText(0, str_raw_textures_num);
    _root_raw_textures->addChild(raw_texture);
    cobj->_it_raw_texture   = raw_texture;*/

   /* if(cobj->get_mesh() != NULL)
    {
        QTreeWidgetItem * raw_decim_surface = new QTreeWidgetItem();
        QString str_raw_decim_surfaces_num =  str_raw_decim_surfaces.arg(_obj_counter).arg((cobj->get_mesh() != NULL)?cobj->get_mesh()->get_tri_mesh()->_triangles.size()/1000:0);
        raw_decim_surface->setText(0, str_raw_decim_surfaces_num);
        raw_decim_surface->setIcon(1, QPixmap(":/images/icons/se_eyeopen.png"));
        _root_raw_decim_surfaces->addChild(raw_decim_surface);
        cobj->_it_raw_decim_surface = raw_decim_surface;

    }*/
    // make cobject remember own tree items

   // cobj->_it_raw_video     = raw_video;
    _see_pcls.push_back(true);
    _see_meshs.push_back(true);


    update_scene_explorer();
}


void CSceneExplorerManager::clean_all()
{


    for(int i=_root_raw_pclouds->childCount(); i>=0; i--)
    {
         _root_raw_pclouds->removeChild(_root_raw_pclouds->child(i));

    }
    _obj_counter = 0;

    _see_pcls.clear();
    _see_meshs.clear();

    update_scene_explorer();

}

QPixmap CSceneExplorerManager::prepare_pixmap(QString file, bool opaque)
{
    QPixmap selPixmap(file);
    if(opaque)
           return selPixmap;

    float opacity = 0.25;
    QPixmap transPixmap(selPixmap.size());
    transPixmap.fill(Qt::transparent);
    QPainter pd(&transPixmap);
    pd.setBackgroundMode(Qt::TransparentMode);
    pd.setBackground(QBrush(Qt::transparent));
    pd.eraseRect(selPixmap.rect());
    pd.setOpacity(opacity );
    pd.drawPixmap(0, 0, selPixmap);
    pd.end();

    return transPixmap;
}

void CSceneExplorerManager::update_scene_explorer()
{


    if(_scene->_disable_raw_points_render)
        _root_raw_pclouds->setIcon(1, QPixmap(":/images/icons/se_eyeclosed.png"));
    else
        _root_raw_pclouds->setIcon(1, QPixmap(":/images/icons/se_eyeopen.png"));

   /* if(_scene->_disable_decim_surface_render)
        _root_raw_decim_surfaces->setIcon(1, QPixmap(":/images/icons/se_eyeclosed.png"));
    else
        _root_raw_decim_surfaces->setIcon(1, QPixmap(":/images/icons/se_eyeopen.png"));*/

    if(_scene->_disable_fused_surface_render)
        _root_fused_surfaces->setIcon(1, QPixmap(":/images/icons/se_eyeclosed.png"));
    else
        _root_fused_surfaces->setIcon(1, QPixmap(":/images/icons/se_eyeopen.png"));


    std::vector<CGLCompoundObject*> cobjlist = _scene->get_list_of_all_cglobjects();

    std::vector<CGLCompoundObject*>::iterator cit = cobjlist.begin();
    for( int c = 0; cit != cobjlist.end(); cit++, c++)
    {

        CGLCompoundObject * cobj = *cit;

        if(cobj->is_unique_object()) continue;

        /*
        if( //cobj->_it_raw_video     == NULL ||
            cobj->_it_raw_pcloud    == NULL ||
            cobj->_it_raw_texture   == NULL ||
            cobj->_it_raw_decim_surface == NULL)
                continue;
        */

        // close all eyes

        QString o_pcloud_pixmap(":/images/icons/se_eyeclosed.png");
        QString o_surface_pixmap(":/images/icons/se_eyeclosed.png");


        /*
        CGLCompoundObject::ViewingMode mode = cobj->get_vmode();
        switch(mode)
        {
           case CGLCompoundObject::RAW_POINTS:
                o_pcloud_pixmap = QString(":/images/icons/se_eyeopen.png");
                break;
           case CGLCompoundObject::MESH:
                o_surface_pixmap = QString(":/images/icons/se_eyeopen.png");
                break;
        }*/

        if(_see_pcls[c])
            o_pcloud_pixmap = QString(":/images/icons/se_eyeopen.png");
        if(_see_meshs[c])
            o_surface_pixmap = QString(":/images/icons/se_eyeopen.png");


        if(cobj->_it_raw_pcloud != NULL)
            cobj->_it_raw_pcloud->setIcon(1, prepare_pixmap(o_pcloud_pixmap, !_scene->_disable_raw_points_render));
        if(cobj->_it_raw_decim_surface!= NULL)
            cobj->_it_raw_decim_surface->setIcon(1, prepare_pixmap(o_surface_pixmap, !_scene->_disable_decim_surface_render));


    }



    update_field_buttons();

}

void CSceneExplorerManager::update_field_buttons()
{
    std::vector<CGLCompoundObject*> cobjlist = _scene->get_list_of_all_cglobjects();


    std::vector<CGLCompoundObject*>::iterator cit = cobjlist.begin();
    bool has_raw_pnts = false;
    bool has_mesh = false;
    for( ; cit != cobjlist.end(); cit++ )
    {
        CGLCompoundObject * cobj = *cit;
        if(cobj->get_raw_points() != NULL)
            has_raw_pnts = true;

        if(cobj->get_mesh() != NULL)
            has_mesh = true;
    }

    _va->fbtn_vw_wr->setEnabled(false);
    _va->fbtn_vw_tx->setEnabled(false);
    _va->fbtn_vw_fl->setEnabled(false);
    _va->fbtn_vw_sm->setEnabled(false);

    if(!_scene->_disable_raw_points_render && has_raw_pnts)
    {
        _va->fbtn_vw_sm->setEnabled(true);
        _va->fbtn_vw_tx->setEnabled(true);

        switch(_current_btn_raw )
        {
            case FBTN_TEXT:
                _va->fbtn_vw_wr->setChecked(false);
                _va->fbtn_vw_fl->setChecked(false);
                _va->fbtn_vw_sm->setChecked(false);
                _va->fbtn_vw_tx->setChecked(true);
                break;
            default:
            case FBTN_SMOO:
                _va->fbtn_vw_wr->setChecked(false);
                _va->fbtn_vw_fl->setChecked(false);
                _va->fbtn_vw_sm->setChecked(true);
                _va->fbtn_vw_tx->setChecked(false);
                break;
        }
    }
    else if((!_scene->_disable_decim_surface_render  || !_scene->_disable_fused_surface_render)&& has_mesh )
    {
        _va->fbtn_vw_wr->setEnabled(true);
        _va->fbtn_vw_fl->setEnabled(true);
        _va->fbtn_vw_sm->setEnabled(true);
        _va->fbtn_vw_tx->setEnabled(true);

        _va->fbtn_vw_wr->setChecked(false);
        _va->fbtn_vw_fl->setChecked(false);
        _va->fbtn_vw_sm->setChecked(false);
        _va->fbtn_vw_tx->setChecked(false);
        switch(_current_btn_surf)
        {
            case FBTN_WIRE:
                _va->fbtn_vw_wr->setChecked(true);
                break;
            case FBTN_FLAT:
                _va->fbtn_vw_fl->setChecked(true);
                break;
            case FBTN_TEXT:
                _va->fbtn_vw_tx->setChecked(true);
                break;
            default:
            case FBTN_SMOO:
                _va->fbtn_vw_sm->setChecked(true);
                break;
        }
    }

    /// TOERROR

    if(cobjlist.size() != 0 )
        _va->_main_window->updateFieldButton();
   //////////
}


void  CSceneExplorerManager::set_rend_mode_wire()
{

    if(!_scene->_disable_decim_surface_render || !_scene->_disable_fused_surface_render)
    {
        _current_btn_surf = FBTN_WIRE;
        std::vector<CGLCompoundObject*> cobjlist = _scene->get_list_of_all_cglobjects();
        std::vector<CGLCompoundObject*>::iterator cit = cobjlist.begin();

        for( ; cit != cobjlist.end(); cit++ )
        {
            CGLCompoundObject * cobj = *cit;
            cobj->set_mesh_render_mode(CTriangularMesh::MESH_WIRE);
        }
    }
    update_field_buttons();
}

void  CSceneExplorerManager::set_rend_mode_flat()
{
    if(!_scene->_disable_decim_surface_render || !_scene->_disable_fused_surface_render)
    {
        _current_btn_surf = FBTN_FLAT;
        std::vector<CGLCompoundObject*> cobjlist = _scene->get_list_of_all_cglobjects();
        std::vector<CGLCompoundObject*>::iterator cit = cobjlist.begin();

        for( ; cit != cobjlist.end(); cit++ )
        {
            CGLCompoundObject * cobj = *cit;
            cobj->set_mesh_render_mode(CTriangularMesh::MESH_FLAT);
        }
    }
    update_field_buttons();
}

void CSceneExplorerManager::set_rend_mode_smoo()
{
    if(!_scene->_disable_decim_surface_render || !_scene->_disable_fused_surface_render)
    {
        _current_btn_surf = FBTN_SMOO;
        std::vector<CGLCompoundObject*> cobjlist = _scene->get_list_of_all_cglobjects();
        std::vector<CGLCompoundObject*>::iterator cit = cobjlist.begin();
        for( ; cit != cobjlist.end(); cit++ )
        {
            CGLCompoundObject * cobj = *cit;
            cobj->set_mesh_render_mode(CTriangularMesh::MESH_SMOOTH);
        }
    }
    else if(!_scene->_disable_raw_points_render )
    {
        _current_btn_raw = FBTN_SMOO;
        std::vector<CGLCompoundObject*> cobjlist = _scene->get_list_of_all_cglobjects();
        std::vector<CGLCompoundObject*>::iterator cit = cobjlist.begin();
        for( ; cit != cobjlist.end(); cit++ )
        {
            CGLCompoundObject * cobj = *cit;
            cobj->set_common_colors_raw_points(true);
        }

    }
    update_field_buttons();
}

void CSceneExplorerManager::set_rend_mode_tex()
{

    if(!_scene->_disable_decim_surface_render || !_scene->_disable_fused_surface_render)
    {
        _current_btn_surf = FBTN_TEXT;
        std::vector<CGLCompoundObject*> cobjlist = _scene->get_list_of_all_cglobjects();
        std::vector<CGLCompoundObject*>::iterator cit = cobjlist.begin();
        for( ; cit != cobjlist.end(); cit++ )
        {
            CGLCompoundObject * cobj = *cit;
            cobj->set_mesh_render_mode(CTriangularMesh::MESH_TEX);
        }
    }
    else if(!_scene->_disable_raw_points_render )
    {
        _current_btn_raw = FBTN_TEXT;
        std::vector<CGLCompoundObject*> cobjlist = _scene->get_list_of_all_cglobjects();
        std::vector<CGLCompoundObject*>::iterator cit = cobjlist.begin();
        for( ; cit != cobjlist.end(); cit++ )
        {
            CGLCompoundObject * cobj = *cit;
            cobj->set_common_colors_raw_points(false);
        }

    }
    update_field_buttons();
}


/*

void CGLCompoundObject::update_menu_interface()
{
    if (  (_vmode != RAW_POINTS) && (_vmode != MESH) && (_vmode != MESHCAST) ) return;
    if(_va == NULL) return;
    if(!_is_current) return;

    if(_raw_points == NULL)
    {
       _va->view_raw_point_Act->setEnabled(false);
       _va->view_shad_pnt_Act->setEnabled(false);
    }
    else
    {
        _va->view_raw_point_Act->setEnabled(true);
        _va->view_shad_pnt_Act->setEnabled(true);
    }

    if((_mesh == NULL) )
    {

        _va->view_mesh_wire_Act->setEnabled(false);
        _va->view_mesh_flat_Act->setEnabled(false);
        _va->view_mesh_smoo_Act->setEnabled(false);
        _va->view_mesh_tex_Act->setEnabled(false);

        _va->fbtn_vw_wr->setEnabled(false);
        _va->fbtn_vw_tx->setEnabled(false);
        _va->fbtn_vw_fl->setEnabled(false);
        _va->fbtn_vw_sm->setEnabled(false);
    }
    else
    {
        _va->view_mesh_wire_Act->setEnabled(true);
        _va->view_mesh_flat_Act->setEnabled(true);
        _va->view_mesh_smoo_Act->setEnabled(true);

        _va->fbtn_vw_wr->setEnabled(true);
        _va->fbtn_vw_fl->setEnabled(true);
        _va->fbtn_vw_sm->setEnabled(true);

            QString msg = sbInfo.arg(_mesh->get_tri_mesh()->_triangles.size()/1000);
            _va->_main_window->setStatusBarInfo(msg);

            if(_mesh->get_tri_mesh()->_texture == NULL)
            {
                _va->view_mesh_tex_Act->setEnabled(false);
                _va->fbtn_vw_tx->setEnabled(false);
            }
            else
            {
                _va->view_mesh_tex_Act->setEnabled(true);
                _va->fbtn_vw_tx->setEnabled(true);
            }
    }

    if(_vmode == RAW_POINTS)
       _va->view_raw_point_Act->setChecked(true);
    else if ((_vmode == MESH) && (_mesh != NULL) )
    {
        CTriangularMesh::RenderingMode rm = _mesh->get_rendering_mode();
        switch(rm)
        {
            case CTriangularMesh::MESH_WIRE:
                _va->view_mesh_wire_Act->setChecked(true);
                _va->fbtn_vw_wr->setChecked(true);
                _va->fbtn_vw_fl->setChecked(false);
                _va->fbtn_vw_sm->setChecked(false);
                _va->fbtn_vw_tx->setChecked(false);
                break;
            case CTriangularMesh::MESH_FLAT:
                _va->view_mesh_flat_Act->setChecked(true);
                _va->fbtn_vw_wr->setChecked(false);
                _va->fbtn_vw_fl->setChecked(true);
                _va->fbtn_vw_sm->setChecked(false);
                _va->fbtn_vw_tx->setChecked(false);
                break;
            case CTriangularMesh::MESH_TEX:
                _va->view_mesh_tex_Act->setChecked(true);
                _va->fbtn_vw_wr->setChecked(false);
                _va->fbtn_vw_fl->setChecked(false);
                _va->fbtn_vw_sm->setChecked(false);
                _va->fbtn_vw_tx->setChecked(true);
                break;
            default:
            case CTriangularMesh::MESH_SMOOTH:
                _va->view_mesh_smoo_Act->setChecked(true);
                _va->fbtn_vw_wr->setChecked(false);
                _va->fbtn_vw_fl->setChecked(false);
                _va->fbtn_vw_sm->setChecked(true);
                _va->fbtn_vw_tx->setChecked(false);
                break;
        }
    }

    _va->_main_window->updateFieldButton();
}

*/
