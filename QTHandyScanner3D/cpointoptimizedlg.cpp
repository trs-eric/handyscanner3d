#include "cpointoptimizedlg.h"
#include "ui_cpointoptimizedlg.h"
#include <QMessageBox>
#include "cglwidget.h"
#include "cpointoptimizethread.h"
#include "messages.h"
#include "settings/psettings.h"

int slider_max = 28;

CPointOptimizeDlg::CPointOptimizeDlg(CGLWidget * glWidget, QWidget *parent) :
   _glWidget(glWidget), QDialog(parent),
    ui(new Ui::CPointOptimizeDlg), _min_dist(0.1f), _max_dist(1.5f), _on_apply(false), _in_process(false),
    _thread(NULL), _is_result_valid(false)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);

    ui->statistics->setText(msg_PointsBeforeAfter);

    _mdist_value = 0.15;//_max_dist/2;
    CGlobalSettings::_min_opt_dist = _mdist_value ;

    ui->mdistEdit->setText(QString::number(_mdist_value));

    ui->mdistSlider->setMinimum(0);
    ui->mdistSlider->setMaximum(slider_max );
//    ui->mdistSlider->set
    ui->progressBar->setMaximum(100);

    float slid_value = slider_max * (_mdist_value - _min_dist)/ (_max_dist - _min_dist);

    ui->mdistSlider->setValue(slid_value);

    connect(ui->previewBtn, SIGNAL(clicked()), this, SLOT(onPreviewBtn()));
    connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));

    connect(ui->mdistSlider, SIGNAL(valueChanged(int)),   this, SLOT(slider2edit()));
    connect(ui->mdistEdit,   SIGNAL(textEdited(QString)), this, SLOT(edit2slider(QString)));


    _thread = new CPointOptimizeThread;

    // user pressed cancel button
    connect(ui->cancelBtn,  SIGNAL(clicked()), _thread, SLOT(stop_process()));
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

    // user closed dialg
    connect(this,  SIGNAL(finished(int)), _thread, SLOT(stop_process()));
    //connect(this,  SIGNAL(finished(int)), this, SLOT(onCancelBtn()));

    // working with a thread
    connect(_thread, SIGNAL(send_back(int)), this, SLOT(onMakeProgressStep(int)));
    connect(_thread, SIGNAL(send_result(CPointCloud*)), this, SLOT(onCompleteProcessing(CPointCloud*)));
    connect(_thread, SIGNAL(send_result(CPointCloud*)), _glWidget, SLOT(on_optim_pnts_computed(CPointCloud*)));
    connect(_thread, SIGNAL(finished()), this, SLOT(onStopProcessing()));
}

void CPointOptimizeDlg::onMakeProgressStep(int val)
{
    ui->progressBar->setValue(val);
}

void CPointOptimizeDlg::onCompleteProcessing(CPointCloud * pcld)
{
    if(_on_apply)
    {
        close_dialog();
        return;
    }

    _is_result_valid = true;
    ui->progressBar->setValue(100);
    if (pcld)
        ui->pointsA->setText(QString::number(pcld->get_points_count()));

}

void CPointOptimizeDlg::onStopProcessing()
{
    ui->applyBtn->setEnabled(true);
    ui->previewBtn->setEnabled(true);
    ui->mdistEdit->setEnabled(true);
    ui->mdistSlider->setEnabled(true);

    _in_process = false;
}
void CPointOptimizeDlg::slider2edit()
{
    _is_result_valid = false;

    _mdist_value  =  _min_dist + (_max_dist - _min_dist) * float(ui->mdistSlider->value()) / float(slider_max);
    ui->mdistEdit->setText(QString::number(_mdist_value));
}

void CPointOptimizeDlg::edit2slider(QString  str)
{
    _is_result_valid = false;

    bool ok = false;
    float cval = str.toFloat(&ok);
    if(ok)
    {
        float slid_value;
        if(cval >= _min_dist && cval <= _max_dist)
        {
            _mdist_value = cval;
            slid_value = float(slider_max) * (_mdist_value - _min_dist)/ (_max_dist - _min_dist);
        }
        else if(cval < _min_dist )
        {
            (cval < 0)? _mdist_value = 0 : _mdist_value = cval;
            ui->mdistEdit->setText(QString::number(_mdist_value));
            slid_value=0;
        }
        else if(cval > _max_dist)
        {
            _mdist_value = cval;
            slid_value=slider_max;
        }
        bool block = ui->mdistSlider->blockSignals(true);
        ui->mdistSlider->setValue(slid_value);
        ui->mdistSlider->blockSignals(block);
    }
}

CPointOptimizeDlg::~CPointOptimizeDlg()
{
    delete ui;
    delete _thread;
}

void CPointOptimizeDlg::close_dialog()
{
    _on_apply = false;
    close();
}
void CPointOptimizeDlg::onCancelBtn()
{
    if(!_in_process)
    {
        //_glWidget->discard_optim_points_of_current_object();
        close_dialog();
    }
    _in_process = false;
    _on_apply = false;
    ui->pointsA->setText(QString::number(0));
}

void CPointOptimizeDlg::onPreviewBtn()
{
    CPointCloud * pcld = _glWidget->get_raw_points_of_current_object();
    if(pcld == NULL)
    {
       QMessageBox::warning(this, tr("Warning"),wrn_NothingOptim);
       return;
    }

    ui->applyBtn    ->setEnabled(false);
    ui->previewBtn  ->setEnabled(false);
    ui->mdistEdit   ->setEnabled(false);
    ui->mdistSlider ->setEnabled(false);

    _in_process = true;

    CGlobalSettings::_min_opt_dist = _mdist_value;
    _thread->start_process(pcld, _mdist_value);

}

void CPointOptimizeDlg::onApplyBtn()
{
    _on_apply = true;
    if(!_is_result_valid)
        onPreviewBtn();
    else
        close_dialog();
}

void CPointOptimizeDlg::showEvent(QShowEvent * event)
{

    CPointCloud * pcld = _glWidget->get_raw_points_of_current_object();
    if (pcld)
        ui->pointsB->setText(QString::number(pcld->get_points_count()));
    else
        ui->pointsB->setText(QString::number(0));

    ui->progressBar->setValue(0);
    ui->pointsA->setText(QString::number(0));
    _is_result_valid    = false;
    _on_apply           = false;
    _in_process         = false;


}


