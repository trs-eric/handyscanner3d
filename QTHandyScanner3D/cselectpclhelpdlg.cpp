#include "cselectpclhelpdlg.h"
#include "ui_cselectpclhelpdlg.h"
#include "cglwidget.h"
CSelectPCLHelpDlg::CSelectPCLHelpDlg(CGLWidget *glWidget , QWidget *parent) :
  _glWidget (glWidget),  QDialog(parent),
    ui(new Ui::CSelectPCLHelpDlg)
{
     Qt::WindowFlags flags = windowFlags();
     Qt::WindowFlags helpFlag = Qt::WindowContextHelpButtonHint;
     flags = flags & (~helpFlag);
     setWindowFlags(flags);


    ui->setupUi(this);
}


CSelectPCLHelpDlg::~CSelectPCLHelpDlg()
{
    delete ui;
}


void CSelectPCLHelpDlg::hideEvent(QHideEvent * e)
{
    _glWidget->set_view_scene_mode(CGLWidget::DEF_VIEW_MODE);
}
