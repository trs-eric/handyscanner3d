#ifndef CINPUTDATAMANAGER_H
#define CINPUTDATAMANAGER_H

#include <map>
#include "cframe.h"

class CInputDataManager
{
public:
    CInputDataManager();

    CFrame * get_frame_with_number(int idx);

    void init_euler_angles_map();

protected:
    // auxulary map - img_idx to 3 euler angles

    std::map<int, cv::Mat> _euler_angles_map;
};


#endif // CINPUTDATAMANAGER_H
