#include "SURFFeatureMatcher.h"

#include "FindCameraMatrices.h"
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/nonfree/nonfree.hpp>

#include <iostream>
#include <set>
using namespace std;
using namespace cv;

SURFFeatureMatcher::SURFFeatureMatcher(std::vector<cv::Mat>& imgs_,
                                       std::vector<std::vector<cv::KeyPoint> >& imgpts_) :
    imgpts(imgpts_), imgs(imgs_)
{
    detector =  new SurfFeatureDetector (150); //FeatureDetector::create("SURF"); // def is 50
    extractor = DescriptorExtractor::create("SURF");

    std::cout << " -------------------- extract feature points for all images -------------------\n";

    detector->detect(imgs, imgpts);
    extractor->compute(imgs, imgpts, descriptors);
}


void SURFFeatureMatcher::MatchFeatures(int idx_i, int idx_j, vector<DMatch>* matches)
{
#ifdef __SFM__DEBUG__
    const Mat& img_1 = imgs[idx_i];
    const Mat& img_2 = imgs[idx_j];
#endif

    const vector<KeyPoint>& imgpts1 = imgpts[idx_i];
    const vector<KeyPoint>& imgpts2 = imgpts[idx_j];
    const Mat& descriptors_1 = descriptors[idx_i];
    const Mat& descriptors_2 = descriptors[idx_j];

    std::vector< DMatch > good_matches_,very_good_matches_;
    std::vector<KeyPoint> keypoints_1, keypoints_2;

    cout << "imgpts1 has " << imgpts1.size() << " points (descriptors " << descriptors_1.rows << ")" << endl;
    cout << "imgpts2 has " << imgpts2.size() << " points (descriptors " << descriptors_2.rows << ")" << endl;

    keypoints_1 = imgpts1;
    keypoints_2 = imgpts2;

    if(descriptors_1.empty()) {
        CV_Error(0,"descriptors_1 is empty");
    }
    if(descriptors_2.empty()) {
        CV_Error(0,"descriptors_2 is empty");
    }


    cout << "step 2" << endl;

    //matching descriptor vectors using Brute Force matcher
    BFMatcher matcher;
    std::vector< DMatch > matches_;
    if (matches == NULL) {
        matches = &matches_;
    }
    if (matches->size() == 0) {
        cout << "match " << descriptors_1.rows << " vs. " << descriptors_2.rows << " ...";

        if(true) {
            vector<vector<DMatch> > knn_matches;
            Mat trainIdx,distance,allDist;
            //CV_PROFILE("match",
               // matcher.knnMatchSingle(descriptors_1,descriptors_2,trainIdx,distance,allDist,2);
               // matcher.knnMatchDownload(trainIdx,distance,knn_matches);
                       matcher.knnMatch(descriptors_1, descriptors_2, knn_matches,2);

            // )

            (*matches).clear();

            //ratio test
            double ratioT = 0.7;
            for(int i=0;i<knn_matches.size();i++) {
                if(knn_matches[i][0].distance / knn_matches[i][1].distance < ratioT) {
                    (*matches).push_back(knn_matches[i][0]);
                }
            }
            cout << "kept " << (*matches).size() << " features after ratio test with T=" << ratioT << endl;
        } else {
            CV_PROFILE("match",matcher.match( descriptors_1, descriptors_2, *matches );)
        }
    }


}
