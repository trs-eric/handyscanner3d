#include "c3dcorrector.h"
#include <fstream>

C3DCorrector::C3DCorrector()
{
}

void C3DCorrector::save_calib_data(QString fname)
{
    std::ofstream ofile;
    ofile.open(fname.toLocal8Bit().constData(), std::ios::out | std::ios::binary);

    ofile.write(reinterpret_cast<const char *>(&_xs), sizeof(int));
    ofile.write(reinterpret_cast<const char *>(&_ys), sizeof(int));

    int cnum = _corners.size();
    ofile.write(reinterpret_cast<const char *>(&cnum), sizeof(int));

    std::vector<cv::Point2f>::iterator itc = _corners.begin();
    for(; itc != _corners.end(); itc++)
    {
        float x = itc->x;
        ofile.write(reinterpret_cast<const char *>(&x), sizeof(float));
        float y = itc->y;
        ofile.write(reinterpret_cast<const char *>(&y), sizeof(float));
    }

    int bcnum = _ext_corners.size();
    ofile.write(reinterpret_cast<const char *>(&bcnum), sizeof(int));

    std::vector<board_corners>::iterator ite = _ext_corners.begin();
    for(; ite != _ext_corners.end(); ite++)
    {
        float x00 = ite->x00;
        ofile.write(reinterpret_cast<const char *>(&x00), sizeof(float));
        float y00 = ite->y00;
        ofile.write(reinterpret_cast<const char *>(&y00), sizeof(float));

        float x01 = ite->x01;
        ofile.write(reinterpret_cast<const char *>(&x01), sizeof(float));
        float y01 = ite->y01;
        ofile.write(reinterpret_cast<const char *>(&y01), sizeof(float));

        float x11 = ite->x11;
        ofile.write(reinterpret_cast<const char *>(&x11), sizeof(float));
        float y11 = ite->y11;
        ofile.write(reinterpret_cast<const char *>(&y11), sizeof(float));

        float x10 = ite->x10;
        ofile.write(reinterpret_cast<const char *>(&x10), sizeof(float));
        float y10 = ite->y10;
        ofile.write(reinterpret_cast<const char *>(&y10), sizeof(float));

        float pA  = ite->pA;
        ofile.write(reinterpret_cast<const char *>(&pA), sizeof(float));
        float pB = ite->pB;
        ofile.write(reinterpret_cast<const char *>(&pB), sizeof(float));
        float pC  = ite->pC;
        ofile.write(reinterpret_cast<const char *>(&pC), sizeof(float));
        float pD  = ite->pD;
        ofile.write(reinterpret_cast<const char *>(&pD), sizeof(float));


    }

//    CArray2D<cv::Point3f> deform_fin(marks_x, marks_y);
    int dfsz = _deform_map.size();
    ofile.write(reinterpret_cast<const char *>(&dfsz), sizeof(int));

    for (int d = 0; d <dfsz ; d++ )
    {
        for (int yy = 0; yy < _ys; yy ++)
            for (int xx = 0; xx< _xs; xx++)
            {
                    float def_x =  _deform_map[d]._data[yy][xx].x;
                    float def_y =  _deform_map[d]._data[yy][xx].y;
                    float def_z =  _deform_map[d]._data[yy][xx].z;

                    ofile.write(reinterpret_cast<const char *>(&def_x), sizeof(float));
                    ofile.write(reinterpret_cast<const char *>(&def_y), sizeof(float));
                    ofile.write(reinterpret_cast<const char *>(&def_z), sizeof(float));
            }
    }


    ofile.close();
}

void C3DCorrector::load_calib_data(QString fname)
{
    std::ifstream ifile;
    ifile.open(fname.toLocal8Bit().constData(), std::ios::in | std::ios::binary);
    if(!ifile.is_open()) return false;


    ifile.read(reinterpret_cast<char *>(&_xs), sizeof(int));
    ifile.read(reinterpret_cast<char *>(&_ys), sizeof(int));

     std::cout << _xs << std::endl;
     std::cout << _ys << std::endl;

    int cnum = 0;
    ifile.read(reinterpret_cast<char *>(&cnum), sizeof(int));
    _corners.clear();
    for(int c =0; c < cnum ; c++)
    {
        float x = 0;
        ifile.read(reinterpret_cast<char *>(&x), sizeof(float));
        float y = 0;
        ifile.read(reinterpret_cast<char *>(&y), sizeof(float));

        cv::Point2f pp;
        pp.x =x;
        pp.y = y;

        _corners.push_back(pp);


        // std::cout << pp.x << " " << pp.y << std::endl;

    }

    int bcnum = 0;
    ifile.read(reinterpret_cast<char *>(&bcnum), sizeof(int));
    _ext_corners.clear();
    for(int b =0; b < bcnum ; b++)
    {
        board_corners tbc;

        ifile.read(reinterpret_cast<const char *>(&tbc.x00), sizeof(float));
        ifile.read(reinterpret_cast<const char *>(&tbc.y00), sizeof(float));
        ifile.read(reinterpret_cast<const char *>(&tbc.x01), sizeof(float));
        ifile.read(reinterpret_cast<const char *>(&tbc.y01), sizeof(float));
        ifile.read(reinterpret_cast<const char *>(&tbc.x11), sizeof(float));
        ifile.read(reinterpret_cast<const char *>(&tbc.y11), sizeof(float));
        ifile.read(reinterpret_cast<const char *>(&tbc.x10), sizeof(float));
        ifile.read(reinterpret_cast<const char *>(&tbc.y10), sizeof(float));


        ifile.read(reinterpret_cast<const char *>(&tbc.pA), sizeof(float));
        ifile.read(reinterpret_cast<const char *>(&tbc.pB), sizeof(float));
        ifile.read(reinterpret_cast<const char *>(&tbc.pC), sizeof(float));
        ifile.read(reinterpret_cast<const char *>(&tbc.pD), sizeof(float));


        _ext_corners.push_back(tbc);
/*
        std::cout << tbc.x00 << " " << tbc.y00<< std::endl;
        std::cout << tbc.x01 << " " << tbc.y01<< std::endl;
        std::cout << tbc.x11 << " " << tbc.y11<< std::endl;
        std::cout << tbc.x10 << " " << tbc.y10<< std::endl;


        std::cout << tbc.pA <<  std::endl;
        std::cout << tbc.pB <<  std::endl;
        std::cout << tbc.pC <<  std::endl;
        std::cout << tbc.pD <<  std::endl;*/

    }



    int dfsz = 0;
    ifile.read(reinterpret_cast< char *>(&dfsz), sizeof(int));
    _deform_map.clear();

    for (int d = 0; d < dfsz ; d++ )
    {
        std::cout << d << std::endl;
        CArray2D<cv::Point3f> lay(_xs,_ys);
        for (int yy = 0; yy < _ys; yy ++)
        {
            for (int xx = 0; xx< _xs; xx++)
            {
                    float def_x =  0;
                    float def_y =  0;
                    float def_z =  0;

                    ifile.read(reinterpret_cast<char *>(&def_x), sizeof(float));
                    ifile.read(reinterpret_cast<char *>(&def_y), sizeof(float));
                    ifile.read(reinterpret_cast<char *>(&def_z), sizeof(float));

                    lay._data[yy][xx].x = def_x;
                    lay._data[yy][xx].y = def_y;
                    lay._data[yy][xx].z = def_z;


                   // std::cout << yy << " " << xx << " " << def_x << " " << def_y << " " << def_z << std::endl;

            }
        }
        _deform_map.push_back(lay);
    }


    std::cout << "done with  deform reading" << std::endl;

    ifile.close();

}


void C3DCorrector::set_grid_sz(int xs, int ys)
{
    _xs = xs;
    _ys = ys;
}

/*void C3DCorrector::set_up_lblmap(CArray2D<int> lbl)
{
    _lbl_map = lbl;
}*/

void C3DCorrector::set_up_corners(std::vector<cv::Point2f> c)
{
    _corners = c;
}

/*void  C3DCorrector::set_up_board_plain(float A, float B, float C, float D)
{
    _pA = A;
    _pB = B;
    _pC = C;
    _pD = D;
}*/

void C3DCorrector::set_up_correction_level(CArray2D<cv::Point3f> v, int k )
{
    _xs = v._xs;
    _ys = v._ys;

    _deform_map.push_back(v);
}


void C3DCorrector::compute_parametric(float x, float y,
                                      float & px, float & py, int k)
{
   /* std::cout << "BC " << std::endl;
    std::cout << _ext_corners[k].x00 << " " << _ext_corners[k].y00 << std::endl;
    std::cout << _ext_corners[k].x10 << " " << _ext_corners[k].y10 << std::endl;
    std::cout << _ext_corners[k].x11 << " " << _ext_corners[k].y11 << std::endl;
    std::cout << _ext_corners[k].x01 << " " << _ext_corners[k].y01 << std::endl;
*/

    float A1 = _ext_corners[k].x10 - _ext_corners[k].x00;
    float B1 = _ext_corners[k].x11 - _ext_corners[k].x01 - _ext_corners[k].x10 + _ext_corners[k].x00;
    float C1 = _ext_corners[k].x01 - _ext_corners[k].x00;
    float D1 = x - _ext_corners[k].x00;

  //  std::cout << A1 << " " << B1 << " " << C1  << " " << D1  << std::endl;


    float A2 = _ext_corners[k].y10 - _ext_corners[k].y00;
    float B2 = _ext_corners[k].y11 - _ext_corners[k].y01 - _ext_corners[k].y10 + _ext_corners[k].y00;
    float C2 = _ext_corners[k].y01 - _ext_corners[k].y00;
    float D2 = y - _ext_corners[k].y00;

    //std::cout << A2 << " " << B2 << " " << C2  << " " << D2  << std::endl;

    float alpha = B1 * C2 - B2 * C1;
    float betta = C2 * A1 - A2 * C1 + B2 * D1 - D2 * B1;
    float gamma = A2 * D1 - D2 * A1;

    float Desc = betta * betta - 4 * alpha * gamma;

    ///// CHECK THIS  + or -
    py = (- betta - std::sqrt(Desc) )/ (2 * alpha);
    px = (D1 - C1 * py) /(A1 + B1 * py);

}

void C3DCorrector::set_ext_corners( board_corners  bc)
{
    _ext_corners.push_back(bc);
}

void C3DCorrector::do_correction2d(float & x, float & y, float & z)
{
    float deform_x = 0;
    float deform_y = 0;
    float deform_z = 0;

    do_correction2d_aux(x, y, z, deform_x, deform_y, deform_z, 0);

    x += deform_x;
    y += deform_y;
    z += deform_z;
}


void C3DCorrector::do_correction2d_aux(float & x, float & y, float & z,
                                       float & deform_x, float & deform_y, float & deform_z, int k)
{

    // parametric point coords [0,1]
    float px = 0;
    float py = 0;

    /////////////////// CHECK THIS !!!! is it X and Z
    compute_parametric(x, z, px, py, k);

   /// std::cout << px << " " << py << " " << x << " " << y << " " <<z << std::endl;

    float spx = px * (_xs - 1) ; // 0 ... (mark_x -1)
    float spy = py * (_ys - 1) ; // 0 ... (mark_y -1)

    int x00 = spx; // round down to nearest integer
    if(x00 < 0)       x00 = 0;
    if(x00 > _xs - 2) x00 = _xs - 2;

    int y00 = spy; // round down to nearest integer
    if(y00 < 0)       y00 = 0;
    if(y00  > _ys - 2) y00 = _ys - 2;

    float wx0 = spx - float(x00);
    if(wx0 < 0 ) wx0 = 0;
    if(wx0 > 1 ) wx0 = 1;
    float wx1 = 1 - wx0;

    float wy0 = spy - float(y00);
    if(wy0 < 0 ) wy0 = 0;
    if(wy0 > 1 ) wy0 = 1;
    float wy1 = 1 - wy0;

    float w00 = wy1 * wx1;
    float w01 = wy1 * wx0;
    float w10 = wy0 * wx1;
    float w11 = wy0 * wx0;

    deform_x =  w00 * _deform_map[k]._data[y00][x00].x +
                w01 * _deform_map[k]._data[y00][x00 + 1].x  +
                w10 * _deform_map[k]._data[y00 + 1][x00 ].x +
                w11 * _deform_map[k]._data[y00 + 1][x00 + 1].x ;

    deform_y =  w00 * _deform_map[k]._data[y00][x00].y +
                w01 * _deform_map[k]._data[y00][x00 + 1].y  +
                w10 * _deform_map[k]._data[y00 + 1][x00 ].y +
                w11 * _deform_map[k]._data[y00 + 1][x00 + 1].y ;

    deform_z =  w00 * _deform_map[k]._data[y00][x00].z +
                w01 * _deform_map[k]._data[y00][x00 + 1].z  +
                w10 * _deform_map[k]._data[y00 + 1][x00 ].z +
                w11 * _deform_map[k]._data[y00 + 1][x00 + 1].z ;

}

void C3DCorrector::do_correction3d(float & x, float & y, float & z)
{


    //// find proper layers
    int k = 0;
    int layers = _ext_corners.size();
    for(; k < layers ; k++)
    {

        float plain_y = -( _ext_corners[k].pA * x +
                           _ext_corners[k].pC * z +
                           _ext_corners[k].pD)/ _ext_corners[k].pB;

        if(plain_y < y )
            break;
    }

    if((k == 0) || (k == layers))
    {

        /// point is before or after correction plains
        /// use single deform layer
        if(k == layers) k--;

        float deform_x = 0;
        float deform_y = 0;
        float deform_z = 0;

        do_correction2d_aux(x, y, z, deform_x, deform_y, deform_z, k);

        x += deform_x;
        y += deform_y;
        z += deform_z;
    }
    else
    {

        /// the point is inside correction cube

        int lay_far_idx  = k-1;
        int lay_near_idx = k;


        float plain_y1 = -( _ext_corners[lay_far_idx].pA * x + _ext_corners[lay_far_idx].pC * z +
                           _ext_corners[lay_far_idx].pD)/ _ext_corners[lay_far_idx].pB;

        float plain_y2 = -( _ext_corners[lay_near_idx].pA * x + _ext_corners[lay_near_idx].pC * z +
                           _ext_corners[lay_near_idx].pD)/ _ext_corners[lay_near_idx].pB;


        float wei_y = (1.0 - fabs((y -  plain_y1 )/(plain_y1 - plain_y2)));

        ////std::cout << wei_y << " " <<  lay_far_idx << " " << lay_near_idx << std::endl;


        float deform_x1 = 0;
        float deform_y1 = 0;
        float deform_z1 = 0;
        do_correction2d_aux(x, y, z, deform_x1, deform_y1, deform_z1, lay_far_idx);

        float deform_x2 = 0;
        float deform_y2 = 0;
        float deform_z2 = 0;
        do_correction2d_aux(x, y, z, deform_x2, deform_y2, deform_z2, lay_near_idx);


        float deform_x = deform_x1 * wei_y + deform_x2 * (1.0 - wei_y);
        float deform_y = deform_y1 * wei_y + deform_y2 * (1.0 - wei_y);
        float deform_z = deform_z1 * wei_y + deform_z2 * (1.0 - wei_y);


        x += deform_x;
        y += deform_y;
        z += deform_z;


    }
}

/*void C3DCorrector::do_correction(float & x, float & y, float & z, float u, float v)
{

   int lidx = _lbl_map._data[int(v+0.5)][int(u+0.5)];
   int x00 = lidx % _xs;
   int y00 = lidx / _xs;

   cv::Point2f c00 =  _corners[lidx];
   cv::Point2f c01 =  _corners[lidx + 1];
   cv::Point2f c10 =  _corners[lidx +     _xs];
   cv::Point2f c11 =  _corners[lidx + 1 + _xs];

   float dx = c01.x - c00.x;
   float wx0 = (float(u) - c00.x)/dx;
   if(wx0 < 0 ) wx0 = 0;
   if(wx0 > 1 ) wx0 = 1;
   float wx1 = 1 - wx0;

   float dy = c10.y - c00.y;
   float wy0 = (float(v) - c00.y) / dy;
   if(wy0 < 0 ) wy0 = 0;
   if(wy0 > 1 ) wy0 = 1;
   float wy1 = 1 - wy0;

   float w00 = wy1 * wx1;
   float w01 = wy1 * wx0;
   float w10 = wy0 * wx1;
   float w11 = wy0 * wx0;

   float deform_x = w00 * _deflay._data[y00][x00].x +
                    w01 * _deflay._data[y00][x00 + 1].x  +
                    w10 * _deflay._data[y00 + 1][x00 ].x +
                    w11 * _deflay._data[y00 + 1][x00 + 1].x ;

   float deform_y = w00 * _deflay._data[y00][x00].y +
                    w01 * _deflay._data[y00][x00 + 1].y  +
                    w10 * _deflay._data[y00 + 1][x00 ].y +
                    w11 * _deflay._data[y00 + 1][x00 + 1].y ;

   float deform_z = w00 * _deflay._data[y00][x00].z +
                    w01 * _deflay._data[y00][x00 + 1].z  +
                    w10 * _deflay._data[y00 + 1][x00 ].z +
                    w11 * _deflay._data[y00 + 1][x00 + 1].z ;

   x += deform_x;
   y += deform_y;
   z += deform_z;
}*/
