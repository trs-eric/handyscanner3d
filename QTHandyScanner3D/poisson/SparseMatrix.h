/*
Copyright (c) 2006, Michael Kazhdan and Matthew Bolitho
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of
conditions and the following disclaimer. Redistributions in binary form must reproduce
the above copyright notice, this list of conditions and the following disclaimer
in the documentation and/or other materials provided with the distribution. 

Neither the name of the Johns Hopkins University nor the names of its contributors
may be used to endorse or promote products derived from this software without specific
prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES 
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.
*/

#ifndef __SPARSEMATRIX_HPP
#define __SPARSEMATRIX_HPP

#include "Vector.h"
#include "Allocator.h"

template <class T>
struct MatrixEntry
{
	MatrixEntry( void )		{ N =-1; Value = 0; }
	MatrixEntry( int i )	{ N = i; Value = 0; }
	int N;
	T Value;
};
template <class T,int Dim>
struct NMatrixEntry
{
	NMatrixEntry( void )		{ N =-1; memset(Value,0,sizeof(T)*Dim); }
	NMatrixEntry( int i )	{ N = i; memset(Value,0,sizeof(T)*Dim); }
	int N;
	T Value[Dim];
};

template<class T> class SparseMatrix
{
private:
	static int UseAlloc;
public:
	static Allocator<MatrixEntry<T> > Allocator;
	static int UseAllocator(void);
	static void SetAllocator(const int& blockSize);

	int rows;
	int* rowSizes;
	MatrixEntry<T>** m_ppElements;

	SparseMatrix();
	SparseMatrix( int rows );
	void Resize( int rows );
	void SetRowSize( int row , int count );
	int Entries(void);

	SparseMatrix( const SparseMatrix& M );
	~SparseMatrix();

	void SetZero();
	void SetIdentity();

	SparseMatrix<T>& operator = (const SparseMatrix<T>& M);

	SparseMatrix<T> operator * (const T& V) const;
	SparseMatrix<T>& operator *= (const T& V);


	SparseMatrix<T> operator * (const SparseMatrix<T>& M) const;
	SparseMatrix<T> Multiply( const SparseMatrix<T>& M ) const;
	SparseMatrix<T> MultiplyTranspose( const SparseMatrix<T>& Mt ) const;

	template<class T2>
	Vector<T2> operator * (const Vector<T2>& V) const
	{
		return Multiply(V);
	}

	template<class T2>
	Vector<T2> Multiply( const Vector<T2>& V ) const;
	template<class T2>
	void Multiply( const Vector<T2>& In, Vector<T2>& Out ) const
	{
	for (int i=0; i<rows; i++){
		T2 temp=T2();
		for(int j=0;j<rowSizes[i];j++){temp+=m_ppElements[i][j].Value * In.m_pV[m_ppElements[i][j].N];}
		Out.m_pV[i]=temp;
	}
	}


	SparseMatrix<T> Transpose() const;

	static int Solve			(const SparseMatrix<T>& M,const Vector<T>& b,const int& iters,Vector<T>& solution,const T eps=1e-8);

	template<class T2>
	static int SolveSymmetric	(const SparseMatrix<T>& M,const Vector<T2>& b,const int& iters,Vector<T2>& solution,const T2 eps=1e-8,const int& reset=1)
	{	Vector<T2> d,r,Md;
	T2 alpha,beta,rDotR;
	Md.Resize(b.Dimensions());
	//if(reset){
		solution.Resize(b.Dimensions());
		solution.SetZero();
	//}
	d=r=b-M.Multiply(solution);
	rDotR=r.Dot(r);
	if(b.Dot(b)<=eps){
		solution.SetZero();
		return 0;
	}

	int i;
	for(i=0;i<iters;i++){
		T2 temp;
		M.Multiply(d,Md);
		temp=d.Dot(Md);
		if(temp<=eps){break;}
		alpha=rDotR/temp;
		r.SubtractScaled(Md,alpha);
		temp=r.Dot(r);
		if(temp/b.Dot(b)<=eps){break;}
		beta=temp/rDotR;
		solution.AddScaled(d,alpha);
		if(beta<=eps){break;}
		rDotR=temp;
		Vector<T2>::Add(d,beta,r,d);
	}
	return i;
	}


};
template<class T,int Dim> class SparseNMatrix
{
private:
	static int UseAlloc;
public:
	static Allocator<NMatrixEntry<T,Dim> > Allocator;
	static int UseAllocator(void);
	static void SetAllocator(const int& blockSize);

	int rows;
	int* rowSizes;
	NMatrixEntry<T,Dim>** m_ppElements;

	SparseNMatrix();
	SparseNMatrix( int rows );
	void Resize( int rows );
	void SetRowSize( int row , int count );
	int Entries(void);

	SparseNMatrix( const SparseNMatrix& M );
	~SparseNMatrix();

	SparseNMatrix& operator = (const SparseNMatrix& M);

	SparseNMatrix  operator *  (const T& V) const;
	SparseNMatrix& operator *= (const T& V);

	template<class T2>
	NVector<T2,Dim> operator * (const Vector<T2>& V) const
	{
	NVector<T2,Dim> R( rows );
	
	for (int i=0; i<rows; i++)
	{
		T2 temp[Dim];
		for(int ii=0;ii<Dim;ii++){temp[ii]=T2();}
		for(int ii=0;ii<rowSizes[i];ii++){
			for(int jj=0;jj<Dim;jj++){temp[jj]+=m_ppElements[i][ii].Value[jj]*V.m_pV[m_ppElements[i][jj].N];}
		}
		for(int ii=0;ii<Dim;ii++){R[i][ii]=temp[ii];}
	}
	return R;
	}

	template<class T2>
	Vector<T2> operator * (const NVector<T2,Dim>& V) const
	{
	Vector<T2> R( rows );
	
	for (int i=0; i<rows; i++)
	{
		T2 temp();
		for(int ii=0;ii<rowSizes[i];ii++){
			for(int jj=0;jj<Dim;jj++){temp+=m_ppElements[i][ii].Value[jj]*V.m_pV[m_ppElements[i][ii].N][jj];}
		}
		R(i)=temp;
	}
	return R;
	}
};



template <class T>
class SparseSymmetricMatrix : public SparseMatrix<T>{
public:

  template<class T2>
	Vector<T2> operator * (const Vector<T2>& V) const{return Multiply(V);}
	template<class T2>
	Vector<T2> Multiply( const Vector<T2>& V ) const
	{
	Vector<T2> R( this->rows );
	
	for (int i=0; i<this->rows; i++){
		for(int ii=0;ii<this->rowSizes[i];ii++){
			int j=this->m_ppElements[i][ii].N;
			R(i)+=this->m_ppElements[i][ii].Value * V.m_pV[j];
			R(j)+=this->m_ppElements[i][ii].Value * V.m_pV[i];
		}
	}
	return R;
	}
	template<class T2>
	void Multiply( const Vector<T2>& In, Vector<T2>& Out ) const
	{
		Out.SetZero();
		for (int i=0; i<this->rows; i++){
		MatrixEntry<T>* temp=this->m_ppElements[i];
		T2& in1=In.m_pV[i];
		T2& out1=Out.m_pV[i];
		int rs=this->rowSizes[i];
		for(int ii=0;ii<rs;ii++){
			MatrixEntry<T>& temp2=temp[ii];
			int j=temp2.N;
			T2 v=temp2.Value;
			out1+=v * In.m_pV[j];
			Out.m_pV[j]+=v * in1;
		}
		}
	}

	template<class T2>
	static int Solve(const SparseSymmetricMatrix<T>& M,const Vector<T2>& b,const int& iters,Vector<T2>& solution,const T2 eps=1e-8,const int& reset=1)
	{
		Vector<T2> d,r,Md;
	T2 alpha,beta,rDotR,bDotB;
	Md.Resize(b.Dimensions());
	if(reset){
		solution.Resize(b.Dimensions());
		solution.SetZero();
	}
	d=r=b-M.Multiply(solution);
	rDotR=r.Dot(r);
	bDotB=b.Dot(b);
	if(b.Dot(b)<=eps){
		solution.SetZero();
		return 0;
	}
	int i;
	for(i=0;i<iters;i++){
		T2 temp;
		M.Multiply(d,Md);
		temp=d.Dot(Md);
		if(fabs(temp)<=eps){break;}
		alpha=rDotR/temp;
		r.SubtractScaled(Md,alpha);
		temp=r.Dot(r);
		if(temp/bDotB<=eps){break;}
		beta=temp/rDotR;
		solution.AddScaled(d,alpha);
		if(beta<=eps){break;}
		rDotR=temp;
		Vector<T2>::Add(d,beta,r,d);
	}
	return i;
	}


	template<class T2>
	static int Solve(const SparseSymmetricMatrix<T>& M,const Vector<T>& diagonal,const Vector<T2>& b,const int& iters,Vector<T2>& solution,const T2 eps=1e-8,const int& reset=1)
	{	Vector<T2> d,r,Md;

	if(reset){
		solution.Resize(b.Dimensions());
		solution.SetZero();
	}
	Md.Resize(M.rows);
	for(int i=0;i<iters;i++){
		M.Multiply(solution,Md);
		r=b-Md;
		for(int j=0;j<int(M.rows);j++){solution[j]+=r[j]/diagonal[j];}
	}
	return iters;
	}
};

#include "SparseMatrix.inl"

#endif

