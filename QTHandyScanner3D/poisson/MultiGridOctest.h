#ifndef __MULTIGRIDOCTEST_H__
#define __MULTIGRIDOCTEST_H__


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>

#define SCALE 1.25

#include <stdarg.h>


#include "MarchingCubes.h"
//#include "Octree.h"
#include "SparseMatrix.h"
//#include "FunctionData.h"
//#include "PPolynomial.h"

//#include "MultiGridOctreeData.h"
typedef float Real;
#include "PoissonParam.h"

namespace vcg
{
typedef bool CallBack( const char * str );
typedef bool CallBackPos(const int pos, const char * str );
}
void ShowUsage(char* ex);
int Execute2(PoissonParam &Par, std::vector<Point3D<Real> > Pts, std::vector<Point3D<Real> > Nor, 
             CoredVectorMeshData &mesh, Point3D<Real> &newCenter, Real &newScale, CMeshConstructionThread *, bool * );

#endif
