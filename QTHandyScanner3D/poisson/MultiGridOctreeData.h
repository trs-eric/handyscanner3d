/*
Copyright (c) 2006, Michael Kazhdan and Matthew Bolitho
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of
conditions and the following disclaimer. Redistributions in binary form must reproduce
the above copyright notice, this list of conditions and the following disclaimer
in the documentation and/or other materials provided with the distribution. 

Neither the name of the Johns Hopkins University nor the names of its contributors
may be used to endorse or promote products derived from this software without specific
prior writften permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES 
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.
*/

#ifndef MULTI_GRID_OCTREE_DATA_INCLUDED
#define MULTI_GRID_OCTREE_DATA_INCLUDED

#include "Hash.h"
#include "SparseMatrix.h"
#include "Octree.h"
#include "FunctionData.h"

typedef float Real;
typedef float FunctionDataReal;
typedef OctNode<class TreeNodeData, float> TreeOctNode;

#ifndef EPSI
#define EPSI
const Real EPSILON=Real(1e-6);
#endif

class RootInfo{
public:
	const TreeOctNode* node;
	int edgeIndex;
	__int64 key;
};

class VertexData{
public:
	static __int64 EdgeIndex(const TreeOctNode* node,const int& eIndex,const int& maxDepth,int index[DIMENSION]);
	static __int64 EdgeIndex(const TreeOctNode* node,const int& eIndex,const int& maxDepth);
	static __int64  FaceIndex(const TreeOctNode* node,const int& fIndex,const int& maxDepth,int index[DIMENSION]);
	static __int64 FaceIndex(const TreeOctNode* node,const int& fIndex,const int& maxDepth);
	static __int64 CornerIndex(const int& depth,const int offSet[DIMENSION],const int& cIndex,const int& maxDepth,int index[DIMENSION]);
	static __int64 CornerIndex(const TreeOctNode* node,const int& cIndex,const int& maxDepth,int index[DIMENSION]);
	static __int64  CornerIndex(const TreeOctNode* node,const int& cIndex,const int& maxDepth);
	static __int64  CenterIndex(const int& depth,const int offSet[DIMENSION],const int& maxDepth,int index[DIMENSION]);
	static __int64 CenterIndex(const TreeOctNode* node,const int& maxDepth,int index[DIMENSION]);
	static __int64 CenterIndex(const TreeOctNode* node,const int& maxDepth);
};
class SortedTreeNodes{
public:
	TreeOctNode** treeNodes;
	int *nodeCount;
	int maxDepth;
	SortedTreeNodes(void);
	~SortedTreeNodes(void);
	void set(TreeOctNode& root,const int& setIndex);
};

class TreeNodeData{
public:
	static int UseIndex;
	union{
		int mcIndex;
		struct{
			int nodeIndex;
			Real centerWeightContribution;
		};
	};
	Real value;

	TreeNodeData(void);
	~TreeNodeData(void);
};

template<int Degree>
class Octree{
public:
	TreeOctNode::NeighborKey neighborKey;	
	TreeOctNode::NeighborKey2 neighborKey2;

	Real radius;
	int width;
	void setNodeIndices(TreeOctNode& tree,int& idx);
	Real GetDotProduct(const int index[DIMENSION]) const;
	Real GetLaplacian(const int index[DIMENSION]) const;
	Real GetDivergence(const int index[DIMENSION],const Point3D<Real>& normal) const;

	class DivergenceFunction{
	public:
		Point3D<Real> normal;
		Octree<Degree>* ot;
		int index[DIMENSION],scratch[DIMENSION];
		void Function(TreeOctNode* node1,const TreeOctNode* node2)
			{
	Point3D<Real> n=normal;
	if(FunctionData<Degree,Real>::SymmetricIndex(index[0],int(node1->off[0]),scratch[0])){n.coords[0]=-n.coords[0];}
	if(FunctionData<Degree,Real>::SymmetricIndex(index[1],int(node1->off[1]),scratch[1])){n.coords[1]=-n.coords[1];}
	if(FunctionData<Degree,Real>::SymmetricIndex(index[2],int(node1->off[2]),scratch[2])){n.coords[2]=-n.coords[2];}
	double dot=ot->fData.dotTable[scratch[0]]*ot->fData.dotTable[scratch[1]]*ot->fData.dotTable[scratch[2]];
	node1->nodeData.value+=Real(dot*(ot->fData.dDotTable[scratch[0]]*n.coords[0]+ot->fData.dDotTable[scratch[1]]*n.coords[1]+ot->fData.dDotTable[scratch[2]]*n.coords[2]));
}
	};

	class LaplacianProjectionFunction{
	public:
		double value;
		Octree<Degree>* ot;
		int index[DIMENSION],scratch[DIMENSION];
		void Function(TreeOctNode* node1,const TreeOctNode* node2)
		{
			scratch[0]=FunctionData<Degree,Real>::SymmetricIndex(index[0],int(node1->off[0]));
			scratch[1]=FunctionData<Degree,Real>::SymmetricIndex(index[1],int(node1->off[1]));
			scratch[2]=FunctionData<Degree,Real>::SymmetricIndex(index[2],int(node1->off[2]));
			node1->nodeData.value-=Real(ot->GetLaplacian(scratch)*value);
		}
	};
	class LaplacianMatrixFunction{
	public:
		int x2,y2,z2,d2;
		Octree<Degree>* ot;
		int index[DIMENSION],scratch[DIMENSION];
		int elementCount,offset;
		MatrixEntry<float>* rowElements;
		int Function(const TreeOctNode* node1,const TreeOctNode* node2)
			{
	Real temp;
	int d1=int(node1->d);
	int x1,y1,z1;
	x1=int(node1->off[0]);
	y1=int(node1->off[1]);
	z1=int(node1->off[2]);
	int dDepth=d2-d1;
	int d;
	d=(x2>>dDepth)-x1;
	if(d<0){return 0;}
	if(!dDepth){
		if(!d){
			d=y2-y1;
			if(d<0){return 0;}
			else if(!d){
				d=z2-z1;
				if(d<0){return 0;}
			}
		}
		scratch[0]=FunctionData<Degree,Real>::SymmetricIndex(index[0],x1);
		scratch[1]=FunctionData<Degree,Real>::SymmetricIndex(index[1],y1);
		scratch[2]=FunctionData<Degree,Real>::SymmetricIndex(index[2],z1);
		temp=ot->GetLaplacian(scratch);
		if(node1==node2){temp/=2;}
		if(fabs(temp)>EPSILON){
			rowElements[elementCount].Value=temp;
			rowElements[elementCount].N=node1->nodeData.nodeIndex-offset;
			elementCount++;
		}
		return 0;
	}
	return 1;
}
	};
	class RestrictedLaplacianMatrixFunction{
	public:
		int depth,offset[3];
		Octree<Degree>* ot;
		Real radius;
		int index[DIMENSION],scratch[DIMENSION];
		int elementCount;
		MatrixEntry<float>* rowElements;
		int Function(const TreeOctNode* node1,const TreeOctNode* node2)
		{
	int d1,d2,off1[3],off2[3];
	node1->depthAndOffset(d1,off1);
	node2->depthAndOffset(d2,off2);
	int dDepth=d2-d1;
	int d;
	d=(off2[0]>>dDepth)-off1[0];
	if(d<0){return 0;}

	if(!dDepth){
		if(!d){
			d=off2[1]-off1[1];
			if(d<0){return 0;}
			else if(!d){
				d=off2[2]-off1[2];
				if(d<0){return 0;}
			}
		}
		// Since we are getting the restricted matrix, we don't want to propogate out to terms that don't contribute...
		if(!TreeOctNode::Overlap2(depth,offset,0.5,d1,off1,radius)){return 0;}
		scratch[0]=FunctionData<Degree,Real>::SymmetricIndex(index[0],BinaryNode<Real>::Index(d1,off1[0]));
		scratch[1]=FunctionData<Degree,Real>::SymmetricIndex(index[1],BinaryNode<Real>::Index(d1,off1[1]));
		scratch[2]=FunctionData<Degree,Real>::SymmetricIndex(index[2],BinaryNode<Real>::Index(d1,off1[2]));
		Real temp=ot->GetLaplacian(scratch);
		if(node1==node2){temp/=2;}
		if(fabs(temp)>EPSILON){
			rowElements[elementCount].Value=temp;
			rowElements[elementCount].N=node1->nodeData.nodeIndex;
			elementCount++;
		}
		return 0;
	}
	return 1;
	}
	};

	///////////////////////////
	// Evaluation Functions  //
	///////////////////////////
	class PointIndexValueFunction{
	public:
		int res2;
		FunctionDataReal* valueTables;
		int index[DIMENSION];
		Real value;
		void Function(const TreeOctNode* node)
			{
	int idx[DIMENSION];
	idx[0]=index[0]+int(node->off[0]);
	idx[1]=index[1]+int(node->off[1]);
	idx[2]=index[2]+int(node->off[2]);
	value+=node->nodeData.value*   Real( valueTables[idx[0]]* valueTables[idx[1]]* valueTables[idx[2]]);
	}
	};
	class PointIndexValueAndNormalFunction{
	public:
		int res2;
		FunctionDataReal* valueTables;
		FunctionDataReal* dValueTables;
		Real value;
		Point3D<Real> normal;
		int index[DIMENSION];
		void Function(const TreeOctNode* node)
		{
			int idx[DIMENSION];
			idx[0]=index[0]+int(node->off[0]);
			idx[1]=index[1]+int(node->off[1]);
			idx[2]=index[2]+int(node->off[2]);
			value+=				node->nodeData.value*   Real( valueTables[idx[0]]* valueTables[idx[1]]* valueTables[idx[2]]);
			normal.coords[0]+=	node->nodeData.value*   Real(dValueTables[idx[0]]* valueTables[idx[1]]* valueTables[idx[2]]);
			normal.coords[1]+=	node->nodeData.value*   Real( valueTables[idx[0]]*dValueTables[idx[1]]* valueTables[idx[2]]);
			normal.coords[2]+=	node->nodeData.value*   Real( valueTables[idx[0]]* valueTables[idx[1]]*dValueTables[idx[2]]);
		}

	};

	class AdjacencyCountFunction{
	public:
		int adjacencyCount;
		void Function(const TreeOctNode* node1,const TreeOctNode* node2){adjacencyCount++;}
	};
	class AdjacencySetFunction{
	public:
		int *adjacencies,adjacencyCount;
		void Function(const TreeOctNode* node1,const TreeOctNode* node2){adjacencies[adjacencyCount++]=node1->nodeData.nodeIndex;}
	};

	class RefineFunction{
	public:
		int depth;
		void Function(TreeOctNode* node1,const TreeOctNode* node2)
		{
			if(!node1->children && node1->depth()<depth){node1->initChildren();}
		}
	};
	class FaceEdgesFunction{
	public:
		int fIndex,maxDepth;
		std::vector<std::pair<__int64 ,__int64 > >* edges;
		std::map<__int64 ,std::pair<RootInfo,int> >* vertexCount;
		void Function(const TreeOctNode* node1,const TreeOctNode* node2)
			{
	if(!node1->children && MarchingCubes::HasRoots(node1->nodeData.mcIndex)){
		RootInfo ri1,ri2;
		std::map<__int64 ,std::pair<RootInfo,int> >::iterator iter;
		int isoTri[DIMENSION*MarchingCubes_MAX_TRIANGLES];
		int count=MarchingCubes::AddTriangleIndices(node1->nodeData.mcIndex,isoTri);

		for(int j=0;j<count;j++){
			for(int k=0;k<3;k++){
				if(fIndex==Cube::FaceAdjacentToEdges(isoTri[j*3+k],isoTri[j*3+((k+1)%3)])){
					if(GetRootIndex(node1,isoTri[j*3+k],maxDepth,ri1) && GetRootIndex(node1,isoTri[j*3+((k+1)%3)],maxDepth,ri2)){
						edges->push_back(std::pair<__int64 ,__int64 >(ri2.key,ri1.key));
						iter=vertexCount->find(ri1.key);
						if(iter==vertexCount->end()){
							(*vertexCount)[ri1.key].first=ri1;
							(*vertexCount)[ri1.key].second=0;
						}
						iter=vertexCount->find(ri2.key);
						if(iter==vertexCount->end()){
							(*vertexCount)[ri2.key].first=ri2;
							(*vertexCount)[ri2.key].second=0;
						}
						(*vertexCount)[ri1.key].second--;
						(*vertexCount)[ri2.key].second++;
					}
					else{fprintf(stderr,"Bad Edge 1: %d %d\n",ri1.key,ri2.key);}
				}
			}
		}
	}
}
	};

	int SolveFixedDepthMatrix(const int& depth,const SortedTreeNodes& sNodes);
	int SolveFixedDepthMatrix(const int& depth,const int& startingDepth,const SortedTreeNodes& sNodes);

	int GetFixedDepthLaplacian(SparseSymmetricMatrix<float>& matrix,const int& depth,const SortedTreeNodes& sNodes);
	int GetRestrictedFixedDepthLaplacian(SparseSymmetricMatrix<float>& matrix,const int& depth,const int* entries,const int& entryCount,const TreeOctNode* rNode,const Real& radius,const SortedTreeNodes& sNodes);

	void SetIsoSurfaceCorners(const Real& isoValue,const int& subdivisionDepth,const int& fullDepthIso);
	static int IsBoundaryFace(const TreeOctNode* node,const int& faceIndex,const int& subdivideDepth);
	static int IsBoundaryEdge(const TreeOctNode* node,const int& edgeIndex,const int& subdivideDepth);
	static int IsBoundaryEdge(const TreeOctNode* node,const int& dir,const int& x,const int& y,const int& subidivideDepth);
	void PreValidate(const Real& isoValue,const int& maxDepth,const int& subdivideDepth);
	void PreValidate(TreeOctNode* node,const Real& isoValue,const int& maxDepth,const int& subdivideDepth);
	void Validate(TreeOctNode* node,const Real& isoValue,const int& maxDepth,const int& fullDepthIso,const int& subdivideDepth);
	void Validate(TreeOctNode* node,const Real& isoValue,const int& maxDepth,const int& fullDepthIso);
	void Subdivide(TreeOctNode* node,const Real& isoValue,const int& maxDepth);

	int SetBoundaryMCRootPositions(const int& sDepth,const Real& isoValue,
		std::map<__int64,int>& boundaryRoots,std::map<__int64 ,std::pair<Real,Point3D<Real> > >& boundaryNormalHash,CoredMeshData* mesh,const int& nonLinearFit);
	int SetMCRootPositions(TreeOctNode* node,const int& sDepth,const Real& isoValue,
		std::map<__int64 ,int>& boundaryRoots,std::map<__int64 ,int>* interiorRoots,
		std::map<__int64 ,std::pair<Real,Point3D<Real> > >& boundaryNormalHash, std::map<__int64 ,std::pair<Real,Point3D<Real> > >* interiorNormalHash,
		std::vector<Point3D<float> >* interiorPositions,
		CoredMeshData* mesh,const int& nonLinearFit);

	int GetMCIsoTriangles(TreeOctNode* node,CoredMeshData* mesh, std::map<__int64 ,int>& boundaryRoots,
		std::map<__int64 ,int>* interiorRoots,std::vector<Point3D<float> >* interiorPositions,const int& offSet,const int& sDepth);

	static int AddTriangles(CoredMeshData* mesh,std::vector<CoredPointIndex> edges[3],std::vector<Point3D<float> >* interiorPositions,const int& offSet);
	static int AddTriangles(CoredMeshData* mesh,std::vector<CoredPointIndex>& edges,std::vector<Point3D<float> >* interiorPositions,const int& offSet);
	void GetMCIsoEdges(TreeOctNode* node, std::map<__int64 ,int>& boundaryRoots, std::map<__int64 ,int>* interiorRoots,const int& sDepth,
		std::vector<std::pair<__int64 ,__int64 > >& edges);
	static int GetEdgeLoops(std::vector<std::pair<__int64 ,__int64 > >& edges,std::vector<std::vector<std::pair<__int64 ,__int64 > > >& loops);
	static int InteriorFaceRootCount(const TreeOctNode* node,const int &faceIndex,const int& maxDepth);
	static int EdgeRootCount(const TreeOctNode* node,const int& edgeIndex,const int& maxDepth);
	int GetRoot(const RootInfo& ri,const Real& isoValue,const int& maxDepth,Point3D<Real> & position,std::map<__int64 ,std::pair<Real,Point3D<Real> > >& normalHash,
		Point3D<Real>* normal,const int& nonLinearFit);
	int GetRoot(const RootInfo& ri,const Real& isoValue,Point3D<Real> & position,std::map<__int64 ,std::pair<Real,Point3D<Real> > >& normalHash,const int& nonLinearFit);
	static int GetRootIndex(const TreeOctNode* node,const int& edgeIndex,const int& maxDepth,RootInfo& ri);
	static int GetRootIndex(const TreeOctNode* node,const int& edgeIndex,const int& maxDepth,const int& sDepth,RootInfo& ri);
	static int GetRootIndex(const __int64 & key, std::map<__int64 ,int>& boundaryRoots, std::map<__int64 ,int>* interiorRoots,CoredPointIndex& index);
	static int GetRootPair(const RootInfo& root,const int& maxDepth,RootInfo& pair);

	int NonLinearUpdateWeightContribution(TreeOctNode* node,const Point3D<Real>& position,const Real& weight=Real(1.0));
	Real NonLinearGetSampleWeight(TreeOctNode* node,const Point3D<Real>& position);
	void NonLinearGetSampleDepthAndWeight(TreeOctNode* node,const Point3D<Real>& position,const Real& samplesPerNode,Real& depth,Real& weight);
	int NonLinearSplatOrientedPoint(TreeOctNode* node,const Point3D<Real>& point,const Point3D<Real>& normal);
	void NonLinearSplatOrientedPoint(const Point3D<Real>& point,const Point3D<Real>& normal,const int& kernelDepth,const Real& samplesPerNode,const int& minDepth,const int& maxDepth);

	int HasNormals(TreeOctNode* node,const Real& epsilon);

	Real getCenterValue(const TreeOctNode* node);
	Real getCornerValue(const TreeOctNode* node,const int& corner);
	void getCornerValueAndNormal(const TreeOctNode* node,const int& corner,Real& value,Point3D<Real>& normal);
public:
	static double maxMemoryUsage;
	static double MemoryUsage(void);
	std::vector< Point3D<Real> >* normals;
	Real postNormalSmooth;
	TreeOctNode tree;
	FunctionData<Degree,float> fData;
	Octree();

	void setFunctionData(const PPolynomial<Degree>& ReconstructionFunction,const int& maxDepth,const int& normalize,const Real& normalSmooth=-1);
	void finalize1(const int& refineNeighbors=-1);
	void finalize2(const int& refineNeighbors=-1);
	int setTree(std::vector<Point3D<Real> > &Pts, std::vector<Point3D<Real> > &Nor, 
							const int& maxDepth,const int& kernelDepth,const Real& samplesPerNode,
							const Real& scaleFactor,Point3D<Real>& center,Real& scale,const int& resetSampleDepths,const int& useConfidence);

	void SetLaplacianWeights(void);
	void ClipTree(void);
	int LaplacianMatrixIteration(const int& subdivideDepth);

	Real GetIsoValue(void);
	void GetMCIsoTriangles(const Real& isoValue,CoredMeshData* mesh,const int& fullDepthIso=0,const int& nonLinearFit=1);
	void GetMCIsoTriangles(const Real& isoValue,const int& subdivideDepth,CoredMeshData* mesh,const int& fullDepthIso=0,const int& nonLinearFit=1);
};

#include "MultiGridOctreeData.inl"
#endif // MULTI_GRID_OCTREE_DATA_INCLUDED
