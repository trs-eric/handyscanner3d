#ifndef COCTOCUBE_H
#define COCTOCUBE_H

#include <cstdlib>
#include <iostream>
#include <cmath>

#include <fstream>
#include "carray3d.h"
#include "carray2d.h"
#include "graphicutils.h"

extern  int global_octo_depth_max;
extern  int global_octo_depth_cut;


//template<class T> class COctoCubeLowest;
template<class T> class COctoCubeLeaf;


template<class T>
class COctoCube
{
public:
    COctoCube(float x, float y, float z, float hside, int depth = 0) :
        _x(x), _y(y), _z(z), _half_side(hside),_depth(depth)
    {
        for(int i = 0; i < 8; i++)
              _cubes[i] = NULL;
    }

    virtual ~COctoCube()
    {
        clear_data();
    }


    virtual std::list<C3DPoint> get_points()
    {

       // std::cout << _depth << std::endl;

        std::list<C3DPoint> res;
        for(int i = 0; i < 8; i++)
        {
            if(_cubes[i] != NULL)
            {
                std::list<C3DPoint> ires =  _cubes[i]->get_points();
               // std::cout << _depth << " " << i << " " << ires.size() << std::endl;

                if(ires.size())
                   res.insert(res.end(), ires.begin(),ires.end());

            }
           /// std::cout << _depth << " " << i << " " << res.size() << std::endl;
        }
        return res;
    }

    virtual void add_point(float x, float y, float z)
    {
        if(_depth == 0)
        {
            //  if the point outside bb we drop it
            if(x < (_x - _half_side)) return;
            if(x > (_x + _half_side)) return;
            if(y < (_y - _half_side)) return;
            if(y > (_y + _half_side)) return;
            if(z < (_z - _half_side)) return;
            if(z > (_z + _half_side)) return;
        }

           int i = (x<=_x)?0:1;
           int j = (y<=_y)?0:1;
           int k = (z<=_z)?0:1;

           int idx = k*4 + j*2 + i;

           if(_cubes[idx] == NULL)
           {
               // add cube here
               float small_side = _half_side/2.0;
               float xc = (i==0)? (_x - small_side):(_x + small_side);
               float yc = (j==0)? (_y - small_side):(_y + small_side);
               float zc = (k==0)? (_z - small_side):(_z + small_side);

               if(_depth < (global_octo_depth_cut-1))
                   _cubes[idx] = new COctoCube<T>(xc, yc, zc, small_side, _depth+1);
               else
                   _cubes[idx] = new COctoCubeLeaf<T>(xc, yc, zc, small_side, _depth+1);

           }
           else
               _cubes[idx]->add_point(x,y,z);
    }

    float get_smallest_cube_side()
    {
        if(_depth == 0)
        {
            return (2*_half_side) / std::pow(2.0,global_octo_depth_max);
        }
        else
            return 0.; //unknown value
    }

    int get_cube_side_sz()
    {
            return std::pow(2.0,global_octo_depth_max);
    }

    virtual void clear_data()
    {
        for(int i = 0; i < 8; i++)
        {
            if(_cubes[i] != NULL)
            {
                delete  _cubes[i];
                _cubes[i] = NULL;
            }
        }
    }

protected:
    // sub cubes
    COctoCube<T> * _cubes[8];

    // cube center
    float _x, _y, _z;

    // a/2
    float _half_side;

    // hierarhical depth
    int   _depth;
};

struct avpnt
{
    // average point coordinates
    float _x, _y, _z;

    //hit number
    float _hits;

    avpnt(float v = 0): _x(0), _y(0), _z(0), _hits(0){}

    void add_point(float x, float y, float z)
    {
        if(_hits == 0)
        {
            _x = x;
            _y = y;
            _z = z;
            _hits = 1;
        }
        else
        {
            float inchits = (_hits + 1);
            _x = (_hits * _x + x)/ inchits;
            _y = (_hits * _y + y)/ inchits;
            _z = (_hits * _z + z)/ inchits;

            _hits = inchits;
        }
    }
};

template<class T>
class COctoCubeLeaf : public COctoCube<T>
{
public:
    COctoCubeLeaf(float x, float y, float z, float hside, int depth = 0)  :
        COctoCube<T>(x, y ,z, hside, depth)
    {
        float levels = global_octo_depth_max - global_octo_depth_cut;
        float sz = std::pow(2.0,levels);
        _data = new CArray3D<T> (sz,sz,sz);
    }

    virtual void add_point(float x, float y, float z)
    {

        float sz = _data->_xs;
        float factor = sz/ (2.0*COctoCube<T>::_half_side);

        float xf = (x - (COctoCube<T>::_x - COctoCube<T>::_half_side)) * factor;
        if (xf <  0)  xf = 0;
        if (xf >= sz) xf = sz-1;
        int xi =  int(xf);

        float yf = (y - (COctoCube<T>::_y - COctoCube<T>::_half_side)) * factor;
        if (yf <  0)  yf = 0;
        if (yf >= sz) yf = sz-1;
        int yi =  int(yf);

        float zf = (z - (COctoCube<T>::_z - COctoCube<T>::_half_side)) * factor;
        if (zf <  0)  zf = 0;
        if (zf >= sz) zf = sz-1;
        int zi = int(zf) ;

        _data->_data[xi][yi][zi].add_point(x, y, z);
    }


    virtual std::list<C3DPoint>  get_points()
    {
        std::list<C3DPoint> res;

        float szx = _data->_xs;
        float szy = _data->_ys;
        float szz = _data->_zs;

       /// std::cout << "in ";
        for (int x = 0; x < szx; x++)
        {
            for (int y = 0; y < szy; y++)
            {
                for(int z = 0; z < szz; z++)
                {

                    if(_data->_data[x][y][z]._hits != 0)
                    {
                        C3DPoint p;
                        p._x = _data->_data[x][y][z]._x;
                        p._y = _data->_data[x][y][z]._y;
                        p._z = _data->_data[x][y][z]._z;
                        p._r = .8;
                        p._g = .8;
                        p._b = .8;
                        res.push_back(p);
                    }
                }
            }
        }
        ///std::cout<< res.size() << " out" << std::endl;


        return res;
    }

    virtual ~COctoCubeLeaf()
    {
        if(_data != NULL )
        {
            delete _data;
            _data = NULL;
        }
    }

protected:
    CArray3D<T> * _data;
};


/*

template<class T>
class COctoCubeLowest : public COctoCube<T>
{
public:
    COctoCubeLowest(float x, float y, float z, float hside, int depth = 0) :
    COctoCube<T>(x, y, z, hside, depth), _data(NULL)
    {
        float levels = global_octo_depth_max - global_octo_depth_cut;
        float sz = std::pow(2.0,levels);
        _data = new CArray3D<T> (sz,sz,sz);
    }

    virtual void add_value(float x, float y, float z, T val)
    {

        float sz = _data->_xs;
        float factor = sz/ (2.0*COctoCube<T>::_half_side);

        float xf = (x - (COctoCube<T>::_x - COctoCube<T>::_half_side)) * factor;
        if (xf <  0)  xf = 0;
        if (xf >= sz) xf = sz-1;
        int xi =  int(xf);

        float yf = (y - (COctoCube<T>::_y - COctoCube<T>::_half_side)) * factor;
        if (yf <  0)  yf = 0;
        if (yf >= sz) yf = sz-1;
        int yi =  int(yf);

        float zf = (z - (COctoCube<T>::_z - COctoCube<T>::_half_side)) * factor;
        if (zf <  0)  zf = 0;
        if (zf >= sz) zf = sz-1;
        int zi = int(zf) ;

        _data->_data[xi][yi][zi] +=val;
    }

    virtual T get_value(float x, float y, float z)
    {
        float sz = _data->_xs;
        float factor = sz / (2.0 * COctoCube<T>::_half_side);

        float xf = (x - (COctoCube<T>::_x - COctoCube<T>::_half_side)) * factor;
        if (xf <  0)  xf = 0;
        if (xf >= sz) xf = sz-1;
        int xi =  int(xf);

        float yf = (y - (COctoCube<T>::_y - COctoCube<T>::_half_side)) * factor;
        if (yf <  0)  yf = 0;
        if (yf >= sz) yf = sz-1;
        int yi =  int(yf);

        float zf = (z - (COctoCube<T>::_z - COctoCube<T>::_half_side)) * factor;
        if (zf <  0)  zf = 0;
        if (zf >= sz) zf = sz-1;
        int zi = int(zf) ;

       return _data->_data[xi][yi][zi];
    }

    virtual ~COctoCubeLowest()
    {
        delete _data;
    }
protected:
    CArray3D<T> * _data;
}; */


#endif // COCTOCUBE_H
