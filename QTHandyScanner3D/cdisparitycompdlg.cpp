#include "cdisparitycompdlg.h"
#include "ui_cdisparitycompdlg.h"
#include <QFileDialog>

#include "cglwidget.h"
#include "cdisparitycompthread.h"
#include "cpointcloud.h"

CDisparityCompDlg::CDisparityCompDlg(CGLWidget * glWidget, QWidget *parent) :
    _glWidget(glWidget), QDialog(parent),
    ui(new Ui::CDisparityCompDlg)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);

    ui->setupUi(this);


   // ui->limgEdt->setText(QString("cones_left.ppm"));
   // ui->rimgEdt->setText(QString("cones_right.ppm"));
   // ui->maxDispEdt->setText(QString::number(59));

    /* ui->limgEdt->setText(QString("imgU_left_1.bmp"));
    ui->rimgEdt->setText(QString("imgU_right_1.bmp"));
    ui->maxDispEdt->setText(QString::number(128));
    ui->shiftLeftEdt->setText(QString::number(40));


    ui->thresBorEdt->setText(QString::number(3));
    ui->tau1Edt->setText(QString::number(7));
    ui->tau2Edt->setText(QString::number(2));
    ui->gammaEdt->setText(QString::number(0.1));
    ui->rEdt->setText(QString::number(11));
    ui->epsEdt->setText(QString::number(0.02));

    ui->gammacEdt->setText(QString::number(0.1));
    ui->gammadEdt->setText(QString::number(9));
    ui->rMedianEdt->setText(QString::number(19));
     ui->RefinChkBox->setChecked(true);*/

     ui->edtCamImg->setText("./realrect08apr/rectified_100.bmp");
     ui->edtDecim->setText(QString::number(1));
     ui->edtIgnoreDisp->setText(QString::number(70));
     ui->chkUseCol->setChecked(true);
     ui->chkSmoothDisp->setChecked(true);
     connect(ui->btnBrowse, SIGNAL(clicked()), this, SLOT(onBrowseBtn()));



    connect(ui->previewBtn, SIGNAL(clicked()), this, SLOT(onPreviewBtn()));
    connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));

    _thread = new CDisparityCompThread;


    // user pressed cancel button
      connect(ui->cancelBtn,  SIGNAL(clicked()), _thread, SLOT(stop_process()));
      connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

      // user closed dialg
      connect(this,  SIGNAL(finished(int)), _thread, SLOT(stop_process()));

      // working with a thread
      connect(_thread, SIGNAL(send_back(int)), this, SLOT(onMakeProgressStep(int)));
      /// connect(_thread, SIGNAL(send_result(std::vector<C3DPoint> * )), this, SLOT(onCompleteProcessing(std::vector<C3DPoint> * )));
      /// connect(_thread, SIGNAL(send_result(std::vector<C3DPoint> * )), _glWidget, SLOT(construct_next_scan_left(std::vector<C3DPoint> * )));

      connect(_thread, SIGNAL(send_result(std::vector<C3DPoint> * )), this, SLOT(onCompleteProcessing(std::vector<C3DPoint> * )));
      connect(_thread, SIGNAL(send_result(std::vector<C3DPoint> * )), _glWidget, SLOT(construct_new_scan(std::vector<C3DPoint> *)));

      connect(_thread, SIGNAL(finished()), this, SLOT(onStopProcessing()));


}

void CDisparityCompDlg::onMakeProgressStep(int val)
{
    ui->progressBar->setValue(val);
}

void CDisparityCompDlg::onCompleteProcessing(std::vector<C3DPoint> * )
{
    if(_on_apply)
    {
        close_dialog();
        return;
    }

    _is_result_valid = true;
    ui->progressBar->setValue(100);
}

void CDisparityCompDlg::onStopProcessing()
{
    ui->applyBtn->setEnabled(true);
    ui->previewBtn->setEnabled(true);
    ui->btnBrowse->setEnabled(true);

    ui->edtCamImg->setEnabled(true);
    ui->edtDecim->setEnabled(true);
    ui->edtIgnoreDisp->setEnabled(true);
    ui->chkUseCol->setEnabled(true);
    ui->chkSmoothDisp->setEnabled(true);


    /*
    ui->limgEdt->setEnabled(true);
    ui->rimgEdt->setEnabled(true);
    ui->maxDispEdt->setEnabled(true);
    ui->shiftLeftEdt->setEnabled(true);

    ui->thresBorEdt->setEnabled(true);
    ui->tau1Edt->setEnabled(true);
    ui->tau2Edt->setEnabled(true);
    ui->gammaEdt->setEnabled(true);
    ui->rEdt->setEnabled(true);
    ui->epsEdt->setEnabled(true);


    ui->gammacEdt->setEnabled(true);
    ui->gammadEdt->setEnabled(true);
    ui->rMedianEdt->setEnabled(true);
    ui->RefinChkBox->setEnabled(true);*/

    _in_process = false;
}

CDisparityCompDlg::~CDisparityCompDlg()
{
    delete ui;
    delete _thread;
}

void CDisparityCompDlg::close_dialog()
{
    _on_apply = false;
    close();
}



void CDisparityCompDlg::onCancelBtn()
{
    if(!_in_process)
    {
        _glWidget->discard_raw_points_of_current_object();
        close_dialog();
    }
    _in_process = false;
    _on_apply = false;
}

void CDisparityCompDlg::onBrowseBtn()
{
    QString fileName = QFileDialog::getOpenFileName(this,
          tr("Open camera image ..."), "", "Stere-pair(*.bmp)");

    if (fileName.isEmpty()) return;

    ui->edtCamImg->setText(fileName);
}

void CDisparityCompDlg::onPreviewBtn()
{

    ui->applyBtn->setEnabled(false);
    ui->previewBtn->setEnabled(false);
    ui->btnBrowse->setEnabled(false);
    ui->edtCamImg->setEnabled(false);
    ui->edtDecim->setEnabled(false);
    ui->edtIgnoreDisp->setEnabled(false);
    ui->chkUseCol->setEnabled(false);
    ui->chkSmoothDisp->setEnabled(false);

    /*ui->limgEdt->setEnabled(false);
    ui->rimgEdt->setEnabled(false);
    ui->shiftLeftEdt->setEnabled(false);
    ui->maxDispEdt->setEnabled(false);
    ui->thresBorEdt->setEnabled(false);
    ui->tau1Edt->setEnabled(false);
    ui->tau2Edt->setEnabled(false);
    ui->gammaEdt->setEnabled(false);
    ui->rEdt->setEnabled(false);
    ui->epsEdt->setEnabled(false);
    ui->gammacEdt->setEnabled(false);
    ui->gammadEdt->setEnabled(false);
    ui->rMedianEdt->setEnabled(false);
    ui->RefinChkBox->setEnabled(false);*/

    _in_process = true;

    QImage * cam_img= new QImage(ui->edtCamImg->text());
    /// QImage * cam_img = new QImage(cam_img_old->scaled(cam_img_old->width(), cam_img_old->height(),Qt::KeepAspectRatio));

    _thread->start_process(cam_img, ui->chkUseCol->isChecked(),ui->edtIgnoreDisp->text().toDouble(),
                           ui->edtDecim->text().toInt(), ui->chkSmoothDisp->isChecked());
    /*
    QImage * left_im = new QImage(ui->limgEdt->text());
    QImage * right_im = new QImage(ui->rimgEdt->text());



    _thread->start_process(left_im, right_im,
                           ui->shiftLeftEdt->text().toInt(),
                            ui->thresBorEdt->text().toDouble()/255.0,
                            ui->maxDispEdt->text().toInt(),
                            ui->tau1Edt->text().toDouble()/255.0,
                            ui->tau2Edt->text().toDouble()/255.0,
                            ui->gammaEdt->text().toDouble(),
                            ui->rEdt->text().toInt(),
                            ui->epsEdt->text().toDouble(),
                            ui->gammacEdt->text().toDouble(),
                            ui->gammadEdt->text().toDouble(),
                            ui->rMedianEdt->text().toInt(),
                            ui->RefinChkBox->isChecked()) ;*/
}

void CDisparityCompDlg::onApplyBtn()
{
    _on_apply = true;
    if(!_is_result_valid)
        onPreviewBtn();
    else
        close_dialog();
}

void CDisparityCompDlg::showEvent(QShowEvent * event)
{
    ui->progressBar->setValue(0);

    _is_result_valid    = false;
    _on_apply           = false;
    _in_process         = false;

}

















