#include "cqecdecimdlg.h"
#include "ui_cqecdecimdlg.h"
#include "cglwidget.h"

#include "cqecdecimthread.h"
#include "cmeshdumpthread.h"
#include "messages.h"

std::string qecd_mesh_dump_str("./qecd.dump");

CQECDecimDlg::CQECDecimDlg(CGLWidget * glWidget, QWidget *parent) :
    QDialog(parent),  _glWidget(glWidget),
    ui(new Ui::CQECDecimDlg)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);

    ui->edtPercReduc->setText(QString::number(50));
    ui->edtQualThres->setText(QString::number(0.3));
    ui->edtBoundPresWei->setText(QString::number(1));
    ui->chkOptimalPos->setChecked(true);
    ui->chkPostSimp->setChecked(true);
    ui->progressBar->setMaximum(100);

    connect(ui->previewBtn, SIGNAL(clicked()), this, SLOT(onPreviewBtn()));
    connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));

    _thread = new CQECDecimThread;

    // user pressed cancel button
    connect(ui->cancelBtn,  SIGNAL(clicked()), _thread, SLOT(stop_process()));
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

    // user closed dialg
    connect(this,  SIGNAL(finished(int)), _thread, SLOT(stop_process()));
    // working with a thread
    connect(_thread, SIGNAL(send_back(int)), this, SLOT(onMakeProgressStep(int)));
    connect(_thread, SIGNAL(send_result(CTriangularMesh*)), this, SLOT(onCompleteProcessing(CTriangularMesh*)));
    connect(_thread, SIGNAL(send_result(CTriangularMesh*)), _glWidget, SLOT(on_mesh_computed(CTriangularMesh*)));
    connect(_thread, SIGNAL(finished()), this, SLOT(onStopProcessing()));

    //// working with mesh dumping thread
    _info = new QMessageBox(this);
    _ithread = new CMeshDumpThread;
    connect(_ithread, SIGNAL(done_dumping()), this, SLOT(onFinishDumping()));

}

CQECDecimDlg::~CQECDecimDlg()
{
    delete ui;
    delete _thread;
    delete _info;
    delete _ithread;
}

void CQECDecimDlg::onMakeProgressStep(int val)
{
    ui->progressBar->setValue(val);
}

void CQECDecimDlg::onCompleteProcessing(CTriangularMesh * m)
{
   if(m == NULL)
   {
       QMessageBox::warning(this, tr("Warning"),wrn_FailedMesh);
       return;
   }
   if(_on_apply)
   {
       close_dialog();
       return;
   }

   _is_result_valid = true;
   ui->progressBar->setValue(100);
}

void CQECDecimDlg::onStopProcessing()
{
    ui->applyBtn     ->setEnabled(true);
    ui->previewBtn   ->setEnabled(true);
    ui->edtPercReduc ->setEnabled(true);
    ui->edtQualThres ->setEnabled(true);
    ui->chkPresBound ->setEnabled(true);
    ui->edtBoundPresWei->setEnabled(true);
    ui->chkPresNorm  ->setEnabled(true);
    ui->chkPresTopo  ->setEnabled(true);
    ui->chkOptimalPos->setEnabled(true);
    ui->chkPlanarSimp->setEnabled(true);
    ui->chkWeiSimp   ->setEnabled(true);
    ui->chkPostSimp  ->setEnabled(true);

    _in_process = false;
}

void CQECDecimDlg::close_dialog()
{
    _on_apply = false;
    close();
}

CTriangularMesh * CQECDecimDlg::restore_mesh()
{
    _glWidget->discard_mesh_of_current_object();
    std::ifstream ifile;
    ifile.open(qecd_mesh_dump_str.c_str(), std::ios::in | std::ios::binary);
    CTriangularMesh * mesh = new CTriangularMesh;
    mesh->load(ifile);
    ifile.close();
    _glWidget->on_mesh_computed(mesh);

    return mesh;
}

void CQECDecimDlg::onCancelBtn()
{
    if(!_in_process)
    {
        if(_is_result_valid)
                restore_mesh();
        close_dialog();
    }

       _in_process = false;
       _on_apply = false;
}

void CQECDecimDlg::onPreviewBtn()
{
    CTriangularMesh * mesh = _glWidget->get_mesh_of_current_object() ;
    if(mesh == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NoMesh);
        return;
    }

    ui->progressBar->setValue(0);
    ui->applyBtn     ->setEnabled(false);
    ui->previewBtn   ->setEnabled(false);
    ui->edtPercReduc ->setEnabled(false);
    ui->edtQualThres ->setEnabled(false);
    ui->chkPresBound ->setEnabled(false);
    ui->edtBoundPresWei->setEnabled(false);
    ui->chkPresNorm  ->setEnabled(false);
    ui->chkPresTopo  ->setEnabled(false);
    ui->chkOptimalPos->setEnabled(false);
    ui->chkPlanarSimp->setEnabled(false);
    ui->chkWeiSimp   ->setEnabled(false);
    ui->chkPostSimp  ->setEnabled(false);
    _in_process = true;

    CTriangularMesh * mm = (_is_result_valid)? restore_mesh() :_glWidget->get_mesh_of_current_object();
    _thread->start_process(mm,
                           0,
                           ui->edtPercReduc ->text().toInt(),
                           ui->edtQualThres ->text().toFloat(),
                           ui->chkPresBound ->isChecked(),
                           ui->edtBoundPresWei->text().toFloat(),
                           ui->chkPresNorm ->isChecked(),
                           ui->chkPresTopo ->isChecked(),
                           ui->chkOptimalPos->isChecked(),
                           ui->chkPlanarSimp->isChecked(),
                           ui->chkWeiSimp->isChecked(),
                           ui->chkPostSimp->isChecked());

}

void CQECDecimDlg::onApplyBtn()
{
    _on_apply = true;
    if(!_is_result_valid)
        onPreviewBtn();
    else
        close_dialog();
}

void CQECDecimDlg::showEvent(QShowEvent * event)
{
    ui->progressBar->setValue(0);
    _is_result_valid    = false;
    _on_apply           = false;
    _in_process         = false;

    CTriangularMesh * cmesh = _glWidget->get_mesh_of_current_object();


   if(cmesh != NULL)
   {
        _ithread->start_process(cmesh, qecd_mesh_dump_str);
        _info->setText("Dumping mesh to external file ... ");
        _info->setStandardButtons(0);
        _info->show();
   }
}

void CQECDecimDlg::onFinishDumping()
{
    std::cout << "onFinishDumping" << std::endl;

    _info->setText("Dumping mesh to external file ... done");
    _info->hide();
}
