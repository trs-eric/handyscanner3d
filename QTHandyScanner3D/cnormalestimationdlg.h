#ifndef CNORMALESTIMATIONDLG_H
#define CNORMALESTIMATIONDLG_H

#include <QDialog>

class CGLWidget;
class CNormalCloud;
class CNormalEstimationThread;

namespace Ui {
class CNormalEstimationDlg;
}

class CNormalEstimationDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CNormalEstimationDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CNormalEstimationDlg();

protected:
    void showEvent(QShowEvent * event);


private:
    Ui::CNormalEstimationDlg *ui;

    int _ncount;
    bool _nflip;

    CGLWidget *_glWidget;

    bool _in_process;
    bool _on_apply;
    bool _is_result_valid;
    CNormalEstimationThread * _thread;

private slots:
    void onCancelBtn();
    void onPreviewBtn();
    void onApplyBtn();

    void onMakeProgressStep(int val);
    void onCompleteProcessing(CNormalCloud * );
    void onStopProcessing();

    void ncountNew(QString );
    void nflipNew(bool);
    void close_dialog();

};

#endif // CNORMALESTIMATIONDLG_H
