#ifndef CIOSTATE_H
#define CIOSTATE_H

#include <fstream>

class CIOState
{
public:
    CIOState();
    virtual void save(std::ofstream & ofile) = 0;
    virtual void load(std::ifstream & ifile) = 0;

};

#endif // CIOSTATE_H
