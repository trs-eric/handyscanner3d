#ifndef CNORMALCLOUD_H
#define CNORMALCLOUD_H

#include<vector>
#include "cglobject.h"
#include "graphicutils.h"
#include "ciostate.h"

class CNormalCloud : public CGLObject, public CIOState
{
public:
    CNormalCloud();

    void create_glList();

    int get_normals_count(){return _normals.size();}
    std::vector<C3DPoint> get_normal_cpy(){return _normals;}
    void set_normals(std::vector<C3DPoint> & nrm){_normals = nrm;}
    void append_normals(std::vector<C3DPoint> & nrm){_normals.insert(_normals.end(), nrm.begin(), nrm.end());}


    void save(std::ofstream & ofile);
    void load(std::ifstream & ifile);

protected:

    std::vector<C3DPoint> _normals;

private:

    int _alphaN;
    int _bettaN;
    float _aux_s;
    float _aux_r;
    std::vector<C3DPoint> _tnormals;
    std::vector<C3DPoint> _tv0;
    std::vector<C3DPoint> _tv1;
    std::vector<C3DPoint> _tv2;
    std::vector<C3DPoint> _tv3;

    void construct_nv_table();
    int find_proper_normal(C3DPoint & p);
    float find_quick_smallest_cloud_distance();
};

#endif // CNORMALCLOUD_H
