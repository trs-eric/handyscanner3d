#include "ccropdiskthread.h"
#include "cpointcloud.h"
#include "settings/psettings.h"

CCropDiskThread::CCropDiskThread(QObject *parent):
      QThread(parent), _in_cloud(NULL), _crop_top(false),
      _crop_top_val(0), _crop_bot(false),
      _crop_bot_val(0), _crop_cyl(false),
      _crop_cyl_val(0),_fill_top(false), _fill_bottom(false)
{
    _abort = false;
}


CCropDiskThread::~CCropDiskThread()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
    wait();
}

void CCropDiskThread::start_process(CPointCloud * in,
                                    bool crop_top,
                                    float crop_top_val,
                                    bool crop_bot,
                                    float crop_bot_val,
                                    bool crop_cyl,
                                    float crop_cyl_val,
                                    bool bottom, bool top)
{
    _abort = false;
    _crop_top = crop_top;
    _crop_top_val = crop_top_val;

    _crop_bot = crop_bot;
    _crop_bot_val = crop_bot_val;

    _crop_cyl = crop_cyl;
    _crop_cyl_val = crop_cyl_val;

    _fill_bottom = bottom;
    _fill_top = top;
    _in_cloud = in;

    start();
}

void CCropDiskThread::stop_process()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
}

bool crop_compare(C3DPoint a, C3DPoint b)
{
    if (a._y == b._y) return (a._x < b._x);
    else return (a._y < b._y);
}

void CCropDiskThread::run()
{
    float div = 0;
    if(_crop_bot ||  _crop_top  || _crop_cyl )
        div += 1;
    if(_fill_bottom) div += 1;
    if(_fill_top)    div += 1;
    float progress_max = 100/div;
    int last_progress = 0;

    float crop_cyl_val_sq = _crop_cyl_val * _crop_cyl_val;
    std::vector<C3DPoint> final_res;
    std::vector<C3DPoint> orig_points =  _in_cloud->get_points_cpy();
    if (_crop_bot || _crop_top || _crop_cyl)
    {
        int sz  = orig_points.size();
        std::vector<C3DPoint>::iterator it = orig_points.begin();
        for( int i = 0 ; it != orig_points.end(); it++, i++)
        {

            bool add_point = true;

            float pdist  = it->_x * it->_x + it->_z * it->_z;

            if(  (_crop_bot && (it->_y < _crop_bot_val)) ||
                 (_crop_top && (it->_y > _crop_top_val)) ||
                 (_crop_cyl && (pdist > crop_cyl_val_sq) ) )
                add_point = false;


            if(add_point)
                final_res.push_back(*it);

            //setup progress value
            emit send_back(i * progress_max / (sz-1));

            if (_abort)
            {
                emit send_back(0);
                return;
            }
        }

        last_progress = progress_max;
    }
    else
        final_res = orig_points;

    if (_fill_bottom || _fill_top)
    {

       float angle_step = 2;
       int angle_sz = 360/angle_step;


       std::vector<int> validity_top(angle_sz);
       std::vector<C3DPoint *> ch_top;
       for(int i = 0; i < angle_sz ; i++)
           ch_top.push_back(NULL);

       std::vector<int> validity_bottom(angle_sz);
       std::vector<C3DPoint *> ch_bottom(angle_sz);
       for(int i = 0; i < angle_sz ; i++)
           ch_bottom.push_back(NULL);

       float min_y = final_res.front()._y;
       float max_y = 0;

       float bgm_angle_step = BGM(angle_step);
       std::vector<C3DPoint>::iterator it = final_res.begin();
       int sz_points = final_res.size();
       for( int i = 0; it != final_res.end(); it++, i++)
       {
           //
           // int bucket_idx = it->_angle / bgm_angle_step;
           int bucket_idx = (atan2(it->_z, it->_x) + cPI) / bgm_angle_step;
           //std::cout <<bucket_idx << " " ;

           if(ch_bottom[bucket_idx] == NULL)
           {
               ch_bottom[bucket_idx] = new C3DPoint;
               *ch_bottom[bucket_idx] = *it;
               validity_bottom[bucket_idx] = 1;
           }
           else
           {
               if(ch_bottom[bucket_idx]->_y > it->_y)
               {
                   *ch_bottom[bucket_idx] = *it;


               }
                validity_bottom[bucket_idx] ++;

           }
           if(ch_bottom[bucket_idx]->_y < min_y) min_y = ch_bottom[bucket_idx]->_y;


           if(ch_top[bucket_idx] == NULL)
           {
               ch_top[bucket_idx] = new C3DPoint;
               *ch_top[bucket_idx] = *it;
               validity_top[bucket_idx] = 1;
           }
           else
           {
               if(ch_top[bucket_idx]->_y < it->_y)
               {
                   *ch_top[bucket_idx] = *it;


               }
                validity_top[bucket_idx] ++;
           }
           if(ch_top[bucket_idx]->_y > max_y) max_y = ch_top[bucket_idx]->_y;

           if (_abort)
           {
               emit send_back(0);
               return;
           }

           emit send_back(last_progress + i * progress_max / (sz_points-1));


       }

       /// fix bucket content for spickes

        for (int b = 0 ; b < angle_sz ; b++)
       {
           if(ch_bottom[b] != NULL)
           {
              //std::cout <<"bottom:"<< b << " " <<  validity_bottom[b] << std::endl;
               if(abs(ch_bottom[b]->_y - min_y) > 2 * CGlobalSettings::_min_opt_dist)
               {
                     delete ch_bottom[b] ;
                     ch_bottom[b] = NULL;
               }
               else ch_bottom[b]->_y = min_y;
           }

           if(ch_top[b] != NULL)
           {
              // std::cout <<"top: " << b << " " <<  validity_top[b] << std::endl;
              if(abs(ch_top[b]->_y - max_y) > 2 * CGlobalSettings::_min_opt_dist)
              {
                    delete ch_top[b] ;
                    ch_top[b] = NULL;
              }
              else ch_top[b]->_y = max_y;

           }
        }

       if (_fill_bottom)
       {
           std::vector<C3DPoint> fill_bottom;
           for(int bb_idx = 0; bb_idx <angle_sz/2; bb_idx++)
           {
               int be_idx = bb_idx + angle_sz/2;
               if(ch_bottom[bb_idx] != NULL && ch_bottom[be_idx] !=NULL)
               {
                   C3DPoint begp = *ch_bottom[bb_idx];
                   C3DPoint endp = *ch_bottom[be_idx];

                   float dist = sqrt( (begp._x - endp._x) * (begp._x - endp._x) +
                                (begp._y - endp._y) * (begp._y - endp._y) +
                                (begp._z - endp._z) * (begp._z - endp._z) );

                   int tau_count = dist / (2*CGlobalSettings::_min_opt_dist);
                   //std::cout << tau_count << " " << CGlobalSettings::_min_opt_dist;
                   for(int tau = 0; tau < tau_count ;  tau++)
                   {

                        C3DPoint curp = begp;
                        curp._x += (endp._x - begp._x) * float(tau)/float(tau_count);
                        curp._y += (endp._y - begp._y) * float(tau)/float(tau_count);
                        curp._z += (endp._z - begp._z) * float(tau)/float(tau_count);
                        curp._nx =  0;
                        curp._ny = -1;
                        curp._nz =  0;

                        fill_bottom.push_back(curp);
                   }

                   if (_abort)
                   {
                       emit send_back(0);
                       return;
                   }


               }

           }
           ////////////////////// optimization
           int sz  = fill_bottom.size();
           bool * visit = new bool[sz]();
           for (int k =0; k<sz; k++) visit[k] = false;

           std::vector<C3DPoint> fill_bottom_opt;
           float dist_thres_sq = CGlobalSettings::_min_opt_dist * CGlobalSettings::_min_opt_dist;

           std::vector<C3DPoint>::iterator it = fill_bottom.begin();
           for( int i =0 ; it != fill_bottom.end(); it++, i++)
           {
               // skip considered point
               if(visit[i]) continue;

               std::vector<C3DPoint> small_point_pull;
               small_point_pull.push_back(*it);

               std::vector<C3DPoint>::iterator itn = it+1;
               for( int j = i + 1; itn != fill_bottom.end(); itn++, j++)
               {
                   if(!visit[j])
                   {
                       if(     ((it->_x - itn->_x)*(it->_x - itn->_x) +
                                (it->_y - itn->_y)*(it->_y - itn->_y) +
                                (it->_z - itn->_z)*(it->_z - itn->_z)) < dist_thres_sq)
                       {
                           small_point_pull.push_back(*itn);
                           visit[j] = true;
                       }

                   }
               }

               if(small_point_pull.size() == 1)
                   fill_bottom_opt.push_back(*it);
               else
                   fill_bottom_opt.push_back(compute_avg_pnt(small_point_pull));

               visit[i] = true;

               if (_abort)
               {
                   emit send_back(0);
                   return;
               }
           }
           delete [] visit;
           ////////////////////////
           final_res.insert(final_res.end(), fill_bottom_opt.begin(), fill_bottom_opt.end());
       }

       if(_fill_top)
       {
           std::vector<C3DPoint> fill_top;
           for(int bb_idx = 0; bb_idx <angle_sz/2; bb_idx++)
           {
               int be_idx = bb_idx + angle_sz/2;
               if(ch_top[bb_idx] != NULL && ch_top[be_idx] !=NULL)
               {
                   C3DPoint begp = *ch_top[bb_idx];
                   C3DPoint endp = *ch_top[be_idx];

                   float dist = sqrt( (begp._x - endp._x) * (begp._x - endp._x) +
                                (begp._y - endp._y) * (begp._y - endp._y) +
                                (begp._z - endp._z) * (begp._z - endp._z) );

                   int tau_count = dist / (2*CGlobalSettings::_min_opt_dist);
                   for(int tau = 0; tau < tau_count ;  tau++)
                   {

                        C3DPoint curp = begp;
                        curp._x += (endp._x - begp._x) * float(tau)/float(tau_count);
                        curp._y += (endp._y - begp._y) * float(tau)/float(tau_count);
                        curp._z += (endp._z - begp._z) * float(tau)/float(tau_count);
                        curp._nx = 0;
                        curp._ny = 1;
                        curp._nz = 0;

                        fill_top.push_back(curp);
                   }

                   if (_abort)
                   {
                       emit send_back(0);
                       return;
                   }


               }

           }
           ////////////////////// optimization
           int sz  = fill_top.size();
           bool * visit = new bool[sz]();
           for (int k =0; k<sz; k++) visit[k] = false;

           std::vector<C3DPoint> fill_top_opt;
           float dist_thres_sq = CGlobalSettings::_min_opt_dist * CGlobalSettings::_min_opt_dist;

           std::vector<C3DPoint>::iterator it = fill_top.begin();
           for( int i =0 ; it != fill_top.end(); it++, i++)
           {
               // skip considered point
               if(visit[i]) continue;

               std::vector<C3DPoint> small_point_pull;
               small_point_pull.push_back(*it);

               std::vector<C3DPoint>::iterator itn = it+1;
               for( int j = i + 1; itn != fill_top.end(); itn++, j++)
               {
                   if(!visit[j])
                   {
                       if(     ((it->_x - itn->_x)*(it->_x - itn->_x) +
                                (it->_y - itn->_y)*(it->_y - itn->_y) +
                                (it->_z - itn->_z)*(it->_z - itn->_z)) < dist_thres_sq)
                       {
                           small_point_pull.push_back(*itn);
                           visit[j] = true;
                       }

                   }
               }

               if(small_point_pull.size() == 1)
                   fill_top_opt.push_back(*it);
               else
                   fill_top_opt.push_back(compute_avg_pnt(small_point_pull));

               visit[i] = true;

               if (_abort)
               {
                   emit send_back(0);
                   return;
               }
           }
           delete [] visit;
           ////////////////////////

           final_res.insert(final_res.end(), fill_top_opt.begin(), fill_top_opt.end());
       }

       std::vector<C3DPoint *>::iterator itt = ch_top.begin();
       for(; itt != ch_top.end() ; itt++)
           if(*itt != NULL) delete (*itt);

       std::vector<C3DPoint *>::iterator itb = ch_bottom.begin();
       for(; itb != ch_bottom.end() ; itb++ )
           if(*itb != NULL) delete (*itb);
    }

    CPointCloud * res_cld = new CPointCloud;
    res_cld->set_points(final_res);
    emit send_result(res_cld);

}
