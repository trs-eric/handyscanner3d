#include "ctexturingdlg.h"
#include "ui_ctexturingdlg.h"
#include "cglwidget.h"
#include "ctexturingthread.h"

#include <QMessageBox>
#include <QFileDialog>

#include "messages.h"

CTexturingDlg::CTexturingDlg(CGLWidget * glWidget,
                             QWidget *parent) :
    QDialog(parent), _glWidget(glWidget),
    ui(new Ui::CTexturingDlg)
{

    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);

     ui->edtGroupProc->setText(QString::number(1.0));
    ui->edtColorIter->setText(QString::number(18));
    ui->progressBar->setMaximum(100);

    /// connect(ui->previewBtn, SIGNAL(clicked()), this, SLOT(onPreviewBtn()));
    connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));


    _thread = new CTexturingThread;

    // user pressed cancel button
    connect(ui->cancelBtn,  SIGNAL(clicked()), _thread, SLOT(stop_process()));
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

    // user closed dialg
    connect(this,  SIGNAL(finished(int)), _thread, SLOT(stop_process()));


    // working with a thread
    connect(_thread, SIGNAL(send_back(int)), this, SLOT(onMakeProgressStep(int)));
    connect(_thread, SIGNAL(send_result()), this, SLOT(onCompleteProcessing()));
    connect(_thread, SIGNAL(send_result()), _glWidget, SLOT(on_mesh_updated()));

    connect(_thread, SIGNAL(finished()), this, SLOT(onStopProcessing()));
    connect(ui->btnBrowseINI, SIGNAL(clicked()), this, SLOT(onLoadINIBtn()));



}

CTexturingDlg::~CTexturingDlg()
{
    delete ui;
    delete _thread;
}

void CTexturingDlg::onLoadINIBtn()
{

    QString fileName = QFileDialog::getOpenFileName(this,
              tr("Open scan INI file ..."), "", "Scan parameters(*.ini)");

    if (fileName.isEmpty()) return;

    ui->edtININame->setText(fileName);

    //QFileInfo info_fname(fileName);
    //QSettings settings(fileName , QSettings::IniFormat);
}

void CTexturingDlg::onMakeProgressStep(int val)
{
    ui->progressBar->setValue(val);
}

 void CTexturingDlg::onCompleteProcessing(/*CTriangularMesh * m*/)
{
    /*if(m == NULL)
    {
        QMessageBox::warning(this, tr("Warning"),wrn_FailedMesh);
        return;
    }*/
    if(_on_apply)
    {
        close_dialog();
        return;
    }

    _is_result_valid = true;
    ui->progressBar->setValue(100);

}

void CTexturingDlg::onStopProcessing()
{
    ui->applyBtn    ->setEnabled(true);
     ui->edtGroupProc->setEnabled(true);
    ui->edtColorIter->setEnabled(true);
    ui->edtININame->setEnabled(true);
    ui->btnBrowseINI->setEnabled(true);

    _in_process = false;
}

void CTexturingDlg::close_dialog()
{
    _on_apply = false;
    close();
}

void CTexturingDlg::onCancelBtn()
{
    if(!_in_process)
        close_dialog();
    _in_process = false;
    _on_apply = false;
}


void CTexturingDlg::onPreviewBtn()
{

    CTriangularMesh * mesh = _glWidget->get_fused_mesh() ;

    if(mesh == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NoMesh);
        return;
    }

    ui->applyBtn->setEnabled(false);
    ui->edtGroupProc->setEnabled(false);
    ui->edtColorIter->setEnabled(false);
    ui->edtININame->setEnabled(false);
    ui->btnBrowseINI->setEnabled(false);
    _in_process = true;
    _thread->start_process(mesh, 1, ui->edtGroupProc->text().toFloat()/100.0,
                          ui->edtColorIter->text().toInt(), ui->edtININame->text() );
}



void CTexturingDlg::onApplyBtn()
{
    _on_apply = true;
    if(!_is_result_valid)
        onPreviewBtn();
    else
        close_dialog();
}

void CTexturingDlg::showEvent(QShowEvent * event)
{
    ui->progressBar->setValue(0);
    _is_result_valid    = false;
    _on_apply           = false;
    _in_process         = false;
}

