#include "ccameratrack.h"
#include "cframe.h"
#include "./settings/psettings.h"

CCameraTrack::CCameraTrack(MenuActions * va) : _va(va)
{

    _color[0] = 0.44f;
    _color[1] = 0.86f;
    _color[2] = 0.64f;


    isVisible = true;
}

CCameraTrack::~CCameraTrack()
{
    clean();
}


void CCameraTrack::set_visible(bool v)
{
    isVisible = v;

    std::vector<CCameraPose *>::iterator it = _camerasp.begin();
    for( ;it != _camerasp.end(); it++)
        (*it)->isVisible = v;
}
void CCameraTrack::clean()
{
    std::vector<CCameraPose *>::iterator it = _camerasp.begin();
    for( ;it != _camerasp.end(); it++)
        delete (*it);
    _camerasp.clear();
}

void CCameraTrack::remake_glList()
{
    CGLObject::delete_glLists();
    std::vector<CCameraPose *>::iterator it = _camerasp.begin();
    for( ;it != _camerasp.end(); it++)
        (*it)->remake_glList();
}

void CCameraTrack::glDraw()
{

    CGLObject::glDraw();
    std::vector<CCameraPose *>::iterator it = _camerasp.begin();
    for( ;it != _camerasp.end(); it++)
        (*it)->glDraw();
}

void CCameraTrack::create_glList()
{
  //// std::cout << "CCameraTrack::create_glList()" << std::endl;
    delete_glLists();
    if(global_keyframes_sequence.size() == 0 )
    {
        _va->view_camara_path_Act->setEnabled(false);
        return;
    }
    else
        _va->view_camara_path_Act->setEnabled(true);

    clean();

   /// std::cout << "CCameraTrack::create_glList() " << global_keyframes_sequence.size() << std::endl;


    _oglid = glGenLists(1);
    glNewList(_oglid, GL_COMPILE);

    /*glDisable(GL_LIGHTING);


    ///////////////// draw track
    glBegin(GL_LINE_STRIP);
    std::vector<CFrame *>::iterator it = global_keyframes_sequence.begin();
    for( ; it != global_keyframes_sequence.end(); it++)
    {
        if(!(*it)->_valid_coord) continue;

        double px = convert_real_coord_to_gl_x ((*it)->_global_pos.at<double>(0,0));
        double py = convert_real_coord_to_gl_y ((*it)->_global_pos.at<double>(1,0));
        double pz = convert_real_coord_to_gl_z ((*it)->_global_pos.at<double>(2,0));

        ///std::cout << "!!!!!!!! " << px << " " << py  << " " << pz << " " <<  (*it)->_valid_coord<< std::endl;


        glColor4f(_color[0], _color[1] , _color[2], CGlobalHSSettings::gl_cam_alpha);
        glVertex3f(px, py, pz);

    }
    glEnd();

    glColor4f(1.f, 1.f, 1.f, 1.f);
    glEnable(GL_LIGHTING);*/
    glEndList();


    ///////////////////// draw individual cameras
    std::vector<CFrame *>::iterator it = global_keyframes_sequence.begin();
    for( ; it != global_keyframes_sequence.end(); it++)
    {
        if(!(*it)->_valid_coord) continue;

        double px = convert_real_coord_to_gl_x ((*it)->_global_pos.at<double>(0,0));
        double py = convert_real_coord_to_gl_y ((*it)->_global_pos.at<double>(1,0));
        double pz = convert_real_coord_to_gl_z ((*it)->_global_pos.at<double>(2,0));

        CCameraPose * ps = new CCameraPose(isVisible);
        ps->_tx = px;
        ps->_ty = py;
        ps->_tz = pz;

       ps->_rotx = IBGM( -(*it)->_glob_rot_vec.at<double>(0,0));
       ps->_roty = IBGM(  (*it)->_glob_rot_vec.at<double>(0,1));
       ps->_rotz = IBGM(  (*it)->_glob_rot_vec.at<double>(0,2));
        _camerasp.push_back(ps);
    }

     /*

    CCameraPose * ps = new CCameraPose;
    ps->_tx =     gT.at<double>(0,0)   * glob_gl_scale + glob_gl_offx;
    ps->_ty =  - gT.at<double>(1,0)   * glob_gl_scale + glob_gl_offy ;
    ps->_tz =  - gT.at<double>(2,0)    * glob_gl_scale + glob_gl_offz;

    ps->_rotx = -rvec_glob_L1.at<double>(0,0)*360/cPI2;
    ps->_roty = -rvec_glob_L1.at<double>(0,1)*360/cPI2;
    ps->_rotz = -rvec_glob_L1.at<double>(0,2)*360/cPI2;

*/

     // create new display list
  /*   _oglid = glGenLists(1);
     glNewList(_oglid, GL_COMPILE);
     glDisable(GL_LIGHTING);
         glBegin(GL_LINES);
         // x axis
         glColor3f(1, 0,  0);
         glVertex3f(0, 0, 0);
         glColor3f(1, 0,  0);
         glVertex3f(.5, 0, 0);

         // y axis
         glColor3f(0, 1,  0);
         glVertex3f(0, 0, 0);
         glColor3f(0, 1,  0);
         glVertex3f(0, .5, 0);

         // z axis
         glColor3f(0, 0,  1);
         glVertex3f(0, 0, 0);
         glColor3f(0, 0,  1);
         glVertex3f(0, 0, -0.5);

         glEnd();
      glEnable(GL_LIGHTING);

      glEndList();
*/

};
