#include "cglwidget.h"
#include <cmath>
#include <algorithm>
#include <QApplication>

#include<QMouseEvent>
#include<QMessageBox>
#include<QTreeWidgetItem>

#include "cglsampletrivtable.h"

#include "cglcamera.h"
#include "messages.h"

#include "cimportmanager.h"
#include "settings/psettings.h"

extern CGLCamera * gReferencePoint;


CGLWidget::CGLWidget(QWidget * treew, MenuActions * va, QWidget *parent) :
    QGLWidget(parent),_treew(treew), _va(va), _scene(NULL), _update_counter(0),
    _vmode(DEF_VIEW_MODE), _vselmode(SEL_POLY)
{
    setFocusPolicy(Qt::StrongFocus);

    _mouseLastPos = QPoint(0,0);


     _cursor_snorm_px = QPixmap (":/images/icons/mask_norm.png");
     _cursor_ssub_px = QPixmap (":/images/icons/mask_sub.png");
     _cursor_sadd_px = QPixmap (":/images/icons/mask_add.png");
     _cursor_spoly_px = QPixmap (":/images/icons/mask_norm.png");


}


void CGLWidget::set_view_scene_mode(viewSceneMode v)
{

    _vmode = v;

    updating_view_scene_mode();

    update();
}

void CGLWidget::updating_view_scene_mode()
{
    if(_scene == NULL) return;
    switch(_vmode)
    {
        case SEL_VIEW_MODE:

            /// _scene->set_selection_brush(false);
            _scene->set_selection_mode(true);
             switch(_vselmode)
            {

                case SEL_SUB:
                this->setCursor(_cursor_ssub_px);
                break;

                case SEL_ADD:
                this->setCursor(_cursor_sadd_px);
                break;

                case SEL_POLY:
                this->setCursor(_cursor_spoly_px);
                break;

                default:
                case SEL_NORM:
                this->setCursor(_cursor_snorm_px);
                break;

            }
            break;

        default:
        case DEF_VIEW_MODE:
         emit def_vmode();
            this->setCursor(Qt::ArrowCursor);
            _scene->set_selection_mode(false);
             updateGL();
            _scene->set_empty_selection();


        break;
    }
     updateGL();


}

std::vector<CGLCompoundObject *> CGLWidget::get_list_of_valid_meshes()
{
    if(_scene != NULL)
    {
        std::vector<CGLCompoundObject *>  res = _scene->get_list_of_valid_meshes();
        std::vector<CGLCompoundObject *> ::iterator pos = std::find(res.begin(), res.end(), _scene->get_current_object());
        if (pos != res.end())  res.erase(pos);

        return res;

    }
    else
        return std::vector<CGLCompoundObject *> ();
}

CTriangularMesh * CGLWidget::find_mesh_by_its_name(QString ttl)
{
    std::vector<CGLCompoundObject *>  res = _scene->get_list_of_valid_meshes();
    std::vector<CGLCompoundObject *>::iterator it =  res.begin();
    for( ; it != res.end(); it ++)
    {
        if((*it)->get_title() == ttl)
            return (*it)->get_mesh();
    }
    return NULL;
}

void CGLWidget::on_double_click_treew_item(QTreeWidgetItem * item, int col)
{
   _scene->update_selection_state(item,col);
   updateGL();
}


void CGLWidget::on_single_click_treew_item(QTreeWidgetItem * item, int col)
{
    _scene->on_single_click_treew_switch(item,col);
    updateGL();
}

CGLWidget::~CGLWidget()
{
    makeCurrent();
}

void CGLWidget::make_new_scene()
{
    set_view_scene_mode(DEF_VIEW_MODE);

    delete_scene();

    updateGL();
}

void CGLWidget::delete_scene()
{
    if(_scene)
    {
        delete _scene;
        _scene = NULL;
    }
}

void CGLWidget::initializeGL()
{
    //GLfloat lightPos[4] = { 5.0f, 5.0f, 10.0f, 1.0f };
    //GLfloat reflectance1[4] = { 0.8f, 0.1f, 0.0f, 1.0f };
    //GLfloat reflectance2[4] = { 0.0f, 0.8f, 0.2f, 1.0f };
    //GLfloat reflectance3[4] = { 0.2f, 0.2f, 1.0f, 1.0f };


    glLightfv(GL_LIGHT0, GL_POSITION, CGlobalHSSettings::gl_light_Position);
    glLightfv(GL_LIGHT0, GL_AMBIENT,  CGlobalHSSettings::gl_light_Ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  CGlobalHSSettings::gl_light_Diffuse);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);

    glEnable(GL_NORMALIZE);

    ///glEnable(GL_COLOR_MATERIAL);

     //glEnable( GL_BLEND );
     //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glClearColor(CGlobalHSSettings::gcolor_sr, CGlobalHSSettings::gcolor_sg, CGlobalHSSettings::gcolor_sb, 0.0f);
}

GLuint texid;
void Load_texture()
{

    QString tex_file("eggtex.jpg"/*tex_check.png"*/);
    QImage * img = new QImage(tex_file);
    uchar * bits = img->bits();



    glGenTextures(1, &texid);
    glBindTexture(GL_TEXTURE_2D, texid);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR); // scale linearly when image bigger than texture
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR); // scale linearly when image smalled than texture


    glTexImage2D(GL_TEXTURE_2D, 0, 4, img->width(), img->height(), 0, GL_BGRA, GL_UNSIGNED_BYTE, bits);
}

void Draw_texture()
{
    int h = 2;
        glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texid);
    glBegin(GL_QUADS);

        //glNormal3f (0, 0, 10);
        //glColor3f(1, 0, 0);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-h,  -h, 0);

        //glNormal3f (0, 0, 10);
        //glColor3f(0, 1, 0);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-h, h, 0);


        //glNormal3f (0, 0, 10);
        //glColor3f(0, 1, 0);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(h, h, 0);


        //glNormal3f (0, 0, 10);
        //glColor3f(1, 0, 0);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(h, -h, 0);
    glEnd();
        glDisable(GL_TEXTURE_2D);
}

void CGLWidget::paintGL()
{
    /// std::cout << this->_update_counter << " CGLWidget::paintGL()" << std::endl;
    if(_scene == NULL)
    {
        _scene = new CGLScene(_treew,_va, this);
      //Load_texture();
    }
    _scene->GLRenderScene();
    //Draw_texture();
}


void CGLWidget::resizeGL(int width, int height)
{
    if(width ==0)  height = 1;
    glViewport(0,0,width ,height );
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45,GLfloat(width) /height ,0.5,500);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}


void CGLWidget::mousePressEvent(QMouseEvent *event)
{
    _mouseLastPos = event->pos();
    if( (event->buttons() & Qt::LeftButton) && (_vmode == SEL_VIEW_MODE) )
          _mouseSelStPos = event->pos();
    if((event->buttons() & Qt::LeftButton) && (_vmode == SEL_VIEW_MODE) && (_vselmode == SEL_POLY)  )
    {
         if(_scene != NULL)
             _scene->extend_poly_selection(cv::Point2f(event->pos().x(),height()-event->pos().y()));
    }
    updateGL();
}


void CGLWidget::mouseDoubleClickEvent( QMouseEvent * event)
{
    if((_vmode == SEL_VIEW_MODE) && (_vselmode == SEL_POLY)  )
    {
        if(_scene != NULL)
        {
            QApplication::setOverrideCursor(Qt::WaitCursor);

            _scene->extend_poly_selection(cv::Point2f(event->pos().x(),height()-event->pos().y()));
            updateGL();

            _scene->update_selection_points(CPointCloud::POLY);
            updateGL();

            _scene->set_empty_selection();

            QApplication::restoreOverrideCursor();
            updateGL();
        }
    }

}

void CGLWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if(_scene == NULL) return;

    if((_vmode == SEL_VIEW_MODE)   )
    {
        CPointCloud::SelectMode sm;
        switch(_vselmode)
        {
            case SEL_SUB:
            sm = CPointCloud::SUB;
            break;

            case SEL_ADD:
            sm = CPointCloud::ADD;
            break;


            case SEL_POLY:
                    return; // ?
            break;

            default:
            case SEL_NORM:
            sm = CPointCloud::DEF;
            break;
        }

        if(event->button() == Qt::LeftButton)
             _scene->update_selection_points(sm);
        _scene-> set_selection_frame(0, 0, 0, 0);

    }

    updateGL();
}

void CGLWidget::mouseMoveEvent(QMouseEvent *event)
{


    float diffx = event->x() - _mouseLastPos.x();
    float diffy = event->y() - _mouseLastPos.y();
    _mouseLastPos = event->pos();

    float vtr  = 0.02f;
    float vrot = 0.09f;
    bool pos_changed  = false;

    // waiting for init
    if(_scene == NULL) return;

    if( event->buttons() & Qt::LeftButton )
    {
        if(_vmode == SEL_VIEW_MODE)
        {
            if(_vselmode != SEL_POLY)
                _scene-> set_selection_frame(_mouseSelStPos.x(), height() - _mouseSelStPos.y(), _mouseLastPos.x() - _mouseSelStPos.x(), _mouseSelStPos.y()-_mouseLastPos.y());
            else
            {
               // _scene-> set_selection_frame(event->x(),height() - event->y(), 0, 0);
               // _scene->update_selection_points(CPointCloud::BRUSH);
            }
            pos_changed = true;
        }
    }
    else if( event->buttons() & Qt::RightButton )
    {
      //  if(_vmode == DEF_VIEW_MODE)
        {
            gReferencePoint->turn_right (2*vrot*diffx);
            gReferencePoint->turn_up  (2*vrot*diffy);
            pos_changed = true;
        }


    } else 	if( event->buttons() & Qt::MiddleButton)
    {
      //  if(_vmode == DEF_VIEW_MODE)
        {
            gReferencePoint->move_left (0.8*vtr*diffx) ;
            gReferencePoint->move_up (0.8*vtr*diffy);
            pos_changed = true;
        }
    }
    if (pos_changed)
    {
        // _update_counter ++;
       //  if(_update_counter > 5)
        {
         //    _update_counter =0;
             //_scene->update_referece_dependant_glRepo();
        }
        updateGL();

    }
}


void CGLWidget::udate_gl()
{
    updateGL();
}

void CGLWidget::wheelEvent ( QWheelEvent* event)
{
    float vtr  = 0.005f;
    if(gReferencePoint != NULL)
        gReferencePoint->move_forward(vtr*float(event->delta()));

    // should we remake gl?
    // _scene->update_referece_dependant_glRepo();
    updateGL();
}

void CGLWidget::import_scene(QString &fname)
{
    if(!_scene->ImportScene(fname))
        QMessageBox::warning(this, tr("Warning"), err_cannotLoadFile);

    compute_proper_col_width();
    updateGL();
}

void CGLWidget::construct_single_scan(std::vector<C3DPoint> & pnts)
{
    _scene->ConstructSingleScan(pnts);

    compute_proper_col_width();
    updateGL();
}

void CGLWidget::construct_new_scan(std::vector<C3DPoint> * pnts)
{
    _scene->ConstructSingleScan(*pnts);

    if(pnts != NULL) delete pnts;

    compute_proper_col_width();
    updateGL();
}

void CGLWidget::send_clean_scan()
{
    _scene->discard_raw_points_of_current_object();
    compute_proper_col_width();
    updateGL();
}

void CGLWidget::send_clean_all_scans()
{

    _scene->delete_all_scans();

    _scene->UpdateCameraTrack();
    compute_proper_col_width();
    updateGL();
}



void CGLWidget::add_new_globj(CGLObject* obj)
{
    _scene->add_new_globj(obj);
    updateGL();
}

void CGLWidget::prepaire_new_scan()
{
    _scene->PrepaireNewScan();
}

void CGLWidget::construct_next_scan_left(std::vector<C3DPoint> * pnts)
{
    _scene->ConstructNextScanLeft(*pnts);
    delete pnts;
    compute_proper_col_width();
    updateGL();
}

void CGLWidget::construct_next_scan_right(std::vector<C3DPoint> * pnts)
{
    _scene->ConstructNextScanRight(*pnts);
    delete pnts;
    compute_proper_col_width();
    updateGL();
}

void CGLWidget::construct_next_texture_left(CRawTexture * tex)
{
    _scene->ConstructNextTextureLeft(tex);
    compute_proper_col_width();
    updateGL();
}

void CGLWidget::construct_next_texture_right(CRawTexture * tex)
{
    _scene->ConstructNextTextureRight(tex);
    compute_proper_col_width();
    updateGL();
}

void CGLWidget::construct_next_meshcast(int fid, CTriMesh* m)
{
    _scene->ConstructNextMeshCast(fid, m);
    _scene->UpdateCameraTrack();

    compute_proper_col_width();
    updateGL();
}

void CGLWidget::construct_next_scan(std::vector<C3DPoint> * pnts)
{
    _scene->ConstructNextScan(*pnts);
    _scene->UpdateTable();
  //  _scene->UpdateCameraTrack();
    delete pnts;
    compute_proper_col_width();
    updateGL();
}


void CGLWidget::export_scene(QString &fname)
{
    _scene->ExportScene(fname);
}


bool CGLWidget::load_project(QString &fname)
{
    return _scene->load_from_rub_file(fname);
}

void CGLWidget::finilize_loading_from_rub()
{
    _scene->finilize_loading_from_rub();
    compute_proper_col_width();
    updateGL();
}


void CGLWidget::compute_proper_col_width()
{
    ((QTreeWidget*)_treew)->resizeColumnToContents(0);
}
void CGLWidget::save_project(QString &fname)
{
    _scene->save_into_rub_file(fname);
}

void CGLWidget::on_raw_pnts_computed(CPointCloud* pnts)
{

    if(pnts == NULL) return;
    _scene->set_raw_poins_of_current_object(pnts);
    compute_proper_col_width();
    updateGL();
}

void CGLWidget::on_dpcl_computed(std::vector<C3DPoint> * pnts)
{
    if(pnts == NULL)
        return;
    _scene->set_dpcl_of_current_object(pnts);
    compute_proper_col_width();
    updateGL();
}

void CGLWidget::on_optim_pnts_computed(CPointCloud* pnts)
{
    if(pnts == NULL) return;
    _scene->set_optim_poins_of_current_object(pnts);
    compute_proper_col_width();
    updateGL();
}

void CGLWidget::on_normal_pnts_computed(CNormalCloud* nrms)
{
    if(nrms == NULL) return;
    _scene->set_normal_poins_of_current_object(nrms);
    updateGL();
}
void CGLWidget::on_mesh_computed(CTriangularMesh * mesh)
{
    if(mesh == NULL) return;
    _scene->set_mesh_of_current_object(mesh);
   //_scene->update_mesh_of_current_object();
   //  updateGL();
     //on_mesh_updated();
        updateGL();
        updateGL();

}

void CGLWidget::on_new_fused_mesh_computed(CTriangularMesh * mesh)
{
    if(mesh == NULL) return;

    _scene->delete_unique_cobject();
    _scene->create_unique_cobject(mesh);

    compute_proper_col_width();
    updateGL();
    updateGL();

}

void CGLWidget::on_rawpnts_updated()
{
    _scene->update_all_raw_points();
    updateGL();
}

void CGLWidget::on_mesh_updated()
{
    _scene->update_mesh_of_current_object();
    updateGL();
}

void CGLWidget::on_new_mesh_computed(CTriangularMesh * mesh)
{
    if(mesh == NULL) return;

    _scene->create_new_cobject(mesh);
    ///_scene->set_mesh_of_current_object(mesh);

    compute_proper_col_width();
    updateGL();
}

void CGLWidget::on_new_cglobject_computed(std::vector<C3DPoint> *pcl, CTriangularMesh *mesh, float r, float g, float b)
{

    _scene->create_new_cglobject(pcl, mesh, r, g, b);
    _scene->UpdateCameraTrack();
    _scene->UpdateTable();
    _scene->ResetCameraPos();
    compute_proper_col_width();
    updateGL();
}


void CGLWidget::on_new_campos()
{
    _scene->UpdateCameraTrack();
    updateGL();
}

void CGLWidget::on_reset_camera_pos()
{
    _scene->ResetCameraPos();
    updateGL();
}

void CGLWidget::on_table_pos()
{
    _scene->UpdateTable();
    updateGL();
}

CPointCloud * CGLWidget::get_raw_points_of_current_object()
{
    return _scene->get_raw_poins_of_current_object();
}

void CGLWidget::discard_raw_points_of_current_object()
{
    _scene->discard_raw_points_of_current_object();
    updateGL();
}

void CGLWidget::clean_current_scan()
{
    _scene->CleanCurrentScan();
    updateGL();
}

CPointCloud * CGLWidget::get_optim_points_of_current_object()
{
    return _scene->get_optim_points_of_current_object();
}

void CGLWidget::discard_optim_points_of_current_object()
{
   _scene->discard_optim_points_of_current_object();
   updateGL();
}


void CGLWidget::discard_mesh_of_current_object()
{
    _scene->discard_mesh_of_current_object();
    updateGL();
}


CTriangularMesh * CGLWidget::get_mesh_of_current_object()
{
    if(_scene == NULL) return NULL;
    return _scene->get_mesh_of_current_object();
}

CTriangularMesh * CGLWidget::get_fused_mesh()
{
    if(_scene == NULL) return NULL;
    return _scene->get_fused_mesh();
}

std::vector<C3DPoint> * CGLWidget::get_dpcl_of_current_object()
{
    if(_scene == NULL) return NULL;
    return _scene->get_dpcl_of_current_object();
}

std::vector<C3DPoint> *  CGLWidget::get_merged_dpcl_of_scene()
{
    if(_scene == NULL) return NULL;
    return _scene->get_merged_dpcl_of_scene();
}


CNormalCloud * CGLWidget::get_normals_of_current_object()
{
    return _scene->get_normals_of_current_object();
}

void CGLWidget::discard_normal_points_of_current_object()
{
    _scene->discard_normal_points_of_current_object();
    updateGL();
}

void CGLWidget::chainge_view_mode_of_current_object(CGLCompoundObject::ViewingMode vm)
{
    if(_scene == NULL) return;
    _scene->chainge_view_mode_of_current_object(vm);
    updateGL();
}

void CGLWidget::set_mesh_mode_chainge_rmode_of_current_object(CTriangularMesh::RenderingMode rm)
{
     if(_scene == NULL) return;
     _scene->chainge_view_mode_of_current_object(CGLCompoundObject::MESH);
     _scene->chainge_mesh_rmode_of_current_object(rm);
    updateGL();
}

void CGLWidget::set_view_shad_points(bool v)
{
    if(_scene == NULL) return;
    _scene->set_view_shad_points(v);
    updateGL();
}

void CGLWidget::set_view_camera_path(bool v)
{
    if(_scene == NULL) return;
    _scene->set_view_camera_path(v);
    updateGL();
}

void CGLWidget::keyReleaseEvent(QKeyEvent *event)
{
    if(_vmode == SEL_VIEW_MODE)
    {
        if(_scene != NULL)
            _scene->set_selection_poly(false);
        switch(event->key())
        {
            case Qt::Key_C:
            case Qt::Key_Shift:
            case Qt::Key_Control:
            case Qt::Key_Delete:
            case Qt::Key_Escape:

                _vselmode = SEL_POLY;
                if(_scene != NULL)
                    _scene->set_selection_poly(true);
            break;

            /*
                _scene->update_selection_points(CPointCloud::POLY);
                _vselmode = SEL_ADD;
                break;*/
        }
        updating_view_scene_mode();
        return;
    }
}

void CGLWidget::delete_selected_points()
{
    if(_vmode != SEL_VIEW_MODE) return;
    _scene->delete_selected_points( );
    updateGL();
}

void CGLWidget::process_selected_faces()
{
    if(_vmode != SEL_VIEW_MODE) return;
    _scene->process_selected_faces();
    updateGL();
}

void CGLWidget::clear_selected_faces()
{
    if(_vmode != SEL_VIEW_MODE) return;
     _scene->set_empty_selection();
     _scene->update_selection_points(CPointCloud::POLY);

    updateGL();

//    updating_view_scene_mode();
//    _scene->update_mesh_of_current_object();

}

void CGLWidget::keyPressEvent(QKeyEvent *event)
{

    if(_vmode == SEL_VIEW_MODE)
    {
        _vselmode = SEL_POLY;
        if(_scene != NULL)
            _scene->set_selection_poly(true);

            switch(event->key())
            {

                case Qt::Key_Control:
                    _vselmode = SEL_ADD;
                    if(_scene != NULL)
                    {
                        _scene->set_empty_selection();
                        _scene->set_selection_poly(false);
                    }
                break;

                case Qt::Key_Shift:
                    _vselmode = SEL_SUB;
                    if(_scene != NULL)
                    {
                        _scene->set_empty_selection();
                        _scene->set_selection_poly(false);
                    }
                break;

                case Qt::Key_Escape:
                    if(_scene != NULL)
                    {
                        if(_scene->is_there_selected_raw_points())
                        {
                            _scene->update_selection_points(CPointCloud::DEF);
                            _scene->set_empty_selection();
                        }
                        else
                            _vmode = DEF_VIEW_MODE;
                    }
                    else
                        _vmode = DEF_VIEW_MODE;


                    break;

                case Qt::Key_Delete:
                    delete_selected_points();
                    break;

            }
            updating_view_scene_mode();
            return;
    }

    switch(event->key())
    {


    case Qt::Key_R:
        _scene->chainge_view_mode_of_current_object(CGLCompoundObject::RAW_POINTS);
        updateGL();
        break;
    /* case Qt::Key_O:
        _scene->chainge_view_mode_of_current_object(CGLCompoundObject::OPTIM_POINTS);
        updateGL();
        break;
    case Qt::Key_N:
        _scene->chainge_view_mode_of_current_object(CGLCompoundObject::NORMALS);
        updateGL();
        break;*/
    case Qt::Key_M:
        _scene->chainge_view_mode_of_current_object(CGLCompoundObject::MESH);
        updateGL();
        break;
    /*case Qt::Key_Z:
        _scene->chainge_view_mode_of_current_object(CGLCompoundObject::NONE);
        updateGL();
        break;*/
    case Qt::Key_1:
        _scene->chainge_mesh_rmode_of_current_object(CTriangularMesh::MESH_WIRE);
        updateGL();
        break;
    case Qt::Key_2:
        _scene->chainge_mesh_rmode_of_current_object(CTriangularMesh::MESH_FLAT);
        updateGL();
        break;
    case Qt::Key_3:
        _scene->chainge_mesh_rmode_of_current_object(CTriangularMesh::MESH_SMOOTH);
        updateGL();
        break;
    case Qt::Key_4:
        _scene->chainge_mesh_rmode_of_current_object(CTriangularMesh::MESH_TEX);
        updateGL();
        break;

    }
}
