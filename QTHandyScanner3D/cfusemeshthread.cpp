#include "cfusemeshthread.h"
#include "ctriangularmesh.h"
#include "settings/psettings.h"

CFuseMeshThread::CFuseMeshThread(QObject *parent) :
    QThread(parent)
{
        _abort = false;
}

CFuseMeshThread::~CFuseMeshThread()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
    wait();
}
void CFuseMeshThread::start_process(int decim, float elen)
{
    _abort = false;

    _decim_step = decim;
    _max_side_len = elen;

    start();
}

void CFuseMeshThread::stop_process()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
}

void CFuseMeshThread::run()
{
    CArray2D<aux_pix_data *> * mdata = NULL;
    CTriMesh * rmesh = NULL;
    try
    {

        mdata = simplify_polar_map(g_polar_map);

        fill_gaps_in_merged_data_triv(mdata);

       rmesh  = construct_mesh_from_single_shot_round(*mdata, _decim_step);

        ////////////////////////////////////////////////////
        int xs = mdata->_xs;
        int ys = mdata->_ys;
        for (int j=0; j < ys; j++)
                for (int i=0; i < xs; i++)
                     if (mdata->_data[j][i]!= NULL)
                         delete mdata->_data[j][i];
        delete mdata;
        ////////////////////////////////////////////////////

        CTriangularMesh * mmesh = new CTriangularMesh;
       //// mmesh->use_loc_coloring();
        mmesh->set_mesh(rmesh);
        emit send_result_fuse(mmesh);
    }
    catch(...)
    {
        if(mdata) delete mdata;
        if(rmesh) delete rmesh;
        emit send_result_fuse(NULL);
        emit send_back(0);
        return;
    }
}


CTriMesh * CFuseMeshThread::construct_mesh_from_single_shot_round(CArray2D<aux_pix_data *> & merged_data,
                                                                  int decimstep)
{

    CTriMesh * tmesh = new CTriMesh;

    int xs = merged_data._xs;
    int ys = merged_data._ys;

    const int undefid = -1;
    std::vector<int> id_points(xs*ys, undefid);

    ///////////////// define points
    int pnt_idx = 0;
    for (int y = 0; y < ys; y+= decimstep )
    for (int x = 0; x < xs; x+= decimstep )
    {
        if(merged_data._data[y][x]!= NULL)
        {

            merged_data._data[y][x]->_gx = convert_real_coord_to_gl_x(merged_data._data[y][x]->_x);
            merged_data._data[y][x]->_gy = convert_real_coord_to_gl_y(-merged_data._data[y][x]->_z);
            merged_data._data[y][x]->_gz = convert_real_coord_to_gl_z(merged_data._data[y][x]->_y);


            C3DPoint p3d;
            p3d._x = merged_data._data[y][x]->_gx;
            p3d._y = merged_data._data[y][x]->_gy;
            p3d._z = merged_data._data[y][x]->_gz;

            merged_data._data[y][x]->_pidx = tmesh->_points.size();
            tmesh->_points.push_back(p3d);
            id_points[y*xs + x] = pnt_idx;
            pnt_idx++;
        }
    }

    ///////////////// define triangles
    for (int y = 0; y < ys - decimstep ; y+= decimstep )
    for (int x = 0; x < xs ;  x+= decimstep )
    {

        int x_far =  (x+decimstep);
        if(x_far >= xs) x_far = 0;

        int pidx_0 = id_points[y*xs + x];
        int pidx_1 = id_points[y*xs + x_far];
        int pidx_2 = id_points[(y + decimstep) * xs + x];
        int pidx_3 = id_points[(y + decimstep) * xs +x_far ];


        // first valid triangle
        if((pidx_0 != undefid) && (pidx_1 != undefid) && (pidx_2 != undefid) )
        {
            bool valid_dist_01 = (compute_dist_pnt(merged_data._data[y][x],
                                                   merged_data._data[y][x_far]) < _max_side_len*decimstep);
            bool valid_dist_12 = (compute_dist_pnt(merged_data._data[y][x_far],
                                                   merged_data._data[y+decimstep][x]) < _max_side_len*decimstep);
            bool valid_dist_20 = (compute_dist_pnt(merged_data._data[y+decimstep][x],
                                                   merged_data._data[y][x]) < _max_side_len*decimstep);
            if(valid_dist_01 && valid_dist_12 && valid_dist_20)
            {
                // add new triangle
                C3DPointIdx tri;
                tri.pnt_index[0] = pidx_2;
                tri.pnt_index[1] = pidx_1;
                tri.pnt_index[2] = pidx_0;
                tmesh->_triangles.push_back(tri);
            }
        }


        // second valid triangle
        if((pidx_2 != undefid) && (pidx_1 != undefid) && (pidx_3 != undefid)  )
        {
            bool valid_dist_21 = (compute_dist_pnt(merged_data._data[y+decimstep][x],
                                                   merged_data._data[y][x_far ]) < _max_side_len*decimstep);
            bool valid_dist_13 = (compute_dist_pnt(merged_data._data[y][x_far ],
                                                   merged_data._data[y+decimstep][x_far]) < _max_side_len*decimstep);
            bool valid_dist_32 = (compute_dist_pnt(merged_data._data[y+decimstep][x_far],
                                                   merged_data._data[y+decimstep][x]) < _max_side_len*decimstep);
            if(  valid_dist_21 && valid_dist_13 && valid_dist_32 )
            {
                // add new triangle
                C3DPointIdx tri;
                tri.pnt_index[0] = pidx_3;
                tri.pnt_index[1] = pidx_1;
                tri.pnt_index[2] = pidx_2;
                tmesh->_triangles.push_back(tri);
            }
         }
    }

    ///////// update mesh normals per face
    std::vector<C3DPointIdx>::iterator itt = tmesh->_triangles.begin();
    for(int fidx = 0 ; itt != tmesh->_triangles.end(); itt++, fidx++)
    {
        tmesh->_points[ itt->pnt_index[0] ]._faceidx.push_back(fidx);
        C3DPoint pnt0 = tmesh->_points[ itt->pnt_index[0] ];

        tmesh->_points[ itt->pnt_index[1] ]._faceidx.push_back(fidx);
        C3DPoint pnt1 = tmesh->_points[ itt->pnt_index[1] ];

        tmesh->_points[ itt->pnt_index[2] ]._faceidx.push_back(fidx);
        C3DPoint pnt2 = tmesh->_points[ itt->pnt_index[2] ];

        itt->tri_normal = compute_normal(pnt2, pnt1, pnt0);
    }

    //////// update mesh normals per vertex
    std::vector<C3DPoint>::iterator itv = tmesh->_points.begin();
    for(; itv != tmesh->_points.end(); itv++)
    {
        itv->_nx =0;
        itv->_ny =0;
        itv->_nz =0;

        std::vector<int>::iterator fit = itv->_faceidx.begin();
        for( ; fit != itv->_faceidx.end(); fit++)
        {

            itv->_nx += tmesh->_triangles[(*fit)].tri_normal._x;
            itv->_ny += tmesh->_triangles[(*fit)].tri_normal._y;
            itv->_nz += tmesh->_triangles[(*fit)].tri_normal._z;
        }
        normalize3(itv->_nx, itv->_ny, itv->_nz);
    }
    return tmesh;
}


CTriMesh * CFuseMeshThread::construct_mesh_from_single_shot(CArray2D<aux_pix_data *> & merged_data,
                                                                  int decimstep)
{

    CTriMesh * tmesh = new CTriMesh;

    int xs = merged_data._xs;
    int ys = merged_data._ys;

    const int undefid = -1;
    std::vector<int> id_points(xs*ys, undefid);

    ///////////////// define points
    int pnt_idx = 0;
    for (int y = 0; y < ys; y+= decimstep )
    for (int x = 0; x < xs; x+= decimstep )
    {
        if(merged_data._data[y][x]!= NULL)
        {

            merged_data._data[y][x]->_gx = convert_real_coord_to_gl_x(merged_data._data[y][x]->_x);
            merged_data._data[y][x]->_gy = convert_real_coord_to_gl_y(-merged_data._data[y][x]->_z);
            merged_data._data[y][x]->_gz = convert_real_coord_to_gl_z(merged_data._data[y][x]->_y);


            C3DPoint p3d;
            p3d._x = merged_data._data[y][x]->_gx;
            p3d._y = merged_data._data[y][x]->_gy;
            p3d._z = merged_data._data[y][x]->_gz;

            merged_data._data[y][x]->_pidx = tmesh->_points.size();
            tmesh->_points.push_back(p3d);
            id_points[y*xs + x] = pnt_idx;
            pnt_idx++;
        }
    }

    ///////////////// define triangles
    for (int y = 0; y < ys - decimstep ; y+= decimstep )
    for (int x = 0; x < xs - decimstep;  x+= decimstep )
    {
        int pidx_0 = id_points[y*xs + x];
        int pidx_1 = id_points[y*xs + x + decimstep];
        int pidx_2 = id_points[(y + decimstep) * xs + x];
        int pidx_3 = id_points[(y + decimstep) * xs + x + decimstep];


        // first valid triangle
        if((pidx_0 != undefid) && (pidx_1 != undefid) && (pidx_2 != undefid) )
        {
            bool valid_dist_01 = (compute_dist_pnt(merged_data._data[y][x],
                                                   merged_data._data[y][x+decimstep]) < _max_side_len*decimstep);
            bool valid_dist_12 = (compute_dist_pnt(merged_data._data[y][x+decimstep],
                                                   merged_data._data[y+decimstep][x]) < _max_side_len*decimstep);
            bool valid_dist_20 = (compute_dist_pnt(merged_data._data[y+decimstep][x],
                                                   merged_data._data[y][x]) < _max_side_len*decimstep);
            if(valid_dist_01 && valid_dist_12 && valid_dist_20)
            {
                // add new triangle
                C3DPointIdx tri;
                tri.pnt_index[0] = pidx_2;
                tri.pnt_index[1] = pidx_1;
                tri.pnt_index[2] = pidx_0;
                tmesh->_triangles.push_back(tri);
            }
        }


        // second valid triangle
        if((pidx_2 != undefid) && (pidx_1 != undefid) && (pidx_3 != undefid)  )
        {
            bool valid_dist_21 = (compute_dist_pnt(merged_data._data[y+decimstep][x],
                                                   merged_data._data[y][x+decimstep]) < _max_side_len*decimstep);
            bool valid_dist_13 = (compute_dist_pnt(merged_data._data[y][x+decimstep],
                                                   merged_data._data[y+decimstep][x+decimstep]) < _max_side_len*decimstep);
            bool valid_dist_32 = (compute_dist_pnt(merged_data._data[y+decimstep][x+decimstep],
                                                   merged_data._data[y+decimstep][x]) < _max_side_len*decimstep);
            if(  valid_dist_21 && valid_dist_13 && valid_dist_32 )
            {
                // add new triangle
                C3DPointIdx tri;
                tri.pnt_index[0] = pidx_3;
                tri.pnt_index[1] = pidx_1;
                tri.pnt_index[2] = pidx_2;
                tmesh->_triangles.push_back(tri);
            }
         }
    }

    ///////// update mesh normals per face
    std::vector<C3DPointIdx>::iterator itt = tmesh->_triangles.begin();
    for(int fidx = 0 ; itt != tmesh->_triangles.end(); itt++, fidx++)
    {
        tmesh->_points[ itt->pnt_index[0] ]._faceidx.push_back(fidx);
        C3DPoint pnt0 = tmesh->_points[ itt->pnt_index[0] ];

        tmesh->_points[ itt->pnt_index[1] ]._faceidx.push_back(fidx);
        C3DPoint pnt1 = tmesh->_points[ itt->pnt_index[1] ];

        tmesh->_points[ itt->pnt_index[2] ]._faceidx.push_back(fidx);
        C3DPoint pnt2 = tmesh->_points[ itt->pnt_index[2] ];

        itt->tri_normal = compute_normal(pnt2, pnt1, pnt0);
    }

    //////// update mesh normals per vertex
    std::vector<C3DPoint>::iterator itv = tmesh->_points.begin();
    for(; itv != tmesh->_points.end(); itv++)
    {
        itv->_nx =0;
        itv->_ny =0;
        itv->_nz =0;

        std::vector<int>::iterator fit = itv->_faceidx.begin();
        for( ; fit != itv->_faceidx.end(); fit++)
        {

            itv->_nx += tmesh->_triangles[(*fit)].tri_normal._x;
            itv->_ny += tmesh->_triangles[(*fit)].tri_normal._y;
            itv->_nz += tmesh->_triangles[(*fit)].tri_normal._z;
        }
        normalize3(itv->_nx, itv->_ny, itv->_nz);
    }
    return tmesh;
}

float CFuseMeshThread::compute_dist_pnt(aux_pix_data * p0, aux_pix_data *p1)
{
    float x0 = p0->_x;
    float y0 = p0->_y;
    float z0 = p0->_z;

    float x1 = p1->_x;
    float y1 = p1->_y;
    float z1 = p1->_z;

    return std::sqrt((x0-x1)*(x0-x1) + (y0-y1)*(y0-y1) + (z0-z1)*(z0-z1));
}

void CFuseMeshThread::fill_gaps_in_merged_data_triv(CArray2D<aux_pix_data *> * merged_data)
{
    /// for averaging use truth only points!!!!

    int xs = merged_data->_xs;
    int ys = merged_data->_ys;

    CArray2D<bool> mask(xs,ys,true);

    for (int j = 1; j < ys-1; j++)
    {
        for (int i = 1; i < xs-1; i++)
        {


            if(merged_data->_data[j][i] == NULL)
            {
                   std::vector<aux_pix_data*> neighb;
                   if(merged_data->_data[j][i-1]!= NULL && mask._data[j][i-1])
                         neighb.push_back(merged_data->_data[j][i-1]);

                    if(merged_data->_data[j][i+1]!= NULL && mask._data[j][i+1])
                          neighb.push_back(merged_data->_data[j][i+1]);

                    if(merged_data->_data[j+1][i]!= NULL && mask._data[j+1][i])
                          neighb.push_back(merged_data->_data[j+1][i]);

                    if(merged_data->_data[j-1][i]!= NULL && mask._data[j-1][i])
                          neighb.push_back(merged_data->_data[j-1][i]);


                    if(merged_data->_data[j-1][i-1]!= NULL && mask._data[j-1][i-1])
                          neighb.push_back(merged_data->_data[j-1][i-1]);

                    if(merged_data->_data[j+1][i+1]!= NULL && mask._data[j+1][i+1])
                           neighb.push_back(merged_data->_data[j+1][i+1]);

                    if(merged_data->_data[j-1][i+1]!= NULL && mask._data[j-1][i+1])
                           neighb.push_back(merged_data->_data[j-1][i+1]);

                    if(merged_data->_data[j+1][i-1]!= NULL && mask._data[j+1][i-1])
                           neighb.push_back(merged_data->_data[j+1][i-1]);

                    float nsz = neighb.size();
                    if(nsz > 3)
                    {
                        merged_data->_data[j][i] = new aux_pix_data;
                        mask._data[j][i] = false;

                        merged_data->_data[j][i]->_idx = neighb.front()->_idx;
                        merged_data->_data[j][i]->_langle= neighb.front()->_langle;


                        std::vector<aux_pix_data*>::iterator it = neighb.begin();
                        for( ; it != neighb.end(); it++)
                        {
                            merged_data->_data[j][i]->_u += (*it)->_u;
                            merged_data->_data[j][i]->_v += (*it)->_v;
                            merged_data->_data[j][i]->_x += (*it)->_x;
                            merged_data->_data[j][i]->_y += (*it)->_y;
                            merged_data->_data[j][i]->_z += (*it)->_z;
                            merged_data->_data[j][i]->_gx += (*it)->_gx;
                            merged_data->_data[j][i]->_gy += (*it)->_gy;
                            merged_data->_data[j][i]->_gz += (*it)->_gz;
                        }

                        merged_data->_data[j][i]->_u /= nsz;
                        merged_data->_data[j][i]->_v /= nsz;
                        merged_data->_data[j][i]->_x /= nsz;
                        merged_data->_data[j][i]->_y /= nsz;
                        merged_data->_data[j][i]->_z /= nsz;
                        merged_data->_data[j][i]->_gx /= nsz;
                        merged_data->_data[j][i]->_gy /= nsz;
                        merged_data->_data[j][i]->_gz /= nsz;


                    }
            }
        }
    }

}


CArray2D<aux_pix_data *> * CFuseMeshThread::simplify_polar_map(CArray2D< std::list<aux_pix_data *> > * polar_map)
{
    int ploar_map_width = polar_map->_xs;
    int polar_map_height = polar_map->_ys;

    CArray2D<aux_pix_data *> * mdata = new CArray2D<aux_pix_data *>(ploar_map_width, polar_map_height);
    for (int j=0; j < polar_map_height; j++)
    {
        emit send_back(j * 100 / polar_map_height);
    for (int i=0; i < ploar_map_width; i++)
    {

        int total_count = 0;
        aux_pix_data sum_dd;
        for (int y=-2; y <= 2; y++)
        {
        for (int x=-2; x <= 2; x++)
        {
            int ry = (j+y) ;
            int rx = (i+x) ;
            if(ry < 0) ry = 0;
            if(ry >= polar_map_height) ry = polar_map_height-1;
            if(rx < 0) rx += ploar_map_width;
            if(rx >= ploar_map_width) rx -= ploar_map_width;


            ///std::cout << rx << " " << ry << std::endl;

            int lsz = polar_map->_data[ry][rx].size();
            if( lsz != 0)
            {
                std::list<aux_pix_data *>::iterator itL  = polar_map->_data[ry][rx].begin();

                for ( ; itL  != polar_map->_data[ry][rx].end()  ; itL++ )
                {
                    sum_dd._x += (*itL)->_x;
                    sum_dd._y += (*itL)->_y;
                    sum_dd._z += (*itL)->_z;
                    total_count ++;
                }
            }

        }
        }

        if(total_count > 0)
        {
            aux_pix_data * dd = new aux_pix_data;
            dd->_x =  sum_dd._x / float(total_count);
            dd->_y =  sum_dd._y / float(total_count);
            dd->_z =  sum_dd._z / float(total_count);

            mdata->_data[j][i] = dd;
        }

    }
    }


   /*  QString filname = QString("__mdat_polar_map.raw");
    std::ofstream dataf;
    dataf.open (filname.toStdString().c_str(), std::ios::out| std::ios::binary);
    for (int j=0; j < polar_map_height; j++)
    for (int i=0; i < ploar_map_width; i++)
    {

      float val = (mdata->_data[j][i] != NULL)? mdata->_data[j][i]->_y : 0;
      dataf.write((char*)&val,  sizeof(float));
    }
     dataf.close();*/

    return mdata;
}
