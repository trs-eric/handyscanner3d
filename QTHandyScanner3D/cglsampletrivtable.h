#ifndef CGLSAMPLETRIVTABLE_H
#define CGLSAMPLETRIVTABLE_H

#include "CGLObject.h"
#include  <string>

class CGLSampleTrivTable : public CGLObject
{
public:
    CGLSampleTrivTable();
    virtual ~CGLSampleTrivTable();

    void set_pos(float, float, float);
protected:
    virtual void create_glList();
    void draw_circle(GLfloat radius, GLfloat opacity);
    void draw_foot(GLfloat radius, GLfloat opacity);

    void draw_tangents(GLfloat radius, GLfloat opacity);
    void create_tex();

    std::string _tbl_img_name;
    GLuint _tex_matid;

};

#endif // CGLSAMPLETRIVTABLE_H
