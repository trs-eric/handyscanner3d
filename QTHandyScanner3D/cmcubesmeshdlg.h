#ifndef CMCUBESMESHDLG_H
#define CMCUBESMESHDLG_H

#include <QDialog>
#include "carray3d.h"
#include "coctocube.h"
class CGLWidget;
class  CTriangularMesh;

namespace Ui {
class CMCubesMeshDlg;
}

class CMCubesMeshDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CMCubesMeshDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CMCubesMeshDlg();

private:
    Ui::CMCubesMeshDlg *ui;
    CGLWidget *_glWidget;

   // inline void set_in_matrix_neighbourhood( CArray3D<uchar> * dmtrix, int i, int j, int k);
   // inline void set_in_matrix_neighbourhood_improved( CArray3D<uchar> * dmtrix, int i, int j, int k, int neighb_sz);
   // inline void set_in_octocube_neighbourhood( COctoCube<float> *, float x, float y, float z, int neighb_sz);

signals:
    void send_result(CTriangularMesh *);
    void send_fpcl(std::vector<C3DPoint> * );


private slots:
    void onCancelBtn();
    void onComputeBtn();
    void odepth_changed(QString);

  //  void onComputeBtn_old();

protected:

    int _matrix_dim_N;
    float _nei_windsz_factor;
    float _neighb_size_f;
    int   _neighb_size_i;

    float _midle_fill_vlue;

    double * _gauss_kernel;
    int _gkernel_size;
    float _gsigma;

    void init_gauss_kernel();
};

#endif // CMCUBESMESHDLG_H
