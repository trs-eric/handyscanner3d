#ifndef CGLALIGNMENTCLOUDDLG_H
#define CGLALIGNMENTCLOUDDLG_H

#include <QDialog>
#include "graphicutils.h"
class CGLWidget;


namespace Ui {
class CGLAlignmentCloudDlg;
}

class CGLAlignmentCloudDlg : public QDialog
{
    Q_OBJECT

public slots:
    void close_dlg();
    void onApplyBtn();
    void onPreviewBtn();

protected:
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

public:
    explicit CGLAlignmentCloudDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CGLAlignmentCloudDlg();

private:
    Ui::CGLAlignmentCloudDlg *ui;
    CGLWidget *_glWidget;

    void restore_scene();
    void fix_cloud_normals();

    std::vector< std::vector<C3DPoint> > _orig_rpnts;

};

#endif // CGLALIGNMENTCLOUDDLG_H
