#ifndef CPCLCOLORINGDLG_H
#define CPCLCOLORINGDLG_H

#include <QDialog>

#include <QMessageBox>


class CGLWidget;

namespace Ui {
class CPCLColoringDlg;
}


class CPCLColoringDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CPCLColoringDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CPCLColoringDlg();
protected:
    void showEvent(QShowEvent * event);

private:
    Ui::CPCLColoringDlg *ui;

  CGLWidget *_glWidget;
private slots:
    void onCancelBtn();
    void onPreviewBtn();
    void onApplyBtn();
    void onBrightChanged(int );
    void onContrastChanged(int );
    void onGammaChanged(int );
};

#endif // CPCLCOLORINGDLG_H
