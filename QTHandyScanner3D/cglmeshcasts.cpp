#include "cglmeshcasts.h"
#include "settings/psettings.h"

CGLMeshCasts::CGLMeshCasts() : _cmap_size(255)
{
    init_color_map();
}

void CGLMeshCasts::init_color_map()
{   //std::map<int, cv::Point3f> _colormap;
    for (int c = 0; c < _cmap_size; c++)
    {
        cv::Point3f clr;
        clr.x = 0.;
        clr.y = 0.;
        clr.z = 0.;

        // red channel
        if((c < (_cmap_size/6)) || (c >= (5*_cmap_size/6)))
            clr.x = 1.0f;
        if(c >= (_cmap_size/6) &&  c < (_cmap_size/3) )
            clr.x = 1.0f - float(6*c - _cmap_size)/ float(_cmap_size);
        if(c >= (2*_cmap_size/3) && c < (5*_cmap_size/6) )
            clr.x = 2.0f * float(3*c - 2*_cmap_size)/ float(_cmap_size);

        // green channel
        if(c >= 0 &&  c < (_cmap_size/6))
            clr.y = float(6*c)/float(_cmap_size);
        if(c >= (_cmap_size/6) &&  c < (_cmap_size/2) )
            clr.y = 1.0f;
        if(c >= (_cmap_size/2) &&  c < (2*_cmap_size/3))
            clr.y = 1.0f - 3.0f * float(2*c -_cmap_size)/float(_cmap_size);

        // blue channel
        if( c >= (_cmap_size/3) && c < (_cmap_size/2) )
            clr.z = 2.0f * float(3*c - _cmap_size)/float(_cmap_size);
        if(c >= (_cmap_size/2) && c < (5*_cmap_size/6))
            clr.z = 1.0f;
        if(c >= (5*_cmap_size/6) )
            clr.z = 1.0f - float(6*c - 5 * _cmap_size)/float(_cmap_size);

        _colormap[c] = clr;
    }

}

CGLMeshCasts::~CGLMeshCasts()
{
    std::map<int, CTriangularMesh*>::iterator it =   _mcasts.begin();
    for( ; it != _mcasts.end(); it++ )
        delete it->second;
    _mcasts.clear();
}

CTriangularMesh::RenderingMode CGLMeshCasts::get_rendering_mode()
{
    if(_mcasts.size() != 0)
        return _mcasts.begin()->second->get_rendering_mode();
    else
        return CTriangularMesh::MESH_FLAT;
}

void CGLMeshCasts::set_rendering_mode(CTriangularMesh::RenderingMode rm)
{
    std::map<int, CTriangularMesh*>::iterator it = _mcasts.begin();
    for( ; it != _mcasts.end(); it++ )
        it->second->set_rendering_mode(rm);
}

void CGLMeshCasts::add_new_meshcast(int fidx, CTriMesh * m)
{
   std::map<int, CTriangularMesh*>::iterator itf =   _mcasts.find(fidx);
   if(itf != _mcasts.end())
       delete itf->second;

   CTriangularMesh * cm  = new CTriangularMesh;

   int color_idx = fidx * _cmap_size / CGlobalHSSettings::glob_max_frames;
   cv::Point3f clr =  _colormap[color_idx];
   cm -> _color[0] = clr.x;
   cm -> _color[1] = clr.y;
   cm -> _color[2] = clr.z;
   cm -> use_loc_coloring();
   cm -> set_mesh(m);
   _mcasts[fidx] = cm;
}

void CGLMeshCasts::glDraw()
{
    std::map<int, CTriangularMesh*>::iterator it =   _mcasts.begin();
    for( ; it != _mcasts.end(); it++ )
        it->second->glDraw();
}

///
