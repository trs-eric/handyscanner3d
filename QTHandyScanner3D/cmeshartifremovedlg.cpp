#include "cmeshartifremovedlg.h"
#include "ui_cmeshartifremovedlg.h"
#include "cglwidget.h"

#include <QApplication>
#include "settings/psettings.h"

CMeshArtifRemoveDlg::CMeshArtifRemoveDlg(CGLWidget *glWidget ,QWidget *parent) :
    QDialog(parent),  _glWidget (glWidget),
    ui(new Ui::CMeshArtifRemoveDlg)
{
    Qt::WindowFlags flags = windowFlags();
    Qt::WindowFlags helpFlag = Qt::WindowContextHelpButtonHint;
    flags = flags & (~helpFlag);// & Qt::WindowStaysOnTopHint;
    setWindowFlags(flags);
    //setWindowFlags(Qt::WindowStaysOnTopHint);

    ui->setupUi(this);

    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->btnProcess,  SIGNAL(clicked()), this, SLOT(process_selected_faces()));

    connect(ui->btnDeselect,  SIGNAL(clicked()), this, SLOT(deselected_faces()));

    ui->chkUseRT->setChecked(true);


    connect(ui->chkUseRT, SIGNAL(clicked()), this, SLOT(change_selection_mode()));

}

void CMeshArtifRemoveDlg::change_selection_mode()
{
    g_use_ray_tracing = ui->chkUseRT->isChecked();
}

void CMeshArtifRemoveDlg::height_changed(int v)
{
}

CMeshArtifRemoveDlg::~CMeshArtifRemoveDlg()
{
    delete ui;
}

void CMeshArtifRemoveDlg::process_selected_faces()
{
    ui->btnProcess->setEnabled(false);
    ui->cancelBtn->setEnabled(false);
            QApplication::setOverrideCursor(Qt::WaitCursor);
      _glWidget->process_selected_faces();
            QApplication::restoreOverrideCursor();
    ui->btnProcess->setEnabled(true);
    ui->cancelBtn->setEnabled(true);


    close();
}

void CMeshArtifRemoveDlg::deselected_faces()
{

    QApplication::setOverrideCursor(Qt::WaitCursor);
    _glWidget->clear_selected_faces();
    QApplication::restoreOverrideCursor();
}

void CMeshArtifRemoveDlg::hideEvent(QHideEvent * e)
{
    _glWidget->set_view_scene_mode(CGLWidget::DEF_VIEW_MODE);

}
