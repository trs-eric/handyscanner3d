#ifndef CCOMPUTESPARSEPCLDLG_H
#define CCOMPUTESPARSEPCLDLG_H

#include <QDialog>
#include <QMutex>
#include <QWaitCondition>

class CGLWidget;
class CSPCLConstrThread;
class CFramePrepThread;

namespace Ui {
class CComputeSparsePCLDlg;
}

class CComputeSparsePCLDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CComputeSparsePCLDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CComputeSparsePCLDlg();

protected:

    QMutex _syncro_mutex;
    QWaitCondition _wait_frames_cond;

    CSPCLConstrThread *_spcl_thread;
    CFramePrepThread  * _framep_thread;

    void showEvent(QShowEvent * event);

private:
    Ui::CComputeSparsePCLDlg *ui;


    CGLWidget *_glWidget;

private slots:
    void onCancelBtn();
    void onComputeBtn();
    void onInfoComplete(double, double, double, double);

    void close_dialog();
};

#endif // CCOMPUTESPARSEPCLDLG_H
