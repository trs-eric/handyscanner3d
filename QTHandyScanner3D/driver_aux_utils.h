#ifndef DRIVER_AUX_UTILS_H
#define DRIVER_AUX_UTILS_H
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "driver/ftd2xx.h"
#include "driver/types.h"
#include "driver/mt9p006.h"
#include "driver/fr101.h"


static void sdramTest(void);
static void blockReadTest(u8* buffer);
u16 getSnapshotPixelValue(const u8* buffer, int x, int y, int width);
void convertSnapshotToPPM(const u8* buffer, const char* pFilename);
void testSnapshot(void);
void step4(u8 step);
void stepMotorTest(void);


#endif // DRIVER_AUX_UTILS_H
