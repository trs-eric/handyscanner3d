#ifndef CTEXTURINGDLG_H
#define CTEXTURINGDLG_H

#include <QDialog>

class CGLWidget;
class CTriangularMesh;
class CTexturingThread;

namespace Ui {
class CTexturingDlg;
}

class CTexturingDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CTexturingDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CTexturingDlg();

protected:
    void showEvent(QShowEvent * event);

private:
    Ui::CTexturingDlg *ui;
    CGLWidget *_glWidget;


    bool _in_process;
    bool _on_apply;
    bool _is_result_valid;
    CTexturingThread * _thread;

private slots:
    void onCancelBtn();
    void onPreviewBtn();
    void onApplyBtn();
    void onLoadINIBtn();

    void onMakeProgressStep(int val);
    void onCompleteProcessing(/*CTriangularMesh **/ );
    void onStopProcessing();

    void close_dialog();
};

#endif // CTEXTURINGDLG_H
