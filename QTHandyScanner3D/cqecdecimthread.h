#ifndef CQECDECIMTHREAD_H
#define CQECDECIMTHREAD_H

#include <QThread>
#include <QMutex>
#include "graphicutils.h"
#include "ctriangularmesh.h"
#include "./meshlab/mesho.h"

class CQECDecimThread : public QThread
{
    Q_OBJECT
public:
    explicit CQECDecimThread(QObject *parent = 0);
    ~CQECDecimThread();

    void start_process(CTriangularMesh * mesh,
                       int, int, float, bool, float,
                       bool, bool, bool,
                       bool, bool, bool );
    bool is_abort(){return _abort;}
    void emit_send_back(int i);

signals:
    void send_back(const int val);
    void send_result(CTriangularMesh *);


protected:
    void run();
    void  QuadricSimplification(CMeshO &m,
                               int  TargetFaceNum,
                               vcg::tri::TriEdgeCollapseQuadricParameter &pp);
public slots:
    void stop_process();


private:
    bool    _abort;
    QMutex  _mutex;

    CTriangularMesh * _inmesh;
    int     _tar_num;
    int     _perc;
    float   _qual;
    bool    _pbound;
    float  _boundw;
    bool _pnorm;
    bool _ptopo;
    bool _optimpos;
    bool _psimp;
    bool _weisimp;
    bool _postsimp;



};

#endif // CQECDECIMTHREAD_H
