#ifndef CSPCLCONSTRTHREAD_H
#define CSPCLCONSTRTHREAD_H

#include <QThread>
#include <QMutex>
#include <QWaitCondition>

#include <opencv2/core/core.hpp>

#include "graphicutils.h"

class CFrame;

class CSPCLConstrThread : public QThread
{
    Q_OBJECT
public:
    explicit CSPCLConstrThread(QObject *parent = 0);
    ~CSPCLConstrThread();
    void start_process(QMutex * smutex, QWaitCondition * wcond);

signals:
    void send_spcl(std::vector<C3DPoint> * cpoints);
    void send_resulting_info(double tt, double tav, double ret, double reav );
    void send_meshcast(int, CTriMesh*);

//public slots:
//     void stop_process();

protected:
     void run();

     QMutex * _syncro_mutex;
     QWaitCondition * _wait_cond_suffice_keyframes;

     /// cv::Point3f find_3d_offset_for_camera_pose(CFrame *, CFrame *);
     /// cv::Point3f improve_z_offset_iter(int iteration_number, cv::Point3f & offT,
     ///                                  CFrame * prev_frame, CFrame * curr_frame, double & repro_error);
     /// double compute_average_reproj_error(cv::Mat & camP, cv::Mat & camK,
     ///                                    CFrame *f1, CFrame *f2, double cutoff_min, double cautoff_max);

     /// std::vector<C3DPoint> * construct_spcl_for_frame(CFrame *);

     /// cv::Point3f find_3d_offset_with_homography(CFrame * prev_frame, CFrame * curr_frame, double & repro_error);


     cv::Point3f find_3d_offset_with_icp(CFrame * prev_frame, CFrame * curr_frame, cv::Point3f & initT, double & repro_error);
     cv::Point3f improve_camera_pose_with_rg(CFrame * prev_frame, CFrame * curr_frame, cv::Point3f & inT, double & in_repro_error);


     ///bool    _abort;
     ///QMutex  _mutex_abort;
};

#endif // CSPCLCONSTRTHREAD_H
