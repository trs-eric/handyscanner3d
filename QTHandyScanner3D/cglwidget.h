#ifndef CGLWIDGET_H
#define CGLWIDGET_H


#include <QGLWidget>
#include <vector>
#include "cglscene.h"
#include "menu_vew_actions.h"

class RadianceScalingRendererPlugin;

class CGLWidget : public QGLWidget
{
    Q_OBJECT
public slots:
    void discard_raw_points_of_current_object();

public:

    enum viewSceneMode {DEF_VIEW_MODE = 0, SEL_VIEW_MODE} ;
    enum vertSelMode {SEL_NORM = 0, SEL_ADD, SEL_SUB, SEL_POLY};

    explicit CGLWidget(QWidget * treew, MenuActions * va, QWidget *parent = 0);
    ~CGLWidget();

    void set_view_scene_mode(viewSceneMode v);
    void updating_view_scene_mode();
    void updating_view_scene_selecet_mode();
    void clear_selected_faces();

    CGLScene  * get_scene() {return _scene;};
    void import_scene(QString &fname);
    void export_scene(QString &fname);


    bool load_project(QString &fname);
    void save_project(QString &fname);
    void finilize_loading_from_rub();

    void udate_gl();
    ///void update_gl_settings();

    CPointCloud * get_raw_points_of_current_object();
    void delete_selected_points();
    void process_selected_faces();

    void discard_optim_points_of_current_object();
    CPointCloud * get_optim_points_of_current_object();

    void discard_normal_points_of_current_object();
    CNormalCloud * get_normals_of_current_object();

    void discard_mesh_of_current_object();
    CTriangularMesh * get_mesh_of_current_object();

    std::vector<C3DPoint> * get_dpcl_of_current_object();

    std::vector<C3DPoint> * get_merged_dpcl_of_scene();

    CTriangularMesh * get_fused_mesh();


    std::vector<CGLCompoundObject *> get_list_of_valid_meshes();
    CTriangularMesh * find_mesh_by_its_name(QString);

    void make_new_scene();
    void delete_scene();

    void compute_proper_col_width();

    void construct_single_scan(std::vector<C3DPoint> & pnts);

    void chainge_view_mode_of_current_object(CGLCompoundObject::ViewingMode vm);
    void set_mesh_mode_chainge_rmode_of_current_object(CTriangularMesh::RenderingMode rm);
    void set_view_camera_path(bool v);
    void set_view_shad_points(bool v);


    RadianceScalingRendererPlugin * get_radiance_render()
        {return _scene->get_radiance_render();}



signals:
    void def_vmode();

public slots:

    void on_dpcl_computed(std::vector<C3DPoint> * pnts);

    void on_rawpnts_updated();
    void on_raw_pnts_computed(CPointCloud*);
    void on_optim_pnts_computed(CPointCloud*);
    void on_normal_pnts_computed(CNormalCloud*);
    void on_mesh_computed(CTriangularMesh *);
    void on_new_fused_mesh_computed(CTriangularMesh *);

    void on_mesh_updated();
    void on_new_mesh_computed(CTriangularMesh *);
    void on_new_cglobject_computed(std::vector<C3DPoint> *, CTriangularMesh *, float, float, float);

    void on_double_click_treew_item(QTreeWidgetItem *, int);
    void on_single_click_treew_item(QTreeWidgetItem *, int);
    void construct_next_scan(std::vector<C3DPoint> * pnts);
    void add_new_globj(CGLObject*);

    void construct_new_scan(std::vector<C3DPoint> * pnts);
    void send_clean_all_scans();
    void send_clean_scan();

    void prepaire_new_scan();

    void construct_next_scan_left(std::vector<C3DPoint> * pnts);
    void construct_next_scan_right(std::vector<C3DPoint> * pnts);
    void construct_next_texture_left(CRawTexture *);
    void construct_next_texture_right(CRawTexture *);

    void clean_current_scan();

    void on_new_campos();
    void on_table_pos();
    void on_reset_camera_pos();

    void construct_next_meshcast(int, CTriMesh*);

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseDoubleClickEvent( QMouseEvent * event);

    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent ( QWheelEvent* event);
    // void	closeEvent	( QCloseEvent* event	);
    void keyReleaseEvent(QKeyEvent *event);
    void keyPressEvent(QKeyEvent *event);

    CGLScene * _scene;
    QPoint _mouseLastPos;
    QWidget * _treew;
    MenuActions * _va;

    int _update_counter;

    viewSceneMode _vmode;
    vertSelMode _vselmode;
    QPoint _mouseSelStPos;
    QPixmap _cursor_snorm_px;
    QPixmap _cursor_ssub_px;
    QPixmap _cursor_sadd_px;
    QPixmap _cursor_spoly_px;


};

#endif // CGLWIDGET_H
