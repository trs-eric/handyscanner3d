#include "cdpclconstructionthread.h"

#include "cframe.h"

CDPCLConstructionThread::CDPCLConstructionThread(QObject *parent) :
    QThread(parent), _decim_step(0), _neighb_count(0)
{
    _abort = false;
}

CDPCLConstructionThread::~CDPCLConstructionThread()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
    wait();
}

void CDPCLConstructionThread::start_process( int decim_step, int neighb_count)
{
    _abort = false;
    _decim_step = decim_step;
    _neighb_count = neighb_count ;

    start();
}

void CDPCLConstructionThread::stop_process()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
}

void CDPCLConstructionThread::run()
{
    std::vector<C3DPoint> * dpcl_pnts = new std::vector<C3DPoint>;

    /// initializ auxulary maps
    std::vector<CFrame *> sorted_sequence;
    std::vector<CFrame *>::iterator itr = global_keyframes_sequence.begin();
    for( ; itr != global_keyframes_sequence.end(); itr++ )
    {
            (*itr)->adjust_angular_range();
            sorted_sequence.push_back(*itr);
    }

    /// sort frames according to their angular distance to (0,0)
    std::sort(sorted_sequence.begin(), sorted_sequence.end(), CFrame::cmp_angular_dist);

    unsigned long int total_points = 0;
    float total_repo_error = 0;

    std::vector<CFrame *>::iterator its = sorted_sequence.begin();
    for( int i = 0; its != sorted_sequence.end(); its++, i++ )
    {
        if (_abort)
        {
            emit send_back(0);
            return;
        }

        std::cout << "frame prcoessing : " << i << std::endl;
        if(i == 0)
        {
            (*its)->load_LR_images_from_hd();
            (*its)->compute_disparity_map();
            (*its)->compute_depth_image();

            int pnts_size =(*its)->construct_dpcl_from_depth_img_with_norms(dpcl_pnts, _decim_step,_neighb_count);
            total_points += pnts_size;

            (*its)->dump_unused_data();
            continue;
        }

        CFrame * cf = *(sorted_sequence.begin());
        std::vector<CFrame *>::iterator iti = sorted_sequence.begin();
        for( ; iti != its; iti++ )
        {
            if(((*its)->get_angular_distance(*iti) - (*its)->get_angular_distance(cf))==0)
            {
                if(std::abs((*its)->_id - (*iti)->_id) < std::abs((*its)->_id - cf->_id) )
                    cf = *iti;
            }
            else
                if((*its)->get_angular_distance(*iti) < (*its)->get_angular_distance(cf))
                     cf = *iti;
        }

        ///global_keyframes_sequence_corr[*its] = cf;
        std::cout << (*its)->_id << " " << cf->_id << std::endl;

        CFrame * curr_frame = *its;
        CFrame * prev_frame = cf;

        // process current frame
        (curr_frame )->load_LR_images_from_hd();
        (curr_frame )->compute_disparity_map();
        (curr_frame )->compute_depth_image();
        (curr_frame )->compute_keypoints_with_desc();

        // process previous frame
        (prev_frame )->load_LR_images_from_hd();
        (prev_frame )->compute_disparity_map();
        (prev_frame )->compute_depth_image();
        (prev_frame )->compute_keypoints_with_desc();

        (*its)->match_features(cf);


        double repro_error = 0;
        cv::Point3f realT = curr_frame->find_3d_offset_with_homography(prev_frame, repro_error);

        total_repo_error += repro_error;
        std::cout << "2: reprojection - error" << repro_error << std::endl;

        curr_frame->udate_global_and_relat_pose(prev_frame,  realT);

        int pnts_size =curr_frame->construct_dpcl_from_depth_img_with_norms(dpcl_pnts, _decim_step,_neighb_count);
        total_points += pnts_size;

        curr_frame ->dump_unused_data();
        prev_frame ->dump_unused_data();

        emit send_back(float(i+1)*100/float(sorted_sequence.size()));
        emit send_newcam();
    }
    std::cout << "total repo error: " << total_repo_error << " " << total_repo_error/float(sorted_sequence.size()) << std::endl;

     emit send_dpcl(dpcl_pnts);
    /// TODO
    /// send_dpcl back
}
