#include "ccaliblaserangledlg.h"
#include "ui_ccaliblaserangledlg.h"

#include <QTimer>
#include "settings/psettings.h"
#include "img2space/edgedetection.h"

#include <QMessageBox>
#include <QThread>

CCalibLaserAngleDlg::CCalibLaserAngleDlg(cv::VideoCapture * cam, PVOID dh, QWidget *parent) :
    QDialog(parent),  _current_cam(cam), _dev_handle(dh),
    _max_good_frames(8),
    ui(new Ui::CCalibLaserAngleDlg)
{
    ui->setupUi(this);

    connect(ui->cancelBtn, SIGNAL(clicked()), this, SLOT(close_dlg()));
    connect(ui->btnStart, SIGNAL(clicked()), this, SLOT(start_calib()));

    for (int j=0; j < g_marks_y ; j++)
    for (int i=0; i < g_marks_x; i++)
            _boardPoints.push_back(cv::Point3f(i *  g_single_cell_width , j *  g_single_cell_width , 0.0f));


    ui->lblMsg->setText("Please place the board in front of the camera<br> with the laser line on its left side.");

    _timer = new QTimer(this);
     connect(_timer, SIGNAL(timeout()), this, SLOT(update_main_wnd()));


}

CCalibLaserAngleDlg::~CCalibLaserAngleDlg()
{
    delete ui;
    delete _timer;
}

void CCalibLaserAngleDlg::update_main_wnd()
{
    cv::Mat frame;
   bool Succ = _current_cam->read(frame);

   if(!Succ) return;

    int wi = frame.cols;
    int he = frame.rows;
    int dx = 300;
    int dy = 150;


    cv::line(frame, cv::Point(dx,dy), cv::Point(wi-dx,dy),   cv::Scalar(0,255,0),2);
    cv::line(frame, cv::Point(dx,he-dy), cv::Point(wi-dx, he-dy),  cv::Scalar(0,255,0),2);
    cv::line(frame, cv::Point(dx,dy), cv::Point(dx,he-dy),   cv::Scalar(0,255,0),2);
    cv::line(frame, cv::Point(wi-dx,dy), cv::Point(wi-dx, he-dy),   cv::Scalar(0,255,0),2);

    cv::line(frame, cv::Point(dx+50,dy/2), cv::Point(dx+50,he - dy/2),   cv::Scalar(0,0,255),3);

    emit update_main_preview(frame);
}

void CCalibLaserAngleDlg::showEvent(QShowEvent * event)
{
     emit stop_main_preview();
    _timer->start(300);


}


void CCalibLaserAngleDlg::hideEvent(QHideEvent * event)
{
    _timer->stop();
    ui->progressBar->setValue(0);
    ui->btnStart->setEnabled(true);
    if(_dev_handle != NULL)
    {
        driv::motor_rotate_signal(_dev_handle, 2);
        driv::set_M0(_dev_handle, 1);
        driv::set_M1(_dev_handle, 2);
    }
    emit start_main_preview();
}

void CCalibLaserAngleDlg::start_calib()
{
    _timer->stop();
    ui->lblMsg->setText("Calibrating laser, please wait ...");

    if(_dev_handle != NULL)
    {
        driv::laser_turn_off(_dev_handle);
        QThread::msleep(900);
    }

    ui->btnStart->setEnabled(false);
    ui->progressBar->setMaximum(_max_good_frames);
    cv::Mat frame;
    while(!_current_cam->read(frame));
    while(!_current_cam->read(frame));

    frame.copyTo(_bckg);

    /////////////// turn on laser
    if(_dev_handle != NULL)
    {
        driv::set_M0(_dev_handle, 1);
        driv::set_M1(_dev_handle, 1);

        driv::laser_turn_on(_dev_handle);
        driv::motor_dir_change(_dev_handle,1); // to the right
        driv::motor_rotate_signal(_dev_handle, 1); // start
    }

    _total_tries = 0;
    _good_frames = 0;
    _last_angle = 0;

    single_timer_shoot();


}

void CCalibLaserAngleDlg::close_dlg()
{
    ui->btnStart->setEnabled(true);
    close();
}

void compute_parametric(float x, float y, float x00, float y00,
                                      float x01, float y01,
                                      float x11, float y11,
                                      float x10, float y10,
                                      float & px, float & py)
{
    float A1 = x10 - x00;
    float B1 = x11 - x01 - x10 + x00;
    float C1 = x01 - x00;
    float D1 =  x -  x00;

    float A2 = y10 - y00;
    float B2 = y11 - y01 - y10 + y00;
    float C2 = y01 - y00;
    float D2 = y   - y00;

    float alpha = B1 * C2 - B2 * C1;
    float betta = C2 * A1 - A2 * C1 + B2 * D1 - D2 * B1;
    float gamma = A2 * D1 - D2 * A1;

    float Desc = betta * betta - 4 * alpha * gamma;

    if(alpha != 0)
    {
        ///// CHECK THIS  + or -
        px = (- betta - std::sqrt(Desc) )/ (2 * alpha);
        py = (D1 - C1 * px) /(A1 + B1 * px);
    }
    else
    {
        px = - gamma / betta;

        if(A1 + B1 * px != 0)
            py = (D1 - C1 * px) /(A1 + B1 * px);
        else
            py = (y - y01) / (y11 - y01);

    }

}

float CCalibLaserAngleDlg::convert_langle(int v)
{
    return  2 * cPI * float(v) / 16384.;

}

void CCalibLaserAngleDlg::capture_new_frame()
{
    float angle_step = driv::get_laser_angle(_dev_handle);

    cv::Mat frame;
    bool bSuccess = _current_cam->read(frame);
    if (bSuccess)
    {
        if(fabs (angle_step  -  _last_angle  ) < float(16384.) * 2. / 360.  ) // 2 degress
        {
            _total_tries++;
            return check_stop_condition();
        }

         _last_angle  = angle_step;

         cv::Mat frame_o;
         frame.copyTo(frame_o);


        cv::Size board_sz  = cv::Size(g_marks_x, g_marks_y);
        std::vector<cv::Point2f> corners;
        bool found = cv::findCirclesGrid(frame, board_sz, corners);
        if(!found)
        {
            _total_tries++;
            return check_stop_condition();
        }
        else
            cv::drawChessboardCorners(frame_o, board_sz, corners, found);



        emit update_main_preview(frame_o);

        cv::Mat rvec, tvec;
        cv::solvePnP( _boardPoints, corners, g_cam_intrinsics, g_cam_distortion, rvec, tvec, false );

        cv::Mat iMat;
        cv::Rodrigues(rvec, iMat);
        cv::Mat P = (cv::Mat_<double>(3,4) << iMat.at<double>(0,0), iMat.at<double>(0,1), iMat.at<double>(0,2), tvec.at<double>(0,0),
                                              iMat.at<double>(1,0), iMat.at<double>(1,1), iMat.at<double>(1,2), tvec.at<double>(1,0),
                                              iMat.at<double>(2,0), iMat.at<double>(2,1), iMat.at<double>(2,2), tvec.at<double>(2,0));

        cv::Mat rvec0 = (cv::Mat_<double>(1,3) <<  0, 0, 0 );
        cv::Mat tvec0 = (cv::Mat_<double>(1,3) <<  0, 0, 0 );

        std::vector<cv::Point3f> cam_boardPoints;
        std::vector<cv::Point3f>::iterator it = _boardPoints.begin();
        for( ; it != _boardPoints.end(); it++ )
        {
            cv::Point3f p = *it;
            cv::Mat pe = (cv::Mat_<double>(4,1) << p.x, p.y, p.z, 1.0 );
            cv::Mat pr = P * pe;
            cam_boardPoints.push_back(cv::Point3f(pr.at<double>(0,0),
                                                  pr.at<double>(1,0),
                                                  pr.at<double>(2,0) ));
        }


        QImage img_bg((uchar*)_bckg.data, _bckg.cols, _bckg.rows,  QImage::Format_RGB888);
        img_bg = img_bg.rgbSwapped();

        QImage img_in((uchar*)frame.data, frame.cols, frame.rows,  QImage::Format_RGB888);
        img_in = img_in.rgbSwapped();

        difference_image_fast_supp_red(&img_in, &img_bg, CGlobalSettings::_ignore_read_thresh);


        std::vector<CIPixel> pix = get_edge_points_lines_hills_1D_undistorted(&img_in, g_cam_intrinsics,
                                                                              g_cam_distortion);

        if(pix.size() < 25)
        {
            _total_tries++;
            return check_stop_condition();
        }

        std::vector<cv::Point2f> undist_corners;
        cv::Mat R = (cv::Mat_<double>(3,3) << 1, 0., 0,
                                             0.,  1, 0,
                                             0.,  0., 1. );
        cv::undistortPoints(corners, undist_corners, g_cam_intrinsics, g_cam_distortion, R, g_cam_intrinsics);

        std::vector<cv::Point2f> convex_hull; // inside rectangle
        convex_hull.push_back(undist_corners[1 + g_marks_x]);         // 0 0        // x o x x
        convex_hull.push_back(undist_corners[g_marks_x - 2 + g_marks_x]); // 0 1  // ... x o x
        convex_hull.push_back(undist_corners[( g_marks_y  - 2)*g_marks_x + g_marks_x - 2 ]);// 1 13  // ... x o x
        convex_hull.push_back(undist_corners[( g_marks_y - 2)*g_marks_x  + 1 ]);   //  1 0             // x o x x


        std::vector<cv::Point3f> convex_hull_3d; // inside rectangle
        convex_hull_3d.push_back(_boardPoints[1 + g_marks_x]);
        convex_hull_3d.push_back(_boardPoints[g_marks_x - 2 + g_marks_x]);
        convex_hull_3d.push_back(_boardPoints[( g_marks_y  - 2)*g_marks_x  + g_marks_x - 2 ]);
        convex_hull_3d.push_back(_boardPoints[( g_marks_y  - 2)*g_marks_x  + 1 ]);

        cv::Mat laser_orig = (cv::Mat_<double>(3,1) << -g_camera_laser_dist, 0, g_laser_depth_offset);
        cv::Point3f laser_orig_pnt (-g_camera_laser_dist, 0, g_laser_depth_offset);

        cv::Point3f plane_normal(0, 0, -1);
        float plane_dist = -tvec.at<double>(2,0);

        bool is_in_area = false;
        std::vector<CIPixel>::iterator itl = pix.begin();
        for ( int i = 0; itl != pix.end(); itl++, i++ )
        {
           if( (i % 5) ) continue;

            // is (u,v) in the grid
           if( cv::pointPolygonTest( convex_hull, cv::Point2f(itl->_u,itl->_v), false) > 0 )
           {
                 float  px =0, py =0; // [0,1]
                 compute_parametric(itl->_u, itl->_v, convex_hull[0].x, convex_hull[0].y,
                                                       convex_hull[1].x, convex_hull[1].y,
                                                       convex_hull[2].x, convex_hull[2].y,
                                                       convex_hull[3].x, convex_hull[3].y,
                                                       px, py);


                  float w00 = (1 - px) * (1 - py);
                  float w10 =      px  * (1 - py);
                  float w11 =      px  * py;
                  float w01 = (1 - px) * py;

                  cv::Point3f pres = convex_hull_3d[0] * w00 +
                                     convex_hull_3d[1] * w10 +
                                     convex_hull_3d[2] * w11 +
                                     convex_hull_3d[3] * w01 ;

                  cv::Mat mpres = (cv::Mat_<double>(4,1) << pres.x, pres.y, pres.z, 1);

                  cv::Mat laser_point_coord = P * mpres;

                  cv::Mat laser_vec = laser_point_coord - laser_orig;
                  cv::Point3f laser_vec_pnt (laser_vec.at<double>(0,0),laser_vec.at<double>(1,0), laser_vec.at<double>(2,0)  );


                  float nDotA = /*plane_normal.x * laser_orig.at<double>(0,0) +
                                plane_normal.y * laser_orig.at<double>(1,0) +
                                plane_normal.z **/ -laser_orig.at<double>(2,0);

                  float nDotBA = /*plane_normal.x * laser_vec.at<double>(0,0) +
                                 plane_normal.y * laser_vec.at<double>(1,0) +
                                 plane_normal.z **/ -laser_vec.at<double>(2,0);

                  float coeff = (plane_dist - nDotA)/nDotBA;
                   cv::Point3f isect_point = laser_orig_pnt + coeff * laser_vec_pnt;

                 float angleR = cPI  - atan2( fabs(isect_point.z - g_laser_depth_offset), fabs(isect_point.x + g_camera_laser_dist));
                 float angleE =  convert_langle(angle_step) ;
                 float angle_init = angleR - angleE;
                 _laser_init_cands.push_back(angle_init);
                 is_in_area = true;

            }
        }
        if(is_in_area )
        {
            _good_frames++;
            _total_tries = 0;
            ui->progressBar->setValue(_good_frames);

          /*  cv::Mat imgbg = frame;
             std::vector<cv::Point2f> projPnts2;
             cv::projectPoints(  isect_pnts , rvec0, tvec0, g_cam_intrinsics, g_cam_distortion,  projPnts2  );
             std::vector<cv::Point2f>::iterator itf = projPnts2.begin();
             for(  ; itf != projPnts2.end(); itf++  )
             {
               cv::circle( imgbg,  *itf, 4 ,CV_RGB(255,0,0) );
             }
             cv::imshow("image left",  imgbg);
             cv::waitKey(0);*/
        }

    }

    return check_stop_condition();
}


void CCalibLaserAngleDlg::calculate_laser_init_angle()
{
    std::sort(_laser_init_cands.begin(), _laser_init_cands.end());
    int beg =  _laser_init_cands.size() * 10 / 100;
    int end =  _laser_init_cands.size() * 90 / 100;

    int count = 0;
    float tangle = 0;
    for( int i = beg ; i < end ; i++)
    {
        tangle +=  _laser_init_cands[i];
        count++ ;
    }
    g_meta_angle_correction = IBGM(tangle/count);
}

void CCalibLaserAngleDlg::check_stop_condition()
{
    if(_total_tries >  100) // ? is it right?
    {
        if(_dev_handle != NULL)
        {
            driv::motor_rotate_signal(_dev_handle, 2);
            driv::set_M0(_dev_handle, 1);
            driv::set_M1(_dev_handle, 2);
        }

        QString msg = QString("Callibration is failed due to<br> the maximal number of unsuccessful tries.");
        QMessageBox::information(this, tr("Information"), msg);


        return close_dlg();
    }

    if(_good_frames > _max_good_frames ) // ? is it right?
    {
        if(_dev_handle != NULL)
        {
            driv::motor_rotate_signal(_dev_handle, 2);
            driv::set_M0(_dev_handle, 1);
            driv::set_M1(_dev_handle, 2);
        }

        calculate_laser_init_angle();

        QString msg = QString("Callibration succided. Initial laser angle is %1 deg.").arg(QString::number(g_meta_angle_correction));
        QMessageBox::information(this, tr("Information"), msg);

        return close_dlg();
    }
    single_timer_shoot();
}

void CCalibLaserAngleDlg::single_timer_shoot()
{
    QTimer::singleShot(100, this, SLOT(capture_new_frame()));
}


