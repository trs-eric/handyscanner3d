#include "cpclcoloringdlg.h"
#include "ui_cpclcoloringdlg.h"
#include "cglwidget.h"

CPCLColoringDlg::CPCLColoringDlg(CGLWidget * glWidget, QWidget *parent) :
    _glWidget(glWidget), QDialog(parent),
    ui(new Ui::CPCLColoringDlg)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);

    connect(ui->chkPreview, SIGNAL(clicked()), this, SLOT(onPreviewBtn()));
    connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

    connect(ui->slidBrightness,  SIGNAL(valueChanged(int)), this, SLOT(onBrightChanged(int)));
    connect(ui->slidContrast,  SIGNAL(valueChanged(int)), this, SLOT(onContrastChanged(int)));
    connect(ui->slidGamma,  SIGNAL(valueChanged(int)), this, SLOT(onGammaChanged(int)));
}
void CPCLColoringDlg::showEvent(QShowEvent * event)
{
    ui->slidGamma->setValue(10);
    ui->slidBrightness->setValue(0);
    ui->slidContrast->setValue(0);

    ui->chkPreview->setChecked(true);
}


void CPCLColoringDlg::onGammaChanged(int v )
{
    ui->lblGamma->setText(QString::number(float(v)/10));
    onPreviewBtn();
}

void CPCLColoringDlg::onBrightChanged(int v )
{
    ui->lblBright->setText(QString::number(v));
    onPreviewBtn();
}

void CPCLColoringDlg::onContrastChanged(int v )
{
    ui->lblContrast->setText(QString::number(v));
    onPreviewBtn();
}

void CPCLColoringDlg::onCancelBtn()
{
    std::vector<CGLCompoundObject*> cgllist = _glWidget->get_scene()->get_list_of_all_cglobjects();

    if(!cgllist.size())
    {
       close();
       return;
    }

    std::vector<CGLCompoundObject*>::iterator it = cgllist.begin();
    for( ; it != cgllist.end(); it++)
    {
        if((*it)->is_unique_object()) continue;

        CPointCloud * rp = (*it)->get_raw_points();

        rp->turn_off_tcs();
        rp->remake_glList();
    }
    _glWidget->update();

    close();
}

void CPCLColoringDlg::onPreviewBtn()
{


    std::vector<CGLCompoundObject*> cgllist = _glWidget->get_scene()->get_list_of_all_cglobjects();

    if(cgllist.size()==0)
       return;

    std::vector<CGLCompoundObject*>::iterator it = cgllist.begin();
    for( ; it != cgllist.end(); it++)
    {
        if((*it)->is_unique_object()) continue;

        CPointCloud * rp = (*it)->get_raw_points();

        float bright  = float(ui->slidBrightness->value())/255.;
        float contrast = float(ui->slidContrast->value() + 127  )/255.;
        float gamma = float(ui->slidGamma->value())/10.;


        if(!ui->chkPreview->isChecked())
        {
          bright = 0;
          contrast =0.5;
          gamma = 1;
        }
        rp->set_temp_coloring_scheme(bright, contrast, gamma);
    }

    _glWidget->update();


}

void CPCLColoringDlg::onApplyBtn()
{

    std::vector<CGLCompoundObject*> cgllist = _glWidget->get_scene()->get_list_of_all_cglobjects();

    if(!cgllist.size())
    {
       close();
       return;
    }

    std::vector<CGLCompoundObject*>::iterator it = cgllist.begin();
    for( ; it != cgllist.end(); it++)
    {
        if((*it)->is_unique_object()) continue;

        CPointCloud * rp = (*it)->get_raw_points();
        rp->apply_coloring_scheme();
    }
    _glWidget->update();

   close();

}

CPCLColoringDlg::~CPCLColoringDlg()
{
    delete ui;
}
