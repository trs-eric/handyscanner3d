#ifndef CCAPTURESEQUENCEDLG_H
#define CCAPTURESEQUENCEDLG_H

#include <QDialog>
#include <QTimer>
#include <ctime>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

///#include <QSerialPort>
#include "graphicutils.h"


#include "./driver/driver_aux_util.h"
#include "cdevcontrolthread.h"


namespace Ui {
class CCaptureSequenceDlg;
}

class CCaptureSequenceDlg : public QDialog
{
    Q_OBJECT

public slots:
        void closedlg();
        void start();
        void stop();

        void start_scan();
        void stop_scan();

        void browse();
        void capture_new_frame();
        void new_laserangle(int);
        void set_start_angle();
        void set_end_angle();
        void i2c_motor_right_pressed();
        void i2c_motor_left_pressed();
        void i2c_motor_stop();
        void read_and_update_lblA();

signals:
       void dev_getLA();
       void stop_main_preview();
       void start_main_preview();
       void update_main_preview(cv::Mat & frame);

public:
    explicit CCaptureSequenceDlg(cv::VideoCapture * ,   float roll, float pitch,
                                 float rcx, float rcy, float rcz,  float depth_can, PVOID dh,
                                 int eled, QWidget *parent = 0);
    ~CCaptureSequenceDlg();

protected:
    void hideEvent(QHideEvent * event);
    void showEvent(QShowEvent * event);

    QString serial_backinfo();
    void write_INI();

    void rotate_sample_table();
    float convert_langle(int v);
    void update_lblA(int v);
    void move_laser_start_position();
    void disable_interface();
    void enable_interface();
    void clean_video_structures();
    void single_timer_shoot();
    void record_texure();
    void record_additional_texture();

private:
    Ui::CCaptureSequenceDlg *ui;

    int _timer_period;
    bool _capturing;
    int _frame_count;
    int _total_tics;
    int _period_tics;

    cv::VideoCapture * _current_cam;

     QString _bckg_name;
     QString _desc_name;
     QString _vid_name;

    PVOID _dev_handle;

    QThread * _dev_ctrl_thread;
    CDevControlThread * _dev_ctrl;
    float _board_distance;

    float _roll;
    float _pitch;
    float _rc_x;
    float _rc_y;
    float _rc_z;


    int _skipped_frames;

    int _laser_angle;
    bool _langle_ready;


    bool _motor_cdir;
    QString _prefix;
    int _scan_num;
    int _scan_sz;
    int _scan_angle_step;

    float _angle_min;
    float _angle_max;

    cv::VideoWriter * _current_output_video;
    std::ofstream * _current_video_desc;
    bool _inside_capture;

    float _max_radi_to_cancel_pnts;

    QString _tex_path;
    bool _non_add_tex;
    int _eled;

};

extern float g_meta_angle_correction;

#endif // CCAPTURESEQUENCEDLG_H
