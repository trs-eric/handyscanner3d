#include "Image2SpaceModel.h"
#include <cmath>

#include "../metadata/qmetadata.h"

void image_to_space_xy(float u, int Nx, float fov_x,
                        float Sparam, float & Tx, float & Ty)
{
    float ux;
    if(Nx % 2)
        ux = u - float(Nx)/2.f;
        // ux = float(Nx)/2.f - u;
     else
        ux = u - float(Nx-1)/2.f ;
        // ux =  float(Nx-1)/2.f - u;

    float alpha_x = atan((2.0f * ux/ float(Nx-1) ) * (float)tan(fov_x / 2.0f));
    Tx = Sparam * sin(alpha_x);
    Ty = Sparam * cos(alpha_x);
}



void image_to_space_z(float v, int Ny, float fov_z,
                        float yp,  float h, float & zp)
{
    float vz;
    if(Ny % 2)
        vz = float(Ny)/2.f    - v;
    else
        vz = float(Ny-1)/2.f  - v;

    float tan_alpha_z = (2.f * vz / float(Ny-1) ) * (float)tan(fov_z / 2.0f);
    zp = tan_alpha_z * yp + h;
}

/*
float get_point_height(float iz, float py,  float d, float h)
{
    float ph = iz * (d-py) / d;
    float pz = ph + h;
    return pz;

}
*/

void segment_intersection_pnt(float x1, float y1, float x2, float y2, float x3,
                              float y3, float x4, float y4, float * xres, float * yres)
{

    float up = ((x4 - x3)*(y1 - y3) - (y4 - y3) * (x1 - x3) );
    float div = ((y4 - y3)*(x2 - x1) - (x4 - x3) * (y2 - y1) );
    if (div == 0) std::cout << div << std::endl;

    float ua = ((x4 - x3)*(y1 - y3) - (y4 - y3) * (x1 - x3) )/
               ((y4 - y3)*(x2 - x1) - (x4 - x3) * (y2 - y1) );

    (*xres) = x1 + ua * (x2 - x1);
    (*yres) = y1 + ua * (y2 - y1);

}
/*
void point_XY_rotation(float * x, float * y, float angle)
{

    float x0 = *x;
    float y0 = *y;


    (*x) = x0 * (float)cos(angle) - y0 * (float)sin(angle);
    (*y) = x0 * (float)sin(angle) + y0 * (float)cos(angle);

}
*/


C3DPoint image2space_conv(float d, // camera-laser distance
                          float u, float v, float betta,
                          float laser_depth_off,
                          int Nx, int Ny,
                          float fov_x, float fov_z, float h)
{

    float Tx = 0;
    float Ty = 0;
    float Sparam = 400; // mm

    image_to_space_xy(u, Nx, fov_x, Sparam, Tx, Ty);

    float Rx = d - Sparam * cos(betta);
    float Ry = laser_depth_off + Sparam * sin(betta);

    float xp =0.0, yp =0.0, zp = 0.0;
    segment_intersection_pnt( 0,  0, // camera point
                             Tx, Ty, //physical flik point
                              d, laser_depth_off,  // laser origin
                             Rx, Ry, // laser projection
                             &xp, &yp);


     image_to_space_z( v,  Ny,  fov_z, yp,  h, zp);

     C3DPoint pnt_res;
     pnt_res._x = xp;
     pnt_res._y = yp;
     pnt_res._z = zp;

     return pnt_res;
}

C3DPoint image2space_conv_ext(float d, // camera-laser distance
                          float u, float v, float betta,
                          float laser_depth_off,
                          float cx, float cy,
                          float fx, float fy)
{

    float Sparam = 200; // mm
    float Mparam = 200; // mm

    float Rx = d - Sparam * cos(betta);
    float Ry = laser_depth_off + Sparam * sin(betta);


    float Tx = (u - cx) * Mparam / fx;
    float Ty = Mparam;

    float xp =0.0, yp =0.0, zp = 0.0;
    segment_intersection_pnt( 0,  0, // camera point
                             Tx, Ty, //physical flik point
                             d, laser_depth_off,  // laser origin
                             Rx, Ry, // laser projection
                             &xp, &yp);

      zp =  (cy - v) * yp   / fy ;


     C3DPoint pnt_res;
     pnt_res._x = xp;
     pnt_res._y = yp;
     pnt_res._z = zp;

     return pnt_res;
}



std::map<std::string, float> parse_pairs(std::string in)
{
    std::map<std::string, float> res;

    /// std::cout << in << std::endl;
    std::string::size_type pos = 0, prev_pos, pos_beg =0, pos_end = 0;
    prev_pos = pos;
    bool bin = false;
    int cnt = 0;
    bool key = true;
    std::string last_key;
    float last_val;
    while((pos = in.find_first_of("'", pos)) != std::string::npos)
    {
        if(cnt%2)
        {
            std::string parse_str = in.substr(prev_pos, pos-prev_pos);

            if(key)
               last_key = parse_str;
            else
            {
                last_val = atof(parse_str.c_str());
                res[last_key] = last_val;
                /// std::cout << "adding " << last_key <<" = " << last_val << std::endl;
            }
            key = !key;
        }
        cnt ++;
        prev_pos = ++pos;
    }
    return res;
}

/*
std::map<std::string, float> parse_pairs_simple(std::string in)
{

    std::vector<std::string> substr;
    {
        std::string s = in;
        std::string delimiter = ";";

        size_t pos = 0;
        std::string token;
        while ((pos = s.find(delimiter)) != std::string::npos) {
            token = s.substr(0, pos);
            // std::cout << token << std::endl;
            s.erase(0, pos + delimiter.length());

            substr.push_back(token);
        }
    }
    std::map<std::string, float> res;
    {
        std::string s = substr[1];
        std::string delimiter = " ";

        size_t pos = 0;
        std::string token;
        std::string key;
        float value = 0;
        int r = 0;
        while ((pos = s.find(delimiter)) != std::string::npos) {
            token = s.substr(0, pos);
            // std::cout << token << std::endl;
            s.erase(0, pos + delimiter.length());

            if(r == 0) key = token;
            else value = atof(token.c_str());
        }

        res[key] =value;

        std::cout << key << " " << value << std::endl;
    }
    return res;

}*/

float read_laserangle_metadata(QString & fname, QString  patt)
{

    QMetaData  exifObject(fname);
    QList<QExifImageHeader *> *exifImageHeaders = exifObject.exifImageHeaderList();
    if(exifImageHeaders == NULL) return 0;
    if(exifImageHeaders->count() == 0) return 0;

    QExifImageHeader * e = ((*exifImageHeaders)[0]);
    if(e == NULL) return 0;

    QExifValue ev =  e->value( QExifImageHeader::UserComment);
    QString str(ev.toByteArray());

    ////std::cout << str.toStdString()  << std::endl;

    /// JSON FORMAT
    std::string in = str.toStdString().substr(1, str.length()-2);
    std::map<std::string, float> mp = parse_pairs(in);

    std::map<std::string, float>::iterator it = mp.find(patt.toStdString());
    if(it != mp.end());
        return it->second;

    return 0;
}



/*
std::vector<C3DPoint> image2space_conv
(std::vector<CIPixel> pxls,
 float d, float h,
 float alpha_x, float alpha_z,
 float delta_x, float delta_y,
 float betta,   float scan_step_angle,
 int Nx, int Ny, int angle_step, bool right_laser,
 float max_radii, QImage * src, bool use_col,
 float laser_off_center, float laser_off_mm)
{

    std::vector<C3DPoint> res_3d_points;

    //std::vector<CIPixel>::iterator it = pxls.begin();
    //for ( ; it != pxls.end(); it ++)


    int psz = pxls.size();
    QRgb * iBits   = (QRgb*)src->bits();
    int imwidth = src->width();

    //#pragma omp parallel for
    for(int pi = 0; pi < psz ; pi++)
    {
        float pi_u = pxls[pi]._u;
        float pi_v = pxls[pi]._v;

        float Delta_x = 0.0;
        float laser_proj_mm = 0;
        float ix = image_to_space_u(pi_u, Nx, alpha_x, delta_x, d, &Delta_x,
                                    laser_off_center, &laser_proj_mm);

        float xp =0.0, yp =0.0, zp = 0.0;
        segment_intersection_pnt(Delta_x, d, // camera point
                                ix, 0.0f,   // image pixel
                                laser_off_mm , d, // laser origin
                                laser_proj_mm , 0.0, // laser projection
                                &xp, &yp);


        float iz = image_to_space_v(pi_v, Ny, alpha_z, delta_y, d);
        zp = get_point_height( iz, yp, d, h);


        float rotation_angle = float(angle_step) * scan_step_angle;
        if(right_laser)
            rotation_angle -= 4 * betta; // magic constant 4
        point_XY_rotation(&xp, &yp, rotation_angle);


        C3DPoint pnt_res;
        pnt_res._x = xp;
        pnt_res._y = zp;
        pnt_res._z = yp	;
        pnt_res._angle  = rotation_angle;

        QRgb pix = iBits[int(pi_v) * imwidth + int(pi_u)];

        int pxr = qRed  (pix);
        int pxg = qGreen(pix);
        int pxb = qBlue (pix);

        if(use_col)
        {
            pnt_res._r = float(pxr)/255.f;
            pnt_res._g = float(pxg)/255.f;
            pnt_res._b = float(pxb)/255.f;
        }
        else
        {
            pnt_res._r = 1.0;
            pnt_res._g = 1.0;
            pnt_res._b = 1.0;
        }


        // check this
        // #pragma omp critical
        if( (pnt_res._z*pnt_res._z + pnt_res._x*pnt_res._x) < max_radii*max_radii)
            res_3d_points.push_back(pnt_res);
    }

    return res_3d_points;
}
*/
