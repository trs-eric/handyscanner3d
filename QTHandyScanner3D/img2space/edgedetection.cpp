#include "edgedetection.h"



#define Eu_Number 2.71828182845904523536


std::vector<CIPixel> get_edge_points_lines_1D(QImage * img)
{

    std::vector<CIPixel> res;

    int xs = img->width();
    int ys = img->height();

    CArray2D<double> nimg = convert_RGB_to_gray(img);

    double sum_weights = 0;
    double sum_pos = 0;
    for(int y = 0 ; y < ys; y++)
    {
        bool in_line = false;
        for(int x = 0; x < xs; x++)
        {

            if (nimg._data[y][x] < CGlobalSettings::_lcthresh && in_line)
            {
                in_line = false;
                res.push_back(CIPixel(sum_pos/sum_weights, float(y)));
                continue;
            }

            if (nimg._data[y][x] < CGlobalSettings::_lcthresh)
                continue;

            if ( !in_line)
            {
                // found new line
                in_line = true;

                sum_weights = 0;
                sum_pos = 0;
            }

            //if (in_line)
            {
                sum_weights += float(nimg._data[y][x]);
                sum_pos   += float(x) * float(nimg._data[y][x]);
            }

        }
    }
    return res;
}

float * gauss_kernel(int & sz, float sigma)
{
    sz =int( 1 + 2 * ceil(5 * sigma));
    if (sz < 5) sz = 5;
    float * kernel = new float[sz];
    int center = sz / 2;

    float sum = 0;
    for( int i=0; i <sz; i++ )
    {
        float x  = float(i - center);
        float fx = pow(Eu_Number, -0.5*x*x/(sigma*sigma)) / (sigma * sqrt(2*M_PI));
        kernel[i] = fx;
        sum += fx;
    }

    //std::cout << "gauss sz="<< sz << " center=" << center << std::endl;

    for( int i=0; i < sz; i++)
    {
        kernel[i] /= sum;
        //std::cout << i-center << " " <<  kernel[i] << std::endl;
    }
    return kernel;
}

void get_edge_points_smoothed_1D(std::vector<CIPixel> & inpnts)
{

    int ksz;
    float * kernel = gauss_kernel(ksz, CGlobalSettings::_gauss_sigma);
    int center = ksz / 2;
    std::vector<CIPixel> outpnts;


    //std::cout << "start smooth " << std::endl;
    //std::cout << kernel[0] << " " << kernel[1] << std::endl;

    std::vector<CIPixel>::iterator it = inpnts.begin();
    int pnts_count = inpnts.size();
    for(int i = 0 ; it !=  inpnts.end(); it++, i++)
    {

        CIPixel cpix = *it;

        if( (i >= center) && (i < (pnts_count - center)) )
        {
            float dot = 0.0;
            float sum = 0.0;
            for(int cc=(-center); cc<=center; cc++)
            {
                if(((i+cc) >= 0) && ((i+cc) < pnts_count))
                {
                    dot += kernel[center+cc] * inpnts[i+cc]._u ;
                    sum += kernel[center+cc];
                }
            }
            cpix._u = dot/sum;
        }

        outpnts.push_back(cpix);
    }

    inpnts = outpnts;
    delete kernel;

}

bool inter_laser_pnts_sort_x(const CIPixel & a, const CIPixel & b)
{
    return (a._u < b._u);
}

std::vector<CIPixel> do_laser_points_correction(std::vector<CIPixel> & pix)
{
    if(pix.size() < 5) return std::vector<CIPixel>() ;
    int corrwndsz = CGlobalSettings::_laser_corrwnd_sz;
    if(corrwndsz < 2) return pix;
    std::vector<CIPixel> res_pixs;


    ///////////// fill single outlier gaps
    double laser_ray_width = 5;
    double max_vert_dist = 1.3;
    int pxsz  = pix.size();
    for(int p = 1; p < (pxsz - 1) ; p++ )
    {
        CIPixel pix_prev = pix[p-1];
        CIPixel pix_cur  = pix[p];
        CIPixel pix_next = pix[p+1];

        /// found outlier
        if( (std::abs(pix_cur._v - pix_prev._v)<= max_vert_dist ) &&
            (std::abs(pix_next._v - pix_cur._v )<= max_vert_dist ) &&
            (std::abs(pix_next._u - pix_prev._u )<= laser_ray_width ) &&
            (std::abs(pix_cur._u - pix_prev._u ) > laser_ray_width )
           )
        {
           pix_cur._u = (pix_next._u + pix_prev._u)/2;
           pix[p] = pix_cur;
            p++;
        }

    }

    //// remove stand alone outliers
    for(int p = 1; p < (pxsz - 1) ; p++ )
    {
        CIPixel pix_prev = pix[p-1];
        CIPixel pix_cur  = pix[p];
        CIPixel pix_next = pix[p+1];

        if( (std::abs(pix_cur._u  - pix_next._u ) > laser_ray_width ) &&
            (std::abs(pix_cur._u - pix_prev._u ) > laser_ray_width )
           )
        {

        }
        else
        {
            res_pixs.push_back(pix_cur);
        }



    }


    pix.clear();
    pix.insert(pix.begin(), res_pixs.begin(), res_pixs.end());
    res_pixs.clear();

    ////res_pixs.insert(res_pixs.begin(), pix.begin(), pix.end());

     pxsz  = pix.size();
    int window_hwidth = corrwndsz;//6;
    for(int p = window_hwidth  ; p < (pxsz - window_hwidth ); p++ )
    {

        std::vector<CIPixel> pix_wnd;
        for(int pp = -window_hwidth ; pp <  window_hwidth; pp++)
            pix_wnd.push_back(pix[p+pp]);


      ////  std::cout << p << " " << pxsz  << " " << pix_wnd.size()<< std::endl;

        std::sort(pix_wnd.begin(), pix_wnd.end(), inter_laser_pnts_sort_x);
      double cutoff_min =  double(window_hwidth*2 + 1) * 0.3;
        double cutoff_max =   double(window_hwidth*2 + 1) * 0.7;
        std::vector<CIPixel> good_pix;

        float avu   = 0;
        int cnt     = 0;
        for(int i = cutoff_min; i <=  cutoff_max; i++, cnt++)
           avu += pix_wnd[i]._u;
        avu /= cnt;

           CIPixel res;
           res._u = avu;
           res._v = pix[p]._v;

           if(std::sqrt( (res._u - pix[p]._u)*(res._u - pix[p]._u) +
                         (res._v - pix[p]._v)*(res._v - pix[p]._v)) < laser_ray_width )
                res_pixs.push_back(res);
    }

    /////////////////////////////////////////////////////
    std::vector<CIPixel> very_good_pix;

    int short_length = 5;\
    float connected_length = 1.7;
    int psz = res_pixs.size();


    std::vector<CIPixel> segment;
    CIPixel curpix = res_pixs[0];
    segment.push_back(curpix);
    for( int p = 1 ; p < psz ; p++)
    {
        CIPixel nexpix = res_pixs[p];

        if(std::sqrt((nexpix._u - curpix._u)*(nexpix._u - curpix._u) +
                     (nexpix._v - curpix._v)*(nexpix._v - curpix._v)) < connected_length)
        {
            segment.push_back(nexpix);

        }
        else
        {
            if(segment.size() > short_length)
                very_good_pix.insert(very_good_pix.end(), segment.begin(), segment.end());

            segment.clear();
            segment.push_back(nexpix);
        }
        curpix = nexpix;


    }
    if(segment.size() > short_length)
        very_good_pix.insert(very_good_pix.end(), segment.begin(), segment.end());



    return very_good_pix;
}

std::vector<CIPixel> get_edge_points_lines_hills_1D(QImage * img)
{

    std::vector<CIPixel> res;

    int xs = img->width();
    int ys = img->height();

    CArray2D<double> nimg = convert_RGB_to_gray(img);
    ///CArray2D<double> nimg = convert_RGB_to_gray_suppress_non_red(img,  CGlobalSettings::_lcthresh);



    double sum_weights = 0;
    double sum_pos = 0;


    //#pragma omp parallel for
    for(int y = 0 ; y < ys; y++)
    {

        bool emax_found = false;
        float emax_x = 0;
        float emax_y = y;
        float emax_c = 0;

        for(int x = 0; x < xs; x++)
        {

            float curr_col = nimg._data[y][x];
            if ( curr_col > CGlobalSettings::_lcthresh && curr_col > emax_c)
            {
                emax_x = x;
                emax_c = curr_col;
                emax_found = true;
            }
        }
        if(emax_found)
        {
            float sum_weights = 0.0;
            float sum_pos = 0.0;
            int i = emax_x;

            float mid_col = CGlobalSettings::_lcthresh + 0.7 * float(emax_c  - CGlobalSettings::_lcthresh);
            while(nimg._data[y][i]>= CGlobalSettings::_lcthresh) // go left
            {
                float clr = float(nimg._data[y][i]);

                float weight =  (clr  - CGlobalSettings::_lcthresh) / (emax_c  - CGlobalSettings::_lcthresh);
                if(clr > mid_col ) weight *=4;
                else weight /=4;

                sum_weights += weight ;
                sum_pos   += float(i) * weight;//float(nimg._data[y][i]);
                i--;
                if(i < 0 ) break;
            }

            i = emax_x+1;
            while(nimg._data[y][i]>= CGlobalSettings::_lcthresh) // go right
            {
                float clr = float(nimg._data[y][i]);

                float weight =  (clr  - CGlobalSettings::_lcthresh) / (emax_c  - CGlobalSettings::_lcthresh);
                if(clr > mid_col ) weight *=4;
                else weight /=4;

                sum_weights += weight;
                sum_pos   += float(i) * weight;// float(nimg._data[y][i]);
                i++;
                if(i >= xs) break;
            }

            // FIX: donot add points from right image side
            //if(sum_pos/sum_weights < xs/2)
            //#pragma omp critical
            res.push_back(CIPixel(sum_pos/sum_weights, float(y)));
        }
    }

    //if(CGlobalSettings::_gauss_sigma > 0)
//        get_edge_points_smoothed_1D(res);
    return res;
}




std::vector<CIPixel> get_edge_points_lines_hills_1D_undistorted(QImage * img,
                                                                cv::Mat & intrinsics,
                                                                cv::Mat & distortion)
{
    std::vector<cv::Point2f>  rpnts;

    int xs = img->width();
    int ys = img->height();

    CArray2D<double> nimg = convert_RGB_to_gray(img);

    double sum_weights = 0;
    double sum_pos = 0;

    for(int y = 0 ; y < ys; y++)
    {

        bool emax_found = false;
        float emax_x = 0;
        float emax_y = y;
        float emax_c = 0;

        for(int x = 0; x < xs; x++)
        {

            float curr_col = nimg._data[y][x];
            if ( curr_col > CGlobalSettings::_lcthresh && curr_col > emax_c)
            {
                emax_x = x;
                emax_c = curr_col;
                emax_found = true;
            }
        }
        if(emax_found)
        {
            float sum_weights = 0.0;
            float sum_pos = 0.0;
            int i = emax_x;

            float mid_col = CGlobalSettings::_lcthresh + 0.7 * float(emax_c  - CGlobalSettings::_lcthresh);
            while(nimg._data[y][i]>= CGlobalSettings::_lcthresh) // go left
            {
                float clr = float(nimg._data[y][i]);

                float weight =  (clr  - CGlobalSettings::_lcthresh) / (emax_c  - CGlobalSettings::_lcthresh);
                if(clr > mid_col ) weight *=4;
                else weight /=4;

                sum_weights += weight ;
                sum_pos   += float(i) * weight;//float(nimg._data[y][i]);
                i--;
                if(i < 0 ) break;
            }

            i = emax_x+1;
            while(nimg._data[y][i]>= CGlobalSettings::_lcthresh) // go right
            {
                float clr = float(nimg._data[y][i]);

                float weight =  (clr  - CGlobalSettings::_lcthresh) / (emax_c  - CGlobalSettings::_lcthresh);
                if(clr > mid_col ) weight *=4;
                else weight /=4;

                sum_weights += weight;
                sum_pos   += float(i) * weight;// float(nimg._data[y][i]);
                i++;
                if(i >= xs) break;
            }
           // res.push_back(CIPixel(sum_pos/sum_weights, float(y)));
            rpnts.push_back(cv::Point2f(sum_pos/sum_weights, float(y)));
        }
    }
    std::vector<CIPixel> res;

   if ( rpnts.size() > 0)
   {
        std::vector<cv::Point2f> undist_pnts(rpnts.size());
        cv::Mat R = (cv::Mat_<double>(3,3) << 1, 0., 0,
                               0.,  1, 0,
                               0.,  0., 1. );

      cv::undistortPoints(rpnts, undist_pnts, intrinsics, distortion, R, intrinsics);

       for(  std::vector<cv::Point2f>::iterator it = undist_pnts.begin();   it != undist_pnts.end(); it++)
            res.push_back(CIPixel ( it->x, it->y )  );

     ///   for(  std::vector<cv::Point2f>::iterator it = rpnts.begin();   it != rpnts.end(); it++)
     ///       res.push_back(CIPixel ( it->x, it->y )  );
    }

    return res;
}




