#ifndef IMAGE2SPACEMODEL_H
#define IMAGE2SPACEMODEL_H

#include <vector>
#include "graphicutils.h"


void image_to_space_xy(float u,// pixel horizontal index
                       int Nx, // image pixel width
                       float fov_x, // horizontal camera FOV
                        float Sparam, // ray len arbitrary param
                       float & Tx, // real coordinates (mm)
                       float & Ty);

void image_to_space_z(float v, // pixel vertical index
                      int Ny, // image pixel height
                      float fov_z, // vertical camera FOV
                       float yp,  // intersection coord mm
                      float h, // camera evaliation
                      float & zp); // resulting

C3DPoint image2space_conv(float d, // camera-laser distance
                          float u, // flick projection (u,v)
                          float v,
                          float betta, // laser angle
                          float laser_depth_off, // laser depth offset
                          int Nx, int Ny, // size of the image
                          float fov_x, float fov_z, // camera FOVs
                          float h); //camera elevaion

C3DPoint image2space_conv_ext(float d, // camera-laser distance
                          float u, // flick projection (u,v)
                          float v,
                          float betta, // laser angle
                          float laser_depth_off, // laser depth offset
                          float cx, // camera matrix
                          float cy,
                          float fx,
                          float fy ); //camera elevaion


std::map<std::string, float> parse_pairs(std::string in);
float read_laserangle_metadata(QString & fname, QString patt=QString("angle_"));

/*
float image_to_space_v(float v,
                       int Ny,
                       float alpha_z,
                       float delta_y, // vertical camera-rot offset
                       float d); // camera-ceneter distance
*/
/*
float get_point_height(float iz, // world pix coorf
                       float py, // real point Y coord
                       float d, // camera-ceneter distance
                       float h); //

void segment_intersection_pnt(float x1, // first segment endpoints
                              float y1,
                              float x2,
                              float y2,
                              float x3, // scond segment endpoints
                              float y3,
                              float x4,
                              float y4,
                              float * xres, // resulting point
                              float * yres
                              );
void point_XY_rotation(float * x, float * y, float angle );//(rad)

std::vector<C3DPoint> image2space_conv (std::vector<CIPixel> pxls,
 float d, float h, float alpha_x, float alpha_z,
 float delta_x, float delta_y,  float betta,   float scan_step_angle,
 int Nx, int Ny, int angle_step, bool right_laser,
                                        float max_radii, QImage *, bool,
                                        float laser_off_center, float laser_off_mm);
*/
#endif // IMAGE2SPACEMODEL_H
