#ifndef EDGEDETECTION_H
#define EDGEDETECTION_H

#include <vector>
#include <QImage>
#include "graphicutils.h"
#include "../settings/psettings.h"


std::vector<CIPixel> get_edge_points_lines_1D(QImage * img);
std::vector<CIPixel> get_edge_points_lines_hills_1D(QImage * img);
std::vector<CIPixel> get_edge_points_lines_hills_1D_undistorted(QImage * img, cv::Mat & intrinsics,
                                                                cv::Mat & distortion);

float * gauss_kernel(int & sz, float sigma);
std::vector<CIPixel> do_laser_points_correction(std::vector<CIPixel> & pix);

#endif // EDGEDETECTION_H
