#ifndef CFREEGLCAMERA_H
#define CFREEGLCAMERA_H

#include "cglcamera.h"

class CFreeGLCamera : public CGLCamera
{
public:
    CFreeGLCamera();
    virtual ~CFreeGLCamera();


    void move_forward   (float v = 0);
    void move_backward  (float v = 0);
    void move_left		(float v = 0);
    void move_right		(float v = 0);
    void move_up		(float v = 0);
    void move_down		(float v = 0);


    void turn_left		(float v = 0);
    void turn_right		(float v = 0);
    void turn_up		(float v = 0);
    void turn_down		(float v = 0);

    virtual void set_view();

    void init();
    void recompute_tpos();
protected:
    virtual void update_pos();
    void update_target();

    float _focal_len;
};

#endif // CFREEGLCAMERA_H
