#ifndef CTEXPREVIEW_H
#define CTEXPREVIEW_H

#include <QDialog>

class QLabel;
class QScrollBar;
class CGLCompoundObject;
namespace Ui {
class CTexPreview;
}

class CTexPreview : public QDialog
{
    Q_OBJECT

public:
    explicit CTexPreview(CGLCompoundObject *,
                         QImage & tex, QString name, QWidget *parent = 0);
    ~CTexPreview();

private slots:
    void image_save();

    void zoom_in();
    void zoom_out();
    void zoom_norm();

    void apply_img();
    void reset_img();

    void brightness_changed(int );
    void contrast_changed(int );
    void vibrance_changed(int );
    void deblur_changed(int );
    void huesat_changed(int );


private:
    void adjust_scroll_bar(QScrollBar *scrollBar, float factor);
    void scale_image();
    void modify_image();
    void unsharp_mask();

    void  set_sliders_default();

    float _scale_factor;
    QImage _texture;
    QImage _texture_pre;


    Ui::CTexPreview *ui;

    QLabel * _tex_label;
    QString _tex_ttl;

    CGLCompoundObject * _parent;


};

#endif // CTEXPREVIEW_H
