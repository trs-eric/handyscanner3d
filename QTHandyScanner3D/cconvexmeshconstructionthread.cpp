#include "cconvexmeshconstructionthread.h"
#include "ctriangularmesh.h"
#include "cpointcloud.h"
#include "cnormalcloud.h"
#include "graphicutils.h"

CConvexMeshConstructionThread::CConvexMeshConstructionThread(QObject *parent) :
QThread(parent), _pcloud(NULL), _vert_dist_threas(0), _horiz_dist_threas(0),
  _closed_shape(true), _gen_texture(false)
{
    _abort = false;
}


CConvexMeshConstructionThread::~CConvexMeshConstructionThread()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
    wait();
}

void CConvexMeshConstructionThread::start_process(CPointCloud * pcld,
     float vert_dist_threas, float horiz_dist_threas, bool closed_shape, bool gentex)
{
    _abort = false;

    _pcloud    = pcld;
    _vert_dist_threas = vert_dist_threas;
    _horiz_dist_threas = horiz_dist_threas;
    _closed_shape = closed_shape;
    _gen_texture = gentex;

    start();
}

void CConvexMeshConstructionThread::stop_process()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
}

void CConvexMeshConstructionThread::emit_send_back(int i)
{
     emit send_back(i);
}


void add_triangles(int L0, int L1, int R0, int R1,
                   std::vector<C3DPoint> & pnts,
                   std::vector<int> & pnts_neighbours,
                   std::vector<int> & triangles)
{
    int Lm = L0;
    int Rm = R0;

    int Steps = ((L1 - L0) < (R1 - R0))?(L1 - L0):(R1 - R0);
    for(int s = 0; s < Steps ; s++, Lm++, Rm++)
    {
        float ldist = compute_sq_dist(pnts[Lm + 1], pnts[Rm    ]);
        float rdist = compute_sq_dist(pnts[Lm    ], pnts[Rm + 1]);

        if(ldist < rdist)
        {
            triangles.push_back(Lm+1);
            triangles.push_back(Lm);
            triangles.push_back(Rm);
            //std::cout << "add:" << (Lm+1) << " "<< (Lm) << " "<< (Rm) << std::endl;

            triangles.push_back(Rm);
            triangles.push_back(Rm+1);
            triangles.push_back(Lm+1);
            //std::cout << "add:" << (Rm) << " "<< (Rm+1) << " "<< (Lm+1) << std::endl;
        }
        else
        {
            triangles.push_back(Lm+1);
            triangles.push_back(Lm);
            triangles.push_back(Rm+1);
            //std::cout << "add:" << (Lm+1) << " "<< (Lm) << " "<< (Rm+1) << std::endl;

            triangles.push_back(Rm);
            triangles.push_back(Rm+1);
            triangles.push_back(Lm);
            //std::cout << "add:" << (Rm) << " "<< (Rm+1) << " "<< (Lm) << std::endl;
        }
    }
    Lm++; Rm++;

    if((L1 - L0) == (R1 - R0))
        return;

    if((L1 - L0) < (R1 - R0))
    {

        //std::cout << "case r" << std::endl;
        for( ; Rm <= (R1); Rm++)
        {
            triangles.push_back(Rm-1);
            triangles.push_back(Rm);
            triangles.push_back(Lm-1);

            //std::cout << "add:" << (Lm) << " "<< (Rm) << " "<< (Rm+1) << std::endl;
        }
    }
    else
    {
        //std::cout << "case l" << Lm << " "<< (L1) << " " << Rm << std::endl;
        for( ; Lm <= (L1); Lm++)
        {
            triangles.push_back(Lm);
            triangles.push_back(Lm-1);
            triangles.push_back(Rm-1);
            //std::cout << "add:" << (Rm) << " "<< (Lm+1) << " "<< (Lm) << std::endl;
        }
    }
}


// TODO solve seg fault with triangle deletion
bool pnts_cmp(const C3DPoint &f, const C3DPoint &s)
{
    if(f._angle < s._angle)
        return true;
    else
        return false;
}

void CConvexMeshConstructionThread::run()
{
     if(_pcloud == NULL) return;

     std::vector<C3DPoint> pnts = _pcloud->get_points_cpy();
     int num_points = pnts.size();

     ////// filing scanline param
     int k = 0;
     int pnts_num = pnts.size();
     int line = 0;
     float cur_angle = pnts[0]._angle;
     float init_scan_angle = atan2(pnts[0]._x, pnts[0]._z)- cPI ;
     while(k < pnts_num)
     {
         if( pnts[k]._angle != cur_angle)
         {
             cur_angle = pnts[k]._angle;
             line ++;
         }
         pnts[k]._scanline = line;
         /// std::cout << pnts[k]._scanline << " " ;
         k++;

     }
     //////////////////////////

     std::vector<int> pnts_neighbours(pnts.size());

     std::vector<int> triangles;

     bool first_scan     = (_closed_shape)? true: false;
     bool scanlines_scan = true;

     int scanline_beg = 0;
     int scanline_mid = 0;
     int scanline_end = 0;

     int total_scanlines = 0;
     int max_pnts_in_one_scanline = 0;
     while(scanlines_scan)
     {
         total_scanlines ++;
         float cur_angle = pnts[scanline_beg]._angle;

         int k;
         if(!first_scan)
         {
             k = scanline_beg;
             while(k < num_points )
             {
                if( pnts[k]._angle != cur_angle)
                {
                     scanline_mid = k;
                     break;
                }
                k++;
             }
         }
         else
         {
             k =  num_points - 1;
             cur_angle = pnts[k]._angle;
             while(k >= 0)
             {
                 if( pnts[k]._angle != cur_angle)
                 {
                      scanline_beg = k+1;
                      break;
                 }
                 k--;
             }
         }

         if(k == num_points )
                 break; // last scanline

         k = scanline_mid;
         cur_angle = pnts[k]._angle;
         while(k < num_points )
         {
            if( pnts[k]._angle != cur_angle)
            {
                scanline_end = k-1;
                break;
            }
            k++;
        }

        if(k == num_points )
                 scanline_end = num_points-1;


        if(max_pnts_in_one_scanline < (scanline_mid - scanline_beg + 1) )
            max_pnts_in_one_scanline = (scanline_mid - scanline_beg + 1) ;

        if(max_pnts_in_one_scanline < (scanline_end - scanline_mid + 1) )
            max_pnts_in_one_scanline = (scanline_end - scanline_mid + 1) ;



        ///std::cout << scanline_beg << " " << scanline_mid << " " << scanline_end << " " << std::endl;
        ///std::cout << num_points << " " <<  pnts[scanline_beg]._angle << " " << pnts[scanline_mid]._angle
        ///          << " " << pnts[scanline_end]._angle  << std::endl;

        ///////////// compute neighbour pairs /// (3)
        int scanline_mid_prev = (first_scan)? num_points-1 : (scanline_mid-1);
        for( int i = scanline_beg; i <= scanline_mid_prev; i++ )
        {
            C3DPoint lpnt = pnts[i];

            int ridx  = scanline_mid;
            C3DPoint rpnt = pnts[ridx];
            float pdist = compute_sq_dist(lpnt, rpnt);

            for(int n = scanline_mid; n <= scanline_end; n++)
            {
                C3DPoint rpnt = pnts[n];
                float cdist = compute_sq_dist(lpnt, rpnt);

                if(cdist < pdist)
                {
                    ridx = n;
                    pdist = cdist;
                }
            }
            pnts_neighbours[i] = ridx;
        }

        for( int j = scanline_mid; j <= scanline_end; j++ ) // (4)
        {
            C3DPoint rpnt = pnts[j];
            int lidx  = scanline_beg;
            C3DPoint lpnt = pnts[lidx];
            float pdist = compute_sq_dist(lpnt, rpnt);
            for(int n = scanline_beg; n <= scanline_mid_prev; n++)
            {
                C3DPoint lpnt = pnts[n];
                float cdist = compute_sq_dist(lpnt, rpnt);

                if(cdist < pdist)
                {
                    lidx = n;
                    pdist = cdist;
                 }
             }
             pnts_neighbours[j] = lidx;
        }
        ////////////////////////////////////////////////


        bool cont = true;
        int left_marker       = scanline_beg;
        int last_left_marker  = scanline_mid_prev;
        int right_marker      = scanline_mid;
        int last_right_marker = scanline_end;
        while(cont)
        {
            if( (left_marker  == last_left_marker) ||
                (right_marker == last_right_marker))
                            break;

            int next_left_marker  = (left_marker + 1 <= last_left_marker)?
                                    (left_marker + 1) : last_left_marker;
            int next_right_marker = (right_marker + 1 <= last_right_marker)?
                                    (right_marker + 1) : last_right_marker;

            int new_left_marker = next_left_marker;
            bool lfound = false;
            while(new_left_marker < last_left_marker)
            {
                if(pnts_neighbours[new_left_marker] >=next_right_marker)
                {
                    lfound = true;
                    break;
                }
                new_left_marker++;
            }

            int new_right_marker = next_right_marker;
            bool rfound = false;
            while(new_right_marker < last_right_marker)
            {
                if(pnts_neighbours[new_right_marker] >=next_left_marker)
                {
                    rfound = true;
                    break;
                }
                new_right_marker++;
            }

            if(rfound && lfound)
            {

                if( ( abs(pnts_neighbours[new_left_marker] - next_right_marker)<
                      abs(pnts_neighbours[new_right_marker]- next_left_marker))
                  )
                    new_right_marker = pnts_neighbours[new_left_marker];
                else
                     new_left_marker  = pnts_neighbours[new_right_marker];
           }
            else
            {
                new_left_marker  = last_left_marker;
                new_right_marker = last_right_marker;
            }

            add_triangles(left_marker, new_left_marker,
                               right_marker,  new_right_marker,
                               pnts, pnts_neighbours, triangles);

            left_marker  = new_left_marker;
            right_marker = new_right_marker;
        }
        ///std::cout << "done current 2 scans"<< std::endl;

        ///setup progress value
        emit send_back(scanline_mid * 50 / num_points);

        if (_abort)
        {
            emit send_back(0);
            return;
        }

        scanline_beg = scanline_mid;
        first_scan = false;

     }
     ///std::cout << "done all scans"<< std::endl;



     const int non_valid_index = -1;
     std::vector<int> new_pnts_indices(num_points, non_valid_index);
     std::vector<C3DPoint> new_pnts;
     float vertT2 = _vert_dist_threas  * _vert_dist_threas;
     float horiT2 = _horiz_dist_threas * _horiz_dist_threas;

     CTriMesh * rmesh = new CTriMesh ;

     int trian_size = triangles.size()/3;
     std::vector<int>::iterator ittr = triangles.begin();
     int point_idx = 0;
     for (int face_idx = 0 ; ittr != triangles.end(); ittr++, face_idx++)
     {

         //// stage 1. check triangle size
         C3DPoint pnt0 = pnts[ *ittr    ];
         C3DPoint pnt1 = pnts[ *(ittr + 1)];
         C3DPoint pnt2 = pnts[ *(ittr + 2)];

         if( compute_sq_dist(pnt0, pnt1) > vertT2 )
         {
            ittr++;
            ittr++;
            face_idx --;
            continue;
         }
         if( compute_sq_dist(pnt0, pnt2) > horiT2 )
         {
             ittr++;
             ittr++;
             face_idx--;
             continue;
         }
         if( compute_sq_dist(pnt1, pnt2) > horiT2 )
         {
             ittr++;
             ittr++;
             face_idx--;
             continue;
         }


         // std::cout << "add point 0" << std::endl;
         // add point 0
         if(new_pnts_indices[*ittr] == non_valid_index)
         {
             pnt0._faceidx.push_back(face_idx);
             new_pnts.push_back(pnt0);
             new_pnts_indices[*ittr] = point_idx;
             point_idx++;
         }
         else
             new_pnts[new_pnts_indices[*ittr]]._faceidx.push_back(face_idx);

         // std::cout << "add point 1" << std::endl;
         // add point 1
         if(new_pnts_indices[*(ittr + 1)] == non_valid_index)
         {
             pnt1._faceidx.push_back(face_idx);
             new_pnts.push_back(pnt1);
             new_pnts_indices[*(ittr + 1)] = point_idx;
             point_idx++;
         }
         else
             new_pnts[new_pnts_indices[*(ittr + 1)]]._faceidx.push_back(face_idx);

         // std::cout << "add point 2" << std::endl;
         // add point 2
         if(new_pnts_indices[*(ittr + 2)] == non_valid_index)
         {
             pnt2._faceidx.push_back(face_idx);
             new_pnts.push_back(pnt2);
             new_pnts_indices[*(ittr + 2)] = point_idx;
             point_idx++;
         }
         else
             new_pnts[new_pnts_indices[*(ittr + 2)]]._faceidx.push_back(face_idx);

         /// std::cout << "add triangle" << std::endl;
         //// stage 2. add point and triangle
         C3DPointIdx tri;

         tri.pnt_index[0] = new_pnts_indices[*ittr]; ittr++;
         tri.pnt_index[1] = new_pnts_indices[*ittr]; ittr++;
         tri.pnt_index[2] = new_pnts_indices[*ittr];
         tri.tri_normal = compute_normal( pnt2, pnt1, pnt0);

         rmesh->_triangles.push_back(tri);

         emit send_back(face_idx * 25 / trian_size + 50 );
     }
     rmesh->_points = new_pnts;

     //////// update mesh normals per vertec
     int pnts_count  = rmesh->_points.size();
     std::vector<C3DPoint>::iterator itv = rmesh->_points.begin();
     for(int p = 0; itv != rmesh->_points.end(); itv++, p++)
     {
         itv->_nx =0;
         itv->_ny =0;
         itv->_nz =0;

         std::vector<int>::iterator fit = itv->_faceidx.begin();
         for( ; fit != itv->_faceidx.end(); fit++)
         {

             itv->_nx += rmesh->_triangles[(*fit)].tri_normal._x;
             itv->_ny += rmesh->_triangles[(*fit)].tri_normal._y;
             itv->_nz += rmesh->_triangles[(*fit)].tri_normal._z;
         }
         normalize3(itv->_nx, itv->_ny, itv->_nz);

         emit send_back(p * 25 / pnts_count + 75 );


     }

     //CNormalCloud * ncld = new CNormalCloud ;
     //ncld->set_normals(rmesh->_points);

     //std::cout << "mesh init was done" << std::endl;
     CTriangularMesh * mmesh = new CTriangularMesh;
     mmesh->set_mesh(rmesh);

    emit send_back(100);


     // black vertical stripes - done
     // add texture coords
     // fix smooth and tex meshes

     /////////////////////////////////////////////////////
     /// fix texture generation for rotated object !!!!!!
     ///////////////////////////////////////////////
     if(_gen_texture)
     {

         float bb_miny = pnts[0]._y;
         float bb_maxy = pnts[0]._y;

         std::vector<C3DPoint>::iterator itp =  pnts.begin();
         for( ; itp !=  pnts.end() ; itp++)
         {
            if( itp->_y < bb_miny) bb_miny = itp->_y;
            if( itp->_y > bb_maxy) bb_maxy = itp->_y;
         }

         if(_closed_shape)
             total_scanlines--;
        //std::cout << "scan lines " << total_scanlines << std::endl;
        //std::cout << "max pnts in line " << max_pnts_in_one_scanline << std::endl;


        float factor = 2;
        int xs = total_scanlines;
        int ys = max_pnts_in_one_scanline * factor;


        QImage * exist_map = new QImage(xs, ys, QImage::Format_RGB32);
        QImage * color_map = new QImage(xs, ys, QImage::Format_RGB32);


        int pnts_num = pnts.size();

        int line = 0;
        int k    = 0;
        float cur_angle = pnts[0]._angle;

        while(k < pnts_num)
        {
            if( pnts[k]._angle != cur_angle)
            {
                cur_angle = pnts[k]._angle;
                line ++;
            }
             /// [0,1]
            float u = float(line) / float(xs);
            float v = (bb_maxy - pnts[k]._y) / (bb_maxy - bb_miny);

            /// [0,N)
            float ui = line;
            float vi = v * float(ys);
            if(ui>=xs) ui=xs-1;
            if(vi>=ys) vi=ys-1;
            if(ui<0) ui = 0;
            if(vi<0) vi = 0;

            ///std::cout << "."; ///// !!!!!!!!!!

            float red   = pnts[k]._r * 255;
            float green = pnts[k]._g * 255;
            float blue  = pnts[k]._b * 255;

            QRgb col = qRgb( red, green,  blue);
            color_map->setPixel( int(ui),int(vi), col);

            QRgb col2 = qRgb( 255, 255,  255);
            exist_map->setPixel( int(ui),int(vi), col2);
            k++;
        }

        fillin_texture(color_map, exist_map, xs, ys);

        rmesh->_texture = color_map;
        rmesh->_texture_name = "sample texture";
        rmesh->_tex_bb_maxy = bb_maxy;
        rmesh->_tex_bb_miny = bb_miny;
        rmesh->_tex_init_angle = init_scan_angle;
        rmesh->init_mass_center();

        ///std::cout << pnts.size() << " vs " << rmesh->_points.size() << std::endl;

        int tot_pnts = rmesh->_points.size();
        for( int k = 0; k < tot_pnts; k++)
        {
            /// std::cout << rmesh->_points[k]._scanline << " " ;

            Vector2f tex_coord;
            tex_coord.x = float(rmesh->_points[k]._scanline)/float(xs);
            tex_coord.y =   (rmesh->_points[k]._y - bb_miny)/( bb_maxy - bb_miny);

            rmesh->_tex_coords.push_back(tex_coord);
        }


        std::vector<C3DPointIdx>::iterator itt = rmesh->_triangles.begin();
        for( ; itt != rmesh->_triangles.end(); itt++)
        {
            itt->tex_index[0] = itt->pnt_index[0];
            itt->tex_index[1] = itt->pnt_index[1];
            itt->tex_index[2] = itt->pnt_index[2];
        }

     }
     ///std::cout << " done " << std::endl;

     emit send_result(mmesh);

}

void fillin_line(QImage * cmap, QImage * emap, int x,
                 bool valid_up, int up, bool valid_dawn, int down)
{

    int xs = cmap->width();
    int ys = cmap->height();

    QRgb marker = qRgb( 255, 255,  255);

    if( valid_up && valid_dawn)
    {
        //std::cout << "1-1" << std::endl;

        QRgb cu = cmap->pixel(x, up);
        QRgb cd = cmap->pixel(x, down);

        int cy = up+1;
        while(cy != down )
        {
            float red = float(qRed(cu))   + float(cy - up) * (float(qRed(cd))   - float(qRed(cu)))  / float(down - up);
            float gre = float(qGreen(cu)) + float(cy - up) * (float(qGreen(cd)) - float(qGreen(cu)))/ float(down - up);
            float blu = float(qBlue(cu))  + float(cy - up) * (float(qBlue(cd))  - float(qBlue(cu))) / float(down - up);

            if(red <   0) red = 0;
            if(red > 255) red = 255;
            if(gre <   0) gre = 0;
            if(gre > 255) gre = 255;
            if(blu <   0) blu = 0;
            if(blu > 255) blu = 255;

            //std::cout << "[" << red << "(" << qRed(cu) << ")" << "," << gre << "(" << qGreen(cu) << ")" << "," << blu << "(" << qBlue(cu) << ")" << "] ";
            QRgb rc = qRgba(int(red), int(gre), int(blu), 255);
            cmap->setPixel( x, cy, rc);
            emap->setPixel( x, cy, marker);

            cy++;
        }
    }
    else if(valid_up)
    {
        QRgb cu = cmap->pixel(x, up);
        int cy = up+1;
        while(cy != ys)
        {
            cmap->setPixel(x, cy, cu);
            emap->setPixel( x, cy, marker);
            cy++;
        }

    }
    else // valid down
    {
        QRgb cd = cmap->pixel(x, down);
        int cy = down-1;
        while(cy >= 0)
        {
            cmap->setPixel(x, cy, cd);
            emap->setPixel( x, cy, marker);
            cy--;
        }
    }
}

void CConvexMeshConstructionThread::fillin_texture(QImage * cimg, QImage * emap, int xs, int ys)
{
    int mcount = 0;
    QRgb marker = qRgb( 255, 255, 255);
    for(int i = 0; i < xs; i++)
    {
        for(int j = 0; j < ys; j++)
        {
            QRgb c = emap->pixel(i,j);
            if(c != marker)
            {
                mcount++;


                int up = j;
                bool valid_up = true;
                if (up == 0)
                    valid_up = false;
                else
                    up--;


                int down = j+1;
                bool valid_dawn = true;
                while (down < ys)
                {
                    if(emap->pixel(i,down) == marker)
                        break;
                    down++;
                }

                if(down == ys)
                {
                    down --;
                    valid_dawn = false;
                }
                fillin_line(cimg, emap, i, valid_up, up, valid_dawn, down);

                j = down; // jump

                //if(valid_up) std::cout << "[" << up;
                //if(valid_dawn) std::cout << " - " << down << "];";


            }

        }
    }

    std::cout << "::::" <<  mcount << std::endl;

}

