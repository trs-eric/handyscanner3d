#ifndef CTEXTURINGTHREAD_H
#define CTEXTURINGTHREAD_H

#include <QThread>
#include <QMutex>
#include "graphicutils.h"
#include "ctrianglesgroup.h"

class CTriangularMesh;

class CTexturingThread : public QThread
{
    Q_OBJECT
public:
    explicit CTexturingThread(QObject *parent = 0);
    ~CTexturingThread();

    void start_process(CTriangularMesh * mesh, int step, float mgt, float gits, QString inif);
    bool is_abort(){return _abort;}
    void emit_send_back(int i);

signals:
    void send_back(const int val);
    void send_result();
    /// void send_spcl(std::vector<C3DPoint> * cpoints);

protected:
    void run();
    void run_old();
public slots:
        void stop_process();

private:

    QString _inif;
    bool    _abort;
    QMutex  _mutex;

    void compose_global_frame_map();
    void construct_and_add_new_cframe(std::vector<CFrame *> & mp, float rxc, float ryc,
            QString & fname, int frame_idx,
            float sca_angle, float camera_x_incline);

    std::vector<int> assign_mesh_points_with_truth_color(std::vector<Vector3f> & );
    void global_color_adjustment(std::vector<Vector3f> &, std::vector<int> & );
    void assign_triangles_to_view(std::vector<int> &, std::vector<int> &);
    void contruct_individual_face_groups(std::vector<int> & ,
                                std::vector< CTrianglesGroup*> & );
    void merge_small_face_groups(std::vector< CTrianglesGroup*> & );
    void compose_individual_textures(std::vector< CTrianglesGroup*> & , std::vector<Vector3f> & );
    void construct_texture_atlas(std::vector< CTrianglesGroup*> & );
    void clean_intermediate_data(std::vector< CTrianglesGroup*> & );


    CTriangularMesh * _mesh;
    int _frame_step;
    float _merge_group_thres;
    float _max_color_adjust_iterations;

    CTriMesh * _tri_mesh;
    std::vector<int> _frame_ref;
    std::vector<CTrianglesGroup*> _group_ref;

    void construct_face_group(std::list<int> & face_group_current,
            std::list<int> & face_group_next,
            int frame_ref_group,
            CTrianglesGroup* tg,
            std::list<int> & face_group);

   // CTrianglesGroup* find_largest_neighbour( std::vector< CTrianglesGroup*> & face_groups,
   //                                          CTrianglesGroup* current_group, int current_group_idx);

    void merge_with_largest_neighbour(CTrianglesGroup*,
                                      std::vector< CTrianglesGroup*> &  );


};

#endif // CTEXTURINGTHREAD_H
