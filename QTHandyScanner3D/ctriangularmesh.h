#ifndef CTRIANGULARMESH_H
#define CTRIANGULARMESH_H

#include<vector>
#include<QString>

#include "cglobject.h"
#include "graphicutils.h"
#include "ciostate.h"
#include "opencv_aux_utils.h"

class CollisionModel3D;
class CGLCamera;

class CTriangularMesh : public CGLObject, public CIOState
{
public:
    enum RenderingMode{MESH_WIRE=0, MESH_FLAT=1, MESH_SMOOTH=2, MESH_TEX=4};

    CTriangularMesh();
    CTriangularMesh(const CTriangularMesh &);

    virtual ~CTriangularMesh();

    virtual void create_glList();
    void create_glList_wire();
    void create_glList_flat();
    void create_glList_smooth();
    void create_glList_tex();

    void set_mesh(CTriMesh * m);
    void set_rendering_mode(RenderingMode rm);
    RenderingMode get_rendering_mode(){return _rmode;}

    bool m_export(QString & file);
    bool m_export_off(QString & file);
    bool m_export_obj(QString & file);
    bool m_import_obj(QString &file);
    bool m_export_xyz(QString & fname);

    void save(std::ofstream & ofile);
    void load(std::ifstream & ifile);

    bool is_textured();
    QString get_texture_name();

    CTriMesh * get_tri_mesh(){return _mesh;}

    void replace_mesh_texture(QImage * tex);
    void remove_mesh_texture();

    virtual void remake_glList();

    void use_glob_coloring()
    {
        _use_glob_coloring=true;
        remake_glList();
    }

    void use_loc_coloring()
    {
        _use_glob_coloring=false;
        remake_glList();
    }



    void update_section( std::vector<cv::Point2f> & v, CGLCamera * c );
    void clear_selection();
    CollisionModel3D * construct_collision_model();
    void process_selected_faces();
    void process_selected_faces_old();


protected:
    void delete_mesh();
    void delete_glLists();
    void delete_glList_tex();

    void set_correct_glid();
    void load_tex_matid();

    CTriMesh * _mesh;
    RenderingMode _rmode;

    GLuint _wire_glid;
    GLuint _flat_glid;
    GLuint _smooth_glid;
    GLuint _tex_glid;
    GLuint _tex_matid;

    bool _use_glob_coloring;


};

#endif // CTRIANGULARMESH_H
