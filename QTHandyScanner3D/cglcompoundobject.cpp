#include "cglcompoundobject.h"
#include <QTreeWidget>
#include "commonutils.h"
#include <QMessageBox>
#include "messages.h"

// #include "ctexpreview.h"
#include "menu_vew_actions.h"
#include "mainwindow.h"
#include <iostream>

int global_compound_object_index = 0;

CGLCompoundObject::CGLCompoundObject(QWidget * treew, MenuActions * va, bool select, QString ttl) : _title(ttl), _raw_points(NULL), _optim_points(NULL),
    _normal_points(NULL), _mesh(NULL),  _dpcl(NULL),
_vmode(NONE), _prev_vmode(NONE), _root_item(NULL),
    _rawp_item(NULL), _proc_tex_item(NULL),/*_raw_tex_item(NULL),  _proc_tex_subitem(NULL),
    _optim_item(NULL) , _norm_item(NULL),*/ _surf_item(NULL),
    _is_current(select),_unique_object(false)
{
   _treew = (QTreeWidget*)treew;
   _va = va;
    _normal_font = QFont("Times", 8,QFont::Normal, false);
    _div_font    = QFont("Times", 8,QFont::Normal, true);
    _sel_font    = QFont("Times", 8,QFont::Bold, false);

    _it_raw_video   = NULL;
    _it_raw_pcloud  = NULL;
    _it_raw_texture = NULL;
    _it_raw_decim_surface = NULL;

    init_treew_root();

    update_menu_interface();
}


void CGLCompoundObject::save(std::ofstream & ofile)
{
    int title_len = _title.length();
    char * title_buf = _title.toLocal8Bit().constData();

    // title
    ofile.write(reinterpret_cast<const char *>(&title_len), sizeof(int));
    ofile.write(title_buf, title_len);

    // vmode
    ofile.write(reinterpret_cast<const char *>(&_vmode), sizeof(ViewingMode));

    // is_current
    ofile.write(reinterpret_cast<const char *>(&_is_current), sizeof(bool));

    // unique_object
    ofile.write(reinterpret_cast<const char *>(&_unique_object), sizeof(bool));


   // raw points
    bool have_raws = (_raw_points == NULL)? false : true;
    ofile.write(reinterpret_cast<const char *>(&have_raws), sizeof(bool));
    if(have_raws)
        _raw_points->save(ofile);        

    // optim points
    bool have_optims = (_optim_points == NULL)? false : true;
    ofile.write(reinterpret_cast<const char *>(&have_optims), sizeof(bool));
    if(have_optims)
        _optim_points->save(ofile);

    bool have_normals = (_normal_points == NULL)? false : true;
    ofile.write(reinterpret_cast<const char *>(&have_normals), sizeof(bool));
    if(have_normals)
        _normal_points->save(ofile);

    bool have_mesh = (_mesh == NULL)? false : true;
    ofile.write(reinterpret_cast<const char *>(&have_mesh), sizeof(bool));
    if(have_mesh)
        _mesh->save(ofile);

    int tex_count = _raw_textures.size();
    ofile.write(reinterpret_cast<const char *>(&tex_count), sizeof(int));
    std::vector<CRawTexture*>::iterator itt = _raw_textures.begin();
    for( ; itt != _raw_textures.end(); itt++)
        (*itt)->save(ofile);

    int dpcl_sz = (_dpcl == NULL)?0:_dpcl->size();
    ofile.write(reinterpret_cast<const char *>(&dpcl_sz), sizeof(int));
    if(_dpcl != NULL)
    {
        std::vector<C3DPoint>::iterator itp = _dpcl->begin();
        for( ; itp != _dpcl->end(); itp++)
            (*itp).save(ofile);
    }


}

void CGLCompoundObject::load(std::ifstream & ifile)
{
    // title len
    int title_len = 0;
    ifile.read(reinterpret_cast<char *>(&title_len), sizeof(int));

    // title buf
    char * title_buf = new char [title_len];
    ifile.read(title_buf, title_len);
    _title = QString::fromLocal8Bit(title_buf, title_len);
    delete [] title_buf;

    // vmode
    ifile.read(reinterpret_cast<char *>(&_vmode), sizeof(ViewingMode));

    // is_current
    ifile.read(reinterpret_cast<char *>(&_is_current), sizeof(bool));

    // is_current
    ifile.read(reinterpret_cast<char *>(&_unique_object), sizeof(bool));

    // raw points
    bool have_raws = false;
    ifile.read(reinterpret_cast<char *>(&have_raws), sizeof(bool));
    if(have_raws)
    {
        delete_raw_points();
        _raw_points = new CPointCloud;
        _raw_points->load(ifile);
    }

    bool have_optims = false;
    ifile.read(reinterpret_cast<char *>(&have_optims), sizeof(bool));
    if(have_optims)
    {
        delete_optim_points();
        _optim_points = new CPointCloud;
        _optim_points->load(ifile);
    }

    bool have_normals = false;
    ifile.read(reinterpret_cast<char *>(&have_normals), sizeof(bool));
    if(have_normals)
    {
        delete_normal_points();
        _normal_points = new CNormalCloud;
        _normal_points->load(ifile);
    }

    bool have_mesh = false;
    ifile.read(reinterpret_cast<char *>(&have_mesh), sizeof(bool));
    if(have_mesh)
    {
        delete_mesh();
        _mesh = new CTriangularMesh;
        _mesh->load(ifile);
    }

    _raw_textures.clear();
    int tex_count = 0;
    ifile.read(reinterpret_cast<char *>(&tex_count), sizeof(int));
    for (int t = 0; t < tex_count; t++)
    {
        CRawTexture * rt = new CRawTexture;
        rt->load(ifile);
        _raw_textures.push_back(rt);
    }



   int dpcl_sz = 0;
   ifile.read(reinterpret_cast<const char *>(&dpcl_sz), sizeof(int));
   if(dpcl_sz > 0)
   {
       delete_dpcl();
       _dpcl = new  std::vector<C3DPoint>;
       for (int i = 0; i < dpcl_sz ; i++)
       {
           C3DPoint p;
           p.load(ifile);
           _dpcl->push_back(p);
       }
   }
}


void CGLCompoundObject::extend_title(QString add)
{
    _title = _title + add;
}

void CGLCompoundObject::init_treew_root()
{
    return;

    _root_item = new QTreeWidgetItem();
    if(_title.isEmpty())
    {
        _title = QString("SCAN%1").arg(global_compound_object_index,2);
        global_compound_object_index++;
    }
    _root_item->setText(0, _title);
    _root_item->setIcon(1, QPixmap(":/images/icons/se_eyeopen.png"));
    _root_item->setIcon(2, QPixmap(":/images/icons/scan_del.png"));

    _treew->insertTopLevelItem(0, _root_item);

    ////_root_item->setChildIndicatorPolicy(QTreeWidgetItem::ShowIndicator);
    ////_root_item->setExpanded(true);

    init_treew_children();
}

void  CGLCompoundObject::select_object(bool v)
{
    _is_current = v;
    if(v)_treew->expandItem(_root_item);
}

void CGLCompoundObject::init_treew_children()
{
    return;

    _rawp_item = new QTreeWidgetItem(_root_item, 0);
    _treew->insertTopLevelItem(_root_item, _rawp_item);

    // _raw_tex_item = new QTreeWidgetItem(_root_item, 0);
    // _treew->insertTopLevelItem(_root_item, _raw_tex_item);

    //_proc_tex_subitem = new QTreeWidgetItem(_proc_tex_item, 0);
    // _treew->insertTopLevelItem(_proc_tex_item, _proc_tex_subitem );

    // _optim_item = new QTreeWidgetItem(_root_item, 0);
    //_treew->insertTopLevelItem(_root_item, _optim_item );

    //_norm_item = new QTreeWidgetItem(_root_item, 0);
    //_treew->insertTopLevelItem(_root_item, _norm_item );

    _surf_item = new QTreeWidgetItem(_root_item, 0);
     _treew->insertTopLevelItem(_root_item, _surf_item );

     _proc_tex_item = new QTreeWidgetItem(_root_item, 0);
     _treew->insertTopLevelItem(_root_item, _proc_tex_item);


     update_treew_children();
    _treew->expandAll();
}

void CGLCompoundObject::delete_tree_raw_textures_childs()
{
/*    if (_raw_tex_item == NULL) return;

    int tcount = _raw_tex_item->childCount();
    if(tcount == 0 ) return;

    for (int t = tcount -1 ; t >= 0; t-- )
        _raw_tex_item->removeChild(_raw_tex_item->child(t));
*/
}

void CGLCompoundObject::create_tree_raw_textures_childs()
{
/*    if (_raw_tex_item == NULL) return;
    if (_raw_textures.size() == 0) return;

    delete_tree_raw_textures_childs();

    std::vector<CRawTexture*>::iterator it = _raw_textures.begin();
    for( ; it != _raw_textures.end(); it++)
    {
         QTreeWidgetItem * tex_child = new QTreeWidgetItem(_raw_tex_item,0);
         tex_child ->setFont(0, _normal_font);
         tex_child ->setText(0, (*it)->_name);
        _raw_tex_item->addChild(tex_child);
    }
*/
}

void CGLCompoundObject::delete_trew_structure()
{
    return;

    if(_rawp_item)
    {
        _root_item->removeChild(_rawp_item);
        delete _rawp_item;
        _rawp_item = NULL;
    }

    /*if(_raw_tex_item)
    {
        _root_item->removeChild(_raw_tex_item);
        delete _raw_tex_item;
        _raw_tex_item = NULL;
    }

    if(_proc_tex_subitem )
    {
        _proc_tex_item->removeChild(_proc_tex_subitem );
        delete _proc_tex_subitem ;
        _proc_tex_subitem = NULL;
    }*/

    if(_proc_tex_item)
    {
        /*QTreeWidgetItem * tex_name = _proc_tex_item->child(0);
        if(tex_name)
            _proc_tex_item->removeChild(tex_name);
        _root_item->removeChild(_proc_tex_item);
        */
       _root_item->removeChild(_proc_tex_item);
        delete _proc_tex_item;
        _proc_tex_item = NULL;
    }

    /*if(_optim_item)
    {
        _root_item->removeChild(_optim_item);
        delete _optim_item;
        _optim_item = NULL;
    }*/


   /* if(_norm_item)
    {
        _root_item->removeChild(_norm_item);
        delete _norm_item;
        _norm_item = NULL;
    }*/

    if(_surf_item)
    {
        _root_item->removeChild(_surf_item);
        delete _surf_item;
        _surf_item = NULL;
    }

    if(_root_item)
    {
        _treew->removeItemWidget(_root_item, 0);
        delete _root_item;
        _root_item = NULL;
    }
}
void CGLCompoundObject::update_selection_state(QTreeWidgetItem * item, int col)
{
    if (col == 0 && item->parent() == _root_item)
    {
        if(_rawp_item == item )
            set_view_mode(RAW_POINTS);
        //if(_optim_item == item )
        //    set_view_mode(OPTIM_POINTS);
        //if(_norm_item == item)
         //   set_view_mode(NORMALS);
        else if(_surf_item == item)
           set_view_mode(MESH);
        else if(_proc_tex_item == item)
        {
            if(_mesh != NULL)
            {
                CTriMesh * tmesh = _mesh->get_tri_mesh();
                if( tmesh != NULL)
                {
                    if(tmesh->_texture != NULL)
                    {
                        //    CTexPreview * dlg = new CTexPreview(this, *tmesh->_texture,
                        //                            QString(tmesh->_texture_name.c_str()));
                         //   dlg->show();
                         //   dlg->raise();
                    }
                }

            }
        }
        return;
    }

    /*if(item->parent() == _raw_tex_item)
    {

        int idx = _raw_tex_item->indexOfChild(item);
        if(idx >= 0 && idx < _raw_textures.size())
        {
            CTexPreview * dlg = new CTexPreview(*(_raw_textures[idx]->_img), _raw_textures[idx]->_name);
            dlg->show();
            dlg->raise();
        }
    }*/

}

void CGLCompoundObject::update_visability_state(QTreeWidgetItem * item, int col)
{
    if(col == 1 && item == _root_item)
    {
        if(_vmode != NONE)
        {
            _prev_vmode = _vmode;
             set_view_mode(NONE);
        }
        else
        {
            set_view_mode(_prev_vmode);
        }
    }
}


void CGLCompoundObject::update_surface_state(QTreeWidgetItem * item, int col)
{
    if (col == 0) return;

    if(_surf_item == item )
    {
        switch(col)
        {
         case 1:
             if(_mesh) _mesh->set_rendering_mode(CTriangularMesh::MESH_WIRE);
             break;
         case 2:
             if(_mesh) _mesh->set_rendering_mode(CTriangularMesh::MESH_FLAT);
             break;
         case 3:
             if(_mesh) _mesh->set_rendering_mode(CTriangularMesh::MESH_SMOOTH);
             break;
         case 4:
         default:
             if(_mesh) _mesh->set_rendering_mode(CTriangularMesh::MESH_TEX);
             break;
        }
    }
    update_treew_children();
}


void CGLCompoundObject::clean_gllook()
{
    if(_mesh != NULL)
        // _mesh->create_glList();
        _mesh->remake_glList();
}

void CGLCompoundObject::update_slection_raw_points(int x, int y, int w, int h,
                                                   std::vector<cv::Point2f> & v, CPointCloud::SelectMode sm)
{
    if(_raw_points == NULL) return;
    _raw_points ->update_section(x,y,w,h,v, sm );

}

void  CGLCompoundObject::update_slection_mesh_faces(std::vector<cv::Point2f> & v, CGLCamera * cam )
{
    if(_mesh == NULL) return;
    _mesh->update_section( v, cam );

}

bool CGLCompoundObject::is_there_selected_raw_points()
{
    if(_raw_points == NULL) return false;
    return _raw_points->is_there_selected();
}

void CGLCompoundObject::clear_slected_features()
{
    if(_raw_points != NULL)
        _raw_points ->clear_selection();

    if(_mesh != NULL)
          _mesh ->clear_selection();
}

void CGLCompoundObject::clean_rawp_gllook()
{
    if(_raw_points != NULL)
        _raw_points ->remake_glList();
}

bool CGLCompoundObject::is_update_child_selection_state(QTreeWidgetItem *item, int col)
{
    return false;

    if(/*col == 0 && */ item->parent() ==_root_item)
        return true;
    return false;
}

bool CGLCompoundObject::is_update_root_selection_state(QTreeWidgetItem *item, int col)
{
    return false;

    if(col == 0 && item ==_root_item)
        return true;
    return false;
}

bool CGLCompoundObject::is_remove_state(QTreeWidgetItem *item, int col)
{
    return false;

    if(col == 2 && item == _root_item)
        return true;
    return false;
}

void CGLCompoundObject::set_mesh_render_mode(CTriangularMesh::RenderingMode rm)
{
    if(_mesh != NULL)
        _mesh->set_rendering_mode(rm);

    update_treew_children();
    update_menu_interface();
}

void CGLCompoundObject::update_menu_interface()
{


    if (  (_vmode != RAW_POINTS) && (_vmode != MESH) && (_vmode != MESHCAST) ) return;
    if(_va == NULL) return;
    if(!_is_current) return;

    if(_raw_points == NULL)
    {
       _va->view_raw_point_Act->setEnabled(false);
       _va->view_shad_pnt_Act->setEnabled(false);
    }
    else
    {
        _va->view_raw_point_Act->setEnabled(true);
        _va->view_shad_pnt_Act->setEnabled(true);
    }

    if((_mesh == NULL) )
    {

        _va->view_mesh_wire_Act->setEnabled(false);
        _va->view_mesh_flat_Act->setEnabled(false);
        _va->view_mesh_smoo_Act->setEnabled(false);
        _va->view_mesh_tex_Act->setEnabled(false);

        _va->fbtn_vw_wr->setEnabled(false);
        _va->fbtn_vw_tx->setEnabled(false);
        _va->fbtn_vw_fl->setEnabled(false);
        _va->fbtn_vw_sm->setEnabled(false);
    }
    else
    {
        _va->view_mesh_wire_Act->setEnabled(true);        
        _va->view_mesh_flat_Act->setEnabled(true);
        _va->view_mesh_smoo_Act->setEnabled(true);

        _va->fbtn_vw_wr->setEnabled(true);
        _va->fbtn_vw_fl->setEnabled(true);
        _va->fbtn_vw_sm->setEnabled(true);

            QString msg = sbInfo.arg(_mesh->get_tri_mesh()->_triangles.size()/1000);
            _va->_main_window->setStatusBarInfo(msg);

            if(_mesh->get_tri_mesh()->_texture == NULL)
            {
                _va->view_mesh_tex_Act->setEnabled(false);
                _va->fbtn_vw_tx->setEnabled(false);
            }
            else
            {
                _va->view_mesh_tex_Act->setEnabled(true);
                _va->fbtn_vw_tx->setEnabled(true);
            }
    }

    if(_vmode == RAW_POINTS)
       _va->view_raw_point_Act->setChecked(true);
    else if ((_vmode == MESH) && (_mesh != NULL) )
    {
        CTriangularMesh::RenderingMode rm = _mesh->get_rendering_mode();
        switch(rm)
        {
            case CTriangularMesh::MESH_WIRE:
                _va->view_mesh_wire_Act->setChecked(true);
                _va->fbtn_vw_wr->setChecked(true);
                _va->fbtn_vw_fl->setChecked(false);
                _va->fbtn_vw_sm->setChecked(false);
                _va->fbtn_vw_tx->setChecked(false);
                break;
            case CTriangularMesh::MESH_FLAT:
                _va->view_mesh_flat_Act->setChecked(true);
                _va->fbtn_vw_wr->setChecked(false);
                _va->fbtn_vw_fl->setChecked(true);
                _va->fbtn_vw_sm->setChecked(false);
                _va->fbtn_vw_tx->setChecked(false);
                break;
            case CTriangularMesh::MESH_TEX:
                _va->view_mesh_tex_Act->setChecked(true);
                _va->fbtn_vw_wr->setChecked(false);
                _va->fbtn_vw_fl->setChecked(false);
                _va->fbtn_vw_sm->setChecked(false);
                _va->fbtn_vw_tx->setChecked(true);
                break;
            default:
            case CTriangularMesh::MESH_SMOOTH:
                _va->view_mesh_smoo_Act->setChecked(true);
                _va->fbtn_vw_wr->setChecked(false);
                _va->fbtn_vw_fl->setChecked(false);
                _va->fbtn_vw_sm->setChecked(true);
                _va->fbtn_vw_tx->setChecked(false);
                break;
        }
    }

    _va->_main_window->updateFieldButton();
}

void CGLCompoundObject::update_treew_children()
{
    return;

    _root_item->setText(0, _title);
    if(_vmode != NONE)
        _root_item->setIcon(1, QPixmap(":/images/icons/se_eyeopen.png"));
    else
        _root_item ->setIcon(1, QPixmap(":/images/icons/se_eyeclosed.png"));

    if(_is_current)
        _root_item->setFont(0, _sel_font);
    else
        _root_item->setFont(0, _normal_font);

    if(_raw_points == NULL)
    {
        _rawp_item->setFont(0, _div_font);
        _rawp_item ->setText(0, "raw points");
    }
    else
    {
        _rawp_item->setFont(0, _normal_font);
        QString rtitle = QString("raw points (%1)").arg(get_separated_int(_raw_points->get_points_count()));
        _rawp_item ->setText(0,  rtitle);
    }
    if(_raw_points == NULL)
        _rawp_item->setIcon(0, QPixmap(":/images/icons/se_eyeclosed_trans.png"));
    else
    {
        if(_vmode == RAW_POINTS)
            _rawp_item->setIcon(0, QPixmap(":/images/icons/se_eyeopen.png"));
        else
            _rawp_item ->setIcon(0, QPixmap(":/images/icons/se_eyeclosed.png"));
    }

   /* bool is_raw_textures = (_raw_textures.size())? true: false;
    if(!is_raw_textures)
    {
        _raw_tex_item->setFont(0, _div_font);
        _raw_tex_item->setText(0, "raw textures (0)");
        _raw_tex_item->setIcon(0, QPixmap(":/images/icons/tex_gray.png"));

    }
    else
    {
        _raw_tex_item->setFont(0, _normal_font);
        QString ttl = QString("raw textures (") + QString::number(_raw_textures.size())+QString(")");
        _raw_tex_item->setText(0, ttl);
        _raw_tex_item->setIcon(0, QPixmap(":/images/icons/tex_color.png"));
        create_tree_raw_textures_childs();
    }*/



    /*if(_optim_points == NULL)
    {
        _optim_item->setFont(0, _div_font);
        _optim_item ->setText(0, "optim points");
    }
    else
    {
        _optim_item->setFont(0, _normal_font);
        QString optitle = QString("optim points (%1)").arg(get_separated_int(_optim_points->get_points_count()));
        _optim_item ->setText(0,  optitle);
    }

    if(_optim_points == NULL)
        _optim_item->setIcon(0, QPixmap(":/images/icons/se_eyeclosed_trans.png"));
    else
    {
        if(_vmode == OPTIM_POINTS)
            _optim_item ->setIcon(0, QPixmap(":/images/icons/se_eyeopen.png"));
        else
            _optim_item ->setIcon(0, QPixmap(":/images/icons/se_eyeclosed.png"));
    }*/

    /* if(_normal_points == NULL)
        _norm_item->setFont(0, _div_font);
    else
        _norm_item->setFont(0, _normal_font);
    _norm_item ->setText(0, "normals");
    if(_normal_points == NULL)
        _norm_item->setIcon(0, QPixmap(":/images/icons/se_eyeclosed_trans.png"));
    else
    {
        if(_vmode == NORMALS)
            _norm_item ->setIcon(0, QPixmap(":/images/icons/se_eyeopen.png"));
        else
            _norm_item ->setIcon(0, QPixmap(":/images/icons/se_eyeclosed.png"));
    }*/

    if(_mesh == NULL)
        _surf_item->setFont(0, _div_font);
    else
        _surf_item->setFont(0, _normal_font);
    _surf_item ->setText(0, "surface");
    if(_mesh == NULL)
    {
        _surf_item->setIcon(0, QPixmap(":/images/icons/se_eyeclosed_trans.png"));
        _surf_item->setIcon(1, QPixmap());
        _surf_item->setIcon(2, QPixmap());
        _surf_item->setIcon(3, QPixmap());
        _surf_item->setIcon(4, QPixmap());

    }
    else
    {
        if(_vmode == MESH)
            _surf_item ->setIcon(0, QPixmap(":/images/icons/se_eyeopen.png"));
        else
            _surf_item ->setIcon(0, QPixmap(":/images/icons/se_eyeclosed.png"));

        CTriangularMesh::RenderingMode rm = _mesh->get_rendering_mode();
        if( rm == CTriangularMesh::MESH_WIRE)
            _surf_item ->setIcon(1, QPixmap(":/images/icons/surface_wire_1.png"));
        else
            _surf_item ->setIcon(1, QPixmap(":/images/icons/surface_wire_2.png"));

        if(rm == CTriangularMesh::MESH_FLAT)
            _surf_item ->setIcon(2, QPixmap(":/images/icons/surface_flat_1.png"));
        else
            _surf_item ->setIcon(2, QPixmap(":/images/icons/surface_flat_2.png"));

        if(rm == CTriangularMesh::MESH_SMOOTH)
            _surf_item ->setIcon(3, QPixmap(":/images/icons/surface_smooth_1.png"));
        else
            _surf_item ->setIcon(3, QPixmap(":/images/icons/surface_smooth_2.png"));

        if(rm == CTriangularMesh::MESH_TEX)
            _surf_item ->setIcon(4, QPixmap(":/images/icons/surface_tex_1.png"));
        else
            _surf_item ->setIcon(4, QPixmap(":/images/icons/surface_tex_2.png"));

    }

    bool is_textured = false;
    if(_mesh != NULL)
        is_textured = _mesh->is_textured();

    if(is_textured)
    {
        _proc_tex_item->setFont(0, _normal_font);
        _proc_tex_item->setIcon(0, QPixmap(":/images/icons/tex_color.png"));


        //_proc_tex_subitem->setFont(0, _normal_font);
        //_proc_tex_subitem->setText(0,_mesh->get_texture_name());

        //if(_proc_tex_item->childCount() == 0)
         //   _proc_tex_item->addChild(_proc_tex_subitem);
           // _treew->insertTopLevelItem(_proc_tex_item, _proc_tex_subitem );
    }
    else
    {
        _proc_tex_item->setFont(0, _div_font);
        _proc_tex_item->setIcon(0, QPixmap(":/images/icons/tex_gray.png"));

        //if(_proc_tex_item->childCount() != 0)
         //   _proc_tex_item->removeChild(_proc_tex_item->child(0));
    }
    _proc_tex_item->setText(0, "texture");
}

CGLCompoundObject::~CGLCompoundObject()
{
    delete_raw_points();
    delete_optim_points();
    delete_normal_points();
    delete_mesh();
    delete_trew_structure();
    delete_raw_textures();
    ////delete_meshcasts();
    delete_dpcl();
}

void CGLCompoundObject::replace_texture(QImage * tex)
{
    if(_mesh !=NULL && _mesh->is_textured() && tex)
    {
        _mesh->replace_mesh_texture(tex);
        set_mesh_render_mode(CTriangularMesh::MESH_TEX);
    }
}

void CGLCompoundObject::set_shad_raw_points(bool v)
{
    if(_raw_points)
    {
        _raw_points->_use_viewref = v;
        _raw_points->remake_glList();
    }

    /// update_refdependant_glRepo();

    /// update_treew_children();
    /// update_menu_interface();
}

void CGLCompoundObject::set_common_colors_raw_points(bool v)
{
    if(_raw_points)
    {
        _raw_points->_use_common_color = v;
        _raw_points->remake_glList();
    }

}

bool CGLCompoundObject::set_view_mode(ViewingMode mode)
{
    switch(mode)
    {
    case RAW_POINTS:
        if(_raw_points)   _vmode = RAW_POINTS;
        break;
   case NORMALS:
        if(_normal_points) _vmode = NORMALS;
        break;
    case MESH:
        if(_mesh) _vmode = MESH;
        break;
    case MESHCAST:
    case NONE:
    default:
        _vmode = NONE;
    }
    update_refdependant_glRepo();
    update_treew_children();
    update_menu_interface();
}

bool CGLCompoundObject::import_raw_points(QString & file)
{
   delete_raw_points();
   _raw_points = new CPointCloud;
   if(!_raw_points->import(file))
   {
       delete _raw_points;
       _raw_points = NULL;
        return false;
   }
   set_view_mode(RAW_POINTS);
   return true;
}

bool CGLCompoundObject::set_raw_points(std::vector<C3DPoint> & pnts)
{
    delete_raw_points();
    _raw_points = new CPointCloud;
   _raw_points->set_points(pnts);
    set_view_mode(RAW_POINTS);
    return true;
}



bool CGLCompoundObject::add_raw_points(std::vector<C3DPoint> & pnts)
{
    if(_raw_points == NULL)
        return set_raw_points(pnts); // first call
    _raw_points->add_points(pnts);
    _raw_points->remake_glList();
     set_view_mode(RAW_POINTS);
     return true;
}

/*
void CGLCompoundObject::add_new_meshcast(int fidx, CTriMesh *m)
{
    if(_meshcasts == NULL)
        _meshcasts = new CGLMeshCasts;

    _meshcasts->add_new_meshcast(fidx, m);
    set_view_mode(MESHCAST);
}*/

void CGLCompoundObject::add_raw_texture(CRawTexture *tex)
{
    _raw_textures.push_back(tex);
}

void CGLCompoundObject::export_mesh_xyz(QString & file)
{
    if(_mesh == NULL)
    {
        QMessageBox::warning(NULL, QObject::tr("Warning"), wrn_CannotExportMesh);
        return;
    }
   _mesh->m_export_xyz(file);

}

void CGLCompoundObject::export_mesh(QString & file)
{
    if(_mesh == NULL)
    {
        QMessageBox::warning(NULL, QObject::tr("Warning"), wrn_CannotExportMesh);
        return;
    }
   _mesh->m_export(file);
}

bool CGLCompoundObject::import_mesh(QString & file)
{
    delete_mesh();
    _mesh = new CTriangularMesh;
    _mesh->m_import_obj(file);
    set_view_mode(MESH);

    return true;
}

void CGLCompoundObject::export_optim_points(QString & file)
{
    if(_optim_points == NULL)
    {
        QMessageBox::warning(NULL, QObject::tr("Warning"), wrn_CannotExportOptimXYZ);
        return;
    }
    _optim_points->pexport(file);
}

void CGLCompoundObject::export_raw_points(QString & file)
{
    if(_raw_points == NULL)
    {
        QMessageBox::warning(NULL, QObject::tr("Warning"), wrn_CannotExportRawXYZ);
        return;
    }
    _raw_points->pexport(file);
}

void CGLCompoundObject::glDraw()
{
    switch(_vmode)
    {
    case RAW_POINTS:
        if(_raw_points) _raw_points->glDraw();
        break;
    //case OPTIM_POINTS:
    //    if(_optim_points) _optim_points->glDraw();
    //    break;
    //case NORMALS:
    //    if(_normal_points) _normal_points->glDraw();
    //    break;
    case MESH:
        if(_mesh) _mesh->glDraw();
        break;
    }
}

void CGLCompoundObject::update_refdependant_glRepo()
{
//    std::cout << "update_refdependant_glRepo" << std::endl;
 if(_raw_points && (_vmode & RAW_POINTS))
      if(_raw_points->_use_viewref)
          _raw_points->remake_glList();



  //if(_optim_points && (_vmode & OPTIM_POINTS))
  //      _optim_points->remake_glList();
}

void CGLCompoundObject::delete_raw_points()
{
    if(_raw_points != NULL)
    {
        delete _raw_points;
        _raw_points = NULL;
    }
}


void CGLCompoundObject::delete_selected_raw_points()
{
    if(_raw_points != NULL)
       _raw_points->delete_selected_points();
}

void CGLCompoundObject::process_selected_faces()
{
    if(_mesh != NULL)
       _mesh->process_selected_faces();
}

void CGLCompoundObject::set_raw_points(CPointCloud * npnts)
{
    delete_raw_points();
    _raw_points = npnts;
    set_view_mode(RAW_POINTS);

}

void CGLCompoundObject::set_mesh(CTriangularMesh * mesh)
{
   delete_mesh();
    _mesh = mesh;
    set_view_mode(MESH);
}

void CGLCompoundObject::update_glmesh()
{
    if(_mesh)
        _mesh->remake_glList();
}

void CGLCompoundObject::delete_optim_points()
{
    if(_optim_points)
    {
        delete _optim_points;
        _optim_points = NULL;
    }
}

void CGLCompoundObject::delete_normal_points()
{
    if(_normal_points)
    {
        delete _normal_points;
        _normal_points = NULL;
    }
}


void CGLCompoundObject::set_optim_points(CPointCloud * npnts)
{
    delete_optim_points();
    _optim_points = npnts;
    set_view_mode(OPTIM_POINTS);
}

void CGLCompoundObject::set_normal_points(CNormalCloud * nnpnts)
{
    delete_normal_points();
    _normal_points = nnpnts;
    set_view_mode(NORMALS);
}

void CGLCompoundObject::delete_mesh()
{
    if(_mesh)
    {
        delete _mesh;
        _mesh = NULL;
    }
}
void CGLCompoundObject::set_dpcl(std::vector<C3DPoint> * pnts)
{
    delete_dpcl();
    _dpcl = pnts;
}

void CGLCompoundObject::delete_dpcl()
{
   if(_dpcl)
   {
       delete _dpcl;
       _dpcl = NULL;
   }
}

/*
void CGLCompoundObject::delete_meshcasts()
{
    if(_meshcasts)
    {
        delete _meshcasts;
        _meshcasts = NULL;
    }
}*/

void CGLCompoundObject::delete_raw_textures()
{
    std::vector<CRawTexture*>::iterator it = _raw_textures.begin();
    for( ; it != _raw_textures.end(); it++ )
        delete *it;
}
