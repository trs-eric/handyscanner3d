#include "cdevcontrolthread.h"

#include <iostream>
#include <unistd.h>
CDevControlThread::CDevControlThread(PVOID dh, QObject *parent)  :
  _dev_handle(dh),  QObject(parent), _abort(false)
{
}

void CDevControlThread::stop_process()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();

    driv::motor_stop(_dev_handle);
    std::cout << "STOP MOTOR" << std::endl;
}

void CDevControlThread::motor_left_request()
{
    stop_process();
    emit finished();

    _mode = MOTOR_LEFT;

    driv::vmotor_on   = true;
    driv::vmotor_left = true;
    driv::vmotor_step = true;
    driv::update_motor_status();

    emit dev_motor_left();
}

void CDevControlThread::turn_table_request()
{

    stop_process();
    emit finished();

    _mode = TABLE_ROT;
    emit dev_turn_table();

}

void CDevControlThread::motor_right_request()
{

    stop_process();
    emit finished();

    _mode = MOTOR_RIGHT;

    driv::vmotor_on   = true;
    driv::vmotor_left = false;
    driv::vmotor_step = true;
    driv::update_motor_status();

    emit dev_motor_right();
}

void CDevControlThread::go_motor_step()
{
    while(!_abort)
    {
        driv::motor_step(_dev_handle);
    }
}

void CDevControlThread::go_table_rot()
{
    driv::table_rot(_dev_handle);
}

void CDevControlThread::run()
{
    _mutex.lock();
    _abort = false;
    _mutex.unlock();

 /*   std::cout << " CDevControlThread::run() " << std::endl;


    switch(_mode)
    {
        case MOTOR_LEFT:
        case MOTOR_RIGHT:
            go_motor_step();
            break;

        case TABLE_ROT:
            go_table_rot();
            break;
    }
*/

    int v = driv::get_laser_angle(_dev_handle);
    emit finish_lasangle(v);
    emit finished();


}
