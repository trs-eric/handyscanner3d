#include "cfreeglcamera.h"
#include "graphicutils.h"


// TODO move_left
// TODO wheel
CFreeGLCamera::CFreeGLCamera()
{
    init();
}

CFreeGLCamera::~CFreeGLCamera()
{
}


void CFreeGLCamera::init()
{

    _tx = 0.0f;
    _ty = 5.0f;
    _tz = 15.0f;

    _focal_len = sqrt(_tx * _tx + _ty * _ty + _tz * _tz);

    _cx = 0;
    _cy = 0;
    _cz = 0;

    _rotx = IBGM(atan((_ty - _cy)/ (_tz - _cz) ));
    _roty = -180;
    _rotz = 0;
}

void CFreeGLCamera::recompute_tpos()
{
    _tx = _cx + _focal_len * sin(BGM(_roty)) * cos(BGM(_rotx));
    _ty = _cy + _focal_len * sin(BGM(_rotx));
    _tz = _cz - _focal_len * cos(BGM(_rotx)) * cos(BGM(_roty)) ;
}

void CFreeGLCamera::update_target()
{
    _cx = _tx - _focal_len * sin(BGM(_roty)) * cos(BGM(_rotx));
    _cy = _ty - _focal_len * sin(BGM(_rotx));
    _cz = _tz + _focal_len * cos(BGM(_rotx)) * cos(BGM(_roty)) ;
}

void CFreeGLCamera::move_forward   (float v)
{
    float new_flen  = _focal_len - v;
    if ((new_flen < 1.0) || (new_flen > 100.0)) return;

    _focal_len = new_flen;
    recompute_tpos();
}

void CFreeGLCamera::move_backward  (float v)
{
    move_forward(-v);
}

void CFreeGLCamera::move_right	(float v)
{
    move_left(-v);
}

void CFreeGLCamera::move_left	(float v)
{
    _tx += v * cos(BGM(_roty)); // clear it
    _tz += v * sin(BGM(_roty));
    update_target();
}

void CFreeGLCamera::move_up		(float v)
{
    _ty += v;
    update_target();
}

void CFreeGLCamera::move_down	(float v)
{
    move_up(-v);
}

void CFreeGLCamera::turn_left	(float v)
{
    turn_right(-v);
}

void CFreeGLCamera::turn_right	(float v)
{
    _roty = (_roty + v );
    recompute_tpos();
}

void CFreeGLCamera::turn_up	     (float v)
{
    turn_down(-v);
}

void CFreeGLCamera::turn_down    (float v)
{

    float diff_x = _rotx - v;
    if(( diff_x < -70.0) || (diff_x > 70.0) ) return;

    _rotx = diff_x;
    recompute_tpos();
}



void CFreeGLCamera::update_pos()
{
}

void CFreeGLCamera::set_view()
{
    update_pos();

    gluLookAt(	_tx,	_ty,	 _tz,
                _cx,	_cy,	 _cz,
                _ux,	_uy,	 _uz);
}

