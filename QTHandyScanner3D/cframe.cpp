#include "cframe.h"

#include <set>
#include <iostream>
#include <ctime>

#include "./sfmToyLib/FindCameraMatrices.h"
#include "./settings/psettings.h"
#include "./libelasomp/image.h"
#include "./libelasomp/elas.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include "normals/normal_utils.h"
#include "carray2d.h"

std::vector<CFrame *> global_keyframes_sequence;
//std::map<CFrame *, CFrame *> global_keyframes_sequence_corr;

int global_keyframes_for_processing = 0;
bool global_no_more_keyframes = false;

CFrame::CFrame(int id) : _id(id), _valid_coord(false), _merged_data(NULL)
{
    _global_pos = (cv::Mat_<double>(3,1) << 0,0,0);
    _relat_pos  = (cv::Mat_<double>(3,1) << 0,0,0);
}

CFrame::~CFrame()
{
}

void CFrame::save(std::ofstream & ofile)
{
    // _id
    ofile.write(reinterpret_cast<const char *>(&_id), sizeof(int));

    // _imgL_str
    int imgL_str_len = _imgL_str.length();
    char * imgL_str_buf = _imgL_str.toLocal8Bit().constData();
    ofile.write(reinterpret_cast<const char *>(&imgL_str_len), sizeof(int));
    ofile.write(imgL_str_buf, imgL_str_len);

    // _imgR_str
    int imgR_str_len = _imgR_str.length();
    char * imgR_str_buf = _imgR_str.toLocal8Bit().constData();
    ofile.write(reinterpret_cast<const char *>(&imgR_str_len), sizeof(int));
    ofile.write(imgR_str_buf, imgR_str_len);

    //  cv::Mat _glob_rot_vec;
    double theta_x =  _glob_rot_vec.at<double>(0,0);
    double theta_y =  _glob_rot_vec.at<double>(0,1);
    double theta_z =  _glob_rot_vec.at<double>(0,2);
    ofile.write(reinterpret_cast<const char *>(&theta_x), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&theta_y), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&theta_z), sizeof(double));

    // _glob_rot_alpha;
    ofile.write(reinterpret_cast<const char *>(&_glob_rot_alpha), sizeof(double));

    // _glob_rot_betta;
    ofile.write(reinterpret_cast<const char *>(&_glob_rot_betta), sizeof(double));

    // _global_pos;
    double cam_x = _global_pos.at<double>(0,0);
    double cam_y = _global_pos.at<double>(1,0);
    double cam_z = _global_pos.at<double>(2,0);
    ofile.write(reinterpret_cast<const char *>(&cam_x), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&cam_y), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&cam_z), sizeof(double));

    // _relat_pos
    double rcam_x = _relat_pos.at<double>(0,0);
    double rcam_y = _relat_pos.at<double>(1,0);
    double rcam_z = _relat_pos.at<double>(2,0);
    ofile.write(reinterpret_cast<const char *>(&rcam_x), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&rcam_y), sizeof(double));
    ofile.write(reinterpret_cast<const char *>(&rcam_z), sizeof(double));

    /* we dont store this fields!
    cv::Mat _img_L;
    cv::Mat _img_R;
    cv::Mat _img_disparity;
    cv::Mat _img_3d;
    cv::Mat _descriptors;
    std::vector<cv::KeyPoint> _keypoints;
    std::map< int, std::vector<cv::DMatch>  *> _matches;
    */

    ofile.write(reinterpret_cast<const char *>(&_valid_coord), sizeof(bool));


}

void CFrame::load(std::ifstream & ifile)
{
    // _id
    ifile.read(reinterpret_cast<const char *>(&_id), sizeof(int));

    // _imgL_str
    int imgL_str_len = 0;
    ifile.read(reinterpret_cast<char *>(&imgL_str_len), sizeof(int));
    char * imgL_str_buf = new char [imgL_str_len];
    ifile.read(imgL_str_buf , imgL_str_len);
    _imgL_str = QString::fromLocal8Bit(imgL_str_buf , imgL_str_len );
    delete [] imgL_str_buf;

    // _imgR_str
    int imgR_str_len = 0;
    ifile.read(reinterpret_cast<char *>(&imgR_str_len), sizeof(int));
    char * imgR_str_buf = new char [imgR_str_len];
    ifile.read(imgR_str_buf , imgR_str_len);
    _imgR_str = QString::fromLocal8Bit(imgR_str_buf , imgR_str_len );
    delete [] imgR_str_buf;


    //  cv::Mat _glob_rot_vec;
    double theta_x =  0;
    double theta_y =  0;
    double theta_z =  0;
    ifile.read(reinterpret_cast<const char *>(&theta_x), sizeof(double));
    ifile.read(reinterpret_cast<const char *>(&theta_y), sizeof(double));
    ifile.read(reinterpret_cast<const char *>(&theta_z), sizeof(double));
    _glob_rot_vec = (cv::Mat_<double>(1,3) << theta_x, theta_y, theta_z);

    // _glob_rot_alpha;
    ifile.read(reinterpret_cast<const char *>(&_glob_rot_alpha), sizeof(double));

    // _glob_rot_betta;
    ifile.read(reinterpret_cast<const char *>(&_glob_rot_betta), sizeof(double));

    // _global_pos;
    double cam_x = 0;
    double cam_y = 0;
    double cam_z = 0;
    ifile.read(reinterpret_cast<const char *>(&cam_x), sizeof(double));
    ifile.read(reinterpret_cast<const char *>(&cam_y), sizeof(double));
    ifile.read(reinterpret_cast<const char *>(&cam_z), sizeof(double));
    _global_pos = (cv::Mat_<double>(3,1) << cam_x,cam_y,cam_z);


    // _relat_pos
    double rcam_x = 0;
    double rcam_y = 0;
    double rcam_z = 0;
    ifile.read(reinterpret_cast<const char *>(&rcam_x), sizeof(double));
    ifile.read(reinterpret_cast<const char *>(&rcam_y), sizeof(double));
    ifile.read(reinterpret_cast<const char *>(&rcam_z), sizeof(double));
    _relat_pos  = (cv::Mat_<double>(3,1) << rcam_x,rcam_y,rcam_z);

    ifile.read(reinterpret_cast<const char *>(&_valid_coord), sizeof(bool));

}

void CFrame::adjust_angular_range()
{
    _glob_rot_alpha =  _glob_rot_vec.at<double>(0,0);
    _glob_rot_betta =  _glob_rot_vec.at<double>(0,1);

    if(_glob_rot_alpha < -cPI)
        _glob_rot_alpha += cPI2;
    if(_glob_rot_alpha > cPI)
        _glob_rot_alpha -= cPI2;


    if(_glob_rot_betta < -cPI)
       _glob_rot_betta += cPI2;
    if(_glob_rot_betta > cPI)
        _glob_rot_betta -= cPI2;

   //// std::cout << _glob_rot_alpha << " " << _glob_rot_betta << std::endl;

}

bool CFrame::cmp_angular_dist(const CFrame *f1, const CFrame *f2)
{
    double d1 = f1->_glob_rot_alpha * f1->_glob_rot_alpha + f1->_glob_rot_betta * f1->_glob_rot_betta;
    double d2 = f2->_glob_rot_alpha * f2->_glob_rot_alpha + f2->_glob_rot_betta * f2->_glob_rot_betta;

    return (d1 < d2);
}

double CFrame::get_angular_distance(CFrame * fr)
{
    return std::sqrt(( fr->_glob_rot_alpha - _glob_rot_alpha ) * ( fr->_glob_rot_alpha - _glob_rot_alpha )
            + ( fr->_glob_rot_betta - _glob_rot_betta ) * ( fr->_glob_rot_betta - _glob_rot_betta ));
}

bool CFrame::load_LR_images_from_hd()
{
    load_L_images_from_hd();
    _img_R = cv::imread(_imgR_str.toStdString());
}


bool CFrame::load_tex_from_video_hd()
{
    cv::VideoCapture video(_imgL_str.toLocal8Bit().constData());
    if(!video.isOpened()) return false;



    video.set(CV_CAP_PROP_POS_FRAMES, _id);

    cv::Mat frame;
    video.read(frame);

    if(frame.empty())return false;
    video.release();

    cv::Mat uframe;
    cv::undistort(frame, uframe, g_cam_intrinsics, g_cam_distortion);
    _img_L = uframe;

}

QImage CFrame::get_as_image()
{
    if(_img_L.empty())
       // load_tex_from_video_hd();
        load_L_images_from_hd();

    if(_img_L.empty())
        return QImage();

    QImage img((uchar*)_img_L.data, _img_L.cols, _img_L.rows,  QImage::Format_RGB888);
    return img.rgbSwapped();


}

bool CFrame::load_L_images_from_hd()
{
    if(_imgL_str.length())
        _img_L = cv::imread(_imgL_str.toStdString());
}

bool CFrame::load_left_image_from_hd()
{
    _img_L = cv::imread(_imgL_str.toStdString());
}

bool CFrame::compute_disparity_map()
{
    // convert to grayscale
    cv::Mat imgL_g, imgR_g;
    cv::cvtColor(_img_L, imgL_g, CV_BGR2GRAY);
    cv::cvtColor(_img_R, imgR_g, CV_BGR2GRAY);

    /* cv::StereoSGBM sbm;
    sbm.SADWindowSize =  3;
    sbm.numberOfDisparities = 256;
    sbm.preFilterCap = 63;
    sbm.minDisparity = 0;
    sbm.uniquenessRatio = 10;
    sbm.speckleWindowSize = 100;
    sbm.speckleRange = 32;
    sbm.disp12MaxDiff = 1;
    sbm.fullDP = false;
    sbm.P1 = 8*3*sbm.SADWindowSize*sbm.SADWindowSize; // 216;
    sbm.P2 = 32*3*sbm.SADWindowSize*sbm.SADWindowSize; // 864;
    sbm(_img_L, _img_R, _img_disparity);


    int width  = imgL_g.cols;
    int height = imgL_g.rows;

    cv::Mat fdisparity(height,width,CV_32F);


    for (int y=0; y < height; y++)
        for (int x=0; x < width; x++)
            fdisparity.at<float>(y,x) = float(_img_disparity.at<short>(y,x)) / 16.0;

    _img_disparity = fdisparity;*/

    int width  = imgL_g.cols;
    int height = imgL_g.rows;
    int fbytes = width*height*sizeof(float);

    const int32_t dims[3] = {width, height, width}; // bytes per line = width
    float* L_data = (float*)malloc(fbytes);
    float* R_data = (float*)malloc(fbytes);

    Elas elas(CGlobalHSSettings::_disparity_parameters);
    elas.process(imgL_g.data, imgR_g.data, L_data, R_data, dims);


    cv::Mat resmat = cv::Mat(width*height, 1, CV_32F, L_data).clone();
    _img_disparity = resmat.reshape(0, height);

    free(L_data);
    free(R_data);

    return true;

   /*
    QString filname =QString("disp_float_") +QString::number( qrand()%100) +QString(".raw");
    std::ofstream dispf;
    dispf.open (filname.toStdString().c_str(), std::ios::out| std::ios::binary);
    for (int32_t i=0; i<width*height; i++)
    {
      float val = L_data[i];
      dispf.write((char*)&val,  sizeof(float));
    }
    dispf.close();*/
}

bool CFrame::compute_depth_image()
{
    cv::reprojectImageTo3D(_img_disparity, _img_3d, CGlobalHSSettings::_stereo_Q);
    return true;
}


bool CFrame::compute_keypoints_with_desc(bool filter_keypoints)
{
    CGlobalHSSettings::_feature_detector->detect( _img_L, _keypoints);

    /// filter out keypints which are too far
   if (filter_keypoints)
       filter_out_far_keypoints();

    CGlobalHSSettings::_feature_extractor->compute(_img_L, _keypoints, _descriptors);

    /*
    if(_descriptors.type()!=CV_32F)
    {
        cv::Mat convDesc;
        _descriptors.convertTo(convDesc, CV_32F);
        _descriptors = convDesc;
    }*/
}

void CFrame::udate_global_and_relat_pose(CFrame * prev_frame,
                                         cv::Point3f & realT)
{

    //////////////////////////////////////////////////
    ///// compute raltive pose and update global pose
    /// cv::Mat rvec_glob_L1 = -prev_frame->_glob_rot_vec;
    /// cv::Mat Rglob;
    /// cv::Rodrigues(rvec_glob_L1, Rglob);

    double theta_x =  -prev_frame->_glob_rot_vec.at<double>(0,0);
    double theta_y =  -prev_frame->_glob_rot_vec.at<double>(0,1);
    double theta_z =  -prev_frame->_glob_rot_vec.at<double>(0,2);

    cv::Mat rotX = (cv::Mat_<double>(3,1) << theta_x, 0, 0);
    cv::Mat rotY = (cv::Mat_<double>(3,1) << 0, theta_y, 0);
    cv::Mat rotZ = (cv::Mat_<double>(3,1) << 0, 0, theta_z);

    cv::Mat rotM_X, rotM_Y, rotM_Z;
    cv::Rodrigues(rotX, rotM_X);
    cv::Rodrigues(rotY, rotM_Y);
    cv::Rodrigues(rotZ, rotM_Z);

    cv::Mat Rglob;
    Rglob =  rotM_Y  * rotM_Z * rotM_X   ; /// ORDERING


    cv::Mat camT = (cv::Mat_<double>(3,1) << realT.x, realT.y, realT.z);
    cv::Mat relative_pose = Rglob * camT;
    std::cout << " OFFSET IN GLOBAL COORDS computed " << relative_pose.t() << std::endl;

    //  double rx = relative_pose.at<double>(0,0);
    //  double ry = relative_pose.at<double>(1,0);
    //  double rz = relative_pose.at<double>(2,0);
    //    relative_pose.at<double>(0,0) = 0;
    //  relative_pose.at<double>(1,0) = 0;
    // relative_pose.at<double>(2,0) = 0;

    relative_pose.copyTo(_relat_pos);

    ///// TEMPORAL FIX, WHYYYYY
    ///curr_frame->_relat_pos.at<double>(1,0) = 1.25* curr_frame->_relat_pos.at<double>(1,0);
    std::cout << " OFFSET IN GLOBAL COORDS " << relative_pose.t() << std::endl;
   //  global_frame_pos += relative_pose;
   // global_frame_pos.copyTo(_global_pos);

    _global_pos = prev_frame->_global_pos + relative_pose;
    std::cout << "computed gT: " << _global_pos.t() << std::endl;
}

cv::Point3f CFrame::find_3d_offset_with_homography(CFrame * prev_frame,  double & repro_error)
{
    cv::Point3f offT = find_3d_offset_for_camera_pose(prev_frame);

    int iteration_number = 5;
    cv::Point3f realT = improve_z_offset_iter(iteration_number, offT, prev_frame,  repro_error);
    return realT;
}

cv::Point3f CFrame::improve_z_offset_iter(int iteration_number, cv::Point3f & offT,
                                  CFrame * prev_frame, double & repro_error)
{
    cv::Point3f  imp_3d_offset = offT;

    double factor  = 0.3;
    double resTz_left    = offT.z *(1 - factor);
    double resTz_right   = offT.z *(1 + factor);

    double resTz = offT.z;
    double globTz = offT.z;
    double globErr = 10000;

    cv::Mat forwR = get_pos_angular_difference_as_rotation_matrix(prev_frame);

    double cutoff_min = 0.2;
    double cutoff_max = 0.8;
    for(int i = 0; i < iteration_number; i++)
    {

        cv::Mat camT = (cv::Mat_<double>(3,1) << offT.x, offT.y, resTz);
        cv::Mat poseT = -forwR * camT;

        cv::Mat P2 = (cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2),  poseT.at<double>(0,0) ,
                                              forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2),   poseT.at<double>(1,0) ,
                                              forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2),   poseT.at<double>(2,0));
       double f_xi = compute_average_reproj_error(P2, CGlobalHSSettings::_camL_K, prev_frame, cutoff_min, cutoff_max);
       cv::Mat camTd = (cv::Mat_<double>(3,1) << offT.x, offT.y, resTz_right);

       cv::Mat poseTd = -forwR * camTd;

       cv::Mat P2d = (cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2),  poseTd.at<double>(0,0) ,
                                              forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2),   poseTd.at<double>(1,0) ,
                                              forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2),   poseTd.at<double>(2,0));
        double f_xi_dx = compute_average_reproj_error(P2d, CGlobalHSSettings::_camL_K, prev_frame, cutoff_min, cutoff_max);

        cv::Mat camTdd = (cv::Mat_<double>(3,1) << offT.x, offT.y, resTz_left);
        cv::Mat poseTdd = -forwR * camTdd;
        cv::Mat P2dd = (cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2),  poseTdd.at<double>(0,0) ,
                                              forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2),   poseTdd.at<double>(1,0) ,
                                              forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2),   poseTdd.at<double>(2,0));
        double f_xi_ddx = compute_average_reproj_error(P2dd, CGlobalHSSettings::_camL_K, prev_frame,  cutoff_min, cutoff_max);


        if(f_xi_ddx < f_xi_dx)
        {
            resTz_right = resTz;
            resTz       = (resTz_right + resTz_left)/2;
        }
        else
        {
            resTz_left = resTz;
            resTz       = (resTz_right + resTz_left)/2;
        }

        if(f_xi < globErr)
        {
             globTz = resTz;
             globErr = f_xi;

        }
        if(f_xi_dx < globErr)
        {
            globTz  = resTz_right;
            globErr = f_xi_dx;

        }

        if(f_xi_ddx < globErr)
        {
            globTz  = resTz_left;
            globErr = f_xi_ddx;

        }
    }
    imp_3d_offset.z = globTz;
    repro_error     = globErr;

    return imp_3d_offset;
}


double CFrame::compute_average_reproj_error(cv::Mat & camP, cv::Mat & camK,
                                                       CFrame *f1,  double cutoff_min, double cautoff_max)
{
    std::vector<double> vec_res_err;
    std::vector<cv::DMatch>::iterator gmit = _matches[f1->_id]->begin();
    cv::Mat camK_camP = camK * camP;

    double KP00 = camK_camP.at<double>(0,0);
    double KP01 = camK_camP.at<double>(0,1);
    double KP02 = camK_camP.at<double>(0,2);
    double KP03 = camK_camP.at<double>(0,3);

    double KP10 = camK_camP.at<double>(1,0);
    double KP11 = camK_camP.at<double>(1,1);
    double KP12 = camK_camP.at<double>(1,2);
    double KP13 = camK_camP.at<double>(1,3);


    double KP20 = camK_camP.at<double>(2,0);
    double KP21 = camK_camP.at<double>(2,1);
    double KP22 = camK_camP.at<double>(2,2);
    double KP23 = camK_camP.at<double>(2,3);


    for( ; gmit != _matches[f1->_id]->end(); gmit++)
    {
        /// get 3d point in pcl1
        cv::Point2f p_1L = f1->_keypoints[gmit->queryIdx].pt;
        cv::Point3f  pp =  f1->_img_3d.at<cv::Point3f>(p_1L);
        /// cv::Mat pp_mat = (cv::Mat_<double>(4,1) << pp.x, pp.y, pp.z,1);
        /// cv::Mat uv = camK_camP * pp_mat;
        /// uv  /= uv.at<double>(2,0);

        double uv_x = KP00 * pp.x + KP01 * pp.y + KP02* pp.z + KP03;
        double uv_y = KP10 * pp.x + KP11 * pp.y + KP12* pp.z + KP13;
        double uv_z = KP20 * pp.x + KP21 * pp.y + KP22* pp.z + KP23;
        uv_x /= uv_z; uv_y /= uv_z;

       // cv::Point2f puv;
       // puv.x = uv.at<double>(0,0);
       // puv.y = uv.at<double>(1,0);

        cv::Point2f p_2L = _keypoints[gmit->trainIdx].pt;
        /// cv::Mat p_2L_mat = (cv::Mat_<double>(3,1) << p_2L.x, p_2L.y, 1);
        /// vec_res_err.push_back(cv::norm(p_2L_mat - uv));
        vec_res_err.push_back(std::sqrt((p_2L.x  - uv_x)*(p_2L.x  - uv_x)+ (p_2L.y - uv_y)*(p_2L.y - uv_y)));
    }

    std::sort(vec_res_err.begin(), vec_res_err.end());
    int ersz = vec_res_err.size();
    double ecutoff_min = double(ersz ) * cutoff_min;
    double ecutoff_max = double(ersz ) * cautoff_max;
    double res_error = 0;
    int ecount = 0;
    for(int i = ecutoff_min; i <  ecutoff_max; i++ )
    {
        res_error += vec_res_err[i];
        ecount ++;
    }
    res_error /= double(ecount);

    return res_error;
}

aux_pix_data * CFrame::find_best_neighbour_merged_data(int u, int v)
{
    int xs = _merged_data->_xs;
    int ys = _merged_data->_ys;

    int umin = ((u-1)<  0)? 0    : u-1;
    int umax = ((u+1)>=xs)? xs-1 : u+1;

    int vmin = ((v-1)<  0)? 0    : v-1;
    int vmax = ((v+1)>=ys)? ys-1 : v+1;


    for(int y = vmin; y <= vmax; y++)
        for(int x = umin; x <= umax; x++)
            if(_merged_data->_data[y][x] != NULL)
                return _merged_data->_data[y][x];
    return NULL;

}

bool off_sort_depth(const cv::Point3f & a, const cv::Point3f & b)
{
    return (std::fabs(a.z) < std::fabs(b.z));
}

bool off_sort_x(const cv::Point3f & a, const cv::Point3f & b)
{
    return ( a.x  <  b.x);
}

bool off_sort_y(const cv::Point3f & a, const cv::Point3f & b)
{
    return ( a.y  <  b.y);
}

cv::Point3f CFrame::find_3d_offset_with_merged_data(CFrame * prev_frame)
{
    std::vector<cv::Point3f> offsets;

    int good_count = 0;
    std::vector<cv::DMatch>::iterator gmit = _matches[prev_frame->_id]->begin();
    for( ; gmit != _matches[prev_frame->_id]->end(); gmit ++)
    {
        cv::Point2f p_1L = prev_frame->_keypoints[gmit->queryIdx].pt;
        int p1u = (p_1L.x+0.5);
        int p1v = (p_1L.y+0.5);

        cv::Point2f p_2L   = _keypoints[gmit->trainIdx].pt;
        int p2u = (p_2L.x + 0.5);
        int p2v = (p_2L.y + 0.5);


        aux_pix_data * datp = NULL;
        if(prev_frame->_merged_data->_data[p1v][p1u] != NULL)
            datp = prev_frame->_merged_data->_data[p1v][p1u];
    //     else
    //         datp = prev_frame->find_best_neighbour_merged_data(p1u, p1v);

        aux_pix_data * datc = NULL;
        if((_merged_data->_data[p2v][p2u] != NULL))
           datc = _merged_data->_data[p2v][p2u];
    //     else
     //      datc = find_best_neighbour_merged_data(p2u, p2v);

        if(datc != NULL && datp != NULL)
        {
            good_count ++;

            cv::Point3f off;
            off.x = datp->_x - datc->_x;
            off.y = datp->_y - datc->_y;
            off.z = datp->_z - datc->_z;
            offsets.push_back(off);

            std::cout << off.x << " " << off.y << " " <<  off.z << "::: " <<p1u << " " << p1v << " " << p2u << " " << p2v   << std::endl;
        }

    }

    std::sort (offsets.begin(), offsets.end(), off_sort_depth);
    int pnts_sz = offsets.size();
    int cutoff_max = pnts_sz/2;

    float x_est = 0;
    float y_est = 0;
 std::cout << " ---------- " <<std::endl;
    for(int i = 0, cnt = 0; i <  cutoff_max; i++, cnt++ )
    {

        std::cout << offsets[i].x << " " << offsets[i].y << " " << offsets[i].z <<std::endl;

        x_est += offsets[i].x;
        y_est += offsets[i].y;
    }

    x_est /= cnt;
    y_est /= cnt;

    std::cout << "FINAL: " <<x_est << " " <<  y_est << std::endl;

    cv::Point3f res_3d_offset;
    res_3d_offset.x = x_est;
    res_3d_offset.y = y_est;
    return   res_3d_offset;
}


void CFrame::fill_interpolated_merged_data()
{
    std::cout << "fill_interpolated_merged_data()" << std::endl;
    int xs = _merged_data->_xs;
    int ys = _merged_data->_ys;

    for(int vc = 0; vc < ys; vc++)
    {
       /// std::cout << vc << std::endl;
        int uc = 0;
        while (uc < xs)
        {
            if(_merged_data->_data[vc][uc] != NULL)
            {
                int found_uc_left = uc;

                uc++;
                while (uc < xs)
                {
                    if(_merged_data->_data[vc][uc] != NULL)
                    {

                        int found_uc_right = uc;
                        aux_pix_data * datL = _merged_data->_data[vc][found_uc_left];
                        aux_pix_data * datR = _merged_data->_data[vc][found_uc_right];

                        float lin_dist = found_uc_right - found_uc_left;
                        int middle = (found_uc_left+1 +found_uc_right)/2;
                        for (int x = found_uc_left+1;  x < found_uc_right; x++)
                        {
                           // std::cout << "nval "<< std::endl;
                            ///if(x == middle)
                            {
                                _merged_data->_data[vc][x] = new aux_pix_data;

                                float sc = (x - found_uc_left)/lin_dist;


                                _merged_data->_data[vc][x]->_x = datL->_x + (datR->_x - datL->_x)*sc;
                                _merged_data->_data[vc][x]->_y = datL->_y + (datR->_y - datL->_y)*sc;
                                _merged_data->_data[vc][x]->_z = datL->_z + (datR->_z - datL->_z)*sc;
                            }


                        }
                        uc --;
                        break;
                    }


                    uc++;
                }


            }

            uc++;
        }

    }

    std::cout << "complete interpolation" << std::endl;
}

cv::Point3f CFrame::find_3d_offset_with_merged_data_icp(CFrame * prev_frame, cv::Point3f in)
{
    int xs = _merged_data->_xs;
    int ys = _merged_data->_xs;

    std::vector<cv::Point3f> offsets;

    /// found out correspondance as closest point

    float inx = in.x;
    float iny = in.y;

    #pragma omp parallel for  num_threads(4)
    for(int vc = 0; vc < ys; vc+=5)
    {
        ///std::cout << vc << " " << std::endl;
        for(int uc = 0; uc < xs; uc+=5)
        {
            aux_pix_data * datc = _merged_data->_data[vc][uc];
            if( datc != NULL)
            {
                int count_updates = 0;
                float best_dist = 10e10;
                aux_pix_data * best_mathc_datp = NULL;
                for(int vp = 0; vp < ys; vp++)
                {

                    for(int up = 0; up < xs; up++)
                    {
                        aux_pix_data * datp =prev_frame->_merged_data->_data[vp][up] ;
                        if(datp != NULL)
                        {
                            float dx =  datc->_x - datp->_x + inx;
                            float dy =  datc->_y - datp->_y + iny;
                            float dz = (datc->_z - datp->_z);
                            float ddist = dx*dx + dy*dy;
                          if( (ddist < 1) && (dz*dz < 0.001) )
                           {

                                // compute dist
                                float dist = ddist;

                                 if(dist < best_dist)
                                {
                                    best_mathc_datp = datp;
                                    best_dist = dist;
                                    count_updates++;

                                    up = xs;
                                    vp = ys;
                                }
                            }
                        }
                    }
                }

                if(best_mathc_datp != NULL)
                {
                    cv::Point3f off;
                    off.x = best_mathc_datp->_x - datc->_x;
                    off.y = best_mathc_datp->_y - datc->_y;
                    off.z = best_mathc_datp->_z - datc->_z;
                   #pragma omp critical
                    {
                        offsets.push_back(off);
                    }
                    //std::cout << off.x << " " << off.y << " " << off.y << " " << count_updates << std::endl;
                }

            }
        }
    }

    std::cout << "ICP total inserted poinst are " << offsets.size() << std::endl;
/*
    std::sort (offsets.begin(), offsets.end(), off_sort_depth);
    int curoff_max = offsets.size()/2;
    std::vector<cv::Point3f> icp_after_z;
    for(int i = 0 ; i <  curoff_max; i++  )
        icp_after_z.push_back(offsets[i]);

    std::sort (icp_after_z.begin(), icp_after_z.end(), off_sort_x);
    int pxsz = icp_after_z.size();
    int cutoff_min = float(pxsz)*0.3;
    int cutoff_max = float(pxsz)*0.7;
    std::vector<cv::Point3f> icp_after_x;
    for(int i = cutoff_min ; i < cutoff_max; i++  )
        icp_after_x.push_back(icp_after_z[i]);


    std::sort (icp_after_x.begin(), icp_after_x.end(), off_sort_y);
    int pysz = icp_after_x.size();
    int cutoffy_min =  float(pysz)*0.3;
    int cutoffy_max = float(pysz)*0.7;
    std::vector<cv::Point3f> icp_after_y;
    for(int i = cutoffy_min ; i < cutoffy_max; i++  )
        icp_after_y.push_back(icp_after_x[i]);*/


    float x_est = 0;
    float y_est = 0;
    for(int i = 0, cnt = 0; i <  offsets/*icp_after_y*/.size(); i++, cnt++ )
    {
        x_est += offsets/*icp_after_y*/[i].x;
        y_est += offsets/*icp_after_y*/[i].y;
    }

    x_est /= cnt;
    y_est /= cnt;

    std::cout << "ICP current result " << x_est << " " <<  y_est << std::endl;

    cv::Point3f res_3d_offset;
    res_3d_offset.x = x_est;
    res_3d_offset.y = y_est;
    return   res_3d_offset;/**/
}


cv::Point3f  CFrame::find_3d_offset_with_merged_data_icp_y(CFrame * prev_frame, cv::Point3f in)
{
    int xs = _merged_data->_xs;
    int ys = _merged_data->_xs;

    std::vector<cv::Point3f> offsets;
    #pragma omp parallel for  num_threads(4)
    for(int vc = 0; vc < ys; vc+=4)
    {
        ///std::cout << vc << " " << std::endl;
        for(int uc = 0; uc < xs; uc+=4)
        {
            aux_pix_data * datc = _merged_data->_data[vc][uc];
            if( datc != NULL)
            {
                int count_updates = 0;
                float best_dist = 10e10;
                aux_pix_data * best_mathc_datp = NULL;
                for(int vp = 0; vp < ys; vp++)
                {

                    for(int up = 0; up < xs; up++)
                    {
                        aux_pix_data * datp =prev_frame->_merged_data->_data[vp][up] ;
                        if(datp != NULL)
                        {
                            if( (std::fabs(datc->_x - datp->_x+ in.x) < 1) && (std::fabs(datc->_z - datp->_z) < 0.01))
                            {

                                // compute dist
                                float dist = /*(datc->_x - datp->_x + in.x)*(datc->_x - datp->_x + in.x) +*/
                                             (datc->_y - datp->_y + in.y)*(datc->_y - datp->_y + in.y);

                                if(dist < best_dist)
                                {
                                    best_mathc_datp = datp;
                                    best_dist = dist;
                                    count_updates++;
                                }
                            }
                        }
                    }
                }

                if(best_mathc_datp != NULL && (count_updates > 3))
                {
                    cv::Point3f off;
                    off.x = best_mathc_datp->_x - datc->_x;
                    off.y = best_mathc_datp->_y - datc->_y;
                    off.z = best_mathc_datp->_z - datc->_z;
                   #pragma omp critical
                    {
                        offsets.push_back(off);
                    }

                    //if(count_updates > 3)
                    //std::cout << off.y << " " << count_updates << std::endl;
                }

            }
        }
    }

    std::sort (offsets.begin(), offsets.end(), off_sort_depth);
    int curoff_max = offsets.size()/2;
    std::vector<cv::Point3f> icp_after_z;
    for(int i = 0 ; i <  curoff_max; i++  )
        icp_after_z.push_back(offsets[i]);

    std::sort (icp_after_z.begin(), icp_after_z.end(), off_sort_x);
    int pxsz = icp_after_z.size();
    int cutoff_min = float(pxsz)*0.2;
    int cutoff_max = float(pxsz)*0.8;
    std::vector<cv::Point3f> icp_after_x;
    for(int i = cutoff_min ; i < cutoff_max; i++  )
        icp_after_x.push_back(icp_after_z[i]);


    std::sort (icp_after_x.begin(), icp_after_x.end(), off_sort_y);
    int pysz = icp_after_x.size();
    int cutoffy_min = float(pysz)*0.2;
    int cutoffy_max = float(pysz)*0.8;
    std::vector<cv::Point3f> icp_after_y;
    for(int i = cutoffy_min ; i < cutoffy_max; i++  )
        icp_after_y.push_back(icp_after_x[i]);

    float y_est = 0;
    for(int i = 0, cnt = 0; i <  icp_after_y.size(); i++, cnt++ )
        y_est += icp_after_y[i].y;

    y_est /= cnt;

    std::cout << "ICP current result " <<  y_est << std::endl;

    cv::Point3f res_3d_offset;
    res_3d_offset.x = in.x;
    res_3d_offset.y = y_est;
    res_3d_offset.z = 0;
    return   res_3d_offset;
}

float CFrame::compute_farest_dist()
{
    int xs = _merged_data->_xs;
    int ys = _merged_data->_ys;

    float ymax =0;
    std::vector<cv::Point3f> points;
    for(int vc = 0; vc < ys; vc++)
    {
        for(int uc = 0; uc < xs; uc++)
        {
            aux_pix_data * datc = _merged_data->_data[vc][uc];
            if( datc != NULL)
                if( datc->_y > ymax ) ymax = datc->_y;
        }
    }

    return ymax;
}

cv::Point3f  CFrame::compute_bb_center_with_RANSAC()
{
    int xs = _merged_data->_xs;
    int ys = _merged_data->_ys;

    std::vector<cv::Point3f> points;
    for(int vc = 0; vc < ys; vc++)
    {
        for(int uc = 0; uc < xs; uc++)
        {
            aux_pix_data * datc = _merged_data->_data[vc][uc];
            if( datc != NULL)
            {
                cv::Point3f pnt;
                pnt.x = datc->_x;
                pnt.y = datc->_y;
                pnt.z = datc->_z;

                points.push_back(pnt);
            }
        }
    }



    std::sort (points.begin(), points.end(), off_sort_depth);
    int pxsz = points.size();
    int cutoff_min = float(pxsz)*0.2;
    int cutoff_max = float(pxsz)*0.8;
    std::vector<cv::Point3f> icp_after_z;
    for(int i = cutoff_min ; i < cutoff_max; i++  )
        icp_after_z.push_back(points[i]);



    std::sort (icp_after_z.begin(), icp_after_z.end(), off_sort_x);
    int pxszz = icp_after_z.size();
    int cutoff_minx = float(pxszz)*0.2;
    int cutoff_maxx = float(pxszz)*0.8;
    std::vector<cv::Point3f> icp_after_x;
    for(int i = cutoff_minx ; i < cutoff_maxx; i++  )
        icp_after_x.push_back(icp_after_z[i]);



    std::sort (icp_after_x.begin(), icp_after_x.end(), off_sort_y);
    int pxszx = icp_after_x.size();
    int cutoff_miny = float(pxszx)*0.2;
    int cutoff_maxy = float(pxszx)*0.8;
    std::vector<cv::Point3f> icp_after_y;
    for(int i = cutoff_miny ; i < cutoff_maxy; i++  )
        icp_after_y.push_back(icp_after_x[i]);


    float x_est = 0;
    float y_est = 0;
    float z_est = 0;

    for(int i = 0, cnt = 0; i <  icp_after_y.size(); i++, cnt++ )
    {
        x_est += icp_after_y[i].x;
        y_est += icp_after_y[i].y;
        z_est += icp_after_y[i].z;

   }
    x_est /= cnt;
    y_est /= cnt;
    z_est /= cnt;

    cv::Point3f res_3d_offset;
    res_3d_offset.x = x_est;
    res_3d_offset.y = y_est;
    res_3d_offset.z = z_est;
    return   res_3d_offset;

}

cv::Point3f CFrame::find_2d_offset_with_correlation(CFrame * prev_frame)
{

    //// 1. step 3d find bounding box for both point sets
    cv::Point3f c_bb_min;
    c_bb_min.x = 10e13;
    c_bb_min.y = 10e13;
    c_bb_min.z = 10e13;

    cv::Point3f c_bb_max;
    c_bb_max.x = -10e13;
    c_bb_max.y = -10e13;
    c_bb_max.z = -10e13;

    cv::Point3f p_bb_min;
    p_bb_min.x = 10e13;
    p_bb_min.y = 10e13;
    p_bb_min.z = 10e13;

    cv::Point3f p_bb_max;
    p_bb_max.x = 0;
    p_bb_max.y = 0;
    p_bb_max.z = 0;

    int xs = _merged_data->_xs;
    int ys = _merged_data->_ys;
    for(int vc = 0; vc < ys; vc++)
    {
        for(int uc = 0; uc < xs; uc++)
        {
            aux_pix_data * datc = _merged_data->_data[vc][uc];
            if( datc != NULL)
            {
                if(datc->_x > c_bb_max.x) c_bb_max.x = datc->_x;
                if(datc->_x < c_bb_min.x) c_bb_min.x = datc->_x;
                if(datc->_y > c_bb_max.y) c_bb_max.y = datc->_y;
                if(datc->_y < c_bb_min.y) c_bb_min.y = datc->_y;
                if(datc->_z > c_bb_max.z) c_bb_max.z = datc->_z;
                if(datc->_z < c_bb_min.z) c_bb_min.z = datc->_z;
            }


            aux_pix_data * datp = prev_frame->_merged_data->_data[vc][uc];
            if( datp != NULL)
            {
                if(datp->_x > p_bb_max.x) p_bb_max.x = datp->_x;
                if(datp->_x < p_bb_min.x) p_bb_min.x = datp->_x;
                if(datp->_y > p_bb_max.y) p_bb_max.y = datp->_y;
                if(datp->_y < p_bb_min.y) p_bb_min.y = datp->_y;
                if(datp->_z > p_bb_max.z) p_bb_max.z = datp->_z;
                if(datp->_z < p_bb_min.z) p_bb_min.z = datp->_z;
            }
        }
    }
    std::cout << "curframe bb: " << c_bb_min << " " << c_bb_max << std::endl;
    std::cout << "preframe bb: " << p_bb_min << " " << p_bb_max << std::endl;


    float fwidth  = ((p_bb_max.x -  p_bb_min.x) > (c_bb_max.x - c_bb_min.x))?
                     (p_bb_max.x -  p_bb_min.x) : (c_bb_max.x - c_bb_min.x);

    cv::Point3f tot_bb_max;
    tot_bb_max.x = (p_bb_max.x > c_bb_max.x )? p_bb_max.x : c_bb_max.x ;
    tot_bb_max.y = (p_bb_max.y > c_bb_max.y )? p_bb_max.y : c_bb_max.y ;
    tot_bb_max.z = (p_bb_max.z > c_bb_max.z )? p_bb_max.z : c_bb_max.z ;

    cv::Point3f tot_bb_min;
    tot_bb_min.x = (p_bb_min.x < c_bb_min.x )? p_bb_min.x : c_bb_min.x ;
    tot_bb_min.y = (p_bb_min.y < c_bb_min.y )? p_bb_min.y : c_bb_min.y ;
    tot_bb_min.z = (p_bb_min.z < c_bb_min.z )? p_bb_min.z : c_bb_min.z ;

    float fheight = tot_bb_max.z -  tot_bb_min.z;


    std::cout << "float width / height : " << fwidth << " " << fheight<< std::endl;

    std::cout << "resulting bb: " << tot_bb_min << " " << tot_bb_max << std::endl;

    int rheight = 1000;
    int rwidth = float(rheight) *  fwidth / fheight + 1;

    std::cout << "resulting width / height: " <<rwidth << " " << rheight << std::endl;

    float conflicted  = -5;
    float not_visited = -1;
    CArray2D<float> cmap(rwidth, rheight, not_visited );
    CArray2D<float> pmap(rwidth, rheight, not_visited );
    CArray2D<float> cmap_x(rwidth, rheight, not_visited );
    CArray2D<float> pmap_x(rwidth, rheight, not_visited );

    for(int vc = 0; vc < ys; vc++)
    {
        for(int uc = 0; uc < xs; uc++)
        {
            aux_pix_data * datc = _merged_data->_data[vc][uc];
            if( datc != NULL)
            {

                int cmap_idx_x = (datc->_x - c_bb_min.x )  * float(rwidth) / fwidth ;
                int cmap_idx_y = (datc->_z - tot_bb_min.z) * float(rheight)/ fheight ;

                if(cmap._data[cmap_idx_y][cmap_idx_x] != conflicted)
                {
                    if (cmap._data[cmap_idx_y][cmap_idx_x] == not_visited )
                    {
                       cmap._data[cmap_idx_y][cmap_idx_x]   = datc->_y ;
                       cmap_x._data[cmap_idx_y][cmap_idx_x] = datc->_x ;

                    }
                    else
                    {
                        cmap._data[cmap_idx_y][cmap_idx_x] = conflicted;
                    }
                }
            }


            aux_pix_data * datp = prev_frame->_merged_data->_data[vc][uc];
            if( datp != NULL)
            {

                int pmap_idx_x = (datp->_x - p_bb_min.x )  * float(rwidth) / fwidth ;
                int pmap_idx_y = (datp->_z - tot_bb_min.z) * float(rheight)/ fheight ;

                if(pmap._data[pmap_idx_y][pmap_idx_x] != conflicted)
                {
                    if (pmap._data[pmap_idx_y][pmap_idx_x] == not_visited )
                    {
                       pmap._data[pmap_idx_y][pmap_idx_x]   = datp->_y ;
                       pmap_x._data[pmap_idx_y][pmap_idx_x] = datp->_x ;
                    }
                    else
                    {
                        pmap._data[pmap_idx_y][pmap_idx_x] = conflicted;
                    }
                }
            }
        }
    }

    /*
    std::string cf("____cmap.raw");
    cmap.dump(cf);
    std::string ff("____pmap.raw");
    pmap.dump(ff);

    std::string cfx("____cmap_x.raw");
    cmap_x.dump(cfx);
    std::string ffx("____pmap_x.raw");
    pmap_x.dump(ffx);
    */

    float iny = 86.5;
    float best_count = 0;
    int best_offset = 0;

    for (int offs = -rwidth/2; offs < rwidth/2; offs++)
    {
        /// float current_correlation = 0;

        float current_x_offset = fwidth * float(offs)/float(rwidth) + ( p_bb_min.x -c_bb_min.x);


        //std::cout << current_x_offset << std::endl;

        int good_count = 0;
        for(int v = 0; v < rheight; v++)
        {
            for(int u = 0; u < rwidth; u++)
            {
                if(((u-offs) < rwidth) && ((u-offs) >= 0))
                  if(pmap._data[v][u] > 0)
                        if (cmap._data[v][u-offs]> 0)
                        {
                            //// current_correlation += pmap._data[v][u] * cmap._data[v][u-offs];

                            float dx =  cmap_x._data[v][u-offs] - pmap_x._data[v][u] + current_x_offset;
                            float dy =  cmap._data[v][u-offs] - pmap._data[v][u] + iny;

                            ////std::cout << dx << " " << dy << std::endl;

                            float ddist = dx*dx + dy*dy;

                            if(ddist< 1)
                               good_count++;


                        }
            }
        }

        if(best_count < good_count)
        {
            best_count = good_count;
            best_offset = offs;
        }
       ////std::cout << offs << " " << good_count << std::endl;

    }

   /*
    CArray2D<float> over_map(rwidth, rheight, conflicted );
    for(int v = 0; v < rheight; v++)
    {
        for(int u = 0; u < rwidth; u++)
        {
           over_map._data[v][u]  = pmap._data[v][u] ;

           if(((u-best_offset) < rwidth) && ((u-best_offset) >= 0))
                if(over_map._data[v][u] <  cmap._data[v][u-best_offset])
                    over_map._data[v][u] =  cmap._data[v][u-best_offset];
        }
    }
    std::string of("____over_map.raw");
    over_map.dump(of);
*/

    float phys_offset_inside  = fwidth * float(best_offset)/float(rwidth);
    float phys_offset_outside = ( p_bb_min.x -c_bb_min.x) ;


    std::cout << "done with correlation, offset " << best_offset << " " << best_count << std::endl;
    std::cout << "found physical offset " << phys_offset_inside + phys_offset_outside << std::endl;

    cv::Point3f res_3d_offset;
    res_3d_offset.x = phys_offset_inside + phys_offset_outside;
    res_3d_offset.y = 0;
    res_3d_offset.z = 0;
    return   res_3d_offset;

}


cv::Point3f CFrame::find_2d_offset_with_correlation_init( CFrame * prev_frame,
        CArray2D<float> * & cmap,
        CArray2D<float> * & pmap,
        CArray2D<float> * & cmap_x,
        CArray2D<float> * & pmap_x,
        float & fwidth,
        int & rwidth, int & rheight,  float & p_c_bb_min_x_diff
        )
{
    cv::Point3f c_bb_min;
    c_bb_min.x = 10e13;
    c_bb_min.y = 10e13;
    c_bb_min.z = 10e13;

    cv::Point3f c_bb_max;
    c_bb_max.x = -10e13;
    c_bb_max.y = -10e13;
    c_bb_max.z = -10e13;

    cv::Point3f p_bb_min;
    p_bb_min.x = 10e13;
    p_bb_min.y = 10e13;
    p_bb_min.z = 10e13;

    cv::Point3f p_bb_max;
    p_bb_max.x = 0;
    p_bb_max.y = 0;
    p_bb_max.z = 0;

    int xs = _merged_data->_xs;
    int ys = _merged_data->_ys;
    for(int vc = 0; vc < ys; vc++)
    {
        for(int uc = 0; uc < xs; uc++)
        {
            aux_pix_data * datc = _merged_data->_data[vc][uc];
            if( datc != NULL)
            {
                if(datc->_x > c_bb_max.x) c_bb_max.x = datc->_x;
                if(datc->_x < c_bb_min.x) c_bb_min.x = datc->_x;
                if(datc->_y > c_bb_max.y) c_bb_max.y = datc->_y;
                if(datc->_y < c_bb_min.y) c_bb_min.y = datc->_y;
                if(datc->_z > c_bb_max.z) c_bb_max.z = datc->_z;
                if(datc->_z < c_bb_min.z) c_bb_min.z = datc->_z;
            }


            aux_pix_data * datp = prev_frame->_merged_data->_data[vc][uc];
            if( datp != NULL)
            {
                if(datp->_x > p_bb_max.x) p_bb_max.x = datp->_x;
                if(datp->_x < p_bb_min.x) p_bb_min.x = datp->_x;
                if(datp->_y > p_bb_max.y) p_bb_max.y = datp->_y;
                if(datp->_y < p_bb_min.y) p_bb_min.y = datp->_y;
                if(datp->_z > p_bb_max.z) p_bb_max.z = datp->_z;
                if(datp->_z < p_bb_min.z) p_bb_min.z = datp->_z;
            }
        }
    }
    std::cout << "curframe bb: " << c_bb_min << " " << c_bb_max << std::endl;
    std::cout << "preframe bb: " << p_bb_min << " " << p_bb_max << std::endl;


    fwidth  = ((p_bb_max.x -  p_bb_min.x) > (c_bb_max.x - c_bb_min.x))?
                     (p_bb_max.x -  p_bb_min.x) : (c_bb_max.x - c_bb_min.x);

    cv::Point3f tot_bb_max;
    tot_bb_max.x = (p_bb_max.x > c_bb_max.x )? p_bb_max.x : c_bb_max.x ;
    tot_bb_max.y = (p_bb_max.y > c_bb_max.y )? p_bb_max.y : c_bb_max.y ;
    tot_bb_max.z = (p_bb_max.z > c_bb_max.z )? p_bb_max.z : c_bb_max.z ;

    cv::Point3f tot_bb_min;
    tot_bb_min.x = (p_bb_min.x < c_bb_min.x )? p_bb_min.x : c_bb_min.x ;
    tot_bb_min.y = (p_bb_min.y < c_bb_min.y )? p_bb_min.y : c_bb_min.y ;
    tot_bb_min.z = (p_bb_min.z < c_bb_min.z )? p_bb_min.z : c_bb_min.z ;

    float fheight = tot_bb_max.z -  tot_bb_min.z;


    std::cout << "float width / height : " << fwidth << " " << fheight<< std::endl;

    std::cout << "resulting bb: " << tot_bb_min << " " << tot_bb_max << std::endl;

    rheight = 1000;
    rwidth = float(rheight) *  fwidth / fheight + 1;

    p_c_bb_min_x_diff = p_bb_min.x  - c_bb_min.x;

    std::cout << "resulting width / height: " <<rwidth << " " << rheight << std::endl;

    float conflicted  = -5;
    float not_visited = -1;
    cmap = new CArray2D<float> (rwidth, rheight, not_visited );
    pmap = new CArray2D<float> (rwidth, rheight, not_visited );
    cmap_x = new CArray2D<float> (rwidth, rheight, not_visited );
    pmap_x = new CArray2D<float> (rwidth, rheight, not_visited );

    for(int vc = 0; vc < ys; vc++)
    {
        for(int uc = 0; uc < xs; uc++)
        {
            aux_pix_data * datc = _merged_data->_data[vc][uc];
            if( datc != NULL)
            {

                int cmap_idx_x = (datc->_x - c_bb_min.x )  * float(rwidth) / fwidth ;
                int cmap_idx_y = (datc->_z - tot_bb_min.z) * float(rheight)/ fheight ;

                if( (cmap_idx_x >= 0) && (cmap_idx_x < rwidth) && (cmap_idx_y>= 0) && (cmap_idx_y < rheight))
                {
                    if(cmap->_data[cmap_idx_y][cmap_idx_x] != conflicted)
                    {
                        if (cmap->_data[cmap_idx_y][cmap_idx_x] == not_visited )
                        {
                           cmap->_data[cmap_idx_y][cmap_idx_x]   = datc->_y ;
                           cmap_x->_data[cmap_idx_y][cmap_idx_x] = datc->_x ;

                        }
                        else
                        {
                            cmap->_data[cmap_idx_y][cmap_idx_x] = conflicted;
                        }
                    }
                }
            }


            aux_pix_data * datp = prev_frame->_merged_data->_data[vc][uc];
            if( datp != NULL)
            {

                int pmap_idx_x = (datp->_x - p_bb_min.x )  * float(rwidth) / fwidth ;
                int pmap_idx_y = (datp->_z - tot_bb_min.z) * float(rheight)/ fheight ;

                if((pmap_idx_x >= 0) && (pmap_idx_x < rwidth) && (pmap_idx_y>= 0) && (pmap_idx_y < rheight))
                {
                    if(pmap->_data[pmap_idx_y][pmap_idx_x] != conflicted)
                    {
                        if (pmap->_data[pmap_idx_y][pmap_idx_x] == not_visited )
                        {
                           pmap->_data[pmap_idx_y][pmap_idx_x]   = datp->_y ;
                           pmap_x->_data[pmap_idx_y][pmap_idx_x] = datp->_x ;
                        }
                        else
                        {
                            pmap->_data[pmap_idx_y][pmap_idx_x] = conflicted;
                        }
                    }
                }
            }
        }
    }
}

cv::Point3f CFrame::find_2d_offset_with_correlation_step(
        CArray2D<float> * cmap,
        CArray2D<float> * pmap,
        CArray2D<float> * cmap_x,
        CArray2D<float> * pmap_x,
        float fwidth,
        int rwidth, int rheight,  float iny, float p_c_bb_min_x_diff, int & best_count)
{

    best_count = 0;
    int best_offset = 0;


    for (int offs = -rwidth/2; offs < rwidth/2; offs++)
    {
        float current_x_offset = fwidth * float(offs)/ float(rwidth) + p_c_bb_min_x_diff;

        int good_count = 0;

        #pragma omp parallel for num_threads(CGlobalHSSettings::_computing_threads)
        for(int v = 0; v < rheight; v+=2) ////// BE CAREFUL
        {
            for(int u = 0; u < rwidth; u+=2) ////// BE CAREFUL
            {

                if(pmap->_data[v][u] > 0)
                {
                    int u_offs =(u-offs);
                    if((u_offs< rwidth) && (u_offs>= 0))

                        if (cmap->_data[v][u_offs]> 0)
                        {
                            float dx =  cmap_x->_data[v][u_offs] - pmap_x->_data[v][u] + current_x_offset;
                            float dy =  cmap->_data[v][u_offs] - pmap->_data[v][u] + iny;

                            if((dx*dx + dy*dy)< 0.25f)
                            {
                               #pragma omp critical
                                {
                                    good_count++;
                                }
                            }
                        }
                }
            }
        }

        if(best_count < good_count)
        {
                best_count = good_count;
                best_offset = offs;
        }

    }

   //std::cout << "done with correlation, offset " << best_offset << " " << best_count << std::endl;
    //std::cout << "found physical offset " << phys_offset_inside + phys_offset_outside << std::endl;

    cv::Point3f res_3d_offset;
    res_3d_offset.x = fwidth * float(best_offset)/ float(rwidth) + p_c_bb_min_x_diff;
    res_3d_offset.y = 0;
    res_3d_offset.z = 0;
    return   res_3d_offset;
}


/*
cv::Point3f CFrame::find_2d_offset_with_correlation(CFrame * prev_frame)
{

    //// 1. step 3d find bounding box for both point sets
    cv::Point3f c_bb_min;
    c_bb_min.x = 10e13;
    c_bb_min.y = 10e13;
    c_bb_min.z = 10e13;

    cv::Point3f c_bb_max;
    c_bb_max.x = -10e13;
    c_bb_max.y = -10e13;
    c_bb_max.z = -10e13;

    cv::Point3f p_bb_min;
    p_bb_min.x = 10e13;
    p_bb_min.y = 10e13;
    p_bb_min.z = 10e13;

    cv::Point3f p_bb_max;
    p_bb_max.x = 0;
    p_bb_max.y = 0;
    p_bb_max.z = 0;

    int xs = _merged_data->_xs;
    int ys = _merged_data->_ys;
    for(int vc = 0; vc < ys; vc++)
    {
        for(int uc = 0; uc < xs; uc++)
        {
            aux_pix_data * datc = _merged_data->_data[vc][uc];
            if( datc != NULL)
            {
                if(datc->_x > c_bb_max.x) c_bb_max.x = datc->_x;
                if(datc->_x < c_bb_min.x) c_bb_min.x = datc->_x;
                if(datc->_y > c_bb_max.y) c_bb_max.y = datc->_y;
                if(datc->_y < c_bb_min.y) c_bb_min.y = datc->_y;
                if(datc->_z > c_bb_max.z) c_bb_max.z = datc->_z;
                if(datc->_z < c_bb_min.z) c_bb_min.z = datc->_z;
            }


            aux_pix_data * datp = prev_frame->_merged_data->_data[vc][uc];
            if( datp != NULL)
            {
                if(datp->_x > p_bb_max.x) p_bb_max.x = datp->_x;
                if(datp->_x < p_bb_min.x) p_bb_min.x = datp->_x;
                if(datp->_y > p_bb_max.y) p_bb_max.y = datp->_y;
                if(datp->_y < p_bb_min.y) p_bb_min.y = datp->_y;
                if(datp->_z > p_bb_max.z) p_bb_max.z = datp->_z;
                if(datp->_z < p_bb_min.z) p_bb_min.z = datp->_z;
            }
        }
    }
    std::cout << "curframe bb: " << c_bb_min << " " << c_bb_max << std::endl;
    std::cout << "preframe bb: " << p_bb_min << " " << p_bb_max << std::endl;


    float fwidth  = ((p_bb_max.x -  p_bb_min.x) > (c_bb_max.x - c_bb_min.x))?
                     (p_bb_max.x -  p_bb_min.x) : (c_bb_max.x - c_bb_min.x);

    cv::Point3f tot_bb_max;
    tot_bb_max.x = (p_bb_max.x > c_bb_max.x )? p_bb_max.x : c_bb_max.x ;
    tot_bb_max.y = (p_bb_max.y > c_bb_max.y )? p_bb_max.y : c_bb_max.y ;
    tot_bb_max.z = (p_bb_max.z > c_bb_max.z )? p_bb_max.z : c_bb_max.z ;

    cv::Point3f tot_bb_min;
    tot_bb_min.x = (p_bb_min.x < c_bb_min.x )? p_bb_min.x : c_bb_min.x ;
    tot_bb_min.y = (p_bb_min.y < c_bb_min.y )? p_bb_min.y : c_bb_min.y ;
    tot_bb_min.z = (p_bb_min.z < c_bb_min.z )? p_bb_min.z : c_bb_min.z ;

    float fheight = tot_bb_max.z -  tot_bb_min.z;


    std::cout << "float width / height : " << fwidth << " " << fheight<< std::endl;

    std::cout << "resulting bb: " << tot_bb_min << " " << tot_bb_max << std::endl;

    int rheight = 1000;
    int rwidth = float(rheight) *  fwidth / fheight + 1;

    std::cout << "resulting width / height: " <<rwidth << " " << rheight << std::endl;

    float conflicted  = -5;
    float not_visited = -1;
    CArray2D<float> cmap(rwidth, rheight, not_visited );
    CArray2D<float> pmap(rwidth, rheight, not_visited );
    CArray2D<float> cmap_x(rwidth, rheight, not_visited );
    CArray2D<float> pmap_x(rwidth, rheight, not_visited );

    for(int vc = 0; vc < ys; vc++)
    {
        for(int uc = 0; uc < xs; uc++)
        {
            aux_pix_data * datc = _merged_data->_data[vc][uc];
            if( datc != NULL)
            {

                int cmap_idx_x = (datc->_x - c_bb_min.x )  * float(rwidth) / fwidth ;
                int cmap_idx_y = (datc->_z - tot_bb_min.z) * float(rheight)/ fheight ;

                if(cmap._data[cmap_idx_y][cmap_idx_x] != conflicted)
                {
                    if (cmap._data[cmap_idx_y][cmap_idx_x] == not_visited )
                    {
                       cmap._data[cmap_idx_y][cmap_idx_x] = datc->_y - c_bb_min.y;
                    }
                    else
                    {
                        cmap._data[cmap_idx_y][cmap_idx_x] = conflicted;
                    }
                }
            }


            aux_pix_data * datp = prev_frame->_merged_data->_data[vc][uc];
            if( datp != NULL)
            {

                int pmap_idx_x = (datp->_x - p_bb_min.x )  * float(rwidth) / fwidth ;
                int pmap_idx_y = (datp->_z - tot_bb_min.z) * float(rheight)/ fheight ;

                if(pmap._data[pmap_idx_y][pmap_idx_x] != conflicted)
                {
                    if (pmap._data[pmap_idx_y][pmap_idx_x] == not_visited )
                    {
                       pmap._data[pmap_idx_y][pmap_idx_x] = datp->_y - p_bb_min.y;
                    }
                    else
                    {
                        pmap._data[pmap_idx_y][pmap_idx_x] = conflicted;
                    }
                }
            }
        }
    }

    ////std::string cf("____cmap.raw");
    ////cmap.dump(cf);
    ////std::string ff("____pmap.raw");
    ////pmap.dump(ff);

    float best_correlation = 0;
    int best_offset = 0;
    for (int offs = -rwidth/2; offs < rwidth/2; offs++)
    {
        float current_correlation = 0;

        int good_count = 0;
        for(int v = 0; v < rheight; v++)
        {
            for(int u = 0; u < rwidth; u++)
            {
                if(((u-offs) < rwidth) && ((u-offs) >= 0))
                  if(pmap._data[v][u] > 0)
                        if (cmap._data[v][u-offs]> 0)
                        {
                            current_correlation += pmap._data[v][u] * cmap._data[v][u-offs];
                            good_count++;
                        }
            }
        }

        if(best_correlation < current_correlation)
        {
            best_correlation = current_correlation;
            best_offset = offs;
        }
        ///std::cout << offs << " " << current_correlation << " " << good_count << std::endl;

    }

   // best_offset = 165;
    CArray2D<float> over_map(rwidth, rheight, conflicted );
    for(int v = 0; v < rheight; v++)
    {
        for(int u = 0; u < rwidth; u++)
        {
           over_map._data[v][u]  = pmap._data[v][u] ;

           if(((u-best_offset) < rwidth) && ((u-best_offset) >= 0))
                if(over_map._data[v][u] <  cmap._data[v][u-best_offset])
                    over_map._data[v][u] =  cmap._data[v][u-best_offset];
        }
    }
    std::string of("____over_map.raw");
    over_map.dump(of);


    float phys_offset_inside  = fwidth * float(best_offset)/float(rwidth);
    float phys_offset_outside = ( p_bb_min.x -c_bb_min.x) ;


    std::cout << "done with correlation, offset " << best_offset << std::endl;
    std::cout << "found physical offset " << phys_offset_inside + phys_offset_outside << std::endl;

    cv::Point3f res_3d_offset;
    res_3d_offset.x = phys_offset_inside + phys_offset_outside;
    res_3d_offset.y = 0;
    res_3d_offset.z = 0;
    return   res_3d_offset;

}*/

cv::Point3f  CFrame::find_3d_offset_with_merged_data_icp_x(CFrame * prev_frame, cv::Point3f in)
{
    int xs = _merged_data->_xs;
    int ys = _merged_data->_xs;

    std::vector<cv::Point3f> offsets;
   #pragma omp parallel for  num_threads(4)

    for(int vc = 0; vc < ys; vc+=4)
    {
        //int uc = 525;
        for(int uc = 0; uc < xs; uc+=4)
        {
            aux_pix_data * datc = _merged_data->_data[vc][uc];
            if( datc != NULL)
            {
                float best_dist = 10e10;
                int count_updates = 0;
                aux_pix_data * best_mathc_datp = NULL;
                for(int vp = 0; vp < ys; vp++)
                {

                    for(int up = 0; up < xs; up++)
                    {
                        aux_pix_data * datp =prev_frame->_merged_data->_data[vp][up] ;
                        if(datp != NULL)
                        {
                            if( ((datc->_y - datp->_y+ in.y)*(datc->_y - datp->_y+/*+210*/ in.y) < 1) && ((datc->_z - datp->_z)* (datc->_z - datp->_z) < 0.001))
                            {

                                // compute dist
                                float dist = (datc->_x - datp->_x + in.x)*(datc->_x - datp->_x + in.x)/* +
                                             (datc->_y - datp->_y + in.y)*(datc->_y - datp->_y + in.y)*/;

                                if(dist < best_dist)
                                {
                                    best_mathc_datp = datp;
                                    best_dist = dist;
                                    count_updates++;

                                }
                            }
                        }
                    }
                }

                if((best_mathc_datp != NULL) &&(count_updates>3 ))
                {
                   // std::cout << uc << " " << vc << "<-->" << best_mathc_datp->_x - datc->_x << " " << best_mathc_datp->_y - datc->_y << std::endl;
                    cv::Point3f off;
                    off.x = best_mathc_datp->_x - datc->_x;
                    off.y = best_mathc_datp->_y - datc->_y;
                    off.z = best_mathc_datp->_z - datc->_z;
                   #pragma omp critical
                    {
                        offsets.push_back(off);
                    }

                    //if(count_updates > 3)
                   // std::cout << off.x << " " << count_updates << std::endl;
                }

            }
        }
    }
    std::cout << "ICP   " << offsets.size() << std::endl;

    std::sort (offsets.begin(), offsets.end(), off_sort_depth);
    int curoff_max = offsets.size()/2;
    std::vector<cv::Point3f> icp_after_z;
    for(int i = 0 ; i <  curoff_max; i++  )
        icp_after_z.push_back(offsets[i]);

    std::cout << "ICP   " << icp_after_z.size() << std::endl;

    std::sort (icp_after_z.begin(), icp_after_z.end(), off_sort_x);
    int pxsz = icp_after_z.size();
    int cutoff_min = float(pxsz)*0.2;
    int cutoff_max = float(pxsz)*0.8;
    std::vector<cv::Point3f> icp_after_x;
    for(int i = cutoff_min ; i < cutoff_max; i++  )
        icp_after_x.push_back(icp_after_z[i]);

    std::cout << "ICP   " << icp_after_x.size() << std::endl;

    std::sort (icp_after_x.begin(), icp_after_x.end(), off_sort_y);
    int pysz = icp_after_x.size();
    int cutoffy_min = float(pysz)*0.2;
    int cutoffy_max = float(pysz)*0.8;
    std::vector<cv::Point3f> icp_after_y;
    for(int i = cutoffy_min ; i < cutoffy_max; i++  )
        icp_after_y.push_back(icp_after_x[i]);
    std::cout << "ICP   " << icp_after_y.size() << std::endl;


    float x_est = 0;
    //float y_est = 0;
    for(int i = 0, cnt = 0; i <  icp_after_y.size(); i++, cnt++ )
    {
        x_est += icp_after_y[i].x;
    ///    y_est += icp_after_y[i].y;
    }

    x_est /= cnt;
    ///y_est /= cnt;

    std::cout << "ICP current result " << x_est  << std::endl;

    cv::Point3f res_3d_offset;
    res_3d_offset.x = x_est;
    res_3d_offset.y = in.y;
    res_3d_offset.z = 0;
    return   res_3d_offset;

  /*  cv::Point3f res_3d_offset;
    return   res_3d_offset;*/
}


cv::Point3f CFrame::find_3d_offset_for_camera_pose(CFrame * prev_frame)
{
    cv::Mat backR = get_neg_angular_difference_as_rotation_matrix(prev_frame) ;

    std::vector<cv::Point2f> proj3d_1L;
    std::vector<cv::Point2f> proj3d_2L;

    std::vector<cv::DMatch>::iterator gmit = this->_matches[prev_frame->_id]->begin();
    std::vector<double> vecTz;
    for( ; gmit != this->_matches[prev_frame->_id]->end(); gmit ++)
    {
        /// get 3d point in pcl1
        cv::Point2f p_1L = prev_frame->_keypoints[gmit->queryIdx].pt;
        cv::Point3f  p3_1L = (prev_frame->_img_3d.at<cv::Point3f>(p_1L));

        cv::Point2f pp1L;
        pp1L.x = p3_1L.x;
        pp1L.y = p3_1L.y;
        proj3d_1L.push_back(pp1L);

        /// unrotate 3d point in pcl2
        cv::Point2f p_2L   = _keypoints[gmit->trainIdx].pt;
        cv::Point3f p3_2L = (_img_3d.at<cv::Point3f>(p_2L));
        cv::Mat p3d_2L = (cv::Mat_<double>(3,1) << p3_2L.x, p3_2L.y, p3_2L.z );
        cv::Mat p3d_2L_rot = backR * p3d_2L;

        cv::Point2f pp2L;
        pp2L.x = p3d_2L_rot.at<double>(0,0);
        pp2L.y = p3d_2L_rot.at<double>(1,0);
        proj3d_2L.push_back(pp2L);

        double dz = p3_1L.z - p3d_2L_rot.at<double>(2,0);
        vecTz.push_back(dz);
        // std::cout << "homo: " << pp1L << " " <<  pp2L  << std::endl;
    }

    std::sort(vecTz.begin(), vecTz.end());
    int vsz = vecTz.size();
    double cutoff_min = double(vsz) * 0.2;
    double cutoff_max = double(vsz) * 0.8;
    double initTz = 0;
    int vcount = 0;
    for(int i = cutoff_min; i <  cutoff_max; i++ )
    {
        initTz += vecTz[i];
        vcount ++;
    }
    initTz /= vcount;

    ///cv::Mat H = cv::findHomography(proj3d_2L, proj3d_1L, CV_LMEDS);
    cv::Mat H = cv::findHomography(proj3d_2L, proj3d_1L,  CV_RANSAC);

    ///std::cout << "homography matrix " << H << std::endl;
    // POSSIBLE PATCH - check both methods and select the best one

    double a = H.at<double>(0,0);
    double b = H.at<double>(0,1);
    double c = H.at<double>(0,2);
    double d = H.at<double>(1,0);
    double e = H.at<double>(1,1);
    double f = H.at<double>(1,2);

    double p = sqrt(a*a + b*b);
    double r = (a*e - b*d)/p;
    double q = (a*d+b*e)/(a*e - b *d);

    double theta = atan2(b,a);

    std::cout << "2: translation " << c << " " << f << std::endl;
    std::cout << "2: scale " << p << " " << r << std::endl;
    std::cout << "2: shear " << q << std::endl;
    std::cout << "2: theta " <<  theta * 180 / 3.14159<< " deg" << std::endl;
    std::cout << "2: initTz " << initTz << std::endl;

    cv::Point3f res_3d_offset;
    res_3d_offset.x = c;
    res_3d_offset.y = f;
    res_3d_offset.z = initTz;
    std::cout << "init offset " << res_3d_offset << std::endl;

    double cn = c * cos(theta) - f * sin(theta);
    double fn = c * sin(theta) + f * cos(theta);
    res_3d_offset.x = cn; //c*0.99;
    res_3d_offset.y = fn; //f*1.24;
    std::cout << "rotated offset " << cn << " " << fn << " vs " << f*1.2 << std::endl;


    return res_3d_offset;

     /////////////////////////////////////////////////////////////
     //////////////// visualize homograhy
     /* cv::Mat homoviz_orig(750, 1024, CV_8UC3);
     std::vector<cv::Point2f>::iterator pit = proj3d_1L.begin();
     std::vector<cv::Point2f>::iterator pit2L = proj3d_2L.begin();

     for(   ; pit != proj3d_1L.end() && pit2L != proj3d_2L.end(); pit2L++, pit++ )
     {
          cv::Vec3b color1 (255,0,0); // blue
          cv::Point2f p1 = (*pit) ;
          cv::Mat p3d_mat =  (cv::Mat_<double>(3,1) << p1.x, p1.y, 1);
          cv::Mat uv1 = CGlobalHSSettings::_camL_K * p3d_mat;
          uv1 /= uv1.at<double>(2,0);

          cv::Vec3b color2(0,0,255); // red
          cv::Point2f p2 = (*pit2L) ;
          cv::Mat p3d_mat2 =  (cv::Mat_<double>(3,1) << p2.x, p2.y, 1);
          cv::Mat uv2 = CGlobalHSSettings::_camL_K * p3d_mat2;
          uv2 /= uv2.at<double>(2,0);

          cv::Vec3b color3 (0,255,0); // proj3d_2L_colors[i];
         // cv::Point2f p3 = (*pit2L) ;
         // cv::Mat p3d_mat3 =  (cv::Mat_<double>(3,1) << p3.x, p3.y, 1);
          // cv::Mat H_p3d_mat3 =  H * p3d_mat3;
          // H_p3d_mat3 /= H_p3d_mat3.at<double>(2,0);
           //cv::Mat p3d_mat3_sh =  (cv::Mat_<double>(3,1) << H_p3d_mat3.at<double>(0,0),
         //                         H_p3d_mat3.at<double>(1,0), 1);
           cv::Mat p3d_mat3_sh =  (cv::Mat_<double>(3,1) << p2.x + cn,
                                                            p2.y + fn, 1);
          cv::Mat uv3 = CGlobalHSSettings::_camL_K *  p3d_mat3_sh;
          uv3 /= uv3.at<double>(2,0);


          uv1 = uv1/300 +300;
          uv2 = uv2/300 +300;
          uv3 = uv3/300 +300;

          //std::cout << "p1: " <<  uv1.t() << " " << color1 << std::endl;
         //  homoviz_orig.at<cv::Vec3b>(uv1.at<double>(1,0), uv1.at<double>(0,0)) = color1;

         // homoviz_orig.at<cv::Vec3b>(uv2.at<double>(1,0), uv2.at<double>(0,0)) = color2;
          //std::cout << "p2: " << uv2.t() << " " << color2 << std::endl;

         //homoviz_orig.at<cv::Vec3b>(uv3.at<double>(1,0), uv3.at<double>(0,0)) = color3;
           //std::cout << "p3: " << uv3.t()  << " " << color3 << std::endl;
     }
      //cv::imshow( "Homo - orig", homoviz_orig);
       // cv::waitKey(0);
    */
     //////////////////////////////////////////////////////////////
}


void CFrame::filter_out_far_keypoints()
{
    std::vector<cv::KeyPoint> keypoints_filtered;

    std::vector<cv::KeyPoint>::iterator it =  _keypoints.begin();
    for ( ; it != _keypoints.end(); it++ )
    {
        cv::Point2f p_L = it->pt;
        float disp_1L = _img_disparity.at<float>(p_L.y,p_L.x);
        if(disp_1L  >= CGlobalHSSettings::global_ignore_disparity_for_matching)
            keypoints_filtered.push_back(*it);
    }


    std::cout << "1: far keypoints removed from  " << _keypoints.size() << " to "
              << keypoints_filtered.size() << std::endl;

    _keypoints = keypoints_filtered;
}


/*
std::vector<cv::DMatch> matches_1L_2L_good;
std::vector<cv::DMatch>::iterator mit = _matches[dframe->_id]->begin();
for( ; mit != _matches[dframe->_id]->end(); mit ++)
{

   cv::Point2f p_1L = dframe->_keypoints[mit->queryIdx].pt;
   cv::Point2f p_2L = _keypoints[mit->trainIdx].pt;

   float disp_1L = dframe->_img_disparity.at<float>(p_1L.y,p_1L.x);
   float disp_2L = _img_disparity.at<float>(p_2L.y,p_2L.x);

   if( (disp_1L > CGlobalHSSettings::global_ignore_disparity_offset) &&
           (disp_2L > CGlobalHSSettings::global_ignore_disparity_offset) )
       matches_1L_2L_good.push_back(*mit);
}
*_matches[dframe->_id] = matches_1L_2L_good;
*/

// FLANN based matching
// http://docs.opencv.org/doc/tutorials/features2d/feature_flann_matcher/feature_flann_matcher.html

std::vector<cv::DMatch> * CFrame::flann_match(CFrame * dframe)
{
    //std::cout << "imgpts1 has " << dframe->_keypoints.size() << " points (descriptors " << dframe->_descriptors.rows << ")" << std::endl;
    //std::cout << "imgpts2 has " << _keypoints.size() << " points (descriptors " << _descriptors.rows << ")" << std::endl;

    if(dframe->_descriptors.empty())
    {
        std::cout << "ERROR: destination  frame has not descriptors" << std::endl;
        return NULL;
    }

    if(_descriptors.empty())
    {
        std::cout << "ERROR: current frame has not descriptors" << std::endl;
        return NULL;
    }

    //cv::BFMatcher matcher(cv::NORM_L2);
    //std::vector< cv::DMatch > matches;
    //matcher.match( dframe->_descriptors, _descriptors, matches );

    cv::FlannBasedMatcher matcher;
    std::vector<cv::DMatch > * matches = new std::vector<cv::DMatch >;
    matcher.match(dframe->_descriptors, _descriptors, *matches);

    double max_dist = 0; double min_dist = 10000;
    for( int i = 0; i < dframe->_descriptors.rows; i++ )
    {
        double dist = (*matches)[i].distance;
        if( dist < min_dist ) min_dist = dist;
        if( dist > max_dist ) max_dist = dist;
    }

    if (min_dist < 10.0)
        min_dist = 10.0;

    // Eliminate any re-matching of training points (multiple queries to one training)
    double cutoff = 4.0 * min_dist;
    std::set<int> existing_trainIdx;
    std::vector< cv::DMatch > good_matches_;

    for(unsigned int i = 0; i < matches->size(); i++ )
    {
        //"normalize" matching: somtimes imgIdx is the one holding the trainIdx
        if ((*matches)[i].trainIdx <= 0)
            (*matches)[i].trainIdx = (*matches)[i].imgIdx;


        int tidx = (*matches)[i].trainIdx;
        if((*matches)[i].distance > 0.0 && (*matches)[i].distance < cutoff)
        {
            if( existing_trainIdx.find(tidx) == existing_trainIdx.end() &&
               tidx >= 0 && tidx < (int)(_keypoints.size()) )
            {
                good_matches_.push_back( (*matches)[i]);
                 existing_trainIdx.insert(tidx);
            }
        }
    }

    std::cout << "2: keep " << good_matches_.size() << " out of " << matches->size() << " matches" << std::endl;

    *matches = good_matches_;
    return matches;
}

std::vector<cv::DMatch> * CFrame::knn_match(CFrame *dframe)
{

    //std::cout << "imgpts1 has " << dframe->_keypoints.size() << " points (descriptors " << dframe->_descriptors.rows << ")" << std::endl;
   // std::cout << "imgpts2 has " << _keypoints.size() << " points (descriptors " << _descriptors.rows << ")" << std::endl;

    if(dframe->_descriptors.empty())
    {
        std::cout << "ERROR: current frame has not descriptors" << std::endl;
        return NULL;
    }

    if(_descriptors.empty())
    {
        std::cout << "ERROR: destination frame has not descriptors" << std::endl;
        return NULL;
    }

    cv::BFMatcher matcher(cv::NORM_HAMMING, true);
    std::vector<cv::DMatch > * matches = new std::vector<cv::DMatch >;

    std::vector<double> dists;
    std::vector<std::vector<cv::DMatch> > nn_matches;

    clock_t msstart = clock();

    matcher.knnMatch(dframe->_descriptors, _descriptors, nn_matches, 1);

    clock_t mffinish = clock();
    int sec_conv = (mffinish - msstart);
    std::cout << "1: knn match only - time clocks: " << sec_conv << " secs: " << ((float)sec_conv )/CLOCKS_PER_SEC << std::endl;


    for(int i = 0; i < nn_matches.size(); i++)
    {
        if(nn_matches[i].size()> 0 )
        {
                matches->push_back(nn_matches[i][0]);
                double dist = matches->back().distance;
                if(std::fabs(dist) > 10000) dist = 1.0;
                matches->back().distance = dist;
                dists.push_back(dist);
        }
    }


    double max_dist = 0; double min_dist = 0.0;
    cv::minMaxIdx(dists,&min_dist,&max_dist);

    std::vector<cv::KeyPoint> imgpts1_good, imgpts2_good;

    if (min_dist < 10.0)
        min_dist = 10.0;

    // Eliminate any re-matching of training points (multiple queries to one training)
    double cutoff = 4.0 * min_dist;
    std::set<int> existing_trainIdx;
    std::vector< cv::DMatch > good_matches_, very_good_matches_;

    for(unsigned int i = 0; i < matches->size(); i++ )
    {
        //"normalize" matching: somtimes imgIdx is the one holding the trainIdx
        if ((*matches)[i].trainIdx <= 0)
            (*matches)[i].trainIdx = (*matches)[i].imgIdx;


        int tidx = (*matches)[i].trainIdx;
        if((*matches)[i].distance > 0.0 && (*matches)[i].distance < cutoff)
        {
            if( existing_trainIdx.find(tidx) == existing_trainIdx.end() &&
               tidx >= 0 && tidx < (int)(_keypoints.size()) )
            {
                good_matches_.push_back( (*matches)[i]);
                 existing_trainIdx.insert(tidx);
            }
        }
    }

    std::cout << "1: keep " << good_matches_.size() << " out of " << matches->size() << " matches" << std::endl;

    *matches = good_matches_;

    return matches;
}



void CFrame::filter_out_matches_wrt_epipolar_sence(CFrame * dframe)
{
    std::vector<cv::KeyPoint> pts_good_1L, pts_good_2L;
    GetFundamentalMat(dframe->_keypoints, _keypoints,
                         pts_good_1L, pts_good_2L,
                          *_matches[dframe->_id]  );


    /*
    std::vector<cv::DMatch> matches_1L_2L_good;
    std::vector<cv::DMatch>::iterator mit = _matches[dframe->_id]->begin();
    for( ; mit != _matches[dframe->_id]->end(); mit ++)
    {

       cv::Point2f p_1L = dframe->_keypoints[mit->queryIdx].pt;
       cv::Point2f p_2L = _keypoints[mit->trainIdx].pt;

       //int disp_1L = dframe->_img_disparity.at<short>(p_1L.y,p_1L.x);
       //int disp_2L = _img_disparity.at<short>(p_2L.y,p_2L.x);
       int disp_1L = dframe->_img_disparity.at<float>(p_1L.y,p_1L.x);
       int disp_2L = _img_disparity.at<float>(p_2L.y,p_2L.x);

       if( (disp_1L > CGlobalHSSettings::global_ignore_disparity_for_matching) && (disp_2L > CGlobalHSSettings::global_ignore_disparity_for_matching) )
       {
           // add good match
           matches_1L_2L_good.push_back(*mit);
       }
    }*/

    std::cout << "1: encounter very good matches : " << _matches[dframe->_id]->size() << std::endl;

}


/// DONT forget to check
///  ORB with KNN
/// SIFT with FLANN

void CFrame::match_features(CFrame * dframe)
{
    std::cout << "1: matching frames " << _id << " and " << dframe->_id << std::endl;




   _matches[dframe->_id] = knn_match(dframe);
   //_matches[dframe->_id] = flann_match(dframe);

    /// use fundamental matrix to filter out some matches
  filter_out_matches_wrt_epipolar_sence(dframe);


  //-- Draw only "good" matches
  /*cv::Mat img_matches;
  cv::drawMatches(  dframe->_img_L, dframe->_keypoints,_img_L, _keypoints,
                  *_matches[dframe->_id], img_matches, cv::Scalar::all(-1), cv::Scalar::all(-1),
                 std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
   cv::imshow( "Good Matches", img_matches);
   cv::waitKey(0);
    */



    std::cout << "1: final match count " <<  _matches[dframe->_id]->size() << std::endl;




}


void CFrame::dump_unused_data()
{

    std::map< int, std::vector<cv::DMatch>  *>::iterator it = _matches.begin();
    for(; it != _matches.end(); it++)
        delete it->second;
    _matches.clear();

    _img_L.release();
    _img_R.release();
    _img_disparity.release();
    _img_3d.release();
    _descriptors.release();
    _keypoints.clear();
}

cv::Mat CFrame::get_global_rotation_matrix_forw()
{
    double theta_xp =  _glob_rot_vec.at<double>(0,0);
    double theta_yp =  _glob_rot_vec.at<double>(0,1);
    double theta_zp =  _glob_rot_vec.at<double>(0,2);

    cv::Mat rotXp = (cv::Mat_<double>(3,1) << theta_xp, 0, 0);
    cv::Mat rotYp = (cv::Mat_<double>(3,1) << 0, theta_yp, 0);
    cv::Mat rotZp = (cv::Mat_<double>(3,1) << 0, 0, theta_zp);
    cv::Mat rotM_Xp, rotM_Yp, rotM_Zp;
    cv::Rodrigues(rotXp, rotM_Xp);
    cv::Rodrigues(rotYp, rotM_Yp);
    cv::Rodrigues(rotZp, rotM_Zp);

    cv::Mat forwRot =  rotM_Xp * rotM_Zp * rotM_Yp;
    return forwRot;
}

cv::Mat CFrame::get_global_rotation_matrix_bakw()
{
    double theta_x =  -_glob_rot_vec.at<double>(0,0);
    double theta_y =  -_glob_rot_vec.at<double>(0,1);
    double theta_z =  -_glob_rot_vec.at<double>(0,2);

    cv::Mat rotX = (cv::Mat_<double>(3,1) << theta_x, 0, 0);
    cv::Mat rotY = (cv::Mat_<double>(3,1) << 0, theta_y, 0);
    cv::Mat rotZ = (cv::Mat_<double>(3,1) << 0, 0, theta_z);

    cv::Mat rotM_X, rotM_Y, rotM_Z;
    cv::Rodrigues(rotX, rotM_X);
    cv::Rodrigues(rotY, rotM_Y);
    cv::Rodrigues(rotZ, rotM_Z);

    cv::Mat backwRot  =  rotM_Y * rotM_Z * rotM_X   ;
    return backwRot;
}

cv::Mat CFrame::get_neg_angular_difference_as_rotation_matrix(CFrame * dframe)
{
    /*

    // old version
    cv::Mat rvec = dframe->_glob_rot_vec - _glob_rot_vec;


    double theta_x = rvec.at<double>(0,0);
    double theta_y = rvec.at<double>(0,1);
    double theta_z = rvec.at<double>(0,2);


    cv::Mat rotX = (cv::Mat_<double>(3,1) << theta_x, 0, 0);
    cv::Mat rotY = (cv::Mat_<double>(3,1) << 0, theta_y, 0);
    cv::Mat rotZ = (cv::Mat_<double>(3,1) << 0, 0, theta_z);

    cv::Mat rotM_X, rotM_Y, rotM_Z;
    cv::Rodrigues(rotX, rotM_X);
    cv::Rodrigues(rotY, rotM_Y);
    cv::Rodrigues(rotZ, rotM_Z);

    cv::Mat backRot;
    backRot =rotM_X * rotM_Y *  rotM_Z ;

    return backRot; */

    cv::Mat rvec_c = _glob_rot_vec;
    double theta_xc = -rvec_c.at<double>(0,0);
    double theta_yc = -rvec_c.at<double>(0,1);
    double theta_zc = -rvec_c.at<double>(0,2);
    cv::Mat rotXc = (cv::Mat_<double>(3,1) << theta_xc, 0, 0);
    cv::Mat rotYc = (cv::Mat_<double>(3,1) << 0, theta_yc, 0);
    cv::Mat rotZc = (cv::Mat_<double>(3,1) << 0, 0, theta_zc);

    cv::Mat rotM_Xc, rotM_Yc, rotM_Zc;
    cv::Rodrigues(rotXc, rotM_Xc);
    cv::Rodrigues(rotYc, rotM_Yc);
    cv::Rodrigues(rotZc, rotM_Zc);
    cv::Mat backRot;
   /// backRot = rotM_Xc * rotM_Yc *  rotM_Zc ;
backRot = rotM_Yc *  rotM_Zc  * rotM_Xc ;

    cv::Mat rvec_p = dframe->_glob_rot_vec;
    double theta_xp = rvec_p.at<double>(0,0);
    double theta_yp = rvec_p.at<double>(0,1);
    double theta_zp = rvec_p.at<double>(0,2);
    cv::Mat rotXp = (cv::Mat_<double>(3,1) << theta_xp, 0, 0);
    cv::Mat rotYp = (cv::Mat_<double>(3,1) << 0, theta_yp, 0);
    cv::Mat rotZp = (cv::Mat_<double>(3,1) << 0, 0, theta_zp);
    cv::Mat rotM_Xp, rotM_Yp, rotM_Zp;
    cv::Rodrigues(rotXp, rotM_Xp);
    cv::Rodrigues(rotYp, rotM_Yp);
    cv::Rodrigues(rotZp, rotM_Zp);

    cv::Mat forwRot;
 ///   forwRot = rotM_Zp * rotM_Yp *  rotM_Xp ;
forwRot = rotM_Xp * rotM_Zp * rotM_Yp ;

    return forwRot*backRot;

}


cv::Mat CFrame::get_pos_angular_difference_as_rotation_matrix(CFrame * dframe)
{
    /*
     *

    /// old version
    cv::Mat rvec = dframe->_glob_rot_vec -  _glob_rot_vec;

    double theta_x = -rvec.at<double>(0,0);
    double theta_y = -rvec.at<double>(0,1);
    double theta_z = -rvec.at<double>(0,2);

    cv::Mat rotX = (cv::Mat_<double>(3,1) << theta_x, 0, 0);
    cv::Mat rotY = (cv::Mat_<double>(3,1) << 0, theta_y, 0);
    cv::Mat rotZ = (cv::Mat_<double>(3,1) << 0, 0, theta_z);

    cv::Mat rotM_X, rotM_Y, rotM_Z;
    cv::Rodrigues(rotX, rotM_X);
    cv::Rodrigues(rotY, rotM_Y);
    cv::Rodrigues(rotZ, rotM_Z);

    cv::Mat forwRot;
    forwRot = rotM_Z * rotM_Y * rotM_X;

    return forwRot ;*/



    cv::Mat rvec_p = dframe->_glob_rot_vec;
    double theta_xp = -rvec_p.at<double>(0,0);
    double theta_yp = -rvec_p.at<double>(0,1);
    double theta_zp = -rvec_p.at<double>(0,2);
    cv::Mat rotXp = (cv::Mat_<double>(3,1) << theta_xp, 0, 0);
    cv::Mat rotYp = (cv::Mat_<double>(3,1) << 0, theta_yp, 0);
    cv::Mat rotZp = (cv::Mat_<double>(3,1) << 0, 0, theta_zp);
    cv::Mat rotM_Xp, rotM_Yp, rotM_Zp;
    cv::Rodrigues(rotXp, rotM_Xp);
    cv::Rodrigues(rotYp, rotM_Yp);
    cv::Rodrigues(rotZp, rotM_Zp);

    cv::Mat backwRot;
   /// backwRot = rotM_Xp * rotM_Yp * rotM_Zp ;
 backwRot =  rotM_Yp* rotM_Zp * rotM_Xp ;

    cv::Mat rvec_c = _glob_rot_vec;
    double theta_xc = rvec_c.at<double>(0,0);
    double theta_yc = rvec_c.at<double>(0,1);
    double theta_zc = rvec_c.at<double>(0,2);
    cv::Mat rotXc = (cv::Mat_<double>(3,1) << theta_xc, 0, 0);
    cv::Mat rotYc = (cv::Mat_<double>(3,1) << 0, theta_yc, 0);
    cv::Mat rotZc = (cv::Mat_<double>(3,1) << 0, 0, theta_zc);

    cv::Mat rotM_Xc, rotM_Yc, rotM_Zc;
    cv::Rodrigues(rotXc, rotM_Xc);
    cv::Rodrigues(rotYc, rotM_Yc);
    cv::Rodrigues(rotZc, rotM_Zc);
    cv::Mat forwRot;
    /// forwRot = rotM_Zc * rotM_Yc *  rotM_Xc  ;
forwRot = rotM_Xc * rotM_Zc *  rotM_Yc  ;


    return forwRot*backwRot;
}


C3DPoint CFrame::convert_point_coord_real_to_gl( cv::Mat & Rglob, int x, int y )
{
    cv::Point3f pp = _img_3d.at<cv::Point3f>(y,x);
    cv::Mat pp_mat = (cv::Mat_<double>(3,1) << pp.x, pp.y, pp.z);
    cv::Mat pp_mat_glob = Rglob * pp_mat + _global_pos;

    C3DPoint p;
    p._x = convert_real_coord_to_gl_x ( pp_mat_glob.at<double>(0,0));
    p._y = convert_real_coord_to_gl_y ( pp_mat_glob.at<double>(1,0));
    p._z = convert_real_coord_to_gl_z ( pp_mat_glob.at<double>(2,0));


    return p;
}

std::vector<cv::Point3f> CFrame::get_neghbour_3d_glpoints(std::vector<C3DPoint> & gl_points, int x, int y, int xs, int ys, int decimstep, int nsz)
{
    std::vector<cv::Point3f> res;

    for (int j = y - nsz * decimstep;
         j <= y+ nsz *decimstep; j+=decimstep )
    {
        for (int i = x - nsz * decimstep;
             i <= x + nsz * decimstep; i+=decimstep)
        {

            if( (j >= 0) && (j < ys) && (i >=0) && (i < xs))
            {
                 int cdisp = _img_disparity.at<float>(j,i);
                 if((cdisp > CGlobalHSSettings::global_ignore_disparity_offset) && (cdisp < CGlobalHSSettings::_disparity_parameters.disp_max) )
                 {
                        C3DPoint p = gl_points[j * xs + i];
                        cv::Point3f pp;
                        pp.x = p._x;
                        pp.y = p._y;
                        pp.z = p._z;
                        res.push_back(pp);
                 }
            }
        }
    }

    return res;
}

CTriMesh * CFrame::construct_meshcast_from_depth_img(int decimstep)
{
    CTriMesh * tmesh = new CTriMesh;
    cv::Mat Rglob  = get_global_rotation_matrix_bakw();

    int xs = _img_L.cols;
    int ys = _img_L.rows;

    const int undefid = -1;
    std::vector<int> id_points(xs*ys, undefid);

    ///////////////// define points
    int pnt_idx = 0;
    for (int y = 0; y < ys; y+= decimstep )
    for (int x = 0; x < xs; x+= decimstep )
    {
        int cdisp = _img_disparity.at<float>(y,x);
        if((cdisp > CGlobalHSSettings::global_ignore_disparity_offset) && (cdisp < CGlobalHSSettings::_disparity_parameters.disp_max) )
        {
            tmesh->_points.push_back( convert_point_coord_real_to_gl(Rglob, x, y) );
            id_points[y*xs + x] = pnt_idx;
            pnt_idx++;
        }
    }

    ///////////////// define triangles
    for (int y = 0; y < ys - decimstep ; y+= decimstep )
    for (int x = 0; x < xs - decimstep;  x+= decimstep )
    {
        int pidx_0 = id_points[y*xs + x];
        int pidx_1 = id_points[y*xs + x + decimstep];
        int pidx_2 = id_points[(y + decimstep) * xs + x];
        int pidx_3 = id_points[(y + decimstep) * xs + x + decimstep];

        // first valid triangle
        if((pidx_0 != undefid) && (pidx_1 != undefid) && (pidx_2 != undefid) )
        {
            // add new triangle
            C3DPointIdx tri;
            tri.pnt_index[0] = pidx_2;
            tri.pnt_index[1] = pidx_1;
            tri.pnt_index[2] = pidx_0;
            tmesh->_triangles.push_back(tri);
        }

        // second valid triangle
        if((pidx_2 != undefid) && (pidx_1 != undefid) && (pidx_3 != undefid) )
        {
            // add new triangle
            C3DPointIdx tri;
            tri.pnt_index[0] = pidx_3;
            tri.pnt_index[1] = pidx_1;
            tri.pnt_index[2] = pidx_2;
            tmesh->_triangles.push_back(tri);
        }
    }

    ///////// update mesh normals per face
    std::vector<C3DPointIdx>::iterator itt = tmesh->_triangles.begin();
    for(int fidx = 0 ; itt != tmesh->_triangles.end(); itt++, fidx++)
    {
        tmesh->_points[ itt->pnt_index[0] ]._faceidx.push_back(fidx);
        C3DPoint pnt0 = tmesh->_points[ itt->pnt_index[0] ];

        tmesh->_points[ itt->pnt_index[1] ]._faceidx.push_back(fidx);
        C3DPoint pnt1 = tmesh->_points[ itt->pnt_index[1] ];

        tmesh->_points[ itt->pnt_index[2] ]._faceidx.push_back(fidx);
        C3DPoint pnt2 = tmesh->_points[ itt->pnt_index[2] ];

        itt->tri_normal = compute_normal(pnt2, pnt1, pnt0);
    }

    //////// update mesh normals per vertex
    std::vector<C3DPoint>::iterator itv = tmesh->_points.begin();
    for(; itv != tmesh->_points.end(); itv++)
    {
        itv->_nx =0;
        itv->_ny =0;
        itv->_nz =0;

        std::vector<int>::iterator fit = itv->_faceidx.begin();
        for( ; fit != itv->_faceidx.end(); fit++)
        {

            itv->_nx += tmesh->_triangles[(*fit)].tri_normal._x;
            itv->_ny += tmesh->_triangles[(*fit)].tri_normal._y;
            itv->_nz += tmesh->_triangles[(*fit)].tri_normal._z;
        }
        normalize3(itv->_nx, itv->_ny, itv->_nz);
    }



    return tmesh;
}

std::vector<C3DPoint> *   CFrame::construct_spcl_from_depth_img_with_norms(bool usedecim=true)
{

    double theta_x =  -_glob_rot_vec.at<double>(0,0);
    double theta_y =  -_glob_rot_vec.at<double>(0,1);
    double theta_z =  -_glob_rot_vec.at<double>(0,2);

    cv::Mat rotX = (cv::Mat_<double>(3,1) << theta_x, 0, 0);
    cv::Mat rotY = (cv::Mat_<double>(3,1) << 0, theta_y, 0);
    cv::Mat rotZ = (cv::Mat_<double>(3,1) << 0, 0, theta_z);

    cv::Mat rotM_X, rotM_Y, rotM_Z;
    cv::Rodrigues(rotX, rotM_X);
    cv::Rodrigues(rotY, rotM_Y);
    cv::Rodrigues(rotZ, rotM_Z);

    cv::Mat Rglob  =  rotM_Y * rotM_Z * rotM_X   ;
    cv::Mat Rglob_inv  =  Rglob.inv();

    int xs = _img_L.cols;
    int ys = _img_L.rows;

    int decimstep = (usedecim)?CGlobalHSSettings::global_spcl_decim_step:1;

    /////////////// compute possible gl_points

    std::vector<C3DPoint> gl_points(xs*ys);
    for (int y = 0; y < ys; y+= decimstep )
    for (int x = 0; x < xs; x+= decimstep )
    {
        int cdisp = _img_disparity.at<float>(y,x);
        if((cdisp > CGlobalHSSettings::global_ignore_disparity_offset) && (cdisp < CGlobalHSSettings::_disparity_parameters.disp_max) )
        {
            gl_points[y*xs +x ] = convert_point_coord_real_to_gl(Rglob, x, y);
        }
    }

    ////////////////////////////////////
    std::vector<C3DPoint> *  final_res = new std::vector<C3DPoint>;
    for (int y = 0; y < ys; y+= decimstep )
    for (int x = 0; x < xs; x+= decimstep )
    {
        int cdisp = _img_disparity.at<float>(y,x);
        if((cdisp > CGlobalHSSettings::global_ignore_disparity_offset) && (cdisp < CGlobalHSSettings::_disparity_parameters.disp_max) )
        {
             std::vector<cv::Point3f> neigh_pnts = get_neghbour_3d_glpoints(gl_points, x, y, xs, ys, decimstep, 5);
             if(neigh_pnts.size() > 5)
            {
                    Vector3f n = compute_lse_plane_normal(neigh_pnts);
                    /// normal postprocessing
                    cv::Mat n_mat = (cv::Mat_<double>(3,1) << n.x, n.y, n.z);
                    cv::Mat n_mat_rot = Rglob  * n_mat;
                    if(n_mat_rot.at<double>(2,0) < 0.)
                    {
                        n.x = -n.x;
                        n.y = -n.y;
                        n.z = -n.z;
                    }
                    ////////////////////////

                    C3DPoint p = gl_points[y*xs +x ];

                    cv::Vec3b rgbv = _img_L.at<cv::Vec3b>(y,x);
                    p._r = float(rgbv.val[2]) / 256.0;
                    p._g = float(rgbv.val[1]) / 256.0;
                    p._b = float(rgbv.val[0]) / 256.0;

                    p._nx = n.x;
                    p._ny = n.y;
                    p._nz = n.z;

                    final_res->push_back(p);

             }
        }
    }

    return final_res;
}


std::vector<cv::Point3f> CFrame::get_neghbour_3d_points(int x, int y, int decimstep)
{

    std::vector<cv::Point3f> res;

    int xs = _img_L.cols;
    int ys = _img_L.rows;

    int neigh_sz = 10;

    for (int j = y-neigh_sz* decimstep; j <= y+neigh_sz*decimstep; j+=decimstep )
    {
        for (int i = x-neigh_sz*decimstep; i <= x+neigh_sz*decimstep; i+=decimstep)
        {

            if(j>=0 && j < ys && i >=0 && i < xs)
            {
                 int cdisp = _img_disparity.at<float>(j,i);
                 if((cdisp > CGlobalHSSettings::global_ignore_disparity_offset) && (cdisp < CGlobalHSSettings::_disparity_parameters.disp_max) )
                 {
                     cv::Point3f pp = _img_3d.at<cv::Point3f>(j,i);
                     res.push_back(pp);
                 }
            }
        }
    }

    return res;

}

int CFrame::construct_dpcl_from_depth_img(std::list<C3DPoint> * pnts, int decimstep)
{

    cv::Mat Rglob  = get_global_rotation_matrix_bakw();

    int xs = _img_L.cols;
    int ys = _img_L.rows;

    int points_count = 0;
    for (int y = 0; y < ys; y+= decimstep )
    for (int x = 0; x < xs; x+= decimstep )
    {
        int cdisp = _img_disparity.at<float>(y,x);
        if((cdisp > CGlobalHSSettings::global_ignore_disparity_offset) && (cdisp < CGlobalHSSettings::_disparity_parameters.disp_max) )
        {
            cv::Point3f pp = _img_3d.at<cv::Point3f>(y,x);
            cv::Mat pp_mat = (cv::Mat_<double>(3,1) << pp.x, pp.y, pp.z);
            cv::Mat pp_mat_glob = Rglob * pp_mat + _global_pos;

            C3DPoint p;
            p._x =   pp_mat_glob.at<double>(0,0)  * CGlobalHSSettings::glob_gl_scale + CGlobalHSSettings::glob_gl_offx;
            p._y = - pp_mat_glob.at<double>(1,0)  * CGlobalHSSettings::glob_gl_scale + CGlobalHSSettings::glob_gl_offy;
            p._z = - pp_mat_glob.at<double>(2,0)  * CGlobalHSSettings::glob_gl_scale + CGlobalHSSettings::glob_gl_offz;

            cv::Vec3b rgbv = _img_L.at<cv::Vec3b>(y,x);
             p._r = 0.8;//float(rgbv.val[2]) / 256.0;
             p._g = 0.8;//float(rgbv.val[1]) / 256.0;
             p._b = 0.8;//float(rgbv.val[0]) / 256.0;

            pnts->push_back(p);
            points_count++;
        }
    }
    return points_count;
}

int CFrame::construct_dpcl_from_depth_img_with_norms(std::vector<C3DPoint> * pnts, int decimstep, int nsz)
{

    cv::Mat Rglob  = get_global_rotation_matrix_bakw();
    cv::Mat Rglob_inv  =  Rglob.inv();

    int xs = _img_L.cols;
    int ys = _img_L.rows;


    /////////////// compute possible gl_points
    std::vector<C3DPoint> gl_points(xs*ys);
    for (int y = 0; y < ys; y+= decimstep )
    for (int x = 0; x < xs; x+= decimstep )
    {
        int cdisp = _img_disparity.at<float>(y,x);
        if((cdisp > CGlobalHSSettings::global_ignore_disparity_offset) && (cdisp < CGlobalHSSettings::_disparity_parameters.disp_max) )
        {
            gl_points[y*xs +x ] = convert_point_coord_real_to_gl(Rglob, x, y);
        }
    }

    int points_count = 0;
    ////////////////////////////////////
    for (int y = 0; y < ys; y+= decimstep )
    for (int x = 0; x < xs; x+= decimstep )
    {
        int cdisp = _img_disparity.at<float>(y,x);
        if((cdisp > CGlobalHSSettings::global_ignore_disparity_offset) && (cdisp < CGlobalHSSettings::_disparity_parameters.disp_max) )
        {
             std::vector<cv::Point3f> neigh_pnts = get_neghbour_3d_glpoints(gl_points, x, y, xs, ys, decimstep, nsz);
             if(neigh_pnts.size() > 5)
            {
                    Vector3f n = compute_lse_plane_normal(neigh_pnts);
                    /// normal postprocessing
                    cv::Mat n_mat = (cv::Mat_<double>(3,1) << n.x, n.y, n.z);
                    cv::Mat n_mat_rot = Rglob  * n_mat;
                    if(n_mat_rot.at<double>(2,0) < 0.)
                    {
                        n.x = -n.x;
                        n.y = -n.y;
                        n.z = -n.z;
                    }
                    ////////////////////////

                    C3DPoint p = gl_points[y*xs +x ];

                    cv::Vec3b rgbv = _img_L.at<cv::Vec3b>(y,x);
                    p._r = float(rgbv.val[2]) / 256.0;
                    p._g = float(rgbv.val[1]) / 256.0;
                    p._b = float(rgbv.val[0]) / 256.0;

                    p._nx = n.x;
                    p._ny = n.y;
                    p._nz = n.z;

                    pnts->push_back(p);
                    points_count++;
             }
        }
    }


    return points_count;

}


std::vector<C3DPoint> *  CFrame::construct_spcl_from_depth_img(bool usedecim)
{
    cv::Mat Rglob  = get_global_rotation_matrix_bakw();

    int xs = _img_L.cols;
    int ys = _img_L.rows;

    int decimstep = (usedecim)?CGlobalHSSettings::global_spcl_decim_step:1;

    std::vector<C3DPoint> *  final_res = new std::vector<C3DPoint>;
    for (int y = 0; y < ys; y+= decimstep )
    for (int x = 0; x < xs; x+= decimstep )
    {
        int cdisp = _img_disparity.at<float>(y,x);
        if((cdisp > CGlobalHSSettings::global_ignore_disparity_offset) && (cdisp < CGlobalHSSettings::_disparity_parameters.disp_max) )
        {

            C3DPoint p = convert_point_coord_real_to_gl(Rglob, x, y);

            cv::Vec3b rgbv = _img_L.at<cv::Vec3b>(y,x);
            p._r = float(rgbv.val[2]) / 256.0;
            p._g = float(rgbv.val[1]) / 256.0;
            p._b = float(rgbv.val[0]) / 256.0;

            final_res->push_back(p);
        }
    }
    return final_res;
}
