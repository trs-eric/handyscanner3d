#include "ccropdiskdlg.h"
#include "ui_ccropdiskdlg.h"

#include <QMessageBox>
#include "cglwidget.h"
#include "ccropdiskthread.h"
#include "messages.h"

CCropDiskDlg::CCropDiskDlg(CGLWidget * glWidget, QWidget *parent) :
    QDialog(parent), _glWidget(glWidget),
    ui(new Ui::CCropDiskDlg), _on_apply(false), _in_process(false),
     _thread(NULL), _is_result_valid(false), _orig_cloud(NULL)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);

    ui->cropBotCheck->setChecked(true);
    ui->progressBar->setMaximum(100);
    ui->cropBotVal->setValue(0);
    ui->cropTopVal->setValue(80);
    ui->cropCylVal->setValue(80);


    connect(ui->previewBtn, SIGNAL(clicked()), this, SLOT(onPreviewBtn()));
    connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));


    _orig_cloud = new CPointCloud;
    _thread = new CCropDiskThread;

    // user pressed cancel button
    connect(ui->cancelBtn,  SIGNAL(clicked()), _thread, SLOT(stop_process()));
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

    // user closed dialg
    connect(this,  SIGNAL(finished(int)), _thread, SLOT(stop_process()));

    // working with a thread
    connect(_thread, SIGNAL(send_back(int)), this, SLOT(onMakeProgressStep(int)));
    connect(_thread, SIGNAL(send_result(CPointCloud*)), this, SLOT(onCompleteProcessing(CPointCloud*)));
    connect(_thread, SIGNAL(send_result(CPointCloud*)), _glWidget, SLOT(on_raw_pnts_computed(CPointCloud*)));
    connect(_thread, SIGNAL(finished()), this, SLOT(onStopProcessing()));

}

void CCropDiskDlg::onMakeProgressStep(int val)
{
    ui->progressBar->setValue(val);
}

void CCropDiskDlg::onCompleteProcessing(CPointCloud * pcld)
{
    if(_on_apply)
    {
        close_dialog();
        return;
    }

    _is_result_valid = true;
    ui->progressBar->setValue(100);
}

void CCropDiskDlg::onStopProcessing()
{
    ui->applyBtn->setEnabled(true);
    ui->previewBtn->setEnabled(true);
    ui->bottomCheck->setEnabled(true);
    ui->topCheck->setEnabled(true);

    ui->cropBotCheck->setEnabled(true);
    ui->cropBotVal->setEnabled(true);
    ui->cropCylCheck->setEnabled(true);
    ui->cropCylVal->setEnabled(true);
    ui->cropTopCheck->setEnabled(true);
    ui->cropTopVal->setEnabled(true);

    _in_process = false;
}

CCropDiskDlg::~CCropDiskDlg()
{
    delete ui;
    delete _thread;
    delete _orig_cloud;
}

void CCropDiskDlg::close_dialog()
{
    _on_apply = false;
    close();
}

void CCropDiskDlg::onCancelBtn()
{
    if(!_in_process)
    {
        //_glWidget->discard_optim_points_of_current_object();
        CPointCloud * restore_cloud = new CPointCloud;
        std::vector<C3DPoint> pnts = _orig_cloud->get_points_cpy();
        restore_cloud->set_points(pnts);
        _glWidget->on_raw_pnts_computed(restore_cloud);
        close_dialog();
    }
    _in_process = false;
    _on_apply = false;
}

void CCropDiskDlg::onPreviewBtn()
{
    if(!ui->cropBotCheck->isChecked() &&
       !ui->cropTopCheck->isChecked() &&
       !ui->cropCylCheck->isChecked() &&
       !ui->bottomCheck->isChecked()  &&
       !ui->topCheck->isChecked() ) return;

    if(_orig_cloud == NULL)
    {
       QMessageBox::warning(this, tr("Warning"),wrn_NothingNormal);
       return;
    }

    ui->applyBtn    ->setEnabled(false);
    ui->previewBtn  ->setEnabled(false);
    ui->bottomCheck->setEnabled(false);
    ui->topCheck->setEnabled(false);
    ui->cropBotCheck->setEnabled(false);
    ui->cropBotVal->setEnabled(false);
    ui->cropCylCheck->setEnabled(false);
    ui->cropCylVal->setEnabled(false);
    ui->cropTopCheck->setEnabled(false);
    ui->cropTopVal->setEnabled(false);

    _in_process = true;
    _thread->start_process(_orig_cloud,
                           ui->cropTopCheck->isChecked(),
                           ui->cropTopVal->value(),
                           ui->cropBotCheck->isChecked(),
                           ui->cropBotVal->value(),
                           ui->cropCylCheck->isChecked(),
                           ui->cropCylVal->value(),
                           false, false
                           /*ui->bottomCheck->isChecked(), ui->topCheck->isChecked()*/);

}

void CCropDiskDlg::onApplyBtn()
{
    _on_apply = true;
    if(!_is_result_valid)
        onPreviewBtn();
    else
        close_dialog();
}

void CCropDiskDlg::showEvent(QShowEvent * event)
{
    ui->progressBar->setValue(0);

    _is_result_valid    = false;
    _on_apply           = false;
    _in_process         = false;

    std::vector<C3DPoint> pnts = _glWidget->get_raw_points_of_current_object()->get_points_cpy();
    _orig_cloud ->set_points(pnts);

    if(_orig_cloud == NULL)
    {
       QMessageBox::warning(this, tr("Warning"),wrn_NothingNormal);
       return;
    }

}
