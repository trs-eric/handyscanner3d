#ifndef CDEVICECONTROLDLG_H
#define CDEVICECONTROLDLG_H

#include <QDialog>

namespace Ui {
class CDeviceControlDlg;
}

class CDeviceControlDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CDeviceControlDlg(QWidget *parent = 0);
    ~CDeviceControlDlg();

private slots:
    void test_motor();
    void test_snapshot();

private:
    Ui::CDeviceControlDlg *ui;
};

#endif // CDEVICECONTROLDLG_H
