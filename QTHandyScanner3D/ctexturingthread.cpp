#include "ctexturingthread.h"
#include "ctriangularmesh.h"
#include "cframe.h"
#include "./settings/psettings.h"
#include <QPainter>
#include <QFileInfo>
#include <QSettings>


#include "./libcoldet/coldet.h"
#include "scene_ini_constants.h"
#include <ctime>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>

////// solve pnp
///// http://stackoverflow.com/questions/28684164/cvsolvepnpransac-returns-identity-matrix


CTexturingThread::CTexturingThread(QObject *parent) :
    QThread(parent)
{
        _abort = false;
}

CTexturingThread::~CTexturingThread()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
    wait();
}

void CTexturingThread::start_process(CTriangularMesh * mesh, int step, float mgt, float gait,  QString fn)
{

    _abort = false;
    _mesh = mesh;
    _frame_step = step;
    _merge_group_thres = mgt;
    _max_color_adjust_iterations = gait;
    _inif = fn;
    start();
}


void CTexturingThread::stop_process()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
}

void CTexturingThread::emit_send_back(int i)
{
     emit send_back(i);
}


const int not_visited = -1;


void CTexturingThread::construct_face_group(std::list<int> & face_group_current,
                                            std::list<int> & face_group_next,
            int frame_ref_group, CTrianglesGroup* curr_group,
        std::list<int> & face_group)
{


    std::list<int>::iterator itf = face_group_current.begin();
    for( ; itf != face_group_current.end();  itf++)
    {
        if(_group_ref[*itf] != NULL)
            continue;

        /// fix for invisible areas
        ///if (_frame_ref[*itf] == not_visited)
         ///   _frame_ref[*itf] = frame_ref_group;

        if((_frame_ref[*itf] != frame_ref_group) )
            continue;

        _group_ref[*itf] = curr_group;
        face_group.push_back(*itf);

        for (int p = 0; p < 3; p++)
        {
            std::vector<int>::iterator itfm = _tri_mesh->_points[ _tri_mesh->_triangles[*itf].pnt_index[p] ]._faceidx.begin();
            for( ; itfm != _tri_mesh->_points[ _tri_mesh->_triangles[*itf].pnt_index[p] ]._faceidx.end(); itfm++)
            {
                if(*itfm == *itf) // same face - skip
                  continue;

                face_group_next.push_back(*itfm);
            }
        }

    }
}

void CTexturingThread::merge_with_largest_neighbour(CTrianglesGroup * current_group,
                                                    std::vector< CTrianglesGroup*> & face_groups)
{


    ///std::cout << " current group "<< current_group->_frame->_imgL_str.toStdString()<< std::endl;

    // find neighbour group with the largest number of triangles

    CTrianglesGroup * result_group = NULL;

    int result_sz = 0;

    // check all faces
    std::list<int>::iterator itf = current_group->_face_group.begin();
    for( ; itf != current_group->_face_group.end();  itf++)
    {
        // chack all neighbour faces via corner points
        for (int p = 0; p < 3; p++)
        {
            std::vector<int>::iterator itfm = _tri_mesh->_points[ _tri_mesh->_triangles[*itf].pnt_index[p] ]._faceidx.begin();
            for( ; itfm != _tri_mesh->_points[ _tri_mesh->_triangles[*itf].pnt_index[p] ]._faceidx.end(); itfm++)
            {

                if(*itfm == *itf) // same face - skip
                  continue;

                if(_group_ref[*itfm] == NULL)
                    continue;

                // it is group with no frame
                if(_group_ref[*itfm] ->_frame == NULL)
                    continue;

               if(_group_ref[*itfm] == current_group ) // same triangle group
                    continue;

               if( _group_ref[*itfm]->_face_group.size() >= result_sz)
               {
                    result_group = _group_ref[*itfm];
                    result_sz = _group_ref[*itfm]->_face_group.size();
               }
            }
        }
    }


    if(result_group == NULL)
    {
        std::cout<< "found a group without any neighbour " << std::endl;
        return;
    }

     result_group->merge(current_group);


    std::vector<CTrianglesGroup*>::iterator itt =  _group_ref.begin();
    for( ; itt != _group_ref.end(); itt++)
    {
        if(*itt == current_group)
           *itt = result_group;
    }
    delete current_group;


    // keep the list sorted properly
     std::vector< CTrianglesGroup*>::iterator lpos =  face_groups.begin();
    while ( (*lpos != result_group) && (lpos != face_groups.end()))
          lpos++;

     if(lpos == face_groups.end())
         return;

    face_groups.erase(lpos);


    lpos =  face_groups.begin();
    while ( ((*lpos)->_face_group.size() < result_group->_face_group.size()) && (lpos != face_groups.end()))
        lpos++;
    face_groups.insert(lpos,result_group);

   /// std::cout << " resulting group "<< result_group->_frame->_imgL_str.toStdString()<< std::endl;
}

/*
CTrianglesGroup* CTexturingThread::find_largest_neighbour( std::vector< CTrianglesGroup*> & face_groups,
                                         CTrianglesGroup* current_group, int current_group_idx)
{
    CTrianglesGroup * result_group = NULL;
    int result_sz = 0;

    std::list<int>::iterator itf = current_group->_face_group.begin();
    for( ; itf != current_group->_face_group.end();  itf++)
    {
        for (int p = 0; p < 3; p++)
        {
            std::vector<int>::iterator itfm = _tri_mesh->_points[ _tri_mesh->_triangles[*itf].pnt_index[p] ]._faceidx.begin();
            for( ; itfm != _tri_mesh->_points[ _tri_mesh->_triangles[*itf].pnt_index[p] ]._faceidx.end(); itfm++)
            {
                if(*itfm == *itf) // same face - skip
                  continue;

                if(_group_ref[*itfm] == current_group_idx ) // same triangle group
                    continue;

               if( face_groups[_group_ref[*itfm]]->_face_group.size() > result_sz)
               {
                    result_group = face_groups[_group_ref[*itfm]];
                    result_sz = face_groups[_group_ref[*itfm]]->_face_group.size();
               }
            }
        }
    }
    return result_group;
}*/

bool test_Moeller_Trumbore_triangle_intersection
(C3DPoint V1 , C3DPoint V2, C3DPoint V3, Vector3f & camera_origin, Vector3f & camera_normal)
{

    // Find vectors for two edges sharing V1
    // SUB(e1, V2, V1);
    Vector3f e1;
    e1.x = V2._x - V1._x;
    e1.y = V2._y - V1._y;
    e1.z = V2._z - V1._z;

    // SUB(e2, V3, V1);
    Vector3f e2;
    e2.x = V3._x - V1._x;
    e2.y = V3._y - V1._y;
    e2.z = V3._z - V1._z;

    // Begin calculating determinant - also used to calculate u parameter
    // CROSS(P, D, e2);
    Vector3f P;
    P.x = camera_normal.y * e2.z - camera_normal.z * e2.y;
    P.y = camera_normal.z * e2.x - camera_normal.x * e2.z;
    P.z = camera_normal.x * e2.y - camera_normal.y * e2.x;

    // if determinant is near zero, ray lies in plane of triangle
    // float det = DOT(e1, P);
    float det = e1.x * P.x + e1.y * P.y + e1.z * P.z;

    //NOT CULLING
    if((det > -cEPSILON) && (det < cEPSILON)) return false;
    float inv_det = 1.f / det;

    // calculate distance from V1 to ray origin
    // SUB(T, O, V1);
    Vector3f T;
    T.x = camera_origin.x - V1._x;
    T.y = camera_origin.y - V1._y;
    T.z = camera_origin.z - V1._z;

    //Calculate u parameter and test bound
    // float u = DOT(T, P) * inv_det;
    float u = (T.x * P.x + T.y * P.y + T.z * P.z) * inv_det;

    // The intersection lies outside of the triangle
    if((u < 0.f) || (u > 1.f)) return false;

    // Prepare to test v parameter
    // CROSS(Q, T, e1);
    Vector3f Q;
    Q.x = T.y * e1.z - T.z * e1.y;
    Q.y = T.z * e1.x - T.x * e1.z;
    Q.z = T.x * e1.y - T.y * e1.x;

    // Calculate V parameter and test bound
    // float v = DOT(D, Q) * inv_det;
    float v = (camera_normal.x * Q.x +
               camera_normal.y * Q.y +
               camera_normal.z * Q.z) * inv_det;

    // The intersection lies outside of the triangle
    if( (v < 0.f) || (u + v  > 1.f)) return false;

    // float t = DOT(e2, Q) * inv_det;
    float t = (e2.x * Q.x + e2.y * Q.y + e2.z * Q.z) * inv_det;

    // ray intersection
    if(t > cEPSILON) return true;

    // No hit, no win
    return false;
}

bool Moeller_Trumbore_triangle_intersection_simple( std::vector<C3DPoint> &	 points,
        int v0, int v1, int v2, Vector3f & orig, Vector3f & dir)
{
    C3DPoint V0 = points[v0];
    C3DPoint V1 = points[v1];
    C3DPoint V2 = points[v2];

  //  bool rayTriangleIntersect(
  //  const Vec3f &orig, const Vec3f &dir,
  //  const Vec3f &v0, const Vec3f &v1, const Vec3f &v2,
  //  float &t, float &u, float &v)
  //  {

    /// Vec3f v0v1 = v1 - v0;
    Vector3f v0v1;
    v0v1.x = V1._x - V0._x;
    v0v1.y = V1._y - V0._y;
    v0v1.z = V1._z - V0._z;

    /// Vec3f v0v2 = v2 - v0;
    Vector3f v0v2;
    v0v2.x = V2._x - V0._x;
    v0v2.y = V2._y - V0._y;
    v0v2.z = V2._z - V0._z;

    /// Vec3f pvec = dir.crossProduct(v0v2);
    Vector3f pvec;
    pvec.x = dir.y * v0v2.z - dir.z * v0v2.y;
    pvec.y = dir.z * v0v2.x - dir.x * v0v2.z;
    pvec.z = dir.x * v0v2.y - dir.y * v0v2.x;

    ///  float det = v0v1.dotProduct(pvec);
    float det = ( v0v1.x * pvec.x + v0v1.y * pvec.y  + v0v1.z * pvec.z);

    // ray and triangle are parallel if det is close to 0
    if((det > -cEPSILON) && (det < cEPSILON)) return false;
    float invDet = 1.0 / det;

    /// Vec3f tvec = orig - v0;
    Vector3f tvec;
    tvec.x = orig.x - V0._x;
    tvec.y = orig.y - V0._y;
    tvec.z = orig.z - V0._z;

    float u = ( tvec.x * pvec.x + tvec.y * pvec.y + tvec.z * pvec.z ) * invDet;
    if ((u < 0.) || (u > 1.)) return false;

    // Vec3f qvec = tvec.crossProduct(v0v1);
    Vector3f qvec;
    qvec.x = tvec.y * v0v1.z - tvec.z * v0v1.y;
    qvec.y = tvec.z * v0v1.x - tvec.x * v0v1.z;
    qvec.z = tvec.x * v0v1.y - tvec.y * v0v1.x;


    float v = (dir.x * qvec.x + dir.y * qvec.y + dir.z * qvec.z ) * invDet;
    if ((v < 0.) || ((u + v) > 1.)) return false;

    float t = (v0v2.x * qvec.x + v0v2.y * qvec.y + v0v2.z * qvec.z) * invDet;

    if(t > cEPSILON) return true;

    // No hit, no win
    return false;
}

inline bool Moeller_Trumbore_triangle_intersection ( const Vector3f & V1,  const Vector3f  & V2, const  Vector3f & V3,
                                                     const Vector3f & camera_origin, const Vector3f & camera_normal)  __attribute__((always_inline));
inline bool Moeller_Trumbore_triangle_intersection ( const Vector3f & V1, const  Vector3f & V2, const  Vector3f & V3,
                                                     const Vector3f & camera_origin, const Vector3f & camera_normal)
{

    // Find vectors for two edges sharing V1
    // SUB(e1, V2, V1);
    Vector3f e1;
    e1.x = V2.x - V1.x;
    e1.y = V2.y - V1.y;
    e1.z = V2.z - V1.z;

    // SUB(e2, V3, V1);
    Vector3f e2;
    e2.x = V3.x - V1.x;
    e2.y = V3.y - V1.y;
    e2.z = V3.z - V1.z;


    // Begin calculating determinant - also used to calculate u parameter
    // CROSS(P, D, e2);
    Vector3f P;
    P.x = camera_normal.y * e2.z - camera_normal.z * e2.y;
    P.y = camera_normal.z * e2.x - camera_normal.x * e2.z;
    P.z = camera_normal.x * e2.y - camera_normal.y * e2.x;

    // if determinant is near zero, ray lies in plane of triangle
    // float det = DOT(e1, P);
    float det = e1.x * P.x + e1.y * P.y + e1.z * P.z;

    //NOT CULLING
    if((det > -cEPSILON) && (det < cEPSILON)) return false;
    float inv_det = 1.f / det;

    // calculate distance from V1 to ray origin
    // SUB(T, O, V1);
    Vector3f T;
    T.x = camera_origin.x - V1.x;
    T.y = camera_origin.y - V1.y;
    T.z = camera_origin.z - V1.z;

    //Calculate u parameter and test bound
    // float u = DOT(T, P) * inv_det;
    float u = (T.x * P.x + T.y * P.y + T.z * P.z) * inv_det;

    // The intersection lies outside of the triangle
    if((u < 0.f) || (u > 1.f)) return false;

    // Prepare to test v parameter
    // CROSS(Q, T, e1);
    Vector3f Q;
    Q.x = T.y * e1.z - T.z * e1.y;
    Q.y = T.z * e1.x - T.x * e1.z;
    Q.z = T.x * e1.y - T.y * e1.x;

    // Calculate V parameter and test bound
    // float v = DOT(D, Q) * inv_det;
    float v = (camera_normal.x * Q.x +
               camera_normal.y * Q.y +
               camera_normal.z * Q.z) * inv_det;

    // The intersection lies outside of the triangle
    if( (v < 0.f) || (u + v  > 1.f)) return false;

    // float t = DOT(e2, Q) * inv_det;
    float t = (e2.x * Q.x + e2.y * Q.y + e2.z * Q.z) * inv_det;

    // ray intersection
   if(t > cEPSILON)
            return true;
    // No hit, no win
    return false;
}

void compute_mesh_points_visability( CollisionModel3D* collision_model,
                                    std::vector<C3DPoint> &	 points,
                                    std::vector<C3DPointIdx> & triangles,
                                    Vector3f & camera_origin,
                                    Vector3f & camera_normal,
                                    std::vector<bool> & points_vis,
                                    std::vector<Vector3f> & points_col)
{

    float _angle_threshold = -cos(BGM(88.0));

    // compute triangle distance to camera
    /* std::vector<float> tri_dist(triangles.size());
    std::vector<Vector3f> tri_pnts;
    std::vector<C3DPointIdx>::iterator itt = triangles.begin();
    for( int t = 0; itt != triangles.end(); itt++, t++ )
    {
        C3DPoint pnt = points[ itt->pnt_index[0] ];
        tri_dist[t] = std::sqrt((pnt._x - camera_origin.x) * (pnt._x - camera_origin.x) +
                                (pnt._y - camera_origin.y) * (pnt._y - camera_origin.y) +
                                (pnt._z - camera_origin.z) * (pnt._z - camera_origin.z) );


        Vector3f p0;
        p0.x = pnt._x;
        p0.y = pnt._y;
        p0.z = pnt._z;
        tri_pnts.push_back(p0);

        C3DPoint pnt1 = points[ itt->pnt_index[1] ];
        Vector3f p1;
        p1.x = pnt1._x;
        p1.y = pnt1._y;
        p1.z = pnt1._z;
        tri_pnts.push_back(p1);

        C3DPoint pnt2 = points[ itt->pnt_index[2] ];
        Vector3f p2;
        p2.x = pnt2._x;
        p2.y = pnt2._y;
        p2.z = pnt2._z;
        tri_pnts.push_back(p2);
    }*/


     float test_origin[3];
     float test_direction[3];

    std::vector<C3DPoint>::iterator vit = points.begin();
    for( int vidx = 0; vidx < points.size() ; vit++, vidx++)
    {

        /////////////////////////////////////////////////////////
        /// step 1 back face culling - skip point with wrong normal
        /////////////////////////////////////////////////////////
        C3DPoint pnt =  *vit;
        float pnlen = std::sqrt(pnt._nx * pnt._nx + pnt._ny * pnt._ny + pnt._nz * pnt._nz);

        ///if(pnlen == 0) std::cout << "wrong normal" << std::endl;

        float cos_alpha = (pnt._nx * camera_normal.x +
                           pnt._ny * camera_normal.y +
                           pnt._nz * camera_normal.z)/pnlen;

         if(cos_alpha > _angle_threshold)
            continue;

        float dist_op = std::sqrt((pnt._x - camera_origin.x) * (pnt._x - camera_origin.x) +
                                  (pnt._y - camera_origin.y) * (pnt._y - camera_origin.y) +
                                  (pnt._z - camera_origin.z) * (pnt._z - camera_origin.z) );

        /////////////////////////////////////////////////////////
        /// step 2 Collision test
        /////////////////////////////////////////////////////////
        Vector3f view_direction;
        view_direction.x = -pnt._x + camera_origin.x ;
        view_direction.y = -pnt._y + camera_origin.y ;
        view_direction.z = -pnt._z + camera_origin.z ;
         float vlen = std::sqrt (view_direction.x * view_direction.x  +
                                 view_direction.y * view_direction.y  +
                                 view_direction.z * view_direction.z  );
        view_direction.x /= vlen;
        view_direction.y /= vlen;
        view_direction.z /= vlen;

        int intsc_count = 0;
        //bool is_visible = true;
        //bool cont_loop = true;

        test_origin[0] = pnt._x + 0.01 * view_direction.x;
        test_origin[1] = pnt._y + 0.01 * view_direction.y;
        test_origin[2] = pnt._z + 0.01 * view_direction.z;

        test_direction[0] = view_direction.x;
        test_direction[1] = view_direction.y;
        test_direction[2] = view_direction.z;

        bool collide  = collision_model->rayCollision(test_origin, test_direction);
        if(collide) continue;
        points_vis[vidx] = true;
        /// std::cout << vidx << " " << collide << std::endl;

    }

        //std::vector<C3DPointIdx>::iterator itt = triangles.begin();
        /*#pragma omp parallel for num_threads(4)
        for(int t =0 ; (t < triangles.size()) ; t++)
        {
            // if found intersection - skip
            if(!cont_loop) continue;

            // if this triangle is too far - skip
            if(tri_dist[t]> dist_op)
                continue;

            // if the current point in this triangle - skip
            if( (triangles[t].pnt_index[0] == vidx) ||
                (triangles[t].pnt_index[1] == vidx) ||
                (triangles[t].pnt_index[2] == vidx) )
                 continue;

            // skip triangle with wrong normal
            C3DPoint tri_normal =  triangles[t].tri_normal;

             float nlen = std::sqrt(tri_normal._x * tri_normal._x +
                                   tri_normal._y * tri_normal._y +
                                   tri_normal._z * tri_normal._z);

            float cos_alpha = (tri_normal._x * camera_normal.x +
                               tri_normal._y * camera_normal.y +
                               tri_normal._z * camera_normal.z)/nlen;

            if(cos_alpha > _angle_threshold)
                continue;


            bool intersect = Moeller_Trumbore_triangle_intersection( tri_pnts[t*3],
                    tri_pnts[t*3+1], tri_pnts[t*3+2], camera_origin, view_direction);

            if(intersect)
            {
                is_visible = false;
                #pragma omp critical
                {
                    intsc_count++;
                    cont_loop = false;
                }
            }
        }


        if(collide)
        {
            points_vis[vidx] = false;
            intsc_count = 1;
        }
        else
            points_vis[vidx] = true;

        if(intsc_count == 0)
        {
            points_col[vidx].x = 1;
            points_col[vidx].y = 1;
            points_col[vidx].z = 1;
        }
        else if(intsc_count == 1)
        {
            points_col[vidx].x = 1;
            points_col[vidx].y = 0;
            points_col[vidx].z = 0;
        }
*/


}

void update_best_point_view(int frame_idx,
                            Vector3f & camera_origin,
                            Vector3f & camera_normal,
                            int xs,
                            int ys,
                            cv::Mat & image,
                            cv::Mat & camLK_P,
                            std::vector<C3DPoint> &	 points,
                            std::vector<bool>     & points_vis,
                            std::vector<int>      & points_bview,
                            std::vector<float> & points_bview_meas,
                            std::vector<Vector3f> & points_col)
{
    std::cout << "update_best_point_view " << std::endl;

    std::vector<C3DPoint>::iterator vit = points.begin();
    for( int vidx = 0; vidx < points.size() ; vit++, vidx++)
    {
        C3DPoint pnt =  *vit;
        float w_total = -1;
        if(points_vis[vidx])
        {
            float w_ori = - (camera_normal.x * pnt._nx +
                             camera_normal.y * pnt._ny +
                             camera_normal.z * pnt._nz);


            ///std::cout << w_ori << " " << pnt._nx << " " << camera_normal.x   << std::endl;

            float dist = std::sqrt( (camera_origin.x - pnt._x)*(camera_origin.x - pnt._x) +
                                    (camera_origin.y - pnt._y)*(camera_origin.y - pnt._y) +
                                    (camera_origin.z - pnt._z)*(camera_origin.z - pnt._z));
            float w_res = 1./ dist;


            /// compute projection
            float px = (pnt._x - CGlobalHSSettings::glob_gl_offx)/   CGlobalHSSettings::glob_gl_scale;
            float py = (pnt._y - CGlobalHSSettings::glob_gl_offy)/ (-CGlobalHSSettings::glob_gl_scale);
            float pz = (pnt._z - CGlobalHSSettings::glob_gl_offz)/ (-CGlobalHSSettings::glob_gl_scale);

            cv::Mat pp_mat = (cv::Mat_<double>(4,1) << px, py, pz,1);
            cv::Mat uv = camLK_P * pp_mat;
            uv  /= uv.at<double>(2,0);

            float uf = uv.at<double>(0,0);
            float vf = uv.at<double>(0,0);
            int ui = uv.at<double>(0,0);
            int vi = uv.at<double>(1,0);

            Vector3f true_color;

            float w_dis = 0;
            if((ui >= 0) && (ui < xs) && (vi >= 0) && (vi < ys))
            {
                w_dis = std::sqrt (1. - std::max(std::abs(-1. + 2.*float(ui)/float(xs)),
                                                 std::abs(-1. + 2.*float(vi)/float(ys))));

                cv::Vec3b rgbv = image.at<cv::Vec3b>(vi,ui);
                true_color.x = float(rgbv.val[2]) / 256.0;
                true_color.y = float(rgbv.val[1]) / 256.0;
                true_color.z = float(rgbv.val[0]) / 256.0;
            }


            w_total =  w_ori * w_res * w_dis;

            /// std::cout << ui << " " << vi << "; " << w_ori << " " <<w_res << " " << w_dis << " "<<w_total << std::endl;

           if(points_bview_meas[vidx] < w_total)
            {
                points_bview_meas[vidx] = w_total;
                points_bview[vidx] = frame_idx;
                points_col[vidx] = true_color;
            }

        }
    }
}

std::vector<int> CTexturingThread::assign_mesh_points_with_truth_color(std::vector<Vector3f> & points_col)
{

    CTriMesh * m = _mesh->get_tri_mesh();

    /////////////////////////////////////////////////////////////////////
    ///// construct collision structure
    /////////////////////////////////////////////////////////////////////
    CollisionModel3D* collision_model = newCollisionModel3D();

    float aux_v[9];
    std::vector<C3DPointIdx>::iterator itt = m->_triangles.begin();
    int t = 0;
    for( ; itt != m->_triangles.end(); itt++, t++ )
    {

            C3DPoint pnt0 = m->_points[ itt->pnt_index[0] ];
            aux_v[0] = pnt0._x;
            aux_v[1] = pnt0._y;
            aux_v[2] = pnt0._z;


            C3DPoint pnt1 = m->_points[ itt->pnt_index[1] ];
            aux_v[3] = pnt1._x;
            aux_v[4] = pnt1._y;
            aux_v[5] = pnt1._z;

            C3DPoint pnt2 = m->_points[ itt->pnt_index[2] ];
            aux_v[6] = pnt2._x;
            aux_v[7] = pnt2._y;
            aux_v[8] = pnt2._z;

            collision_model ->addTriangle(&aux_v[0],&aux_v[3],&aux_v[6]);


    }
    collision_model->finalize();
    std::cout << "Collision model is constructed with " << t << " triangles" << std::endl;
    /////////////////////////////////////////////////////////////////////

    int frames = global_keyframes_sequence.size();
    std::vector<int> points_bview(m->_points.size(),not_visited);

#if 1
    std::vector<float> points_bview_meas(m->_points.size(),0);

    std::vector<CFrame *>::iterator itf = global_keyframes_sequence.begin();
    for(int i= 0; itf != global_keyframes_sequence.end() && (i < frames ) ; itf+=_frame_step , i+=_frame_step )
    {


        if (_abort)
        {
            emit send_back(0);
            return;
        }
        // (*itf)->load_tex_from_video_hd();
        (*itf)->load_L_images_from_hd();

        int xs = (*itf)->_img_L.cols;
        int ys = (*itf)->_img_L.rows;

        std::cout << "frame " << i << " " << xs << " " << ys << std::endl;

        ///////////// novel camera normal code
        double theta_xp =  -(*itf)->_glob_rot_vec.at<double>(0,0);
        double theta_yp =   (*itf)->_glob_rot_vec.at<double>(0,1);
        double theta_zp =   (*itf)->_glob_rot_vec.at<double>(0,2);

        cv::Mat rotXp = (cv::Mat_<double>(3,1) << theta_xp, 0, 0);
        cv::Mat rotYp = (cv::Mat_<double>(3,1) << 0, theta_yp, 0);
        cv::Mat rotZp = (cv::Mat_<double>(3,1) << 0, 0, theta_zp);
        cv::Mat rotM_Xp, rotM_Yp, rotM_Zp;
        cv::Rodrigues(rotXp, rotM_Xp);
        cv::Rodrigues(rotYp, rotM_Yp);
        cv::Rodrigues(rotZp, rotM_Zp);

        cv::Mat camRot =  rotM_Zp * rotM_Yp *  rotM_Xp ;


        cv::Mat unitVec = (cv::Mat_<double>(3,1) << 0, 0, -1);
        cv::Mat rotUnitVec = camRot * unitVec;
        ////std::cout << "rot vexrot " << rotUnitVec << std::endl;

        Vector3f camera_normal;
        camera_normal.x = rotUnitVec.at<double>(0,0);
        camera_normal.y = rotUnitVec.at<double>(1,0);
        camera_normal.z = rotUnitVec.at<double>(2,0);

        /////////////////////////////////////

        cv::Mat forwR = (*itf)->get_global_rotation_matrix_forw();



        float cam_x = (*itf)->_global_pos.at<double>(0,0);
        float cam_y = (*itf)->_global_pos.at<double>(1,0);
        float cam_z = (*itf)->_global_pos.at<double>(2,0);

        Vector3f camera_origin;
        camera_origin.x =   cam_x * CGlobalHSSettings::glob_gl_scale + CGlobalHSSettings::glob_gl_offx;
        camera_origin.y = - cam_y * CGlobalHSSettings::glob_gl_scale + CGlobalHSSettings::glob_gl_offy;
        camera_origin.z = - cam_z * CGlobalHSSettings::glob_gl_scale + CGlobalHSSettings::glob_gl_offz;

        cv::Mat poseT = -forwR * (*itf)->_global_pos;
        cv::Mat P = (cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2), poseT.at<double>(0,0) ,
                                              forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2), poseT.at<double>(1,0) ,
                                              forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2), poseT.at<double>(2,0));

        cv::Mat camLK_P = CGlobalHSSettings::_camL_K * P;

        std::vector<bool> points_vis(m->_points.size(),false);
        /// should be ok
        compute_mesh_points_visability(collision_model,
                                       m->_points,
                                       m->_triangles,
                                       camera_origin,
                                       camera_normal, points_vis, points_col);
        /*Vector3f vis_col;
        vis_col.x = 0;
        vis_col.y = 1;
        vis_col.z = 0;

        Vector3f non_vis_col;
        non_vis_col.x = 1;
        non_vis_col.y = 0;
        non_vis_col.z = 1;

        std::vector<C3DPoint>::iterator vit = m->_points.begin();
        for( int vidx = 0; vidx < m->_points.size() ; vit++, vidx++)
        {

            if(points_vis[vidx])
                 points_col[vidx] = vis_col;
            else
                 points_col[vidx] = non_vis_col;

        }*/

      update_best_point_view(i, camera_origin, camera_normal,
                               xs, ys,(*itf)->_img_L, camLK_P, m->_points,
                                points_vis, points_bview, points_bview_meas,
                               points_col);


        (*itf)->dump_unused_data();
        emit send_back(float(i+1)*100/float(frames));


    }


    //////////////// dump computed data
    /*std::ofstream ofile;
    ofile.open ("data_color.raw", std::ios::out| std::ios::binary);
    for( int vidx = 0; vidx < m->_points.size() ; vidx++)
    {
        // std::vector<int> points_bview
        // std::vector<Vector3f> & points_col

        int cbview = points_bview[vidx];
        ofile.write(reinterpret_cast<const char *>(&cbview), sizeof(int));

        Vector3f cpnt = points_col[vidx];
        ofile.write(reinterpret_cast<const char *>(&cpnt.x), sizeof(float));
        ofile.write(reinterpret_cast<const char *>(&cpnt.y), sizeof(float));
        ofile.write(reinterpret_cast<const char *>(&cpnt.z), sizeof(float));
    }
    ofile.close();*/

  return points_bview;
#else

    std::ifstream ifile("data_color.raw", std::ios::in | std::ios::binary);
    for( int vidx = 0; vidx < m->_points.size() ; vidx++)
    {

        // std::vector<Vector3f> & points_col
        int cbview = -1;
        ifile.read(reinterpret_cast<char *>(&cbview), sizeof(int));
        points_bview[vidx] = cbview;


        Vector3f cpnt;
        ifile.read(reinterpret_cast<char *>(&cpnt.x), sizeof(float));
        ifile.read(reinterpret_cast<char *>(&cpnt.y), sizeof(float));
        ifile.read(reinterpret_cast<char *>(&cpnt.z), sizeof(float));
        points_col[vidx] = cpnt;
    }
    ifile.close();

    return points_bview;
#endif
/*
srand(time(NULL));
    std::vector<Vector3f> pcolors(global_keyframes_sequence.size());
    for(int i= 0; i < global_keyframes_sequence.size() ; i++)
    {

        Vector3f color;
        color.x = float(rand() % 100)/99.0 ;
        color.y = float(rand() % 100)/99.0 ;
        color.z = float(rand() % 100)/99.0 ;
        pcolors[i] = color;
    }

    std::vector<C3DPoint>::iterator vit = m->_points.begin();

  for( int vidx = 0; vidx < m->_points.size() ; vit++, vidx++)
    {
         if(points_bview[vidx] != not_visited)
             points_col[vidx] = pcolors[points_bview[vidx] ];
       /// if(points_vis[vidx])
       ///     points_col[vidx] = vis_col;
       /// else
        ///     points_col[vidx] = non_vis_col;

    }

 return points_bview;*/
}

inline float compute_color_constraint_red(int vidx, float color, std::vector<Vector3f> & g, std::vector<Vector3f> & f,
                                      std::vector<C3DPoint> &  points, std::vector<C3DPointIdx>  & triangles,
                                      std::vector<int> & pvis) __attribute__((always_inline));

inline float compute_color_constraint_red(int vidx, float color, std::vector<Vector3f> & g, std::vector<Vector3f> & f,
                               std::vector<C3DPoint> &	 points, std::vector<C3DPointIdx>  & triangles,
                                      std::vector<int> & pvis)
{
    float alpha = 100.0;
    float betta = 0.001;

    float sum_grad = 0;
    float sum_neig = 0;
    float sum_chan = (color - f[vidx].x) *(color - f[vidx].x);

    std::vector<int>::iterator itf = points[vidx]._faceidx.begin();
    for( ; itf != points[vidx]._faceidx.end(); itf++)
    {
        C3DPointIdx ctt = triangles[*itf];

        // if one of the corners is bad - skip triangle
        if( pvis[ctt.pnt_index[0]] == not_visited ||
            pvis[ctt.pnt_index[1]] == not_visited ||
            pvis[ctt.pnt_index[2]] == not_visited )
            continue;

        int vidx_left = vidx;
        int vidx_right = vidx;
        if(ctt.pnt_index[0] == vidx)
        {
          vidx_left = ctt.pnt_index[1];
          vidx_right = ctt.pnt_index[2];
        }

        if(ctt.pnt_index[1] == vidx)
        {
           vidx_left = ctt.pnt_index[0];
           vidx_right = ctt.pnt_index[2];
        }

        if(ctt.pnt_index[2] == vidx)
        {
           vidx_left  = ctt.pnt_index[0];
           vidx_right = ctt.pnt_index[1];
        }

        float f_left  = f[vidx_left].x;
        float f_right = f[vidx_right].x;
        float g_left  = g[vidx_left].x;
        float g_right = g[vidx_right].x ;
        float f_vidx = f[vidx].x;

        sum_grad += ((g_left  - color) - (f_left  - f_vidx )) *
                    ((g_left  - color) - (f_left  - f_vidx )) +
                    ((g_right - color) - (f_right - f_vidx ))*
                    ((g_right - color) - (f_right - f_vidx ));

        sum_neig += (g_left  - color)  * (g_left  - color) +
                    (g_right - color)  * (g_right - color) +
                    (g_right - g_left) * (g_right - g_left);

        sum_chan += (g_left  - f_left ) * (g_left  - f_left ) +
                    (g_right - f_right) * (g_right - f_right);

    }
   return sum_grad  + alpha* sum_neig + betta * sum_chan;
}


inline float compute_color_constraint_green(int vidx, float color, std::vector<Vector3f> & g, std::vector<Vector3f> & f,
                                      std::vector<C3DPoint> &  points, std::vector<C3DPointIdx>  & triangles,
                                      std::vector<int> & pvis) __attribute__((always_inline));

inline float compute_color_constraint_green(int vidx, float color, std::vector<Vector3f> & g, std::vector<Vector3f> & f,
                               std::vector<C3DPoint> &	 points, std::vector<C3DPointIdx>  & triangles,
                                      std::vector<int> & pvis)
{
    float alpha = 100.0;
    float betta = 0.001;

    float sum_grad = 0;
    float sum_neig = 0;
    float sum_chan = (color - f[vidx].y) *(color - f[vidx].y);

    std::vector<int>::iterator itf = points[vidx]._faceidx.begin();
    for( ; itf != points[vidx]._faceidx.end(); itf++)
    {
        C3DPointIdx ctt = triangles[*itf];

        // if one of the corners is bad - skip triangle
        if( pvis[ctt.pnt_index[0]] == not_visited ||
            pvis[ctt.pnt_index[1]] == not_visited ||
            pvis[ctt.pnt_index[2]] == not_visited )
            continue;

        int vidx_left = vidx;
        int vidx_right = vidx;
        if(ctt.pnt_index[0] == vidx)
        {
          vidx_left = ctt.pnt_index[1];
          vidx_right = ctt.pnt_index[2];
        }

        if(ctt.pnt_index[1] == vidx)
        {
           vidx_left = ctt.pnt_index[0];
           vidx_right = ctt.pnt_index[2];
        }

        if(ctt.pnt_index[2] == vidx)
        {
           vidx_left  = ctt.pnt_index[0];
           vidx_right = ctt.pnt_index[1];
        }

        float f_left  = f[vidx_left].y;
        float f_right = f[vidx_right].y;
        float g_left  = g[vidx_left].y;
        float g_right = g[vidx_right].y ;
        float f_vidx = f[vidx].y;

        sum_grad += ((g_left  - color) - (f_left  - f_vidx )) *
                    ((g_left  - color) - (f_left  - f_vidx )) +
                    ((g_right - color) - (f_right - f_vidx ))*
                    ((g_right - color) - (f_right - f_vidx ));

        sum_neig += (g_left  - color)  * (g_left  - color) +
                    (g_right - color)  * (g_right - color) +
                    (g_right - g_left) * (g_right - g_left);

        sum_chan += (g_left  - f_left ) * (g_left  - f_left ) +
                    (g_right - f_right) * (g_right - f_right);

    }
   return sum_grad  + alpha* sum_neig + betta * sum_chan;
}


inline float compute_color_constraint_blue(int vidx, float color, std::vector<Vector3f> & g, std::vector<Vector3f> & f,
                                      std::vector<C3DPoint> &  points, std::vector<C3DPointIdx>  & triangles,
                                      std::vector<int> & pvis) __attribute__((always_inline));

inline float compute_color_constraint_blue(int vidx, float color, std::vector<Vector3f> & g, std::vector<Vector3f> & f,
                               std::vector<C3DPoint> &	 points, std::vector<C3DPointIdx>  & triangles,
                                      std::vector<int> & pvis)
{
    float alpha = 100.0;
    float betta = 0.001;

    float sum_grad = 0;
    float sum_neig = 0;
    float sum_chan = (color - f[vidx].z) *(color - f[vidx].z);

    std::vector<int>::iterator itf = points[vidx]._faceidx.begin();
    for( ; itf != points[vidx]._faceidx.end(); itf++)
    {
        C3DPointIdx ctt = triangles[*itf];

        // if one of the corners is bad - skip triangle
        if( pvis[ctt.pnt_index[0]] == not_visited ||
            pvis[ctt.pnt_index[1]] == not_visited ||
            pvis[ctt.pnt_index[2]] == not_visited )
            continue;

        int vidx_left = vidx;
        int vidx_right = vidx;
        if(ctt.pnt_index[0] == vidx)
        {
          vidx_left = ctt.pnt_index[1];
          vidx_right = ctt.pnt_index[2];
        }

        if(ctt.pnt_index[1] == vidx)
        {
           vidx_left = ctt.pnt_index[0];
           vidx_right = ctt.pnt_index[2];
        }

        if(ctt.pnt_index[2] == vidx)
        {
           vidx_left  = ctt.pnt_index[0];
           vidx_right = ctt.pnt_index[1];
        }

        float f_left  = f[vidx_left].z;
        float f_right = f[vidx_right].z;
        float g_left  = g[vidx_left].z;
        float g_right = g[vidx_right].z ;
        float f_vidx = f[vidx].z;

        sum_grad += ((g_left  - color) - (f_left  - f_vidx )) *
                    ((g_left  - color) - (f_left  - f_vidx )) +
                    ((g_right - color) - (f_right - f_vidx ))*
                    ((g_right - color) - (f_right - f_vidx ));

        sum_neig += (g_left  - color)  * (g_left  - color) +
                    (g_right - color)  * (g_right - color) +
                    (g_right - g_left) * (g_right - g_left);

        sum_chan += (g_left  - f_left ) * (g_left  - f_left ) +
                    (g_right - f_right) * (g_right - f_right);

    }
   return sum_grad  + alpha* sum_neig + betta * sum_chan;
}


void CTexturingThread::global_color_adjustment(std::vector<Vector3f> & pcolors, std::vector<int> & pvis)
{

    CTriMesh * m = _mesh->get_tri_mesh();

    std::vector<Vector3f> pcolors_o = pcolors;
    float color_step = 1./255.;
    int iteration_max = _max_color_adjust_iterations;
    for(int it = 0 ; it < iteration_max; it++)
    {
        if (_abort)
        {
            emit send_back(0);
            return;
        }

        emit send_back(float(it)*100/float(iteration_max));

        float glob_error_red = 0;
        float glob_error_green = 0;
        float glob_error_blue = 0;

        std::vector<Vector3f>::iterator vit = pcolors.begin();
        for( int vidx = 0; vit != pcolors.end(); vit++, vidx++ )
        {
            //// skip invisible point
            if(pvis[vidx] == not_visited)
                continue;

            /////////////////////
            float mp0_r = compute_color_constraint_red(vidx, pcolors[vidx].x, pcolors,
                                                   pcolors_o,  m->_points, m->_triangles, pvis);
            float mp1_r = compute_color_constraint_red(vidx, pcolors[vidx].x + color_step,
                                                   pcolors, pcolors_o,  m->_points, m->_triangles, pvis);
            float mm1_r = compute_color_constraint_red(vidx, pcolors[vidx].x - color_step,
                                                   pcolors, pcolors_o,  m->_points, m->_triangles, pvis);

            glob_error_red += mp0_r;

            float smallest_m_r = mm1_r;
            float delta_r = 0;
            if(mp1_r < mm1_r)
            {
                delta_r = color_step;
                smallest_m_r = mp1_r;
            }else
                delta_r = -color_step;

            if(smallest_m_r < mp0_r)
                pcolors[vidx].x +=  delta_r;
            if (pcolors[vidx].x < 0)
                    pcolors[vidx].x =0;
            if (pcolors[vidx].x > 1.)
                    pcolors[vidx].x = 1.;


            /////////////////////
            float mp0_g = compute_color_constraint_green(vidx, pcolors[vidx].y, pcolors,
                                                   pcolors_o,  m->_points, m->_triangles, pvis);
            float mp1_g = compute_color_constraint_green(vidx, pcolors[vidx].y + color_step,
                                                   pcolors, pcolors_o,  m->_points, m->_triangles, pvis);
            float mm1_g = compute_color_constraint_green(vidx, pcolors[vidx].y - color_step,
                                                   pcolors, pcolors_o,  m->_points, m->_triangles, pvis);

            glob_error_green += mp0_g;

            float smallest_m_g = mm1_g;
            float delta_g = 0;
            if(mp1_g < mm1_g)
            {
                delta_g = color_step;
                smallest_m_g = mp1_g;
            }else
                delta_g = -color_step;

            if(smallest_m_g < mp0_g)
                pcolors[vidx].y +=  delta_g;
            if (pcolors[vidx].y < 0)
                    pcolors[vidx].y =0;
            if (pcolors[vidx].y > 1.)
                    pcolors[vidx].y = 1.;

            /////////////////////
            float mp0_b = compute_color_constraint_blue(vidx, pcolors[vidx].z, pcolors,
                                                   pcolors_o,  m->_points, m->_triangles, pvis);
            float mp1_b = compute_color_constraint_blue(vidx, pcolors[vidx].z + color_step,
                                                   pcolors, pcolors_o,  m->_points, m->_triangles, pvis);
            float mm1_b = compute_color_constraint_blue(vidx, pcolors[vidx].z - color_step,
                                                   pcolors, pcolors_o,  m->_points, m->_triangles, pvis);

            glob_error_blue += mp0_b;

            float smallest_m_b = mm1_b;
            float delta_b = 0;
            if(mp1_b < mm1_b)
            {
                delta_b = color_step;
                smallest_m_b = mp1_b;
            }else
                delta_b = -color_step;

            if(smallest_m_b < mp0_b)
                pcolors[vidx].z +=  delta_b;
            if (pcolors[vidx].z < 0)
                    pcolors[vidx].z =0;
            if (pcolors[vidx].z > 1.)
                    pcolors[vidx].z = 1.;

        }

        std::cout << "glob color " << it << " " << glob_error_red  << " " << glob_error_green << " " << glob_error_blue << std::endl;
    }
}


/*void CTexturingThread::global_color_adjustment(std::vector<Vector3f> & pcolors, std::vector<int> & pvis)
{

    CTriMesh * m = _mesh->get_tri_mesh();

    std::vector<Vector3f> pcolors_o = pcolors;
    float color_step = 1./255.;
    int iteration_max = 3;
    for(int it = 0 ; it < iteration_max; it++)
    {
        for( int vidx = 0; vidx < pcolors.size();  vidx++)
        {

            //// skip invisible point
            if(pvis[vidx] == not_visited)
                continue;

            Vector3f pcol = pcolors_o[vidx];

            std::set<int> uniq_neighb;
            std::vector<int>::iterator itf =  m->_points[vidx]._faceidx.begin();
            for( ; itf !=  m->_points[vidx]._faceidx.end(); itf++)
            {
                C3DPointIdx ctt = m->_triangles[*itf];
                for(int p = 0; p < 3; p++)
                    if((ctt.pnt_index[p] != vidx) && (pvis[ctt.pnt_index[p]] != not_visited) )
                       uniq_neighb.insert(ctt.pnt_index[p]);
            }

            Vector3f pcol_res;

            float ncount = uniq_neighb.size();
            std::set<int>::iterator its = uniq_neighb.begin();
            for( ; its != uniq_neighb.end(); its++)
            {
                pcol_res.x += pcolors_o[*its].x;
                pcol_res.y += pcolors_o[*its].y;
                pcol_res.z += pcolors_o[*its].z;
            }

            float weith = 0.5;

            if(ncount > 0)
            {
                pcol_res.x  /= ncount;
                pcol_res.y  /= ncount;
                pcol_res.z  /= ncount;
            }


             pcol_res.x  *= (1. - weith);
             pcol_res.y  *= (1. - weith);
             pcol_res.z  *= (1. - weith);

             pcol_res.x += weith * pcol.x;
             pcol_res.y += weith * pcol.y;
             pcol_res.z += weith * pcol.z;


            pcolors[vidx] = pcol_res;

        }
        pcolors_o = pcolors;
        std::cout << "glob color " << it <<   std::endl;
    }
}*/

void CTexturingThread::assign_triangles_to_view(std::vector<int> & frame_ref_pnt, std::vector<int> & frame_ref)
{
    int one_count = 0;
    int two_count = 0;
    int three_count =0;

    CTriMesh * m = _mesh->get_tri_mesh();
    std::vector<C3DPointIdx>::iterator itt = m->_triangles.begin();
    for(int t =0 ; itt != m->_triangles.end(); itt++, t++)
    {
        if (_abort)
        {
            emit send_back(0);
            return;
        }

        C3DPointIdx ctt = *itt;

        // if one of the corners is bad - skip triangle
        if( frame_ref_pnt[ctt.pnt_index[0]] == not_visited ||
            frame_ref_pnt[ctt.pnt_index[1]] == not_visited ||
            frame_ref_pnt[ctt.pnt_index[2]] == not_visited )
            continue;

        // all corners are the same
        if(frame_ref_pnt[ctt.pnt_index[0]] == frame_ref_pnt[ctt.pnt_index[1]] &&
           frame_ref_pnt[ctt.pnt_index[1]] == frame_ref_pnt[ctt.pnt_index[2]] )
        {
            three_count++;
            frame_ref[t] = frame_ref_pnt[ctt.pnt_index[0]];
        }
        else if(frame_ref_pnt[ctt.pnt_index[0]] == frame_ref_pnt[ctt.pnt_index[1]])
        {

            two_count++;
            frame_ref[t] = frame_ref_pnt[ctt.pnt_index[0]];
        }
        else if (frame_ref_pnt[ctt.pnt_index[1]] == frame_ref_pnt[ctt.pnt_index[2]])
        {

            two_count++;
            frame_ref[t] = frame_ref_pnt[ctt.pnt_index[1]];
        }
        else if (frame_ref_pnt[ctt.pnt_index[2]] == frame_ref_pnt[ctt.pnt_index[0]])
        {
            two_count++;
            frame_ref[t] = frame_ref_pnt[ctt.pnt_index[2]];
        }
        else
        {
            one_count++;
            frame_ref[t] = frame_ref_pnt[ctt.pnt_index[0]];
        }

    }

    std::cout <<  m->_triangles.size() << " " << three_count<< " " << two_count << " "<< one_count << " "  << std::endl;
    std::cout << "assign_triangles_to_view" << std::endl;
}

void CTexturingThread::contruct_individual_face_groups(std::vector<int> & frame_ref,
                                                  std::vector< CTrianglesGroup*> & face_groups)
{
    CTriMesh * m = _mesh->get_tri_mesh();
    _tri_mesh = m;
    _frame_ref  = frame_ref;
    _group_ref = std::vector<CTrianglesGroup*>(m->_triangles.size(), NULL);

     int total_face_count = 0;

     std::vector<C3DPointIdx>::iterator itt = m->_triangles.begin();
     for(int t =0 ; itt != m->_triangles.end(); itt++, t++)
     {
         if (_abort)
         {
             emit send_back(0);
             return;
         }
         // have we seen this face before
         if(_group_ref[t] != NULL)
            continue;

         // skip triangles without proper projection
         //if(frame_ref[t] == not_visited)
         //    continue;

         std::list<int> face_group;

         std::list<int> face_group_current;
         face_group_current.push_back(t);

         std::list<int> face_group_next;

         CTrianglesGroup * curr_group = new CTrianglesGroup;
         curr_group->_frame =(frame_ref[t] != not_visited)? global_keyframes_sequence[frame_ref[t]]:NULL;
         curr_group->_mesh = m;

         while(face_group_current.size() > 0)
         {
            construct_face_group(face_group_current,
                                 face_group_next,
                                 frame_ref[t],
                                 curr_group,
                                 face_group);

            face_group_current = face_group_next;
            face_group_next.clear();

         }

         total_face_count += face_group.size();
         curr_group->_face_group = face_group;
         face_groups.push_back(curr_group);
         emit send_back(float(t+1)*100/float(m->_triangles.size()));
     }
     std::cout << "total triangles grouped " << total_face_count << " vs " <<  m->_triangles.size() << std::endl;
}

void CTexturingThread::merge_small_face_groups(std::vector< CTrianglesGroup*> & face_groups)
{
    std::sort (face_groups.begin(), face_groups.end(), cmp_face_groups_asc);

    int largest_group_sz = (*(face_groups.end()-1) )->_face_group.size();
    int group_sz_threashold =  float(largest_group_sz)*_merge_group_thres ;

    int progress = 0;
    int init_groups_count  = face_groups.size();
    CTrianglesGroup * curr_group = *face_groups.begin();
    while( curr_group->_face_group.size() < group_sz_threashold  )
    {
        if (_abort)
        {
            emit send_back(0);
            return;
        }
           std::cout << curr_group << " " << curr_group->_face_group.size() << " " << face_groups.size() << std::endl;
           face_groups.erase(face_groups.begin());
           merge_with_largest_neighbour(curr_group, face_groups);

           curr_group = *face_groups.begin();

           progress++;
           emit send_back(float(progress)*100/float(init_groups_count));
    }
}

void CTexturingThread::compose_individual_textures(std::vector< CTrianglesGroup*> & face_groups, std::vector<Vector3f> & points_col)
{
    std::sort (face_groups.begin(), face_groups.end(), cmp_face_groups_desc);
    std::vector< CTrianglesGroup*>::iterator itfg = face_groups.begin();
    for(int h = 0 ; itfg != face_groups.end(); itfg++, h++)
    {

       if (_abort)
       {
            emit send_back(0);
            return;
       }

       std::cout << "group count " << h << std::endl;
       CTrianglesGroup* current_group = *itfg;
       /// current_group ->construct_texture();
      if(current_group->_frame == NULL) continue;

       current_group ->construct_texture_color_correction(points_col);
       current_group->compute_tex_uv();
    }

}

void CTexturingThread::construct_texture_atlas(std::vector< CTrianglesGroup*> & face_groups)
{
    std::sort (face_groups.begin(), face_groups.end(), cmp_face_groups_desc_tex);

    int init_texture_width  = 2048;
    int init_texture_height = 2048;
    int current_tex_x = 0;
    int current_tex_y = 0;
    int line_height = 0;
    QImage * texture_atlas = new QImage(init_texture_width, init_texture_height, QImage::Format_RGB32);
    texture_atlas->fill(Qt::white);

    std::vector< CTrianglesGroup*>::iterator itfg = face_groups.begin();
    for( ; itfg != face_groups.end(); itfg++)
    {

        QImage * current_tex = (*itfg)->_texture;
        if(current_tex == NULL) continue;

        std::cout << "tex coord " << current_tex_x << " " << current_tex_y << " " <<  current_tex->width() << " " << current_tex->height() << " " << std::endl;
        /// if we starting a new line
        if(current_tex_x == 0)
            line_height = current_tex->height();

        /// check if there is enough space
        bool width_ok = false;
        if(( current_tex_x + current_tex->width() ) < texture_atlas->width())
            width_ok = true;
        if(!width_ok)
        {
            current_tex_x =0;
            current_tex_y += line_height;
            line_height = current_tex->height();
        }

        bool height_ok = false;
        if(( current_tex_y + current_tex->height() ) < texture_atlas->height())
            height_ok = true;
        if(!height_ok)
        {
            // create larger image
            init_texture_height += 1024;
            QImage * n_texture_atlas = new QImage(init_texture_width, init_texture_height, QImage::Format_RGB32);
            n_texture_atlas->fill(Qt::white);

            QPainter painter(n_texture_atlas);
            painter.drawImage(QPoint(0, 0), *texture_atlas );
            painter.end();

            delete texture_atlas;
            texture_atlas = n_texture_atlas;
        }


        /// copy texture to final image
        QPainter painter(texture_atlas);
        painter.drawImage(QPoint(current_tex_x, current_tex_y), *current_tex );
        painter.end();

        /// update corners for uv
        (*itfg)->_texture_pos_x = current_tex_x;
        (*itfg)->_texture_pos_y = current_tex_y;

        current_tex_x += current_tex->width();
    }

    /////////////////////  recompute uvs ////////////////////////////////
    itfg = face_groups.begin();
    for( ; itfg != face_groups.end(); itfg++)
          (*itfg)->recompute_uv(texture_atlas);

    QImage mimg = texture_atlas->mirrored(false,true);
    mimg.save("texture_atlas.png");

    QImage * final_tatlase = new QImage(mimg);
    _mesh->get_tri_mesh()->replace_texture(final_tatlase);
    delete texture_atlas;
}

void CTexturingThread::clean_intermediate_data(std::vector< CTrianglesGroup*> & face_groups)
{
    _frame_ref.clear();
    _group_ref.clear();

     std::vector<CTrianglesGroup*>::iterator itgr =  face_groups.begin();
     for(; itgr != face_groups.end(); itgr++)
     {
         if(*itgr != NULL)
             delete *itgr;
     }
     face_groups.clear();
}

void CTexturingThread::construct_and_add_new_cframe(
        std::vector<CFrame *> & mp, float rxc, float ryc,
        QString & fname, int frame_idx,
        float sca_angle, float camera_x_incline)
{
    std::cout << "compose " << fname.toStdString() << std::endl;
    // what about y inclination ???
    CFrame * cframe = new CFrame ;

    cframe->_imgL_str = fname;
    cframe->_id = frame_idx;

    float c_scan_angle = sca_angle * float(frame_idx);
    float c_rmat_00 =  cos(c_scan_angle);
    float c_rmat_01 = -sin(c_scan_angle);
    float c_rmat_10 =  sin(c_scan_angle);
    float c_rmat_11 =  cos(c_scan_angle);

    float cam_x = 0 - rxc;
    float cam_y = 0 - ryc;
    float cam_z = 0;


    float cam_nx = cam_x * c_rmat_00 + cam_y * c_rmat_01;
    float cam_ny = cam_x * c_rmat_10 + cam_y* c_rmat_11;
    float cam_nz = cam_z;


    cframe->_global_pos     = (cv::Mat_<double>(3,1) << (cam_nx + rxc), -cam_nz, (cam_ny + ryc));
    cframe->_glob_rot_vec   = (cv::Mat_<double>(1,3) <<  -camera_x_incline, c_scan_angle, 0);
    cframe->_valid_coord    = true;


    mp.push_back(cframe);
}

void CTexturingThread::compose_global_frame_map()
{
    QFileInfo info_fname(_inif);
    QSettings settings(_inif, QSettings::IniFormat);

    float camera_x_incline = -BGM(settings.value(ini_camera_x_incline).toFloat());
    float camera_y_incline = -BGM(settings.value(ini_camera_y_incline).toFloat());
    float rot_center_x = settings.value(ini_rot_center_x).toFloat();
    float rot_center_y = settings.value(ini_rot_center_y).toFloat();


    QString path = info_fname.absolutePath();
    QString texture_img_fmt =  settings.value(ini_texture_img_fmt).toString();
    int fmax = settings.value(ini_texture_img_cnts).toInt();


    float sca_angle = BGM(360)/ float(fmax);
    std::vector<CFrame *> mp;
    for(int f = 0; f < fmax ; f++)
    {
        QString img_name = QString(texture_img_fmt).arg(QString::number(f));
        img_name = path + img_name;
        construct_and_add_new_cframe(mp, rot_center_x, rot_center_y, img_name, f, sca_angle, camera_x_incline);
    }

    /// FIXIT
    global_keyframes_sequence = mp;
}

void CTexturingThread::run()
{

    compose_global_frame_map();

    CTriMesh * m = _mesh->get_tri_mesh();

    std::vector<Vector3f> points_col(m->_points.size());
    std::vector<int> frame_ref_pnt = assign_mesh_points_with_truth_color(points_col);

    if (_abort)
    {
        emit send_back(0);
        return;
    }

    global_color_adjustment(points_col, frame_ref_pnt);

    if (_abort)
    {
        emit send_back(0);
        return;
    }
    /////////////////////////////////////////////////////////////
    /////// visualize truth colors of points

    /*std::vector<C3DPoint>::iterator vit = m->_points.begin();
    for( int vidx = 0; vit != m->_points.end(); vit++, vidx++ )
    {
        (*vit)._r = points_col[vidx].x;
        (*vit)._g = points_col[vidx].y;
        (*vit)._b = points_col[vidx].z;
    }

    /// first thread
    _mesh->remake_glList();
     std::cout << "CTexturingThread::run()" << std::endl;
    return;*/
    //////////////////////////////////////////////////////////////

    std::vector<int> frame_ref(m->_triangles.size(), not_visited);
    assign_triangles_to_view(frame_ref_pnt, frame_ref);

    if (_abort)
    {
        emit send_back(0);
        return;
    }
    ///////////////////////////////////////
    // fill it with random colors
    /*int frames = global_keyframes_sequence.size();
    std::vector<Vector3f> tri_colors(frames);
    std::vector<CFrame *>::iterator itf = global_keyframes_sequence.begin();

    srand(time(NULL));
    for(int i= 0; itf != global_keyframes_sequence.end() ; itf++, i++)
    {

        Vector3f color;
        color.x = float(rand() % 100)/99.0 ;
        color.y = float(rand() % 100)/99.0 ;
        color.z = float(rand() % 100)/99.0 ;
        tri_colors[i] = color;
    }
    std::vector<C3DPointIdx>::iterator itt = m->_triangles.begin();
    for(int t =0 ; itt != m->_triangles.end(); itt++, t++)
    {

        if(frame_ref[t] ==  not_visited)
        {
            // color black and continue

            m->_points[ itt->pnt_index[0] ]._r  = 1;
            m->_points[ itt->pnt_index[0] ]._g  = 0;
            m->_points[ itt->pnt_index[0] ]._b  = 0;

            m->_points[ itt->pnt_index[1] ]._r  = 1;
            m->_points[ itt->pnt_index[1] ]._g  = 0;
            m->_points[ itt->pnt_index[1] ]._b  = 0;

            m->_points[ itt->pnt_index[2] ]._r  = 1;
            m->_points[ itt->pnt_index[2] ]._g  = 0;
            m->_points[ itt->pnt_index[2] ]._b  = 0;


           /// std::cout << "nv " <<  normal_cos[t] << std::endl;
            continue;
        }

        ///std::cout << "vd " <<  normal_cos[t] << std::endl;
        m->_points[ itt->pnt_index[0] ]._r  = tri_colors[frame_ref[t]].x;
        m->_points[ itt->pnt_index[0] ]._g  = tri_colors[frame_ref[t]].y;
        m->_points[ itt->pnt_index[0] ]._b  = tri_colors[frame_ref[t]].z;

        m->_points[ itt->pnt_index[1] ]._r  = tri_colors[frame_ref[t]].x;
        m->_points[ itt->pnt_index[1] ]._g  = tri_colors[frame_ref[t]].y;
        m->_points[ itt->pnt_index[1] ]._b  = tri_colors[frame_ref[t]].z;

        m->_points[ itt->pnt_index[2] ]._r  = tri_colors[frame_ref[t]].x;
        m->_points[ itt->pnt_index[2] ]._g  = tri_colors[frame_ref[t]].y;
        m->_points[ itt->pnt_index[2] ]._b  = tri_colors[frame_ref[t]].z;
    }
    _mesh->remake_glList();
    return; */
    ////////////////////////////////////////

    std::vector< CTrianglesGroup*> face_groups;
    contruct_individual_face_groups(frame_ref, face_groups);
    std::cout << "found " << face_groups.size() << " groups " << std::endl;

    if (_abort)
    {
        emit send_back(0);
        return;
    }
    ////////////////////////////////////////

     merge_small_face_groups(face_groups);
    std::cout << "merged " << face_groups.size() << " groups " << std::endl;

    if (_abort)
    {
        emit send_back(0);
        return;
    }
    ////////////////////////////////////////
    /*srand(time(NULL));
    std::vector<Vector3f> tri_colors(face_groups.size());
     for(int i= 0; i < face_groups.size() ; i++)
     {

         Vector3f color;
         color.x = float(rand() % 100)/99.0 ;
         color.y = float(rand() % 100)/99.0 ;
         color.z = float(rand() % 100)/99.0 ;
         tri_colors[i] = color;
     }

     std::vector< CTrianglesGroup*>::iterator itg = face_groups.begin();
     for( int g = 0; itg != face_groups.end(); itg++, g++ )
     {

         std::list<int>::iterator itt = (*itg)->_face_group.begin();
        for( ; itt != (*itg)->_face_group.end(); itt++ )
        {
            C3DPointIdx f = m->_triangles[*itt];

            m->_points[ f.pnt_index[0] ]._r  = tri_colors[g].x;
            m->_points[ f.pnt_index[0] ]._g  = tri_colors[g].y;
            m->_points[ f.pnt_index[0] ]._b  = tri_colors[g].z;

            m->_points[ f.pnt_index[1] ]._r  = tri_colors[g].x;
            m->_points[ f.pnt_index[1] ]._g  = tri_colors[g].y;
            m->_points[ f.pnt_index[1] ]._b  = tri_colors[g].z;

            m->_points[ f.pnt_index[2] ]._r  = tri_colors[g].x;
            m->_points[ f.pnt_index[2] ]._g  = tri_colors[g].y;
            m->_points[ f.pnt_index[2] ]._b  = tri_colors[g].z;
        }


     }
     _mesh->remake_glList();
     return;*/
     ////////////////////////////////////////


    compose_individual_textures(face_groups, points_col);

    if (_abort)
    {
        emit send_back(0);
        return;
    }



     construct_texture_atlas(face_groups);

    if (_abort)
    {
        emit send_back(0);
        return;
    }
    clean_intermediate_data(face_groups);

    if (_abort)
    {
        emit send_back(0);
        return;
    }
    _mesh->set_rendering_mode(CTriangularMesh::MESH_TEX);
    emit send_result();
    emit send_back(100);

}

void CTexturingThread::run_old()
{
    std::cout << "started  texturing" << std::endl;

    ////// algorithm parameters
    float _angle_threshold = -cos(80.0 * cPI/180 );
    
    /////////////////////////////////////////////////////////////////////////////
    ///////////////////// phase 1. assign each triangle to a particular image
    //////////////////////////////////////////////////////////////////////////////
    std::cout << "phase 1" << std::endl;

    CTriMesh * m = _mesh->get_tri_mesh();
    int frames = global_keyframes_sequence.size();

    std::vector<float>   normal_cos(m->_triangles.size());
    std::vector<int> frame_ref(m->_triangles.size(), not_visited);

    std::vector<CFrame *>::iterator itf = global_keyframes_sequence.begin();
    for(int i= 0; itf != global_keyframes_sequence.end() && (i < frames); itf+=_frame_step , i+=_frame_step )
    {

        std::cout << "processing " << (*itf)->_imgL_str.toStdString() << " " << (*itf)->_id << std::endl;
        (*itf)->load_LR_images_from_hd();
        cv::Mat forwR = (*itf)->get_global_rotation_matrix_forw();

        cv::Mat unitVec = (cv::Mat_<double>(3,1) << 0, 0, -1);
        cv::Mat rotUnitVec = forwR * unitVec;
        float cam_nx = rotUnitVec.at<double>(0,0);
        float cam_ny = rotUnitVec.at<double>(1,0);
        float cam_nz = rotUnitVec.at<double>(2,0);

        std::vector<C3DPointIdx>::iterator itt = m->_triangles.begin();
        for(int t =0 ; itt != m->_triangles.end(); itt++, t++)
        {
            C3DPoint tri_normal =  itt->tri_normal;

            float nlen = std::sqrt(tri_normal._x * tri_normal._x +
                                   tri_normal._y * tri_normal._y +
                                   tri_normal._z * tri_normal._z);


            float cos_alpha = (tri_normal._x * cam_nx +
                               tri_normal._y * cam_ny +
                               tri_normal._z * cam_nz)/nlen;

            if(cos_alpha > _angle_threshold)
                continue; // nothing to do

            if(frame_ref[t] ==  not_visited)
            {
                normal_cos[t] = cos_alpha;
                frame_ref[t] = i;
            }
            else
            {
                if(normal_cos[t] > cos_alpha)
                {
                    normal_cos[t] = cos_alpha;
                    frame_ref[t] = i;
                }
            }
        }

        (*itf)->dump_unused_data();
        emit send_back(float(i+1)*100/float(frames));
    }
    normal_cos.clear();

    /*
    // fill it with random colors
    std::vector<Vector3f> tri_colors(frames);
    itf = global_keyframes_sequence.begin();
    for(int i= 0; itf != global_keyframes_sequence.end() ; itf++, i++)
    {

        Vector3f color;
        color.x = float(rand() % 100)/99.0 ;
        color.y = float(rand() % 100)/99.0 ;
        color.z = float(rand() % 100)/99.0 ;
        tri_colors[i] = color;
    }
    std::vector<C3DPointIdx>::iterator itt = m->_triangles.begin();
    for(int t =0 ; itt != m->_triangles.end(); itt++, t++)
    {

        if(frame_ref[t] ==  not_visited)
        {
            // color black and continue

            m->_points[ itt->pnt_index[0] ]._r  = 0;
            m->_points[ itt->pnt_index[0] ]._g  = 0;
            m->_points[ itt->pnt_index[0] ]._b  = 0;

            m->_points[ itt->pnt_index[1] ]._r  = 0;
            m->_points[ itt->pnt_index[1] ]._g  = 0;
            m->_points[ itt->pnt_index[1] ]._b  = 0;

            m->_points[ itt->pnt_index[2] ]._r  = 0;
            m->_points[ itt->pnt_index[2] ]._g  = 0;
            m->_points[ itt->pnt_index[2] ]._b  = 0;


           /// std::cout << "nv " <<  normal_cos[t] << std::endl;
            continue;
        }

        ///std::cout << "vd " <<  normal_cos[t] << std::endl;
        m->_points[ itt->pnt_index[0] ]._r  = tri_colors[frame_ref[t]].x;
        m->_points[ itt->pnt_index[0] ]._g  = tri_colors[frame_ref[t]].y;
        m->_points[ itt->pnt_index[0] ]._b  = tri_colors[frame_ref[t]].z;

        m->_points[ itt->pnt_index[1] ]._r  = tri_colors[frame_ref[t]].x;
        m->_points[ itt->pnt_index[1] ]._g  = tri_colors[frame_ref[t]].y;
        m->_points[ itt->pnt_index[1] ]._b  = tri_colors[frame_ref[t]].z;

        m->_points[ itt->pnt_index[2] ]._r  = tri_colors[frame_ref[t]].x;
        m->_points[ itt->pnt_index[2] ]._g  = tri_colors[frame_ref[t]].y;
        m->_points[ itt->pnt_index[2] ]._b  = tri_colors[frame_ref[t]].z;


    }
    */


    /////////////////////////////////////////////////////////////////////////////
    ///////////////////// phase 2. construct face groups
    //////////////////////////////////////////////////////////////////////////////
    std::cout << "phase 2 " << std::endl;

    _tri_mesh = m;
    _frame_ref  = frame_ref;
    _group_ref = std::vector<CTrianglesGroup*>(m->_triangles.size(), NULL);

     int total_face_count = 0;


     std::vector< CTrianglesGroup*> face_groups;

     std::vector<C3DPointIdx>::iterator itt = m->_triangles.begin();
     for(int t =0 ; itt != m->_triangles.end(); itt++, t++)
     {
         // have we seen this face before
         if(_group_ref[t] != NULL)
            continue;

         // skip triangles without proper projection
         if(frame_ref[t] == not_visited)
             continue;

         std::list<int> face_group;

         std::list<int> face_group_current;
         face_group_current.push_back(t);

         std::list<int> face_group_next;

         CTrianglesGroup * curr_group = new CTrianglesGroup;
         curr_group->_frame = global_keyframes_sequence[frame_ref[t]];
         curr_group->_mesh = m;

      ////  std::cout <<"gr frame ref" << curr_group->_frame->_global_pos.t() << std::endl;

         while(face_group_current.size() > 0)
         {
            construct_face_group(face_group_current,
                                 face_group_next,
                                 frame_ref[t],
                                 curr_group,
                                 face_group);

            face_group_current = face_group_next;
            face_group_next.clear();

         }

         ///if(face_group.size() > 1)
          ///  std::cout << current_group_idx << " " << face_group.size() << std::endl;
         ///current_group_idx++;
         total_face_count += face_group.size();

         curr_group->_face_group = face_group;

         face_groups.push_back(curr_group);

          emit send_back(float(t+1)*100/float(m->_triangles.size()));
     }
     std::cout << "total triangles grouped " << total_face_count << " vs " <<  m->_triangles.size() << std::endl;


    /////////////////////////////////////////////////////////////////////////////
    ///////////////////// phase 3. merge small groups ///////////////////////////
    /////////////////////////////////////////////////////////////////////////////
     std::cout << "phase 3 " << std::endl;

     std::sort (face_groups.begin(), face_groups.end(), cmp_face_groups_asc);

     int largest_group_sz = (*(face_groups.end()-1) )->_face_group.size();
     int group_sz_threashold =  float(largest_group_sz)*_merge_group_thres ;

     int progress = 0;
     int init_groups_count  = face_groups.size();
     CTrianglesGroup * curr_group = *face_groups.begin();
     while( curr_group->_face_group.size() < group_sz_threashold  )
     {
            std::cout << curr_group << " " << curr_group->_face_group.size() << " " << face_groups.size() << std::endl;
            face_groups.erase(face_groups.begin());
            merge_with_largest_neighbour(curr_group, face_groups);

            curr_group = *face_groups.begin();

            progress++;
            emit send_back(float(progress)*100/float(init_groups_count));
     }


     /////////////////////////////////////////////////////////////////////////////
     ///////////////////// phase 4. compose individual textures //////////////////
     /////////////////////////////////////////////////////////////////////////////

     std::cout << "phase 4 " << std::endl;
    std::sort (face_groups.begin(), face_groups.end(), cmp_face_groups_desc);
    std::vector< CTrianglesGroup*>::iterator itfg = face_groups.begin();
    for(int h = 0 ; itfg != face_groups.end(); itfg++, h++)
    {
        std::cout << "group count " << h << std::endl;
        CTrianglesGroup* current_group = *itfg;
        current_group ->construct_texture();
        current_group->compute_tex_uv();
    }


    /////////////////////////////////////////////////////////////////////////////
    ///////////////////// phase 4a. fix boundary colors - globally //////////////
    /////////////////////////////////////////////////////////////////////////////

    /// 4a1 - compute mid brightness for each texture considering boundary pix only
 std::sort (face_groups.begin(), face_groups.end(), cmp_face_groups_desc_texarea);


    itfg = face_groups.begin();
    for(int h = 0 ; itfg != face_groups.end(); itfg++, h++)
    {
        CTrianglesGroup* current_group = *itfg;
        current_group->construct_bound_groups(_group_ref);
    }

     itfg = face_groups.begin();
    for(int h = 0 ; itfg != face_groups.end(); itfg++, h++)
    {
        CTrianglesGroup* current_group = *itfg;
        current_group->make_texcolor_correction();

    }

    /////////////////////////////////////////////////////////////////////////////
    ///////////////////// phase 5. compose texture atlas ////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    std::sort (face_groups.begin(), face_groups.end(), cmp_face_groups_desc_tex);


    std::cout << "phase 5 " << std::endl;


    int init_texture_width  = 2048;
    int init_texture_height = 2048;
    int current_tex_x = 0;
    int current_tex_y = 0;
    int line_height = 0;
    QImage * texture_atlas = new QImage(init_texture_width, init_texture_height, QImage::Format_RGB32);
    texture_atlas->fill(Qt::white);

    itfg = face_groups.begin();
    for( ; itfg != face_groups.end(); itfg++)
    {
        QImage * current_tex = (*itfg)->_texture;

        std::cout << "tex coord " << current_tex_x << " " << current_tex_y << " " <<  current_tex->width() << " " << current_tex->height() << " " << std::endl;
        /// if we starting a new line
        if(current_tex_x == 0)
            line_height = current_tex->height();

        /// check if there is enough space
        bool width_ok = false;
        if(( current_tex_x + current_tex->width() ) < texture_atlas->width())
            width_ok = true;
        if(!width_ok)
        {
            current_tex_x =0;
            current_tex_y += line_height;
            line_height = current_tex->height();
        }

        bool height_ok = false;
        if(( current_tex_y + current_tex->height() ) < texture_atlas->height())
            height_ok = true;
        if(!height_ok)
        {
            // create larger image
            init_texture_height += 1024;
            QImage * n_texture_atlas = new QImage(init_texture_width, init_texture_height, QImage::Format_RGB32);
            n_texture_atlas->fill(Qt::white);

            QPainter painter(n_texture_atlas);
            painter.drawImage(QPoint(0, 0), *texture_atlas );
            painter.end();

            delete texture_atlas;
            texture_atlas = n_texture_atlas;
        }


        /// copy texture to final image
        QPainter painter(texture_atlas);
        painter.drawImage(QPoint(current_tex_x, current_tex_y), *current_tex );
        painter.end();

        /// update corners for uv
        (*itfg)->_texture_pos_x = current_tex_x;
        (*itfg)->_texture_pos_y = current_tex_y;

        current_tex_x += current_tex->width();
    }

    /////////////////////////////////////////////////////////////////////////////
    ///////////////////// phase 6. recompute uvs ////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    itfg = face_groups.begin();
    for( ; itfg != face_groups.end(); itfg++)
          (*itfg)->recompute_uv(texture_atlas);

    QImage mimg = texture_atlas->mirrored(false,true);
    mimg.save("texture_atlas.png");

    QImage * final_tatlase = new QImage(mimg);
    m->replace_texture(final_tatlase);
    delete texture_atlas;




    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    /*
     std::vector<Vector3f> tri_colors(face_groups.size());
     for(int i= 0; i < face_groups.size() ; i++)
     {

         Vector3f color;
         color.x = float(rand() % 100)/99.0 ;
         color.y = float(rand() % 100)/99.0 ;
         color.z = float(rand() % 100)/99.0 ;
         tri_colors[i] = color;
     }

     std::vector< CTrianglesGroup*>::iterator itg = face_groups.begin();
     for( int g = 0; itg != face_groups.end(); itg++, g++ )
     {

         std::list<int>::iterator itt = (*itg)->_face_group.begin();
        for( ; itt != (*itg)->_face_group.end(); itt++ )
        {
            C3DPointIdx f = m->_triangles[*itt];

            m->_points[ f.pnt_index[0] ]._r  = tri_colors[g].x;
            m->_points[ f.pnt_index[0] ]._g  = tri_colors[g].y;
            m->_points[ f.pnt_index[0] ]._b  = tri_colors[g].z;

            m->_points[ f.pnt_index[1] ]._r  = tri_colors[g].x;
            m->_points[ f.pnt_index[1] ]._g  = tri_colors[g].y;
            m->_points[ f.pnt_index[1] ]._b  = tri_colors[g].z;

            m->_points[ f.pnt_index[2] ]._r  = tri_colors[g].x;
            m->_points[ f.pnt_index[2] ]._g  = tri_colors[g].y;
            m->_points[ f.pnt_index[2] ]._b  = tri_colors[g].z;
        }

     }*/

     //////////////////////////////////////////////////////////
     //////////////////////////////////////////////////////////

    std::cout << "end texturing" << std::endl;

    emit send_result();
    emit send_back(100);

    /// clean all intermediate data

    _frame_ref.clear();
    _group_ref.clear();

     std::vector<CTrianglesGroup*>::iterator itgr =  face_groups.begin();
     for(; itgr != face_groups.end(); itgr++)
     {
         if(*itgr != NULL)
             delete *itgr;
     }
     face_groups.clear();

}

/*
void CTexturingThread::run()
{
    std::cout << "started  texturing" << std::endl;

    CTriMesh * m = _mesh->get_tri_mesh();

    int frames = global_keyframes_sequence.size();
    std::vector<CFrame *>::iterator itf = global_keyframes_sequence.begin();

    float _angle_threshold = -cos(80.0 * cPI/180 );


    const float not_visited = -1;
     std::vector<float> vert_dist(m->_points.size(), not_visited);


    for(int i= 0; itf != global_keyframes_sequence.end() ; itf++, i++)
    {
        std::cout << "frame prcoessing : " << i << std::endl;
        (*itf)->load_LR_images_from_hd();

        cv::Mat forwR = (*itf)->get_global_rotation_matrix_forw();


        std::cout << (*itf)->_imgL_str.toStdString() << std::endl;
        //std::cout << (*itf)->_glob_rot_vec  << std::endl;
        //std::cout << (*itf)->_global_pos.t() << std::endl;

        float cam_x = (*itf)->_global_pos.at<double>(0,0);
        float cam_y = (*itf)->_global_pos.at<double>(1,0);
        float cam_z = (*itf)->_global_pos.at<double>(2,0);

        cv::Mat poseT = -forwR * (*itf)->_global_pos;
        cv::Mat P = (cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2), poseT.at<double>(0,0) ,
                                              forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2), poseT.at<double>(1,0) ,
                                              forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2), poseT.at<double>(2,0));

        cv::Mat camLK_P = CGlobalHSSettings::_camL_K * P;
        int xs = (*itf)->_img_L.cols;
        int ys = (*itf)->_img_L.rows;

        // compute camera vertex
        cv::Mat unitVec = (cv::Mat_<double>(3,1) << 0, 0, -1);
        cv::Mat rotUnitVec = forwR * unitVec;
        float cam_nx = rotUnitVec.at<double>(0,0);
        float cam_ny = rotUnitVec.at<double>(1,0);
        float cam_nz = rotUnitVec.at<double>(2,0);

        std::vector<C3DPoint>::iterator vit = m->_points.begin();
        for( int vidx = 0; vit != m->_points.end(); vit++, vidx++ )
        {
            /// normal test
            /// bool is_visible = false;
            float nlen = std::sqrt((*vit)._nx*(*vit)._nx + (*vit)._ny*(*vit)._ny + (*vit)._nz*(*vit)._nz);
            float cos_alpha = ((*vit)._nx * cam_nx + (*vit)._ny * cam_ny + (*vit)._nz * cam_nz)/nlen;
            if(cos_alpha > _angle_threshold)
                continue; // colinear normals - nothing to do

            float px = ((*vit)._x - CGlobalHSSettings::glob_gl_offx)/   CGlobalHSSettings::glob_gl_scale;
            float py = ((*vit)._y - CGlobalHSSettings::glob_gl_offy)/ (-CGlobalHSSettings::glob_gl_scale);
            float pz = ((*vit)._z - CGlobalHSSettings::glob_gl_offz)/ (-CGlobalHSSettings::glob_gl_scale);

            /// distance test
            float old_dist  = vert_dist[vidx];
            float cdist = (cam_x - px) * (cam_x - px) + (cam_y - py) * (cam_y - py) + (cam_z - pz) * (cam_z - pz);
            if((old_dist != not_visited) && cdist > old_dist)
                continue;
            vert_dist[vidx] = cdist;


            /// get 3d point in pcl1
            cv::Mat pp_mat = (cv::Mat_<double>(4,1) << px, py, pz,1);
            cv::Mat uv = camLK_P * pp_mat;
            uv  /= uv.at<double>(2,0);

            int ui = uv.at<double>(0,0);
            int vi = uv.at<double>(1,0);

            if((ui >= 0) && (ui < xs) && (vi >= 0) && (vi < ys))
            {
                cv::Vec3b rgbv = (*itf)->_img_L.at<cv::Vec3b>(vi,ui);
                (*vit)._r = float(rgbv.val[2]) / 256.0;
                (*vit)._g = float(rgbv.val[1]) / 256.0;
                (*vit)._b = float(rgbv.val[0]) / 256.0;
            }

        }

        (*itf)->dump_unused_data();


        emit send_back(float(i+1)*100/float(frames));
    }


    emit send_result();
    emit send_back(100);
}
*/
