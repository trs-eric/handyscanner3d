#ifndef CSCANRECONSTRUCTIONTHREAD_H
#define CSCANRECONSTRUCTIONTHREAD_H

#include <QThread>
#include <QMutex>
#include "graphicutils.h"
#include "crawtexture.h"

class CScanReconstructionThread : public QThread
{

    Q_OBJECT
public:
    explicit CScanReconstructionThread(QObject *parent = 0);
    ~CScanReconstructionThread();

    void start_process(bool useL, bool useR, int mmax, int mstep,
                       int pous, bool scan_tex , bool save_img, QString & spath);

signals:
    void send_back(const int val);
    void send_time_elapsed(QString str);
    void send_time_ETA(QString str);
    void send_result(std::vector<C3DPoint> * cpoints);
    void send_result_left(std::vector<C3DPoint> * cpoints);
    void send_result_right(std::vector<C3DPoint> * cpoints);

    void send_tex_left (CRawTexture * tex);
    void send_tex_right(CRawTexture * tex);

    void send_clean_result();
    void send_finished();

public slots:
    void stop_process();

protected:
    void run();

    QImage * snap_raw_img_from_device(unsigned char *);
    // void snap_raw_img_from_device_fast(unsigned char * buffer, QImage *& img);
    void snap_raw_img_from_device_fast(unsigned char * buffer, QImage *& img);

    void save_image(QImage * img, QString fname);


    enum eMotorStep {STEP_MOTOR_0=0, STEP_MOTOR_1=1, STEP_MOTOR_2=2, STEP_MOTOR_3=3,
                    STEP_MOTOR_4=4, STEP_MOTOR_5=5, STEP_MOTOR_6=6, STEP_MOTOR_7=7};

    eMotorStep _cur_step;

    void release_motor();
    void make_motor_step();
    void make_motor_step_8();

    void make_motor_step_8_fast(int mmax, int mstep);

private:
    bool    _abort;
    QMutex  _mutex;

    bool _use_left;
    bool _use_right;

    int _mpmax;
    int _mstep;
    int _pause;
    bool _scan_tex;

    bool _save_img;
    QString _saving_path;
};

#endif // CSCANRECONSTRUCTIONTHREAD_H
