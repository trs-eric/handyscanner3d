#include "cmeshconstructionthread.h"
#include "poisson/MultiGridOctest.h"
#include "ctriangularmesh.h"
////#include "cnormalcloud.h"
/// #include "normals/normal_utils.h"
///#include "meshconstructionutils.h"
///#include "cframe.h"


CMeshConstructionThread::CMeshConstructionThread(QObject *parent) :
    QThread(parent), ///_normals(NULL),
    _octree_d(0), _solvdiv(0),
    _sampl_per_node(0), _surf_offset(0), _dpcl(NULL)
{
    _abort = false;
}

CMeshConstructionThread::~CMeshConstructionThread()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
    wait();
}


void CMeshConstructionThread::start_process(int octdepth, int solvdiv, float sampl_per_node, float surf_offset,
                                            std::vector<C3DPoint> * dpcl)
{
    _abort = false;

    _octree_d   = octdepth;
    _solvdiv    = solvdiv;
    _sampl_per_node = sampl_per_node;
    _surf_offset    = surf_offset;
    _dpcl = dpcl;


    start();
}

void CMeshConstructionThread::stop_process()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
}

void CMeshConstructionThread::emit_send_back(int i)
{
     emit send_back(i);
}
/*

bool normalMeshlabCB(const int pos, const char * str )
{
    myThread->emit_send_back(pos);
}*/

void CMeshConstructionThread::run()
{

    //// phase 3. construct resulting mesh
    CTriMesh * rmesh = NULL;
    CTriangularMesh * mmesh = NULL;

   try
   {

    /// set size for those two
    std::vector<Point3D<float> > Pts(_dpcl->size());
    std::vector<Point3D<float> > Nor(_dpcl->size());;

    std::vector<C3DPoint>::iterator it = _dpcl->begin();
    for( int i = 0; it != _dpcl->end(); it++, i++)
    {

        Point3D<float> point;
        point.coords[0] = it->_x;
        point.coords[1] = it->_y;
        point.coords[2] = it->_z;

        Point3D<float> normal;
        normal.coords[0] = it->_nx;
        normal.coords[1] = it->_ny;
        normal.coords[2] = it->_nz;

        Pts[i] = point;
        Nor[i] = normal;
    }

    PoissonParam pparam;
    pparam.Depth			= _octree_d;
    pparam.SolverDivide		= _solvdiv;
    pparam.SamplesPerNode	= _sampl_per_node;
    pparam.Offset			= _surf_offset;

    CoredVectorMeshData mesh;
    Point3D<float> center;
    float scale;

    bool failed = false;
    int ret= Execute2(pparam, Pts, Nor, mesh, center, scale, this, &failed);
    if(failed)
    {
        emit send_back(0);
        return;
    }

    rmesh = new CTriMesh ;

    mesh.resetIterator();
    int outcore_pnts_count = mesh.outOfCorePointCount() ;
    int intcore_pnts_count = mesh.inCorePoints.size();
    int vm = outcore_pnts_count + intcore_pnts_count ;
    int fm = mesh.triangleCount();

    Point3D<float> p;
    for (int i=0; i < intcore_pnts_count; i++)
    {
        p = mesh.inCorePoints[i];

        C3DPoint xpp;
        xpp._x = p.coords[0]*scale+center.coords[0];
        xpp._y = p.coords[1]*scale+center.coords[1];
        xpp._z = p.coords[2]*scale+center.coords[2];
        rmesh->_points.push_back(xpp);

    }

    for (int ii=0; ii <outcore_pnts_count; ii++)
    {
        mesh.nextOutOfCorePoint(p);

        C3DPoint xpp;
        xpp._x = p.coords[0]*scale+center.coords[0];
        xpp._y = p.coords[1]*scale+center.coords[1];
        xpp._z = p.coords[2]*scale+center.coords[2];
        rmesh->_points.push_back(xpp);
    }

    TriangleIndex tIndex;
    int inCoreFlag;
    for ( i = 0; i < fm; i++)
    {

        mesh.nextTriangle(tIndex,inCoreFlag);
        if(!(inCoreFlag & CoredMeshData::IN_CORE_FLAG[0]))
                tIndex.idx[0]+= intcore_pnts_count;
        if(!(inCoreFlag & CoredMeshData::IN_CORE_FLAG[1]))
                tIndex.idx[1]+=intcore_pnts_count;
        if(!(inCoreFlag & CoredMeshData::IN_CORE_FLAG[2]))
                tIndex.idx[2]+=intcore_pnts_count;

        C3DPointIdx tri;
        tri.pnt_index[0] = tIndex.idx[0];
        tri.pnt_index[1] = tIndex.idx[1];
        tri.pnt_index[2] = tIndex.idx[2];

        rmesh->_triangles.push_back(tri);
    }

    ///////// update mesh normals per face
    std::vector<C3DPointIdx>::iterator itt = rmesh->_triangles.begin();
    for(int fidx = 0 ; itt != rmesh->_triangles.end(); itt++, fidx++)
    {
        rmesh->_points[ itt->pnt_index[0] ]._faceidx.push_back(fidx);
        C3DPoint pnt0 = rmesh->_points[ itt->pnt_index[0] ];

        rmesh->_points[ itt->pnt_index[1] ]._faceidx.push_back(fidx);
        C3DPoint pnt1 = rmesh->_points[ itt->pnt_index[1] ];

        rmesh->_points[ itt->pnt_index[2] ]._faceidx.push_back(fidx);
        C3DPoint pnt2 = rmesh->_points[ itt->pnt_index[2] ];

        itt->tri_normal = compute_normal( pnt2, pnt1, pnt0);
    }

    //////// update mesh normals per vertec
    std::vector<C3DPoint>::iterator itv = rmesh->_points.begin();
    for(; itv != rmesh->_points.end(); itv++)
    {
        itv->_nx =0;
        itv->_ny =0;
        itv->_nz =0;

        std::vector<int>::iterator fit = itv->_faceidx.begin();
        for( ; fit != itv->_faceidx.end(); fit++)
        {

            itv->_nx += rmesh->_triangles[(*fit)].tri_normal._x;
            itv->_ny += rmesh->_triangles[(*fit)].tri_normal._y;
            itv->_nz += rmesh->_triangles[(*fit)].tri_normal._z;
        }
        normalize3(itv->_nx, itv->_ny, itv->_nz);
    }
    std::cout << "resulting mesh: vertices " << rmesh->_points.size() << " faces " << rmesh->_triangles.size() << std::endl;

    mmesh = new CTriangularMesh;
    mmesh->set_mesh(rmesh);
    emit send_result(mmesh);
    emit send_back(100);

    }
    catch(...)
    {

        // the procedure fails
        if(rmesh) delete rmesh;
        if(mmesh) delete mmesh;
        emit send_result(NULL);
        emit send_back(0);
        return;
    }

}




