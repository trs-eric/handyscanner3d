#ifndef IOWAITDLG_H
#define IOWAITDLG_H

#include <QDialog>

namespace Ui {
class IOWaitDlg;
}

class IOWaitDlg : public QDialog
{
    Q_OBJECT

public:
    explicit IOWaitDlg(QWidget *parent = 0);
    ~IOWaitDlg();

    void set_caption(QString str);
private:
    Ui::IOWaitDlg *ui;
};

#endif // IOWAITDLG_H
