#ifndef CDIMREFERENCE_H
#define CDIMREFERENCE_H

class CDimReference
{
public:
    CDimReference();
    virtual ~CDimReference();

    virtual void move_forward   (float t = 0) = 0;
    virtual void move_backward  (float t = 0) = 0;
    virtual void move_left		(float t = 0) = 0;
    virtual void move_right		(float t = 0) = 0;
    virtual void move_up		(float t = 0) = 0;
    virtual void move_down		(float t = 0) = 0;
    virtual void turn_left		(float t = 0) = 0;
    virtual void turn_right		(float t = 0) = 0;
    virtual void turn_up		(float t = 0) = 0;
    virtual void turn_down		(float t = 0) = 0;

protected:

    // velocity turn (deg/sec)
    float _vturn;

    // velocity shift
    float _vshift;
};

#endif // CDIMREFERENCE_H
