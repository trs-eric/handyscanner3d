#include "cprojectio.h"

#include "cglwidget.h"

#include <iostream>

CProjectIO::CProjectIO(QObject *parent) :
    QObject(parent)
{
}

void CProjectIO::request_save(CGLWidget * w, QString fp)
{
    _w = w;
    _fp = fp;
    _mode = 1;
    emit save_requested();
}

void CProjectIO::request_load(CGLWidget * w, QString fp)
{
    _w = w;
    _fp = fp;
    _mode = 2;
    emit load_requested();
}

void CProjectIO::request_export(CGLWidget * w, QString fp)
{
    _w = w;
    _fp = fp;
    _mode = 3;
    emit export_requested();
}


void CProjectIO::do_io()
{
    bool res = false;
    switch(_mode)
    {
        case SAVE:
            _w->save_project(_fp);
            std::cout << "do save proj is completed with " << _w << " " << _fp.toStdString() << std::endl;
            emit finished_save_proj();
            break;
        case LOAD:
            try{
                res = _w->load_project(_fp);
            }
            catch(...)
            {
                res = false;
                std::cout << "do load proj is completed with " << _w << " " << _fp.toStdString() << std::endl;
            }
            emit finished_load_proj(res);
            break;
        case EXPORT:
            _w->export_scene(_fp);
            std::cout << "do export proj is completed with " << _w << " " << _fp.toStdString() << std::endl;
            emit finished_export();
            break;
    }


}
