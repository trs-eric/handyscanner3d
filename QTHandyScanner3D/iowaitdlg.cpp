#include "iowaitdlg.h"
#include "ui_iowaitdlg.h"

IOWaitDlg::IOWaitDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::IOWaitDlg)
{
    setWindowFlags(Qt::WindowStaysOnTopHint /*| Qt::FramelessWindowHint*/);


    ui->setupUi(this);


    QPixmap ic (":images/icons/rubitech.ico");
    ic = ic.scaledToWidth(ui->lblIco->width(),Qt::SmoothTransformation);

    ui->lblIco->setPixmap(ic);
    ui->lblText->setText("Please wait until IO-action is completed");
}


void IOWaitDlg::set_caption(QString str)
{
    ui->lblText->setText(str);
}

IOWaitDlg::~IOWaitDlg()
{
    delete ui;
}
