#include "cinputdatamanager.h"
#include <fstream>
#include "./settings/psettings.h"
#include <opencv2/highgui/highgui.hpp>
#include "graphicutils.h"

CInputDataManager::CInputDataManager()
{
    init_euler_angles_map();
}


void CInputDataManager::init_euler_angles_map()
{

    _euler_angles_map.clear();

    QString mapfile_str = CGlobalHSSettings::_current_data_path + CGlobalHSSettings::_rot_angles_file;
    std::ifstream infile(mapfile_str.toStdString().c_str());

    int idx = 0;
    float ax, ay, az;
    while (infile >> idx >> ax >> ay >> az )
    {
        _euler_angles_map [idx] = (cv::Mat_<double>(1,3) << BGM(-ax), BGM(ay),BGM(-az));
         //std::cout << idx << " " <<  ax << " " << ay << " " << az << std::endl;
    }
}



CFrame * CInputDataManager::get_frame_with_number(int idx)
{

    CFrame * cframe = new CFrame(idx);

    /// QString im_L_str  = CGlobalHSSettings::_current_data_path + CGlobalHSSettings::_left_img_prefix +
    ///                    QString("0%1.png").arg(idx, 3, 10, QLatin1Char('0'));
    /// cframe->_img_L = cv::imread(im_L_str.toStdString());
    ///QString im_R_str  = CGlobalHSSettings::_current_data_path + CGlobalHSSettings::_right_img_prefix +
    ///                    QString("0%1.png").arg(idx, 3, 10, QLatin1Char('0'));
    ///cframe->_img_R = cv::imread(im_R_str.toStdString() );


    cframe->_imgL_str = CGlobalHSSettings::_current_data_path + CGlobalHSSettings::_left_img_prefix +
            QString("0%1.png").arg(idx, 3, 10, QLatin1Char('0'));

    cframe->_imgR_str = CGlobalHSSettings::_current_data_path + CGlobalHSSettings::_right_img_prefix +
            QString("0%1.png").arg(idx, 3, 10, QLatin1Char('0'));

    cframe->load_LR_images_from_hd();
    cframe->_glob_rot_vec = _euler_angles_map[idx];

    return cframe;
}
