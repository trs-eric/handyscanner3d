#ifndef CSELECTPCLHELPDLG_H
#define CSELECTPCLHELPDLG_H

#include <QDialog>
class CGLWidget;

namespace Ui {
class CSelectPCLHelpDlg;
}

class CSelectPCLHelpDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CSelectPCLHelpDlg(CGLWidget * , QWidget *parent = 0);
    ~CSelectPCLHelpDlg();
    void hideEvent(QHideEvent * e);

private:
    Ui::CSelectPCLHelpDlg *ui;
    CGLWidget *_glWidget;
};

#endif // CSELECTPCLHELPDLG_H
