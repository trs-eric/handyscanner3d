#ifndef CDEVCONTROLTHREAD_H
#define CDEVCONTROLTHREAD_H

#include <QObject>
#include <QMutex>

#include "./driver/driver_aux_util.h"

class CDevControlThread : public QObject
{
    Q_OBJECT
public:
    explicit CDevControlThread(PVOID dh, QObject *parent = 0);

signals:
     void dev_motor_left();
     void dev_motor_right();
     void dev_turn_table();

     void finished();
     void finish_lasangle(int );
private:
    bool    _abort;
    QMutex  _mutex;

public slots:
    void run();
    void stop_process();
    void motor_left_request();
    void motor_right_request();
    void turn_table_request();


public:
    enum DevMode {NONE = 0, MOTOR_LEFT, MOTOR_RIGHT, TABLE_ROT};
    DevMode _mode;

protected:
    void go_motor_step();
    void go_table_rot();

    PVOID _dev_handle;

};

#endif // CDEVCONTROLTHREAD_H
