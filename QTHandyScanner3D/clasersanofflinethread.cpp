#include "clasersanofflinethread.h"
#include "cpointcloud.h"
#include "img2space/edgedetection.h"
#include "img2space/image2spacemodel.h"
#include <ctime>
#include <QMessageBox>

#include "ctriangularmesh.h"
#include "cframe.h"
#include <QFileInfo>
#include <QSettings>
#include "scene_ini_constants.h"
#include "normals/normal_utils.h"

#include "settings/psettings.h"
#include "opencv_aux_utils.h"
#include "scene_ini_constants.h"

CLaserSanOfflineThread::CLaserSanOfflineThread(QObject *parent) :
    QThread(parent)
{
    _abort = false;



}

CLaserSanOfflineThread::~CLaserSanOfflineThread()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
    wait();
}


void CLaserSanOfflineThread::start_process(QString & ininame,
                   int decim_pnt, bool usecorr)
{

    _abort = false;
    _use_corr = usecorr;


    QFileInfo info_fname(ininame);
    QSettings settings(ininame, QSettings::IniFormat);

    _path = info_fname.absolutePath();

    _video_name_format = settings.value(ini_video_name_format).toString();
    _video_desc_format = settings.value(ini_video_desc_format).toString();
    _video_name_has_scan = settings.value(ini_video_name_has_scan).toBool();

    _scan_first        = settings.value(ini_first_scan).toInt();
    _scan_last         = settings.value(ini_last_scan).toInt();

    _sca_angle = BGM(settings.value(ini_scan_angle_step).toFloat());


    _is_difference  = settings.value(ini_is_difference).toBool();


    _camera_laser_dist = settings.value(ini_camera_laser_dist).toFloat();
    _camera_x_incline = -BGM(settings.value(ini_camera_x_incline).toFloat());
    _camera_y_incline = -BGM(settings.value(ini_camera_y_incline).toFloat());
    _rot_center_x = settings.value(ini_rot_center_x).toFloat();
    _rot_center_y = settings.value(ini_rot_center_y).toFloat();
    _rot_center_z = settings.value(ini_rot_center_z).toFloat();

    _max_depth_to_cancel = settings.value(ini_max_depth_to_cancel).toFloat();

    _decim_pnt          = decim_pnt;

    _use_metadata_angle = settings.value(ini_use_metadata_angle).toBool();
    _camera_laser_interchanged = settings.value(ini_camera_laser_interchanged).toBool();
    _meta_angle_correction =   BGM(settings.value(ini_meta_angle_correction).toFloat());

    _laser_depth_offset = settings.value(ini_laser_depth_offset).toFloat();

    std::cout << ":::computing thread number "  << CGlobalHSSettings::_computing_threads << std::endl;


    bool is_callibrated = settings.value(ini_is_callibrated).toBool();

    if(is_callibrated)
    {
        float  cx = settings.value(ini_cx).toFloat();
        float  fx = settings.value(ini_fx).toFloat();
        float  cy = settings.value(ini_cy).toFloat();
        float  fy = settings.value(ini_fy).toFloat();
        g_cam_intrinsics = (cv::Mat_<double>(3,3) <<fx, 0., cx,
                                    0., fy, cy,
                                    0.,  0., 1. );

        float d1 = settings.value(ini_dist1).toFloat();
        float d2 = settings.value(ini_dist2).toFloat();
        float d3 = settings.value(ini_dist3).toFloat();
        float d4 = settings.value(ini_dist4).toFloat();
        float d5 = settings.value(ini_dist5).toFloat();
        g_cam_distortion = (cv::Mat_<double>(1,5) << d1, d2, d3, d4, d5) ;
    }

    // settings.value(ini_computing_threads).toInt();
   // if(CGlobalHSSettings::_computing_threads > 10) CGlobalHSSettings::_computing_threads = 10;
   // if(CGlobalHSSettings::_computing_threads < 1) CGlobalHSSettings::_computing_threads  = 1;


    start();
}

void CLaserSanOfflineThread::start_process(QString & ininame,
                                           bool rand_lcol,
                                           int decim_pnt,
                                           float side_len,
                                           bool use_lcorr,
                                           bool ismesh,
                                           bool isCalibMode,
                                           bool useADeform)
{



    _abort = false;

    _use_corr = use_lcorr;


    QFileInfo info_fname(ininame);
    QSettings settings(ininame, QSettings::IniFormat);

    _path = info_fname.absolutePath();
    _image_name_format = settings.value(ini_image_name_format).toString();
    _img_beg_idx = settings.value(ini_first_image).toInt();
    _img_end_idx =  settings.value(ini_last_image).toInt();

    _has_several_scans = settings.value(ini_image_name_has_scan).toBool();
    _scan_first        = settings.value(ini_first_scan).toInt();
    _scan_last         = settings.value(ini_last_scan).toInt();

    if(!_has_several_scans)
    {
        _scan_first = 0;
        _scan_last  = 0;
    }

    _sca_angle = BGM(settings.value(ini_scan_angle_step).toFloat());


    _is_difference  = settings.value(ini_is_difference).toBool();
    _has_marks      = settings.value(ini_has_marks).toBool();
    _is_round_marks = settings.value(ini_is_round_marks).toInt();
    _bckg_image     = settings.value(ini_bckg_image).toString();
    _bckg_image2    = settings.value(ini_bckg_image2).toString();
    _marks_x = settings.value(ini_marks_x).toInt();
    _marks_y = settings.value(ini_marks_y).toInt();

    _fov_horiz = BGM(settings.value(ini_fov_horiz).toFloat());
    _fov_vert  = BGM(settings.value(ini_fov_vert).toFloat());


    _camera_laser_dist = settings.value(ini_camera_laser_dist).toFloat();
    _camera_x_incline = -BGM(settings.value(ini_camera_x_incline).toFloat());
    _camera_y_incline = -BGM(settings.value(ini_camera_y_incline).toFloat());
    _rot_center_x = settings.value(ini_rot_center_x).toFloat();
    _rot_center_y = settings.value(ini_rot_center_y).toFloat();
    _rot_center_z = settings.value(ini_rot_center_z).toFloat();

    _max_depth_to_cancel = settings.value(ini_max_depth_to_cancel).toFloat();
    _min_depth_to_cancel = settings.value(ini_min_depth_to_cancel).toFloat();


    _random_line_color  = rand_lcol;
    _decim_pnt          = decim_pnt;
    _max_side_len       = side_len;

    _compute_laser_pos  = settings.value(ini_compute_laser_pos).toBool();
    _las_betta_beg      =  BGM(settings.value(ini_laser_betta_angle_init).toFloat());
    _las_betta_step     = -BGM(settings.value(ini_laser_betta_angle_step).toFloat());

    _tex_name_format = settings.value(ini_tex_name_format).toString();

    _use_metadata_angle = settings.value(ini_use_metadata_angle).toBool();
    _camera_laser_interchanged = settings.value(ini_camera_laser_interchanged).toBool();
    _meta_angle_correction =   BGM(settings.value(ini_meta_angle_correction).toFloat());

    _laser_depth_offset = settings.value(ini_laser_depth_offset).toFloat();



    _laser_onboard_ymin = settings.value(ini_lookfor_laser_onboard_ymin).toFloat();;
    _laser_onboard_ymax = settings.value(ini_lookfor_laser_onboard_ymax).toFloat();
    _single_cell_width = settings.value(ini_single_cell_width).toFloat();
    _camera_obj_dist =   settings.value(ini_camera_obj_dist).toFloat();

    _do_mesh = ismesh;

    CGlobalHSSettings::_computing_threads = settings.value(ini_computing_threads).toInt();
    if(CGlobalHSSettings::_computing_threads > 10) CGlobalHSSettings::_computing_threads = 10;
    if(CGlobalHSSettings::_computing_threads < 1) CGlobalHSSettings::_computing_threads  = 1;

    std::cout << _laser_onboard_ymin << " " << _laser_onboard_ymax << " " << _single_cell_width << " " << _camera_obj_dist << std::endl;

    _is_calib_mode = isCalibMode;
    _use_adeform = useADeform;

    start();
}

void CLaserSanOfflineThread::stop_process()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
}

void CLaserSanOfflineThread::emit_send_back(int i)
{
     emit send_back(i);
}



float compute_dist_pnt(aux_pix_data * p0, aux_pix_data *p1)
{
    float x0 = p0->_x;
    float y0 = p0->_y;
    float z0 = p0->_z;

    float x1 = p1->_x;
    float y1 = p1->_y;
    float z1 = p1->_z;

    return std::sqrt((x0-x1)*(x0-x1) + (y0-y1)*(y0-y1) + (z0-z1)*(z0-z1));
}

CTriMesh *CLaserSanOfflineThread::construct_mesh_from_single_shot(CArray2D<aux_pix_data *> & merged_data,
                                                                  int decimstep)
{

    ////////////////////////
   /* float gl_scale = .04;
    float gl_shift_x = 0;
    float gl_shift_y = 2;
    float gl_shift_z = 11;*/
    ////////////////////////

    CTriMesh * tmesh = new CTriMesh;

    int xs = merged_data._xs;
    int ys = merged_data._ys;

    const int undefid = -1;
    std::vector<int> id_points(xs*ys, undefid);

    ///////////////// define points
    int pnt_idx = 0;
    for (int y = 0; y < ys; y+= decimstep )
    for (int x = 0; x < xs; x+= decimstep )
    {
        if(merged_data._data[y][x]!= NULL)
        {

            merged_data._data[y][x]->_gx = convert_real_coord_to_gl_x(merged_data._data[y][x]->_x);
            merged_data._data[y][x]->_gy = convert_real_coord_to_gl_y(-merged_data._data[y][x]->_z);
            merged_data._data[y][x]->_gz = convert_real_coord_to_gl_z(merged_data._data[y][x]->_y);


            C3DPoint p3d;
            p3d._x = merged_data._data[y][x]->_gx;
            p3d._y = merged_data._data[y][x]->_gy;
            p3d._z = merged_data._data[y][x]->_gz;

            merged_data._data[y][x]->_pidx = tmesh->_points.size();
            tmesh->_points.push_back(p3d);
            id_points[y*xs + x] = pnt_idx;
            pnt_idx++;
        }
    }

    ///////////////// define triangles
    for (int y = 0; y < ys - decimstep ; y+= decimstep )
    for (int x = 0; x < xs - decimstep;  x+= decimstep )
    {
        int pidx_0 = id_points[y*xs + x];
        int pidx_1 = id_points[y*xs + x + decimstep];
        int pidx_2 = id_points[(y + decimstep) * xs + x];
        int pidx_3 = id_points[(y + decimstep) * xs + x + decimstep];


        // first valid triangle
        if((pidx_0 != undefid) && (pidx_1 != undefid) && (pidx_2 != undefid) )
        {
            bool valid_dist_01 = (compute_dist_pnt(merged_data._data[y][x],
                                                   merged_data._data[y][x+decimstep]) < _max_side_len*decimstep);
            bool valid_dist_12 = (compute_dist_pnt(merged_data._data[y][x+decimstep],
                                                   merged_data._data[y+decimstep][x]) < _max_side_len*decimstep);
            bool valid_dist_20 = (compute_dist_pnt(merged_data._data[y+decimstep][x],
                                                   merged_data._data[y][x]) < _max_side_len*decimstep);
            if(valid_dist_01 && valid_dist_12 && valid_dist_20)
            {
                // add new triangle
                C3DPointIdx tri;
                tri.pnt_index[0] = pidx_2;
                tri.pnt_index[1] = pidx_1;
                tri.pnt_index[2] = pidx_0;
                tmesh->_triangles.push_back(tri);
            }
        }


        // second valid triangle
        if((pidx_2 != undefid) && (pidx_1 != undefid) && (pidx_3 != undefid)  )
        {
            bool valid_dist_21 = (compute_dist_pnt(merged_data._data[y+decimstep][x],
                                                   merged_data._data[y][x+decimstep]) < _max_side_len*decimstep);
            bool valid_dist_13 = (compute_dist_pnt(merged_data._data[y][x+decimstep],
                                                   merged_data._data[y+decimstep][x+decimstep]) < _max_side_len*decimstep);
            bool valid_dist_32 = (compute_dist_pnt(merged_data._data[y+decimstep][x+decimstep],
                                                   merged_data._data[y+decimstep][x]) < _max_side_len*decimstep);
            if(  valid_dist_21 && valid_dist_13 && valid_dist_32 )
            {
                // add new triangle
                C3DPointIdx tri;
                tri.pnt_index[0] = pidx_3;
                tri.pnt_index[1] = pidx_1;
                tri.pnt_index[2] = pidx_2;
                tmesh->_triangles.push_back(tri);
            }
         }
    }

    ///////// update mesh normals per face
    std::vector<C3DPointIdx>::iterator itt = tmesh->_triangles.begin();
    for(int fidx = 0 ; itt != tmesh->_triangles.end(); itt++, fidx++)
    {
        tmesh->_points[ itt->pnt_index[0] ]._faceidx.push_back(fidx);
        C3DPoint pnt0 = tmesh->_points[ itt->pnt_index[0] ];

        tmesh->_points[ itt->pnt_index[1] ]._faceidx.push_back(fidx);
        C3DPoint pnt1 = tmesh->_points[ itt->pnt_index[1] ];

        tmesh->_points[ itt->pnt_index[2] ]._faceidx.push_back(fidx);
        C3DPoint pnt2 = tmesh->_points[ itt->pnt_index[2] ];

        itt->tri_normal = compute_normal(pnt2, pnt1, pnt0);
    }

    //////// update mesh normals per vertex
    std::vector<C3DPoint>::iterator itv = tmesh->_points.begin();
    for(; itv != tmesh->_points.end(); itv++)
    {
        itv->_nx =0;
        itv->_ny =0;
        itv->_nz =0;

        std::vector<int>::iterator fit = itv->_faceidx.begin();
        for( ; fit != itv->_faceidx.end(); fit++)
        {

            itv->_nx += tmesh->_triangles[(*fit)].tri_normal._x;
            itv->_ny += tmesh->_triangles[(*fit)].tri_normal._y;
            itv->_nz += tmesh->_triangles[(*fit)].tri_normal._z;
        }
        normalize3(itv->_nx, itv->_ny, itv->_nz);
    }
    return tmesh;
}

void CLaserSanOfflineThread::construct_merged_data(QImage & bckg,
                                                   CArray2D<aux_pix_data *> & merged_data)
{
    int processed = 0;
    #pragma omp parallel for num_threads(4)
    for(int idx = _img_beg_idx; idx <= _img_end_idx; idx += 1)
    {

        QString image_name  = QString("/leftcam0%1.png").arg(idx, 3, 10, QLatin1Char('0'));
        QString filname =_path +image_name;

        QImage img(filname);


        difference_image_fast_supp_red(&img, &bckg, CGlobalSettings::_ignore_read_thresh);
        /// difference_image_fast(&img, &bckg);

        std::vector<CIPixel> pix = get_edge_points_lines_hills_1D(&img);


        #pragma omp critical
        {
            processed++;
            emit send_back(float(processed)*100/float(_img_end_idx));
        }

        std::vector<CIPixel>::iterator pit = pix.begin();
        for( ; pit != pix.end(); pit++)
        {
           int u =  pit->_u;
           int v =  pit->_v;

            #pragma omp critical
            {
               if(merged_data._data[v][u] == NULL)
                    merged_data._data[v][u] = new aux_pix_data;

               merged_data._data[v][u]->_u =  pit->_u;
               merged_data._data[v][u]->_v =  pit->_v;
               merged_data._data[v][u]->_idx =  idx;
            }
        }

        std::cout << "reading " << idx << " " << pix.size() << std::endl;
    }
}

bool cmp_mpix(const aux_pix_data * p1, const aux_pix_data * p2)
{
    if(p1->_y < p2->_y)
        return true;
    return false;
}


void CLaserSanOfflineThread::fill_gaps_in_merged_data(CArray2D<aux_pix_data *> * merged_data)
{
    std::cout << "fill_interpolated_merged_data()" << std::endl;
    int xs = merged_data->_xs;
    int ys = merged_data->_ys;

    for(int vc = 0; vc < ys; vc++)
    {
       /// std::cout << vc << std::endl;
        int uc = 0;
        while (uc < xs)
        {
            if(merged_data->_data[vc][uc] != NULL)
            {
                int found_uc_left = uc;

                uc++;
                while (uc < xs)
                {
                    if(merged_data->_data[vc][uc] != NULL)
                    {

                        int found_uc_right = uc;
                        aux_pix_data * datL = merged_data->_data[vc][found_uc_left];
                        aux_pix_data * datR = merged_data->_data[vc][found_uc_right];

                        float lin_dist = found_uc_right - found_uc_left;
                        int middle = (found_uc_left+1 +found_uc_right)/2;
                        for (int x = found_uc_left+1;  x < found_uc_right; x++)
                        {
                           // std::cout << "nval "<< std::endl;
                            ///if(x == middle)
                            {
                                merged_data->_data[vc][x] = new aux_pix_data;

                                float sc = (x - found_uc_left)/lin_dist;


                                merged_data->_data[vc][x]->_x = datL->_x + (datR->_x - datL->_x)*sc;
                                merged_data->_data[vc][x]->_y = datL->_y + (datR->_y - datL->_y)*sc;
                                merged_data->_data[vc][x]->_z = datL->_z + (datR->_z - datL->_z)*sc;

                                merged_data->_data[vc][x]->_u = x;
                                merged_data->_data[vc][x]->_v = vc;
                            }


                        }
                        uc --;
                        break;
                    }


                    uc++;
                }
            }
            uc++;
        }
    }
    std::cout << "complete interpolation" << std::endl;
}

void CLaserSanOfflineThread::fill_gaps_in_merged_data_triv(CArray2D<aux_pix_data *> & merged_data)
{
    /// for averaging use truth only points!!!!

    int xs = merged_data._xs;
    int ys = merged_data._ys;

    CArray2D<bool> mask(xs,ys,true);

    for (int j = 1; j < ys-1; j++)
    {
        for (int i = 1; i < xs-1; i++)
        {


            if(merged_data._data[j][i] == NULL)
            {
                   std::vector<aux_pix_data*> neighb;
                   if(merged_data._data[j][i-1]!= NULL && mask._data[j][i-1])
                         neighb.push_back(merged_data._data[j][i-1]);

                    if(merged_data._data[j][i+1]!= NULL && mask._data[j][i+1])
                          neighb.push_back(merged_data._data[j][i+1]);

                    if(merged_data._data[j+1][i]!= NULL && mask._data[j+1][i])
                          neighb.push_back(merged_data._data[j+1][i]);

                    if(merged_data._data[j-1][i]!= NULL && mask._data[j-1][i])
                          neighb.push_back(merged_data._data[j-1][i]);


                    if(merged_data._data[j-1][i-1]!= NULL && mask._data[j-1][i-1])
                          neighb.push_back(merged_data._data[j-1][i-1]);

                    if(merged_data._data[j+1][i+1]!= NULL && mask._data[j+1][i+1])
                           neighb.push_back(merged_data._data[j+1][i+1]);

                    if(merged_data._data[j-1][i+1]!= NULL && mask._data[j-1][i+1])
                           neighb.push_back(merged_data._data[j-1][i+1]);

                    if(merged_data._data[j+1][i-1]!= NULL && mask._data[j+1][i-1])
                           neighb.push_back(merged_data._data[j+1][i-1]);

                    float nsz = neighb.size();
                    if(nsz > 3)
                    {
                        merged_data._data[j][i] = new aux_pix_data;
                        mask._data[j][i] = false;

                        merged_data._data[j][i]->_idx = neighb.front()->_idx;
                        merged_data._data[j][i]->_langle= neighb.front()->_langle;


                        std::vector<aux_pix_data*>::iterator it = neighb.begin();
                        for( ; it != neighb.end(); it++)
                        {
                            merged_data._data[j][i]->_u += (*it)->_u;
                            merged_data._data[j][i]->_v += (*it)->_v;
                            merged_data._data[j][i]->_x += (*it)->_x;
                            merged_data._data[j][i]->_y += (*it)->_y;
                            merged_data._data[j][i]->_z += (*it)->_z;
                            merged_data._data[j][i]->_gx += (*it)->_gx;
                            merged_data._data[j][i]->_gy += (*it)->_gy;
                            merged_data._data[j][i]->_gz += (*it)->_gz;
                        }

                        merged_data._data[j][i]->_u /= nsz;
                        merged_data._data[j][i]->_v /= nsz;
                        merged_data._data[j][i]->_x /= nsz;
                        merged_data._data[j][i]->_y /= nsz;
                        merged_data._data[j][i]->_z /= nsz;
                        merged_data._data[j][i]->_gx /= nsz;
                        merged_data._data[j][i]->_gy /= nsz;
                        merged_data._data[j][i]->_gz /= nsz;


                    }
            }
        }
    }

}

void CLaserSanOfflineThread::construct_texture(CArray2D<aux_pix_data *> & merged_data,
                                               CTriMesh * rmesh,  QImage & teximg)
{

    int xs = merged_data._xs;
    int ys = merged_data._ys;

    double u0 = 10e8;
    double v0 = 10e8;
    double u1 = -1;
    double v1 = -1;
    for (int j=0; j < ys; j++)
    for (int i=0; i < xs; i++)
    {
        if( (merged_data._data[j][i]!= NULL))
        {
            if(i < u0) u0 = i;
            if(i > u1) u1 = i;
            if(j < v0) v0 = j;
            if(j > v1) v1 = j;
        }
    }
    double _tex_u0i = std::max(int(u0),0); // min corner
    double _tex_v0i = std::max(int(v0),0);
    double _tex_u1i = std::min((int)std::ceil(u1),teximg.width()-1); // max corner
    double _tex_v1i = std::min((int)std::ceil(v1),teximg.height()-1);

    QImage impart = teximg.copy(_tex_u0i, _tex_v0i , (_tex_u1i-_tex_u0i)+1 , (_tex_v1i-_tex_v0i)+1);
    QImage *_texture = new QImage(impart);



    // resize texture coordinate
    rmesh->_tex_coords = std::vector<Vector2f>(rmesh->_points.size());
    // setup texture coordinates
    for (int j=0; j < ys; j++)
    for (int i=0; i < xs; i++)
    {
        if( (merged_data._data[j][i]!= NULL))
        {
            double norm_u = (merged_data._data[j][i]->_u - _tex_u0i)/ double( (_tex_u1i-_tex_u0i) +  1.);
            norm_u  = std::max(norm_u,0.);
            norm_u  = std::min(norm_u,1.);

            // upside down
            double norm_v = 1 - (merged_data._data[j][i]->_v - _tex_v0i)/double( (_tex_v1i - _tex_v0i) + 1.);
            norm_v  = std::max(norm_v,0.);
            norm_v  = std::min(norm_v,1.);


            Vector2f tex_coord;
            tex_coord.x = norm_u;
            tex_coord.y = norm_v;
            rmesh->_tex_coords[merged_data._data[j][i]->_pidx] = tex_coord;
        }
    }

    int trsz = rmesh->_triangles.size();
    for( int tr = 0; tr < trsz;  tr++)
        for (int p = 0; p < 3; p++)
            rmesh->_triangles[tr].tex_index[p] = rmesh->_triangles[tr].pnt_index[p];

    rmesh->_texture = _texture;
}

void CLaserSanOfflineThread::construct_pcl(CArray2D<aux_pix_data *> * merged_data,
                                           int cx, int cy, int cz, int s,
                                           float gl_red,
                                           float gl_gre,
                                           float gl_blu,
                                           int decim)
{
   QImage tex;
   std::vector<C3DPoint> * pcl_pnts =  prepare_pcl(merged_data, cx, cy, cz, s,
                                                   gl_red, gl_gre, gl_blu, decim, tex);

    emit send_result(pcl_pnts);
}

std::vector<cv::Point3f> CLaserSanOfflineThread::get_neighbour_3d_glpoints(std::vector<C3DPoint*> & gl_points,
                                                                         CArray2D<aux_pix_data *> * _merged_data,
                                                                          int x, int y, int xs, int ys,
                                                                          int decimstep, int nsz)
{
    std::vector<cv::Point3f> res;

    for (int j = y - nsz * decimstep;
         j <= y+ nsz *decimstep; j+=decimstep )
    {
        for (int i = x - nsz * decimstep;
             i <= x + nsz * decimstep; i+=decimstep)
        {
            if( (j >= 0) && (j < ys) && (i >=0) && (i < xs))
            {
                 if( _merged_data->_data[j][i]!= NULL)
                 {
                     if(gl_points[j * xs + i] != NULL)
                     {
                        C3DPoint p = *gl_points[j * xs + i];
                        cv::Point3f pp;
                        pp.x = p._x;
                        pp.y = p._y;
                        pp.z = p._z;
                        res.push_back(pp);
                     }
                 }
            }
        }
    }
    return res;
}

std::vector<C3DPoint> * CLaserSanOfflineThread::prepare_pcl_with_norms(CArray2D<aux_pix_data *> * _merged_data,
                                            int s,
                                           float gl_red,
                                           float gl_gre,
                                           float gl_blu,
                                           int decim,
                                            QImage & tex)
{
    int xs = _merged_data->_xs;
    int ys = _merged_data->_ys;

    std::vector<C3DPoint*>  gl_points(xs*ys,NULL);

    for (int j=0; j < ys; j++)
        for (int i=0; i < xs; i++)
        {

          if( _merged_data->_data[j][i]!= NULL)
          {
               C3DPoint p3d;
               p3d._x = _merged_data->_data[j][i]->_gx;
               p3d._y = _merged_data->_data[j][i]->_gy;
               p3d._z = _merged_data->_data[j][i]->_gz;

               p3d._nx = 0;
               p3d._ny = 0;
               p3d._nz = 1;

              if(tex.width() !=0)
              {
                   QRgb value = tex.pixel(_merged_data->_data[j][i]->_u, _merged_data->_data[j][i]->_v);

                   p3d._r =  qRed(value)/ 255.;
                   p3d._g =  qGreen(value)/ 255.;
                   p3d._b =  qBlue(value)/255.;

              }
              else
              {
                  p3d._r = gl_red;
                  p3d._g = gl_gre;
                  p3d._b = gl_blu;
              }
              gl_points[j*xs + i] = new C3DPoint;
              *gl_points[j*xs + i] = p3d;
          }
        }

    // norml backrotation
    float scan_angle = _sca_angle * float(s);
    float rmat_00 =  cos(scan_angle);
    float rmat_01 = -sin(scan_angle);
    float rmat_10 =  sin(scan_angle);
    float rmat_11 =  cos(scan_angle);

    float mat_11 =  cos(-_camera_x_incline);
    float mat_12 = -sin(-_camera_x_incline);
    float mat_21 =  sin(-_camera_x_incline);
    float mat_22 =  cos(-_camera_x_incline);

    std::vector<C3DPoint> * pcl_pnts =  new std::vector<C3DPoint>;
    for (int j=0; j < ys; j+=decim) // decim
        for (int i=0; i < xs; i+=decim) //decim
        {

          if( _merged_data->_data[j][i]!= NULL)
          {

               std::vector<cv::Point3f> neigh_pnts  = get_neighbour_3d_glpoints(gl_points, _merged_data,
                                                                               i, j, xs, ys, decim, 5);
               if(neigh_pnts.size() > 5)
              {
                      Vector3f n = compute_lse_plane_normal(neigh_pnts);

                      /// rotate around y
                      /// float nx = n.x * rmat_00 + n.z * rmat_01;
                      float ny = n.y;
                      float nz = n.x * rmat_10 + n.z * rmat_11;

                      /// rotate around x
                      /// float nnx =  nx;
                      /// float nny =  ny * mat_11 + nz * mat_12;
                      float nnz =  ny * mat_21 + nz * mat_22;

                      if(nnz < 0.) /// check this condition
                      {
                          n.x = -n.x;
                          n.y = -n.y;
                          n.z = -n.z;
                      }

                      C3DPoint p = *gl_points[j*xs + i];
                      p._nx = n.x;
                      p._ny = n.y;
                      p._nz = n.z;
                      pcl_pnts->push_back(p);
            }
        }
    }

    std::vector<C3DPoint*>::iterator it =  gl_points.begin();
    for( ; it != gl_points.end(); it++)
        if((*it) != NULL) delete (*it);

    return pcl_pnts;
}


std::vector<C3DPoint> * CLaserSanOfflineThread::prepare_pcl(CArray2D<aux_pix_data *> * _merged_data,
                                           int cx, int cy, int cz, int s,
                                           float gl_red,
                                           float gl_gre,
                                           float gl_blu,
                                           int decim,
                                            QImage & tex)
{
    int xs = _merged_data->_xs;
    int ys = _merged_data->_ys;
    std::vector<C3DPoint> * pcl_pnts =  new std::vector<C3DPoint>;
    for (int j=0; j < ys; j+=decim)
        for (int i=0; i < xs; i+=decim)
        {

          if( _merged_data->_data[j][i]!= NULL)
          {
               _merged_data->_data[j][i]->_x += cx;
               _merged_data->_data[j][i]->_y += cy;
               _merged_data->_data[j][i]->_z += cz;

               _merged_data->_data[j][i]->_gx = convert_real_coord_to_gl_x(_merged_data->_data[j][i]->_x );
               _merged_data->_data[j][i]->_gy = convert_real_coord_to_gl_y(-_merged_data->_data[j][i]->_z );
               _merged_data->_data[j][i]->_gz = convert_real_coord_to_gl_z(_merged_data->_data[j][i]->_y );

               C3DPoint p3d;
               p3d._x = _merged_data->_data[j][i]->_gx;
               p3d._y = _merged_data->_data[j][i]->_gy;
               p3d._z = _merged_data->_data[j][i]->_gz;

               p3d._nx = 0;
               p3d._ny = 0;
               p3d._nz = 1;

              if(tex.width() !=0)
              {
                   QRgb value = tex.pixel(_merged_data->_data[j][i]->_u, _merged_data->_data[j][i]->_v);

                   p3d._r =  qRed(value)/ 255.;
                   p3d._g =  qGreen(value)/ 255.;
                   p3d._b =  qBlue(value)/255.;

              }
              else
              {
                  p3d._r = gl_red;
                  p3d._g = gl_gre;
                  p3d._b = gl_blu;

              }
              pcl_pnts->push_back(p3d);
          }
        }
    return pcl_pnts;
}
/*
void CLaserSanOfflineThread::run()
{
    clock_t msstart = clock();

    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////


    int sidx_beg = 1;
    int sidx_end =  _scan_cnt;
    float scan_angle_step = _sca_angle;
    int idx_beg = _img_beg_idx;
    int idx_end = _img_end_idx;

    float las_betta_beg  = _las_betta_beg;
    float las_betta_step = _las_betta_step;

    QString work_path = _path;

    int minimal_laser_pnts = 5;

    ///// determine image sizes
    QString tex_filname = work_path + QString("/bg.jpg");
    QImage bckg(tex_filname);
    int xs = bckg.width();
    int ys = bckg.height();

    float camera_laser_dist = 110; // mm
    float fov_x = _fov_horiz;
    float fov_z = _fov_vert;

    float gl_scale = .04;
    float gl_shift_x = 0;
    float gl_shift_y = 2;
    float gl_shift_z = 11;


    float cam_rot_x = BGM(0); //BGM(-10);
    float max_depth_to_cancel = 300; // mm
    float min_depth_to_cancel = 50; // mm


    //////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////

    // float mat_00 = 1;
    //float mat_01 = 0;
    //float mat_02 = 0;

    //float mat_10 = 0;
    float mat_11 =  cos(cam_rot_x);
    float mat_12 = -sin(cam_rot_x);

    //float mat_20 =  0;
    float mat_21 =  sin(cam_rot_x);
    float mat_22 =  cos(cam_rot_x);
    std::ofstream logfile;
    logfile.open ("laser_files.log");

    std::vector<CArray2D<aux_pix_data *> *> m_scans;
    for(int sidx = sidx_beg; sidx <= sidx_end; sidx++)
    {


        CArray2D<aux_pix_data *> * merged_data = new CArray2D<aux_pix_data *>(xs, ys, NULL);

        int processed = 0;

        #pragma omp parallel for ordered schedule(dynamic,1) num_threads(4)
        for(int idx = idx_beg; idx <= idx_end; idx ++)
        {
            srand (idx);
            float line_r = float(rand() % 100)/99.0 ;
            float line_g = float(rand() % 100)/99.0 ;
            float line_b = float(rand() % 100)/99.0 ;

            if (_abort) continue;

            QString image_name  = QString("/scene_%1.jpg").arg(idx, 3, 10, QLatin1Char('0'));
            QString filname = work_path + image_name;
            QImage img(filname);


            difference_image_fast(&img, &bckg);

            //logfile  << "reading " << filname.toStdString() << " " << img.width() << " " << img.height() << " " << bckg.width() << " " << bckg.height() << std::endl;

            /// actually we should work with difference
            /// not original image
            //std::cout << "color threshold: " << << std::endl;

            std::vector<CIPixel> pix = get_edge_points_lines_hills_1D(&img);

            #pragma omp critical
            {
                processed++;
                emit send_back(float(processed)*100/float(idx_end));
            }

            if(pix.size() > minimal_laser_pnts)
            {

                std::vector<C3DPoint> * pcl_pnts =  new std::vector<C3DPoint>;

                std::vector<CIPixel>::iterator pit = pix.begin();
                for( ; pit != pix.end(); pit++)
                {
                    int u =  pit->_u + 0.5;
                    int v =  pit->_v + 0.5;

                   /// compute 3d point
                   float laser_betta = las_betta_beg + las_betta_step * float(idx)  ;
                    C3DPoint p3d = image2space_conv(camera_laser_dist,
                                             pit->_u,
                                             pit->_v,
                                             laser_betta,
                                             xs, ys,
                                             fov_x, fov_z,
                                             0);


                    if ((p3d._y < min_depth_to_cancel) || (p3d._y > max_depth_to_cancel) ) continue;

                    /// initialize laser flick poisition (u,v)
                    #pragma omp critical
                    {
                       if(merged_data->_data[v][u] == NULL)
                            merged_data->_data[v][u] = new aux_pix_data;

                       merged_data->_data[v][u]->_u   =  pit->_u;
                       merged_data->_data[v][u]->_v   =  pit->_v;
                       merged_data->_data[v][u]->_idx =  idx;
                    }

                    // rotate back from camer
                    float nx = p3d._x;// * mat_00 +   p3d._y * mat_01 + p3d._z * mat_02;
                    float ny =   p3d._y * mat_11 + p3d._z * mat_12;
                    float nz =  p3d._y * mat_21 + p3d._z * mat_22;


                    /// rotate vertical here
                    /// store real coords
                    #pragma omp critical
                    {
                        merged_data->_data[v][u]->_x = nx;
                        merged_data->_data[v][u]->_y = ny;
                        merged_data->_data[v][u]->_z = nz;

                        merged_data->_data[v][u]->_gx =  gl_scale * nx + gl_shift_x;
                        merged_data->_data[v][u]->_gy =  gl_scale * nz + gl_shift_y;
                        merged_data->_data[v][u]->_gz = -gl_scale * ny + gl_shift_z;
                    }

                   //// shift in real coordinates
                   p3d._x = merged_data->_data[v][u]->_gx;
                   p3d._y = merged_data->_data[v][u]->_gy;
                   p3d._z = merged_data->_data[v][u]->_gz;
                   //////////////////////////////////////

                   if(_random_line_color)
                   {
                        p3d._r = line_r;
                        p3d._g = line_g;
                        p3d._b = line_b;
                    }
                    else
                   {
                       QRgb value = bckg.pixel(u,v);

                       p3d._r =   qRed(value) / 255.;
                       p3d._g =  qGreen(value)/ 255.;
                       p3d._b =  qBlue(value)/255.;


                   }

                   pcl_pnts->push_back(p3d);

                }

                #pragma omp critical
                {
                     emit send_result(pcl_pnts);
                }
            } /// there are 3d points

            std::cout << "reading " << idx << " " << pix.size() << std::endl;

        } /// read next image




        //// emit send_clean_scan();

       m_scans.push_back(merged_data);

    } /// read next scan

    if (_abort)
    {
        emit send_back(0);
        return;
    }
    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
    ///
    logfile.close();
}*/


QString CLaserSanOfflineThread::construct_image_name(char * pattern_name_buf, int scan, int idx)
{

    if(_has_several_scans)
        std::sprintf(pattern_name_buf, _image_name_format.toStdString().c_str(), scan,  idx);
    else
        std::sprintf(pattern_name_buf, _image_name_format.toStdString().c_str(), idx);
    QString image_name = _path + QString("/") + QString(pattern_name_buf);
  //  std::cout << "IMG " << image_name.toStdString() << std::endl;

    return image_name;

}


QString CLaserSanOfflineThread::construct_file_name(char * pattern_name_buf, QString & fformat, int scan )
{

    if(_has_several_scans)
        std::sprintf(pattern_name_buf, fformat.toStdString().c_str(), scan);
    else
        std::sprintf(pattern_name_buf, fformat.toStdString().c_str());
    QString name = _path + QString("/") + QString(pattern_name_buf);

    return name;

}

QString CLaserSanOfflineThread::construct_bckg_name(char * pattern_name_buf, int scan)
{

    if(_has_several_scans)
        std::sprintf(pattern_name_buf, _bckg_image.toStdString().c_str(), scan);
    else
        std::sprintf(pattern_name_buf, _bckg_image.toStdString().c_str());
    QString bck_name = _path + QString("/") + QString(pattern_name_buf);

   // std::cout << "BCKG " << bck_name.toStdString() << std::endl;

    return bck_name ;
}


QString CLaserSanOfflineThread::construct_tex_name(char * pattern_name_buf, int scan)
{

    if(_has_several_scans)
        std::sprintf(pattern_name_buf, _tex_name_format.toStdString().c_str(), scan);
    else
        std::sprintf(pattern_name_buf, _tex_name_format.toStdString().c_str());
    QString tex_name = _path + QString("/") + QString(pattern_name_buf);
  //  std::cout << "TEX " << tex_name.toStdString() << std::endl;

    return tex_name;

}



std::vector<CIPixel> CLaserSanOfflineThread::extract_only_grid_laser_pnts(std::vector<CIPixel>  & inpnts)
{
    // to be implemented later, for now take points from min-max roi


    std::vector<CIPixel> res;
    std::vector<CIPixel>::iterator it = inpnts.begin();
    for( ; it != inpnts.end(); it ++)
    {
        CIPixel px = *it;
        if(/*(px._v > _laser_onboard_ymin) &&*/ (px._v < _laser_onboard_ymax) )
            res.push_back(px);
    }
    return res;
}

bool laser_pnts_sort_x(const CIPixel & a, const CIPixel & b)
{
    return (a._u < b._u);
}


bool CLaserSanOfflineThread::convert_pix_to_laser_angle(std::vector<CIPixel> & grid_pix,
                                                        int xs, float & laser_angle)
{

    if( grid_pix.size() < 10)
    {
        std::cout << "ATTENTON: CANNOT FIND ENOUGH POINTS TO COMPUTE LASER POS" << std::endl;
        return false;
    }

    std::sort(grid_pix.begin(), grid_pix.end(), laser_pnts_sort_x);
    int vsz = grid_pix.size();
    double cutoff_min = double(vsz) * 0.2;
    double cutoff_max = double(vsz) * 0.8;
    std::vector<CIPixel> good_points;
    for(int i = cutoff_min; i <  cutoff_max; i++ )
        good_points.push_back(grid_pix[i]);

    float vert_pix_pos = 0;
    std::vector<CIPixel>::iterator it =  good_points.begin();
    for( ; it != good_points.end() ; it++)
        vert_pix_pos += it->_u;
    vert_pix_pos /= float(good_points.size());

    int pc = 0;
    float av_grid_width_px = 0;
    for(int p = 0; p < (_corners.size() -1) ; p++ )
    {

            if( /*(_corners[p].y > _laser_onboard_ymin) &&*/ (_corners[p].y < _laser_onboard_ymax) &&
                /* (_corners[p+1].y > _laser_onboard_ymin) &&*/ (_corners[p+1].y < _laser_onboard_ymax))
            {

                if(_corners[p].x < _corners[p+1].x)
                {

                    av_grid_width_px +=  std::sqrt((_corners[p+1].x - _corners[p].x)*(_corners[p+1].x - _corners[p].x) + (_corners[p+1].y - _corners[p].y)*(_corners[p+1].y - _corners[p].y) );
                    pc++;
                }
            }
    }
    av_grid_width_px /= float(pc);

   /// std::cout << " AV GRID WWW " << av_grid_width_px <<std::endl;
    // std::cout << " AV GRID X " << _corners[p].x  << " " << _corners[p+1].x <<std::endl;
    /// std::cout << " AV GRID Y " << _corners[p].y  << " " << _corners[p+1].y <<std::endl;
    // std::cout << " AV GRID DIFF " << _corners[p+1].x  - _corners[p].x <<std::endl;


    float cam_laser_order = (_camera_laser_interchanged)?-1.0:1.0;

    float laser_center_diff_px = float(xs/2) - vert_pix_pos;


    float d_prime = laser_center_diff_px * _single_cell_width / av_grid_width_px;
  ///  std::cout << d_prime << " " <<  laser_center_diff_px  << " " << _single_cell_width << " " << av_grid_width_px <<std::endl;


   laser_angle = std::atan2(_camera_obj_dist - _laser_depth_offset,  (_camera_laser_dist + cam_laser_order  * d_prime ));
    if(_camera_laser_interchanged)
        laser_angle = cPI - laser_angle;
    std::cout << IBGM(  laser_angle ) << " " <<  good_points.size() << std::endl;
    ///


    return true;
}

void CLaserSanOfflineThread::extract_grid_points(QString bckg_name)
{
   /// if(_bckg_image.size() == 0) return;
   /// QString bckg_name  = _path + QString("/") + _bckg_image;
    _corners = findGridMarkers(bckg_name);

}

CArray2D<int > CLaserSanOfflineThread::contruct_label_map(int xs, int ys,
                                                          int marks_x, int marks_y,
                                                          std::vector<cv::Point2f> & corners)
{

    CArray2D<int >  lbl_map(xs, ys, -1);
    for(int j = 0; j < ys ; j++)
        for(int i = 0; i < xs ; i++)
        {

            float close_dist = 1000000;
            int midx = 0;
            std::vector<cv::Point2f>::iterator itc = corners.begin();
            for(int l=0 ; itc != corners.end(); itc++, l++)
            {
                float dst = std::sqrt( (float(i) - itc->x)*(float(i) - itc->x) + (float(j) - itc->y)*(float(j) - itc->y));
                if(dst < close_dist )
                {
                    close_dist = dst;
                    midx = l;
                }
            }

            int midx_x = midx % marks_x;
            int midx_y = midx / marks_x;

            int xidx, yidx;
            if ( corners[midx].x > i )
                xidx = midx_x - 1;
            else
                xidx = midx_x;
            if(xidx < 0) xidx = 0;
            if(xidx == (marks_x -1 )) xidx = marks_x - 2;

            if (corners[midx].y  > j)
                yidx = midx_y - 1;
            else
                yidx = midx_y;
            if(yidx < 0) yidx = 0;
            if(yidx == (marks_y -1)) yidx = marks_y - 2;

            lbl_map._data[j][i] =yidx * marks_x + xidx;

        }


    return lbl_map;
}


std::vector<cv::Point2f> CLaserSanOfflineThread::find_calibration_board(int marks_x, int marks_y,
                                                                        QString imgname)
{
    std::cout << " find_calibration_board " << std::endl;
    cv::Size patternsize(marks_x, marks_y);

    cv::Mat inimg = imread(imgname.toStdString().c_str(), cv::IMREAD_GRAYSCALE );


  //  std::cout << " read img " << inimg.cols << std::endl;

    std::vector<cv::Point2f> corners;

 /*   cv::SimpleBlobDetector::Params params = contruct_blob_detector_pset();
   cv::SimpleBlobDetector detector(params);

    std::cout << " detector is ready " <<marks_x << " " << marks_y << std::endl;
 */
   /// cv::imshow("input image",  inimg);
   /// cv::waitKey(0);

    bool found = cv::findCirclesGrid(inimg, patternsize, corners/*,  cv::CALIB_CB_SYMMETRIC_GRID, &detector*/ );


    std::cout << " found " << found << std::endl;


  //  cv::drawChessboardCorners(inimg, patternsize, corners, found);
   // cv::imshow("image left",  inimg);
   // cv::waitKey(0);

    return corners;

}

void CLaserSanOfflineThread::construct_board_plain(
        CArray2D<aux_pix_data *>  * mdata, float &A, float & B, float &C, float &D)
{
    A = 0;
    B = 0;
    C = 0;
    D = 0;
    ////// test area
    int xs =   mdata->_xs;
    int ys =   mdata->_ys;
    std::vector<cv::Point3f>  points;
    for(int j = 0; j < ys ; j++)
    {
        for(int i = 0; i < xs ; i++)
        {
            if(mdata->_data[j][i] != NULL)
            {
                cv::Point3f p;
                p.x = mdata->_data[j][i]->_x;
                p.y = mdata->_data[j][i]->_y;
                p.z = mdata->_data[j][i]->_z;

                points.push_back(p);
            }
        }
    }
                                                 // iters //tol
    compute_plain_SVD_RANSAC(points, A, B, C, D, 50, 0.5 );
    std::cout << "ransac is done " << std::endl;
    //////////////////////////
}



CArray2D<cv::Point3f> CLaserSanOfflineThread::construct_deform_pattern( std::vector<CArray2D<aux_pix_data *> *> & merged_data,
                                                       std::vector<cv::Point2f> corners, int marks_x, int marks_y, float cz,
                                                                        board_corners & bc )
{
    CArray2D<aux_pix_data *> * mdata = merged_data[0];


    CArray2D<aux_pix_data *> grid(marks_x, marks_y, NULL);

    ///// construct (i.j) grid with gaps
    int found_markers =0;
    std::vector<cv::Point2f>::iterator it = corners.begin();
    for( int t = 0; it != corners.end(); it++, t++ )
    {

        int u = it-> x;
        int v = it-> y;

        std::cout << "looking for " << t << " " << u << " " << v << std::endl;

        int undefined = -1;
        int mu = undefined ;
        int mv = undefined ;
        for(int y = v-3 ; y <= v+3; y++ )
            for(int x = u-3 ; x <= u+3; x++)
             {
                if(mdata->_data[y][x] != NULL)
                {
                    if((mu == undefined ) && (mv == undefined ))
                    {
                         mu = x; mv = y;
                    }
                    else
                    {
                        if( (mdata->_data[y][x]->_u - it->x)*(mdata->_data[y][x]->_u - it->x) +
                            (mdata->_data[y][x]->_v - it->y)*(mdata->_data[y][x]->_v - it->y)
                            <
                            (mdata->_data[mv][mu]->_u - it->x)*(mdata->_data[mv][mu]->_u - it->x) +
                            (mdata->_data[mv][mu]->_v - it->y)*(mdata->_data[mv][mu]->_v - it->y)
                          )
                        {
                            mu = x; mv = y;
                        }
                    }
                }

            } // lookup neighbours

        if((mv != undefined) && (mu != undefined) )
            if(mdata->_data[mv][mu] != NULL)
            {
                int i = t % marks_x ;
                int j = t / marks_x ;
                grid._data[j][i] = mdata->_data[mv][mu];
                found_markers ++;

                std::cout << "[" << j << ", " << i  << "] " << mu << " " << mv<< std::endl;
                std::cout << "\t " <<   mdata->_data[mv][mu]->_x << " " << mdata->_data[mv][mu]->_y << " " << mdata->_data[mv][mu]->_z  << std::endl;


                /////////////////////// BC INIT

                if((i == 0) && (j == 0))
                {
                    bc.x00 = mdata->_data[mv][mu]->_x;
                    bc.y00 = mdata->_data[mv][mu]->_z;
                }

                if((i == (marks_x-1)) && (j == 0))
                {
                    bc.x10 = mdata->_data[mv][mu]->_x;
                    bc.y10 = mdata->_data[mv][mu]->_z;
                }

                if((i == (marks_x-1)) && (j == (marks_y-1)))
                {
                    bc.x11 = mdata->_data[mv][mu]->_x;
                    bc.y11 = mdata->_data[mv][mu]->_z;
                }

                if((i == 0) && (j == (marks_y-1)))
                {
                    bc.x01 = mdata->_data[mv][mu]->_x;
                    bc.y01 = mdata->_data[mv][mu]->_z;
                }
            }


    }


    construct_board_plain(mdata, bc.pA, bc.pB, bc.pC, bc.pD);


    std::cout << "BC " << std::endl;
    std::cout << bc.x00 << " " << bc.y00 << std::endl;
    std::cout << bc.x01 << " " << bc.y01 << std::endl;
    std::cout << bc.x11 << " " << bc.y11 << std::endl;
    std::cout << bc.x10 << " " << bc.y10 << std::endl;
    std::cout << "plane eq " << bc.pA << " " << bc.pB << " " << bc.pC << " " << bc.pD << std::endl;


    std::cout << "complete first loop" << std::endl;

    /// what if not found anything ?
    /// what if bc is wrong
    if(found_markers < 5)
    {
        std::cout << "ATTENTION found too few markers in callibraion procedure" << std::endl;
        CArray2D<cv::Point3f> deform_fin(marks_x, marks_y);
        return deform_fin;
    }

    ///// find origin marker in (i.j)-grid
    int oi = marks_x;
    int oj = marks_y;
    for(int j = 0; j < marks_y; j++)
    {
        for(int i = 0; i < marks_x; i++)
        {
            if(grid._data[j][i] != NULL)
            {
                if(i < oi )
                {
                    oi = i;
                    oj = j;
                }
                else if(i == oi)
                {
                    if(j < oj)
                    {
                        oi = i;
                        oj = j;
                    }
                }
            } // !NULL
        }
    }

    std::cout << "origin is at " << oi << ", " << oj  << std::endl;

    float ox = grid._data[oj][oi]->_x;
    float oy = grid._data[oj][oi]->_y;
    float oz = grid._data[oj][oi]->_z;

    CArray2D<cv::Point3f> deform_m(marks_x, marks_y);
    for(int j = 0; j < marks_y; j++)
    {
        for(int i = 0; i < marks_x; i++)
        {

            if(grid._data[j][i] != NULL)
            {
                int di = oi - i;
                int dj = j - oj;



                float real_x = grid._data[j][i]->_x;
                float real_y = grid._data[j][i]->_y;
                float real_z = grid._data[j][i]->_z;

                float proj_x = real_x;
                float proj_y = -(  bc.pA * real_x +  bc.pC * real_z +  bc.pD)/  bc.pB;
                float proj_z = real_z;




                float def_x = (ox - di * cz) - proj_x;
                float def_y = oy             - proj_y;
                float def_z = (oz - dj * cz) - proj_z;

                cv::Point3f vec(def_x, def_y, def_z);
                deform_m._data[j][i] = vec;


                std::cout << i << " " << j << " " << def_x << " " << def_y << " " << def_z << std::endl;
            }
        }
    }

    CArray2D<cv::Point3f> deform_fin(marks_x, marks_y);
    for(int j = 0; j < marks_y; j++)
    {
        for(int i = 0; i < marks_x; i++)
        {

            if(grid._data[j][i] != NULL)
            {
                deform_fin._data[j][i] = deform_m._data[j][i];

                std::cout << "orig " << i << " " << j << " " <<   deform_fin._data[j][i].x << " " <<  deform_fin._data[j][i].y << " " <<  deform_fin._data[j][i].z << std::endl;
            }
            else
            {

                float total_sum_x = 0;
                float total_sum_y = 0;
                float total_sum_z = 0;

                float total_wei = 0;
                for(int jj = 0; jj < marks_y; jj++)
                    for(int ii = 0; ii < marks_x; ii++)
                    {
                        if(grid._data[jj][ii] != NULL)
                        {
                            ///////////////
                            float wx = 2 - fabs( i - ii);
                            if(wx < 0) wx = 0;

                             float wy = 2 - fabs( j - jj);
                            if(wy < 0) wy = 0;

                            //float wy = ( j == jj)? 1 : 0;


                        /// http://stackoverflow.com/questions/10900141/fast-plane-fitting-to-many-points

                            float wei = wx * wy;

                            ///////////////
                            /* float dii = (i - ii)*(i - ii);
                            float djj = (j - jj)*(j - jj);
                            float dst = std::sqrt(dii + djj);
                            float wei = 1. / (dst*dst); */

                           total_sum_x += deform_m._data[jj][ii].x * wei;
                           total_sum_y += deform_m._data[jj][ii].y * wei;
                           total_sum_z += deform_m._data[jj][ii].z * wei;

                           total_wei += wei;

                        }

                    }

                float res_x =  total_sum_x/total_wei;
                float res_y =  total_sum_y/total_wei;
                float res_z =  total_sum_z/total_wei;

                deform_fin._data[j][i] = cv::Point3f(res_x, res_y, res_z);
                std::cout << "inter " << i << " " << j << " " <<   deform_fin._data[j][i].x << " " <<  deform_fin._data[j][i].y << " " <<  deform_fin._data[j][i].z << std::endl;

            }
        }
    }

    return deform_fin;
}


void CLaserSanOfflineThread::undistort_image(QImage & q_input_image)
{

    int xs = q_input_image.width();
    int ys = q_input_image.height();
    cv::Mat tmp2(ys, xs, CV_8UC4, (uchar*)q_input_image.bits(),q_input_image.bytesPerLine());
    cv::Mat res = cv::Mat(tmp2.rows, tmp2.cols, CV_8UC3 );
    int from_to[] = { 0,0,  1,1,  2,2 };
    cv::mixChannels( &tmp2, 1, &res, 1, from_to, 3 );
    cv::Mat inimg = cv::Mat(res, cv::Rect(0, 0, xs, ys));

    cv::Mat input_image_undistorted;
    cv::undistort(inimg, input_image_undistorted, g_cam_intrinsics, g_cam_distortion);

    QImage res_img((uchar*)input_image_undistorted.data, input_image_undistorted.cols,
                   input_image_undistorted.rows,  QImage::Format_RGB888);
    res_img = res_img.rgbSwapped();

    q_input_image = res_img;

}

QImage CLaserSanOfflineThread::read_as_undistorted_image(QString & fname)
{


    cv::Mat inimg = cv::imread(fname.toStdString(), CV_LOAD_IMAGE_UNCHANGED);

    if(inimg.cols == 0) return QImage();

    cv::Mat isrc;
    cv::undistort(inimg, isrc, g_cam_intrinsics, g_cam_distortion);


    //cv::imshow("ttl", isrc);
    //cv::waitKey(0);

    QImage res_img((uchar*)isrc.data, isrc.cols,
                   isrc.rows,  QImage::Format_RGB888);
    res_img = res_img.rgbSwapped();

   return res_img;

}


bool CLaserSanOfflineThread::do_find_runetag_in_qimage(QImage & img, cv::runetag::Pose & RT ) // undistorted
{
    std::cout << "do_find_unetag: start  "<< img.width() << " " << img.height() << std::endl;

    int xs = img.width();
    int ys = img.height();
    cv::Mat tmp2(ys, xs, CV_8UC3, (uchar*)img.bits(), img.bytesPerLine());
    cv::Mat res = cv::Mat(tmp2.rows, tmp2.cols, CV_8UC3 );
    int from_to[] = { 0,0,  1,1,  2,2 };
    cv::mixChannels( &tmp2, 1, &res, 1, from_to, 3 );
    cv::Mat frame = cv::Mat(res, cv::Rect(0, 0, xs, ys));

    cv::runetag::MarkerDetected tf;
    cv::Mat distortion = cv::Mat::zeros(1,5,CV_32F);
    float det_error = 0;
    bool res_rune = do_find_runetag(  frame, g_cam_intrinsics,
                                      distortion,
                                      _pat_detector,   _ellipseDetector, RT, tf, &det_error);
    return res_rune;

}

// Calculates rotation matrix to euler angles
// The result is the same as MATLAB except the order
// of the euler angles ( x and z are swapped ).
cv::Vec3f rotationMatrixToEulerAngles(cv::Mat &R)
{


    float sy = sqrt(R.at<double>(0,0) * R.at<double>(0,0) +  R.at<double>(1,0) * R.at<double>(1,0) );

    bool singular = sy < 1e-6; // If

    float x, y, z;
    if (!singular)
    {
        x = atan2(R.at<double>(2,1) , R.at<double>(2,2));
        y = atan2(-R.at<double>(2,0), sy);
        z = atan2(R.at<double>(1,0), R.at<double>(0,0));
    }
    else
    {
        x = atan2(-R.at<double>(1,2), R.at<double>(1,1));
        y = atan2(-R.at<double>(2,0), sy);
        z = 0;
    }
    return cv::Vec3f(x, y, z);



}

cv::Mat rot2euler(const cv::Mat & rotationMatrix)
{
    cv::Mat euler(3, 1, CV_64F);

    double m00 = rotationMatrix.at<double>(0, 0);
    double m02 = rotationMatrix.at<double>(0, 2);
    double m10 = rotationMatrix.at<double>(1, 0);
    double m11 = rotationMatrix.at<double>(1, 1);
    double m12 = rotationMatrix.at<double>(1, 2);
    double m20 = rotationMatrix.at<double>(2, 0);
    double m22 = rotationMatrix.at<double>(2, 2);

    double x, y, z;

    // Assuming the angles are in radians.
    if (m10 > 0.998) { // singularity at north pole
         x = 0;
         y = CV_PI / 2;
         z = atan2(m02, m22);
    }
    else if (m10 < -0.998) { // singularity at south pole
        x = 0;
        y = -CV_PI / 2;
        z = atan2(m02, m22);
    }
    else
    {
         x = atan2(-m12, m11);
        y = asin(m10);
        z = atan2(-m20, m00);
   }

    euler.at<double>(0) = x;
    euler.at<double>(1) = y;
    euler.at<double>(2) = z;

    return euler;
}

void getEulerAngles(cv::Mat &rotCamerMatrix, cv::Vec3d &eulerAngles){

   cv::Mat cameraMatrix,rotMatrix,transVect,rotMatrixX,rotMatrixY,rotMatrixZ;
    double* _r = rotCamerMatrix.ptr<double>();
    double projMatrix[12] = { rotCamerMatrix.at<double>(0,0),rotCamerMatrix.at<double>(0,1),rotCamerMatrix.at<double>(0,2),0,
                          rotCamerMatrix.at<double>(1,0),rotCamerMatrix.at<double>(1,1),rotCamerMatrix.at<double>(1,2),0,
                          rotCamerMatrix.at<double>(2,0),rotCamerMatrix.at<double>(2,1),rotCamerMatrix.at<double>(2,2),0};

   cv::decomposeProjectionMatrix( cv::Mat(3,4,CV_64FC1,projMatrix),
                               cameraMatrix,
                               rotMatrix,
                               transVect,
                               rotMatrixX,
                               rotMatrixY,
                               rotMatrixZ,
                               eulerAngles);
}

QImage CLaserSanOfflineThread::extract_particular_frame(cv::VideoCapture & vid,
                                std::map<int, CFrameDesc> & frame_map,
                                CFrameDesc::CFrameType typ, int & fidx)
{
    int frame_idx = 0;
    if(typ == CFrameDesc::FR )
    {
        frame_idx = fidx;
    }

    if(typ == CFrameDesc::BKG)
    {
        for(std::map<int, CFrameDesc>::iterator it = frame_map.begin();
            it != frame_map.end(); it++)
        {
            if(it->second._typ == CFrameDesc::BKG)
            {
                frame_idx = it->first;
                fidx = frame_idx;
                break;
            }
        }
    }

    if(typ == CFrameDesc::TEX)
    {
        for(std::map<int, CFrameDesc>::iterator it = frame_map.begin();
            it != frame_map.end(); it++)
        {
            if(it->second._typ == CFrameDesc::TEX)
            {
                frame_idx = it->first;
                fidx = frame_idx;
                break;
            }
        }
    }

    vid.set(CV_CAP_PROP_POS_FRAMES, frame_idx);

    cv::Mat frame;
    vid.read(frame);

    if(frame.empty())
        return QImage();

    if(typ == CFrameDesc::TEX)
    {
        cv::Mat uframe;
        cv::undistort(frame, uframe, g_cam_intrinsics, g_cam_distortion);
        frame = uframe;
    }

    QImage img((uchar*)frame.data, frame.cols, frame.rows,  QImage::Format_RGB888);
    return img.rgbSwapped();
}

void CLaserSanOfflineThread::run()
{

    /// SYNTHETIC CAMERA SETTING
    /*

    double Nx = 1280;
    double Ny = 960;
    double cx = Nx/2;
    double cy = Ny/2;

    float FF = cx / tan((BGM(56)/2.));
    std::cout << "my fx " << FF << std::endl;

    g_cam_intrinsics = (cv::Mat_<double>(3,3) <<FF, 0., cx,
                                0., FF, cy,
                                0.,  0., 1. );

    g_cam_distortion = (cv::Mat_<double>(1,5) << 3.0065502014329997e-001,
                         0, 0, 0, 0);
   */

    emit send_delete_scan();

    ///// clean all cameras
    global_keyframes_sequence.clear();

    ///// setup table position
    CGlobalHSSettings::gl_table_rot_center_x = convert_real_coord_to_gl_x(_rot_center_x);
    CGlobalHSSettings::gl_table_rot_center_z = convert_real_coord_to_gl_z(_rot_center_y);
    CGlobalHSSettings::gl_table_rot_center_y = convert_real_coord_to_gl_y(_rot_center_z);

    ///// determine image sizes
    int minimal_laser_pnts = 5;
    int max_pnts_to_send = 2500 / _decim_pnt;

    char * image_name_buf = new char[2048];

    QString first_avi_name = CLaserSanOfflineThread::construct_file_name(image_name_buf, _video_name_format, _scan_first);

    int xs = 0;
    int ys = 0;
    {
        cv::VideoCapture v(first_avi_name.toLocal8Bit().constData());
        xs = v.get(CV_CAP_PROP_FRAME_WIDTH);
        ys = v.get(CV_CAP_PROP_FRAME_HEIGHT);
        v.release();
    }

    float cam_laser_order = (_camera_laser_interchanged)?-1.0:1.0;


    float _cam_cx = g_cam_intrinsics.at<double>(0,2);
    float _cam_cy = g_cam_intrinsics.at<double>(1,2);
    float _cam_fx = g_cam_intrinsics.at<double>(0,0);
    float _cam_fy = g_cam_intrinsics.at<double>(1,1);

    std::vector<CArray2D<aux_pix_data *> *> m_scans;
    std::vector<QImage> textures;

    int total_scan_lines = 0;
    for(int sidx = _scan_first, s = 0; sidx <= _scan_last; sidx++, s++)
    {     
        clock_t msstart = clock();

        if (_abort)
        {
            emit send_back(0);
            return;
        }


        /////// initialize current map
        QString cur_desc_name = CLaserSanOfflineThread::construct_file_name(image_name_buf, _video_desc_format, sidx);
        std::map<int, CFrameDesc> frame_map;
        bool res = parse_description_video_file(cur_desc_name, frame_map);
        if(!res) continue;

        /////// initialize current avi
        QString cur_avi_name = CLaserSanOfflineThread::construct_file_name(image_name_buf, _video_name_format, sidx);
        cv::VideoCapture scan_video(cur_avi_name.toLocal8Bit().constData());
        if(!scan_video.isOpened()) continue;

        /// TODO image sequence my contain more then 2 init frames
        _img_beg_idx = 2; // 0 - b, 1 - t, all other frames

        _img_end_idx = scan_video.get(CV_CAP_PROP_FRAME_COUNT)-1;

        ///// extract background
        int bckg_idx = 0;
        QImage bckg =  extract_particular_frame(scan_video, frame_map, CFrameDesc::BKG, bckg_idx);


        ///// extract texture
        int tex_idx = 0;
        QImage tex_s = extract_particular_frame(scan_video, frame_map, CFrameDesc::TEX, tex_idx);
        textures.push_back(tex_s);

        /////// add new camera into scene
        construct_and_add_new_cframe(s, _rot_center_x, _rot_center_y, cur_avi_name, tex_idx);
        emit send_new_cam();


        CArray2D<aux_pix_data *> * merged_data = new CArray2D<aux_pix_data *>(xs, ys, NULL);

        double c_camera_x_incline = _camera_x_incline;
        double c_camera_y_incline = _camera_y_incline;
        double rotCX = _rot_center_x;
        double rotCY = _rot_center_y;
        double rotCZ = _rot_center_z;


        ///// rotation around y axis
        //float mat_10 = 0;
        double mat_11 =  cos(c_camera_x_incline);
        double mat_12 = -sin(c_camera_x_incline);

        //float mat_20 =  0;
        double mat_21 =  sin(c_camera_x_incline);
        double mat_22 =  cos(c_camera_x_incline);

        ///// rotation around x axis
        double r_mat_00 =  cos(c_camera_y_incline);
        double r_mat_02 =  sin(c_camera_y_incline);

        double r_mat_20 = -sin(c_camera_y_incline);
        double r_mat_22 =  cos(c_camera_y_incline);

        double obj_rot_angle = _sca_angle * (sidx -1 );
        double rmat_00 =  cos(obj_rot_angle);
        double rmat_01 = -sin(obj_rot_angle);
        double rmat_10 =  sin(obj_rot_angle);
        double rmat_11 =  cos(obj_rot_angle);


        int processed = 0;
        std::vector<C3DPoint> * pcl_pnts = NULL;
        #pragma omp parallel for num_threads(CGlobalHSSettings::_computing_threads)
        for(int idxF = _img_beg_idx; idxF <= _img_end_idx; idxF+=_decim_pnt)
        {

            if (_abort) continue;

            QImage img;
            int idx = _img_beg_idx;
            #pragma omp critical
            {
                idx         +=  processed;
                processed   +=_decim_pnt;
                img = extract_particular_frame(scan_video, frame_map, CFrameDesc::FR, idx);

                if(pcl_pnts == NULL)
                {
                   try
                   {
                       pcl_pnts = new  std::vector<C3DPoint>();

                   }
                   catch (std::bad_alloc& ba)
                    {
                        std::cout << "bad_alloc caught: " << ba.what() << std::endl;
                        pcl_pnts  = NULL;
                    }
               }


            }

            if(img.width() == 0)
            {
                std::cout << "reqested frame not found: " << idx << std::endl;
                continue;
            }

            difference_image_fast_supp_red(&img, &bckg, CGlobalSettings::_ignore_read_thresh);

            std::vector<CIPixel> pix = get_edge_points_lines_hills_1D_undistorted(&img, g_cam_intrinsics, g_cam_distortion);
            if(_use_corr)
                pix = do_laser_points_correction(pix);

            float laser_betta = 0;
            /*if(_compute_laser_pos)
            {

                std::vector<CIPixel> grid_pix = extract_only_grid_laser_pnts(pix);
                bool success= convert_pix_to_laser_angle(grid_pix, xs, laser_betta);
                std::cout << " frame " << idx << " laser pnts" << grid_pix.size() << " " << laser_betta << " "<< success << std::endl;
                if(!success) continue;
            }*/
            if(_use_metadata_angle)
            {

                 float meta_int_angle = frame_map[idx]._lpos;
                 laser_betta = BGM( 360. * float(meta_int_angle) /16384. ) + _meta_angle_correction ;
                // std::cout << "lpos " << _meta_angle_correction << " " << 360 * meta_int_angle/16384 << " " << IBGM(laser_betta) << " delta: " <<  IBGM(laser_betta) - 360 * meta_int_angle/16384<< std::endl;
            }
            else
                laser_betta = _las_betta_beg + _las_betta_step * float(idx-1);

            emit send_back(float(processed)*100/float(_img_end_idx));

            if(pix.size() > minimal_laser_pnts)
            {

                #pragma omp critical
                {
                    total_scan_lines++;
                }


                std::vector<CIPixel>::iterator pit = pix.begin();
                for( ; pit != pix.end(); pit++)
                {
                    int u =  pit->_u + 0.5;
                    int v =  pit->_v + 0.5;

                    C3DPoint p3d = image2space_conv_ext (cam_laser_order*_camera_laser_dist,
                                              pit->_u, pit->_v,  laser_betta,
                                               _laser_depth_offset,
                                               _cam_cx, _cam_cy,
                                               _cam_fx, _cam_fy );

                    //std::cout << "before " << pit->_u << " " << pit->_v << std::endl;
                   //  std::cout << "after  " << _cam_fx *p3d._x /p3d._y + _cam_cx << " "<< 960 - ( _cam_fy *p3d._z /p3d._y  + (960- _cam_cy))  <<  std::endl;


                    #pragma omp critical
                    {
                       /// std::cout << idx <<": " << IBGM(laser_betta) << " " << pit->_u  << " "<< pit->_v << " " << p3d._x  << " " << p3d._y  << " " << p3d._z  <<  std::endl;

                        // rotate back from camera inclanation y
                        float nx = p3d._x * r_mat_00 + p3d._z * r_mat_02; ;
                        float ny = p3d._y;
                        float nz = p3d._x * r_mat_20 + p3d._z * r_mat_22;

                        // rotate back from camera inclanation x
                        float nnx = nx;
                        float nny = ny * mat_11 + nz * mat_12;
                        float nnz = ny * mat_21 + nz * mat_22;

                        nx = nnx  - rotCX ;
                        ny = nny  - rotCY ;
                        nz = nnz  ;


                        float rnx = nx * rmat_00 + ny * rmat_01;
                        float rny = nx * rmat_10 + ny * rmat_11;
                        float rnz = nz;

                        double radius = rnx*rnx + rny * rny;

                        nx =  rnx + rotCX ;
                        ny =  rny + rotCY ;
                        nz =  rnz  ;


                        if ( (radius  <  ( _max_depth_to_cancel  *_max_depth_to_cancel)) && // dont recontruct outside the table
                           ( nz > -(_rot_center_z  - 3)))  // dont reconstruct below the table
                        {

                               if(merged_data->_data[v][u] == NULL)
                                    merged_data->_data[v][u] = new aux_pix_data;
                               merged_data->_data[v][u]->_u   =  pit->_u;
                               merged_data->_data[v][u]->_v   =  pit->_v;
                               merged_data->_data[v][u]->_idx =  idx;

                                merged_data->_data[v][u]->_x = nx;
                                merged_data->_data[v][u]->_y = ny;
                                merged_data->_data[v][u]->_z = nz;

                                merged_data->_data[v][u]->_gx = convert_real_coord_to_gl_x(nx);
                                merged_data->_data[v][u]->_gy = convert_real_coord_to_gl_y(-nz);
                                merged_data->_data[v][u]->_gz = convert_real_coord_to_gl_z(ny);

                                //// shift in real coordinates
                                p3d._x = merged_data->_data[v][u]->_gx;
                                p3d._y = merged_data->_data[v][u]->_gy;
                                p3d._z = merged_data->_data[v][u]->_gz;
                                //////////////////////////////////////

                               if(tex_s.width() !=0)
                               {
                                    QRgb value = tex_s.pixel(u,v);

                                    p3d._r =  qRed(value)/ 255.;
                                    p3d._g =  qGreen(value)/ 255.;
                                    p3d._b =  qBlue(value)/255.;
                               }

                               if( pcl_pnts != NULL)
                                    pcl_pnts->push_back(p3d);
                        } // pnt is inside the region


                    }//omp critical
               } // end of for pix

                #pragma omp critical
                {
                    if(pcl_pnts !=NULL)
                    {
                        if(pcl_pnts->size() > max_pnts_to_send)
                        {
                            emit send_result(pcl_pnts);


                            try
                            {
                                pcl_pnts = new  std::vector<C3DPoint>();

                            }
                            catch (std::bad_alloc& ba)
                             {
                               std::cout << "bad_alloc caught: " << ba.what() << std::endl;
                                pcl_pnts  = NULL;
                             }
                        }
                     }
                }
            } /// if there are pixels
            std::cout << "reading " << idx << " " << pix.size() << std::endl;


        } /// for - read next image
        if(pcl_pnts != NULL) emit send_result(pcl_pnts); ///// DOSYL OSTATKOV
        m_scans.push_back(merged_data);
        scan_video.release();

        clock_t mffinish = clock();
        int sec_conv = (mffinish - msstart);
        std::cout << "Done. Total computing time: "  << sec_conv << " secs: " << ((float)sec_conv )/CLOCKS_PER_SEC << std::endl;


    } /// read next scan

    if (_abort)
    {
        emit send_back(0);
        return;
    }

    /// clear single cloud
    emit send_delete_scan();

    set_camera_matrix_for_texturing();


    std::vector<cv::Point3f> mcolors; // mesh color map
    cv::Point3f c1(0., 153./255.,1.);
    cv::Point3f c2(1., 102./255.,0.);
    cv::Point3f c3(1., 204./255.,0.);
    cv::Point3f c4(1., 153./255.,0.);
    cv::Point3f c5(204./255, 102./255.,1.);
    cv::Point3f c6(227./255, 64./255.,0.);
    cv::Point3f c7(0., 222./255.,222./256.);
    cv::Point3f c8(121./256., 240./255.,108./256.);

    mcolors.push_back(c1);
    mcolors.push_back(c2);
    mcolors.push_back(c3);
    mcolors.push_back(c4);
    mcolors.push_back(c5);
    mcolors.push_back(c6);
    mcolors.push_back(c7);
    mcolors.push_back(c8);

    int mcolors_sz = mcolors.size();

    std::vector<CArray2D<aux_pix_data *> *>::iterator its = m_scans.begin();
    for(int s = 0 ; its != m_scans.end()  ; its++, s++)
    {

        if (_abort)
        {
            emit send_back(0);
            return;
        }
        emit send_back(float(s+1)*100/float(m_scans.size()));

        CArray2D<aux_pix_data *> * merged_data = *its;

        QImage tex = textures[s];

        std::vector<C3DPoint> * pcl = prepare_pcl_with_norms(merged_data, s,
                        mcolors[s%mcolors_sz].x,
                        mcolors[s%mcolors_sz].y,
                        mcolors[s%mcolors_sz].z,
                        1, tex);

        emit send_data_for_cglobject(pcl, NULL, mcolors[s%mcolors_sz].x,
                                                mcolors[s%mcolors_sz].y,
                                                mcolors[s%mcolors_sz].z);

    }

    ///////////////////////////
    //// cleaning results
    ///////////////////////////
    its = m_scans.begin();
    for( ; its != m_scans.end()  ; its++  )
    {
        CArray2D<aux_pix_data *> * merged_data = *its;
        delete merged_data;
    }

    delete [] image_name_buf;

    emit valid_result();
}

void CLaserSanOfflineThread::run_old()
{

    ////////////// dummy part
    /*QString gg = _path + QString("/angles_o.log");
    std::ifstream ifs(gg.toStdString().c_str());
    std::string line;
    int idx = 0;
    while ( getline (ifs,line) )
    {

        std::stringstream ss(line);

        std::string substr;
        ss >> substr;
        int idx = atoi(substr.c_str()) ;

        ss >> substr;
        float angle = atof(substr.c_str()) ;


        _dummy_angles[idx] = angle;


    }*/
    ///////////////////////////////////////////

    clock_t msstart = clock();

    ///////////// dummy camera

    CFrame * cframe = new CFrame ;
    cframe ->_global_pos = (cv::Mat_<double>(3,1) << 0,0,0);
    cframe ->_glob_rot_vec = (cv::Mat_<double>(1,3) << -_camera_x_incline, 0, 0);
    cframe->_valid_coord = true;
    global_keyframes_sequence.clear();
    global_keyframes_sequence.push_back(cframe);


    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////


    ///// determine image sizes
    int minimal_laser_pnts = 5;
    char * image_name_buf = new char[2048];
    QString dummy_img_name = construct_image_name(image_name_buf, _scan_first, _img_beg_idx);
    QImage dummy_img(dummy_img_name);
    int xs = dummy_img.width();
    int ys = dummy_img.height();



    //////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////

    float cam_laser_order = (_camera_laser_interchanged)?-1.0:1.0;
    std::cout << "CAM LAS ORDER " <<  cam_laser_order << " " << IBGM(_camera_x_incline) << std::endl;

    //float mat_00 = 1;
    //float mat_01 = 0;
    //float mat_02 = 0;

    //float mat_10 = 0;
    float mat_11 =  cos(_camera_x_incline);
    float mat_12 = -sin(_camera_x_incline);

    //float mat_20 =  0;
    float mat_21 =  sin(_camera_x_incline);
    float mat_22 =  cos(_camera_x_incline);


    // camera_y_incline
    ////float  _rotation_axis_y = BGM(-2.13866);
    float r_mat_00 =  cos(_camera_y_incline);
    float r_mat_02 =  sin(_camera_y_incline);

    float r_mat_20 = -sin(_camera_y_incline);
    float r_mat_22 =  cos(_camera_y_incline);

    std::vector<CArray2D<aux_pix_data *> *> m_scans;
    std::vector<QImage> textures;

    int total_scan_lines = 0;

    for(int sidx = _scan_first; sidx <= _scan_last; sidx++)
    {

        QString bckg_name  = construct_bckg_name(image_name_buf, sidx);
        QImage bckg;
        if(!_is_difference) bckg = QImage(bckg_name);

        if(_compute_laser_pos) extract_grid_points(bckg_name);


        QImage tex_s;
        if(_tex_name_format.size() != 0)
        {
            QString tex_name = construct_tex_name(image_name_buf, sidx);
            tex_s = QImage(tex_name);
            textures.push_back(tex_s);
        }


        CArray2D<aux_pix_data *> * merged_data = new CArray2D<aux_pix_data *>(xs, ys, NULL);

        int processed = 0;

         std::ofstream dbg_A("angle.log", std::ofstream::out); /////////////// REMOVE

        #pragma omp parallel for num_threads(CGlobalHSSettings::_computing_threads)
        for(int idxF = _img_beg_idx; idxF <= _img_end_idx; idxF++)
        {

            int idx = _img_beg_idx;
            #pragma omp critical
            {
                idx+=  processed;
                processed++;
            }

            if (_abort) continue;

            srand(idx);
            float line_r = float(rand() % 100)/99.0 ;
            float line_g = float(rand() % 100)/99.0 ;
            float line_b = float(rand() % 100)/99.0 ;

            QString filname = construct_image_name(image_name_buf, sidx, idx);
            QImage img(filname);
            if(img.width() == 0)
            {
                std::cout << "reuested input image not found " << filname.toStdString() << std::endl;
                continue;
            }

            if(!_is_difference)
            {

                difference_image_fast_supp_red(&img, &bckg, CGlobalSettings::_ignore_read_thresh);
                /// difference_image_fast(&img, &bckg);
            }
            std::vector<CIPixel> pix = get_edge_points_lines_hills_1D(&img);
            if(_use_corr)
                pix = do_laser_points_correction(pix);

            float laser_betta = 0;
            if(_compute_laser_pos)
            {
                // apply found detection here
                std::vector<CIPixel> grid_pix = extract_only_grid_laser_pnts(pix);

                bool success= convert_pix_to_laser_angle(grid_pix, xs, laser_betta);
                
                ////laser_betta -= float(idx) * 0.000015;
                
                std::cout << " frame " << idx << " laser pnts" << grid_pix.size() << " " << laser_betta << " "<< success << std::endl;
                if(!success) continue;


            }
            else if(_use_metadata_angle)
            {
                 /// float meta_angle = read_laserangle_metadata(filname);
                 /// laser_betta = BGM(meta_angle) ;
               //// std::cout << "aa deg " << aa << " " << << std::endl;


                //laser_betta = BGM(  _dummy_angles[idx]) ; //// READ FROM FILE


                 //// reqrite with meta
                 float meta_int_angle = read_laserangle_metadata(filname, QString("lpos"));
                 laser_betta = BGM( 360. * float(meta_int_angle) /16384. ) + _meta_angle_correction ;
                 std::cout << "measured angle " << laser_betta << std::endl;

                 std::cout << "lpos " << _meta_angle_correction << " " << 360 * meta_int_angle/16384 << " " << IBGM(laser_betta) << " delta: " <<  IBGM(laser_betta) - 360 * meta_int_angle/16384<< std::endl;
                 ////////// debug


            }
            else
                laser_betta = _las_betta_beg + _las_betta_step * float(idx);

            //#pragma omp critical
            //{
                emit send_back(float(processed)*100/float(_img_end_idx));

            if(pix.size() > minimal_laser_pnts)
            {

                #pragma omp critical
                {
                    total_scan_lines++;
                   /// dbg_A << total_scan_lines << " " << IBGM(laser_betta ) << " " << filname.toStdString() << std::endl;
                }

                std::vector<C3DPoint> * pcl_pnts =  new std::vector<C3DPoint>;
                std::vector<CIPixel>::iterator pit = pix.begin();
                for( ; pit != pix.end(); pit++)
                {
                    int u =  pit->_u + 0.5;
                    int v =  pit->_v + 0.5;

                    C3DPoint p3d = image2space_conv(cam_laser_order*_camera_laser_dist,
                                             pit->_u,
                                             pit->_v,
                                             laser_betta,
                                             _laser_depth_offset,
                                             xs, ys,
                                             _fov_horiz, _fov_vert,
                                             0);

                  if ((p3d._y < _min_depth_to_cancel) || (p3d._y > _max_depth_to_cancel) ) continue;

                    /// initialize laser flick poisition (u,v)

                     #pragma omp critical
                     {
                      int tidx = -1;
                      int ttidx = -1;
                       if(merged_data->_data[v][u] == NULL)
                            merged_data->_data[v][u] = new aux_pix_data;
                       else
                           tidx = merged_data->_data[v][u]->_idx;

                       if(tidx < idx)
                       {
                            merged_data->_data[v][u]->_u   =  pit->_u;
                            merged_data->_data[v][u]->_v   =  pit->_v;
                            merged_data->_data[v][u]->_idx =  idx;
                       }
                       /// avoid aliasing - fill neighbour pixel
                       int nu = int(pit->_u);
                       if( (nu != u)   )
                       {

                           if(merged_data->_data[v][nu] == NULL)
                                merged_data->_data[v][nu] = new aux_pix_data;
                            else
                               ttidx = merged_data->_data[v][nu] ;

                           if(ttidx < idx)
                           {
                                merged_data->_data[v][nu]->_u   =  pit->_u;
                                merged_data->_data[v][nu]->_v   =  pit->_v;
                                merged_data->_data[v][nu]->_idx =  idx;
                            }
                       }

                    // rotate back from camera inclanation
                    float nx =  p3d._x; // * mat_00 +   p3d._y * mat_01 + p3d._z * mat_02;
                    float ny =  p3d._y * mat_11 + p3d._z * mat_12;
                    float nz =  p3d._y * mat_21 + p3d._z * mat_22;



                    float nnx = nx * r_mat_00 + nz * r_mat_02;
                    float nny = ny;
                    float nnz = nx * r_mat_20 + nz * r_mat_22;


                    nx = nnx;
                    ny = nny;
                    nz = nnz;
                    if(_use_adeform)
                    {
                        /// g_3d_corrector.do_correction(nx, ny, nz,  pit->_u, pit->_v);

                        ///g_3d_corrector.do_correction2d(nx, ny, nz);
                        g_3d_corrector.do_correction3d(nx, ny, nz);
                    }

                    if(tidx < idx)
                    {
                            merged_data->_data[v][u]->_bx = p3d._x;
                            merged_data->_data[v][u]->_by = p3d._y;
                            merged_data->_data[v][u]->_bz = p3d._z;

                            merged_data->_data[v][u]->_x = nx;
                            merged_data->_data[v][u]->_y = ny;
                            merged_data->_data[v][u]->_z = nz;

                           // merged_data->_data[v][u]->_gx =  gl_scale * nx + gl_shift_x;
                           // merged_data->_data[v][u]->_gy =  gl_scale * nz + gl_shift_y;
                           // merged_data->_data[v][u]->_gz = -gl_scale * ny + gl_shift_z;

                            merged_data->_data[v][u]->_gx = convert_real_coord_to_gl_x(nx);
                            merged_data->_data[v][u]->_gy = convert_real_coord_to_gl_y(-nz);
                            merged_data->_data[v][u]->_gz = convert_real_coord_to_gl_z(ny);
                    }
                    /// avoid aliasing - fill neighbour pixel
                    if( (nu != u) )
                    {
                        if(ttidx < idx )
                        {
                            merged_data->_data[v][nu]->_bx = p3d._x;
                            merged_data->_data[v][nu]->_by = p3d._y;
                            merged_data->_data[v][nu]->_bz = p3d._z;

                            merged_data->_data[v][nu]->_x = nx;
                            merged_data->_data[v][nu]->_y = ny;
                            merged_data->_data[v][nu]->_z = nz;

                            merged_data->_data[v][nu]->_gx = merged_data->_data[v][u]->_gx;
                            merged_data->_data[v][nu]->_gy = merged_data->_data[v][u]->_gy;
                            merged_data->_data[v][nu]->_gz = merged_data->_data[v][u]->_gz;
                        }
                    }

                    //// shift in real coordinates
                    p3d._x = merged_data->_data[v][u]->_gx;
                    p3d._y = merged_data->_data[v][u]->_gy;
                    p3d._z = merged_data->_data[v][u]->_gz;
                    //////////////////////////////////////


                    if(p3d._y  < CGlobalHSSettings::gl_table_rot_center_y) // bb
                        CGlobalHSSettings::gl_table_rot_center_y = p3d._y ;

                    ///if(_random_line_color)
                    ///{
                    ///    p3d._r = line_r;
                    ///    p3d._g = line_g;
                    ///    p3d._b = line_b;
                    ///}
                    ///else
                    ///{
                       if(tex_s.width() !=0)
                       {
                            QRgb value = tex_s.pixel(u,v);

                            p3d._r =  qRed(value)/ 255.;
                            p3d._g =  qGreen(value)/ 255.;
                            p3d._b =  qBlue(value)/255.;
                       }
                    ///}

                    }//omp critical
                    pcl_pnts->push_back(p3d);


                } // end of for pix


                emit send_result(pcl_pnts);



            } /// if there are pixels
            std::cout << "reading " << idx << " " << pix.size() << std::endl;

            //} // critical section

        } /// for - read next image
        dbg_A.close();

        emit send_clean_scan();
        m_scans.push_back(merged_data);

        /*if( sidx == _scan_first)
        {
            QString filname = QString("merged_map.raw");
            std::ofstream dataf;
            dataf.open (filname.toStdString().c_str(), std::ios::out| std::ios::binary);
            for (int j=0; j < ys; j++)
            for (int i=0; i < xs; i++)
            {

              float val =  (merged_data->_data[j][i]!= NULL)? merged_data->_data[j][i]->_y: -10;
              dataf.write((char*)&val,  sizeof(float));
            }
             dataf.close();
        }
        std::vector<C3DPoint> * pcl = prepare_pcl(merged_data, 0, 0, 0, 0,
                        gl_scale, gl_shift_x, gl_shift_y, gl_shift_z,
                        100,
                        0,
                        100,
                        1, tex_s);
        emit send_result(pcl);
        return;*/


    } /// read next scan
    if (_abort)
    {
        emit send_back(0);
        return;
    }

    if(_is_calib_mode)
    {
        std::cout << "CALLIBRATION MODE" << std::endl;


        int marks_x = 10;
        int marks_y = 12;
        float cz    = 10; // mm

        QString calib_imgname = _path + QString("/bckg_m.jpg"); // todo parameters

        std::vector<cv::Point2f> _calib_corners;
        _calib_corners = find_calibration_board(marks_x, marks_y, calib_imgname);

        g_3d_corrector.set_up_corners(_calib_corners);

      //  CArray2D<int > lbls =  contruct_label_map(xs, ys, marks_x, marks_y, _calib_corners);
      //  g_3d_corrector.set_up_lblmap(lbls);



       ///std::vector<board_corners> bc =  construct_board_corners(m_scans,_calib_corners, marks_x, marks_y);
       ////g_3d_corrector.set_ext_corners(bc);

        board_corners bc;
        CArray2D<cv::Point3f> deform_pattern = construct_deform_pattern(m_scans,
                                                                        _calib_corners, marks_x, marks_y, cz, bc);
        g_3d_corrector.set_up_correction_level(deform_pattern);
        g_3d_corrector.set_ext_corners(bc);


        return;
    }
    _max_scan_lines = total_scan_lines/(_scan_last - _scan_first + 1);

    ////////////////////////////////
    //////// define projection matrix
    // actually we can construct the K-matrix
    // and use those with the smallest proejction error
    compute_camera_matrix_K(m_scans[0]);

    if (_abort)
    {
        emit send_back(0);
        return;
    }
    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////

    float rxc = 0;
    float ryc = 0;
    int scan_count = m_scans.size();
    if(scan_count > 1)
    {

        int s = 0;

        CFrame * prev_frame = new CFrame(s);
        prev_frame->_merged_data = m_scans[s];
        emit send_back(0);

        std::vector<cv::Point3f> icp_results;

        while(s <  scan_count)
        {
            std::cout << "frame " << s << std::endl;
            ////////////////////////////////////////////////////////////
            /////////////////// rotate for scan_angle_step current cloud
            ////////////////////////////////////////////////////////////
            CArray2D<aux_pix_data *> * merged_data = m_scans[(s+1)%scan_count];
            float scan_angle = _sca_angle;
            float rmat_00 =  cos(scan_angle);
            float rmat_01 = -sin(scan_angle);
            float rmat_10 =  sin(scan_angle);
            float rmat_11 =  cos(scan_angle);


            for(int v = 0; v < ys; v++)
            {
                for(int u = 0; u < xs; u++)
                {
                    if(merged_data->_data[v][u] != NULL)
                    {
                        float nx = merged_data->_data[v][u]->_x * rmat_00 +
                                   merged_data->_data[v][u]->_y * rmat_01;
                        float ny = merged_data->_data[v][u]->_x * rmat_10 +
                                   merged_data->_data[v][u]->_y * rmat_11;
                        float nz = merged_data->_data[v][u]->_z;

                        merged_data->_data[v][u]->_bx = merged_data->_data[v][u]->_x;
                        merged_data->_data[v][u]->_by = merged_data->_data[v][u]->_y;
                        merged_data->_data[v][u]->_bz = merged_data->_data[v][u]->_z;

                        merged_data->_data[v][u]->_x = nx;
                        merged_data->_data[v][u]->_y = ny;
                        merged_data->_data[v][u]->_z = nz;

                    }
                }
            }


            ////////////////////////////////////////////////////////////

            CFrame * curr_frame = new CFrame((s+1)%scan_count);
            curr_frame->_merged_data = m_scans[(s+1)%scan_count];


            ////////////////////////////////////////
            //////////   novel ICP run
            ////////////////////////////////////////
            cv::Point3f pcenter =  prev_frame->compute_bb_center_with_RANSAC();
            cv::Point3f ccenter =  curr_frame->compute_bb_center_with_RANSAC();
            std::cout << "INIT ICP " << pcenter.x - ccenter.x << " " <<  pcenter.y - ccenter.y  << std::endl;

            CArray2D<float> * cmap = NULL;
            CArray2D<float> * pmap = NULL;
            CArray2D<float> * cmap_x = NULL;
            CArray2D<float> * pmap_x = NULL;
            float  fwidth = 0;
            int    rwidth = 0;
            int    rheight = 0;
            float  p_c_bb_min_x_diff = 0;

            curr_frame->find_2d_offset_with_correlation_init(prev_frame,
                     cmap,  pmap, cmap_x,  pmap_x, fwidth,  rwidth, rheight,  p_c_bb_min_x_diff);


            float iny = pcenter.y - ccenter.y ;
            float iny_step = 10 ; //mm
            int ysteps  = 4;
            int its     = 4;

            float besty = 0;
            float bestx = 0;
            float bestmeas = 0;
            float prev_bestmeas = 0;
            for(int iteration = 0; iteration < its; iteration++)
            {
                if (_abort)
                {
                    emit send_back(0);
                    return;
                }
                for(int yy = -ysteps; yy <= ysteps; yy++)
                {
                    float iny_current = iny + yy * iny_step;
                    int measure = 0;
                    cv::Point3f off = curr_frame->find_2d_offset_with_correlation_step(cmap, pmap,
                         cmap_x,  pmap_x, fwidth, rwidth, rheight, iny_current,  p_c_bb_min_x_diff, measure );

                    if(measure > bestmeas)
                    {
                         bestx = off.x;
                         besty = iny_current;
                         bestmeas = measure;
                    }
                }
                std::cout << "best y-value " <<  besty <<   " " << bestx << " " << bestmeas << " " << iny_step << std::endl;
                emit send_back(float(iteration+1)*100/float(its));

                //
                if(bestmeas > prev_bestmeas )
                {
                    iny = besty;
                }
                iny_step = iny_step/ysteps ;
                prev_bestmeas =  bestmeas;
             }

             delete cmap;
             delete pmap;
             delete cmap_x;
             delete pmap_x;

             cv::Point3f icp_offset;
             icp_offset.x = bestx; //////192; //
             icp_offset.y = besty;//80;  //  // //
             std::cout << " ICP result " << icp_offset.x << " " << icp_offset.y << std::endl;

             icp_results.push_back(icp_offset);

            emit send_clean_scan();

            construct_pcl(prev_frame->_merged_data, 0, 0, 0, s,
                          0.,  1., 0.);
            construct_pcl(curr_frame->_merged_data, icp_offset.x, icp_offset.y, 0, (s+1)%scan_count,
                         1., 0., 0.);

            ////////////////////////////////////////////////////////////
            /////////////////// reset back scan_angle_step current cloud
            ////////////////////////////////////////////////////////////
            {
                CArray2D<aux_pix_data *> * merged_data = curr_frame->_merged_data;

                for(int v = 0; v < ys; v++)
                {
                    for(int u = 0; u < xs; u++)
                    {
                        if(merged_data->_data[v][u] != NULL)
                        {
                            merged_data->_data[v][u]->_x = merged_data->_data[v][u]->_bx ;
                            merged_data->_data[v][u]->_y = merged_data->_data[v][u]->_by;
                            merged_data->_data[v][u]->_z = merged_data->_data[v][u]->_bz ;

                        }
                    }
                }
            }
            ////////////////////////////////////////////////////////////


            delete prev_frame;
            prev_frame = curr_frame;

            s++;
        }
        delete prev_frame;

        ////////////////////////////////////////////////////////////
        ////////////////// compute single rotation center
        /////////////////////////////////////////////////////////////
        std::vector<cv::Point3f> rot_center_cands;
        std::vector<cv::Point3f>::iterator itc = icp_results.begin();
        int c = 1;
        for(; itc != icp_results.end() ; itc++, c++ )
        {
            if((!false) && (c == icp_results.size()))
            {
                 c--;
                 break;
            }

            float scan_angle = _sca_angle ;

            float rmat_00 =  0.5;
            float rmat_01 =  0.5 * sin(scan_angle)/(cos(scan_angle) - 1.);
            float rmat_10 =  -0.5* sin(scan_angle)/(cos(scan_angle) - 1.);
            float rmat_11 =  0.5;

            cv::Point3f delta = *itc;
            float xc = rmat_00 * delta.x + rmat_01 * delta.y;
            float yc = rmat_10 * delta.x + rmat_11 * delta.y;

            std::cout << "rotation center  " << c << " : " << xc << " " << yc << std::endl;

            rxc += xc;
            ryc += yc;

        }
        rxc /= float(c);
        ryc /= float(c);


        std::cout << "FINAL rotation center  " <<   rxc << " " << ryc << std::endl;
        emit send_delete_scan();

    } /// found rotation center if
    else
    {

        CFrame * curr_frame = new CFrame(0);
        curr_frame->_merged_data = m_scans[0];
        cv::Point3f ccenter =  curr_frame->compute_bb_center_with_RANSAC();
        float ymax = curr_frame->compute_farest_dist();
        rxc = ccenter.x;
        ryc = ymax; //ccenter.y;

        std::cout << "one scan " << rxc << " " << ryc << std::endl;
        delete curr_frame;
    }


    ///////////////////////////////////////////////
    /// recompute points for found rotation center
    //////////////////////////////////////////////
    global_keyframes_sequence.clear();
    emit send_delete_scan();


    std::vector<CArray2D<aux_pix_data *> *>::iterator it = m_scans.begin();
    for(int s = 0 ; it != m_scans.end()  ; it++, s++)
    {

        emit send_back(float(s+1)*100/float(m_scans.size()));
        CArray2D<aux_pix_data *> * merged_data = *it;

        //////// matrix for back rotation
        float scan_angle = _sca_angle * float(s);
        float rmat_00 =  cos(scan_angle);
        float rmat_01 = -sin(scan_angle);
        float rmat_10 =  sin(scan_angle);
        float rmat_11 =  cos(scan_angle);
        for(int v = 0; v < ys; v++)
        {
            for(int u = 0; u < xs; u++)
            {
                if(merged_data->_data[v][u] != NULL)
                {
                    float nx = (merged_data->_data[v][u]->_x - rxc) * rmat_00 +
                               (merged_data->_data[v][u]->_y - ryc) * rmat_01;
                    float ny = (merged_data->_data[v][u]->_x - rxc) * rmat_10 +
                               (merged_data->_data[v][u]->_y - ryc) * rmat_11;
                    float nz = merged_data->_data[v][u]->_z;

                    merged_data->_data[v][u]->_x = nx + rxc;
                    merged_data->_data[v][u]->_y = ny + ryc;
                    merged_data->_data[v][u]->_z = nz;

                }
            }
        }
        QString vname ;
        construct_and_add_new_cframe(s, rxc, ryc, vname, 0);
    }

    ///// update global ratation ceneter
     CGlobalHSSettings::gl_table_rot_center_x = convert_real_coord_to_gl_x(rxc);
     CGlobalHSSettings::gl_table_rot_center_z = convert_real_coord_to_gl_z(ryc);

    ///////////////////////////
    //////  output gl graphics
    ///////////////////////////


    std::vector<cv::Point3f> mcolors; // mesh color map
    cv::Point3f c1(0., 153./255.,1.);
    cv::Point3f c2(1., 102./255.,0.);
    cv::Point3f c3(1., 204./255.,0.);
    cv::Point3f c4(1., 153./255.,0.);
    cv::Point3f c5(204./255, 102./255.,1.);
    cv::Point3f c6(227./255, 64./255.,0.);
    cv::Point3f c7(0., 222./255.,222./256.);
    cv::Point3f c8(121./256., 240./255.,108./256.);

    mcolors.push_back(c1);
    mcolors.push_back(c2);
    mcolors.push_back(c3);
    mcolors.push_back(c4);
    mcolors.push_back(c5);
    mcolors.push_back(c6);
    mcolors.push_back(c7);
    mcolors.push_back(c8);

    int mcolors_sz = mcolors.size();

    std::vector<CArray2D<aux_pix_data *> *>::iterator its = m_scans.begin();
    for(int s = 0 ; its != m_scans.end()  ; its++, s++)
    {
        if (_abort)
        {
            emit send_back(0);
            return;
        }
        emit send_back(float(s+1)*100/float(m_scans.size()));

        CArray2D<aux_pix_data *> * merged_data = *its;

        //if(!_do_mesh)
        //{
        QImage tex;
        if(textures.size() == m_scans.size())
                                tex = textures[s];

        std::vector<C3DPoint> * pcl = prepare_pcl_with_norms(merged_data, s,
                        mcolors[s%mcolors_sz].x,
                        mcolors[s%mcolors_sz].y,
                        mcolors[s%mcolors_sz].z,
                        1, tex);


        /* QString filname = QString("mdata_f.raw");
        std::ofstream dataf;
        dataf.open (filname.toStdString().c_str(), std::ios::out| std::ios::binary);
        for (int j=0; j < ys; j++)
        for (int i=0; i < xs; i++)
        {

          float val =  (merged_data->_data[j][i]!= NULL)? 100: -10;
          dataf.write((char*)&val,  sizeof(float));
        }
         dataf.close();*/

        // }
       // else
        //{
             /*CTriangularMesh * mmesh = NULL; // donot construct mesh
*/

             CTriangularMesh * mmesh = NULL;

            if(_do_mesh)
            {
                fill_gaps_in_merged_data_triv(*merged_data);

                CTriMesh * rmesh = construct_mesh_from_single_shot(*merged_data,  _decim_pnt );

                /// QString tex_filname = work_path + QString("/tex%1.png").arg(s+1);
                /// QImage  texi(tex_filname);
                construct_texture(*merged_data, rmesh,  tex);

                mmesh = new CTriangularMesh;
                mmesh->use_loc_coloring();
                //mmesh->_color[0] = mcolors[s%mcolors_sz].x;
                //mmesh->_color[1] = mcolors[s%mcolors_sz].y;
                //mmesh->_color[2] = mcolors[s%mcolors_sz].z;

                mmesh->set_mesh(rmesh);
            }
            emit send_data_for_cglobject(pcl, mmesh,mcolors[s%mcolors_sz].x, mcolors[s%mcolors_sz].y, mcolors[s%mcolors_sz].z);
       // }
    }



    /////////////////////////////////////////////////////////
    compute_polar_map_for_fused_mesh(  m_scans, rxc, ryc);


    /////////////////////////////////////////////////////////



    ///////////////////////////
    //// cleaning results
    ///////////////////////////
    its = m_scans.begin();
    for( ; its != m_scans.end()  ; its++  )
    {
        CArray2D<aux_pix_data *> * merged_data = *its;
        ///for (int j=0; j < ys; j++)
        ///        for (int i=0; i < xs; i++);
        ///             if (merged_data->_data[j][i]!= NULL)
        ///                 delete merged_data->_data[j][i];
        delete merged_data;
    }

    delete [] image_name_buf;

    clock_t mffinish = clock();
    int sec_conv = (mffinish - msstart);
    std::cout << "Done. Total computing time: "  << sec_conv << " secs: " << ((float)sec_conv )/CLOCKS_PER_SEC << std::endl;
}

void CLaserSanOfflineThread::compute_polar_map_for_fused_mesh( std::vector<CArray2D<aux_pix_data *> *> & m_scans, float rxc, float ryc)
{

    ////////////////////////////////////////////
    ////////// step 1. find scene paramters


    float rc_gl_x = convert_real_coord_to_gl_x(rxc);
    float rc_gl_z = convert_real_coord_to_gl_z(ryc);

    float rad_min_ponts = 10e15;
    float rad_max_ponts = 0;

    int proj_v_min = 10e15;
    int proj_v_max = 0;

    float min_gl_y = 10e15;
    float max_gl_y = 0;

    std::vector<CArray2D<aux_pix_data *> *>::iterator its = m_scans.begin();
    for(int s = 0 ; its != m_scans.end()  ; its++, s++)
    {
        CArray2D<aux_pix_data *> * merged_data = *its;

        int xs = merged_data->_xs;
        int ys = merged_data->_ys;

        for (int y = 0; y < ys; y++)
        {
            for (int x = 0; x < xs; x++)
            {
                if(merged_data->_data[y][x] != NULL)
                {
                    aux_pix_data * data = merged_data->_data[y][x];

                    float dx = (data->_gx - rc_gl_x);
                    float dx_dx = dx * dx;

                    float dz = (data->_gz - rc_gl_z);
                    float dz_dz = dz * dz;

                    float radius = std::sqrt(dx_dx + dz_dz);

                    if(radius < rad_min_ponts) rad_min_ponts = radius;
                    if(radius > rad_max_ponts) rad_max_ponts = radius;
                    if(y < proj_v_min) proj_v_min = y;
                    if(y > proj_v_max) proj_v_max = y;
                    if(data->_gy < min_gl_y) min_gl_y = data->_gy ;
                    if(data->_gy > max_gl_y) max_gl_y = data->_gy ;
                }
            }
        }

    }

    std::cout << "rad_min_ponts = " << rad_min_ponts << std::endl;
    std::cout << "rad_max_ponts = " << rad_max_ponts << std::endl;
    std::cout << "proj_v_min = " << proj_v_min << std::endl;
    std::cout << "proj_v_max = " << proj_v_max << std::endl;
    std::cout << "min_gl_y = " << min_gl_y << std::endl;
    std::cout << "max_gl_y = " << max_gl_y << std::endl;
    std::cout << "_max_scan_lines = " << _max_scan_lines << std::endl;


    ////////////// clean global polar map
    clean_g_polar_map();

    ////////////////////////////////////////////
    /////////// step 2. construct polar_map

    // the value should be computed dynamically
    float polar_angle_step_deg = 180./float(_max_scan_lines); //400.f
    float polar_angle_step = BGM(polar_angle_step_deg);
    if(m_scans.size() == 1)
    {
        polar_angle_step_deg = polar_angle_step_deg/2;
        polar_angle_step = polar_angle_step/2;

    }
    int ploar_map_width = 360./polar_angle_step_deg;
    int polar_map_height = proj_v_max - proj_v_min;

    float bb_radius = rad_max_ponts *1.5;
    float tan_cam_incl = std::tan(std::fabs(2*_camera_x_incline));

    std::cout << "!!!!! tan_cam_incl " << tan_cam_incl  << std::endl;
    float bb_gl_y_min = min_gl_y + tan_cam_incl * ( bb_radius - rad_max_ponts);
    float bb_gl_y_max = max_gl_y + tan_cam_incl * ( bb_radius - rad_min_ponts);
    g_polar_map = new CArray2D< std::list<aux_pix_data *> > (ploar_map_width, polar_map_height);

    std::cout << "ploar_map_width= " << ploar_map_width <<std::endl;
    std::cout << "polar_map_height= " << polar_map_height <<std::endl;

   /* std::vector<CArray2D<aux_pix_data *> *>::iterator*/ its = m_scans.begin();
    for(int s = 0 ; its != m_scans.end()  ; its++, s++)
    {
        CArray2D<aux_pix_data *> * merged_data = *its;

        int xs = merged_data->_xs;
        int ys = merged_data->_ys;

        for (int y = 0; y < ys; y++)
        {
            for (int x = 0; x < xs; x++)
            {
                if(merged_data->_data[y][x] != NULL)
                {

                    aux_pix_data * pnt = merged_data->_data[y][x];


                    float tst_polar_angle = std::atan2( (pnt->_gz - rc_gl_z), (pnt->_gx - rc_gl_x) ) + cPI   ;
                    if(tst_polar_angle > 2 * cPI) tst_polar_angle -= 2 * cPI;

                    float polar_angle = (tst_polar_angle)/polar_angle_step;
                    int pmap_x = polar_angle;

                    ///std::cout << pnt->_gz << " " << pnt->_gx << " " << polar_angle << std::endl;


                    float dx = (pnt->_gx - rc_gl_x);
                    float dx_dx = dx * dx;
                    float dz = (pnt->_gz - rc_gl_z);
                    float dz_dz = dz * dz;
                    float radius = std::sqrt(dx_dx + dz_dz);

                    float new_pnt_y = pnt->_gy  + tan_cam_incl * ( bb_radius - radius);
                    float pmap_y_f = polar_map_height * (new_pnt_y  - bb_gl_y_min)/ (bb_gl_y_max - bb_gl_y_min);

                    int pmap_y = pmap_y_f;

                    if(pmap_x >= 0 &&  pmap_x < ploar_map_width &&
                       pmap_y >= 0 &&  pmap_y < polar_map_height  )
                            g_polar_map->_data[pmap_y][pmap_x].push_back(pnt);

                }
            }
        }
    }

    /*QString filname = QString("polar_map.raw");
    std::ofstream dataf;
    dataf.open (filname.toStdString().c_str(), std::ios::out| std::ios::binary);
    for (int j=0; j < polar_map_height; j++)
    for (int i=0; i < ploar_map_width; i++)
    {
      float val =  g_polar_map->_data[j][i].size();
      dataf.write((char*)&val,  sizeof(float));
    }
     dataf.close();*/

    /////////// step 3. simplify polar map & interpolat / convert to mesh

   /// CArray2D<aux_pix_data *> * mdata = simplify_polar_map(polar_map);


    /////////// step 4. construct gl mesh

    ///QImage tex;
     /// std::vector<C3DPoint> * pcl = prepare_pcl(mdata, 0, 0, 0, 0, 1, 0.5, 0, 1, tex);
    ///emit send_data_for_cglobject(pcl, NULL, 0, 0, 0);



   /// fill_gaps_in_merged_data_triv(*mdata);
   // CTriMesh * rmesh = construct_mesh_from_single_shot(*mdata,  2/* _decim_pnt*/  );
   // CTriangularMesh * mmesh = new CTriangularMesh;
   //// mmesh->use_loc_coloring();
    //mmesh->set_mesh(rmesh);

    /// emit send_data_for_cglobject(pcl, mmesh, 0.5, 0.5, 0);

   // emit send_result_fuse(mmesh);

}

CArray2D<aux_pix_data *> * CLaserSanOfflineThread::simplify_polar_map(CArray2D< std::list<aux_pix_data *> > & polar_map)
{
    int ploar_map_width = polar_map._xs;
    int polar_map_height = polar_map._ys;

    CArray2D<aux_pix_data *> * mdata = new CArray2D<aux_pix_data *>(ploar_map_width, polar_map_height);
    for (int j=2; j < polar_map_height-2; j++)
    {
    for (int i=2; i < ploar_map_width-2; i++)
    {

        int total_count = 0;
        aux_pix_data sum_dd;
        for (int y=-2; y <= 2; y++)
        {
        for (int x=-2; x <= 2; x++)
        {
            int lsz = polar_map._data[j+y][i+x].size();
            if( lsz != 0)
            {
                std::list<aux_pix_data *>::iterator itL  = polar_map._data[j+y][i+x].begin();

                for ( ; itL  != polar_map._data[j+y][i+x].end()  ; itL++ )
                {
                    sum_dd._x += (*itL)->_x;
                    sum_dd._y += (*itL)->_y;
                    sum_dd._z += (*itL)->_z;
                    total_count ++;
                }
            }

        }
        }

        if(total_count > 0)
        {
            aux_pix_data * dd = new aux_pix_data;
            dd->_x =  sum_dd._x / float(total_count);
            dd->_y =  sum_dd._y / float(total_count);
            dd->_z =  sum_dd._z / float(total_count);

            mdata->_data[j][i] = dd;
        }



    }
    }

    return mdata;
}

bool cand_pnts_sort_x(const aux_pix_data *  a, const aux_pix_data * b)
{
    return ( a->_x  <  b->_x);
}

bool cand_pnts_sort_y(const aux_pix_data *  a, const aux_pix_data * b)
{
    return ( a->_y  <  b->_y);
}

CArray2D<aux_pix_data *> * CLaserSanOfflineThread::simplify_polar_map_ransac(CArray2D< std::list<aux_pix_data *> > & polar_map)
{
    std::cout << "---simplify_polar_map_ransac----" << std::endl;

    int ploar_map_width = polar_map._xs;
    int polar_map_height = polar_map._ys;

    CArray2D<aux_pix_data *> * mdata = new CArray2D<aux_pix_data *>(ploar_map_width, polar_map_height);
    for (int j=2; j < polar_map_height-2; j++)
    {
    for (int i=2; i < ploar_map_width-2; i++)
    {

        std::vector <aux_pix_data *> cand_pnts;

        for (int y=-2; y <= 2; y++)
        {
        for (int x=-2; x <= 2; x++)
        {
            std::list<aux_pix_data *>::iterator itL  = polar_map._data[j+y][i+x].begin();
            for ( ; itL  != polar_map._data[j+y][i+x].end()  ; itL++ )
                cand_pnts.push_back(*itL);

        }
        }

        /// sort cand_pnts

        /////// sorting along x
        std::sort (cand_pnts.begin(), cand_pnts.end(), cand_pnts_sort_x);
        int pxsz_x = cand_pnts.size();
        int cutoff_minx = float(pxsz_x)*0.2;
        int cutoff_maxx = float(pxsz_x)*0.8;
        std::vector <aux_pix_data *> cand_pnts_after_x;
        for(int i = cutoff_minx ; i < cutoff_maxx; i++  )
            cand_pnts_after_x.push_back(cand_pnts[i]);


        std::sort (cand_pnts_after_x.begin(), cand_pnts_after_x.end(), cand_pnts_sort_y);
        int pxsz_y = cand_pnts_after_x.size();
        int cutoff_miny = float(pxsz_y)*0.2;
        int cutoff_maxy = float(pxsz_y)*0.8;
        std::vector <aux_pix_data *> cand_pnts_after_y;
        for(int i = cutoff_miny ; i < cutoff_maxy; i++  )
            cand_pnts_after_y.push_back(cand_pnts_after_x[i]);


        int cnd_sz = cand_pnts_after_y.size();
        int cutoff_min = 0;// (cnd_sz < 9)? 0: float(cnd_sz) * 0.25f;
        int cutoff_max = cnd_sz-1;//(cnd_sz < 9)? (cnd_sz-1): float(cnd_sz) * 0.75f;

        int total_count = 0;
        aux_pix_data sum_dd;
        for(int idx = cutoff_min; idx <= cutoff_max; idx++)
        {
            sum_dd._x += cand_pnts_after_y[idx]->_x;
            sum_dd._y += cand_pnts_after_y[idx]->_y;
            sum_dd._z += cand_pnts_after_y[idx]->_z;
            total_count ++;
        }

        if(total_count > 0)
        {
            aux_pix_data * dd = new aux_pix_data;
            dd->_x =  sum_dd._x / float(total_count);
            dd->_y =  sum_dd._y / float(total_count);
            dd->_z =  sum_dd._z / float(total_count);

            mdata->_data[j][i] = dd;
        }
    }
    }

    return mdata;
}


void CLaserSanOfflineThread::set_camera_matrix_for_texturing()
{
    CGlobalHSSettings::_camL_K = g_cam_intrinsics;
}

void CLaserSanOfflineThread::compute_camera_matrix_K(CArray2D<aux_pix_data *> * merged_data)
{


    std::vector<cv::Point2f>   imgPoints;
    std::vector<cv::Point3f> scenePoints;

    int xs = merged_data->_xs;
    int ys = merged_data->_ys;
    for(int v = 0; v < ys; v+=2)
    {
        for(int u = 0; u < xs; u+=2)
        {
            if(merged_data->_data[v][u] != NULL)
            {
                cv::Point2f pnt_2d;
                pnt_2d.x = merged_data->_data[v][u]->_u;
                pnt_2d.y = merged_data->_data[v][u]->_v;
                imgPoints.push_back(pnt_2d);


                cv::Point3f pnt_3d;
                pnt_3d.x =  merged_data->_data[v][u]->_bx ;
                pnt_3d.y = -merged_data->_data[v][u]->_bz ;
                pnt_3d.z =  merged_data->_data[v][u]->_by ;
                scenePoints.push_back(pnt_3d);

            }
        }
    }

    cv::Mat cameraMatrix= cv::Mat(3, 3, CV_64FC1);
    cv::setIdentity(cameraMatrix);
    cameraMatrix.at<double>(0,2)  = xs/2;
    cameraMatrix.at<double>(1,2)  = ys/2;
    cameraMatrix.at<double>(0,0)  = 1900;
    cameraMatrix.at<double>(1,1)  = 1900;

    cv::Mat D1;
    std::vector<cv::Mat> rvecs, tvecs;

    cv::Size img_sz;
    img_sz.width = xs;
    img_sz.height = ys;

    std::vector<std::vector<cv::Point2f> > imagePointsv;
    imagePointsv.push_back(imgPoints);

    std::vector<std::vector<cv::Point3f> > scenePointsv;
    scenePointsv.push_back(scenePoints);

    // use more than one merged_data ?
    double rms_l =
                cv::calibrateCamera( scenePointsv, imagePointsv, img_sz,
                                cameraMatrix, D1, rvecs, tvecs,  CV_CALIB_ZERO_TANGENT_DIST +
                                    CV_CALIB_USE_INTRINSIC_GUESS
                                    + CV_CALIB_FIX_ASPECT_RATIO
                                   // + CV_CALIB_FIX_K1
                                    + CV_CALIB_FIX_K2 // wihout davinchi
                                     + CV_CALIB_FIX_K3
                                    ,
                                    cvTermCriteria( CV_TERMCRIT_ITER+CV_TERMCRIT_EPS,1500,0.0001  )
                                    );
    std::cout << "left RMS error=" << rms_l << std::endl;

    if(rms_l > 5 )
        send_wrong_K_matrix();


    CGlobalHSSettings::_camL_K = cameraMatrix;
    std::cout << "cameraMatrix: " << cameraMatrix<< std::endl;
    // std::cout << "rvec: " << rvec << std::endl;
    //std::cout << "tvec: " << tvec << std::endl;
    std::cout << "dist: " << D1 << std::endl;


}

void CLaserSanOfflineThread::construct_and_add_new_cframe(int s, float rxc, float ryc, QString & aviname, int frame_idx )
{
    CFrame * cframe = new CFrame ;
    char * image_name_buf = new char[2048];

    /// if(_tex_name_format.size() != 0)
    ///    cframe->_imgL_str = construct_tex_name(image_name_buf, _scan_first + s);
    cframe->_imgL_str = aviname;
    cframe->_id = frame_idx;

    float c_scan_angle = _sca_angle * float(s);
    float c_rmat_00 =  cos(c_scan_angle);
    float c_rmat_01 = -sin(c_scan_angle);
    float c_rmat_10 =  sin(c_scan_angle);
    float c_rmat_11 =  cos(c_scan_angle);

    float cam_x = 0 - rxc;
    float cam_y = 0 - ryc;
    float cam_z = 0;


    float cam_nx = cam_x * c_rmat_00 + cam_y * c_rmat_01;
    float cam_ny = cam_x * c_rmat_10 + cam_y* c_rmat_11;
    float cam_nz = cam_z;


    cframe->_global_pos     = (cv::Mat_<double>(3,1) << (cam_nx + rxc), -cam_nz, (cam_ny + ryc));
    cframe->_glob_rot_vec   = (cv::Mat_<double>(1,3) <<  -_camera_x_incline, c_scan_angle, 0);
    cframe->_valid_coord    = true;

    ///std::cout << "CAMERA POSITION " <<  cframe->_global_pos  << std::endl;

    global_keyframes_sequence.push_back(cframe);
    delete [] image_name_buf;

}
