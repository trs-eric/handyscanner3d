#include "cglsampletrivtable.h"
#include "GraphicUtils.h"
#include <QGLWidget>
#include "settings/psettings.h"

CGLSampleTrivTable::CGLSampleTrivTable()
{
    _color[0] = 1;// 90.0f/255.0f;
    _color[1] = 1;// 90.0f/255.0f;
    _color[2] = 1;//140.0f/255.0f;

    _tex_matid = nonGLlist;
    _tbl_img_name = std::string("grid.bmp");


}

CGLSampleTrivTable::~CGLSampleTrivTable()
{
}

void CGLSampleTrivTable::set_pos(float x, float y, float z)
{
    /// delete_glLists();
    this->_tx = x;
    this->_ty = y;
    this->_tz = z;
}



void CGLSampleTrivTable::create_tex()
{


    ///

    QImage img (_tbl_img_name.c_str());
    std::cout << "load_tex_matid " << img.width()<< std::endl;
    if(img.width() == 0) return;

    glEnable(GL_TEXTURE_2D);
    glGenTextures(1, &_tex_matid);
    QImage tximg = img;// QGLWidget::convertToGLFormat(img);
    ///tximg.fill(QColor(20,128,50));
    int ys = tximg.height();
    int xs = tximg.width();
    for(int j = 0; j < ys; j++)
        for(int i = 0; i < xs; i++)
        {
             int col = j * 255 / ys;
            QRgb p = qRgba(col, col, col, col);
            tximg.setPixel(i,j,p);
        }

    glBindTexture(GL_TEXTURE_2D, _tex_matid);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tximg.width(), tximg.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, tximg.bits());
     gluBuild2DMipmaps(GL_TEXTURE_2D, 3, tximg.width(), tximg.height(), GL_RGBA, GL_UNSIGNED_BYTE, tximg.bits());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,  GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,  GL_REPEAT);

    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
//

    glDisable(GL_TEXTURE_2D);


}

void CGLSampleTrivTable::create_glList()
{

    delete_glLists();

    _oglid = glGenLists(1);
    glNewList(_oglid, GL_COMPILE_AND_EXECUTE);
     glDisable(GL_LIGHTING);
     glEnable(GL_BLEND);
     glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
     //glEnable(GL_LINE_SMOOTH);
     //glLineWidth(1.5);
     //glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
     //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

     float radius = 75; //mm
     float side = radius * CGlobalHSSettings::glob_gl_scale;
    float nmax = 3;
    for (int n = nmax; n >= 0 ; n--)
        draw_circle(side *(1.f - float(n) /nmax), 0.35 );
    draw_tangents(side, 0.35);
    draw_foot(side, 0.2);


    //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
     glEnable(GL_LIGHTING);
     glDisable(GL_BLEND);
    glColor3f(1, 1, 1);
    glEndList();





   /* if(_tex_matid != nonGLlist)
    {
        glDeleteTextures(1,  &_tex_matid);
        _tex_matid = nonGLlist;
    }
    delete_glLists();

    // create new display list
    create_tex();

    _oglid = glGenLists(1);
    glNewList(_oglid, GL_COMPILE);
       glEnable(GL_TEXTURE_2D);
       glBindTexture(GL_TEXTURE_2D, _tex_matid);


         glDisable(GL_LIGHTING);
    glDisable(GL_COLOR_MATERIAL);

       //  glEnable(GL_ALPHA_TEST);

       // glBlendFunc(GL_SRC_COLOR, GL_ONE_MINUS_SRC_COLOR);
         glEnable(GL_BLEND);


    //  glDepthMask(GL_FALSE);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

       glColor4f(1.0,1.0,1.0,1);
  //




     // glDepthFunc(GL_LEQUAL);
     // glEnable(GL_DEPTH_TEST);
     // glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
     // glEnable(GL_BLEND);
     //


    float side = 5;
    glBegin(GL_TRIANGLES);

    glNormal3f (0.f, 1.f, 0.f);
    glTexCoord2f(0, 0);
    glVertex3f(-side, 0.f, side);

    glNormal3f (0.f, 1.f, 0.f);
    glTexCoord2f(1, 0);
    glVertex3f(side, 0.f, side);

    glNormal3f (0.f, 1.f, 0.f);
    glTexCoord2f(1, 1);
    glVertex3f(side, 0.f, -side);


    glNormal3f (0.f, 1.f, 0.f);
    glTexCoord2f(0, 0);
    glVertex3f(-side, 0.f, side);

    glNormal3f (0.f, 1.f, 0.f);
    glTexCoord2f(1, 1);
    glVertex3f(side, 0.f, -side);

    glNormal3f (0.f, 1.f, 0.f);
    glTexCoord2f(0, 1);
    glVertex3f(-side, 0.f, -side);



    glEnd();

   glDisable(GL_BLEND);
    //  glDepthMask(GL_TRUE);
      glColor3f(1, 1, 1);
      glDisable(GL_TEXTURE_2D);

    //  glDisable(GL_ALPHA_TEST);
    glEndList();*/

    /*
    _oglid = glGenLists(1);
    glNewList(_oglid, GL_COMPILE_AND_EXECUTE);
     glDisable(GL_LIGHTING);
     glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    float side = 5;
    float nmax = 5;
    for (int r = side, n = nmax; n > 0 ; n--, r -= side/nmax)
        draw_circle(r);
    draw_tangents(side);
     glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
     glEnable(GL_LIGHTING);
    glEndList();*/
}

void CGLSampleTrivTable::draw_circle(GLfloat radius, GLfloat opacity)
{
    glBegin(GL_LINE_LOOP);
    for (int i=0; i < 360; i+=5)
    {
        glColor4f(_color[0],_color[0], _color[0],opacity);
        glVertex3f(cos(BGM((GLfloat)i))*radius, 0.f, sin(BGM((GLfloat)i))*radius);
    }
    glEnd();
}

void CGLSampleTrivTable::draw_foot(GLfloat radius, GLfloat opacity)
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glBegin(GL_TRIANGLE_FAN);

    glColor4f(0.f, 0.f, 0.f, opacity);
    glVertex3f(0, -0.01f, 0);
    for (int i=0; i <= 360; i++)
    { glColor4f(0.f, 0.f, 0.f, opacity);
        glVertex3f(cos(BGM((GLfloat)i))*radius, -0.01f, sin(BGM((GLfloat)i))*radius);
    }
    glEnd();


}

void CGLSampleTrivTable::draw_tangents(GLfloat radius, GLfloat opacity)
{
    glBegin(GL_LINES);
    int step = 45;
    for (int i=0; i < 360; i+=step )
    {
        glColor4f(_color[0],_color[0], _color[0], opacity);
        glVertex3f( 0.f, 0.f, 0.f);
        glVertex3f(cos(BGM((GLfloat)i))*radius, 0.f, sin(BGM((GLfloat)i))*radius);
    }
    glEnd();
}
