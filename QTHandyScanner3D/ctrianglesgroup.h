#ifndef CTRIANGLESGROUP_H
#define CTRIANGLESGROUP_H

#include <list>
#include <QImage>
#include <set>
#include <map>

#include "graphicutils.h"

class CFrame;
class CTriMesh;
class CTrianglesGroup
{
public:
    CTrianglesGroup();
    CTrianglesGroup( std::list<int> &);

    ~CTrianglesGroup();

    void merge(CTrianglesGroup*);
    void construct_texture();
    void construct_texture_color_correction(std::vector<Vector3f> &);

    void compute_tex_uv();
    void recompute_uv(QImage * tex_atlas);

    /*
    void find_bound_points(std::vector<CTrianglesGroup*> & group_ref);
    void compute_bound_points_color();
    std::set<int> _bound_points;
    double _av_r;
    double _av_g;
    double _av_b;

    void correct_tex_colors(int br_r, int br_g, int br_b);
    void correct_tex_colors(double br);

*/

    void correct_tex_colors(CTrianglesGroup * tg, double br_r, double br_g, double br_b);
    bool _corrected_colors;

    void construct_bound_groups(std::vector<CTrianglesGroup*> & group_ref);
    std::map<CTrianglesGroup*, std::set<std::pair<QRgb,QRgb> > > _bound_maps;
    void make_texcolor_correction();

    std::list<int> _face_group;

    CFrame * _frame;
    QImage * _texture;
    CTriMesh * _mesh;

    int _tex_u0i;
    int _tex_v0i;
    int _tex_u1i;
    int _tex_v1i;

    int _texture_pos_x;
    int _texture_pos_y;
};

bool cmp_face_groups_desc(CTrianglesGroup * , CTrianglesGroup *);
bool cmp_face_groups_asc(CTrianglesGroup * , CTrianglesGroup *);
bool cmp_face_groups_desc_tex(CTrianglesGroup * , CTrianglesGroup *);
bool cmp_face_groups_desc_texarea(CTrianglesGroup * , CTrianglesGroup *);

#endif // CTRIANGLESGROUP_H
