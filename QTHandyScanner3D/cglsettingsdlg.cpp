#include "render_radiance_scaling/radianceScalingRenderer.h"

#include "cglsettingsdlg.h"
#include "ui_cglsettingsdlg.h"
#include <QColorDialog>
#include <QFileDialog>
#include <QSettings>

#include "cglwidget.h"
#include "settings/psettings.h"
#include "mainwindow.h"

#include "messages.h"

CGLSettingsDlg::CGLSettingsDlg(CGLWidget *glWidget , QWidget * mw, QWidget *parent) :
    QDialog(parent), _glWidget (glWidget), _mw(mw),
    ui(new Ui::CGLSettingsDlg)
{

    //setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);

    init_interface();

    connect(ui->btnBckgSideCol,  SIGNAL(clicked()), this, SLOT(change_bckg_side_color()));
    connect(ui->btnBckgCentCol,  SIGNAL(clicked()), this, SLOT(change_bckg_cent_color()));
    connect(ui->btnSurfCol,  SIGNAL(clicked()), this, SLOT(change_surface_color()));
    connect(ui->btnLAmbCol,  SIGNAL(clicked()), this, SLOT(change_light_amb_color()));
    connect(ui->btnLDifCol,  SIGNAL(clicked()), this, SLOT(change_light_dif_color()));
    connect(ui->slidLPosX, SIGNAL(valueChanged(int)), this, SLOT(change_light_posX(int )));
    connect(ui->slidLPosY, SIGNAL(valueChanged(int)), this, SLOT(change_light_posY(int )));
    connect(ui->slidLPosZ, SIGNAL(valueChanged(int)), this, SLOT(change_light_posZ(int )));

    ///connect(ui->slidMatShin, SIGNAL(valueChanged(int)), this, SLOT(change_mat_shin(int )));
    connect(ui->slidMatSpec, SIGNAL(valueChanged(int)), this, SLOT(change_mat_spec(int )));

    connect(ui->slidCamAlpha, SIGNAL(valueChanged(int)), this, SLOT(change_cam_opacity(int )));

    ui->slidPointSz->setValue(CGlobalHSSettings::gpoint_size*10);
    connect(ui->slidPointSz,SIGNAL(valueChanged(int)),this,SLOT(pointsize_changed(int)));


    connect(ui->closeBtn,  SIGNAL(clicked()), this, SLOT(close()));

    /// connect(ui->chkUseRadiance, SIGNAL(stateChanged(int)), this, SLOT(enable_radiance_render(int )));

     connect(ui->chkUseRadiance, SIGNAL(toggled(bool)), this, SLOT(enable_radiance_render(bool )));

    //ui->slidEnhance->setValue(50);
    connect(ui->slidEnhance,SIGNAL(valueChanged(int)),this,SLOT(enhancement_changed(int)));

    //ui->slidTrans->setValue(50);
    connect(ui->slidTrans,SIGNAL(valueChanged(int)),this,SLOT(transition_changed(int)));

    // connect(ui->FchkInvert, SIGNAL(stateChanged(int)), this, SLOT(invert_effect(int )));

    //////////////

    _rc_max_sz = 550;
    _rc_min_sz = 550;
    _rc_lbl_sz = 128;

    QImage img_shad(":/rsrender/render_radiance_scaling/litSpheres/shadow.png");
    _pix_shad = QPixmap::fromImage(img_shad);

    QImage img_bklt(":/rsrender/render_radiance_scaling/litSpheres/backlight.png");
    _pix_bklt = QPixmap::fromImage(img_bklt);

    QImage img_tplt(":/rsrender/render_radiance_scaling/litSpheres/toplight.png");
    _pix_tplt = QPixmap::fromImage(img_tplt);

    QImage img_splt(":/rsrender/render_radiance_scaling/litSpheres/speclight.png");
    _pix_splt = QPixmap::fromImage(img_splt);

    QImage img_mask(":/rsrender/render_radiance_scaling/litSpheres/vinsmall.png");
    _pix_mask = QPixmap::fromImage(img_mask);

    QImage img_rmask(":/rsrender/render_radiance_scaling/litSpheres/rmask2.png");
    _pix_rmask = QPixmap::fromImage(img_rmask);

    _rc_bck_colorL = QColor(25,203,255);
    _rc_bcklight_colorL = QColor(50,255,255);



    _rc_bck_colorR = QColor(200,25,255);
    _rc_bcklight_colorR = QColor(255,50,255);


    connect(ui->btnCanvCol,  SIGNAL(clicked()), this, SLOT(change_rc_canv_color_left()));
    connect(ui->btnBckLightCol,  SIGNAL(clicked()), this, SLOT(change_rc_bklit_color_left()));

    connect(ui->slidShadow,SIGNAL(valueChanged(int)),this,SLOT(repaint_left_sphere()));
    connect(ui->slidShadX,SIGNAL(valueChanged(int)),this,SLOT(repaint_left_sphere()));
    connect(ui->slidShadY,SIGNAL(valueChanged(int)),this,SLOT(repaint_left_sphere()));


    connect(ui->slidBacklight,SIGNAL(valueChanged(int)),this,SLOT(repaint_left_sphere()));
    connect(ui->slidBacklightX,SIGNAL(valueChanged(int)),this,SLOT(repaint_left_sphere()));
    connect(ui->slidBacklightY,SIGNAL(valueChanged(int)),this,SLOT(repaint_left_sphere()));

    connect(ui->slidSpec,SIGNAL(valueChanged(int)),this,SLOT(repaint_left_sphere()));
    connect(ui->slidSpecX,SIGNAL(valueChanged(int)),this,SLOT(repaint_left_sphere()));
    connect(ui->slidSpecY,SIGNAL(valueChanged(int)),this,SLOT(repaint_left_sphere()));

    connect(ui->slidTop,SIGNAL(valueChanged(int)),this,SLOT(repaint_left_sphere()));
    connect(ui->slidTopX,SIGNAL(valueChanged(int)),this,SLOT(repaint_left_sphere()));
    connect(ui->slidTopY,SIGNAL(valueChanged(int)),this,SLOT(repaint_left_sphere()));


    connect(ui->btnCanvColR,  SIGNAL(clicked()), this, SLOT(change_rc_canv_color_right()));
    connect(ui->btnBckLightColR,  SIGNAL(clicked()), this, SLOT(change_rc_bklit_color_right()));

    connect(ui->slidShadowR,SIGNAL(valueChanged(int)),this,SLOT(repaint_right_sphere()));
    connect(ui->slidShadXR,SIGNAL(valueChanged(int)),this,SLOT(repaint_right_sphere()));
    connect(ui->slidShadYR,SIGNAL(valueChanged(int)),this,SLOT(repaint_right_sphere()));

    connect(ui->slidBacklightR,SIGNAL(valueChanged(int)),this,SLOT(repaint_right_sphere()));
    connect(ui->slidBacklightXR,SIGNAL(valueChanged(int)),this,SLOT(repaint_right_sphere()));
    connect(ui->slidBacklightYR,SIGNAL(valueChanged(int)),this,SLOT(repaint_right_sphere()));

    connect(ui->slidSpecR,SIGNAL(valueChanged(int)),this,SLOT(repaint_right_sphere()));
    connect(ui->slidSpecXR,SIGNAL(valueChanged(int)),this,SLOT(repaint_right_sphere()));
    connect(ui->slidSpecYR,SIGNAL(valueChanged(int)),this,SLOT(repaint_right_sphere()));

    connect(ui->slidTopR,SIGNAL(valueChanged(int)),this,SLOT(repaint_right_sphere()));
    connect(ui->slidTopXR,SIGNAL(valueChanged(int)),this,SLOT(repaint_right_sphere()));
    connect(ui->slidTopYR,SIGNAL(valueChanged(int)),this,SLOT(repaint_right_sphere()));

     connect(ui->btnChange,SIGNAL(clicked()),this,SLOT(change_spheres()));

     ////////////////////////////////////////
     connect(ui->btnSavePresets,SIGNAL(clicked()),this,SLOT(save_presets()));
     connect(ui->btnLoadPresets,SIGNAL(clicked()),this,SLOT(load_presets()));


     connect(ui->sldTableZPos,SIGNAL(valueChanged(int)),this,SLOT(change_table_pos(int)));

     connect(ui->btnCamCentering,SIGNAL(clicked()),this,SLOT(reset_camera_pos()));

}


void CGLSettingsDlg::reset_camera_pos()
{
    _glWidget->on_reset_camera_pos();
}

void CGLSettingsDlg::change_table_pos(int zpos)
{

   CGlobalHSSettings::gl_table_rot_center_y = (float)ui->sldTableZPos->value()/10.f;
   _glWidget->on_table_pos();
}

void CGLSettingsDlg::save_presets()
{

    QString fileName = QFileDialog::getSaveFileName(this,
             tr("Save presets ..."), "", "Presets (*.ini)");

    if (fileName.isEmpty()) return;

    QSettings settings(fileName, QSettings::IniFormat);
    settings.setValue("bckg_side_col_r", double(CGlobalHSSettings::gcolor_sr));
    settings.setValue("bckg_side_col_g", double(CGlobalHSSettings::gcolor_sg));
    settings.setValue("bckg_side_col_b", double(CGlobalHSSettings::gcolor_sb));

    settings.setValue("bckg_cent_col_r", double(CGlobalHSSettings::gcolor_cr));
    settings.setValue("bckg_cent_col_g", double(CGlobalHSSettings::gcolor_cg));
    settings.setValue("bckg_cent_col_b", double(CGlobalHSSettings::gcolor_cb));

    settings.setValue("mesh_col_r", double(CGlobalHSSettings::gcolor_mr));
    settings.setValue("mesh_col_g", double(CGlobalHSSettings::gcolor_mg));
    settings.setValue("mesh_col_b", double(CGlobalHSSettings::gcolor_mb));

    settings.setValue("lamb_r", double(CGlobalHSSettings::gl_light_Ambient[0]));
    settings.setValue("lamb_g", double(CGlobalHSSettings::gl_light_Ambient[1]));
    settings.setValue("lamb_b", double(CGlobalHSSettings::gl_light_Ambient[2]));

    settings.setValue("ldif_r", double(CGlobalHSSettings::gl_light_Diffuse[0]));
    settings.setValue("ldif_g", double(CGlobalHSSettings::gl_light_Diffuse[1]));
    settings.setValue("ldif_b", double(CGlobalHSSettings::gl_light_Diffuse[2]));

    settings.setValue("lpos_x", double(CGlobalHSSettings::gl_light_Position[0]));
    settings.setValue("lpos_y", double(CGlobalHSSettings::gl_light_Position[1]));
    settings.setValue("lpos_z", double(CGlobalHSSettings::gl_light_Position[2]));

    settings.setValue("spec", double(CGlobalHSSettings::gl_mat_Specularity[0]));
    settings.setValue("shin", double(CGlobalHSSettings::gl_mat_Shiness));

    settings.setValue("camopas", double(CGlobalHSSettings::gl_cam_alpha));

    settings.setValue("enhance", double(CGlobalFRSettings::_enhancement));
    settings.setValue("transit", double(CGlobalFRSettings::_transition));

    settings.setValue("rc_bck_colL_r", int(CGlobalFRSettings::_rc_bck_colorL.red()));
    settings.setValue("rc_bck_colL_g", int(CGlobalFRSettings::_rc_bck_colorL.green()));
    settings.setValue("rc_bck_colL_b", int(CGlobalFRSettings::_rc_bck_colorL.blue()));

    settings.setValue("rc_shadL_op", double(CGlobalFRSettings::_rc_shadowL[0]));
    settings.setValue("rc_shadL_x", double(CGlobalFRSettings::_rc_shadowL[1]));
    settings.setValue("rc_shadL_y", double(CGlobalFRSettings::_rc_shadowL[2]));

    settings.setValue("rc_bcklight_colL_r", int(CGlobalFRSettings::_rc_bcklight_colorL.red()));
    settings.setValue("rc_bcklight_colL_g", int(CGlobalFRSettings::_rc_bcklight_colorL.green()));
    settings.setValue("rc_bcklight_colL_b", int(CGlobalFRSettings::_rc_bcklight_colorL.blue()));

    settings.setValue("rc_bcklightL_op", double(CGlobalFRSettings::_rc_backlightL[0]));
    settings.setValue("rc_bcklightL_x", double(CGlobalFRSettings::_rc_backlightL[1]));
    settings.setValue("rc_bcklightL_y", double(CGlobalFRSettings::_rc_backlightL[2]));

    settings.setValue("rc_toplightL_op", double(CGlobalFRSettings::_rc_toplightL[0]));
    settings.setValue("rc_toplightL_x", double(CGlobalFRSettings::_rc_toplightL[1]));
    settings.setValue("rc_toplightL_y", double(CGlobalFRSettings::_rc_toplightL[2]));

    settings.setValue("rc_speclightL_op", double(CGlobalFRSettings::_rc_speclightL[0]));
    settings.setValue("rc_speclightL_x", double(CGlobalFRSettings::_rc_speclightL[1]));
    settings.setValue("rc_speclightL_y", double(CGlobalFRSettings::_rc_speclightL[2]));

    settings.setValue("rc_bck_colR_r", int(CGlobalFRSettings::_rc_bck_colorR.red()));
    settings.setValue("rc_bck_colR_g", int(CGlobalFRSettings::_rc_bck_colorR.green()));
    settings.setValue("rc_bck_colR_b", int(CGlobalFRSettings::_rc_bck_colorR.blue()));

    settings.setValue("rc_shadR_op", double(CGlobalFRSettings::_rc_shadowR[0]));
    settings.setValue("rc_shadR_x", double(CGlobalFRSettings::_rc_shadowR[1]));
    settings.setValue("rc_shadR_y", double(CGlobalFRSettings::_rc_shadowR[2]));

    settings.setValue("rc_bcklight_colR_r", int(CGlobalFRSettings::_rc_bcklight_colorR.red()));
    settings.setValue("rc_bcklight_colR_g", int(CGlobalFRSettings::_rc_bcklight_colorR.green()));
    settings.setValue("rc_bcklight_colR_b", int(CGlobalFRSettings::_rc_bcklight_colorR.blue()));

    settings.setValue("rc_bcklightR_op", double(CGlobalFRSettings::_rc_backlightR[0]));
    settings.setValue("rc_bcklightR_x", double(CGlobalFRSettings::_rc_backlightR[1]));
    settings.setValue("rc_bcklightR_y", double(CGlobalFRSettings::_rc_backlightR[2]));

    settings.setValue("rc_toplightR_op", double(CGlobalFRSettings::_rc_toplightR[0]));
    settings.setValue("rc_toplightR_x", double(CGlobalFRSettings::_rc_toplightR[1]));
    settings.setValue("rc_toplightR_y", double(CGlobalFRSettings::_rc_toplightR[2]));


    settings.setValue("rc_speclightR_op", double(CGlobalFRSettings::_rc_speclightR[0]));
    settings.setValue("rc_speclightR_x", double(CGlobalFRSettings::_rc_speclightR[1]));
    settings.setValue("rc_speclightR_y", double(CGlobalFRSettings::_rc_speclightR[2]));


    settings.setValue("table_rot_center_y", double(CGlobalHSSettings::gl_table_rot_center_y));
    settings.setValue("table_rot_center_x", double(CGlobalHSSettings::gl_table_rot_center_x));
    settings.setValue("table_rot_center_z", double(CGlobalHSSettings::gl_table_rot_center_z));


}





void CGLSettingsDlg::load_presets()
{
    QString fileName = QFileDialog::getOpenFileName(this,
          tr("Load presets ..."), "", "Presets (*.ini)");

    if (fileName.isEmpty()) return;

    QSettings settings(fileName, QSettings::IniFormat);

    CGlobalHSSettings::gcolor_sr = settings.value("bckg_side_col_r", CGlobalHSSettings::gcolor_sr).toFloat();
    CGlobalHSSettings::gcolor_sg = settings.value("bckg_side_col_g", CGlobalHSSettings::gcolor_sg).toFloat();
    CGlobalHSSettings::gcolor_sb = settings.value("bckg_side_col_b", CGlobalHSSettings::gcolor_sb).toFloat();

    CGlobalHSSettings::gcolor_cr = settings.value("bckg_cent_col_r", CGlobalHSSettings::gcolor_cr).toFloat();
    CGlobalHSSettings::gcolor_cg = settings.value("bckg_cent_col_g", CGlobalHSSettings::gcolor_cg).toFloat();
    CGlobalHSSettings::gcolor_cb = settings.value("bckg_cent_col_b", CGlobalHSSettings::gcolor_cb).toFloat();

    CGlobalHSSettings::gcolor_mr = settings.value("mesh_col_r", CGlobalHSSettings::gcolor_mr).toFloat();
    CGlobalHSSettings::gcolor_mg = settings.value("mesh_col_g", CGlobalHSSettings::gcolor_mg).toFloat();
    CGlobalHSSettings::gcolor_mb = settings.value("mesh_col_b", CGlobalHSSettings::gcolor_mb).toFloat();

    CGlobalHSSettings::gl_light_Ambient[0] = settings.value("lamb_r", CGlobalHSSettings::gl_light_Ambient[0]).toFloat();
    CGlobalHSSettings::gl_light_Ambient[1] = settings.value("lamb_g", CGlobalHSSettings::gl_light_Ambient[1]).toFloat();
    CGlobalHSSettings::gl_light_Ambient[2] = settings.value("lamb_b", CGlobalHSSettings::gl_light_Ambient[2]).toFloat();

    CGlobalHSSettings::gl_light_Diffuse[0] = settings.value("ldif_r", CGlobalHSSettings::gl_light_Diffuse[0]).toFloat();
    CGlobalHSSettings::gl_light_Diffuse[1] = settings.value("ldif_g", CGlobalHSSettings::gl_light_Diffuse[1]).toFloat();
    CGlobalHSSettings::gl_light_Diffuse[2] = settings.value("ldif_b", CGlobalHSSettings::gl_light_Diffuse[2]).toFloat();

    CGlobalHSSettings::gl_light_Position[0] = settings.value("lpos_x", CGlobalHSSettings::gl_light_Position[0]).toFloat();
    CGlobalHSSettings::gl_light_Position[1] = settings.value("lpos_y", CGlobalHSSettings::gl_light_Position[1]).toFloat();
    CGlobalHSSettings::gl_light_Position[2] = settings.value("lpos_z", CGlobalHSSettings::gl_light_Position[2]).toFloat();

    CGlobalHSSettings::gl_mat_Specularity[0] = settings.value("spec", CGlobalHSSettings::gl_mat_Specularity[0]).toFloat();
    CGlobalHSSettings::gl_mat_Shiness = settings.value("shin", CGlobalHSSettings::gl_mat_Shiness).toFloat();

    CGlobalHSSettings::gl_cam_alpha = settings.value("camopas", CGlobalHSSettings::gl_cam_alpha).toFloat();

    CGlobalFRSettings::_enhancement = settings.value("enhance", CGlobalFRSettings::_enhancement).toFloat();
    CGlobalFRSettings::_transition  = settings.value("transit", CGlobalFRSettings::_transition).toFloat();

    int tRed  = settings.value("rc_bck_colL_r", int(CGlobalFRSettings::_rc_bck_colorL.red())).toInt();
    int tGree = settings.value("rc_bck_colL_g", int(CGlobalFRSettings::_rc_bck_colorL.green())).toInt();
    int tBlu  = settings.value("rc_bck_colL_b", int(CGlobalFRSettings::_rc_bck_colorL.blue())).toInt();
    CGlobalFRSettings::_rc_bck_colorL = QColor(tRed, tGree, tBlu  );

    CGlobalFRSettings::_rc_shadowL[0] = settings.value("rc_shadL_op", CGlobalFRSettings::_rc_shadowL[0]).toFloat();
    CGlobalFRSettings::_rc_shadowL[1] = settings.value("rc_shadL_x", CGlobalFRSettings::_rc_shadowL[1]).toFloat();
    CGlobalFRSettings::_rc_shadowL[2] = settings.value("rc_shadL_y", CGlobalFRSettings::_rc_shadowL[2]).toFloat();


    tRed  = settings.value("rc_bcklight_colL_r", int(CGlobalFRSettings::_rc_bcklight_colorL.red())).toInt();
    tGree = settings.value("rc_bcklight_colL_g", int(CGlobalFRSettings::_rc_bcklight_colorL.green())).toInt();
    tBlu  = settings.value("rc_bcklight_colL_b", int(CGlobalFRSettings::_rc_bcklight_colorL.blue())).toInt();
    CGlobalFRSettings::_rc_bcklight_colorL = QColor(tRed, tGree, tBlu);

    CGlobalFRSettings::_rc_backlightL[0] = settings.value("rc_bcklightL_op", CGlobalFRSettings::_rc_backlightL[0]).toFloat();
    CGlobalFRSettings::_rc_backlightL[1] = settings.value("rc_bcklightL_x", CGlobalFRSettings::_rc_backlightL[1]).toFloat();
    CGlobalFRSettings::_rc_backlightL[2] = settings.value("rc_bcklightL_y", CGlobalFRSettings::_rc_backlightL[2]).toFloat();


    CGlobalFRSettings::_rc_toplightL[0] = settings.value("rc_toplightL_op", CGlobalFRSettings::_rc_toplightL[0]).toFloat();
    CGlobalFRSettings::_rc_toplightL[1] = settings.value("rc_toplightL_x", CGlobalFRSettings::_rc_toplightL[1]).toFloat();
    CGlobalFRSettings::_rc_toplightL[2] = settings.value("rc_toplightL_y", CGlobalFRSettings::_rc_toplightL[2]).toFloat();

    CGlobalFRSettings::_rc_speclightL[0] = settings.value("rc_speclightL_op", CGlobalFRSettings::_rc_speclightL[0]).toFloat();
    CGlobalFRSettings::_rc_speclightL[1] = settings.value("rc_speclightL_x", CGlobalFRSettings::_rc_speclightL[1]).toFloat();
    CGlobalFRSettings::_rc_speclightL[2] = settings.value("rc_speclightL_y", CGlobalFRSettings::_rc_speclightL[2]).toFloat();


    tRed  = settings.value("rc_bck_colR_r", int(CGlobalFRSettings::_rc_bck_colorR.red())).toInt();
    tGree = settings.value("rc_bck_colR_g", int(CGlobalFRSettings::_rc_bck_colorR.green())).toInt();
    tBlu  = settings.value("rc_bck_colR_b", int(CGlobalFRSettings::_rc_bck_colorR.blue())).toInt();
    CGlobalFRSettings::_rc_bck_colorR = QColor(tRed, tGree, tBlu );

    CGlobalFRSettings::_rc_shadowR[0] = settings.value("rc_shadR_op", CGlobalFRSettings::_rc_shadowR[0]).toFloat();
    CGlobalFRSettings::_rc_shadowR[1] = settings.value("rc_shadR_x", CGlobalFRSettings::_rc_shadowR[1]).toFloat();
    CGlobalFRSettings::_rc_shadowR[2] = settings.value("rc_shadR_y", CGlobalFRSettings::_rc_shadowR[2]).toFloat();

    tRed  = settings.value("rc_bcklight_colR_r", int(CGlobalFRSettings::_rc_bcklight_colorR.red())).toInt();
    tGree = settings.value("rc_bcklight_colR_g", int(CGlobalFRSettings::_rc_bcklight_colorR.green())).toInt();
    tBlu  = settings.value("rc_bcklight_colR_b", int(CGlobalFRSettings::_rc_bcklight_colorR.blue())).toInt();
    CGlobalFRSettings::_rc_bcklight_colorR = QColor(tRed, tGree, tBlu);

    CGlobalFRSettings::_rc_backlightR[0] = settings.value("rc_bcklightR_op", CGlobalFRSettings::_rc_backlightR[0]).toFloat();
    CGlobalFRSettings::_rc_backlightR[1] = settings.value("rc_bcklightR_x", CGlobalFRSettings::_rc_backlightR[1]).toFloat();
    CGlobalFRSettings::_rc_backlightR[2] = settings.value("rc_bcklightR_y", CGlobalFRSettings::_rc_backlightR[2]).toFloat();


    CGlobalFRSettings::_rc_toplightR[0] = settings.value("rc_toplightR_op", CGlobalFRSettings::_rc_toplightR[0]).toFloat();
    CGlobalFRSettings::_rc_toplightR[1] = settings.value("rc_toplightR_x", CGlobalFRSettings::_rc_toplightR[1]).toFloat();
    CGlobalFRSettings::_rc_toplightR[2] = settings.value("rc_toplightR_y", CGlobalFRSettings::_rc_toplightR[2]).toFloat();

    CGlobalFRSettings::_rc_speclightR[0] = settings.value("rc_speclightR_op", CGlobalFRSettings::_rc_speclightR[0]).toFloat();
    CGlobalFRSettings::_rc_speclightR[1] = settings.value("rc_speclightR_x", CGlobalFRSettings::_rc_speclightR[1]).toFloat();
    CGlobalFRSettings::_rc_speclightR[2] = settings.value("rc_speclightR_y", CGlobalFRSettings::_rc_speclightR[2]).toFloat();


   // CGlobalHSSettings::gl_table_rot_center_x = settings.value("table_rot_center_x", CGlobalHSSettings::gl_table_rot_center_x).toFloat();
    CGlobalHSSettings::gl_table_rot_center_y = settings.value("table_rot_center_y", CGlobalHSSettings::gl_table_rot_center_y).toFloat();
   // CGlobalHSSettings::gl_table_rot_center_z = settings.value("table_rot_center_z", CGlobalHSSettings::gl_table_rot_center_z).toFloat();


    actualize_interface();
}


void CGLSettingsDlg::block_rc_controls()
{
    ui->slidShadow->blockSignals(true);
    ui->slidShadowR->blockSignals(true);
    ui->slidShadX->blockSignals(true);
    ui->slidShadXR->blockSignals(true);
    ui->slidShadY->blockSignals(true);
    ui->slidShadYR->blockSignals(true);
    ui->slidBacklight->blockSignals(true);
    ui->slidBacklightR->blockSignals(true);
    ui->slidBacklightX->blockSignals(true);
    ui->slidBacklightXR->blockSignals(true);
    ui->slidBacklightY->blockSignals(true);
    ui->slidBacklightYR->blockSignals(true);
    ui->slidSpec->blockSignals(true);
    ui->slidSpecR->blockSignals(true);
    ui->slidSpecX->blockSignals(true);
    ui->slidSpecXR->blockSignals(true);
    ui->slidSpecY->blockSignals(true);
    ui->slidSpecYR->blockSignals(true);
    ui->slidTop->blockSignals(true);
    ui->slidTopR->blockSignals(true);
    ui->slidTopX->blockSignals(true);
    ui->slidTopXR->blockSignals(true);
    ui->slidTopY->blockSignals(true);
    ui->slidTopYR->blockSignals(true);
}

void CGLSettingsDlg::unblock_rc_controls()
{
    ui->slidShadow->blockSignals(false);
    ui->slidShadowR->blockSignals(false);
    ui->slidShadX->blockSignals(false);
    ui->slidShadXR->blockSignals(false);
    ui->slidShadY->blockSignals(false);
    ui->slidShadYR->blockSignals(false);
    ui->slidBacklight->blockSignals(false);
    ui->slidBacklightR->blockSignals(false);
    ui->slidBacklightX->blockSignals(false);
    ui->slidBacklightXR->blockSignals(false);
    ui->slidBacklightY->blockSignals(false);
    ui->slidBacklightYR->blockSignals(false);
    ui->slidSpec->blockSignals(false);
    ui->slidSpecR->blockSignals(false);
    ui->slidSpecX->blockSignals(false);
    ui->slidSpecXR->blockSignals(false);
    ui->slidSpecY->blockSignals(false);
    ui->slidSpecYR->blockSignals(false);
    ui->slidTop->blockSignals(false);
    ui->slidTopR->blockSignals(false);
    ui->slidTopX->blockSignals(false);
    ui->slidTopXR->blockSignals(false);
    ui->slidTopY->blockSignals(false);
    ui->slidTopYR->blockSignals(false);
}

void CGLSettingsDlg::change_spheres()
{
    QColor tmp = _rc_bck_colorL ;
    _rc_bck_colorL  = _rc_bck_colorR;
    _rc_bck_colorR = tmp;


    tmp = _rc_bcklight_colorL;
    _rc_bcklight_colorL = _rc_bcklight_colorR;
    _rc_bcklight_colorR = tmp;

    block_rc_controls();

    int valtmp;


    valtmp = ui->slidShadow->value();
    ui->slidShadow->setValue(ui->slidShadowR->value());
    ui->slidShadowR->setValue(valtmp);

    valtmp = ui->slidShadX->value();
    ui->slidShadX->setValue(ui->slidShadXR->value());
    ui->slidShadXR->setValue(valtmp);

    valtmp = ui->slidShadY->value();
    ui->slidShadY->setValue(ui->slidShadYR->value());
    ui->slidShadYR->setValue(valtmp);


    valtmp = ui->slidBacklight->value();
    ui->slidBacklight->setValue(ui->slidBacklightR->value());
    ui->slidBacklightR->setValue(valtmp);

    valtmp = ui->slidBacklightX->value();
    ui->slidBacklightX->setValue(ui->slidBacklightXR->value());
    ui->slidBacklightXR->setValue(valtmp);

    valtmp = ui->slidBacklightY->value();
    ui->slidBacklightY->setValue(ui->slidBacklightYR->value());
    ui->slidBacklightYR->setValue(valtmp);

    valtmp = ui->slidSpec->value();
    ui->slidSpec->setValue(ui->slidSpecR->value());
    ui->slidSpecR->setValue(valtmp);

    valtmp = ui->slidSpecX->value();
    ui->slidSpecX->setValue(ui->slidSpecXR->value());
    ui->slidSpecXR->setValue(valtmp);

    valtmp = ui->slidSpecY->value();
    ui->slidSpecY->setValue(ui->slidSpecYR->value());
    ui->slidSpecYR->setValue(valtmp);


    valtmp = ui->slidTop->value();
    ui->slidTop->setValue(ui->slidTopR->value());
    ui->slidTopR->setValue(valtmp);

    valtmp = ui->slidTopX->value();
    ui->slidTopX->setValue(ui->slidTopXR->value());
    ui->slidTopXR->setValue(valtmp);

    valtmp = ui->slidTopY->value();
    ui->slidTopY->setValue(ui->slidTopYR->value());
    ui->slidTopYR->setValue(valtmp);

    unblock_rc_controls();

    update_btn_color();
    repaint_left_sphere();
    repaint_right_sphere();
}



/*void CGLSettingsDlg::loadConvex()
{
    RadianceScalingRendererPlugin * render = _glWidget->get_radiance_render();


    QString filename = QFileDialog::getOpenFileName(0, QString(),QString(),
                             tr("Images (*.png *.xpm *.jpg *.bmp *.tif)"));

    if(filename.isNull())
      return;

    changeIcon(filename,0);

     render->setConvexLit(filename);
    transition_changed(ui->slidTrans->value());
}*/

/*void CGLSettingsDlg::changeIcon(QString path,int icon)
{

  const int w = 91;

  QPixmap pix(path);

  pix = pix.scaledToWidth(w,Qt::SmoothTransformation);

    if(icon==0)
        ui->litIcon1->setPixmap(pix);
    else
        ui->litIcon2->setPixmap(pix);

}*/

/*void CGLSettingsDlg::loadConcave()
{
    QString filename = QFileDialog::getOpenFileName(0, QString(),QString(),
                             tr("Images (*.png *.xpm *.jpg *.bmp *.tif)"));

    if(filename.isNull())
      return;

    changeIcon(filename,1);

    RadianceScalingRendererPlugin * render = _glWidget->get_radiance_render();
    render->setConcavLit(filename);
    transition_changed(ui->slidTrans->value());
}*/

/*
void CGLSettingsDlg::invert_effect(int )
{
    RadianceScalingRendererPlugin * render = _glWidget->get_radiance_render();
    render->setInvert(ui->chkInvert->isChecked());
    _glWidget->udate_gl();
}*/

void CGLSettingsDlg::transition_changed(int value)
{
    RadianceScalingRendererPlugin * render = _glWidget->get_radiance_render();

    const float scale = 100.0f;
    float val = (float)value/scale;

    ui->lblTrans->setText(QString::number(val));


    render->setTransition(val);
    CGlobalFRSettings::_transition  = val;
    _glWidget->udate_gl();
}

/*
QColor CGlobalFRSettings::_rc_bck_colorR =  QColor(200,25,255);
float CGlobalFRSettings::_rc_shadowR[3] = {1.f, 0.f, 0.f};
QColor CGlobalFRSettings::_rc_bcklight_colorR = QColor(255,50,255);
float CGlobalFRSettings::_rc_backlightR[3] = {1.f, 0.f, 0.f};
float CGlobalFRSettings::_rc_toplightR[3] = {1.f, 0.f, 0.f};
float CGlobalFRSettings::_rc_speclightR[3] = {1.f, 0.f, 0.f};


*/


void CGLSettingsDlg::enhancement_changed(int value)
{
    RadianceScalingRendererPlugin * render = _glWidget->get_radiance_render();

    const float scale = 100.0f;
    float val = (float)value/scale;
    ui->lblEnh->setText(QString::number(val));

    if(val < 0)
        render->setInvert(true);
    else
        render->setInvert(false);

    render->setEnhancement(std::fabs(val));
    CGlobalFRSettings::_enhancement = val;

    //repaint_left_sphere();
    //repaint_right_sphere();

    _glWidget->udate_gl();
}


void CGLSettingsDlg::enable_radiance_render(bool )
{
    bool enableRender = (ui->chkUseRadiance->isChecked()) ? true : false;
    RadianceScalingRendererPlugin * render = _glWidget->get_radiance_render();

    //global error if render was not consructed

    render->turn(false);

    if(!render->isSupported())
        QMessageBox::warning(this, tr("Warning"), err_RenderNotSupported);

    if( render->isSupported() && enableRender)
    {
        render->turn(true);
        ui->chkUseRadiance->blockSignals(true);
        ui->chkUseRadiance->setChecked(true);
        ui->chkUseRadiance->blockSignals(false);
    }
    else
    {
         ui->chkUseRadiance->blockSignals(true);
        ui->chkUseRadiance->setChecked(false);
        ui->chkUseRadiance->blockSignals(false);
    }

    CGlobalFRSettings::_use = render->turned_on();

    _glWidget->udate_gl();
}

void CGLSettingsDlg::init_interface()
{
    ui->slidLPosX->setValue(CGlobalHSSettings::gl_light_Position[0]);
    ui->slidLPosY->setValue(CGlobalHSSettings::gl_light_Position[1]);
    ui->slidLPosZ->setValue(CGlobalHSSettings::gl_light_Position[2]);

    ui->slidMatSpec->setValue(CGlobalHSSettings::gl_mat_Specularity[0]*256.);
    //ui->slidMatShin->setValue(CGlobalHSSettings::gl_mat_Shiness);
    ui->slidCamAlpha->setValue(CGlobalHSSettings::gl_cam_alpha*100);

    ui->sldTableZPos->setValue(CGlobalHSSettings::gl_table_rot_center_y * 10);
}

void CGLSettingsDlg::actualize_interface()
{

    ui->slidLPosX->blockSignals(true);
    ui->slidLPosY->blockSignals(true);
    ui->slidLPosZ->blockSignals(true);
    ui->slidMatSpec->blockSignals(true);
    ui->slidCamAlpha->blockSignals(true);
    ui->sldTableZPos->blockSignals(true);

    init_interface();

    ui->slidLPosX->blockSignals(false);
    ui->slidLPosY->blockSignals(false);
    ui->slidLPosZ->blockSignals(false);
    ui->slidMatSpec->blockSignals(false);
    ui->slidCamAlpha->blockSignals(false);
    ui->sldTableZPos->blockSignals(false);

    RadianceScalingRendererPlugin * render = _glWidget->get_radiance_render();
    if(render != NULL)
    {
        ui->chkUseRadiance->setChecked(CGlobalFRSettings::_use);

        block_rc_controls();

        _rc_bck_colorL = CGlobalFRSettings::_rc_bck_colorL;
        _rc_bcklight_colorL = CGlobalFRSettings::_rc_bcklight_colorL;
        _rc_bck_colorR = CGlobalFRSettings::_rc_bck_colorR;
        _rc_bcklight_colorR = CGlobalFRSettings::_rc_bcklight_colorR;

        ui->slidShadow->setValue(CGlobalFRSettings::_rc_shadowL[0]);
        ui->slidShadowR->setValue(CGlobalFRSettings::_rc_shadowR[0]);
        ui->slidShadX->setValue(CGlobalFRSettings::_rc_shadowL[1]);
        ui->slidShadXR->setValue(CGlobalFRSettings::_rc_shadowR[1]);
        ui->slidShadY->setValue(CGlobalFRSettings::_rc_shadowL[2]);
        ui->slidShadYR->setValue(CGlobalFRSettings::_rc_shadowR[2]);

        ui->slidBacklight->setValue(CGlobalFRSettings::_rc_backlightL[0]);
        ui->slidBacklightR->setValue(CGlobalFRSettings::_rc_backlightR[0]);
        ui->slidBacklightX->setValue(CGlobalFRSettings::_rc_backlightL[1]);
        ui->slidBacklightXR->setValue(CGlobalFRSettings::_rc_backlightR[1]);
        ui->slidBacklightY->setValue(CGlobalFRSettings::_rc_backlightL[2]);
        ui->slidBacklightYR->setValue(CGlobalFRSettings::_rc_backlightR[2]);


        ui->slidTop->setValue(CGlobalFRSettings::_rc_toplightL[0]);
        ui->slidTopR->setValue(CGlobalFRSettings::_rc_toplightR[0]);
        ui->slidTopX->setValue(CGlobalFRSettings::_rc_toplightL[1]);
        ui->slidTopXR->setValue(CGlobalFRSettings::_rc_toplightR[1]);
        ui->slidTopY->setValue(CGlobalFRSettings::_rc_toplightL[2]);
        ui->slidTopYR->setValue(CGlobalFRSettings::_rc_toplightR[2]);

        ui->slidSpec->setValue(CGlobalFRSettings::_rc_speclightL[0]);
        ui->slidSpecR->setValue(CGlobalFRSettings::_rc_speclightR[0]);
        ui->slidSpecX->setValue(CGlobalFRSettings::_rc_speclightL[1]);
        ui->slidSpecXR->setValue(CGlobalFRSettings::_rc_speclightR[1]);
        ui->slidSpecY->setValue(CGlobalFRSettings::_rc_speclightL[2]);
        ui->slidSpecYR->setValue(CGlobalFRSettings::_rc_speclightR[2]);

        unblock_rc_controls();


        ui->slidEnhance->setValue(CGlobalFRSettings::_enhancement  * 100);
        ui->slidTrans->setValue(CGlobalFRSettings::_transition * 100);


    }

    update_btn_color();
    repaint_left_sphere();
    repaint_right_sphere();
}

void CGLSettingsDlg::showEvent(QShowEvent * event)
{
    actualize_interface();
}

void CGLSettingsDlg::repaint_left_sphere()
{

    QPixmap pix_result(_rc_min_sz, _rc_min_sz);
    pix_result.fill(Qt::transparent);

    QImage img_collt(_rc_max_sz,_rc_max_sz,  QImage::Format_ARGB32 );
    img_collt.fill(_rc_bcklight_colorL);


    QImage img_bckg(_rc_max_sz, _rc_max_sz,  QImage::Format_RGB32);
    int prev_width = _pix_mask.width();
    int off = (pix_result.width() - prev_width)/2;

    int rwidth = 256;
    int roff = (pix_result.width() - rwidth)/2;
    img_bckg.fill(_rc_bck_colorL);
    {
        QPixmap pix_bckg = QPixmap::fromImage(img_bckg);
        QPixmap pix_collt = QPixmap::fromImage(img_collt);

        QPainter painter(&pix_result);
        painter.setCompositionMode(QPainter::CompositionMode_Multiply);
        painter.drawPixmap(0, 0, pix_bckg);
        painter.setCompositionMode(QPainter::CompositionMode_SourceOver);

        painter.setOpacity(float(ui->slidShadow->value())/100.);
        painter.drawPixmap(ui->slidShadX->value(), ui->slidShadY->value(), _pix_shad);


        {
            QPainter painter_bklt(&_pix_bklt);
            painter_bklt.setCompositionMode(QPainter::CompositionMode_SourceAtop);
            painter_bklt.drawPixmap(0, 0, pix_collt);
        }

        painter.setOpacity(float(ui->slidBacklight->value())/100.);
        painter.drawPixmap(ui->slidBacklightX->value(), ui->slidBacklightY->value(), _pix_bklt);


        painter.setOpacity(float(ui->slidTop->value())/100.);
        painter.drawPixmap(ui->slidTopX->value(), ui->slidTopY->value(), _pix_tplt);

        painter.setOpacity(float(ui->slidSpec->value())/100.);
        painter.drawPixmap(ui->slidSpecX->value(), ui->slidSpecY->value(), _pix_splt);

        QPixmap pix_result_cropped = pix_result.copy(roff, roff, rwidth, rwidth);
        {
            QPainter painter_rc(&pix_result_cropped);
            painter_rc.setCompositionMode(QPainter::CompositionMode_SourceOver);
            painter_rc.drawPixmap(0,0, _pix_rmask);
        }
        QImage left_rc_img = pix_result_cropped.toImage();
        QString filename("./left_sphere.png");
        left_rc_img.save(filename);

        RadianceScalingRendererPlugin * render = _glWidget->get_radiance_render();

        if(ui->slidEnhance->value()>=0)
            render->setConvexLit(filename);
        else
            render->setConcavLit(filename);


        transition_changed( CGlobalFRSettings::_transition * 100); // just proper gl redraw

        painter.setOpacity(1);
        painter.drawPixmap(off, off, _pix_mask);



    }
    pix_result = pix_result.copy(off, off, prev_width, prev_width);

    pix_result = pix_result.scaledToWidth(_rc_lbl_sz, Qt::SmoothTransformation);
    ui->lblLeftSphere->setPixmap(pix_result);

    CGlobalFRSettings::_rc_bck_colorL = _rc_bck_colorL;

    CGlobalFRSettings::_rc_shadowL[0] = ui->slidShadow->value();
    CGlobalFRSettings::_rc_shadowL[1] = ui->slidShadX->value();
    CGlobalFRSettings::_rc_shadowL[2] = ui->slidShadY->value();

    CGlobalFRSettings::_rc_bcklight_colorL = _rc_bcklight_colorL;

    CGlobalFRSettings::_rc_backlightL[0] = ui->slidBacklight->value();
    CGlobalFRSettings::_rc_backlightL[1] = ui->slidBacklightX->value();
    CGlobalFRSettings::_rc_backlightL[2] = ui->slidBacklightY->value();

    CGlobalFRSettings::_rc_toplightL[0] = ui->slidTop->value();
    CGlobalFRSettings::_rc_toplightL[1] = ui->slidTopX->value();
    CGlobalFRSettings::_rc_toplightL[2] = ui->slidTopY->value();

    CGlobalFRSettings::_rc_speclightL[0] = ui->slidSpec->value();
    CGlobalFRSettings::_rc_speclightL[1] = ui->slidSpecX->value();
    CGlobalFRSettings::_rc_speclightL[2] = ui->slidSpecY->value();


}



void CGLSettingsDlg::repaint_right_sphere()
{

    QPixmap pix_result(_rc_min_sz, _rc_min_sz);
    pix_result.fill(Qt::transparent);

    QImage img_collt(_rc_max_sz,_rc_max_sz,  QImage::Format_ARGB32 );
    img_collt.fill(_rc_bcklight_colorR);


    QImage img_bckg(_rc_max_sz, _rc_max_sz,  QImage::Format_RGB32);
    int prev_width = _pix_mask.width();
    int off = (pix_result.width() - prev_width)/2;
    int rwidth = 256;
    int roff = (pix_result.width() - rwidth)/2;

    img_bckg.fill(_rc_bck_colorR);
    {
        QPixmap pix_bckg = QPixmap::fromImage(img_bckg);
        QPixmap pix_collt = QPixmap::fromImage(img_collt);

        QPainter painter(&pix_result);
        painter.setCompositionMode(QPainter::CompositionMode_Multiply);
        painter.drawPixmap(0, 0, pix_bckg);
        painter.setCompositionMode(QPainter::CompositionMode_SourceOver);

        painter.setOpacity(float(ui->slidShadowR->value())/100.);
        painter.drawPixmap(ui->slidShadXR->value(), ui->slidShadYR->value(), _pix_shad);

        {
            QPainter painter_bklt(&_pix_bklt);
            painter_bklt.setCompositionMode(QPainter::CompositionMode_SourceAtop);
            painter_bklt.drawPixmap(0, 0, pix_collt);
        }

        painter.setOpacity(float(ui->slidBacklightR->value())/100.);
        painter.drawPixmap(ui->slidBacklightXR->value(), ui->slidBacklightYR->value(), _pix_bklt);

        painter.setOpacity(float(ui->slidTopR->value())/100.);
        painter.drawPixmap(ui->slidTopXR->value(), ui->slidTopYR->value(), _pix_tplt);

        painter.setOpacity(float(ui->slidSpecR->value())/100.);
        painter.drawPixmap(ui->slidSpecXR->value(), ui->slidSpecYR->value(), _pix_splt);



        QPixmap pix_result_cropped = pix_result.copy(roff, roff, rwidth, rwidth);
        {
            QPainter painter_rc(&pix_result_cropped);
            painter_rc.setCompositionMode(QPainter::CompositionMode_SourceOver);
            painter_rc.drawPixmap(0,0, _pix_rmask);
        }
        QImage right_rc_img  = pix_result_cropped.toImage();
        QString filename("./right_sphere.png");
        right_rc_img.save(filename);


        RadianceScalingRendererPlugin * render = _glWidget->get_radiance_render();

        if(ui->slidEnhance->value()>=0)
            render->setConcavLit(filename);
        else
            render->setConvexLit(filename);

        transition_changed(CGlobalFRSettings::_transition * 100 ); // just proper gl redraw

        painter.setOpacity(1);
        painter.drawPixmap(off, off, _pix_mask);



    }
    pix_result = pix_result.copy(off, off, prev_width, prev_width);

    pix_result = pix_result.scaledToWidth(_rc_lbl_sz, Qt::SmoothTransformation);
    ui->lblRightSphere->setPixmap(pix_result);

    CGlobalFRSettings::_rc_bck_colorR = _rc_bck_colorR;

    CGlobalFRSettings::_rc_shadowR[0] = ui->slidShadowR->value();
    CGlobalFRSettings::_rc_shadowR[1] = ui->slidShadXR->value();
    CGlobalFRSettings::_rc_shadowR[2] = ui->slidShadYR->value();

    CGlobalFRSettings::_rc_bcklight_colorR = _rc_bcklight_colorR;

    CGlobalFRSettings::_rc_backlightR[0] = ui->slidBacklightR->value();
    CGlobalFRSettings::_rc_backlightR[1] = ui->slidBacklightXR->value();
    CGlobalFRSettings::_rc_backlightR[2] = ui->slidBacklightYR->value();

    CGlobalFRSettings::_rc_toplightR[0] = ui->slidTopR->value();
    CGlobalFRSettings::_rc_toplightR[1] = ui->slidTopXR->value();
    CGlobalFRSettings::_rc_toplightR[2] = ui->slidTopYR->value();

    CGlobalFRSettings::_rc_speclightR[0] = ui->slidSpecR->value();
    CGlobalFRSettings::_rc_speclightR[1] = ui->slidSpecXR->value();
    CGlobalFRSettings::_rc_speclightR[2] = ui->slidSpecYR->value();
}



CGLSettingsDlg::~CGLSettingsDlg()
{
    delete ui;
}

void CGLSettingsDlg::change_cam_opacity(int v)
{
     CGlobalHSSettings::gl_cam_alpha = (float)ui->slidCamAlpha->value()/100.f;
    _glWidget->on_new_campos();
}

void CGLSettingsDlg::pointsize_changed(int )
{
    float nv = (float)ui->slidPointSz->value()/10.f;
    CGlobalHSSettings::gpoint_size = nv;
    _glWidget->on_rawpnts_updated();
    _glWidget->udate_gl();
}

void CGLSettingsDlg::change_mat_spec(int )
{
    float nv = (float)ui->slidMatSpec->value()/128.f;
    CGlobalHSSettings::gl_mat_Specularity[0] = nv;
    CGlobalHSSettings::gl_mat_Specularity[1] = nv;
    CGlobalHSSettings::gl_mat_Specularity[2] = nv;
    _glWidget->on_mesh_updated();
    _glWidget->udate_gl();
}

/*
void CGLSettingsDlg::change_mat_shin(int )
{
    CGlobalHSSettings::gl_mat_Shiness = ui->slidMatShin->value();
    _glWidget->on_mesh_updated();
    _glWidget->udate_gl();
}*/

void CGLSettingsDlg::change_light_posX(int )
{
    CGlobalHSSettings::gl_light_Position[0] =  ui->slidLPosX->value();
    _glWidget->udate_gl();
}


void CGLSettingsDlg::change_light_posY(int )
{
    CGlobalHSSettings::gl_light_Position[1] =  ui->slidLPosY->value();
    _glWidget->udate_gl();
}


void CGLSettingsDlg::change_light_posZ(int )
{
    CGlobalHSSettings::gl_light_Position[2] =  ui->slidLPosZ->value();
    _glWidget->udate_gl();
}

void CGLSettingsDlg::dyn_change_light_amb_color(const QColor & color)
{
    CGlobalHSSettings::gl_light_Ambient[0] = (float)color.red()/255.;
    CGlobalHSSettings::gl_light_Ambient[1] = (float)color.green()/255.;
    CGlobalHSSettings::gl_light_Ambient[2] = (float)color.blue()/255.;

    _glWidget->udate_gl();

    update_btn_color();
}

void CGLSettingsDlg::change_light_amb_color()
{
    QColorDialog cd(this);
    connect(&cd, SIGNAL(currentColorChanged(const QColor&)), this, SLOT(dyn_change_light_amb_color(const QColor&)));

    /*
    cd.setStandardColor(0, QColor(255,255,255).rgb());
    cd.setStandardColor(1, QColor(255,0,0).rgb());
    cd.setStandardColor(2, QColor(0,255,0).rgb());
    cd.setStandardColor(3, QColor(0,0,255).rgb());
    */


    QColor orig_color(CGlobalHSSettings::gl_light_Ambient[0]*255.,
                           CGlobalHSSettings::gl_light_Ambient[1]*255.,
                           CGlobalHSSettings::gl_light_Ambient[2]*255.);
    cd.setCurrentColor(orig_color);
    if(cd.exec() == QDialog::Accepted)
    {
        QColor color = cd.selectedColor();
        dyn_change_light_amb_color(color);
    }
    else
        dyn_change_light_amb_color(orig_color);

}


void CGLSettingsDlg::dyn_change_light_dif_color(const QColor & color)
{
    CGlobalHSSettings::gl_light_Diffuse[0] = (float)color.red()/255.;
    CGlobalHSSettings::gl_light_Diffuse[1] = (float)color.green()/255.;
    CGlobalHSSettings::gl_light_Diffuse[2] = (float)color.blue()/255.;

    _glWidget->udate_gl();
    update_btn_color();
}

void CGLSettingsDlg::change_light_dif_color()
{
    QColorDialog cd(this);
    connect(&cd, SIGNAL(currentColorChanged(const QColor&)), this, SLOT(dyn_change_light_dif_color(const QColor&)));

    QColor orig_color(CGlobalHSSettings::gl_light_Diffuse[0]*255.,
                           CGlobalHSSettings::gl_light_Diffuse[1]*255.,
                           CGlobalHSSettings::gl_light_Diffuse[2]*255.);

    cd.setCurrentColor(orig_color);
    if(cd.exec() == QDialog::Accepted)
    {
        QColor color = cd.selectedColor();
        dyn_change_light_dif_color(color);
    }
    else
        dyn_change_light_dif_color(orig_color);
}


void CGLSettingsDlg::dyn_change_surface_color(const QColor & c)
{
    CGlobalHSSettings::gcolor_mr = (float)c.red()/255.;
    CGlobalHSSettings::gcolor_mg = (float)c.green()/255.;
    CGlobalHSSettings::gcolor_mb = (float)c.blue()/255.;
    update_btn_color();
    _glWidget->on_mesh_updated();
}

void CGLSettingsDlg::change_surface_color()
{
    QColorDialog cd(this);
    connect(&cd, SIGNAL(currentColorChanged(const QColor&)), this, SLOT(dyn_change_surface_color(const QColor&)));

    QColor orig_color(  CGlobalHSSettings::gcolor_mr*255.,
                        CGlobalHSSettings::gcolor_mg*255.,
                        CGlobalHSSettings::gcolor_mb*255.);
    cd.setCurrentColor(orig_color);

    if(cd.exec() == QDialog::Accepted)
    {
        QColor color = cd.selectedColor();
        dyn_change_surface_color(color);
    }
    else
        dyn_change_surface_color(orig_color);
}


void CGLSettingsDlg::dyn_change_bckg_cent_color(const QColor & color)
{
    CGlobalHSSettings::gcolor_cr = (float)color.red()/255.;
    CGlobalHSSettings::gcolor_cg = (float)color.green()/255.;
    CGlobalHSSettings::gcolor_cb = (float)color.blue()/255.;

    update_btn_color();
    _glWidget->udate_gl();
     _glWidget->udate_gl();
}

void CGLSettingsDlg::change_bckg_cent_color()
{
    QColorDialog cd(this);
    connect(&cd, SIGNAL(currentColorChanged(const QColor&)), this, SLOT(dyn_change_bckg_cent_color(const QColor&)));
    QColor orig_color (CGlobalHSSettings::gcolor_cr*255.,
                                  CGlobalHSSettings::gcolor_cg*255.,
                                  CGlobalHSSettings::gcolor_cb*255.);

    cd.setCurrentColor(orig_color);
    if(cd.exec() == QDialog::Accepted)
    {
        QColor color = cd.selectedColor();
        dyn_change_bckg_cent_color(color);
    }
    else
        dyn_change_bckg_cent_color(orig_color);


}

void CGLSettingsDlg::dyn_change_bckg_side_color(const QColor & color)
{
    CGlobalHSSettings::gcolor_sr = (float)color.red()/255.;
    CGlobalHSSettings::gcolor_sg = (float)color.green()/255.;
    CGlobalHSSettings::gcolor_sb = (float)color.blue()/255.;

    update_btn_color();

   ((MainWindow *) _mw)->updateFieldButton();
    _glWidget->udate_gl();
    _glWidget->udate_gl();
}

void CGLSettingsDlg::dyn_change_rc_canv_color_left(const QColor& color)
{
    _rc_bck_colorL = color;
     update_btn_color();
     repaint_left_sphere();
}

void CGLSettingsDlg::change_rc_canv_color_left()
{

    QColorDialog cd(this);

    connect(&cd, SIGNAL(currentColorChanged(const QColor&)), this, SLOT(dyn_change_rc_canv_color_left(const QColor&)));
    QColor orig_color = _rc_bck_colorL;

    cd.setCurrentColor(_rc_bck_colorL);
    if(cd.exec() == QDialog::Accepted)
        dyn_change_rc_canv_color_left(cd.selectedColor());
    else
        dyn_change_rc_canv_color_left(orig_color);

}

void CGLSettingsDlg::dyn_change_rc_canv_color_right(const QColor& color)
{
    _rc_bck_colorR = color;
     update_btn_color();
     repaint_right_sphere();

}

void CGLSettingsDlg::change_rc_canv_color_right()
{

    QColorDialog cd(this);
    connect(&cd, SIGNAL(currentColorChanged(const QColor&)), this, SLOT(dyn_change_rc_canv_color_right(const QColor&)));
    QColor orig_color = _rc_bck_colorR;

    cd.setCurrentColor(_rc_bck_colorR);
    if(cd.exec() == QDialog::Accepted)
        dyn_change_rc_canv_color_right(cd.selectedColor());
    else
        dyn_change_rc_canv_color_right(orig_color);
}

void CGLSettingsDlg::dyn_change_rc_bklit_color_left(const QColor& color)
{
    _rc_bcklight_colorL = color;
     update_btn_color();
     repaint_left_sphere();
}

void CGLSettingsDlg::change_rc_bklit_color_left()
{
    QColorDialog cd(this);
    connect(&cd, SIGNAL(currentColorChanged(const QColor&)), this, SLOT(dyn_change_rc_bklit_color_left(const QColor&)));
    QColor orig_color =_rc_bcklight_colorL;

    cd.setCurrentColor(_rc_bcklight_colorL);
    if(cd.exec() == QDialog::Accepted)
        dyn_change_rc_bklit_color_left(cd.selectedColor());
    else
        dyn_change_rc_bklit_color_left(orig_color);

}

void CGLSettingsDlg::dyn_change_rc_bklit_color_right(const QColor& color)
{
    _rc_bcklight_colorR = color;
     update_btn_color();
     repaint_right_sphere();
}

void CGLSettingsDlg::change_rc_bklit_color_right()
{
    QColorDialog cd(this);
    connect(&cd, SIGNAL(currentColorChanged(const QColor&)), this, SLOT(dyn_change_rc_bklit_color_right(const QColor&)));
    QColor orig_color = _rc_bcklight_colorR;
    cd.setCurrentColor(_rc_bcklight_colorR);
    if(cd.exec() == QDialog::Accepted)
        dyn_change_rc_bklit_color_right(cd.selectedColor());
    else
        dyn_change_rc_bklit_color_right(orig_color);

}

void CGLSettingsDlg::change_bckg_side_color()
{
    QColorDialog cd(this);
    connect(&cd, SIGNAL(currentColorChanged(const QColor&)), this, SLOT(dyn_change_bckg_side_color(const QColor&)));
    QColor orig_color(CGlobalHSSettings::gcolor_sr*255.,
                                  CGlobalHSSettings::gcolor_sg*255.,
                                  CGlobalHSSettings::gcolor_sb*255.);
    cd.setCurrentColor(orig_color);
    if(cd.exec() == QDialog::Accepted)
    {
        QColor color = cd.selectedColor();
        dyn_change_bckg_side_color(color);
    }
    else
        dyn_change_bckg_side_color(orig_color);
}

void CGLSettingsDlg::update_btn_color()
{
    QString bckgcol("background-color: rgb(%1,%2,%3);");


    QString c1 = bckgcol.arg(CGlobalHSSettings::gcolor_sr*255.).arg(CGlobalHSSettings::gcolor_sg*255.).arg(CGlobalHSSettings::gcolor_sb*255.);
    ui->btnBckgSideCol->setStyleSheet(c1);
    ui->btnBckgSideCol->repaint();

    QString c2 = bckgcol.arg(CGlobalHSSettings::gcolor_cr*255.).arg(CGlobalHSSettings::gcolor_cg*255.).arg(CGlobalHSSettings::gcolor_cb*255.);
    ui->btnBckgCentCol->setStyleSheet(c2);
    ui->btnBckgCentCol->repaint();

    QString c3 = bckgcol.arg(CGlobalHSSettings::gcolor_mr*255.).arg(CGlobalHSSettings::gcolor_mg*255.).arg(CGlobalHSSettings::gcolor_mb*255.);
    ui->btnSurfCol->setStyleSheet(c3);
    ui->btnSurfCol->repaint();

    QString c4 = bckgcol.arg(CGlobalHSSettings::gl_light_Ambient[0]*255.).arg(CGlobalHSSettings::gl_light_Ambient[1]*255.).arg(CGlobalHSSettings::gl_light_Ambient[2]*255.);
    ui->btnLAmbCol->setStyleSheet(c4);
    ui->btnLAmbCol->repaint();

    QString c5 = bckgcol.arg(CGlobalHSSettings::gl_light_Diffuse[0]*255.).arg(CGlobalHSSettings::gl_light_Diffuse[1]*255.).arg(CGlobalHSSettings::gl_light_Diffuse[2]*255.);
    ui->btnLDifCol->setStyleSheet(c5);
    ui->btnLDifCol->repaint();

    QString c6 = bckgcol.arg(_rc_bck_colorL.red()).arg(_rc_bck_colorL.green()).arg(_rc_bck_colorL.blue());
    ui->btnCanvCol->setStyleSheet(c6);
    ui->btnCanvCol->repaint();

    QString c7 = bckgcol.arg(_rc_bcklight_colorL.red()).arg(_rc_bcklight_colorL.green()).arg(_rc_bcklight_colorL.blue());
    ui->btnBckLightCol->setStyleSheet(c7);
    ui->btnBckLightCol->repaint();

    QString c8 = bckgcol.arg(_rc_bck_colorR.red()).arg(_rc_bck_colorR.green()).arg(_rc_bck_colorR.blue());
    ui->btnCanvColR->setStyleSheet(c8);
    ui->btnCanvColR->repaint();

    QString c9 = bckgcol.arg(_rc_bcklight_colorR.red()).arg(_rc_bcklight_colorR.green()).arg(_rc_bcklight_colorR.blue());
    ui->btnBckLightColR->setStyleSheet(c9);
    ui->btnBckLightColR->repaint();


    this->repaint();

}

