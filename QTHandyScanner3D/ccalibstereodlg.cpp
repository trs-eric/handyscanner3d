#include "ccalibstereodlg.h"
#include "ui_ccalibstereodlg.h"


#include <opencv2/features2d/features2d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/contrib/contrib.hpp>

CCalibStereoDlg::CCalibStereoDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CCalibStereoDlg)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);

   // cv::Mat R(10, 10, cv::DataType<float>::type, 3);
  //  cv::FileStorage fs("opencv_test.yml", cv::FileStorage::WRITE);
   // fs << "R " << R;
}

CCalibStereoDlg::~CCalibStereoDlg()
{
    delete ui;
}
