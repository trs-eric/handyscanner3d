#include "cglcamera.h"

CGLCamera::CGLCamera()
{
    _xs = 1.0f;
    _ys = 1.0f;
    _zs = 1.0f;

    _ux = 0.0f;
    _uy = 1.0f;
    _uz = 0.0f;

    _cx = 0.0f;
    _cy = 1.0f;
    _cz = 0.0f;


}

CGLCamera::~CGLCamera()
{
}

void CGLCamera::create_glList()
{
    // nothing to be drawn
};

void CGLCamera::set_view()
{
    update_pos();

    gluLookAt(	_tx,	_ty,	 _tz,
                _cx,	_cy,	 _cz,
                _ux,	_uy,	 _uz);
}

void CGLCamera::glDraw()
{
    // nothing to be drawn
}
