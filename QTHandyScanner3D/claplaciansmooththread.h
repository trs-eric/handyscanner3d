#ifndef CLAPLACIANSMOOTHTHREAD_H
#define CLAPLACIANSMOOTHTHREAD_H

#include <QThread>
#include <QMutex>
#include "graphicutils.h"
#include "ctriangularmesh.h"

class CTriangularMesh;

class CLaplacianSmoothThread : public QThread
{
    Q_OBJECT
public:
    explicit CLaplacianSmoothThread(QObject *parent = 0);
    ~CLaplacianSmoothThread();

    void start_process(CTriangularMesh * mesh, float , int);
    bool is_abort(){return _abort;}
    void emit_send_back(int i);

signals:
    void send_back(const int val);
    void send_result(CTriangularMesh *);



protected:
    void run();

public slots:
    void stop_process();


private:
    bool    _abort;
    QMutex  _mutex;

    CTriangularMesh * _inmesh;
    float _degree;
    int   _iteration;

};



#endif // CLAPLACIANSMOOTHTHREAD_H
