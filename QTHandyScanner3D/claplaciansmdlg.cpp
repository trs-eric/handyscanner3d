#include "claplaciansmdlg.h"
#include "ui_claplaciansmdlg.h"
#include "cglwidget.h"

#include "claplaciansmooththread.h"
#include "cmeshdumpthread.h"
#include "messages.h"

std::string lap_mesh_dump_str("./lapmesh.dump");


CLaplacianSmDlg::CLaplacianSmDlg(CGLWidget * glWidget, QWidget *parent) :
    QDialog(parent),  _glWidget(glWidget),
    ui(new Ui::CLaplacianSmDlg)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);

    ui->edtDevDeg->setText(QString::number(30));
    ui->edtIteration->setText(QString::number(5));
    ui->progressBar->setMaximum(100);

    connect(ui->previewBtn, SIGNAL(clicked()), this, SLOT(onPreviewBtn()));
    connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));

    _thread = new CLaplacianSmoothThread;

    // user pressed cancel button

    connect(ui->cancelBtn,  SIGNAL(clicked()), _thread, SLOT(stop_process()));
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

    // user closed dialg
     connect(this,  SIGNAL(finished(int)), _thread, SLOT(stop_process()));
    // working with a thread
    connect(_thread, SIGNAL(send_back(int)), this, SLOT(onMakeProgressStep(int)));
    connect(_thread, SIGNAL(send_result(CTriangularMesh*)), this, SLOT(onCompleteProcessing(CTriangularMesh*)));
    connect(_thread, SIGNAL(send_result(CTriangularMesh*)), _glWidget, SLOT(on_mesh_computed(CTriangularMesh*)));
    connect(_thread, SIGNAL(finished()), this, SLOT(onStopProcessing()));


    //// working with mesh dumping thread
    _info = new QMessageBox(this);
    _ithread = new CMeshDumpThread;
    connect(_ithread, SIGNAL(done_dumping()), this, SLOT(onFinishDumping()));

}

CLaplacianSmDlg::~CLaplacianSmDlg()
{
    delete ui;
    delete _thread;
    delete _info;
    delete _ithread;
}

void CLaplacianSmDlg::onMakeProgressStep(int val)
{
    ui->progressBar->setValue(val);
}

void CLaplacianSmDlg::onCompleteProcessing(CTriangularMesh * m)
{
   if(m == NULL)
   {
       QMessageBox::warning(this, tr("Warning"),wrn_FailedMesh);
       return;
   }
   if(_on_apply)
   {
       close_dialog();
       return;
   }

   _is_result_valid = true;
   ui->progressBar->setValue(100);

}

void CLaplacianSmDlg::onStopProcessing()
{
    ui->applyBtn    ->setEnabled(true);
    ui->previewBtn  ->setEnabled(true);
    ui->edtDevDeg   ->setEnabled(true);
    ui->edtIteration->setEnabled(true);

    _in_process = false;
}

void CLaplacianSmDlg::close_dialog()
{
    _on_apply = false;
    close();
}

CTriangularMesh * CLaplacianSmDlg::restore_mesh()
{
    _glWidget->discard_mesh_of_current_object();
    std::ifstream ifile;
    ifile.open(lap_mesh_dump_str.c_str(), std::ios::in | std::ios::binary);
    CTriangularMesh * mesh = new CTriangularMesh;
    mesh->load(ifile);
    ifile.close();
    _glWidget->on_mesh_computed(mesh);

    return mesh;
}

void CLaplacianSmDlg::onCancelBtn()
{
    if(!_in_process)
    {
        if(_is_result_valid)
                restore_mesh();
        close_dialog();
    }

       _in_process = false;
       _on_apply = false;


}

void CLaplacianSmDlg::onPreviewBtn()
{
    CTriangularMesh * mesh = _glWidget->get_mesh_of_current_object() ;
    if(mesh == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NoMesh);
        return;
    }

    ui->progressBar->setValue(0);
    ui->applyBtn    ->setEnabled(false);
    ui->previewBtn  ->setEnabled(false);
    ui->edtDevDeg   ->setEnabled(false);
    ui->edtIteration->setEnabled(false);
    _in_process = true;


    if(_is_result_valid)
    {
        CTriangularMesh * mesh = restore_mesh();
        _thread->start_process(mesh, ui->edtDevDeg->text().toFloat(),
                               ui->edtIteration->text().toInt());
    }
    else
        _thread->start_process(_glWidget->get_mesh_of_current_object(),
                               ui->edtDevDeg->text().toFloat(),
                                ui->edtIteration->text().toInt());
}

void CLaplacianSmDlg::onApplyBtn()
{
    _on_apply = true;
    if(!_is_result_valid)
        onPreviewBtn();
    else
        close_dialog();
}


void CLaplacianSmDlg::showEvent(QShowEvent * event)
{
    ui->progressBar->setValue(0);
    _is_result_valid    = false;
    _on_apply           = false;
    _in_process         = false;

    CTriangularMesh * cmesh = _glWidget->get_mesh_of_current_object();


   if(cmesh != NULL)
   {
        _ithread->start_process(cmesh,lap_mesh_dump_str);


        _info->setText("Dumping mesh to external file ... ");
        _info->setStandardButtons(0);
        //info->setWindowFlags(Qt::WindowStaysOnTopHint);
        _info->show();
   }



}

void CLaplacianSmDlg::onFinishDumping()
{
    std::cout << "onFinishDumping" << std::endl;

    _info->setText("Dumping mesh to external file ... done");
    _info->hide();
}
