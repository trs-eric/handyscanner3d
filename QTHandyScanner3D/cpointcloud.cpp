#include "cpointcloud.h"
#include<QMessageBox>


#include <fstream>
#include <sstream>
#include <iterator>
#include "messages.h"
#include "cglcamera.h"

#include "settings/psettings.h"
extern CGLCamera * gReferencePoint;

CPointCloud::CPointCloud()
{
    _color[0] = 0.8f;
    _color[1] = 0.8f;
    _color[2] = 0.8f;

    _use_viewref        = false;
    _use_common_color   = false;

    _use_temp_color_scheme = false;

    _tmp_brightness = 0;
    _tmp_contrast = 0;
    _tmp_gamma = 0;

}


void CPointCloud::save(std::ofstream & ofile)
{
    int rpoint_count = _points.size();
    ofile.write(reinterpret_cast<const char *>(&rpoint_count), sizeof(int));

    ofile.write(reinterpret_cast<const char *>(_color), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(_color + 1), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(_color + 2), sizeof(float));

    // std::cout << "CPointCloud::save " << rpoint_count << std::endl;

    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
        it->save(ofile);
}

void CPointCloud::load(std::ifstream & ifile)
{
    delete_glLists();
    _points.clear();

    int rpoint_count = 0;
    ifile.read(reinterpret_cast<char *>(&rpoint_count), sizeof(int));

    ifile.read(reinterpret_cast<char *>(_color), sizeof(float));
    ifile.read(reinterpret_cast<char *>(_color+1), sizeof(float));
    ifile.read(reinterpret_cast<char *>(_color+2), sizeof(float));

    // std::cout << "CPointCloud::load " << rpoint_count << std::endl;

    for(int i = 0; i < rpoint_count ; i++)
    {
        C3DPoint p;
        p.load(ifile);
        _points.push_back(p);
    }

}

bool CPointCloud::import(QString & file)
{
    if(file.isEmpty()) return false;

    bool res = false;
    // xyz file format
    if(file.endsWith(".xyz", Qt::CaseInsensitive))
        res = import_from_file_xyz(file.toStdString());
    return res;


}

bool CPointCloud::pexport(QString & file)
{
    if(file.isEmpty()) return false;

    bool res = false;
    // xyz file format
    if(file.endsWith(".txt", Qt::CaseInsensitive))
        res = export_xyz_pnts(file.toStdString());
    return res;
}

bool CPointCloud::export_xyz_pnts(std::string fname)
{
    std::ofstream ofile;
    ofile.open(fname.c_str(), std::ios::out );
    if(! ofile.is_open())
                return false;

    // output vertices ony
    std::vector<C3DPoint>::iterator itv =  _points.begin();
    for(  ; itv != _points.end(); itv++ )
        ofile <<  itv->_x <<  " " << itv->_y<< " " << itv->_z << " " << itv->_r << " " << itv->_g << " " << itv->_b <<std::endl;; // << " " << itv->_r <<  " " << itv->_g<< " " << itv->_b <<std::endl;

    ofile.close();
    return true;
}

bool CPointCloud::import_from_file_xyz(std::string fname)
{
    delete_glLists();
    _points.clear();

    std::ifstream ifile;
    ifile.open(fname.c_str(), std::ios::in );
    if(ifile.bad())
        return false;

    std::string line;



    while(ifile.good())
    {
        std::getline (ifile, line);
        std::istringstream iss(line);
        std::vector<std::string> tokens;
        std::copy(std::istream_iterator<std::string>(iss),
                    std::istream_iterator<std::string>(),
                    std::back_inserter<std::vector<std::string> >(tokens));

        if(tokens.size() < 3 )
            continue;

        C3DPoint pval;
        pval._x =  atof(tokens[0].c_str());
        pval._y =  atof(tokens[1].c_str());
        pval._z =  atof(tokens[2].c_str());
        _points.push_back(pval);
    }
    return true;
}

void CPointCloud::create_glList()
{
     ///std::cout << "CPointCloud::create_glList" << std::endl;
    delete_glLists();

    if(_use_temp_color_scheme)
    {
        create_glList_using_temporal_scheme();
        return;
    }



   //if(_use_viewref && (gReferencePoint != NULL))
   if(_use_common_color && (gReferencePoint != NULL))
        create_glList_using_greference();
   else
        create_glList_ignoring_greference();


}

void CPointCloud::create_glList_using_greference()
{
    // find object center
    float xc = 0;
    float yc = 0;
    float zc = 0;
    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
    {
        xc += it->_x;
        yc += it->_y;
        zc += it->_z;
    }
    xc /= _points.size();
    yc /= _points.size();
    zc /= _points.size();

    // find radius of the object
    float radi_sq = 0;
    it = _points.begin();
    for( ; it != _points.end(); it++)
    {
        float tr = sqrt((it->_x - xc) * (it->_x - xc) +
                        (it->_y - yc) * (it->_y - yc) +
                        (it->_z - zc) * (it->_z - zc) );

        if (tr > radi_sq) radi_sq = tr;
    }

    float shadow_cyl_radi = 0.2;
    radi_sq *= shadow_cyl_radi;

    float cam_dist = sqrt(  (gReferencePoint->_tx - xc) * (gReferencePoint->_tx - xc)  +
                            (gReferencePoint->_tz - zc) * (gReferencePoint->_tz - zc));

    float null_dist = cam_dist - radi_sq;

    // create new display list
    _oglid = glGenLists(1);
    glNewList(_oglid, GL_COMPILE);
    glDisable(GL_LIGHTING);
    glPointSize(CGlobalHSSettings::gpoint_size);

    glBegin(GL_POINTS);
    it = _points.begin();
    for( ; it != _points.end(); it++)
    {
        // selection mode
        if( (it->_selected))
        {
            glColor3f(1, 0, 0);
            glNormal3f (it->_nx, it->_ny, it->_nz);
            glVertex3f(it->_x, it->_y, it->_z);
            continue;
        }

        float dp = sqrt(
                  (gReferencePoint->_tx - it->_x ) * (gReferencePoint->_tx - it->_x  ) +
                  //(gReferencePoint->_ty - it->_y*_ry ) * (gReferencePoint->_ty - it->_y*_ry ) +
                  (gReferencePoint->_tz - it->_z  ) * (gReferencePoint->_tz - it->_z  )
                  );

        float c = (dp - null_dist)/ (2*radi_sq);
        c = 1. - c;
        if( c > 1.) c = 1.;
        if( c < 0.) c = 0.;

        //// post corr
        float cshift = 0.4;
        c = c * (1. - cshift )  + cshift;

        GLfloat red = it->_r;
        GLfloat gre = it->_g;
        GLfloat blu = it->_b;
        if(_use_common_color)
        {
            red = _color[0];
            gre = _color[1];
            blu = _color[2];
        }

        glColor3f(c * red, c * gre, c * blu);
       // glNormal3f (it->_nx, it->_ny, it->_nz );

        //glColor3f(c * 1., c * 1., c * 1.);
        glVertex3f(it->_x, it->_y, it->_z);
    }
    glEnd();

    ///// for debuging
    /*if(_use_common_color)
    {
        glBegin(GL_LINES);
         it = _points.begin();
        for( ; it !=  _points.end(); it++)
        {
           glColor3f(1, 0, 0);
           glVertex3f(it->_x, it->_y, it->_z);
           glColor3f(0, 1, 0);
           glVertex3f(it->_x+.1*it->_nx, it->_y+.1*it->_ny, it->_z+.1*it->_nz);

        }
        glEnd();
    }*/

    // set default size
    glPointSize(1.0);

    glEnable(GL_LIGHTING);
    glEndList();

}

bool  CPointCloud::is_there_selected()
{

    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
    {
        if(it->_selected) return true;
    }
    return false;
}

void CPointCloud::update_section(int x, int y, int w, int h,
                                 std::vector<cv::Point2f> & v, SelectMode sm)
{
    GLdouble projection[16];
    glGetDoublev(GL_PROJECTION_MATRIX, projection);

    GLdouble modelview[16];
    glGetDoublev(GL_MODELVIEW_MATRIX, modelview);

    GLint viewport[4];
    glGetIntegerv( GL_VIEWPORT, viewport );

    GLdouble screen_coords[3];

    double x0 = (x < (x + w))? x : x + w;
    double x1 = (x >= (x + w))? x : x + w;;
    double y0 = (y < (y + h))? y : y + h;
    double y1 = (y >= (y + h))? y : y + h;


    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
    {
        GLdouble px = 0;
        GLdouble py = 0;
        GLdouble pz = 0;
        /// compute projection
        gluProject(it->_x, it->_y, it->_z, modelview, projection, viewport, &px, &py, &pz);

        // if(points  projects into RIO)
        bool is_in_ROI = false;

         if(sm == POLY)
         {
             if(v.size() > 2)
                    is_in_ROI = ((cv::pointPolygonTest(v, cv::Point2f(px,py), false)) > 0);
         }
         else
            is_in_ROI = ((px >= x0) && (px <= x1) && (py >= y0) && (py <= y1));

        switch(sm)
        {
                case DEF:
                if(is_in_ROI) it->_selected = true;
                else it->_selected = false;
                break;

                case ADD:
                case POLY:
                if(is_in_ROI) it->_selected = true;
                break;

                case SUB:
                if(is_in_ROI) it->_selected = false;
                break;
        }
     }
    delete_glLists();
}

void CPointCloud::clear_selection()
{
    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
            it->_selected = false;
    delete_glLists();
}

void CPointCloud::delete_selected_points()
{
    std::vector<C3DPoint> rem_pnts;
    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
            if(!it->_selected )
                rem_pnts.push_back(*it);
    _points.clear();
    _points.insert(_points.begin(), rem_pnts.begin(), rem_pnts.end());
    rem_pnts.clear();
    delete_glLists();

}


void CPointCloud::apply_coloring_scheme()
{

    _use_temp_color_scheme = false;

    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
    {
        float red = changeBrightnessf(it->_r, _tmp_brightness);
        float gre = changeBrightnessf(it->_g, _tmp_brightness);
        float blu = changeBrightnessf(it->_b, _tmp_brightness);

        red = changeContrastf(red, _tmp_contrast);
        gre = changeContrastf(gre, _tmp_contrast);
        blu = changeContrastf(blu, _tmp_contrast);

        red = changeGammaf(red, _tmp_gamma);
        gre = changeGammaf(gre, _tmp_gamma);
        blu = changeGammaf(blu, _tmp_gamma);

        it->_r = red;
        it->_g = gre;
        it->_b = blu;


     }
    delete_glLists();


}

void CPointCloud::create_glList_using_temporal_scheme()
{
    ///std::cout  <<_tmp_brightness << " " <<_tmp_contrast << " " << _tmp_gamma <<  std::endl;

    _oglid = glGenLists(1);
    glNewList(_oglid, GL_COMPILE);
    glDisable(GL_LIGHTING);
    glPointSize(CGlobalHSSettings::gpoint_size);


    glBegin(GL_POINTS);
    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
    {
        float red = changeBrightnessf(it->_r, _tmp_brightness);
        float gre = changeBrightnessf(it->_g, _tmp_brightness);
        float blu = changeBrightnessf(it->_b, _tmp_brightness);

        red = changeContrastf(red, _tmp_contrast);
        gre = changeContrastf(gre, _tmp_contrast);
        blu = changeContrastf(blu, _tmp_contrast);

        red = changeGammaf(red, _tmp_gamma);
        gre = changeGammaf(gre, _tmp_gamma);
        blu = changeGammaf(blu, _tmp_gamma);

       glColor3f(red, gre, blu);
       /// glColor3f(it->_r, it->_g, it->_b);
       glNormal3f (it->_nx, it->_ny, it->_nz);
       glVertex3f(it->_x, it->_y, it->_z);

      //  std::cout << it->_nx << " " <<  it->_ny << " " << it->_nz << std::endl;
    }


    glEnd();
    // set default size
    glPointSize(1.0);

    glEnable(GL_LIGHTING);
    glEndList();
}


void CPointCloud::set_temp_coloring_scheme(float brightness, float contrast, float gamma)
{
    _tmp_brightness = brightness;
    _tmp_contrast = contrast;
    _tmp_gamma = gamma;

    _use_temp_color_scheme = true;
    delete_glLists();

}


void CPointCloud::create_glList_ignoring_greference()
{
    // create new display list
    _oglid = glGenLists(1);
    glNewList(_oglid, GL_COMPILE);


    // TODO remake
    glDisable(GL_LIGHTING);

    glPointSize(CGlobalHSSettings::gpoint_size);

    glBegin(GL_POINTS);
    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
    {
        // selection mode
        if( (it->_selected))
        {
            glColor3f(1, 0, 0);
            glNormal3f (it->_nx, it->_ny, it->_nz);
            glVertex3f(it->_x, it->_y, it->_z);
            continue;
        }

        if(_use_common_color)
            glColor3f(_color[0], _color[1], _color[2]);
        else
            glColor3f(it->_r, it->_g, it->_b);

            glNormal3f (it->_nx, it->_ny, it->_nz);
            glVertex3f(it->_x, it->_y, it->_z);

      //  std::cout << it->_nx << " " <<  it->_ny << " " << it->_nz << std::endl;
    }


    glEnd();

    ///// for debuging
   /* glBegin(GL_LINES);
     it = _points.begin();
    for( ; it !=  _points.end(); it++)
    {
       glColor3f(1, 0, 0);
       glVertex3f(it->_x, it->_y, it->_z);
       glColor3f(0, 1, 0);
       glVertex3f(it->_x+.1*it->_nx, it->_y+.1*it->_ny, it->_z+.1*it->_nz);

    }
    glEnd();*/

    glPointSize(1.0);
    // TODO remake

    glEnable(GL_LIGHTING);
    glEndList();
}

void CPointCloud::rotateX(float cos_alpha, float sin_alpha, float * x, float * y, float * z)
{
    //float x0 = *x;
    float y0 = *y;
    float z0 = *z;

    //*x =  cos_alpha * x0 + sin_alpha * z0;
    *y =  cos_alpha * y0 - sin_alpha * z0;
    *z =  sin_alpha * y0 + cos_alpha * z0;
}

void CPointCloud::rotateY(float cos_alpha, float sin_alpha, float * x, float * y, float * z)
{
    float x0 = *x;
    //float y0 = *y;
    float z0 = *z;

    *x =  cos_alpha * x0 + sin_alpha * z0;
    //*y = ;
    *z = -sin_alpha * x0 + cos_alpha * z0;
}

void CPointCloud::rotateZ(float cos_alpha, float sin_alpha, float * x, float * y, float * z)
{
    float x0 = *x;
    float y0 = *y;
    //float z0 = *z;

    *x = cos_alpha * x0 - sin_alpha * y0;
    *y = sin_alpha * x0 + cos_alpha * y0;
    //*z = -sin_alpha * x0 + cos_alpha * z0;

}

void CPointCloud::rotate(float rx, float ry, float rz)
{
    float sin_rx = sin(rx);
    float cos_rx = cos(rx);

    float sin_ry = sin(ry);
    float cos_ry = cos(ry);

    float sin_rz = sin(rz);
    float cos_rz = cos(rz);

    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
    {
        rotateX(cos_rx, sin_rx, &(it->_x), &(it->_y), &(it->_z));
        rotateZ(cos_rz, sin_rz, &(it->_x), &(it->_y), &(it->_z));
        rotateY(cos_ry, sin_ry, &(it->_x), &(it->_y), &(it->_z));


       // it->_x *=  sx;
       // it->_y *=  sy;
       // it->_z *=  sz;
    }
    delete_glLists();
}

void CPointCloud::scale(float sx, float sy, float sz)
{
    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
    {
        it->_x *=  sx;
        it->_y *=  sy;
        it->_z *=  sz;
    }
    delete_glLists();

}

void CPointCloud::translate(float dx, float dy, float dz)
{
    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
    {
        it->_x += dx; ///_rx);
        it->_y += dy; ///_ry);
        it->_z += dz; ///_rz);
    }
    delete_glLists();
}
