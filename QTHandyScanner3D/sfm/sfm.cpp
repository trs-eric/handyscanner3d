#include "sfm.h"

#include <iostream>
#include <fstream>
#include<QString>
#include <string>
#include <fstream>
#include "graphicutils.h"


#include "../sfmToyLib/OFFeatureMatcher.h"
#include "../sfmToyLib/RichFeatureMatcher.h"
#include "../sfmToyLib/GPUSURFFeatureMatcher.h"
#include "../sfmToyLib/Triangulation.h"
#include "../sfmToyLib/FindCameraMatrices.h"
#include "../sfmToyLib/MultiCameraPnP.h"
#include "../sfmToyLib/SURFFeatureMatcher.h"







// keypoint extractor parameter
double g_min_Hessian = 50;
// this parameter controls maximal allowedn keypoint distance factor
// double g_maximal_distance_fact = 2.0;



void compute_correspondance(cv::Mat & img_1, cv::Mat & img_2,
                            cv::FeatureDetector & detector,
                            cv::DescriptorExtractor & extractor,
                            cv::DescriptorMatcher & matcher,
                            std::vector< cv::DMatch > & good_matches,
                            std::vector<cv::KeyPoint> & keypoints_1,
                            std::vector<cv::KeyPoint> & keypoints_2,
                            std::vector<cv::Point2f> & points1,
                            std::vector<cv::Point2f> & points2)
{


    detector.detect( img_1, keypoints_1 );
    detector.detect( img_2, keypoints_2 );

    cv::Mat descriptors_1, descriptors_2;
    extractor.compute( img_1, keypoints_1, descriptors_1 );
    extractor.compute( img_2, keypoints_2, descriptors_2 );

    int g = 0; // final matches
    // flann
    /*std::vector< cv::DMatch > matches;
    matcher.match( descriptors_1, descriptors_2, matches );
    // compute distance range between keypoints
    double max_dist = 0; double min_dist = 10000;
    for( int i = 0; i < descriptors_1.rows; i++ )
    {
        double dist = matches[i].distance;
        if( dist < min_dist ) min_dist = dist;
        if( dist > max_dist ) max_dist = dist;
    }
    for( int i = 0; i < descriptors_1.rows; i++ )
        if( matches[i].distance <= (g_maximal_distance_fact*min_dist))
        {
            good_matches.push_back( matches[i]);
            points1.push_back(keypoints_1[ matches[i].queryIdx ].pt);
            points2.push_back(keypoints_2[ matches[i].trainIdx ].pt);
            g ++;
       }*/
    // BF+KNN
    std::vector<std::vector<cv::DMatch> > knnMatches;
    matcher.knnMatch(descriptors_1, descriptors_2, knnMatches,2);


    std::vector<cv::DMatch> matches;
    const float minRatio = 1.f / 2.0f;
    for (size_t i=0; i < knnMatches.size(); i++)
    {
        const cv::DMatch bestMatch   = knnMatches[i][0];
        const cv::DMatch betterMatch = knnMatches[i][1];
        float distanceRatio = bestMatch.distance / betterMatch.distance;
        if (distanceRatio < minRatio)
        {
            matches.push_back(bestMatch);
            good_matches.push_back( bestMatch);
            points1.push_back(keypoints_1[ bestMatch.queryIdx ].pt);
            points2.push_back(keypoints_2[ bestMatch.trainIdx ].pt);

            g++;
        }
    }



    std::cout << "compute_correspondance: all " << descriptors_1.rows << ", good " << g << std::endl;

/*
    cv::Mat img_keypoints_1, img_keypoints_2;
    cv::drawKeypoints( img_1, keypoints_1, img_keypoints_1, cv::Scalar::all(-1), cv::DrawMatchesFlags::DEFAULT );
    cv::drawKeypoints( img_2, keypoints_2, img_keypoints_2, cv::Scalar::all(-1), cv::DrawMatchesFlags::DEFAULT );

    cv::imshow("Keypoints 1", img_keypoints_1 );
    cv::imshow("Keypoints 2", img_keypoints_2 );
*/
   cv::Mat img_matches;


    cv::drawMatches( img_1, keypoints_1, img_2, keypoints_2,
                   good_matches, img_matches, cv::Scalar::all(-1), cv::Scalar::all(-1),
                   std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );

    cv::imshow( "Good Matches", img_matches );



}

cv::Mat get_back_rotation_matrix()
{
    cv::Mat rvec = (cv::Mat_<double>(1,3) << 0, BGM(15), 0) ;

    cv::Mat rotM;//(3, 3, CV_64FC1);
    cv::Rodrigues(rvec, rotM);
    return rotM;
}

cv::Mat get_forward_rotation_matrix()
{
    cv::Mat rvec = (cv::Mat_<double>(1,3) << 0, -BGM(15), 0) ;

    cv::Mat rotM;//(3, 3, CV_64FC1);
    cv::Rodrigues(rvec, rotM);
    return rotM;
}


double compute_av_repo_error(cv::Mat & P2,
                             cv::Mat & K,
                             std::vector<cv::KeyPoint> & pt_all_1L,
                             std::vector<cv::KeyPoint> & pt_all_2L,
                             std::vector<cv::DMatch> & matches,
                              cv::Mat& img3d1)
{

    double res_error = 0;
    int counter = 0;
    std::vector<cv::DMatch>::iterator gmit = matches.begin();
    for( ; gmit != matches.end(); gmit++)
    {
        /// get 3d point in pcl1
        cv::Point2f p_1L = pt_all_1L[gmit->queryIdx].pt;
        cv::Point3f  pp = (img3d1.at<cv::Point3f>(p_1L));
        cv::Mat pp_mat = (cv::Mat_<double>(4,1) << pp.x, pp.y, pp.z,1);
        cv::Mat uv = K * P2 * pp_mat;
        uv  /= uv.at<double>(2,0);

        cv::Point2f puv;
        puv.x = uv.at<double>(0,0);
        puv.y = uv.at<double>(1,0);

         cv::Point2f p_2L = pt_all_2L[gmit->trainIdx].pt;
         cv::Mat p_2L_mat = (cv::Mat_<double>(3,1) << p_2L.x, p_2L.y, 1);
         res_error += cv::norm(p_2L_mat - uv) ;
         counter ++;
    }
  // std::cout << "resulting projection error " << res_error/counter << std::endl;
    return res_error/counter;
}

double find_camP2_Tz_iter(int iter_num,
                          double Tx,
                          double Ty,
                          double initTz,
                          cv::Mat & K,
                          std::vector<cv::KeyPoint> & pt_all_1L,
                          std::vector<cv::KeyPoint> & pt_all_2L,
                          std::vector<cv::DMatch> & matches,
                           cv::Mat& img3d1)
{

    double resTz = initTz;
    double factor  = 0.3;


    double resTz_left    = resTz *(1 - factor);
    double resTz_right  = resTz *(1 + factor);

    double globTz = initTz;
    double globErr = 10000;

    for(int i = 0; i < iter_num; i++)
    {

        cv::Mat camT = (cv::Mat_<double>(3,1) << Tx, Ty, resTz);

        cv::Mat forwR = get_forward_rotation_matrix();

        cv::Mat poseT = -forwR * camT;

        cv::Mat P2 = (cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2),  poseT.at<double>(0,0) ,
                                              forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2),   poseT.at<double>(1,0) ,
                                              forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2),   poseT.at<double>(2,0));
        double f_xi = compute_av_repo_error(P2, K, pt_all_1L, pt_all_2L,
                                            matches, img3d1);


        cv::Mat camTd = (cv::Mat_<double>(3,1) << Tx, Ty, resTz_right);

        cv::Mat poseTd = -forwR * camTd;

        cv::Mat P2d = (cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2),  poseTd.at<double>(0,0) ,
                                              forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2),   poseTd.at<double>(1,0) ,
                                              forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2),   poseTd.at<double>(2,0));


        double f_xi_dx = compute_av_repo_error(P2d, K, pt_all_1L, pt_all_2L,
                                               matches, img3d1);

        cv::Mat camTdd = (cv::Mat_<double>(3,1) << Tx, Ty, resTz_left);

        cv::Mat poseTdd = -forwR * camTdd;

        cv::Mat P2dd = (cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2),  poseTdd.at<double>(0,0) ,
                                              forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2),   poseTdd.at<double>(1,0) ,
                                              forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2),   poseTdd.at<double>(2,0));


        double f_xi_ddx = compute_av_repo_error(P2dd, K, pt_all_1L, pt_all_2L,
                                               matches, img3d1);


        if(f_xi_ddx < f_xi_dx)
        {
            resTz_right = resTz;
            resTz       = (resTz_right + resTz_left)/2;
        }
        else
        {
            resTz_left = resTz;
            resTz       = (resTz_right + resTz_left)/2;
        }

        if(f_xi < globErr)
        {
             globTz = resTz;
             globErr = f_xi;

        }
        if(f_xi_dx < globErr)
        {
            globTz  = resTz_right;
            globErr = f_xi_dx;

        }

        if(f_xi_ddx < globErr)
        {
            globTz  = resTz_left;
            globErr = f_xi_ddx;

        }

        //// resTz = resTz - (0.5) * factor * (f_xi_dx - f_xi_ddx)/(f_xi_dx - 2*f_xi + f_xi_ddx);

       // std::cout << i << ": Tz = " << resTz << " " << P2 << " " << P2d << std::endl;


    }

    return globTz;

}


std::vector<C3DPoint> * testing_loop()
{

    //////////////// given matrices

    // stereopair
    /*
        def :
        double q[] =
            {
                1, 0, 0, -cc_newr0.x,
                0, 1, 0, -cc_newr0.y,
                0, 0, 0, fc_new,
                0, 0, 1./_t[idx], <---- incorrect
                (idx == 0 ? cc_newr0.x - cc_newr1.x : cc_newr0.y - cc_newr1.y)/_t[idx]
            };
    */
    cv::Mat Q = (cv::Mat_<double>(4,4) <<  1., 0., 0.,           -2.3172327889654838e+002,
                                           0., 1., 0.,           -2.9450375366210938e+002,
                                           0., 0., 0.,            1.9689409472867962e+003,
                                           0., 0., 5.6433084190423988e-001, 0. );

    cv::Mat K = (cv::Mat_<double>(3,3) <<  1.9788960567438073e+003, 0., 2.3172327889654838e+002,
                                           0., 1.9793919385468250e+003, 2.9463345131205296e+002,
                                           0., 0., 1.);

    cv::Mat Kinv = K.inv();

    cv::Mat P1 = (cv::Mat_<double>(3,4) << 1, 0, 0,  0,
                                          0, 1, 0,   0,
                                          0, 0, 1,   0 );

    /////////////////////////////////////////////////////////////////////
    /////// construct stereo depth
    cv::StereoSGBM sbm;
    sbm.SADWindowSize =  3;
    sbm.numberOfDisparities = 128;
    sbm.preFilterCap = 63;
    sbm.minDisparity = 0;
    sbm.uniquenessRatio = 10;
    sbm.speckleWindowSize = 100;
    sbm.speckleRange = 32;
    sbm.disp12MaxDiff = 1;
    sbm.fullDP = false;
    sbm.P1 =  216;
    sbm.P2 = 864;

    //////////////////////////////////////////////
    std::ofstream logfile;
    logfile.open ("camera_coord.log");

    cv::Mat gT = (cv::Mat_<double>(3,1) << 0,0,0);
    for (int img_idx = 0;  img_idx <= 23; img_idx++)
 {
    std::cout << "=============== Processing " << img_idx << " frame" << std::endl;

    /////////////////////////////////////////////////////////////////////
    //////// load left and right image
    QString im_1L_str  = QString("./sfacerpUf/left_face00%1.png").arg(img_idx, 2, 10, QLatin1Char('0'));
    cv::Mat img_1L = cv::imread(im_1L_str.toStdString() );

    QString im_1R_str  = QString("./sfacerpUf/right_face00%1.png").arg(img_idx, 2, 10, QLatin1Char('0'));
    cv::Mat img_1R = cv::imread(im_1R_str.toStdString() );

    QString im_2L_str  = QString("./sfacerpUf/left_face00%1.png").arg(img_idx+1, 2, 10, QLatin1Char('0'));
    cv::Mat img_2L = cv::imread(im_2L_str.toStdString() );

    QString im_2R_str  = QString("./sfacerpUf/right_face00%1.png").arg(img_idx+1, 2, 10, QLatin1Char('0'));
    cv::Mat img_2R = cv::imread(im_2R_str.toStdString() );



    cv::Mat disp1;
    sbm(img_1L, img_1R, disp1);

    // cv::Mat disp8;
    // cv::normalize(disp, disp8, 0, 255, CV_MINMAX, CV_8U);
    // cv::imshow( "disp8_new", disp8);
    cv::Mat img3d1;
    reprojectImageTo3D(disp1, img3d1, Q);
    /// cv::Point3f pp = img3d.at<cv::Point3f>(y,x);
    /// tp._x = pp.x;
    /// tp._y = pp.y;
    /// double vv =  (Q.at<double>(2,3)/(double(cdisp)*Q.at<double>(3,2)));
    /// tp._z = pp.z;


    cv::Mat disp2;
    sbm(img_2L, img_2R, disp2);
    cv::Mat img3d2;
    reprojectImageTo3D(disp2, img3d2, Q);


    /////// compute interesting points

    // add all images
    std::vector<cv::Mat> imgs;
    imgs.push_back(img_1L);
    imgs.push_back(img_1R);
    imgs.push_back(img_2L);
    imgs.push_back(img_2R);
    int img_idx_1L = 0;
    int img_idx_1R = 1;
    int img_idx_2L = 2;
    int img_idx_2R = 3;



    std::vector<std::vector<cv::KeyPoint> >  imgpts; // for all input images
    cv::Ptr<IFeatureMatcher> feature_matcher = new SURFFeatureMatcher(imgs,imgpts);
    /// cv::Ptr<IFeatureMatcher> feature_matcher = new RichFeatureMatcher(imgs,imgpts);

    std::vector<cv::KeyPoint> pt_all_1L = feature_matcher->GetImagePoints(img_idx_1L);
    std::vector<cv::KeyPoint> pt_all_2L = feature_matcher->GetImagePoints(img_idx_2L);

     // cv::Mat img_keypoints;
    // cv::drawKeypoints( img_1L, pt_all_1L, img_keypoints, cv::Scalar::all(-1), cv::DrawMatchesFlags::DEFAULT );
     // imshow("Keypoints", img_keypoints);

    /////////// compute matches
    std::vector<cv::DMatch> matches_1L_2L;
    feature_matcher->MatchFeatures(img_idx_1L, img_idx_2L, &matches_1L_2L);

    /////////// filter good matches only
    std::vector<cv::KeyPoint> pts_good_1L, pts_good_2L;
    GetFundamentalMat(pt_all_1L, pt_all_2L,
                      pts_good_1L, pts_good_2L,
                      matches_1L_2L
    #ifdef __SFM__DEBUG__
                          , img_1L,
                           img_2L
    #endif
                          );


    ////// filter out matches without depth information
    std::vector<cv::DMatch> matches_1L_2L_good;
    std::vector<cv::DMatch>::iterator mit = matches_1L_2L.begin();
    for( ; mit != matches_1L_2L.end(); mit ++)
    {

       cv::Point2f p_1L = pt_all_1L[mit->queryIdx].pt;
       cv::Point2f p_2L = pt_all_2L[mit->trainIdx].pt;

       int disp_1L = disp1.at<short>(p_1L.y,p_1L.x);
       int disp_2L = disp2.at<short>(p_2L.y,p_2L.x);

       if( (disp_1L > 0) && (disp_2L > 0) )
       {
           // add good match
           matches_1L_2L_good.push_back(*mit);
       }
    }
    std::cout << "encounter very good matches : " << matches_1L_2L_good.size() << std::endl;


    ////  cv::Mat match_img;
   //// cv::drawMatches( img_1L, pt_all_1L, img_2L, pt_all_2L,
     ///             matches_1L_2L_good, match_img, cv::Scalar::all(-1), cv::Scalar::all(-1),
      ///           std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
     ////cv::imshow( "Good Matches", match_img);

    //////// alternative coputation for T2

/*
    {
        cv::Mat R2 = get_forward_rotation_matrix();

        std::vector<cv::DMatch>::iterator gmit = matches_1L_2L_good.begin();
        for( ; gmit != matches_1L_2L_good.end(); gmit ++)
        {
            cv::Point2f p_2L   = pt_all_2L[gmit->trainIdx].pt;
            cv::Mat p_2L_mat = (cv::Mat_<double>(3,1) << p_2L.x, p_2L.y, 1);

            cv::Point2f p_1L = pt_all_1L[gmit->queryIdx].pt;
            cv::Mat p_1L_mat = (cv::Mat_<double>(3,1) << p_1L.x, p_1L.y, 1);

            cv::Mat T2 = Kinv * p_2L_mat - R2 * Kinv * p_1L_mat;

          //  std::cout << " T=" << T2.t() << std::endl;


        }
    }
*/

    /// for the first frame we have to convert keypoints to parallel coordinates



    //// rotate mpcl2

    cv::Mat backR = get_back_rotation_matrix();
    //// std::cout << "backward roatation matrix " << backR << std::endl;

    std::vector<cv::Point2f> proj3d_1L;
    std::vector<cv::Point2f> proj3d_2L;
   // std::vector<cv::Vec3b> proj3d_1L_colors;
   // std::vector<cv::Vec3b> proj3d_2L_colors;

    std::vector<cv::DMatch>::iterator gmit = matches_1L_2L_good.begin();
    std::vector<double> vecTz;
    for( ; gmit != matches_1L_2L_good.end(); gmit ++)
    {
        /// get 3d point in pcl1
        cv::Point2f p_1L = pt_all_1L[gmit->queryIdx].pt;
        cv::Point3f  p3_1L = (img3d1.at<cv::Point3f>(p_1L));

        //std::cout << "1: " <<  p3_1L << std::endl;

        cv::Point2f pp1L;
        pp1L.x = p3_1L.x;
        pp1L.y = p3_1L.y;
        proj3d_1L.push_back(pp1L);

       // proj3d_1L_colors.push_back(img_1L.at<cv::Vec3b>(p_1L));

        /// unrotate 3d point in pcl2
        cv::Point2f p_2L   = pt_all_2L[gmit->trainIdx].pt;
        cv::Point3f p3_2L = (img3d2.at<cv::Point3f>(p_2L)); //p_2L.y,p_2L.x
        cv::Mat p3d_2L = (cv::Mat_<double>(3,1) << p3_2L.x, p3_2L.y, p3_2L.z );
        cv::Mat p3d_2L_rot = backR * p3d_2L;

       // std::cout << "2: " <<  p3d_2L_rot.t()  <<  " dz= " << p3d_2L_rot.at<double>(2,0)- p3_1L.z <<std::endl;

        cv::Point2f pp2L;
        pp2L.x = p3d_2L_rot.at<double>(0,0);
        pp2L.y = p3d_2L_rot.at<double>(1,0);
        proj3d_2L.push_back(pp2L);

       //// std::cout << "dz: " <<  p3_1L.z - p3d_2L_rot.at<double>(2,0) << std::endl;

        double dz = p3_1L.z - p3d_2L_rot.at<double>(2,0);
        vecTz.push_back(dz);

       // proj3d_2L_colors.push_back(img_2L.at<cv::Vec3b>(p_2L));
    }

    std::sort(vecTz.begin(), vecTz.end());
    int vsz = vecTz.size();
    double cutoff_min = double(vsz) * 0.2;
    double cutoff_max = double(vsz) * 0.8;
    double initTz = 0;
    int vcount = 0;
    for(int i = cutoff_min; i <  cutoff_max; i++ )
    {
        initTz += vecTz[i];
        vcount ++;
    }
    initTz /= vcount;

    cv::Mat H = cv::findHomography(proj3d_2L, proj3d_1L, CV_LMEDS  );
    ///std::cout << "homography matrix " << H << std::endl;
    // POSSIBLE PATCH - check both methods and select the best one


    double a = H.at<double>(0,0);
    double b = H.at<double>(0,1);
    double c = H.at<double>(0,2);
    double d = H.at<double>(1,0);
    double e = H.at<double>(1,1);
    double f = H.at<double>(1,2);


    double p = sqrt(a*a + b*b);
    double r = (a*e - b*d)/p;
    double q = (a*d+b*e)/(a*e - b *d);


    std::cout << "translation " << c << " " << f << std::endl;
     std::cout << "scale " << p << " " << r << std::endl;
     std::cout << "shear " << q << std::endl;
     std::cout << "theta " << atan2(b,a) * 180 / 3.14159 << std::endl;
   /// std::cout << "initTz " << initTz << std::endl;

    //////////////// visualize homograhy
    /*cv::Mat homoviz_orig(750, 1024, CV_8UC3);
    std::vector<cv::Point2f>::iterator pit = proj3d_1L.begin();
    std::vector<cv::Point2f>::iterator pit2L = proj3d_2L.begin();

    for(   ; pit != proj3d_1L.end() && pit2L != proj3d_2L.end(); pit2L++, pit++ )
    {
         cv::Vec3b color1 (255,0,0); //proj3d_1L_colors[i];
         cv::Point2f p1 = (*pit) ;
         cv::Mat p3d_mat =  (cv::Mat_<double>(3,1) << p1.x, p1.y, 6);
         cv::Mat uv1 = K * p3d_mat;
         uv1 /= uv1.at<double>(2,0);

         //homoviz_orig.at<cv::Vec3b>(uv1.at<double>(1,0), uv1.at<double>(0,0)) = color1;
         // std::cout << "1: " << uv1.t() << " " << color1 << std::endl;


         cv::Vec3b color2(0,0,255);
         cv::Point2f p2 = (*pit2L) ;
         cv::Mat p3d_mat2 =  (cv::Mat_<double>(3,1) << p2.x, p2.y, 6);
         cv::Mat uv2 = K * p3d_mat2;
         uv2 /= uv2.at<double>(2,0);

         // homoviz_orig.at<cv::Vec3b>(uv2.at<double>(1,0), uv2.at<double>(0,0)) = color2;
         // std::cout << "2: " << uv2.t() << " " << color2 << std::endl;

         cv::Vec3b color3 (0,255,0); // proj3d_2L_colors[i];
         cv::Point2f p3 = (*pit2L) ;
         cv::Mat p3d_mat3 =  (cv::Mat_<double>(3,1) << p3.x, p3.y, 1);
         cv::Mat H_p3d_mat3 =  H * p3d_mat3;
         H_p3d_mat3 /= H_p3d_mat3.at<double>(2,0);
         cv::Mat p3d_mat3_sh =  (cv::Mat_<double>(3,1) << H_p3d_mat3.at<double>(0,0),
                                 H_p3d_mat3.at<double>(1,0), 6);

         cv::Mat uv3 = K *  p3d_mat3_sh;
         uv3 /= uv3.at<double>(2,0);

         // homoviz_orig.at<cv::Vec3b>(uv3.at<double>(1,0), uv3.at<double>(0,0)) = color3;
          //std::cout << "3: " << uv3.t() << " " << color3 << std::endl;
    }
   // cv::imshow( "Homo - orig", homoviz_orig);
*/

    double Tx =  c;
    double Ty =  f;

    double Tz = find_camP2_Tz_iter(5, Tx, Ty,  initTz,
            K,pt_all_1L, pt_all_2L, matches_1L_2L_good, img3d1);


    /// double Tx =  c;
    /// double Ty =  f;
    //// double Tz = -.685; /// find Tz using e.g. RANSAC

    cv::Mat camT = (cv::Mat_<double>(3,1) << Tx, Ty, Tz);

    cv::Mat forwR = get_forward_rotation_matrix();

    cv::Mat poseT = -forwR * camT;

    cv::Mat P2 = (cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2),  poseT.at<double>(0,0) ,
                                          forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2),   poseT.at<double>(1,0) ,
                                          forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2),   poseT.at<double>(2,0));

    ////std::cout << "second camera coord: " << camT.t() << std::endl;
    ////std::cout << "second camera pose: "  << P2 << std::endl;

    /////////////////// visualize camera pose
    cv::Mat poseviz_orig(img_2L);
    cv::RNG rng(12345);
    /*std::vector<cv::DMatch>::iterator*/ gmit = matches_1L_2L_good.begin();

    std::vector<double> vec_res_err;
    for( ; gmit != matches_1L_2L_good.end(); gmit ++)
    {
        /// get 3d point in pcl1
        cv::Point2f p_1L = pt_all_1L[gmit->queryIdx].pt;
        cv::Point3f  pp = (img3d1.at<cv::Point3f>(p_1L));
        cv::Mat pp_mat = (cv::Mat_<double>(4,1) << pp.x, pp.y, pp.z,1);
        cv::Mat uv = K * P2 * pp_mat;
        uv  /= uv.at<double>(2,0);

        cv::Point2f puv;
        puv.x = uv.at<double>(0,0);
        puv.y = uv.at<double>(1,0);


       // cv::Vec3b color(0,255,0);
        //poseviz_orig.at<cv::Vec3b>(uv.at<double>(1,0), uv.at<double>(0,0)) = color;


         cv::Point2f p_2L = pt_all_2L[gmit->trainIdx].pt;
         cv::Mat p_2L_mat = (cv::Mat_<double>(3,1) << p_2L.x, p_2L.y, 1);

       //  std::cout << p_2L_mat.t() << " " << uv.t() << " " << cv::norm(p_2L_mat - uv) << std::endl;

        vec_res_err.push_back(cv::norm(p_2L_mat - uv));

        ////cv::line(poseviz_orig,puv ,p_2L, cv::Scalar(rng.uniform(0,255), rng.uniform(0, 255), rng.uniform(0, 255)), 1 );
    }



            std::sort(vec_res_err.begin(), vec_res_err.end());
            int ersz = vec_res_err.size();
            double ecutoff_min = double(ersz ) * 0.2;
            double ecutoff_max = double(ersz ) * 0.8;
            double res_error = 0;
            int ecount = 0;
            for(int i = ecutoff_min; i <  ecutoff_max; i++ )
            {
                res_error += vec_res_err[i];
                ecount ++;
            }
            res_error /= ecount;


  /// cv::imshow( "Pose estimate", poseviz_orig);

    std::cout << "resulting projection error " << res_error << std::endl;

    // cv::normalize(disp, disp8, 0, 255, CV_MINMAX, CV_8U);

    /// gmit = matches_1L_2L_good.begin();
    /// for( int i = 0; gmit != matches_1L_2L_good.end(); gmit ++, i++)
    /// {
    ///    cv::Point2f p_1L = pt_all_1L[gmit->queryIdx].pt;
    ///    homoviz_orig.at<cv::Vec3b>(p_1L) = img_1L.at<cv::Vec3b>(p_1L);
    /// std::cout << "color "<< img_1L.at<cv::Vec3b>(p_1L) << std::endl;
    /// }
    /// cv::imshow( "Homo - orig", homoviz_orig);

    /// std::vector<Point2f> scene_corners(4);
    /// perspectiveTransform( proj3d_2L, scene_corners, H.inv());





    ///// testing backprojection from Pcloud
    /* std::vector<cv::KeyPoint>::iterator itkp = pt_all_1L.begin();
    for( ;itkp != pt_all_1L.end() ; itkp++)
    {

        cv::Point2f kp = itkp->pt;

        int cdisp = disp.at<short>(kp.y,kp.x);
        if(cdisp > 0 )
        {

            /// testing kp = K * P * X
            /// cv::Point3f pp = img3d.at<cv::Point3f>(kp.y,kp.x);
            /// cv::Mat kp_mat =  (cv::Mat_<double>(3,1) << kp.x, kp.y, 1);
            /// cv::Mat pp_mat = (cv::Mat_<double>(4,1) << pp.x, pp.y, pp.z,1);
            /// cv::Mat uv = K * P1 * pp_mat;
            /// uv.at<double>(0,0) /= uv.at<double>(2,0);
            /// uv.at<double>(1,0) /= uv.at<double>(2,0);
            /// uv.at<double>(2,0) /= uv.at<double>(2,0);
            /// std::cout << kp << " [" << uv.at<double>(0,0) << ", " << uv.at<double>(1,0) << "] "
            ///          << cv::norm(kp_mat-uv) << std::endl;
             // uv.at<double>(0,0) - kp.x << " " <<  uv.at<double>(1,0) - kp.y  << " " <<


            /// testing Kinv * kp = P * X
            cv::Point3f pp = img3d.at<cv::Point3f>(kp.y,kp.x);
            cv::Mat pp_mat = (cv::Mat_<double>(4,1) << pp.x, pp.y, pp.z,1);
            cv::Mat uvK = P1 * pp_mat;
             uvK /= uvK.at<double>(2,0); ///

            cv::Mat kp_mat =  (cv::Mat_<double>(3,1) << kp.x, kp.y, 1);
            cv::Mat kp_matK = Kinv * kp_mat;
            std::cout << "==" << kp_matK.t() << " " << uvK.t() << " " << cv::norm(kp_matK-uvK) << std::endl;

        }
    }*/

    std::cout << "====================== Result " << std::endl;
    std::cout << "new camera position" << std::endl;

    cv::Mat rvec = (cv::Mat_<double>(1,3) << 0, BGM((img_idx)*(15)), 0) ;

    cv::Mat rotM;
    cv::Rodrigues(rvec, rotM);

    cv::Mat dcampos = rotM*camT;
    gT += dcampos ;

    std::cout <<  gT.t() << " with local coord" << camT.t()  << std::endl;

    logfile << gT.t() << "  " << camT.t()  << "  " <<res_error   << std::endl;

    }

    logfile.close();

    return NULL;
}


cv::Mat compute_fundamental_matrix( std::vector<cv::Point2f> & points1,
                                    std::vector<cv::Point2f> & points2)
{
    //create the output fundamental matrix
    return cv::findFundamentalMat(points1,points2,CV_FM_RANSAC,0.5,0.99);
}

cv::Mat get_camera_matrix( )
{
    // estimated during callibration phase
    //return (cv::Mat_<double>(3,3) <<  1.9788960567438073e+003, 0., 240.0,
    //                                  0., 1.9793919385468250e+003, 290.0,
    //                                  0., 0., 1.);

    return (cv::Mat_<double>(3,3) <<      1.9788960567438073e+003, 0., 2.3172327889654838e+002, 0.,
            1.9793919385468250e+003, 2.9463345131205296e+002, 0., 0., 1.);
}

void read_camera_rotation_angles(int idx, double &ax, double &ay, double & az)
{
    std::ifstream infile("./sfacerpU/_rot.txt");
    int fidx;

    while (infile >> fidx >> ax >> ay >> az )
    {
        // process pair (a,b)

        if(fidx==idx)
        {
            //std::cout << "get_rotation_angles " << idx << " " << ay << std::endl;
            ax = BGM(ax);
            ay = BGM(ay);
            az = BGM(az);
            break;
        }
    }
}

cv::Mat get_camera_rotation_matrix(int idx)
{
    double P = 0; //x
    double H = 0; //y
    double B = 0; //z
    read_camera_rotation_angles(idx, P, H, B);

     cv::Mat RotM(3,3,CV_32F);


    /*

R = Ry(H)*Rx(P)*Rz(B)
  = | cos H*cos B+sin H*sin P*sin B  cos B*sin H*sin P-sin B*cos H  cos P*sin H |
    |                   cos P*sin B                    cos B*cos P       -sin P |
    | sin B*cos H*sin P-sin H*cos B  sin H*sin B+cos B*cos H*sin P  cos P*cos H |

    */
     return (cv::Mat_<double>(3,3) <<   cos(H) * cos(B) + sin(H) * sin(P) *sin(B),
                                        cos(B) * sin(H) * sin(P) - sin(B) * cos(H),
                                        cos(P) * sin(H),
                                        cos (P) * sin(B),
                                        cos (B) * cos(P),
                                        - sin(P),
                                        sin (B) * cos (H) * sin (P) - sin (H) * cos (B),
                                        sin (H) * sin (B) + cos (B) * cos (H) * sin(P),
                                        cos (P) * cos (H));


    return RotM;
}

cv::Mat compute_essential_matrix( cv::Mat & fundMat, cv::Mat & camMat)
{
    return camMat.t() * fundMat * camMat;
}

// UNKOMMENT TESTING TRIANGULATION ERROR
bool triangulate_btw_frames(

        int working_view,
        int older_view,
        std::vector<cv::KeyPoint> & imgpts1_good,
        std::vector<cv::KeyPoint> & imgpts2_good,
        cv::Mat  & K,
        cv::Mat  & Kinv,
        cv::Mat & distcoeff,
        cv::Matx34d & P,
        cv::Matx34d & P1,
       std::vector<struct CloudPoint>& new_triangulated,
       std::vector<cv::KeyPoint> & correspImg1Pt,
        ///std::vector<cv::KeyPoint> & pt_set1,
        ///std::vector<cv::KeyPoint> & pt_set2,
        std::vector<cv::DMatch> & matches,
        std::vector<int>& add_to_cloud
        )
{

    std::cout << P << " " << P1 << std::endl;
    double reproj_error = TriangulatePoints(imgpts1_good,  imgpts2_good,
                           K, Kinv, distcoeff,
                          P, P1, new_triangulated, correspImg1Pt);

    std::cout << "triangulation reproj error " << reproj_error << std::endl;

    std::vector<uchar> trig_status;
     if(!TestTriangulation(new_triangulated, P, trig_status) ||
       !TestTriangulation(new_triangulated, P1, trig_status))
    {
        std::cout << " Triangulation did not succeed" << std::endl;
  //      return false;
    }
    //filter out outlier points with high reprojection
    std::vector<double> reprj_errors;
    for(int i=0; i <new_triangulated.size(); i++)
        reprj_errors.push_back(new_triangulated[i].reprojection_error);

    std::sort(reprj_errors.begin(),reprj_errors.end());
    //get the 80% precentile
    //threshold from Snavely07 4.2
    double reprj_err_cutoff = reprj_errors[4 * reprj_errors.size() / 5] * 2.4;

    std::vector<CloudPoint> new_triangulated_filtered;
    std::vector<cv::DMatch> new_matches;
    for(int i=0; i < new_triangulated.size();i++)
    {
        if(trig_status[i] == 0)
            continue; //point was not in front of camera
        if(new_triangulated[i].reprojection_error > 16.0)
            continue; //reject point

        if(new_triangulated[i].reprojection_error < 4.0||
            new_triangulated[i].reprojection_error < reprj_err_cutoff)
        {
            new_triangulated_filtered.push_back(new_triangulated[i]);
            new_matches.push_back(matches[i]);
        }
        else
           continue;
     }
    std::cout << "filtered out " << (new_triangulated.size() - new_triangulated_filtered.size()) << " high-error points" << std::endl;

    //all points filtered out?
    if(new_triangulated_filtered.size() <= 0) return false;

    //use filtered points now
    new_triangulated.clear();
    new_triangulated.insert(new_triangulated.begin(), new_triangulated_filtered.begin(), new_triangulated_filtered.end());
    //use filtered matches
    matches = new_matches;

    //update the matches storage
    ////matches_matrix[std::make_pair(older_view,working_view)] = new_matches; //just to make sure, remove if unneccesary
    ////matches_matrix[std::make_pair(working_view,older_view)] = FlipMatches(new_matches);

    //now, determine which points should be added to the cloud

    add_to_cloud.clear();
    add_to_cloud.resize(new_triangulated.size(),1);

    //scan new triangulated points, if they were already triangulated before - strengthen cloud

    int found_other_views_count = 0;
    //int num_views = imgs.size();

    for (int j = 0; j<new_triangulated.size(); j++)
    {
        new_triangulated[j].imgpt_for_img.resize(5,-1); //!!!!!!! imgs.size()

        //matches[j] corresponds to new_triangulated[j]
        //matches[j].queryIdx = point in <older_view>
        //matches[j].trainIdx = point in <working_view>
        //2D reference to <older_view>
        new_triangulated[j].imgpt_for_img[working_view] = matches[j].trainIdx;
        //2D reference to <working_view>
        new_triangulated[j].imgpt_for_img[older_view] = matches[j].queryIdx;
        bool found_in_other_view = false;
      /*   for (unsigned int view_ = 0; view_ < num_views; view_++) {
            if(view_ != older_view) {
                //Look for points in <view_> that match to points in <working_view>
                std::vector<cv::DMatch> submatches = matches_matrix[std::make_pair(view_,working_view)];
                for (unsigned int ii = 0; ii < submatches.size(); ii++) {
                    if (submatches[ii].trainIdx == matches[j].trainIdx &&
                        !found_in_other_view)
                    {
                        //Point was already found in <view_> - strengthen it in the known cloud, if it exists there

                        //cout << "2d pt " << submatches[ii].queryIdx << " in img " << view_ << " matched 2d pt " << submatches[ii].trainIdx << " in img " << i << endl;
                        for (unsigned int pt3d=0; pt3d<pcloud.size(); pt3d++) {
                            if (pcloud[pt3d].imgpt_for_img[view_] == submatches[ii].queryIdx)
                            {
                                //pcloud[pt3d] - a point that has 2d reference in <view_>

                                //cout << "3d point "<<pt3d<<" in cloud, referenced 2d pt " << submatches[ii].queryIdx << " in view " << view_ << endl;
                                {
                                    pcloud[pt3d].imgpt_for_img[working_view] = matches[j].trainIdx;
                                    pcloud[pt3d].imgpt_for_img[older_view] = matches[j].queryIdx;
                                    found_in_other_view = true;
                                    add_to_cloud[j] = 0;
                                }
                            }
                        }
                    }
                }
            }
        } {
            if (found_in_other_view) {
                found_other_views_count++;
            } else {
                add_to_cloud[j] = 1;
            }
        }*/
    }
    std::cout << found_other_views_count << "/"
              << new_triangulated.size()
              << " points were found in other views, adding "
              << cv::countNonZero(add_to_cloud) << " new\n";
    return true;
}

void compute_initial_triangulation()
{

    bool good_triangulation = true;
    /// TriangulatePointsBetweenViews(m_second_view,m_first_view,new_triangulated,add_to_cloud);
   // if(!good_triangulation || cv::countNonZero(add_to_cloud) < 10) {
        //std::cout << "triangulation failed" << std::endl;
        //goodF = false;
        //Pmats[m_first_view] = 0;
        //Pmats[m_second_view] = 0;
       // m_second_view++;
   // } else {
        //assert(new_triangulated[0].imgpt_for_img.size() > 0);
        //std::cout << "before triangulation: " << pcloud.size();
        //for (unsigned int j=0; j<add_to_cloud.size(); j++) {
        //    if(add_to_cloud[j] == 1) {
        //        pcloud.push_back(new_triangulated[j]);
        //    }
    //    }
        //std::cout << " after " << pcloud.size() << std::endl;
}
/*
find those points which are in the current frame and already exist in the cloud

*/
void find_2D3D_correspondences(
        std::vector< std::vector<cv::DMatch> > & all_matches,
        std::vector<CloudPoint> & pcloud,
        std::vector<cv::Point3f>& ppcloud, // output
        std::vector<cv::Point2f>& imgPoints, // output
        std::vector<cv::KeyPoint> & all_pts_curimg)
{
    ppcloud.clear(); imgPoints.clear();


    std::vector<int> pcloud_status(pcloud.size(),0);
    //for (set<int>::iterator done_view = good_views.begin(); done_view != good_views.end(); ++done_view)

    std::vector< std::vector<cv::DMatch> >::iterator it = all_matches.begin();

    for( int old_view = 0; it != all_matches.end(); it++, old_view++)
    {
        //int old_view = *done_view;
        //check for matches_from_old_to_working between i'th frame and <old_view>'th frame (and thus the current cloud)
        std::vector<cv::DMatch> matches_from_old_to_working = *it;


        std::cout << "frame nr : " << old_view << std::endl;
        for (unsigned int match_from_old_view=0;
             match_from_old_view < matches_from_old_to_working.size();
             match_from_old_view++)
        {
           /// std::cout << "match : " << match_from_old_view << std::endl;

            // the index of the matching point in <old_view>
            int idx_in_old_view = matches_from_old_to_working[match_from_old_view].queryIdx;

            //scan the existing cloud (pcloud) to see if this point from <old_view> exists
            for (unsigned int pcldp=0; pcldp<pcloud.size(); pcldp++)
            {
               /// std::cout << pcldp << std::endl;

                // see if corresponding point was found in this point
                if (idx_in_old_view == pcloud[pcldp].imgpt_for_img[old_view] && // CHECK THIS!!!!!!
                        pcloud_status[pcldp] == 0) //prevent duplicates
                {
                    //3d point in cloud
                    ppcloud.push_back(pcloud[pcldp].pt);
                    //2d point in image i
                    imgPoints.push_back(all_pts_curimg[matches_from_old_to_working[match_from_old_view].trainIdx].pt);

                    pcloud_status[pcldp] = 1;
                    break;
                }
            }
        }
    }
    std::cout << "found " << ppcloud.size() << " 3d-2d point correspondences" << std::endl;
}


bool find_pose_estimation(
    cv::Mat  & K,
    cv::Mat & distcoeff,
    cv::Mat_<double>& rvec, // out
    cv::Mat_<double>& t, // out
    cv::Mat_<double>& R, // out
    std::vector<cv::Point3f> ppcloud, // in
    std::vector<cv::Point2f> imgPoints //in
    )
{
    if( ppcloud.size() <= 7   ||
        imgPoints.size() <= 7 ||
        ppcloud.size() != imgPoints.size())
    {
        //something went wrong aligning 3D to 2D points..
        std::cout << "couldn't find [enough] corresponding cloud points... (only " << ppcloud.size() << ")" <<std::endl;
        return false;
    }

    std::vector<int> inliers;
    //use CPU
    // big problem is that we actually know rvec exactly
    // what we wish is to find a proper t using sovePnPRansac
    double minVal,maxVal; cv::minMaxIdx(imgPoints,&minVal,&maxVal);
    CV_PROFILE("solvePnPRansac",cv::solvePnPRansac(ppcloud, imgPoints, K, distcoeff,
                                      rvec, t, true, 1000, 0.006 * maxVal, 0.25 * (double)(imgPoints.size()),
                                                   inliers,CV_ITERATIVE/* CV_EPNP*/);)
    std::vector<cv::Point2f> projected3D;
    cv::projectPoints(ppcloud, rvec, t, K, distcoeff, projected3D);

    if(inliers.size()==0)
    {
        //get inliers
        for(int i=0; i < projected3D.size(); i++)
        {
            if(norm(projected3D[i]-imgPoints[i]) < 10.0)
                inliers.push_back(i);
        }
    }

    if(inliers.size() < (double)(imgPoints.size())/5.0)
    {
        std::cout << "not enough inliers to consider a good pose ("
                  <<inliers.size() << "/" <<imgPoints.size()<<")" << std::endl;
        return false;
    }

    if(cv::norm(t) > 200.0)
    {
        // this is bad...
        std::cout << "estimated camera movement is too big, skip this camera" << std::endl;
        return false;
    }

    cv::Rodrigues(rvec, R);
    if(!CheckCoherentRotation(R))
    {
        std::cout << "rotation is incoherent. we should try a different base view..." << std::endl;
        return false;
    }

    std::cout << "found t = " << t << "\nR = \n" << R << std::endl;
    return true;
}

std::vector<C3DPoint> * testing_loopkk()
{
    // in circle load images

    std::vector<cv::Mat> imgs;
    std::vector<std::string> imgs_names;
    std::string  imgs_path("./crazyhorse/");
    //std::string  imgs_path("./sfacerpU/");

    int img_beg_idx = 65;
    int img_end_idx = 71;
    for( int idx = img_beg_idx ; idx <= img_end_idx ; idx++)
    {
         QString img_str  = QString("./crazyhorse/P10009%1.jpg").arg(idx, 2, 10, QLatin1Char('0'));
        //QString img_str  = QString("./sfacerpU/left_face00%1.png").arg(idx, 2, 10, QLatin1Char('0'));
        cv::Mat imgc = cv::imread(img_str.toStdString()  );
        imgs.push_back(imgc);
        imgs_names.push_back(img_str.toStdString());
    }

    MultiCameraPnP mcam(imgs, imgs_names, imgs_path);
    mcam.RecoverDepthFromImages();

    std::vector<cv::Point3d>     pcld = mcam.getPointCloud();
    std::vector<cv::Vec3b>    pcldrgb = mcam.getPointCloudRGB() ;


    std::cout << "found " << pcld.size() << " points " << std::endl;

    // call MPnP

    // convert output to my format
    std::vector<C3DPoint> *  final_res = new std::vector<C3DPoint>;

    float gl_scale = 60.0;
    float gl_offx  = 0.0;
    float gl_offy  = 0.0;
    float gl_offz  = -600.0;

    std::vector<cv::Point3d>::iterator it = pcld.begin();
    for(int i = 0 ; it != pcld.end(); it++, i++)
    {
        cv::Point3d pp = *it;

        // check for erroneous coordinates (NaN, Inf, etc.)
        if (pp.x != pp.x || pp.y != pp.y || pp.z != pp.z ||
#ifndef WIN32
            isnan(pp.x) || isnan(pp.y) || isnan(pp.z)
#else
            _isnan(pp.x) || _isnan(pp.y) || _isnan(pp.z)
#endif
            )
        {
            continue;
        }

        cv::Vec3b rgbv(255,0,255);
        if (pcldrgb.size() > i) {
            rgbv = pcldrgb[i];
        }


        C3DPoint p;
        p._x =  -pp.x * gl_scale + gl_offx;
        p._y =  -pp.y * gl_scale + gl_offy;
        p._z =   pp.z * gl_scale + gl_offz;

        /// color this point later
         p._r = float(rgbv.val[2]) / 256.0;
         p._g = float(rgbv.val[1]) / 256.0;
         p._b = float(rgbv.val[0]) / 256.0;

        final_res->push_back(p);
     }


    return final_res;

}

std::vector<C3DPoint> * testing_loop333()
{int img_idx = 1;
    // int img_idx =18;
    ////
    std::cout << "========================================" << std::endl;
    std::cout << "Processing : " << img_idx << " frame" << std::endl;
    //////////////////////////////////////////////////////
    //// Load two sequential images
    //////////////////////////////////////////////////////

    QString im_1_str  = QString("./sfacerpU/left_face00%1.png").arg(img_idx, 2, 10, QLatin1Char('0'));
    cv::Mat img_1_c = cv::imread(im_1_str.toStdString() /*, CV_LOAD_IMAGE_GRAYSCALE*/ );

    QString im_2_str  = QString("./sfacerpU/left_face00%1.png").arg(img_idx+1, 2, 10, QLatin1Char('0'));
    cv::Mat img_2_c = cv::imread(im_2_str.toStdString() /*, CV_LOAD_IMAGE_GRAYSCALE */);

    QString im_3_str  = QString("./sfacerpU/left_face00%1.png").arg(img_idx+2, 2, 10, QLatin1Char('0'));
    cv::Mat img_3_c = cv::imread(im_3_str.toStdString() /*, CV_LOAD_IMAGE_GRAYSCALE */);

    cv::Mat img_1, img_2, img_3;
    cv::cvtColor(img_1_c, img_1, CV_BGR2GRAY);
    cv::cvtColor(img_2_c, img_2, CV_BGR2GRAY);
    cv::cvtColor(img_3_c, img_3, CV_BGR2GRAY);


    //////////////////////////////////////////////////////
    //// Compute correspondance
    //////////////////////////////////////////////////////
    std::vector<cv::Mat> imgs;
    imgs.push_back(img_1);
    imgs.push_back(img_2);
    imgs.push_back(img_3);

    std::vector<std::vector<cv::KeyPoint> >  imgpts; // for all input images
    //cv::Ptr<IFeatureMatcher> feature_matcher = new SURFFeatureMatcher(imgs,imgpts);
    cv::Ptr<IFeatureMatcher> feature_matcher = new RichFeatureMatcher(imgs,imgpts);
   //cv::Ptr<IFeatureMatcher> feature_matcher = new OFFeatureMatcher(false,imgs,imgpts);
   // cv::Ptr<IFeatureMatcher> feature_matcher = feature_matcher = new GPUSURFFeatureMatcher(imgs,imgpts);

    std::vector<cv::KeyPoint> pt_set1_all = feature_matcher->GetImagePoints(0);
    std::vector<cv::KeyPoint> pt_set2_all = feature_matcher->GetImagePoints(1);
    std::vector<cv::KeyPoint> pt_set3_all = feature_matcher->GetImagePoints(2);

    std::vector<cv::DMatch> matches_tmp;
    feature_matcher->MatchFeatures(0,1,&matches_tmp);


    std::vector < std::vector<cv::DMatch> > v_frame3toall_matches;
    std::vector<cv::DMatch> matches_3to1;
    feature_matcher->MatchFeatures(0,2,&matches_3to1);
    std::vector<cv::DMatch> matches_3to2;
    feature_matcher->MatchFeatures(1,2,&matches_3to2); // is it right? or 2 to 1?

    std::vector<cv::KeyPoint> imgpts12_1_good_tmp, imgpts12_2_good_tmp;
    GetFundamentalMat(pt_set2_all, pt_set3_all,
                                            imgpts12_1_good_tmp,
                                           imgpts12_2_good_tmp,
                                           matches_3to2
    #ifdef __SFM__DEBUG__
                          , img_2,
                           img_3
    #endif
                          );
    std::vector<cv::KeyPoint> imgpts02_1_good_tmp, imgpts02_2_good_tmp;
    GetFundamentalMat(pt_set1_all, pt_set3_all,
                                            imgpts02_1_good_tmp,
                                           imgpts02_2_good_tmp,
                                           matches_3to1
    #ifdef __SFM__DEBUG__
                          , img_1,
                           img_3
    #endif
                          );

    v_frame3toall_matches.push_back(matches_3to1);
    v_frame3toall_matches.push_back(matches_3to2);

   /// std::cout << "nnn " << matches_3to1.size() << " " <<matches_3to2.size()  << std::endl;




    //////////////////////////////////////////////////////
    //// Compose aligned matches with good points
    //////////////////////////////////////////////////////

    std::vector<cv::KeyPoint> imgpts1_good, imgpts2_good;
    cv::Mat toyF =  GetFundamentalMat(pt_set1_all,
                           pt_set2_all,
                           imgpts1_good,
                           imgpts2_good,
                           matches_tmp
    #ifdef __SFM__DEBUG__
                          , img_1,
                           img_2
    #endif
                          );
    ///////////////////////////////////////////////////////
    /// Estimate camera pose matrices
    ///////////////////////////////////////////////////////

    // camera matrix and its inversion
    cv::Mat  K = get_camera_matrix();
    cv::Mat  Kinv;
    cv::invert(K, Kinv);
    // distortion coeffitients (fisheye is corrected already)
    cv::Mat distcoeff = cv::Mat::zeros(4,1, CV_64FC1);
    // essential matrix
    cv::Mat E = K.t() * toyF * K;

    if(fabsf(determinant(E)) > 1e-07)
        std::cout << "Attention det(E) != 0 : " << determinant(E) << ", may lead to strong estimation errors" << std::endl;

    // get real rotation matrix
    cv::Mat RM = get_camera_rotation_matrix(1);

    // compute camera translation vector
    cv::Mat cTx = E * RM.t();
    cv::Mat cT (3, 1, CV_64F, cv::Scalar(0));
    double vlen = sqrt(cTx.at<double>(2, 1)* cTx.at<double>(2, 1) + cTx.at<double>(2, 0)*cTx.at<double>(2, 0) + cTx.at<double>(1, 0)*cTx.at<double>(1, 0));
    cT.at<double>(0, 0) =  -cTx.at<double>(2, 1)/vlen;  //-x
    cT.at<double>(0, 1) =  cTx.at<double>(2, 0)/vlen; // y
    cT.at<double>(0, 2) =  -cTx.at<double>(1, 0)/vlen; //-z
    std::cout << "estimated translation t = EM * RM.t(): " << cT << std::endl;
    //
     cv::Matx34d P;
     P = cv::Matx34d(1,	0,	0,	0,
                     0,	1,	0,	0,
                     0,	0,	1,	0);
    //
    cv::Matx34d P1;
    P1 = cv::Matx34d(RM.at<double>(0,0),	RM.at<double>(0,1),	RM.at<double>(0,2),	cT.at<double>(0,0),
                     RM.at<double>(1,0),	RM.at<double>(1,1),	RM.at<double>(1,2),	cT.at<double>(1,0),
                     RM.at<double>(2,0),	RM.at<double>(2,1),	RM.at<double>(2,2),	cT.at<double>(2,0));

    /// testing pose estimates
    std::vector<CloudPoint> pointcloudT;
    std::vector<cv::KeyPoint>  correspImg1Pt;

    double res = TriangulatePoints(imgpts1_good,  imgpts2_good,
                           K, Kinv, distcoeff,
                          P, P1, pointcloudT, correspImg1Pt);


    std::vector<uchar>  status;
    bool isTrigood = TestTriangulation(pointcloudT, P1, status);
    if(!isTrigood)
    {
         cT.at<double>(0, 0) =  -cT.at<double>(0, 0);
         cT.at<double>(0, 1) =  -cT.at<double>(0, 1);
         cT.at<double>(0, 2) =  -cT.at<double>(0, 2);
        P1 = cv::Matx34d(RM.at<double>(0,0),	RM.at<double>(0,1),	RM.at<double>(0,2),	cT.at<double>(0,0),
                         RM.at<double>(1,0),	RM.at<double>(1,1),	RM.at<double>(1,2),	cT.at<double>(1,0),
                         RM.at<double>(2,0),	RM.at<double>(2,1),	RM.at<double>(2,2),	cT.at<double>(2,0));

        pointcloudT.clear(); correspImg1Pt.clear();
        double res = TriangulatePoints(imgpts1_good,  imgpts2_good,
                               K, Kinv, distcoeff,
                              P, P1, pointcloudT, correspImg1Pt);
        std::cout << "construct new triangulation" << std::endl;
        TestTriangulation(pointcloudT, P1, status);

    }
    std::cout << "My estimations of camera poses: " << std::endl;
    std::cout << "P0: " << P << std::endl;
    std::cout << "P1: " << P1 << std::endl;

    std::cout << "1st camera position " <<  -RM.t() * cT << std::endl;

     cv::Vec3d rrvec; Rodrigues(RM,rrvec);
     std::cout << "short RM : " << rrvec << std::endl;
    //cv::Mat Ro;Rodrigues(rvec, Ro);
    //std::cout << "and back Ro : " << Ro<< std::endl;


   /* in most case F is bad -- cannot proceed
    * {
        std::cout << "Alternative computation of the camera P matrix " << std::endl;

        cv::Matx34d P;
        P = cv::Matx34d(1,	0,	0,	0,
                        0,	1,	0,	0,
                        0,	0,	1,	0);
        cv::Matx34d P1;
        P1 = cv::Matx34d(1,	0,	0,	0,
                        0,	1,	0,	0,
                        0,	0,	1,	0);

        std::vector<CloudPoint> pointcloud;

        std::vector<cv::KeyPoint> imgpts1_good, imgpts2_good;
        bool isgood = FindCameraMatrices(K, Kinv, distcoeff,
                                pt_set1_all,
                                pt_set2_all,
                                imgpts1_good,
                                imgpts2_good,
                                P,
                                P1,
                                matches_tmp,
                                pointcloud
        #ifdef __SFM__DEBUG__
                                , img_1,
                                img_2
        #endif
                                );
        if(isgood)
        {

            std::cout << "F matrix is good" << std::endl;
            std::cout << "P0: " << P << std::endl;
            std::cout << "P1: " << P1 << std::endl;
        }
         else
            std::cout << "F matrix is bad" << std::endl;


    }*/




    /////////////////////////////////////////////////////
    ///  Construct initital triangulation
    /////////////////////////////////////////////////////


    std::vector<CloudPoint> pointcloud;
    std::vector<int>  add_to_cloud;

    triangulate_btw_frames(1, 0, imgpts1_good,imgpts2_good,
            K, Kinv, distcoeff, P, P1,
           pointcloud,correspImg1Pt,
                           //pt_set1_all, pt_set2_all,
                           matches_tmp, add_to_cloud);


    /////////////////////////////////////////////////////
    ////// Prepare output for visualization
    /////////////////////////////////////////////////////

    std::vector<C3DPoint> *  final_res = new std::vector<C3DPoint>;


    float gl_scale = 10;//10.0;
    float gl_offx  = 0.0;
    float gl_offy  = 0.0;
    float gl_offz  = -400; //-400.0;

    std::vector<CloudPoint>::iterator it = pointcloud.begin();
    std::vector<cv::KeyPoint>::iterator itc = correspImg1Pt.begin();

    for( ; (it != pointcloud.end()) && (itc != correspImg1Pt.end()); it++, itc++ )
    {
        cv::Point3f pp = it->pt;

        C3DPoint p;
        p._x =  -pp.x * gl_scale + gl_offx;
        p._y =  -pp.y * gl_scale + gl_offy;
        p._z =  pp.z * gl_scale + gl_offz;

        /// color this point later
        cv::Point2f uv = itc->pt;
        cv::Vec3b color = img_1_c.at<cv::Vec3b>(int(uv.y),int(uv.x));
         p._r = 1; //float(color.val[2]) / 256.0;
         p._g = 0; //float(color.val[1]) / 256.0;
         p._b = 0;// float(color.val[0]) / 256.0;

        final_res->push_back(p);

        ///std::cout << pp << " " << uv << " " << color <<  std::endl;
    }
//return final_res;
    ////////////////////////////////////////////////////////////////
    //////////// adding new frame part
    ///

     std::vector<cv::Point3f> ppcloud;
    std::vector<cv::Point2f> imgPoints;

    find_2D3D_correspondences( v_frame3toall_matches,
                               pointcloud, ppcloud, imgPoints, pt_set3_all);


    cv::Mat_<double> t = (cv::Mat_<double>(1,3) << P1(0,3), P1(1,3), P1(2,3));
    cv::Mat_<double> R = (cv::Mat_<double>(3,3) << P1(0,0), P1(0,1), P1(0,2),
                                                   P1(1,0), P1(1,1), P1(1,2),
                                                   P1(2,0), P1(2,1), P1(2,2));
    cv::Mat_<double> rvec(1,3); Rodrigues(RM, rvec);

    bool pose_estimated =   find_pose_estimation(K, distcoeff,
                                                 rvec, t, R, ppcloud,imgPoints);

    if(pose_estimated)
        std::cout << "good pose" << std::endl;
    else
        std::cout << "bad pose" << std::endl;


    //store estimated pose
    cv::Matx34d	P2 = cv::Matx34d (R(0,0),R(0,1),R(0,2),t(0),
                             R(1,0),R(1,1),R(1,2),t(1),
                             R(2,0),R(2,1),R(2,2),t(2));

    std::cout << "P2: " << P2 << std::endl;
    std::cout << "found rvec: " << rvec<< std::endl;

    std::cout << "2nd camera position " <<  -R.t() * t << std::endl;


    std::vector<cv::KeyPoint> imgpts12_1_good, imgpts12_2_good;
    //GetAlignedPointsFromMatch( pt_set2_all, pt_set3_all,
    //                           matches_3to2,
    //                           imgpts12_1_good,imgpts12_2_good);

    /* cv::Mat toyF =*/  GetFundamentalMat(pt_set2_all, pt_set3_all,

                                            imgpts12_1_good,
                                           imgpts12_2_good,
                                           matches_3to2
    #ifdef __SFM__DEBUG__
                          , img_2,
                           img_3
    #endif
                          );

  /* cv::Mat oimg;
    cv::drawMatches( img_2, pt_set2_all, img_3, pt_set3_all,
                   matches_3to2, oimg, cv::Scalar::all(-1), cv::Scalar::all(-1),
                   std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );

    cv::imshow( "Good Matches", oimg);
 */
    std::vector<CloudPoint> new_triangulated;
    std::vector<int> add_to_cloud_new;
    std::vector<cv::KeyPoint> correspImg3Pt;
   bool good_triangulation = triangulate_btw_frames(
    2, 1,
            imgpts12_1_good,
           imgpts12_2_good,
           K, Kinv, distcoeff,
            P1, P2,
           new_triangulated,
           correspImg3Pt,
           // pt_set0,
          //  pt_set2,
            matches_3to2,
           add_to_cloud_new
            );

   std::vector<CloudPoint> add_new_triangulated;
   std::vector<cv::KeyPoint> add_correspImg3Pt;


   for (int j=0; j<add_to_cloud_new.size(); j++)
      if(add_to_cloud_new[j] == 1)
      {
          add_correspImg3Pt.push_back(correspImg3Pt[j]);
          add_new_triangulated.push_back(new_triangulated[j]);
      }




 it = add_new_triangulated.begin();
itc = add_correspImg3Pt.begin();

   for( ; (it != add_new_triangulated.end()) && (itc != add_correspImg3Pt.end()); it++, itc++ )
   {
       cv::Point3f pp = it->pt;

       C3DPoint p;
       p._x =  -pp.x * gl_scale + gl_offx;
       p._y =  -pp.y * gl_scale + gl_offy;
       p._z =  pp.z * gl_scale + gl_offz;

       /// color this point later
       cv::Point2f uv = itc->pt;
       cv::Vec3b color = img_2_c.at<cv::Vec3b>(int(uv.y),int(uv.x));
        p._r = 0.0;//float(color.val[2]) / 256.0;
        p._g = 1.0; //float(color.val[1]) / 256.0;
        p._b = 0.0;//float(color.val[0]) / 256.0;

       final_res->push_back(p);

       ///std::cout << pp << " " << uv << " " << color <<  std::endl;
   }


    return final_res;
}

void testing_loop2()
{


   // cv::SiftFeatureDetector detector(5);//( g_min_Hessian );
   // cv::SurfDescriptorExtractor extractor;
   // cv::SiftDescriptorExtractor extractor;

    cv::SurfFeatureDetector detector( g_min_Hessian );
    cv::SurfDescriptorExtractor extractor;
    cv::BFMatcher matcher;
    //cv::FlannBasedMatcher matcher;

    int img_idx = 1;
    QString im_1_str  = QString("./sfacerpUf/left_face00%1.png").arg(img_idx, 2, 10, QLatin1Char('0'));
    cv::Mat img_1 = cv::imread(im_1_str.toStdString(), CV_LOAD_IMAGE_GRAYSCALE );

    QString im_2_str  = QString("./sfacerpUf/left_face00%1.png").arg(img_idx+1, 2, 10, QLatin1Char('0'));
    cv::Mat img_2 = cv::imread(im_2_str.toStdString(), CV_LOAD_IMAGE_GRAYSCALE );

    /// cv::imshow("dest", img_2);

    // read and convert to gray-scale
    //cv::cvtColor(img1,  img_1, CV_BGR2GRAY);
    //cv::cvtColor(img2, img_2, CV_BGR2GRAY);

    std::vector< cv::DMatch >  good_matches;
    std::vector<cv::KeyPoint> keypoints_1, keypoints_2;
    std::vector<cv::Point2f>  points1, points2;
    std::vector<cv::KeyPoint> fullpts1, fullpts2;
    cv::Mat descriptors_1, descriptors_2;

/*

    std::vector<cv::Mat> imgs;
    imgs.push_back(img_1);
    imgs.push_back(img_2);
    std::vector<std::vector<cv::KeyPoint> >  imgpts;
    cv::Ptr<IFeatureMatcher> feature_matcher = new RichFeatureMatcher(imgs,imgpts);
   //cv::Ptr<IFeatureMatcher> feature_matcher = new OFFeatureMatcher(true,imgs,imgpts);
   // cv::Ptr<IFeatureMatcher> feature_matcher = feature_matcher = new GPUSURFFeatureMatcher(imgs,imgpts);

    std::vector<cv::DMatch> matches_tmp;
    feature_matcher->MatchFeatures(0,1,&matches_tmp);

return;
*/
    compute_correspondance(img_1, img_2,
                           detector, extractor, matcher,
                           good_matches, keypoints_1, keypoints_2,
                           points1, points2);

   cv::Mat FM = compute_fundamental_matrix(points1, points2);
   std::cout << FM << std::endl;

    cv::Mat CM = get_camera_matrix();
    std::cout << CM << std::endl;

    cv::Mat EM = compute_essential_matrix(FM, CM);
    std::cout << "EMorig: " <<  EM << std::endl;

    cv::Mat RM = get_camera_rotation_matrix(img_idx+1);
    std::cout << "real RM" << RM << std::endl;

    //cv::Mat transMat =RM.t() * EM ;
    //std::cout << "my T " << transMat  << std::endl;

    // TODO try to decompose matrix with SVD like in
    // http://stackoverflow.com/questions/14150152/extract-translation-and-rotation-from-fundamental-matrix
    // and compare computed and real rotation matrix (!)

    cv::SVD decomp = cv::SVD(EM,cv::SVD::MODIFY_A);

    //U
    cv::Mat U = decomp.u;

    //S
    cv::Mat S(3, 3, CV_64F, cv::Scalar(0));
    S.at<double>(0, 0) = decomp.w.at<double>(0, 0);
    S.at<double>(1, 1) = decomp.w.at<double>(0, 1);
    S.at<double>(2, 2) = decomp.w.at<double>(0, 2);

    //V
    cv::Mat V = decomp.vt.t(); //Needs to be decomp.vt.t(); (transpose once more)

    //W
    cv::Mat W(3, 3, CV_64F, cv::Scalar(0));
    W.at<double>(0, 1) = -1;
    W.at<double>(1, 0) = 1;
    W.at<double>(2, 2) = 1;

    //Z
    cv::Mat Z(3, 3, CV_64F, cv::Scalar(0));
    Z.at<double>(0, 1) = 1;
    Z.at<double>(1, 0) = -1;

    std::cout << "svd RM: " << std::endl;
    cv::Mat svdRM = U * W * V.t();
    std::cout <<svdRM << std::endl;

    cv::Mat svdt = U.col(2);
    std::cout <<"SVD t: " << svdt << std::endl;

    cv::Mat svdTM = svdRM *V * W.t() * S * V.t()* svdRM.t();
   // std::cout << "SVD TM: " <<  svdTM  << std::endl;

    // std::cout << ">>> Esvd = svdRt * svdTM " << svdTM * svdRM    << std::endl;
   /// std::cout << "Eorig = U * S * Vt" << U * S * V.t() << std::endl;


   // std::cout << "Tx = svdRt * EM " << svdRM.t()  * EM << std::endl;
    //std::cout << "svdTx =  EM * svdRt" << EM * svdRM.t() << std::endl;
    //std::cout << "Tx2 =  EM * oRt  " << EM * RM.t()  << std::endl;

    cv::Mat cTx = EM * RM.t();
    cv::Mat cT (3, 1, CV_64F, cv::Scalar(0));
    double vlen = sqrt(cTx.at<double>(2, 1)* cTx.at<double>(2, 1) + cTx.at<double>(2, 0)*cTx.at<double>(2, 0) + cTx.at<double>(1, 0)*cTx.at<double>(1, 0));
    cT.at<double>(0, 0) =  cTx.at<double>(2, 1)/vlen;  //x
    cT.at<double>(0, 1) = -cTx.at<double>(2, 0)/vlen; //-y
    cT.at<double>(0, 2) =  cTx.at<double>(1, 0)/vlen; //z

    std::cout << "estimated translation t = EM * RM.t(): " << cT << std::endl;


    // Tx
    // 0  -Tz  Ty
    // Tz  0  -Tx
    //-Ty  Tx  0

}


// triangulation code
// http://stackoverflow.com/questions/23784589/up-to-scale-in-3d-triangulation-using-epipolar-geometry
// http://docs.ros.org/hydro/api/libviso2/html/viso__mono_8cpp_source.html

// new links with fundamental and reconstruction
// http://stackoverflow.com/questions/16639106/camera-motion-from-corresponding-images
// http://stackoverflow.com/questions/16607630/how-to-test-the-fundamental-matrix
