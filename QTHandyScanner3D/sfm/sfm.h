#ifndef SFM_H
#define SFM_H


#include <opencv2/features2d/features2d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/contrib/contrib.hpp>
#include <opencv2/nonfree/features2d.hpp>

#include "graphicutils.h"

// for a given two gray-scaled images kompute all keypoints
// and compute correspondance in good_matches-array
void compute_correspondance(cv::Mat & img1, cv::Mat & img2,
                            cv::FeatureDetector & detector,
                            cv::DescriptorExtractor & extractor,
                            cv::DescriptorMatcher & matcher,
                            std::vector< cv::DMatch > & good_matches,
                            std::vector<cv::KeyPoint> & keypoints_1,
                            std::vector<cv::KeyPoint> & keypoints_2,
                            std::vector<cv::Point2f> & points1,
                            std::vector<cv::Point2f> & points2);


std::vector<C3DPoint> *  testing_loop();

#endif // SFM_H
