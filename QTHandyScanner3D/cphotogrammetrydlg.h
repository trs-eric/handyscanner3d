#ifndef CPHOTOGRAMMETRYDLG_H
#define CPHOTOGRAMMETRYDLG_H

#include <QDialog>

namespace Ui {
class CPhotogrammetryDlg;
}

class CPhotogrammetryDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CPhotogrammetryDlg(QWidget *parent = 0);
    ~CPhotogrammetryDlg();


private:
    Ui::CPhotogrammetryDlg *ui;
    float _ccd_width;
    QString _img_dir;

private slots:
    void set_ccd_width(const QString &);
    void onImgBrowseBtn();
    void onCancelBtn();
};

#endif // CPHOTOGRAMMETRYDLG_H
