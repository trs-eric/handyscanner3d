#ifndef CDISPARITYCOMPTHREAD_H
#define CDISPARITYCOMPTHREAD_H

#include <QThread>
#include <QMutex>

#include "disparity/disparity_utils.h"
#include "graphicutils.h"

class CPointCloud;

class CDisparityCompThread : public QThread
{
    Q_OBJECT
public:
    explicit CDisparityCompThread(QObject *parent = 0);

    ~CDisparityCompThread();

    void start_process(QImage * left_im, QImage * right_im, int lbias,
                       double thresh_border, int num_disp, double thresColor,
                       double thresGrad, double gamma, int r, double eps,
                       double gamma_c, double gamma_d, int r_median, bool postproc);

    void start_process(QImage * camimg, bool usecol,
                       double disp_cutoff, int decim_step, bool smdisp);

    bool is_abort(){return _abort;}
    void emit_send_back(int i);
signals:
    void send_back(const int val);
    void send_result(std::vector<C3DPoint> * cpoints);


protected:
    void run();

    void run_old();

    CArray1D<double> *  fillPixelsReference(CArray1D<double3> * Il,
                                           CArray1D<double> * final_labels, int xs, int ys);

    CArray1D<double> *  weightedMedian(CArray1D<double3> * Il,
                                       CArray1D<double> * disp_img, int winsize, double gamma_c, double gamma_p, int xs, int ys);

public slots:
    void stop_process();

private:
    bool    _abort;

    QMutex  _mutex;

    QImage * _camera_image;
    bool _usecol;
    double _disp_cutoff;
    int _decim_step;
    bool _smooth_disp;

    QImage * _left_im;
    QImage * _right_im;
    int _lbias;
    double _thresh_border;
    int _num_disp;
    double _thresColor;
    double _thresGrad;
    double _gamma;
    int _r;
    double _eps;
    double _gamma_c;
    double _gamma_d;
    int _r_median;
    bool _postproc;
};

#endif // CDISPARITYCOMPTHREAD_H
