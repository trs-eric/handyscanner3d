#include "cdisparitycompclassicthread.h"
#include "carray2d.h"
#include "sfm/sfm.h"

#include <ctime>

//#include "./sfmToyLib/OFFeatureMatcher.h"
#include "./sfmToyLib/RichFeatureMatcher.h"
//#include "./sfmToyLib/GPUSURFFeatureMatcher.h"
//#include "./sfmToyLib/Triangulation.h"
#include "./sfmToyLib/FindCameraMatrices.h"
//#include "./sfmToyLib/MultiCameraPnP.h"
#include "./sfmToyLib/SURFFeatureMatcher.h"

CDisparityCompClassicThread::CDisparityCompClassicThread(QObject *parent) :
    QThread(parent),
    _left_im(NULL), _right_im(NULL), _num_disp(0),
    _lbias(0),_SADWindowSize(0),  _preFilterCap(0),
    _uniquenessRatio(0), _speckleWindowSize(0),
    _speckleRange(0), _disp12MaxDiff(0),
    _fullDP(false), _P1(0), _P2(0)
{
    _abort = false;
}

CDisparityCompClassicThread::~CDisparityCompClassicThread()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
    wait();
}

void CDisparityCompClassicThread::start_process(QImage * left_im, QImage * right_im, int num_disp, int lbias,
                   int SADWindowSize, int preFilterCap,
                   int uniquenessRatio, int speckleWindowSize,
                   int speckleRange , int disp12MaxDiff ,
                   bool fullDP, int P1, int P2 )
{
    _abort = false;
     _left_im =  left_im;
     _right_im = right_im;
     _num_disp = num_disp;
     _lbias = lbias;
     _SADWindowSize = SADWindowSize;
     _preFilterCap = preFilterCap;
     _uniquenessRatio = uniquenessRatio;
     _speckleWindowSize = speckleWindowSize;
     _speckleRange = speckleRange;
     _disp12MaxDiff = disp12MaxDiff;
     _fullDP = fullDP;
     _P1 = P1;
     _P2 = P2;

     start();
}


void CDisparityCompClassicThread::stop_process()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
}

void CDisparityCompClassicThread::emit_send_back(int i)
{
     emit send_back(i);
}

cv::Mat QImage2CVMat(QImage * img)
{
    int xs = img->width();
    int ys = img->height();
    cv::Mat tmp(ys, xs, CV_8UC4, (uchar*)img->bits(),img->bytesPerLine());
    cv::Mat res = cv::Mat(tmp.rows, tmp.cols, CV_8UC3 );
    int from_to[] = { 0,0,  1,1,  2,2 };
    cv::mixChannels( &tmp, 1, &res, 1, from_to, 3 );
    return res;
}

void CDisparityCompClassicThread::clean_dispmap_sides(int xs, int ys, cv::Mat & disp)
{
    int xs_cut = 40;
    cv::MatIterator_<short> it = disp.begin<short>();
    for(int idx =0;it!=disp.end<short>(); it++, idx++)
    {
        int x = idx % xs;
        int y = idx / xs;

       if(x < xs_cut )
            *it = -16; // min val actally

    }
}

void get_rotation_angles(int idx, float &ax, float &ay, float & az)
{
    std::ifstream infile("./sfacerpUf/rot.txt");
    int fidx;

    while (infile >> fidx >> ax >> ay >> az )
    {
        // process pair (a,b)

        if(fidx==idx)
        {
            std::cout << "get_rotation_angles " << idx << " " << ay << std::endl;
            ax = BGM(ax);
            ay = BGM(ay);
            az = BGM(az);
            break;
        }
    }
}

CArray2D<float> contsruct_rotation_matrix(float &ax, float &ay, float & az)
{

    CArray2D<float> M(3,3,0);

    float H = -ay;
    float P = -ax;
    float B = -az;
    /*

R = Ry(H)*Rx(P)*Rz(B)
  = | cos H*cos B+sin H*sin P*sin B  cos B*sin H*sin P-sin B*cos H  cos P*sin H |
    |                   cos P*sin B                    cos B*cos P       -sin P |
    | sin B*cos H*sin P-sin H*cos B  sin H*sin B+cos B*cos H*sin P  cos P*cos H |

    */
    M._data[0][0] = cos(H) * cos(B) + sin(H) * sin(P) *sin(B);
    M._data[0][1] = cos(B) * sin(H) * sin(P) - sin(B) * cos(H);
    M._data[0][2] = cos(P) * sin(H);

    M._data[1][0] = cos (P) * sin(B);
    M._data[1][1] = cos (B) * cos(P);
    M._data[1][2] =  - sin(P);

    M._data[2][0] = sin (B) * cos (H) * sin (P) - sin (H) * cos (B);
    M._data[2][1] = sin (H) * sin (B) + cos (B) * cos (H) * sin(P);
    M._data[2][2] = cos (P) * cos (H);

   std::cout << "CRM: " << ax << " " << ay << " " << az << " "<< std::endl;
   return M;
}

std::vector<C3DPoint> * CDisparityCompClassicThread::compute_pcloud(cv::StereoSGBM & sbm,
                                                 cv::Mat & Q,
                                                 QImage * left_im,
                                                 QImage * right_im,
                                                 CArray2D<int> & left_map0,
                                                 cv::Mat & disp,
                                                 int idx)
{
    /*
    cv::StereoSGBM sbm;
    sbm.SADWindowSize =  _SADWindowSize;
    sbm.numberOfDisparities = _num_disp;
    sbm.preFilterCap = _preFilterCap;
    sbm.minDisparity = _lbias;
    sbm.uniquenessRatio = _uniquenessRatio;
    sbm.speckleWindowSize = _speckleWindowSize;
    sbm.speckleRange = _speckleRange;
    sbm.disp12MaxDiff = _disp12MaxDiff;
    sbm.fullDP = _fullDP;
    sbm.P1 =  _P1;
    sbm.P2 = _P2;
*/
    int xs = left_im->width();
    int ys = left_im->height();




    cv::Mat gleft, gright;
    cv::Mat imgleft  = QImage2CVMat(left_im);
    cv::Mat imgright = QImage2CVMat(right_im);
    cv::cvtColor(imgleft,  gleft, CV_BGR2GRAY);
    cv::cvtColor(imgright, gright, CV_BGR2GRAY);

    //  cv::waitKey(0);

    // compute disparity map
    sbm(gleft, gright, disp);

   // cv::Mat disp8;
   // cv::normalize(disp, disp8, 0, 255, CV_MINMAX, CV_8U);
   // cv::imshow( "disp8", disp8);

    //clean_dispmap_sides(xs,ys,disp);

/*
     std::ofstream dispf;
      dispf.open ("disp.raw", std::ios::out| std::ios::binary);
      for (int j = 0; j < ys; j++)
          for (int i = 0; i < xs; i++)
          {
              float val = (float)disp.at<short>(j,i);
                dispf.write((char*)&val,  sizeof(float));
          }
      dispf.close();
*/

    // convert disparity map to point cloud
    cv::Mat img3d;

    //cv::Mat dispf;
    //disp.convertTo(dispf, CV_32FC1);

    reprojectImageTo3D(disp, img3d, Q);


   // cv::Mat img3d8;
   // cv::normalize(img3d, img3d8, 0, 255, CV_MINMAX, CV_8U);

   /*std::ofstream im3df;
     im3df.open ("im3d.raw", std::ios::out| std::ios::binary);

     for (int j = 0; j < ys; j++)
         for (int i = 0; i < xs; i++)
         {
             cv::Point3f pp = img3d.at<cv::Point3f>(j,i);

             //std::cout << pp.z << " ";

             im3df.write((char*)&pp.z,  sizeof(float));
         }

    im3df.close();
 */


    float ax = 0;
    float ay = 0;
    float az = 0;
    get_rotation_angles(idx, ax, ay, az);
    CArray2D<float> M = contsruct_rotation_matrix(ax, ay, az);


    float gl_scale = 120.0;
    float gl_offx  = -50.0;
    float gl_offy  =   0.0;
    float gl_offz  = 300.0;


    /////////////////&&&&&&&&&&&&&&&&&//////////////////////////
    ///// IMPORTANT PARAMETER
    ////////////////////////////////////////////////////////////
    int ignore_disp_offset = 0; // 70
    std::vector<C3DPoint> *  final_res = new std::vector<C3DPoint>;
    //std::cout << M._data[0][0]  << " " << M._data[0][1] << " " << M._data[0][2]<< std::endl;
    //std::cout << M._data[1][0]  << " " << M._data[1][1] << " " << M._data[1][2]<< std::endl;
    //std::cout << M._data[2][0]  << " " << M._data[2][1] << " " << M._data[2][2]<< std::endl;


    for (int y = 0; y < ys; y++)
    for (int x = 0; x < xs; x++)
    {


        int cdisp = disp.at<short>(y,x);
        if((cdisp > ( 16* (_lbias + ignore_disp_offset) )) && (cdisp < 16*(_lbias  + _num_disp)) )
        {

            cv::Point3f pp = img3d.at<cv::Point3f>(y,x);

            C3DPoint tp;
            tp._x = pp.x;
            tp._y = pp.y;
             double vv =  (Q.at<double>(2,3)/(double(cdisp)*Q.at<double>(3,2)));
            tp._z = pp.z;

            C3DPoint p;
            p._x = M._data[0][0] * tp._x + M._data[0][1] * tp._y + M._data[0][2] * tp._z;//tp._x;//
            p._y = M._data[1][0] * tp._x + M._data[1][1] * tp._y + M._data[1][2] * tp._z;// //tp._y;//
            p._z = M._data[2][0] * tp._x + M._data[2][1] * tp._y + M._data[2][2] * tp._z;////tp._z;//


            p._x =  p._x * gl_scale + gl_offx;
            p._y = -p._y * gl_scale + gl_offy;
            p._z = -p._z * gl_scale + gl_offz;

           // float diff = pp.z - Q.at<float>(2,3)/(int(cdisp)*Q.at<float>(3,2));
            //    std::cout << pp.z << " " << "(" <<  diff << ", " << cdisp << ") ";
            QRgb c = left_im->pixel(x,y);
            p._r = (float)qRed(c)/255.0f;
            p._g = (float)qGreen(c)/255.0f;
            p._b = (float)qBlue(c)/255.0f;

            p._nx = 0;
            p._ny = 0;
            p._nz = -1;

            left_map0._data[y][x] = final_res->size();
            final_res->push_back(p);
        }


    }

    return final_res;

}

void CDisparityCompClassicThread::compute_correspondance
(cv::SurfFeatureDetector & detector,
 cv::SurfDescriptorExtractor & extractor,
 cv::FlannBasedMatcher & matcher, QImage * left_im_0,
                                                 QImage * left_im_1,
   std::vector< cv::DMatch > & good_matches,
 std::vector<cv::KeyPoint> & keypoints_1,
 std::vector<cv::KeyPoint> & keypoints_2)
{
    cv::Mat img1  = QImage2CVMat(left_im_0);
    cv::Mat img2 = QImage2CVMat(left_im_1);

    cv::Mat img_1, img_2;
    cv::cvtColor(img1,  img_1, CV_BGR2GRAY);
    cv::cvtColor(img2, img_2, CV_BGR2GRAY);

    detector.detect( img_1, keypoints_1 );
    detector.detect( img_2, keypoints_2 );

    cv::Mat descriptors_1, descriptors_2;
    extractor.compute( img_1, keypoints_1, descriptors_1 );
    extractor.compute( img_2, keypoints_2, descriptors_2 );

    std::vector< cv::DMatch > matches;
    matcher.match( descriptors_1, descriptors_2, matches );

    double max_dist = 0; double min_dist = 100;
    for( int i = 0; i < descriptors_1.rows; i++ )
    {
        double dist = matches[i].distance;
        if( dist < min_dist ) min_dist = dist;
        if( dist > max_dist ) max_dist = dist;
    }

   // std::cout << "mindist maxdist " <<  min_dist  << " " << max_dist << std::endl;

    for( int i = 0; i < descriptors_1.rows; i++ )
    {
        if( matches[i].distance <= (5*min_dist)/* std::max(2*min_dist, 0.1)*/ )
        {
              good_matches.push_back( matches[i]);
            //cv::Point2f p0 = keypoints_1[ matches[i].queryIdx ].pt;
            ///cv::Point2f p1  = keypoints_2[ matches[i].trainIdx ].pt;
            /// std::cout << "kpt " << p0.x << " " << p0.y << " " << p1.x << " " << p1.y << std::endl;
       }
    }

/*

    cv::Mat img_matches;
    cv::drawMatches( img_1, keypoints_1, img_2, keypoints_2,
                   good_matches, img_matches, cv::Scalar::all(-1), cv::Scalar::all(-1),
                   std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );

    cv::imshow( "Good Matches", img_matches );
    cv::waitKey(0);*/

}


void extract_2dpoint_coord(cv::Point2f & p0, int& p0x, int& p0y,
                           cv::Point2f & p1, int& p1x, int& p1y, int xs, int ys)
{

    p0x = p0.x+0.5;
    p0x = std::max(0, p0x);
    p0x = std::min(xs-1, p0x);

    p0y = p0.y+0.5;
    p0y = std::max(0, p0y);
    p0y = std::min(ys-1, p0y);


    p1x = p1.x+0.5;
    p1x = std::max(0, p1x);
    p1x = std::min(xs-1, p1x);

    p1y = p1.y+0.5;
    p1y = std::max(0, p1y);
    p1y = std::min(ys-1, p1y);

}


double gdx = 0;
double gdy = 0;
double gdz = 0;

void CDisparityCompClassicThread::fix_affine_transform2(CArray2D<int> & left_map0,
                                                       CArray2D<int> & left_map1,
                                                       std::vector<C3DPoint> *  final_res_0,
                                                       std::vector<C3DPoint> *  final_res_1,
                                                       std::vector< cv::DMatch > & good_matches,
                                                       std::vector<cv::KeyPoint> & keypoints_1,
                                                       std::vector<cv::KeyPoint> & keypoints_2,
                                                       cv::Mat & disp0,
                                                       cv::Mat & disp1)
{
 /*   cv::Mat disp8;
    cv::normalize(disp0, disp8, 0, 255, CV_MINMAX, CV_8U);

  std::ofstream dispf;
    dispf.open ("disp.raw", std::ios::out| std::ios::binary);

    cv::MatIterator_<short> itt = disp0.begin<short>();
    for(;itt!=disp0.end<short>(); itt++)
    {
        double val = double(*itt);
        dispf.write((char*)&val,  sizeof(double));
    }
    dispf.close();


    cv::imshow( "dsp", disp8);
   //cv::waitKey(0);
*/
    double dx =0;
    double dy =0;
    double dz =0;
    int count = 0;

    double ddx = 0;
    double ddy = 0;
    double ddz = 0;
    double R3err = 10e+300;

    for( int i = 0; i < (int)good_matches.size(); i++ )
    {

        cv::Point2f p0 =  keypoints_1[ good_matches[i].queryIdx ].pt;
        cv::Point2f p1  = keypoints_2[ good_matches[i].trainIdx ].pt;
        int p0x, p0y, p1x, p1y;
        extract_2dpoint_coord(p0, p0x, p0y, p1, p1x, p1y, left_map0._xs, left_map0._ys);

        int idx_0 = left_map0._data[p0y][p0x];
        int idx_1 = left_map1._data[p1y][p1x];

         //std::cout << p0x<< " " << p0y  <<  idx_0 << " " << idx_1<< std::endl;

        // if disparity map is valid
         if((idx_0 != -1) && (idx_1 != -1)  )
         {



             ddx = (*final_res_1)[idx_1]._x - (*final_res_0)[idx_0]._x ;
             ddy = (*final_res_1)[idx_1]._y - (*final_res_0)[idx_0]._y ;
             ddz = (*final_res_1)[idx_1]._z - (*final_res_0)[idx_0]._z ;



             ///dx += ddx;
             ///dy += ddy;
             ///dz += ddz;

             // std::cout <<  (*final_res_1)[left_map1._data[p1y][p1x]]._x << " " << (*final_res_1)[left_map1._data[p1y][p1x]]._y << " " <<  (*final_res_1)[left_map1._data[p1y][p1x]]._z << " ";
              //std::cout <<  (*final_res_0)[left_map0._data[p0y][p0x]]._x << " " <<  (*final_res_0)[left_map0._data[p0y][p0x]]._y << " " << (*final_res_0)[left_map0._data[p0y][p0x]]._z<< std::endl;

              //std::cout << "coord " << p0x << " " << p0y << " " << p1x << " " <<p1y << std::endl;
              //std::cout << 255*(*final_res_1)[left_map1._data[p1y][p1x]]._r << " " << 255*(*final_res_1)[left_map1._data[p1y][p1x]]._g << " " << 255*(*final_res_1)[left_map1._data[p1y][p1x]]._b << " ";
              //std::cout << 255*(*final_res_0)[left_map0._data[p0y][p0x]]._r << " " << 255*(*final_res_0)[left_map0._data[p0y][p0x]]._g << " " << 255*(*final_res_0)[left_map0._data[p0y][p0x]]._b<< std::endl;

              //std::cout << "disp " <<  disp0.at<short>(p0y,p0x) << " " << disp1.at<short>( p1y, p1x) << std::endl;
              //std::cout <<  ddx << " " << ddy << " " << ddz << " " << std::sqrt(ddx*ddx + ddy*ddy + ddz*ddz) << std::endl;
              //std::cout <<  "-----" << std::endl;

             // (*final_res_1)[idx_1]._y =0;

            // (*final_res_1)[idx_1]._r =1;

            // (*final_res_1)[idx_1]._g =0;

             //(*final_res_1)[idx_1]._b =0;


             double hyp_dx = ddx;
             double hyp_dy = ddy;
             double hyp_dz = ddz;

             double cR3err =  0;

             int ccount = 0;
             // check best match
             int gmsz = good_matches.size();
             for( int j = 0; j < gmsz; j++ )

             {
                 cv::Point2f hp0 =  keypoints_1[ good_matches[j].queryIdx ].pt;
                 cv::Point2f hp1  = keypoints_2[ good_matches[j].trainIdx ].pt;
                 int hp0x, hp0y, hp1x, hp1y;
                 extract_2dpoint_coord(hp0, hp0x, hp0y, hp1, hp1x, hp1y,
                                       left_map0._xs, left_map0._ys);

                 int hidx_0 = left_map0._data[hp0y][hp0x];
                 int hidx_1 = left_map1._data[hp1y][hp1x];

                 if((hidx_0 != -1) && (hidx_1 != -1)  )
                 {

                     double hdx = (*final_res_1)[hidx_1]._x - hyp_dx - (*final_res_0)[hidx_0]._x ;
                     double hdy = (*final_res_1)[hidx_1]._y - hyp_dy - (*final_res_0)[hidx_0]._y ;
                     double hdz = (*final_res_1)[hidx_1]._z - hyp_dz - (*final_res_0)[hidx_0]._z ;


                     cR3err += sqrt(hdx * hdx + hdy * hdy + hdz * hdz); //*abs(disp1.at<short>(hp1y,hp1x) -  disp0.at<short>(hp0y,hp0x))*disp1.at<short>(hp1y,hp1x)
                     ccount++;
                 }

             };

            // std::cout << "cerr "  <<  cR3err << std::endl;
             if(cR3err/ccount < R3err)
             {
                 dx = hyp_dx;
                 dy = hyp_dy;
                 dz = hyp_dz;
                 R3err = cR3err/ccount;
                 count = ccount;
             }



             ////count ++;
         }

    }

    std::cout << "glob x,y,z " << gdx << " " << gdy << " " << gdz << std::endl;

    gdx += dx;
    gdy += dy;
    gdz += dz;

    //if (count > 0)
    {
        //dx /= double(count);
        //dy /= double(count);
        //dz /= double(count);
        std::cout << "||resof " << dx  << " " << dy << " " << dz << " " << (R3err) << " " << count<< std::endl;
    }

    std::vector<C3DPoint>::iterator it = final_res_1->begin();
    for( ; it != final_res_1->end(); it++)
    {
            it->_x -= dx;
            it->_y -= dy;
            it->_z -= dz;


         // it->_z = 0;
        // it->_r = 1.0;
        // it->_g = 0.0;
        // it->_b = 0.0;

    }



    //std::cout << "end of correction " << std::endl;


}


// pseudo Ransac version
void CDisparityCompClassicThread::fix_affine_transform(CArray2D<int> & left_map0,
                                                       CArray2D<int> & left_map1,
                                                       std::vector<C3DPoint> *  final_res_0,
                                                       std::vector<C3DPoint> *  final_res_1,
                                                       std::vector< cv::DMatch > & good_matches,
                                                       std::vector<cv::KeyPoint> & keypoints_1,
                                                       std::vector<cv::KeyPoint> & keypoints_2,
                                                       cv::Mat & disp0,
                                                       cv::Mat & disp1)
{
    double inliers_count = 0.5; // 50%
    double dist_thres = 10e+300;

    int gmsz = good_matches.size();

    //// determine R^3 threshold
    for( int i = 0; i < gmsz; i++ )
    {
        std::vector<double> dist_hist;

        cv::Point2f p0 =  keypoints_1[ good_matches[i].queryIdx ].pt;
        cv::Point2f p1  = keypoints_2[ good_matches[i].trainIdx ].pt;
        int p0x, p0y, p1x, p1y;
        extract_2dpoint_coord(p0, p0x, p0y, p1, p1x, p1y, left_map0._xs, left_map0._ys);

        int idx_0 = left_map0._data[p0y][p0x];
        int idx_1 = left_map1._data[p1y][p1x];

        if( (idx_0 != -1) && (idx_1 != -1)  )
        {
             double ddx = (*final_res_1)[idx_1]._x - (*final_res_0)[idx_0]._x ;
             double ddy = (*final_res_1)[idx_1]._y - (*final_res_0)[idx_0]._y ;
             double ddz = (*final_res_1)[idx_1]._z - (*final_res_0)[idx_0]._z ;

             for( int j = 0; j < gmsz; j++ )
             {
                 cv::Point2f hp0 =  keypoints_1[ good_matches[j].queryIdx ].pt;
                 cv::Point2f hp1  = keypoints_2[ good_matches[j].trainIdx ].pt;
                 int hp0x, hp0y, hp1x, hp1y;
                 extract_2dpoint_coord(hp0, hp0x, hp0y, hp1, hp1x, hp1y,
                                       left_map0._xs, left_map0._ys);

                 int hidx_0 = left_map0._data[hp0y][hp0x];
                 int hidx_1 = left_map1._data[hp1y][hp1x];

                 if((hidx_0 != -1) && (hidx_1 != -1)  )
                 {
                     double hdx = (*final_res_1)[hidx_1]._x - ddx - (*final_res_0)[hidx_0]._x ;
                     double hdy = (*final_res_1)[hidx_1]._y - ddy - (*final_res_0)[hidx_0]._y ;
                     double hdz = (*final_res_1)[hidx_1]._z - ddz - (*final_res_0)[hidx_0]._z ;
                     double euc = sqrt(hdx * hdx + hdy * hdy + hdz * hdz);

                     dist_hist.push_back(euc);
                 }
             }
         }

         if(dist_hist.size() > 3) // should be long enough
         {

             std::sort(dist_hist.begin(),dist_hist.end());
             double tpos = double(dist_hist.size())*inliers_count;
             double cur_dist_thres = dist_hist[(int)tpos];

             //for( std::vector<double>::iterator it = dist_hist.begin(); it != dist_hist.end(); it++)
             //    std::cout << *it << " ";

             if(dist_thres > cur_dist_thres)
                 dist_thres = cur_dist_thres;

         }
         ////std::cout<<std::endl  << "dist_hist.size() " << dist_hist.size() << " " << dist_thres <<std::endl;

    }

    //// start pseudo-RANSAC

    int glob_inliers = 0;
    double dx =0;
    double dy =0;
    double dz =0;

    for( int i = 0; i <  gmsz; i++ )
    {

        cv::Point2f p0 =  keypoints_1[ good_matches[i].queryIdx ].pt;
        cv::Point2f p1  = keypoints_2[ good_matches[i].trainIdx ].pt;
        int p0x, p0y, p1x, p1y;
        extract_2dpoint_coord(p0, p0x, p0y, p1, p1x, p1y, left_map0._xs, left_map0._ys);

        int idx_0 = left_map0._data[p0y][p0x];
        int idx_1 = left_map1._data[p1y][p1x];

        int loc_inliers = 0;
        double res_dx = 0;
        double res_dy = 0;
        double res_dz = 0;

         if((idx_0 != -1) && (idx_1 != -1)  )
         {
             double ddx = (*final_res_1)[idx_1]._x - (*final_res_0)[idx_0]._x ;
             double ddy = (*final_res_1)[idx_1]._y - (*final_res_0)[idx_0]._y ;
             double ddz = (*final_res_1)[idx_1]._z - (*final_res_0)[idx_0]._z ;


             for( int j = 0; j < gmsz; j++ )

             {
                 cv::Point2f hp0 =  keypoints_1[ good_matches[j].queryIdx ].pt;
                 cv::Point2f hp1  = keypoints_2[ good_matches[j].trainIdx ].pt;
                 int hp0x, hp0y, hp1x, hp1y;
                 extract_2dpoint_coord(hp0, hp0x, hp0y, hp1, hp1x, hp1y,
                                       left_map0._xs, left_map0._ys);

                 int hidx_0 = left_map0._data[hp0y][hp0x];
                 int hidx_1 = left_map1._data[hp1y][hp1x];

                 if((hidx_0 != -1) && (hidx_1 != -1)  )
                 {

                     double hdx = (*final_res_1)[hidx_1]._x - ddx - (*final_res_0)[hidx_0]._x ;
                     double hdy = (*final_res_1)[hidx_1]._y - ddy - (*final_res_0)[hidx_0]._y ;
                     double hdz = (*final_res_1)[hidx_1]._z - ddz - (*final_res_0)[hidx_0]._z ;


                     if ( sqrt(hdx * hdx + hdy * hdy + hdz * hdz) <= dist_thres)
                     {

                         loc_inliers++;

                         res_dx += hdx + ddx;
                         res_dy += hdy + ddy;
                         res_dz += hdz + ddz;
                     }
                 }

             };

             if(loc_inliers > glob_inliers)
             {
                 glob_inliers = loc_inliers;
                 dx = res_dx/loc_inliers;
                 dy = res_dy/loc_inliers;
                 dz = res_dz/loc_inliers;
             }

         }

    }



    std::cout << "||resof " << dx  << " " << dy << " " << dz << " " << glob_inliers << " all " << gmsz << std::endl;

    std::vector<C3DPoint>::iterator it = final_res_1->begin();
    for( ; it != final_res_1->end(); it++)
    {
            it->_x -= dx;
            it->_y -= dy;
            it->_z -= dz;
    }

}

cv::Mat CDisparityCompClassicThread::get_back_rotation_matrix()
{
    cv::Mat rvec = (cv::Mat_<double>(1,3) << 0, BGM(15), 0) ;
    std::cout << " backw matrix " << rvec << std::endl;

    cv::Mat rotM;//(3, 3, CV_64FC1);
    cv::Rodrigues(rvec, rotM);
    return rotM;
}

cv::Mat CDisparityCompClassicThread::get_forward_rotation_matrix()
{
    cv::Mat rvec = (cv::Mat_<double>(1,3) << 0, -BGM(15), 0) ;

    cv::Mat rotM;//(3, 3, CV_64FC1);
    cv::Rodrigues(rvec, rotM);
    return rotM;
}


double CDisparityCompClassicThread::compute_av_repo_error(cv::Mat & P2,
                             cv::Mat & K,
                             std::vector<cv::KeyPoint> & pt_all_1L,
                             std::vector<cv::KeyPoint> & pt_all_2L,
                             std::vector<cv::DMatch> & matches,
                              cv::Mat& img3d1)
{

    double res_error = 0;
    int counter = 0;
    std::vector<cv::DMatch>::iterator gmit = matches.begin();
    for( ; gmit != matches.end(); gmit++)
    {
        /// get 3d point in pcl1
        cv::Point2f p_1L = pt_all_1L[gmit->queryIdx].pt;
        cv::Point3f  pp = (img3d1.at<cv::Point3f>(p_1L));
        cv::Mat pp_mat = (cv::Mat_<double>(4,1) << pp.x, pp.y, pp.z,1);
        cv::Mat uv = K * P2 * pp_mat;
        uv  /= uv.at<double>(2,0);

        cv::Point2f puv;
        puv.x = uv.at<double>(0,0);
        puv.y = uv.at<double>(1,0);

         cv::Point2f p_2L = pt_all_2L[gmit->trainIdx].pt;
         cv::Mat p_2L_mat = (cv::Mat_<double>(3,1) << p_2L.x, p_2L.y, 1);
         res_error += cv::norm(p_2L_mat - uv) ;
         counter ++;
    }
    return res_error/counter;
}

double CDisparityCompClassicThread::find_camP2_Tz_iter(int iter_num,
                          double Tx,
                          double Ty,
                          double initTz,
                          cv::Mat & K,
                          std::vector<cv::KeyPoint> & pt_all_1L,
                          std::vector<cv::KeyPoint> & pt_all_2L,
                          std::vector<cv::DMatch> & matches,
                           cv::Mat& img3d1)
{

    double resTz = initTz;
    double factor  = 0.3;


    double resTz_left    = resTz *(1 - factor);
    double resTz_right  = resTz *(1 + factor);

    double globTz = initTz;
    double globErr = 10000;

    for(int i = 0; i < iter_num; i++)
    {

        cv::Mat camT = (cv::Mat_<double>(3,1) << Tx, Ty, resTz);

        cv::Mat forwR = get_forward_rotation_matrix();

        cv::Mat poseT = -forwR * camT;

        cv::Mat P2 = (cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2),  poseT.at<double>(0,0) ,
                                              forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2),   poseT.at<double>(1,0) ,
                                              forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2),   poseT.at<double>(2,0));
        double f_xi = compute_av_repo_error(P2, K, pt_all_1L, pt_all_2L,
                                            matches, img3d1);


        cv::Mat camTd = (cv::Mat_<double>(3,1) << Tx, Ty, resTz_right);

        cv::Mat poseTd = -forwR * camTd;

        cv::Mat P2d = (cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2),  poseTd.at<double>(0,0) ,
                                              forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2),   poseTd.at<double>(1,0) ,
                                              forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2),   poseTd.at<double>(2,0));


        double f_xi_dx = compute_av_repo_error(P2d, K, pt_all_1L, pt_all_2L,
                                               matches, img3d1);

        cv::Mat camTdd = (cv::Mat_<double>(3,1) << Tx, Ty, resTz_left);

        cv::Mat poseTdd = -forwR * camTdd;

        cv::Mat P2dd = (cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2),  poseTdd.at<double>(0,0) ,
                                              forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2),   poseTdd.at<double>(1,0) ,
                                              forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2),   poseTdd.at<double>(2,0));


        double f_xi_ddx = compute_av_repo_error(P2dd, K, pt_all_1L, pt_all_2L,
                                               matches, img3d1);


        if(f_xi_ddx < f_xi_dx)
        {
            resTz_right = resTz;
            resTz       = (resTz_right + resTz_left)/2;
        }
        else
        {
            resTz_left = resTz;
            resTz       = (resTz_right + resTz_left)/2;
        }

        if(f_xi < globErr)
        {
             globTz = resTz;
             globErr = f_xi;

        }
        if(f_xi_dx < globErr)
        {
            globTz  = resTz_right;
            globErr = f_xi_dx;

        }

        if(f_xi_ddx < globErr)
        {
            globTz  = resTz_left;
            globErr = f_xi_ddx;

        }
    }

    return globTz;
}
float glob_gl_scale     = 1.;
float glob_gl_offx      = 0.;
float glob_gl_offy      = 0.;
float glob_gl_offz      = 3.5;
int pcl_sparce_decim_step = 4;
int glob_ignore_disp_offset = 70;

std::vector<C3DPoint> * CDisparityCompClassicThread::construct_pcl_from_depthmap(cv::Mat & img_L1,
                                                              cv::Mat & disp_L1,
                                                              cv::Mat & depth,
                                                              cv::Mat & R,
                                                              cv::Mat & T)
{
    int xs = img_L1.cols;
    int ys = img_L1.rows;

    std::vector<C3DPoint> *  final_res = new std::vector<C3DPoint>;


    for (int y = 0; y < ys; y+= pcl_sparce_decim_step  )
    for (int x = 0; x < xs; x+=pcl_sparce_decim_step )
    {
        // only for valid disparities

        int cdisp = disp_L1.at<short>(y,x);
        if((cdisp > ( 16* (_lbias + glob_ignore_disp_offset ) )) && (cdisp < 16*(_lbias  + _num_disp)) )
        {
            cv::Point3f pp = depth.at<cv::Point3f>(y,x);

            cv::Mat pp_mat = (cv::Mat_<double>(3,1) << pp.x, pp.y, pp.z);

            cv::Mat pp_mat_glob = R*pp_mat + T;

            C3DPoint p;
            p._x =   pp_mat_glob.at<double>(0,0)  * glob_gl_scale + glob_gl_offx;
            p._y = - pp_mat_glob.at<double>(1,0)  * glob_gl_scale + glob_gl_offy;
            p._z = - pp_mat_glob.at<double>(2,0)  * glob_gl_scale + glob_gl_offz;

            cv::Vec3b rgbv = img_L1.at<cv::Vec3b>(y,x);
             p._r = float(rgbv.val[2]) / 256.0;
             p._g = float(rgbv.val[1]) / 256.0;
             p._b = float(rgbv.val[0]) / 256.0;


            final_res->push_back(p);


        }
    }

    return final_res;
}

bool icp_off_sort(const cv::Point3f & a, const cv::Point3f & b)
{
    return (cv::norm(a) < cv::norm(b));
}

bool icp_off_sort_x(const cv::Point3f & a, const cv::Point3f & b)
{
    return (a.x < b.x);
}

bool icp_off_sort_y(const cv::Point3f & a, const cv::Point3f & b)
{
    return (a.y < b.y);
}

bool icp_off_sort_z(const cv::Point3f & a, const cv::Point3f & b)
{
    return (a.z < b.z);
}

void CDisparityCompClassicThread::run_icp() // icp
{
    cv::Mat Q = (cv::Mat_<double>(4,4) << 1., 0., 0., -6.3939643859863281e+002, // < i've change it
                                          0., 1., 0., -4.7951318359375000e+002,
                                          0., 0., 0., 1.9322238394618969e+003, 0.,
                                          0., 2.3800047808414698e-001, 0. );

    cv::Mat K = (cv::Mat_<double>(3,3) << 1.9323147802365747e+003, 0., 6.3949586966917070e+002,
                                         0.,  1.9322613618745359e+003, 4.7951334959687784e+002,
                                         0.,  0., 1. );
    cv::Mat Kinv = K.inv();

    cv::Mat P1 = (cv::Mat_<double>(3,4) << 1, 0, 0,  0,
                                          0, 1, 0,   0,
                                          0, 0, 1,   0 );

    /////////////////////////////////////////////////////////////////////
    /////// construct stereo depth
    cv::StereoSGBM sbm;
    sbm.SADWindowSize =  _SADWindowSize;
    sbm.numberOfDisparities = _num_disp;
    sbm.preFilterCap = _preFilterCap;
    sbm.minDisparity = _lbias;
    sbm.uniquenessRatio = _uniquenessRatio;
    sbm.speckleWindowSize = _speckleWindowSize;
    sbm.speckleRange = _speckleRange;
    sbm.disp12MaxDiff = _disp12MaxDiff;
    sbm.fullDP = _fullDP;
    sbm.P1 =  _P1;
    sbm.P2 = _P2;



    //////////////////////////////////////////////
    std::ofstream logfile;
    logfile.open ("camera_coord.log");

    cv::Mat gT = (cv::Mat_<double>(3,1) << 0,0,0);

    int img_idx_max =  24;
    for (int img_idx = 0;  img_idx < img_idx_max; img_idx++)
 {
    std::cout << "=============== Processing " << img_idx << " frame" << std::endl;

    /////////////////////////////////////////////////////////////////////
    //////// load left and right image
    QString im_1L_str  = QString("./sfacerpUfXL/left_00%1.png").arg(img_idx, 2, 10, QLatin1Char('0'));
    cv::Mat img_1L = cv::imread(im_1L_str.toStdString() );

    QString im_1R_str  = QString("./sfacerpUfXL/right_00%1.png").arg(img_idx, 2, 10, QLatin1Char('0'));
    cv::Mat img_1R = cv::imread(im_1R_str.toStdString() );

    QString im_2L_str  = QString("./sfacerpUfXL/left_00%1.png").arg(img_idx+1, 2, 10, QLatin1Char('0'));
    cv::Mat img_2L = cv::imread(im_2L_str.toStdString() );

    QString im_2R_str  = QString("./sfacerpUfXL/right_00%1.png").arg(img_idx+1, 2, 10, QLatin1Char('0'));
    cv::Mat img_2R = cv::imread(im_2R_str.toStdString() );

    cv::Mat disp1;
    sbm(img_1L, img_1R, disp1);


    cv::Mat img3d1;
    reprojectImageTo3D(disp1, img3d1, Q);

    cv::Mat disp2;
    sbm(img_2L, img_2R, disp2);
    cv::Mat img3d2;
    reprojectImageTo3D(disp2, img3d2, Q);


    /////// compute interesting points

    // add all images
    std::vector<cv::Mat> imgs;
    imgs.push_back(img_1L);
    imgs.push_back(img_1R);
    imgs.push_back(img_2L);
    imgs.push_back(img_2R);
    int img_idx_1L = 0;
    int img_idx_1R = 1;
    int img_idx_2L = 2;
    int img_idx_2R = 3;



    std::vector<std::vector<cv::KeyPoint> >  imgpts; // for all input images
    cv::Ptr<IFeatureMatcher> feature_matcher = new SURFFeatureMatcher(imgs,imgpts);
    /// cv::Ptr<IFeatureMatcher> feature_matcher = new RichFeatureMatcher(imgs,imgpts);

    std::vector<cv::KeyPoint> pt_all_1L = feature_matcher->GetImagePoints(img_idx_1L);
    std::vector<cv::KeyPoint> pt_all_2L = feature_matcher->GetImagePoints(img_idx_2L);

     // cv::Mat img_keypoints;
    // cv::drawKeypoints( img_1L, pt_all_1L, img_keypoints, cv::Scalar::all(-1), cv::DrawMatchesFlags::DEFAULT );
     // imshow("Keypoints", img_keypoints);

    /////////// compute matches
    std::vector<cv::DMatch> matches_1L_2L;
    feature_matcher->MatchFeatures(img_idx_1L, img_idx_2L, &matches_1L_2L);

    /////////// filter good matches only
    std::vector<cv::KeyPoint> pts_good_1L, pts_good_2L;
    GetFundamentalMat(pt_all_1L, pt_all_2L,
                      pts_good_1L, pts_good_2L,
                      matches_1L_2L
    #ifdef __SFM__DEBUG__
                          , img_1L,
                           img_2L
    #endif
                          );


    ////// filter out matches without depth information
    std::vector<cv::DMatch> matches_1L_2L_good;
    std::vector<cv::DMatch>::iterator mit = matches_1L_2L.begin();
    for( ; mit != matches_1L_2L.end(); mit ++)
    {

       cv::Point2f p_1L = pt_all_1L[mit->queryIdx].pt;
       cv::Point2f p_2L = pt_all_2L[mit->trainIdx].pt;

       int disp_1L = disp1.at<short>(p_1L.y,p_1L.x);
       int disp_2L = disp2.at<short>(p_2L.y,p_2L.x);

       if( (disp_1L > glob_ignore_disp_offset) && (disp_2L > glob_ignore_disp_offset) )
       {
           // add good match
           matches_1L_2L_good.push_back(*mit);
       }
    }
    std::cout << "encounter very good matches : " << matches_1L_2L_good.size() << std::endl;


    cv::Mat backR = get_back_rotation_matrix();
    //// std::cout << "backward roatation matrix " << backR << std::endl;


    std::vector<cv::Point3f> proj3d_1L;
    std::vector<cv::Point3f> proj3d_2L;

    std::vector<cv::DMatch>::iterator gmit = matches_1L_2L_good.begin();
    std::vector<cv::Point3f> icp_off;
    for( ; gmit != matches_1L_2L_good.end(); gmit ++)
    {
        /// get 3d point in pcl1
        cv::Point2f p_1L = pt_all_1L[gmit->queryIdx].pt;
        cv::Point3f  p3_1L = (img3d1.at<cv::Point3f>(p_1L));

        /// unrotate 3d point in pcl2
        cv::Point2f p_2L   = pt_all_2L[gmit->trainIdx].pt;
        cv::Point3f p3_2Lo = (img3d2.at<cv::Point3f>(p_2L)); //p_2L.y,p_2L.x
        cv::Mat p3d_2Lm = (cv::Mat_<double>(3,1) << p3_2Lo.x, p3_2Lo.y, p3_2Lo.z );
        cv::Mat p3d_2L_rot = backR * p3d_2Lm;

        cv::Point3f  p3_2L;
        p3_2L.x = p3d_2L_rot.at<double>(0,0);
        p3_2L.y = p3d_2L_rot.at<double>(1,0);
        p3_2L.z = p3d_2L_rot.at<double>(2,0);

        cv::Point3f  off = p3_1L-p3_2L;

        icp_off.push_back(off);
       // std::cout << off << " norm:" << cv::norm(off) << std::endl;
    }

    /// 3 stage sort

    std::sort(icp_off.begin(), icp_off.end(),icp_off_sort_x);
    std::vector<cv::Point3f> icp_after_x;
    int vsz = icp_off.size();
    double cutoff_min = double(vsz) * 0.2;
    double cutoff_max = double(vsz) * 0.8;
    for(int i = cutoff_min; i <  cutoff_max; i++ )
        icp_after_x.push_back(icp_off[i]);


    std::sort(icp_after_x.begin(), icp_after_x.end(),icp_off_sort_y);
    std::vector<cv::Point3f> icp_after_xy;
    int vszXY = icp_after_x.size();
      cutoff_min = double(vszXY) * 0.2;
      cutoff_max = double(vszXY) * 0.8;
    for(int i = cutoff_min; i <  cutoff_max; i++ )
        icp_after_xy.push_back(icp_after_x[i]);

    std::sort(icp_after_xy.begin(), icp_after_xy.end(),icp_off_sort_z);
    std::vector<cv::Point3f> icp_after_xyz;
    int vszXYZ = icp_after_xy.size();
      cutoff_min = double(vszXYZ) * 0.2;
      cutoff_max = double(vszXYZ) * 0.8;
    for(int i = cutoff_min; i <  cutoff_max; i++ )
        icp_after_xyz.push_back(icp_after_xy[i]);

    int res_sz = icp_after_xyz.size();
    cv::Point3f roff;
    int vcount = 0;
    for(int i = 0; i <  res_sz; i++ )
    {
       std::cout << ": " << icp_after_xyz[i] << std::endl;

        roff += icp_after_xyz[i];
        vcount ++;
    }

    double Tx = roff.x / double(vcount);
    double Ty = roff.y / double(vcount);
    double Tz = roff.z / double(vcount);


    cv::Mat camT = (cv::Mat_<double>(3,1) << Tx, Ty, Tz);
    std::cout << "3d  delta " << camT.t() << std::endl;

    cv::Mat forwR = get_forward_rotation_matrix();

    cv::Mat poseT = -forwR * camT;

    cv::Mat P2 = (cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2),  poseT.at<double>(0,0) ,
                                          forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2),   poseT.at<double>(1,0) ,
                                          forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2),   poseT.at<double>(2,0));

    /////////////////// visualize camera pose

    /*std::vector<cv::DMatch>::iterator*/ gmit = matches_1L_2L_good.begin();

    std::vector<double> vec_res_err;
    for( ; gmit != matches_1L_2L_good.end(); gmit ++)
    {
        /// get 3d point in pcl1
        cv::Point2f p_1L = pt_all_1L[gmit->queryIdx].pt;
        cv::Point3f  pp = (img3d1.at<cv::Point3f>(p_1L));
        cv::Mat pp_mat = (cv::Mat_<double>(4,1) << pp.x, pp.y, pp.z,1);
        cv::Mat uv = K * P2 * pp_mat;
        uv  /= uv.at<double>(2,0);

        cv::Point2f puv;
        puv.x = uv.at<double>(0,0);
        puv.y = uv.at<double>(1,0);

         cv::Point2f p_2L = pt_all_2L[gmit->trainIdx].pt;
         cv::Mat p_2L_mat = (cv::Mat_<double>(3,1) << p_2L.x, p_2L.y, 1);

        vec_res_err.push_back(cv::norm(p_2L_mat - uv));

   }
            std::sort(vec_res_err.begin(), vec_res_err.end());
            int ersz = vec_res_err.size();
            double ecutoff_min = double(ersz ) * 0.2;
            double ecutoff_max = double(ersz ) * 0.8;
            double res_error = 0;
            int ecount = 0;
            for(int i = ecutoff_min; i <  ecutoff_max; i++ )
            {
                res_error += vec_res_err[i];
                ecount ++;
            }
            res_error /= ecount;


    std::cout << "====================== Result " << std::endl;
    // global L1 camera rotation
    cv::Mat rvec_glob_L1 = (cv::Mat_<double>(1,3) << 0, BGM((img_idx)*(15)), 0) ;
    cv::Mat rotM_glob_L1;
    cv::Rodrigues(rvec_glob_L1, rotM_glob_L1);



    std::vector<C3DPoint> * pcl = construct_pcl_from_depthmap(img_1L,disp1, img3d1,rotM_glob_L1, gT );
    emit send_result(pcl);
    emit send_back(100*(img_idx+1)/img_idx_max);

    CCameraPose * ps = new CCameraPose;
    ps->_tx =     gT.at<double>(0,0)   * glob_gl_scale + glob_gl_offx;
    ps->_ty =  - gT.at<double>(1,0)   * glob_gl_scale + glob_gl_offy ;
    ps->_tz =  - gT.at<double>(2,0)    * glob_gl_scale + glob_gl_offz;

    ps->_rotx = -rvec_glob_L1.at<double>(0,0)*360/cPI2;
    ps->_roty = -rvec_glob_L1.at<double>(0,1)*360/cPI2;
    ps->_rotz = -rvec_glob_L1.at<double>(0,2)*360/cPI2;

    //// add orientation
    send_result(ps);
    std::cout << "new camera position" << std::endl;

    cv::Mat dcampos = rotM_glob_L1*camT;

    gT += dcampos ;
    std::cout << "computed gT: " << gT.t() << std::endl;
    std::cout <<  gT.t() << " with local coord" << camT.t()  << std::endl;
    logfile << gT.t() << "  " << camT.t()  << "  " <<res_error   << std::endl;

    }

    logfile.close();

}

void CDisparityCompClassicThread::run()
{
    //////////////// given matrices

   /* cv::Mat Q = (cv::Mat_<double>(4,4) <<  1., 0., 0.,           -2.3172327889654838e+002,
                                           0., 1., 0.,           -2.9450375366210938e+002,
                                           0., 0., 0.,            1.9689409472867962e+003,
                                           0., 0., 5.6433084190423988e-001, 0. );

    cv::Mat K = (cv::Mat_<double>(3,3) <<  1.9788960567438073e+003, 0., 2.3172327889654838e+002,
                                           0., 1.9793919385468250e+003, 2.9463345131205296e+002,
                                           0., 0., 1.);*/

    cv::Mat Q = (cv::Mat_<double>(4,4) << 1., 0., 0., -6.3939643859863281e+002, // < i've change it
                                          0., 1., 0., -4.7951318359375000e+002,
                                          0., 0., 0., 1.9322238394618969e+003, 0.,
                                          0., 2.3800047808414698e-001, 0. );

    cv::Mat K = (cv::Mat_<double>(3,3) << 1.9323147802365747e+003, 0., 6.3949586966917070e+002,
                                         0.,  1.9322613618745359e+003, 4.7951334959687784e+002,
                                         0.,  0., 1. );
    cv::Mat Kinv = K.inv();

    cv::Mat P1 = (cv::Mat_<double>(3,4) << 1, 0, 0,  0,
                                          0, 1, 0,   0,
                                          0, 0, 1,   0 );

    /////////////////////////////////////////////////////////////////////
    /////// construct stereo depth
    cv::StereoSGBM sbm;
    sbm.SADWindowSize =  _SADWindowSize;
    sbm.numberOfDisparities = _num_disp;
    sbm.preFilterCap = _preFilterCap;
    sbm.minDisparity = _lbias;
    sbm.uniquenessRatio = _uniquenessRatio;
    sbm.speckleWindowSize = _speckleWindowSize;
    sbm.speckleRange = _speckleRange;
    sbm.disp12MaxDiff = _disp12MaxDiff;
    sbm.fullDP = _fullDP;
    sbm.P1 =  _P1;
    sbm.P2 = _P2;



    //////////////////////////////////////////////
    std::ofstream logfile;
    logfile.open ("camera_coord.log");

    cv::Mat gT = (cv::Mat_<double>(3,1) << 0,0,0);

    int img_idx_max =  24;
    for (int img_idx = 0;  img_idx < img_idx_max; img_idx++)
 {
    std::cout << "=============== Processing " << img_idx << " frame" << std::endl;

    clock_t msstart_all  = clock();

    /////////////////////////////////////////////////////////////////////
    //////// load left and right image
    QString im_1L_str  = QString("./sfacerpUfXL/left_00%1.png").arg(img_idx, 2, 10, QLatin1Char('0'));
    cv::Mat img_1L = cv::imread(im_1L_str.toStdString() );

    QString im_1R_str  = QString("./sfacerpUfXL/right_00%1.png").arg(img_idx, 2, 10, QLatin1Char('0'));
    cv::Mat img_1R = cv::imread(im_1R_str.toStdString() );

    QString im_2L_str  = QString("./sfacerpUfXL/left_00%1.png").arg(img_idx+1, 2, 10, QLatin1Char('0'));
    cv::Mat img_2L = cv::imread(im_2L_str.toStdString() );

    QString im_2R_str  = QString("./sfacerpUfXL/right_00%1.png").arg(img_idx+1, 2, 10, QLatin1Char('0'));
    cv::Mat img_2R = cv::imread(im_2R_str.toStdString() );




    cv::Mat disp1;
    sbm(img_1L, img_1R, disp1);

/*

    std::ofstream dispf;
    dispf.open ("disp_classic_float.raw", std::ios::out| std::ios::binary);
    for (int j = 0; j < disp1.rows; j++)
    for (int i = 0; i < disp1.cols; i++)
         {
             float val = (float)disp1.at<short>(j,i)/16;
               dispf.write((char*)&val,  sizeof(float));
         }
     dispf.close();
    return;
*/

    cv::Mat img3d1;
    reprojectImageTo3D(disp1, img3d1, Q);

   cv::Mat disp2;
    sbm(img_2L, img_2R, disp2);
    cv::Mat img3d2;
    reprojectImageTo3D(disp2, img3d2, Q);

    clock_t msstart  = clock();


    /////// compute interesting points

    // add all images
    std::vector<cv::Mat> imgs;
    imgs.push_back(img_1L);
    imgs.push_back(img_1R);
    imgs.push_back(img_2L);
    imgs.push_back(img_2R);
    int img_idx_1L = 0;
    int img_idx_1R = 1;
    int img_idx_2L = 2;
    int img_idx_2R = 3;




    std::vector<std::vector<cv::KeyPoint> >  imgpts; // for all input images
    ///cv::Ptr<IFeatureMatcher> feature_matcher = new SURFFeatureMatcher(imgs,imgpts);
    cv::Ptr<IFeatureMatcher> feature_matcher = new RichFeatureMatcher(imgs,imgpts);




    std::vector<cv::KeyPoint> pt_all_1L = feature_matcher->GetImagePoints(img_idx_1L);
    std::vector<cv::KeyPoint> pt_all_2L = feature_matcher->GetImagePoints(img_idx_2L);


     // cv::Mat img_keypoints;
    // cv::drawKeypoints( img_1L, pt_all_1L, img_keypoints, cv::Scalar::all(-1), cv::DrawMatchesFlags::DEFAULT );
     // imshow("Keypoints", img_keypoints);

    clock_t msstartA  = clock();

    /////////// compute matches
    std::vector<cv::DMatch> matches_1L_2L;
    feature_matcher->MatchFeatures(img_idx_1L, img_idx_2L, &matches_1L_2L);

    clock_t mffinishA = clock();
    int sec_convA = (mffinishA - msstartA);
    std::cout << "ONLY MatchFeatures functs - time clocks: " << sec_convA << " secs: " << ((float)sec_convA )/CLOCKS_PER_SEC << std::endl;

    /////////// filter good matches only
    std::vector<cv::KeyPoint> pts_good_1L, pts_good_2L;
    GetFundamentalMat(pt_all_1L, pt_all_2L,
                      pts_good_1L, pts_good_2L,
                      matches_1L_2L
    #ifdef __SFM__DEBUG__
                          , img_1L,
                           img_2L
    #endif
                          );


    ////// filter out matches without depth information
    std::vector<cv::DMatch> matches_1L_2L_good;
    std::vector<cv::DMatch>::iterator mit = matches_1L_2L.begin();
    for( ; mit != matches_1L_2L.end(); mit ++)
    {

       cv::Point2f p_1L = pt_all_1L[mit->queryIdx].pt;
       cv::Point2f p_2L = pt_all_2L[mit->trainIdx].pt;

       int disp_1L = disp1.at<short>(p_1L.y,p_1L.x);
       int disp_2L = disp2.at<short>(p_2L.y,p_2L.x);

       if( (disp_1L > glob_ignore_disp_offset) && (disp_2L > glob_ignore_disp_offset) )
       {
           // add good match
           matches_1L_2L_good.push_back(*mit);
       }
    }
    std::cout << "encounter very good matches : " << matches_1L_2L_good.size() << std::endl;



    cv::Mat backR = get_back_rotation_matrix();
    //// std::cout << "backward roatation matrix " << backR << std::endl;

    std::vector<cv::Point2f> proj3d_1L;
    std::vector<cv::Point2f> proj3d_2L;
   // std::vector<cv::Vec3b> proj3d_1L_colors;
   // std::vector<cv::Vec3b> proj3d_2L_colors;

    std::vector<cv::DMatch>::iterator gmit = matches_1L_2L_good.begin();
    std::vector<double> vecTz;
    for( ; gmit != matches_1L_2L_good.end(); gmit ++)
    {
        /// get 3d point in pcl1
        cv::Point2f p_1L = pt_all_1L[gmit->queryIdx].pt;
        cv::Point3f  p3_1L = (img3d1.at<cv::Point3f>(p_1L));

        //std::cout << "1: " <<  p3_1L << std::endl;

        cv::Point2f pp1L;
        pp1L.x = p3_1L.x;
        pp1L.y = p3_1L.y;
        proj3d_1L.push_back(pp1L);

       // proj3d_1L_colors.push_back(img_1L.at<cv::Vec3b>(p_1L));

        /// unrotate 3d point in pcl2
        cv::Point2f p_2L   = pt_all_2L[gmit->trainIdx].pt;
        cv::Point3f p3_2L = (img3d2.at<cv::Point3f>(p_2L)); //p_2L.y,p_2L.x
        cv::Mat p3d_2L = (cv::Mat_<double>(3,1) << p3_2L.x, p3_2L.y, p3_2L.z );
        cv::Mat p3d_2L_rot = backR * p3d_2L;

       // std::cout << "2: " <<  p3d_2L_rot.t()  <<  " dz= " << p3d_2L_rot.at<double>(2,0)- p3_1L.z <<std::endl;

        cv::Point2f pp2L;
        pp2L.x = p3d_2L_rot.at<double>(0,0);
        pp2L.y = p3d_2L_rot.at<double>(1,0);
        proj3d_2L.push_back(pp2L);

       //// std::cout << "dz: " <<  p3_1L.z - p3d_2L_rot.at<double>(2,0) << std::endl;

        double dz = p3_1L.z - p3d_2L_rot.at<double>(2,0);
        vecTz.push_back(dz);

      // std::cout << "homo: " << pp1L << " " <<  pp2L  << std::endl;

       // proj3d_2L_colors.push_back(img_2L.at<cv::Vec3b>(p_2L));
    }

    std::sort(vecTz.begin(), vecTz.end());
    int vsz = vecTz.size();
    double cutoff_min = double(vsz) * 0.2;
    double cutoff_max = double(vsz) * 0.8;
    double initTz = 0;
    int vcount = 0;
    for(int i = cutoff_min; i <  cutoff_max; i++ )
    {
        initTz += vecTz[i];
        vcount ++;
    }
    initTz /= vcount;

    cv::Mat H = cv::findHomography(proj3d_2L, proj3d_1L, CV_LMEDS  );
    ///std::cout << "homography matrix " << H << std::endl;
    // POSSIBLE PATCH - check both methods and select the best one


    double a = H.at<double>(0,0);
    double b = H.at<double>(0,1);
    double c = H.at<double>(0,2);
    double d = H.at<double>(1,0);
    double e = H.at<double>(1,1);
    double f = H.at<double>(1,2);


    double p = sqrt(a*a + b*b);
    double r = (a*e - b*d)/p;
    double q = (a*d+b*e)/(a*e - b *d);


    std::cout << "translation " << c << " " << f << std::endl;
     std::cout << "scale " << p << " " << r << std::endl;
     std::cout << "shear " << q << std::endl;
     std::cout << "theta " << atan2(b,a) * 180 / 3.14159 << std::endl;
   /// std::cout << "initTz " << initTz << std::endl;


    double Tx =  c;
    double Ty =  f;

    // revisit this procesdure
    double Tz = find_camP2_Tz_iter(5, Tx, Ty,  initTz,
            K,pt_all_1L, pt_all_2L, matches_1L_2L_good, img3d1);



    cv::Mat camT = (cv::Mat_<double>(3,1) << Tx, Ty, Tz);

    cv::Mat forwR = get_forward_rotation_matrix();

    cv::Mat poseT = -forwR * camT;

    cv::Mat P2 = (cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2),  poseT.at<double>(0,0) ,
                                          forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2),   poseT.at<double>(1,0) ,
                                          forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2),   poseT.at<double>(2,0));

    ////std::cout << "second camera coord: " << camT.t() << std::endl;
    ////std::cout << "second camera pose: "  << P2 << std::endl;

    /////////////////// visualize camera pose
    //cv::Mat poseviz_orig(img_2L);
    //cv::RNG rng(12345);
    /*std::vector<cv::DMatch>::iterator*/ gmit = matches_1L_2L_good.begin();

    std::vector<double> vec_res_err;
    for( ; gmit != matches_1L_2L_good.end(); gmit ++)
    {
        /// get 3d point in pcl1
        cv::Point2f p_1L = pt_all_1L[gmit->queryIdx].pt;
        cv::Point3f  pp = (img3d1.at<cv::Point3f>(p_1L));
        cv::Mat pp_mat = (cv::Mat_<double>(4,1) << pp.x, pp.y, pp.z,1);
        cv::Mat uv = K * P2 * pp_mat;
        uv  /= uv.at<double>(2,0);

        cv::Point2f puv;
        puv.x = uv.at<double>(0,0);
        puv.y = uv.at<double>(1,0);


       // cv::Vec3b color(0,255,0);
        //poseviz_orig.at<cv::Vec3b>(uv.at<double>(1,0), uv.at<double>(0,0)) = color;

         cv::Point2f p_2L = pt_all_2L[gmit->trainIdx].pt;
         cv::Mat p_2L_mat = (cv::Mat_<double>(3,1) << p_2L.x, p_2L.y, 1);

       //  std::cout << p_2L_mat.t() << " " << uv.t() << " " << cv::norm(p_2L_mat - uv) << std::endl;

        vec_res_err.push_back(cv::norm(p_2L_mat - uv));

        ////cv::line(poseviz_orig,puv ,p_2L, cv::Scalar(rng.uniform(0,255), rng.uniform(0, 255), rng.uniform(0, 255)), 1 );
    }
            std::sort(vec_res_err.begin(), vec_res_err.end());
            int ersz = vec_res_err.size();
            double ecutoff_min = double(ersz ) * 0.2;
            double ecutoff_max = double(ersz ) * 0.8;
            double res_error = 0;
            int ecount = 0;
            for(int i = ecutoff_min; i <  ecutoff_max; i++ )
            {
                res_error += vec_res_err[i];
                ecount ++;
            }
            res_error /= ecount;


    std::cout << "repo- error" << res_error << std::endl;
    std::cout << "====================== Result " << std::endl;



    // global L1 camera rotation
    cv::Mat rvec_glob_L1 = (cv::Mat_<double>(1,3) << 0, BGM((img_idx)*(15)), 0) ;
    cv::Mat rotM_glob_L1;
    cv::Rodrigues(rvec_glob_L1, rotM_glob_L1);

    std::vector<C3DPoint> * pcl = construct_pcl_from_depthmap(img_1L,disp1, img3d1,rotM_glob_L1, gT );
    emit send_result(pcl);
    emit send_back(100*(img_idx+1)/img_idx_max);

    CCameraPose * ps = new CCameraPose;
    ps->_tx = (   gT.at<double>(0,0)   * glob_gl_scale + glob_gl_offx);// * 0.05;
    ps->_ty = (- gT.at<double>(1,0)   * glob_gl_scale + glob_gl_offy );// * 0.05;
    ps->_tz = (- gT.at<double>(2,0)    * glob_gl_scale + glob_gl_offz);// * 0.05;

    // std::cout << "bef " << rvec_glob_L1 << " " <<  rvec_glob_L1.at<double>(1,0) <<  std::endl;

    ps->_rotx = -rvec_glob_L1.at<double>(0,0)*360/cPI2;
    ps->_roty = -rvec_glob_L1.at<double>(0,1)*360/cPI2;
    ps->_rotz = -rvec_glob_L1.at<double>(0,2)*360/cPI2;

   // std::cout <<  ps->_rotx  << " " <<  ps->_roty << " " <<  ps->_rotz << std::endl;
    //// add orientation
    send_result(ps);


    std::cout << "new camera position" << std::endl;


    cv::Mat dcampos = rotM_glob_L1*camT;

    gT += dcampos ;
    std::cout << "computed gT: " << gT.t() << std::endl;


    ///////////////////// just replace the global values with real one
    /*{

        std::ifstream infile("./sfacerpUfXL/rpos.txt");
        int fidx = 0;
        double tx, ty, tz;
        while (infile >> tx >> ty >> tz )
        {
            // process pair (a,b)

            if(fidx==img_idx)
            {
                //std::cout << "before " << Tx << " " << Ty << " " << Tz << std::endl;
                //Tx = tx; Ty = ty; Tz = tz;
                //std::cout << "after " << Tx << " " << Ty << " " << Tz << std::endl;

                gT.at<double>(0,0) = tx;
                gT.at<double>(1,0) = ty;
                gT.at<double>(2,0) = tz;


                break;
            }
            fidx++;
        }
        infile.close();
    }*/


    std::cout <<  gT.t() << " with local coord" << camT.t()  << std::endl;

    logfile << gT.t() << "  " << camT.t()  << "  " <<res_error   << std::endl;


    clock_t mffinish = clock();
    int sec_conv = (mffinish - msstart);
    std::cout << "rest  - time clocks: " << sec_conv << " secs: " << ((float)sec_conv )/CLOCKS_PER_SEC << std::endl;


    clock_t mffinish_all = clock();
    int sec_conv_all = (mffinish_all - msstart_all);
    std::cout << "all  - time clocks: " << sec_conv_all << " secs: " << ((float)sec_conv_all )/CLOCKS_PER_SEC << std::endl;


     }

    logfile.close();


}

void CDisparityCompClassicThread::run_old()
{
/*
    int max_frame = 22;
    for(int i =16; i <max_frame ; i++)
    {
        std::vector<C3DPoint> * pcl = testing_loop(i); /// 2 and 4 is good
        emit send_result(pcl);
        emit send_back(float(i) * 100.0/float(max_frame) );
    }
    return;*/
     std::vector<C3DPoint> * pcl = testing_loop();
    //emit send_result(pcl);
      emit send_back(100);

   return ;
    /////////////////////////////////////
    // disparity computation object
    cv::StereoSGBM sbm;
    sbm.SADWindowSize =  _SADWindowSize;
    sbm.numberOfDisparities = _num_disp;
    sbm.preFilterCap = _preFilterCap;
    sbm.minDisparity = _lbias;
    sbm.uniquenessRatio = _uniquenessRatio;
    sbm.speckleWindowSize = _speckleWindowSize;
    sbm.speckleRange = _speckleRange;
    sbm.disp12MaxDiff = _disp12MaxDiff;
    sbm.fullDP = _fullDP;
    sbm.P1 =  _P1;
    sbm.P2 = _P2;

    // focus distance
    //double f = 8.;
    // projection matrix
//data: [ 1., 0., 0., 1.3022762451171875e+003,
//        0., 1., 0., -2.9167308425903320e+002,
//        0., 0., 0., 2.0387384956026071e+003,
//       0., 0., 4.7865290673834482e-001, 0. ]

//data: [ 1., 0., 0.,  1.0045414066314697e+002,
//        0., 1., 0., -2.8984715270996094e+002,
//        0., 0., 0., 1.9652318072074245e+003,
//        0., 0., 5.5993690343645319e-001, 0. ]
    /*
    cv::Mat Q= (cv::Mat_<double>(4,4) << 1./f,  0.,   0.,   -480./(2.0*f),
                                          0., -1/f,   0.,    580./(2.0*f),
                                          0.,   0., 2./f,   0,
                                          0.,   0.,   0.,   1.);
*/
    /*

 data: [] // 6 IMAGES
1., 0., 0.,  1.0045414066314697e+002,
                0., 1., 0., -2.8984715270996094e+002,
                0., 0., 0., 1.9652318072074245e+003,
                0., 0., 5.5993690343645319e-001, 0.
*/
    /// 9 images
    cv::Mat Q= (cv::Mat_<double>(4,4) <<  1., 0., 0., -1.4327235102653503e+001,
                                          0., 1., 0., -2.9450375366210938e+002,
                                         0., 0., 0., 1.9689409472867962e+003,
                                           0., 0., 5.6433084190423988e-001, 0. );

   // cv::Mat Q= (cv::Mat_<float>(4,4) <<  1., 0., 0., 0,
   //                                          0., 1., 0., 0,
    //                                         0., 0., 1., 0,
    //                                         0., 0., 0, 100. );

    std::cout << Q << std::endl;

    int minHessian = 50;
    cv::SurfFeatureDetector detector( minHessian );
    cv::SurfDescriptorExtractor extractor;
    cv::FlannBasedMatcher matcher;

    int max_img_idx = 5;//24

    int img_idx = 0;

    //QString left_im_str_0  = QString("./face/face0%1.png").arg(img_idx);
    //QString left_im_str_0  = QString("./sequence/im%1.ppm").arg(img_idx);
    QString left_im_str_0  = QString("./sfacerpUf/left_face00%1.png").arg(img_idx, 2, 10, QLatin1Char('0'));
    QImage * left_im_0 = new QImage(left_im_str_0);

    //QString right_im_str_0 = QString("./face/face0%1.png").arg(img_idx+1);
    //QString right_im_str_0 = QString("./sequence/im%1.ppm").arg(img_idx+1);
    QString right_im_str_0 = QString("./sfacerpUf/right_face00%1.png").arg(img_idx, 2, 10, QLatin1Char('0'));
    QImage * right_im_0 = new QImage(right_im_str_0);

     std::cout << left_im_str_0.toStdString()  << " " << right_im_str_0.toStdString() << std::endl;


    cv::Mat left_disp0;
    CArray2D<int> left_map0(left_im_0->width(),left_im_0->height(), -1);

    std::vector<C3DPoint> *  final_res_0 = compute_pcloud(sbm,Q,left_im_0,right_im_0,left_map0, left_disp0, img_idx);
    emit send_result(final_res_0);

    for (; img_idx < max_img_idx ; img_idx ++)
    {
        QString left_im_str_1  = QString("./sfacerpUf/left_face00%1.png").arg(img_idx+1, 2, 10, QLatin1Char('0'));
        QImage * left_im_1 = new QImage(left_im_str_1);

        /// QString right_im_str_1 = QString("./face/face0%1.png").arg(img_idx+2);
        QString right_im_str_1 = QString("./sfacerpUf/right_face00%1.png").arg(img_idx+1, 2, 10, QLatin1Char('0'));
        QImage * right_im_1 = new QImage(right_im_str_1);

        cv::Mat left_disp1;
        CArray2D<int> left_map1(left_im_1->width(),left_im_1->height(), -1 );
        std::vector<C3DPoint> *  final_res_1 = compute_pcloud(sbm,Q,left_im_1,right_im_1, left_map1, left_disp1, img_idx+1);

        std::vector< cv::DMatch >  good_matches;
        std::vector<cv::KeyPoint> keypoints_1,keypoints_2;
        compute_correspondance(detector, extractor, matcher, left_im_0, left_im_1,
                                good_matches, keypoints_1, keypoints_2);
        fix_affine_transform( left_map0, left_map1, final_res_0, final_res_1,
                               good_matches, keypoints_1, keypoints_2, left_disp0, left_disp1);


        delete left_im_0;
        delete right_im_0;

        left_im_0  = left_im_1;
        right_im_0 = right_im_1;

        left_disp0  = left_disp1;
        final_res_0 = final_res_1;
        left_map0   = left_map1;

        emit send_result(final_res_1);


    }
    delete left_im_0;
    delete right_im_0;


    emit send_back(100);


}


//// convert results to 3d
/*    double factor_xz =  10.0f;
double factor_z = 4.0f;
std::vector<C3DPoint> *  final_res = new std::vector<C3DPoint>;
cv::MatIterator_<short> it = disp.begin<short>();
for(int idx =0;it!=disp.end<short>(); it++, idx++)
{
    int x = idx % xs;
    int y = idx / xs;

    C3DPoint p;
    p._x = double(x - xs/2) / factor_xz;
    p._y = double(ys - y)/factor_xz;

   short cdisp = *it;
    p._z = factor_z*double(cdisp)/16.; // because of sbm

    QRgb c = left_im->pixel(x,y);
    p._r = (float)qRed(c)/255.0f;
    p._g = (float)qGreen(c)/255.0f;
    p._b = (float)qBlue(c)/255.0f;

    p._nx = 0;
    p._ny = 0;
    p._nz = -1;

    /// add valid pnts only
    if((cdisp > 16*_lbias) && (cdisp < 16*(_lbias  + _num_disp)) )
        final_res->push_back(p);


}
*/
/////////////////////////////////


/*
void CDisparityCompClassicThread::run()
{

    std::cout <<  _left_im << " "<<
    _right_im << " " << _num_disp << " " <<
    _lbias << " " << _SADWindowSize<< " " <<
    _preFilterCap << " " << _uniquenessRatio << " " <<
    _speckleWindowSize << " " << _speckleRange << " " <<
    _disp12MaxDiff << " " << _fullDP << " " <<
    _P1 << " " << _P2 << " " << std::endl;

    int xs = _left_im->width();
    int ys = _left_im->height();

    cv::Mat gleft, gright;

    cv::Mat imgleft = QImage2CVMat(_left_im);
    cv::Mat imgright = QImage2CVMat(_right_im);
    cv::cvtColor(imgleft, gleft, CV_BGR2GRAY);
    cv::cvtColor(imgright, gright, CV_BGR2GRAY);

    cv::StereoSGBM sbm;
    sbm.SADWindowSize =  _SADWindowSize;
    sbm.numberOfDisparities = _num_disp;
    sbm.preFilterCap = _preFilterCap;
    sbm.minDisparity = _lbias;
    sbm.uniquenessRatio = _uniquenessRatio;
    sbm.speckleWindowSize = _speckleWindowSize;
    sbm.speckleRange = _speckleRange;
    sbm.disp12MaxDiff = _disp12MaxDiff;
    sbm.fullDP = _fullDP;
    sbm.P1 =  _P1;
    sbm.P2 = _P2;

    cv::Mat disp;
    sbm(gleft, gright, disp);

    double factor_xz =  10.0f;
    double factor_z = 1.5f;

    std::vector<C3DPoint> *  final_res = new std::vector<C3DPoint>;
    cv::MatIterator_<short> it = disp.begin<short>();
    ////    std::ofstream dispf;
    /// dispf.open ("disp.raw", std::ios::out| std::ios::binary);

    for(int idx =0;it!=disp.end<short>(); it++, idx++)
    {
        int x = idx % xs;
        int y = idx / xs;

        C3DPoint p;
        p._x = double(x - xs/2) / factor_xz;
        p._y = double(ys - y)/factor_xz;

       short cdisp = *it;
       ///std::cout << cdisp << " ";
        p._z = double(cdisp)/16.;

        /// double val = double(*it)/16.;
        ///dispf.write((char*)& val,  sizeof(double));

        QRgb c = _left_im->pixel(x,y);
        p._r = (float)qRed(c)/255.0f;
        p._g = (float)qGreen(c)/255.0f;
        p._b = (float)qBlue(c)/255.0f;

        p._nx = 0;
        p._ny = 0;
        p._nz = -1;

        if((cdisp > 16*_lbias) && (cdisp < 16*(_lbias  + _num_disp)) )
            final_res->push_back(p);


    }
    //  dispf.close();

    delete _left_im;
    delete _right_im;

    emit send_result(final_res);
    emit send_back(100);

}
*/
