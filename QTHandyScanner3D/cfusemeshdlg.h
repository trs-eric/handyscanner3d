#ifndef CFUSEMESHDLG_H
#define CFUSEMESHDLG_H

#include <QDialog>


class CGLWidget;
class CTriangularMesh;
class CFuseMeshThread ;

namespace Ui {
class CFuseMeshDlg;
}

class CFuseMeshDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CFuseMeshDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CFuseMeshDlg();

protected:
      void showEvent(QShowEvent * event);
private:
    Ui::CFuseMeshDlg *ui;


    CGLWidget *_glWidget;

    bool _in_process;
    bool _on_apply;
    bool _is_result_valid;
    CFuseMeshThread * _thread;

private slots:

    void onCancelBtn();
    void onPreviewBtn();
    void onApplyBtn();

    void onMakeProgressStep(int val);
    void onCompleteProcessing(CTriangularMesh * );
    void onStopProcessing();

    void close_dialog();


};

#endif // CFUSEMESHDLG_H
