#include "graphicutils.h"

const float C3DPoint::_ioresv[2] = {0,0};



void C3DPoint::load(std::ifstream &ifile)
{
    ifile.read(reinterpret_cast<char *>(&_angle), sizeof(float));

    ifile.read(reinterpret_cast<char *>(&_x), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&_y), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&_z), sizeof(float));

    ifile.read(reinterpret_cast<char *>(&_r), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&_g), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&_b), sizeof(float));

    ifile.read(reinterpret_cast<char *>(&_nx), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&_ny), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&_nz), sizeof(float));

    ifile.read(reinterpret_cast<char *>(&_scanline), sizeof(int));

    char buf[12];
    ifile.read(buf, sizeof(_ioresv));

    int face_count = 0;
    _faceidx.clear(); // just in case
    ifile.read(reinterpret_cast<char *>(&face_count), sizeof(int));
    for (int i =0; i <face_count ; i++)
    {
        int idx = 0;
        ifile.read(reinterpret_cast<char *>(&idx), sizeof(int));
        _faceidx.push_back(idx);
    }
}

void Vector2f::load(std::ifstream &ifile)
{
    ifile.read(reinterpret_cast<char *>(&x), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&y), sizeof(float));
}

void Vector2f::save(std::ofstream &ofile)
{
    ofile.write(reinterpret_cast<const char *>(&x), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&y), sizeof(float));
}


void Vector3f::load(std::ifstream &ifile)
{
    ifile.read(reinterpret_cast<char *>(&x), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&y), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&z), sizeof(float));

}

void Vector3f::save(std::ofstream &ofile)
{
    ofile.write(reinterpret_cast<const char *>(&x), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&y), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&z), sizeof(float));
}

CTriMesh::CTriMesh(): _texture(NULL),
    _tex_bb_maxy(0), _tex_bb_miny(0), _tex_init_angle(0)

{
    _trans.x = 0;
    _trans.y = 0;
    _trans.z = 0;

    _rotat.x = 0;
    _rotat.y = 0;
    _rotat.z = 0;

    _scale.x = 1;
    _scale.y = 1;
    _scale.z = 1;
}


void CTriMesh::replace_texture(QImage * tex)
{
    // check sizes tex
    if(_texture != NULL) delete _texture;
    _texture = new QImage(*tex);
}

void CTriMesh::remove_texture()
{
    if(_texture != NULL) delete _texture;
    _texture = NULL;
}

CTriMesh::~CTriMesh()
{
    if(_texture != NULL) delete _texture;
}

void CTriMesh::load(std::ifstream &ifile)
{
    _points.clear();
    int pcount = 0;
    ifile.read(reinterpret_cast<char *>(&pcount), sizeof(int));
    for (int i =0; i < pcount; i++)
    {
        C3DPoint p;
        p.load(ifile);
        _points.push_back(p);
    }

    _triangles.clear();
    int fcount = 0;
    ifile.read(reinterpret_cast<char *>(&fcount), sizeof(int));
    for (int j =0; j < fcount; j++)
    {
        C3DPointIdx idx;
        idx.load(ifile);
        _triangles.push_back(idx);
    }

    _tex_coords.clear();
    int tcount = 0;
    ifile.read(reinterpret_cast<char *>(&tcount), sizeof(int));
    for (int t =0; t < tcount; t++)
    {
        Vector2f tex;
        tex.load(ifile);
        _tex_coords.push_back(tex);
    }


    bool has_tex = false;
    ifile.read(reinterpret_cast<char *>(&has_tex), sizeof(bool));
    if(has_tex)
    {
        int tex_title_len = 0;
        ifile.read(reinterpret_cast<char *>(&tex_title_len), sizeof(int));

        // title buf
        char * tex_title_buf = new char [tex_title_len];
        ifile.read(tex_title_buf, tex_title_len);
        _texture_name = std::string(tex_title_buf,tex_title_len);
        delete [] tex_title_buf;

        /// image
        int twidth = 0;
        ifile.read(reinterpret_cast<char *>(&twidth), sizeof(int));

        int theight = 0;
        ifile.read(reinterpret_cast<char *>(&theight), sizeof(int));

        int format = 0;
        ifile.read(reinterpret_cast<char *>(&format), sizeof(int));

        int t_bcount = 0;
        ifile.read(reinterpret_cast<char *>(&t_bcount), sizeof(int));

        unsigned char * idata = new char [t_bcount];
        ifile.read(idata, t_bcount);

        QImage::Format iformat = format;
        _texture = new QImage(idata, twidth, theight, iformat);
    }
    ifile.read(reinterpret_cast<char *>(&_tex_bb_maxy), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&_tex_bb_miny), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&_tex_init_angle), sizeof(float));

    _mass_center.load(ifile);
    _trans.load(ifile);
    _scale.load(ifile);
    _rotat.load(ifile);
}

void CTriMesh::save(std::ofstream &ofile)
{
    int pcount = _points.size();
    ofile.write(reinterpret_cast<const char *>(&pcount ), sizeof(int));

    std::vector<C3DPoint>::iterator it=	_points.begin();
    for( ; it != _points.end(); it++)
        it->save(ofile);

    int fcount =  _triangles.size();
    ofile.write(reinterpret_cast<const char *>(&fcount ), sizeof(int));
    std::vector<C3DPointIdx>::iterator itf = _triangles.begin();
    for( ; itf != _triangles.end(); itf++)
        itf->save(ofile);

    int tcount = _tex_coords.size();
    ofile.write(reinterpret_cast<const char *>(&tcount ), sizeof(int));
    std::vector<Vector2f>::iterator itt = _tex_coords.begin();
    for( ; itt != _tex_coords.end(); itt++)
        itt->save(ofile);

    bool has_tex = (_texture != NULL)? true: false;
    ofile.write(reinterpret_cast<const char *>(&has_tex), sizeof(bool));
    if(has_tex)
    {
        /// texture name
        int tex_len = _texture_name.length();
        char * tex_title_buf = _texture_name.c_str();
        ofile.write(reinterpret_cast<const char *>(&tex_len), sizeof(int));
        ofile.write(tex_title_buf, tex_len);

        /// image
        int twidth = _texture->width();
        ofile.write(reinterpret_cast<const char *>(&twidth), sizeof(int));

        int theight = _texture->height();
        ofile.write(reinterpret_cast<const char *>(&theight), sizeof(int));

        int format = _texture->format();
        ofile.write(reinterpret_cast<const char *>(&format), sizeof(int));

        int t_bcount = _texture->byteCount();
        ofile.write(reinterpret_cast<const char *>(&t_bcount), sizeof(int));

        uchar * data = _texture->bits();
        ofile.write(data, t_bcount);
    }

    ofile.write(reinterpret_cast<const char *>(&_tex_bb_maxy), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_tex_bb_miny), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_tex_init_angle), sizeof(float));

    _mass_center.save(ofile);
    _trans.save(ofile);
    _scale.save(ofile);
    _rotat.save(ofile);

}


void CTriMesh::init_mass_center()
{

    Vector3f mc;

    float sz = _points.size();
    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
    {
        mc.x += it->_x;
        mc.y += it->_y;
        mc.z += it->_z;
    }

    _mass_center.x = mc.x/sz;
    _mass_center.y = mc.y/sz;
    _mass_center.z = mc.z/sz;

}


void CTriMesh::rotateX(float cos_alpha, float sin_alpha, float * x, float * y, float * z)
{
    //float x0 = *x;
    float y0 = *y;
    float z0 = *z;

    //*x =  cos_alpha * x0 + sin_alpha * z0;
    *y =  cos_alpha * y0 - sin_alpha * z0;
    *z =  sin_alpha * y0 + cos_alpha * z0;
}

void CTriMesh::rotateY(float cos_alpha, float sin_alpha, float * x, float * y, float * z)
{
    float x0 = *x;
    //float y0 = *y;
    float z0 = *z;

    *x =  cos_alpha * x0 + sin_alpha * z0;
    //*y = ;
    *z = -sin_alpha * x0 + cos_alpha * z0;
}

void CTriMesh::rotateZ(float cos_alpha, float sin_alpha, float * x, float * y, float * z)
{
    float x0 = *x;
    float y0 = *y;
    //float z0 = *z;

    *x = cos_alpha * x0 - sin_alpha * y0;
    *y = sin_alpha * x0 + cos_alpha * y0;
    //*z = -sin_alpha * x0 + cos_alpha * z0;
}

void CTriMesh::rotate_rev(float rx, float ry, float rz)
{
    float sin_rx = sin(rx);
    float cos_rx = cos(rx);

    float sin_ry = sin(ry);
    float cos_ry = cos(ry);

    float sin_rz = sin(rz);
    float cos_rz = cos(rz);

    int stz = _points.size();
    #pragma omp parallel for
    for( int i = 0 ; i < stz; i++)
    {

        rotateY(cos_ry, sin_ry, &(_points[i]._x), &(_points[i]._y), &(_points[i]._z));
        rotateY(cos_ry, sin_ry, &(_points[i]._nx), &(_points[i]._ny), &(_points[i]._nz));

        rotateZ(cos_rz, sin_rz, &(_points[i]._x), &(_points[i]._y), &(_points[i]._z));
        rotateZ(cos_rz, sin_rz, &(_points[i]._nx), &(_points[i]._ny), &(_points[i]._nz));

        rotateX(cos_rx, sin_rx, &(_points[i]._x), &(_points[i]._y), &(_points[i]._z));
        rotateX(cos_rx, sin_rx, &(_points[i]._nx), &(_points[i]._ny), &(_points[i]._nz));

    }


    std::vector<C3DPointIdx>::iterator itt = _triangles.begin();
    for(; itt != _triangles.end(); itt++)
    {
        rotateY(cos_ry, sin_ry, &(itt->tri_normal._x), &(itt->tri_normal._y), &(itt->tri_normal._z));
        rotateZ(cos_rz, sin_rz, &(itt->tri_normal._x), &(itt->tri_normal._y), &(itt->tri_normal._z));
        rotateX(cos_rx, sin_rx, &(itt->tri_normal._x), &(itt->tri_normal._y), &(itt->tri_normal._z));
    }

}

void CTriMesh::rotate(float rx, float ry, float rz)
{
    float sin_rx = sin(rx);
    float cos_rx = cos(rx);

    float sin_ry = sin(ry);
    float cos_ry = cos(ry);

    float sin_rz = sin(rz);
    float cos_rz = cos(rz);

    int stz = _points.size();
    #pragma omp parallel for
    for( int i = 0 ; i < stz; i++)
    {
        rotateX(cos_rx, sin_rx, &(_points[i]._x), &(_points[i]._y), &(_points[i]._z));
        rotateX(cos_rx, sin_rx, &(_points[i]._nx), &(_points[i]._ny), &(_points[i]._nz));

        rotateZ(cos_rz, sin_rz, &(_points[i]._x), &(_points[i]._y), &(_points[i]._z));
        rotateZ(cos_rz, sin_rz, &(_points[i]._nx), &(_points[i]._ny), &(_points[i]._nz));

        rotateY(cos_ry, sin_ry, &(_points[i]._x), &(_points[i]._y), &(_points[i]._z));
        rotateY(cos_ry, sin_ry, &(_points[i]._nx), &(_points[i]._ny), &(_points[i]._nz));

    }


    std::vector<C3DPointIdx>::iterator itt = _triangles.begin();
    for(; itt != _triangles.end(); itt++)
    {
        rotateX(cos_rx, sin_rx, &(itt->tri_normal._x), &(itt->tri_normal._y), &(itt->tri_normal._z));
        rotateZ(cos_rz, sin_rz, &(itt->tri_normal._x), &(itt->tri_normal._y), &(itt->tri_normal._z));
        rotateY(cos_ry, sin_ry, &(itt->tri_normal._x), &(itt->tri_normal._y), &(itt->tri_normal._z));
    }

}

void CTriMesh::scale(float sx, float sy, float sz)
{
    int stz = _points.size();

    #pragma omp parallel for
    for( int i = 0 ; i < stz; i++)
    {
        _points[i]._x *= sx;
        _points[i]._y *= sy;
        _points[i]._z *= sz;
    }
}

void CTriMesh::translate(float dx, float dy, float dz)
{
    int sz = _points.size();

    #pragma omp parallel for
    for( int i = 0 ; i < sz; i++)
    {
        _points[i]._x += dx;
        _points[i]._y += dy;
        _points[i]._z += dz;
    }
}

CTriMesh::CTriMesh(const CTriMesh & src)
{
    ///std::cout << "CTriMesh::CTriMesh" << std::endl;

    _points.clear();
    _points.insert(_points.begin(), src._points.begin(), src._points.end());

    _triangles.clear();
    _triangles.insert(_triangles.begin(), src._triangles.begin(), src._triangles.end());

    _tex_coords.clear();
    _tex_coords.insert(_tex_coords.begin(), src._tex_coords.begin(), src._tex_coords.end());


    if(src._texture)
        _texture = new QImage(*src._texture);
    else
        _texture = NULL;
    _texture_name = src._texture_name;

    _tex_bb_maxy = src._tex_bb_maxy;
    _tex_bb_miny = src._tex_bb_miny;
    _tex_init_angle = src._tex_init_angle;

    _mass_center.x = src._mass_center.x;
    _mass_center.y = src._mass_center.y;
    _mass_center.z = src._mass_center.z;

    _trans.x = src._trans.x ;
    _trans.y = src._trans.y ;
    _trans.z = src._trans.z ;

    _rotat.x = src._rotat.x ;
    _rotat.y = src._rotat.y ;
    _rotat.z = src._rotat.z ;

    _scale.x = src._scale.x ;
    _scale.y = src._scale.y ;
    _scale.z = src._scale.z ;
}

int rbuf[3];
void C3DPointIdx::load(std::ifstream &ifile)
{

    ifile.read(reinterpret_cast<char *>(&rbuf), 3*sizeof(int));
    pnt_index[0] = rbuf[0];
    pnt_index[1] = rbuf[1];
    pnt_index[2] = rbuf[2];

    ifile.read(reinterpret_cast<char *>(&rbuf), 3*sizeof(int));
    tex_index[0] = rbuf[0];
    tex_index[1] = rbuf[1];
    tex_index[2] = rbuf[2];

    tri_normal.load(ifile);
}

int wbuf[3];
void C3DPointIdx::save(std::ofstream &ofile)
{

    wbuf[0] = pnt_index[0];
    wbuf[1] = pnt_index[1];
    wbuf[2] = pnt_index[2];
    ofile.write(reinterpret_cast<const char *>(&wbuf), 3*sizeof(int));

    wbuf[0] = tex_index[0];
    wbuf[1] = tex_index[1];
    wbuf[2] = tex_index[2];
    ofile.write(reinterpret_cast<const char *>(&wbuf), 3*sizeof(int));

    tri_normal.save(ofile);
}

void C3DPoint::save(std::ofstream &ofile)
{
    ofile.write(reinterpret_cast<const char *>(&_angle), sizeof(float));

    ofile.write(reinterpret_cast<const char *>(&_x), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_y), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_z), sizeof(float));

    ofile.write(reinterpret_cast<const char *>(&_r), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_g), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_b), sizeof(float));

    ofile.write(reinterpret_cast<const char *>(&_nx), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_ny), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_nz), sizeof(float));

    ofile.write(reinterpret_cast<const char *>(&_scanline), sizeof(int));

    ofile.write(reinterpret_cast<const char *>(&_ioresv), sizeof(_ioresv));

    int face_num = _faceidx.size();
    ofile.write(reinterpret_cast<const char *>(&face_num), sizeof(int));

    std::vector<int>::iterator it = _faceidx.begin();
    for( ; it != _faceidx.end(); it++)
    {
        int idx = *it;
        ofile.write(reinterpret_cast<const char *>(&idx), sizeof(int));
    }
}

C3DPoint compute_avg_pnt(std::vector<C3DPoint> pnts)
{
    C3DPoint res;

    std::vector<C3DPoint>::iterator it = pnts.begin();
    for( ; it != pnts.end(); it++)
    {
        res._angle += it->_angle;

        res._x += it->_x;
        res._y += it->_y;
        res._z += it->_z;

        res._nx += it->_nx;
        res._ny += it->_ny;
        res._nz += it->_nz;

        res._r += it->_r;
        res._g += it->_g;
        res._b += it->_b;

    }

    int sz = pnts.size();

    res._angle /= sz;

    res._x /= sz;
    res._y /= sz;
    res._z /= sz;

    res._nx /= sz;
    res._ny /= sz;
    res._nz /= sz;

    res._r /= sz;
    res._g /= sz;
    res._b /= sz;

    return res;

}

C3DPoint compute_avg_pnt(C3DPoint & p1, C3DPoint & p2)
{
    C3DPoint res;
    res._angle = (p1._angle + p2._angle)/2;

    res._x = (p1._x + p2._x)/2;
    res._y = (p1._y + p2._y)/2;
    res._z = (p1._z + p2._z)/2;

    res._r = (p1._r + p2._r)/2;
    res._g = (p1._g + p2._g)/2;
    res._b = (p1._b + p2._b)/2;

    return res;


}


void compute_bounding_box(std::vector<C3DPoint> * pnts, C3DPoint * min, C3DPoint * max )
{
    *min = (*pnts)[0];
    *max = (*pnts)[0];
    std::vector<C3DPoint>::iterator it = pnts->begin() ;
    for( ; it != pnts->end(); it++)
    {
        if(min->_x > it->_x)
            min->_x = it->_x;
        if(min->_y > it->_y)
            min->_y = it->_y;
        if(min->_z > it->_z)
            min->_z = it->_z;

        if(max->_x < it->_x)
            max->_x = it->_x;
        if(max->_y < it->_y)
            max->_y = it->_y;
        if(max->_z < it->_z)
            max->_z = it->_z;
    }

}

void  make_bounding_box_sq(C3DPoint * min, C3DPoint * max)
{
    float dx = std::abs(max->_x - min->_x);
    float dy = std::abs(max->_y - min->_y);
    float dz = std::abs(max->_z - min->_z);

    float lside = 0;
    lside  = ((dx>dy)?dx:dy);
    float lside2 = (dz>lside)?dz:lside;

    max->_x = min->_x + lside2;
    max->_y = min->_y + lside2;
    max->_z = min->_z + lside2;
}

void expand_bounding_box(C3DPoint * min, C3DPoint * max, float factor)
{
    float dx = factor * (max->_x - min->_x)/2.0;
    float dy = factor * (max->_y - min->_y)/2.0;
    float dz = factor * (max->_z - min->_z)/2.0;

    min->_x -= dx;
    min->_y -= dy;
    min->_z -= dz;

    max->_x += dx;
    max->_y += dy;
    max->_z += dz;
}

void compensate_normal(C3DPoint & v, float r, C3DPoint & n)
{
    v._x -= r * n._x;
    v._y -= r * n._y;
    v._z -= r * n._z;
}


C3DPoint compute_vector_rotation(float alpha, float betta, C3DPoint & v)
{
    C3DPoint res;
    float sin_a = sin(alpha);
    float cos_a = cos(alpha);
    float sin_b = sin(betta);
    float cos_b = cos(betta);

    res._x = cos_a * v._x -  sin_a * cos_b * v._y + sin_a * sin_b * v._z;
    res._y = sin_a * v._x +  cos_a * cos_b * v._y - cos_a * sin_b * v._z;
    res._z =                         sin_b * v._y + cos_b * v._z;

    return res;
}

C3DPoint add_points(C3DPoint & v1, C3DPoint & v2)
{
    C3DPoint res(v1);
    res._x += v2._x;
    res._y += v2._y;
    res._z += v2._z;
    return res;
}

float compute_area(C3DPoint & p0, C3DPoint & p1, C3DPoint & p2)
{
    /// calculate vectors e1 and e2 (which can be of
    float e1_x = p1._x - p0._x;
    float e1_y = p1._y - p0._y;
    float e1_z = p1._z - p0._z;

    float e2_x = p2._x - p0._x;
    float e2_y = p2._y - p0._y;
    float e2_z = p2._z - p0._z;

    /// calculate e3 = e1 x e2 (cross product)
    float e3_x = e1_y * e2_z - e1_z * e2_y;
    float e3_y = e1_z * e2_x - e1_x * e2_z;
    float e3_z = e1_x * e2_y - e1_y * e2_x;

    /// the tria area is the half length of the

    return 0.5 * sqrt(e3_x * e3_x + e3_y * e3_y + e3_z * e3_z);
}

C3DPoint compute_normal(C3DPoint & p1, C3DPoint & p2, C3DPoint & p3)
{
    float v1x =  p1._x - p2._x;
    float v1y =  p1._y - p2._y;
    float v1z =  p1._z - p2._z;

    float v2x =  p3._x - p2._x;
    float v2y =  p3._y - p2._y;
    float v2z =  p3._z - p2._z;

    float xn = v1y * v2z - v2y * v1z;
    float yn = v1z * v2x - v2z * v1x;
    float zn = v1x * v2y - v2x * v1y;
    float ln = sqrt(xn*xn + yn*yn + zn*zn );

    if(ln== 0)
        return C3DPoint(0.0,1.0,0.0);

    xn /= ln;
    yn /= ln;
    zn /= ln;

    return C3DPoint(xn,yn,zn);
}

void normalize3(float & x, float & y, float & z)
{
    float ln = sqrt(x*x + y*y + z*z );
    if(ln== 0)
        return;

    x /= ln;
    y /= ln;
    z /= ln;
}


CArray2D<double> convert_RGB_to_gray(QImage * img) /// CHANGED
{
    int xs = img->width();
    int ys = img->height();

    CArray2D<double> res(xs, ys, 0);


    for(int j = 0; j < ys; j++)
    {
        for(int i = 0; i < xs; i++)
        {
            QRgb pix = img->pixel(i,j);

            double r =   qRed(pix);
            double g = qGreen(pix);
            double b =  qBlue(pix);

            res._data[j][i] =  0.2989 * r +  0.5870 * g + 0.1140 * b; // //
        }
    }
    return res;
}



CArray2D<double> convert_RGB_to_gray_suppress_non_red(QImage * img, double thresh)
{
    int xs = img->width();
    int ys = img->height();

    CArray2D<double> res(xs, ys, 0);


    for(int j = 0; j < ys; j++)
    {
        for(int i = 0; i < xs; i++)
        {
            QRgb pix = img->pixel(i,j);

            double r =   qRed(pix);
            double g = qGreen(pix);
            double b =  qBlue(pix);

            if(r > thresh)
                res._data[j][i] =  0.2989 * r +  0.5870 * g + 0.1140 * b; // //
            else
                res._data[j][i] =  0;
        }
    }
    return res;
}

float compute_sq_dist(C3DPoint & p1, C3DPoint & p2)
{
    float v1x =  p1._x - p2._x;
    float v1y =  p1._y - p2._y;
    float v1z =  p1._z - p2._z;

    return (v1x * v1x + v1y * v1y + v1z * v1z );

}


// for input angle is converted to [0, 2pi]
float circular_modulo(float angle)
{
    if(angle > cPI2)
        return (angle - cPI2);
    if(angle < 0)
        return (cPI2 + angle);
    return angle;
}


void  aux_pix_data::load(std::ifstream &ifile)
{
    ifile.read(reinterpret_cast<char *>(&_u), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&_v), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&_idx), sizeof(int));
    ifile.read(reinterpret_cast<char *>(&_langle), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&_pidx), sizeof(int));

    ifile.read(reinterpret_cast<char *>(&_x), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&_y), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&_z), sizeof(float));

    ifile.read(reinterpret_cast<char *>(&_bx), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&_by), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&_bz), sizeof(float));

    ifile.read(reinterpret_cast<char *>(&_gx), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&_gy), sizeof(float));
    ifile.read(reinterpret_cast<char *>(&_gz), sizeof(float));
}


void aux_pix_data::save(std::ofstream &ofile)
{
    ofile.write(reinterpret_cast<const char *>(&_u), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_v), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_idx), sizeof(int));
    ofile.write(reinterpret_cast<const char *>(&_langle), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_pidx), sizeof(int));

    ofile.write(reinterpret_cast<const char *>(&_x), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_y), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_z), sizeof(float));

    ofile.write(reinterpret_cast<const char *>(&_bx), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_by), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_bz), sizeof(float));

    ofile.write(reinterpret_cast<const char *>(&_gx), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_gy), sizeof(float));
    ofile.write(reinterpret_cast<const char *>(&_gz), sizeof(float));
}

