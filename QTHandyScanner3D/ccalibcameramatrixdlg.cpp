#include "ccalibcameramatrixdlg.h"
#include "ui_ccalibcameramatrixdlg.h"
#include <iostream>
#include <QTimer>
#include <QMessageBox>
#include <QThread>

#include "settings/psettings.h"

CCalibCameraMatrixDlg::CCalibCameraMatrixDlg(cv::VideoCapture * cam, PVOID dh, QWidget *parent) :
    QDialog(parent),  _current_cam(cam), _dev_handle(dh),
    ui(new Ui::CCalibCameraMatrixDlg)
{
    ui->setupUi(this);

    connect(ui->cancelBtn, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->btnStart, SIGNAL(clicked()), this, SLOT(start_calib()));

    ui->cmbMode->addItem("50 guided photos",      1);
    ui->cmbMode->addItem("100 guided photos",     2);
    ui->cmbMode->addItem("100 unguided photos",   3);
    ui->cmbMode->setCurrentIndex(0);
}

CCalibCameraMatrixDlg::~CCalibCameraMatrixDlg()
{
    delete ui;
}

void CCalibCameraMatrixDlg::init()
{

    if( ui->cmbMode->currentData().toInt() == 1)
    {
        init_frame_series_50();
        _max_frame_check =  _frame_corners.size()/4;
        _mode = 1;
    }
    else if(ui->cmbMode->currentData().toInt() == 2)
    {
        init_frame_series_100();
        _max_frame_check =  _frame_corners.size()/4;
        _mode = 2;
    }
    else
    {
        _max_frame_check = 100;
        _mode = 3;
    }
    _captured_frames = 0;

    _boardSize.width  = g_marks_x ;
    _boardSize.height = g_marks_y ;
    _cell_size = g_single_cell_width ; // mm

    _delay = 500; // msec

    _max_reproj_error = 0.7;

    ui->progressBar->setValue(0);
    ui->progressBar->setMaximum(_max_frame_check);

    _all_img_pnts.clear();
}

void CCalibCameraMatrixDlg::hideEvent(QHideEvent * event)
{
    ui->btnStart->setEnabled(true);
    ui->cmbMode->setEnabled(true);

    emit start_main_preview();
}

void CCalibCameraMatrixDlg::start_calib()
{

    ui->btnStart->setEnabled(false);
    ui->cmbMode->setEnabled(false);


    init();

    emit stop_main_preview();

    single_timer_shoot();

}

void CCalibCameraMatrixDlg::showEvent(QShowEvent * event)
{
}




void CCalibCameraMatrixDlg::draw_frame_helper(cv::Mat & frame)
{
    int cidx = _captured_frames;

    cv::line(frame, _frame_corners[cidx * 4    ], _frame_corners[cidx * 4 + 1] , cv::Scalar(0,255,0),2);
    cv::line(frame, _frame_corners[cidx * 4 + 1], _frame_corners[cidx * 4 + 3] , cv::Scalar(0,255,0),2);
    cv::line(frame, _frame_corners[cidx * 4 + 2], _frame_corners[cidx * 4 + 3] , cv::Scalar(0,255,0),2);
    cv::line(frame, _frame_corners[cidx * 4  ], _frame_corners[cidx * 4  + 2] , cv::Scalar(0,255,0), 2);


    cv::circle(frame, _frame_corners[cidx * 4    ], 10 , cv::Scalar(0,0,255), -1);
    cv::circle(frame, _frame_corners[cidx * 4 +1], 10 , cv::Scalar(0,0,255), -1);
    cv::circle(frame, _frame_corners[cidx * 4 +2], 10 , cv::Scalar(0,0,255), -1);
    cv::circle(frame, _frame_corners[cidx * 4 +3], 10, cv::Scalar(0,0,255), -1);

}

void CDotsExtration::start_thread(cv::Mat fr, cv::Size sz)
{


    fr.copyTo(_frame);
    _boardSize = sz;

    start();
}

void CDotsExtration::run()
{
    _found = cv::findCirclesGrid( _frame, _boardSize, _points );
}




void CCalibCameraMatrixDlg::capture_and_proc_new_frame()
{
    if(_mode == 3)
        QThread::msleep(_delay);

    cv::Mat frame;
    bool bSuccess =  _current_cam->read(frame);
    if (!bSuccess)  return single_timer_shoot();

    cv::Mat aux_frame;

    frame.copyTo(aux_frame);
    if((_mode == 1) || (_mode == 2))
        draw_frame_helper(aux_frame);

    _frame_pix_size = frame.size();

    std::vector<cv::Point2f> bPoints;
    bool found = false;

    CDotsExtration thr;
    thr.start_thread(frame, _boardSize);
    if ( thr.wait(1000) )
    {
        found = thr._found;
        bPoints = thr._points;
    }
    else
    {
        found = false;
        thr.terminate();
    }

    //// std::vector<cv::Point2f> bPoints;
    //// bool found = cv::findCirclesGrid( frame, _boardSize, bPoints );

    float d_threshold = 50;

    if(found)
    {
        if((_mode == 1) || (_mode == 2))
        {
            int cidx = _captured_frames;
            cv::Point2f p0 = _frame_corners[cidx * 4];
            cv::Point2f p1 = _frame_corners[cidx * 4 + 1];
            cv::Point2f p2 = _frame_corners[cidx * 4 + 2];
            cv::Point2f p3 = _frame_corners[cidx * 4 + 3];


            double d0 = cv::norm(bPoints[0]-p0);
            double d1 = cv::norm(bPoints[g_marks_x-1]-p1);
            double d2 = cv::norm(bPoints[g_marks_x*(g_marks_y-1)] -p2);
            double d3 = cv::norm(bPoints[g_marks_x*g_marks_y-1] -p3);


             if((d0 < d_threshold) && (d1 < d_threshold) && (d2 < d_threshold) && (d3 < d_threshold) )
            {

                 _all_img_pnts.push_back(bPoints);
                _captured_frames ++;
                ui->progressBar->setValue(_captured_frames);

                QString num_str = QString("%1/%2").arg(QString::number(_captured_frames)).arg(QString::number(_max_frame_check));
                ui->lblFrame->setText(num_str);

                /// QString nm = QString("calib_0%1.jpg").arg(_captured_frames, 3, 10, QLatin1Char('0'));
                /// cv::imwrite(nm.toStdString(), frame);
            }
        }

        if(_mode == 3)
        {
            _all_img_pnts.push_back(bPoints);
            _captured_frames ++;
             ui->progressBar->setValue(_captured_frames);

             QString num_str = QString("%1/%2").arg(QString::number(_captured_frames)).arg(QString::number(_max_frame_check));
             ui->lblFrame->setText(num_str);

            drawProjectedArea(aux_frame);
        }

        cv::drawChessboardCorners(aux_frame, _boardSize, cv::Mat(bPoints), found );


    }
    emit update_main_preview(aux_frame);

    if(_all_img_pnts.size() >= _max_frame_check)
        return callibrate_and_save();

    single_timer_shoot();
}


void CCalibCameraMatrixDlg::callibrate_and_save()
{

    std::vector<std::vector<cv::Point3f> > objectPoints;
    std::vector<cv::Point3f> corners;
    for( int i = 0; i < _boardSize.height; ++i )
        for( int j = 0; j < _boardSize.width; ++j )
            corners.push_back(cv::Point3f(float( j*_cell_size ), float( i*_cell_size ), 0));

    objectPoints.resize(_all_img_pnts.size(), corners);


    cv::Mat cameraMatrix;
    cv::Mat distCoeffs;
    std::vector<cv::Mat> rvecs, tvecs;
    double reproj_error = calibrateCamera(objectPoints, _all_img_pnts, _frame_pix_size, cameraMatrix,
                                    distCoeffs, rvecs, tvecs,  CV_CALIB_FIX_K3 |  CV_CALIB_FIX_K4 |CV_CALIB_FIX_K5,
                                          cvTermCriteria(  CV_TERMCRIT_EPS,100,0.001));


    bool ok = cv::checkRange(cameraMatrix) && cv::checkRange(distCoeffs);

    reproj_error *= 10000;
    int i_reproj_error = reproj_error;
    reproj_error = i_reproj_error / 10000.;
    std::cout << "Re-projection error reported by calibrateCamera: "<< reproj_error << std::endl;

    std::cout << cameraMatrix << " " << distCoeffs << std::endl;
    if(ok && (reproj_error < _max_reproj_error)  )
    {
        QString msg = QString("Callibration succeded with focus %2 and <br> rep-error %1 px using %3 images").arg(QString::number(reproj_error)).arg(QString::number(CGlobalSettings::_webc_focus)).arg(QString::number(_captured_frames));
        QMessageBox::information(this, tr("Information"), msg);
        g_callibration_map.set_callibration_data(CGlobalSettings::_webc_focus, cameraMatrix, distCoeffs);
        close();
    }
    else
    {
        QString msg = QString("Callibration failed (rep-error = %1 px). Start over?").arg(QString::number(reproj_error));


        QMessageBox::StandardButton reply = QMessageBox::question(this, "Warning", msg,
                                        QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::No)
            close();
        else
            return start_calib();

    }


}

void CCalibCameraMatrixDlg::single_timer_shoot()
{
    QTimer::singleShot(100, this, SLOT(capture_and_proc_new_frame()));
}

void CCalibCameraMatrixDlg::drawProjectedArea(cv::Mat & frame)
{
    cv::Mat dest;

    frame.copyTo(dest );
    int l0 = 0;
    int l1 = _boardSize.width -1;
    int l2 = _boardSize.width * _boardSize.height - 1;
    int l3= _boardSize.width * (_boardSize.height- 1) ;

    std::vector<std::vector<cv::Point2f> >::iterator it = _all_img_pnts.begin();
    for( int c = 0 ; it != _all_img_pnts.end(); it++, c++)
    {

       std::vector<cv::Point> ppt;
       ppt.push_back((*it)[l0]);
       ppt.push_back((*it)[l1]);
       ppt.push_back((*it)[l2]);
       ppt.push_back((*it)[l3]);

      cv::Point* conts[1]  = { &ppt[0] };
       int npnts  = 4;
       cv::fillPoly(dest, conts, &npnts, 1, cv::Scalar(255, 255, 0));

        std::vector<cv::Point2f>::iterator itt = it->begin();
        for( ; itt != it->end(); itt++ )
            cv::circle(dest, *itt, 2, cv::Scalar(0, 128, 0), 2);
    }

    float opacity = 0.2;
    cv::addWeighted(dest, opacity, frame, 1 - opacity, 0, frame);
}

void  CCalibCameraMatrixDlg::init_frame_series_100()
{


    // left test OK
    // processing ... calib_0002.jpg
    _frame_corners.push_back(cv::Point2f(544.64, 464.874));
    _frame_corners.push_back(cv::Point2f(983.757, 457.586));
    _frame_corners.push_back(cv::Point2f(536.747, 880.982));
    _frame_corners.push_back(cv::Point2f(1010.63, 881.942));

    // left test OK
    // processing ... calib_0003.jpg
    _frame_corners.push_back(cv::Point2f(265.075, 402.438));
    _frame_corners.push_back(cv::Point2f(789.448, 377.126));
    _frame_corners.push_back(cv::Point2f(278.94, 908.639));
    _frame_corners.push_back(cv::Point2f(823.358, 851.1));

    // left test OK
    // processing ... calib_0004.jpg
    _frame_corners.push_back(cv::Point2f(108.368, 339.141));
    _frame_corners.push_back(cv::Point2f(705.574, 334));
    _frame_corners.push_back(cv::Point2f(111.174, 910.654));
    _frame_corners.push_back(cv::Point2f(727.881, 857.594));

    // left test OK
    // processing ... calib_0005.jpg
    _frame_corners.push_back(cv::Point2f(65.0177, 338.806));
    _frame_corners.push_back(cv::Point2f(669.658, 322.587));
    _frame_corners.push_back(cv::Point2f(81.6936, 911.28));
    _frame_corners.push_back(cv::Point2f(699.729, 846.601));

    // left test OK
    // processing ... calib_0006.jpg
    _frame_corners.push_back(cv::Point2f(48.1741, 300.96));
    _frame_corners.push_back(cv::Point2f(660.78, 300.576));
    _frame_corners.push_back(cv::Point2f(56.5751, 876.582));
    _frame_corners.push_back(cv::Point2f(675.95, 825.644));

    // left test OK
    // processing ... calib_0007.jpg
    _frame_corners.push_back(cv::Point2f(25.6125, 316.051));
    _frame_corners.push_back(cv::Point2f(635.84, 317.452));
    _frame_corners.push_back(cv::Point2f(36.2815, 889.485));
    _frame_corners.push_back(cv::Point2f(647.848, 830.889));

    // left test OK
    // processing ... calib_0008.jpg
    // test failed
    // processing ... calib_0009.jpg
    _frame_corners.push_back(cv::Point2f(94.2459, 375.32));
    _frame_corners.push_back(cv::Point2f(597.812, 372.188));
    _frame_corners.push_back(cv::Point2f(124.386, 928.261));
    _frame_corners.push_back(cv::Point2f(614.865, 798.131));

    // left test OK
    // processing ... calib_0010.jpg
    _frame_corners.push_back(cv::Point2f(103.323, 363.015));
    _frame_corners.push_back(cv::Point2f(604.472, 368.317));
    _frame_corners.push_back(cv::Point2f(125.797, 918.298));
    _frame_corners.push_back(cv::Point2f(616.898, 795.18));

    // left test OK
    // processing ... calib_0011.jpg
    _frame_corners.push_back(cv::Point2f(99.0311, 357.275));
    _frame_corners.push_back(cv::Point2f(605.95, 367.817));
    _frame_corners.push_back(cv::Point2f(115.658, 909.461));
    _frame_corners.push_back(cv::Point2f(615.341, 796.875));

    // left test OK
    // processing ... calib_0012.jpg
    _frame_corners.push_back(cv::Point2f(113.365, 358.271));
    _frame_corners.push_back(cv::Point2f(637.057, 369.356));
    _frame_corners.push_back(cv::Point2f(124.702, 900.228));
    _frame_corners.push_back(cv::Point2f(643.919, 808.347));

    // left test OK
    // processing ... calib_0013.jpg
    _frame_corners.push_back(cv::Point2f(101.302, 346.901));
    _frame_corners.push_back(cv::Point2f(645.58, 342.532));
    _frame_corners.push_back(cv::Point2f(125.541, 879.369));
    _frame_corners.push_back(cv::Point2f(667.442, 797.443));

    // left test OK
    // processing ... calib_0014.jpg
    _frame_corners.push_back(cv::Point2f(127.496, 347.816));
    _frame_corners.push_back(cv::Point2f(674.168, 336.219));
    _frame_corners.push_back(cv::Point2f(152.236, 874.174));
    _frame_corners.push_back(cv::Point2f(701.702, 802.367));

    // left test OK
    // processing ... calib_0015.jpg
    _frame_corners.push_back(cv::Point2f(154.96, 354.594));
    _frame_corners.push_back(cv::Point2f(703.515, 333.425));
    _frame_corners.push_back(cv::Point2f(183.805, 875.303));
    _frame_corners.push_back(cv::Point2f(735.939, 808.617));

    // left test OK
    // processing ... calib_0016.jpg
    _frame_corners.push_back(cv::Point2f(206.531, 347.012));
    _frame_corners.push_back(cv::Point2f(758.053, 320.53));
    _frame_corners.push_back(cv::Point2f(233.129, 863.424));
    _frame_corners.push_back(cv::Point2f(789.618, 809.965));

    // left test OK
    // processing ... calib_0017.jpg
    _frame_corners.push_back(cv::Point2f(235.252, 333.054));
    _frame_corners.push_back(cv::Point2f(790.341, 303.876));
    _frame_corners.push_back(cv::Point2f(261.383, 846.298));
    _frame_corners.push_back(cv::Point2f(820.689, 806.9));

    // left test OK
    // processing ... calib_0018.jpg
    _frame_corners.push_back(cv::Point2f(284.071, 326.522));
    _frame_corners.push_back(cv::Point2f(843.04, 293.054));
    _frame_corners.push_back(cv::Point2f(309.334, 835.699));
    _frame_corners.push_back(cv::Point2f(871.859, 811.037));

    // left test OK
    // processing ... calib_0019.jpg
    _frame_corners.push_back(cv::Point2f(327.663, 308.13));
    _frame_corners.push_back(cv::Point2f(893.824, 278.904));
    _frame_corners.push_back(cv::Point2f(342.138, 816.522));
    _frame_corners.push_back(cv::Point2f(911.661, 816.337));

    // left test OK
    // processing ... calib_0020.jpg
    _frame_corners.push_back(cv::Point2f(370.687, 312.411));
    _frame_corners.push_back(cv::Point2f(941.731, 279));
    _frame_corners.push_back(cv::Point2f(381.916, 821.21));
    _frame_corners.push_back(cv::Point2f(958.987, 835.859));

    // left test OK
    // processing ... calib_0021.jpg
    _frame_corners.push_back(cv::Point2f(429.139, 304.994));
    _frame_corners.push_back(cv::Point2f(1007.71, 273.465));
    _frame_corners.push_back(cv::Point2f(430.886, 812.483));
    _frame_corners.push_back(cv::Point2f(1013.96, 852.333));

    // left test OK
    // processing ... calib_0022.jpg
    _frame_corners.push_back(cv::Point2f(487.685, 303.386));
    _frame_corners.push_back(cv::Point2f(1075.2, 266.501));
    _frame_corners.push_back(cv::Point2f(490.721, 810.695));
    _frame_corners.push_back(cv::Point2f(1082.93, 864.006));

    // left test OK
    // processing ... calib_0023.jpg
    _frame_corners.push_back(cv::Point2f(517.238, 305.018));
    _frame_corners.push_back(cv::Point2f(1111.99, 258));
    _frame_corners.push_back(cv::Point2f(526.103, 811.107));
    _frame_corners.push_back(cv::Point2f(1123.96, 864.022));

    // left test OK
    // processing ... calib_0024.jpg
    _frame_corners.push_back(cv::Point2f(556.387, 294.921));
    _frame_corners.push_back(cv::Point2f(1163.43, 235.469));
    _frame_corners.push_back(cv::Point2f(572.719, 802.696));
    _frame_corners.push_back(cv::Point2f(1181.17, 854.152));

    // left test OK
    // processing ... calib_0025.jpg
    _frame_corners.push_back(cv::Point2f(590.958, 270.945));
    _frame_corners.push_back(cv::Point2f(1212.26, 201.612));
    _frame_corners.push_back(cv::Point2f(614.366, 777.826));
    _frame_corners.push_back(cv::Point2f(1230.79, 827.038));

    // left test OK
    // processing ... calib_0026.jpg
    _frame_corners.push_back(cv::Point2f(601.01, 227.262));
    _frame_corners.push_back(cv::Point2f(1235.89, 152.337));
    _frame_corners.push_back(cv::Point2f(628.782, 737.304));
    _frame_corners.push_back(cv::Point2f(1248.89, 782.483));

    // left test OK
    // processing ... calib_0027.jpg
    _frame_corners.push_back(cv::Point2f(580.487, 112.929));
    _frame_corners.push_back(cv::Point2f(1267.18, 54.4992));
    _frame_corners.push_back(cv::Point2f(609.909, 643.235));
    _frame_corners.push_back(cv::Point2f(1230.77, 697.34));

    // left test OK
    // processing ... calib_0028.jpg
    _frame_corners.push_back(cv::Point2f(542.566, 106.429));
    _frame_corners.push_back(cv::Point2f(1223.11, 58.7777));
    _frame_corners.push_back(cv::Point2f(571.224, 638.592));
    _frame_corners.push_back(cv::Point2f(1183.18, 691.212));

    // left test OK
    // processing ... calib_0029.jpg
    _frame_corners.push_back(cv::Point2f(460.575, 84.4288));
    _frame_corners.push_back(cv::Point2f(1118.58, 45.5441));
    _frame_corners.push_back(cv::Point2f(490.804, 615.064));
    _frame_corners.push_back(cv::Point2f(1081.79, 659.129));

    // left test OK
    // processing ... calib_0030.jpg
    _frame_corners.push_back(cv::Point2f(402.772, 98.4006));
    _frame_corners.push_back(cv::Point2f(1045.14, 62.9854));
    _frame_corners.push_back(cv::Point2f(440.45, 626.161));
    _frame_corners.push_back(cv::Point2f(1019.59, 653.312));

    // left test OK
    // processing ... calib_0031.jpg
    _frame_corners.push_back(cv::Point2f(356.686, 97.6067));
    _frame_corners.push_back(cv::Point2f(1003.45, 57.6438));
    _frame_corners.push_back(cv::Point2f(406.217, 631.462));
    _frame_corners.push_back(cv::Point2f(987.3, 644.969));

    // left test OK
    // processing ... calib_0032.jpg
    _frame_corners.push_back(cv::Point2f(278.538, 98.5216));
    _frame_corners.push_back(cv::Point2f(920.245, 55.7437));
    _frame_corners.push_back(cv::Point2f(339.314, 636.071));
    _frame_corners.push_back(cv::Point2f(916.014, 633.385));

    // left test OK
    // processing ... calib_0033.jpg
    _frame_corners.push_back(cv::Point2f(217.217, 85.3875));
    _frame_corners.push_back(cv::Point2f(865.289, 41.0945));
    _frame_corners.push_back(cv::Point2f(288.21, 631.099));
    _frame_corners.push_back(cv::Point2f(867.162, 618.172));

    // left test OK
    // processing ... calib_0034.jpg
    _frame_corners.push_back(cv::Point2f(153.86, 74.1495));
    _frame_corners.push_back(cv::Point2f(810.662, 34.1841));
    _frame_corners.push_back(cv::Point2f(231.365, 629.911));
    _frame_corners.push_back(cv::Point2f(816.246, 610.005));

    // left test OK
    // processing ... calib_0035.jpg
    _frame_corners.push_back(cv::Point2f(106.64, 81.5606));
    _frame_corners.push_back(cv::Point2f(768.707, 46.9945));
    _frame_corners.push_back(cv::Point2f(185.805, 646.421));
    _frame_corners.push_back(cv::Point2f(777.275, 618.265));

    // left test OK
    // processing ... calib_0036.jpg
    _frame_corners.push_back(cv::Point2f(75.1784, 95.0401));
    _frame_corners.push_back(cv::Point2f(748.024, 72.0953));
    _frame_corners.push_back(cv::Point2f(148.807, 673.246));
    _frame_corners.push_back(cv::Point2f(752.899, 638.157));

    // left test OK
    // processing ... calib_0037.jpg
    _frame_corners.push_back(cv::Point2f(65.7111, 121.061));
    _frame_corners.push_back(cv::Point2f(689.99, 111.018));
    _frame_corners.push_back(cv::Point2f(127.62, 675.225));
    _frame_corners.push_back(cv::Point2f(698.012, 622.335));

    // left test OK
    // processing ... calib_0038.jpg
    _frame_corners.push_back(cv::Point2f(76.775, 135.545));
    _frame_corners.push_back(cv::Point2f(671.019, 139.911));
    _frame_corners.push_back(cv::Point2f(127.568, 678.067));
    _frame_corners.push_back(cv::Point2f(671.688, 615.342));

    // left test OK
    // processing ... calib_0039.jpg
    _frame_corners.push_back(cv::Point2f(107.061, 149.703));
    _frame_corners.push_back(cv::Point2f(658.945, 168.932));
    _frame_corners.push_back(cv::Point2f(144.726, 692.101));
    _frame_corners.push_back(cv::Point2f(659.57, 616.34));

    // left test OK
    // processing ... calib_0040.jpg
    _frame_corners.push_back(cv::Point2f(151.942, 182.02));
    _frame_corners.push_back(cv::Point2f(670.58, 208.578));
    _frame_corners.push_back(cv::Point2f(180.361, 727.64));
    _frame_corners.push_back(cv::Point2f(673.96, 644.059));

    // left test OK
    // processing ... calib_0041.jpg
    _frame_corners.push_back(cv::Point2f(156.734, 210.766));
    _frame_corners.push_back(cv::Point2f(652.022, 231.293));
    _frame_corners.push_back(cv::Point2f(195.116, 761.155));
    _frame_corners.push_back(cv::Point2f(669.639, 657.89));

    // left test OK
    // processing ... calib_0042.jpg
    _frame_corners.push_back(cv::Point2f(139.603, 222.476));
    _frame_corners.push_back(cv::Point2f(635.328, 234.755));
    _frame_corners.push_back(cv::Point2f(190.039, 776.928));
    _frame_corners.push_back(cv::Point2f(662.985, 661.088));

    // left test OK
    // processing ... calib_0043.jpg
    _frame_corners.push_back(cv::Point2f(161.291, 209.793));
    _frame_corners.push_back(cv::Point2f(665.122, 219.756));
    _frame_corners.push_back(cv::Point2f(207.528, 759.693));
    _frame_corners.push_back(cv::Point2f(682.77, 649.42));

    // left test OK
    // processing ... calib_0044.jpg
    _frame_corners.push_back(cv::Point2f(213.894, 195.2));
    _frame_corners.push_back(cv::Point2f(717.729, 203.416));
    _frame_corners.push_back(cv::Point2f(260.647, 752.613));
    _frame_corners.push_back(cv::Point2f(732.924, 640.52));

    // left test OK
    // processing ... calib_0045.jpg
    _frame_corners.push_back(cv::Point2f(228.338, 178.561));
    _frame_corners.push_back(cv::Point2f(740.948, 185.507));
    _frame_corners.push_back(cv::Point2f(270.24, 731.441));
    _frame_corners.push_back(cv::Point2f(745.493, 625.337));

    // left test OK
    // processing ... calib_0046.jpg
    _frame_corners.push_back(cv::Point2f(220.661, 139.711));
    _frame_corners.push_back(cv::Point2f(768.807, 130.397));
    _frame_corners.push_back(cv::Point2f(274.133, 678.754));
    _frame_corners.push_back(cv::Point2f(759.477, 578.678));

    // left test OK
    // processing ... calib_0047.jpg
    _frame_corners.push_back(cv::Point2f(212.271, 125.427));
    _frame_corners.push_back(cv::Point2f(788.136, 91.7362));
    _frame_corners.push_back(cv::Point2f(286.771, 650.443));
    _frame_corners.push_back(cv::Point2f(777.737, 548.173));

    // left test OK
    // processing ... calib_0048.jpg
    _frame_corners.push_back(cv::Point2f(219.347, 124.187));
    _frame_corners.push_back(cv::Point2f(819.595, 71.2636));
    _frame_corners.push_back(cv::Point2f(309.633, 640.068));
    _frame_corners.push_back(cv::Point2f(807.524, 536.184));

    // left test OK
    // processing ... calib_0049.jpg
    _frame_corners.push_back(cv::Point2f(250.737, 151.48));
    _frame_corners.push_back(cv::Point2f(869.097, 81.1183));
    _frame_corners.push_back(cv::Point2f(350.838, 649.577));
    _frame_corners.push_back(cv::Point2f(852.048, 547.509));

    // left test OK
    // processing ... calib_0050.jpg
    _frame_corners.push_back(cv::Point2f(278.446, 160.112));
    _frame_corners.push_back(cv::Point2f(904.754, 83.8842));
    _frame_corners.push_back(cv::Point2f(381.771, 634.43));
    _frame_corners.push_back(cv::Point2f(882.291, 546.656));

    // left test OK
    // processing ... calib_0051.jpg
    _frame_corners.push_back(cv::Point2f(335.83, 138.291));
    _frame_corners.push_back(cv::Point2f(976.359, 82.539));
    _frame_corners.push_back(cv::Point2f(430.562, 602.221));
    _frame_corners.push_back(cv::Point2f(940.189, 555.061));

    // left test OK
    // processing ... calib_0052.jpg
    _frame_corners.push_back(cv::Point2f(361.671, 112.749));
    _frame_corners.push_back(cv::Point2f(1029.93, 66.2654));
    _frame_corners.push_back(cv::Point2f(459.066, 575.2));
    _frame_corners.push_back(cv::Point2f(981.885, 555.654));

    // left test OK
    // processing ... calib_0053.jpg
    _frame_corners.push_back(cv::Point2f(419.38, 82.1278));
    _frame_corners.push_back(cv::Point2f(1113.18, 70.5701));
    _frame_corners.push_back(cv::Point2f(502.649, 543.23));
    _frame_corners.push_back(cv::Point2f(1034.69, 571.312));

    // left test OK
    // processing ... calib_0054.jpg
    _frame_corners.push_back(cv::Point2f(486.091, 56.0417));
    _frame_corners.push_back(cv::Point2f(1200.59, 69.7484));
    _frame_corners.push_back(cv::Point2f(557.075, 524.534));
    _frame_corners.push_back(cv::Point2f(1100.71, 596.762));

    // left test OK
    // processing ... calib_0055.jpg
    _frame_corners.push_back(cv::Point2f(550.858, 49.5924));
    _frame_corners.push_back(cv::Point2f(1263.49, 105.641));
    _frame_corners.push_back(cv::Point2f(596.259, 523.344));
    _frame_corners.push_back(cv::Point2f(1137.51, 653.055));

    // left test OK
    // processing ... calib_0056.jpg
    _frame_corners.push_back(cv::Point2f(525.936, 243.166));
    _frame_corners.push_back(cv::Point2f(1125.67, 141.583));
    _frame_corners.push_back(cv::Point2f(601.6, 721.338));
    _frame_corners.push_back(cv::Point2f(1169.55, 736.569));

    // left test OK
    // processing ... calib_0057.jpg
    _frame_corners.push_back(cv::Point2f(476.329, 236.648));
    _frame_corners.push_back(cv::Point2f(1046.39, 143.549));
    _frame_corners.push_back(cv::Point2f(552.591, 705.798));
    _frame_corners.push_back(cv::Point2f(1092.16, 737.766));

    // left test OK
    // processing ... calib_0058.jpg
    _frame_corners.push_back(cv::Point2f(530.477, 247.202));
    _frame_corners.push_back(cv::Point2f(1057.94, 164.338));
    _frame_corners.push_back(cv::Point2f(587.983, 698.477));
    _frame_corners.push_back(cv::Point2f(1096.76, 758.403));

    // left test OK
    // processing ... calib_0059.jpg
    _frame_corners.push_back(cv::Point2f(618.249, 266.342));
    _frame_corners.push_back(cv::Point2f(1177.91, 211.875));
    _frame_corners.push_back(cv::Point2f(647.583, 726.131));
    _frame_corners.push_back(cv::Point2f(1185.05, 818.015));

    // left test OK
    // processing ... calib_0060.jpg
    _frame_corners.push_back(cv::Point2f(598.883, 313.336));
    _frame_corners.push_back(cv::Point2f(1165.6, 238.39));
    _frame_corners.push_back(cv::Point2f(612.93, 796.09));
    _frame_corners.push_back(cv::Point2f(1200.43, 830.469));

    // left test OK
    // processing ... calib_0061.jpg
    _frame_corners.push_back(cv::Point2f(544.258, 348.402));
    _frame_corners.push_back(cv::Point2f(1097.12, 274.402));
    _frame_corners.push_back(cv::Point2f(558.654, 839.489));
    _frame_corners.push_back(cv::Point2f(1154.86, 835.188));

    // left test OK
    // processing ... calib_0062.jpg
    _frame_corners.push_back(cv::Point2f(518.211, 368.706));
    _frame_corners.push_back(cv::Point2f(1062.5, 293.106));
    _frame_corners.push_back(cv::Point2f(534.71, 865.489));
    _frame_corners.push_back(cv::Point2f(1136.27, 843.253));

    // left test OK
    // processing ... calib_0063.jpg
    _frame_corners.push_back(cv::Point2f(546.134, 401.341));
    _frame_corners.push_back(cv::Point2f(1082.36, 342.016));
    _frame_corners.push_back(cv::Point2f(548.958, 896.125));
    _frame_corners.push_back(cv::Point2f(1159.03, 879.497));

    // left test OK
    // processing ... calib_0064.jpg
    _frame_corners.push_back(cv::Point2f(585.356, 433.986));
    _frame_corners.push_back(cv::Point2f(1108.08, 393.909));
    _frame_corners.push_back(cv::Point2f(571.041, 918.183));
    _frame_corners.push_back(cv::Point2f(1190.33, 912.347));

    // left test OK
    // processing ... calib_0065.jpg
    _frame_corners.push_back(cv::Point2f(439.768, 482.575));
    _frame_corners.push_back(cv::Point2f(929.805, 454.701));
    _frame_corners.push_back(cv::Point2f(382.668, 942.382));
    _frame_corners.push_back(cv::Point2f(1008.57, 924.725));

    // left test OK
    // processing ... calib_0066.jpg
    _frame_corners.push_back(cv::Point2f(405.221, 460.2));
    _frame_corners.push_back(cv::Point2f(895.249, 446.545));
    _frame_corners.push_back(cv::Point2f(321.637, 909.017));
    _frame_corners.push_back(cv::Point2f(953.663, 908.974));

    // left test OK
    // processing ... calib_0067.jpg
    _frame_corners.push_back(cv::Point2f(365.024, 446.325));
    _frame_corners.push_back(cv::Point2f(854.229, 452.411));
    _frame_corners.push_back(cv::Point2f(255.592, 889.013));
    _frame_corners.push_back(cv::Point2f(891.698, 909.236));

    // left test OK
    // processing ... calib_0068.jpg
    _frame_corners.push_back(cv::Point2f(334.295, 438.027));
    _frame_corners.push_back(cv::Point2f(823.264, 452.372));
    _frame_corners.push_back(cv::Point2f(209.459, 874.783));
    _frame_corners.push_back(cv::Point2f(847.556, 904.51));

    // left test OK
    // processing ... calib_0069.jpg
    _frame_corners.push_back(cv::Point2f(310.553, 429.848));
    _frame_corners.push_back(cv::Point2f(798.698, 474.546));
    _frame_corners.push_back(cv::Point2f(159.882, 863.414));
    _frame_corners.push_back(cv::Point2f(800.851, 925.054));

    // left test OK
    // processing ... calib_0070.jpg
    _frame_corners.push_back(cv::Point2f(278.896, 430.204));
    _frame_corners.push_back(cv::Point2f(765.051, 493.242));
    _frame_corners.push_back(cv::Point2f(108.229, 858.642));
    _frame_corners.push_back(cv::Point2f(750.639, 940.356));

    // left test OK
    // processing ... calib_0071.jpg
    _frame_corners.push_back(cv::Point2f(246.687, 407.76));
    _frame_corners.push_back(cv::Point2f(724.505, 511.788));
    _frame_corners.push_back(cv::Point2f(53.8232, 828.493));
    _frame_corners.push_back(cv::Point2f(687.175, 947.251));

    // left test OK
    // processing ... calib_0072.jpg
    _frame_corners.push_back(cv::Point2f(198.507, 356.69));
    _frame_corners.push_back(cv::Point2f(665.156, 491.673));
    _frame_corners.push_back(cv::Point2f(13.4347, 788.398));
    _frame_corners.push_back(cv::Point2f(618.977, 916.26));

    // left test OK
    // processing ... calib_0073.jpg
    _frame_corners.push_back(cv::Point2f(233.882, 304.897));
    _frame_corners.push_back(cv::Point2f(692.754, 482.557));
    _frame_corners.push_back(cv::Point2f(27.3279, 749.172));
    _frame_corners.push_back(cv::Point2f(614.484, 917.181));

    // left test OK
    // processing ... calib_0074.jpg
    _frame_corners.push_back(cv::Point2f(244.802, 228.643));
    _frame_corners.push_back(cv::Point2f(718.377, 423.471));
    _frame_corners.push_back(cv::Point2f(40.2843, 711.074));
    _frame_corners.push_back(cv::Point2f(621.99, 870.613));

    // left test OK
    // processing ... calib_0075.jpg
    _frame_corners.push_back(cv::Point2f(195.126, 172.538));
    _frame_corners.push_back(cv::Point2f(738.476, 289.623));
    _frame_corners.push_back(cv::Point2f(91.0228, 708.133));
    _frame_corners.push_back(cv::Point2f(677.46, 768.254));

    // left test OK
    // processing ... calib_0076.jpg
    _frame_corners.push_back(cv::Point2f(141.754, 144.285));
    _frame_corners.push_back(cv::Point2f(724.45, 196.738));
    _frame_corners.push_back(cv::Point2f(112.255, 694.707));
    _frame_corners.push_back(cv::Point2f(701.99, 698.136));

    // left test OK
    // processing ... calib_0077.jpg
    _frame_corners.push_back(cv::Point2f(102.298, 128.221));
    _frame_corners.push_back(cv::Point2f(707.742, 131.889));
    _frame_corners.push_back(cv::Point2f(126.684, 683.042));
    _frame_corners.push_back(cv::Point2f(715.171, 650.747));

    // left test OK
    // processing ... calib_0078.jpg
    _frame_corners.push_back(cv::Point2f(64.6111, 130.742));
    _frame_corners.push_back(cv::Point2f(683.544, 87.8293));
    _frame_corners.push_back(cv::Point2f(138.281, 683.421));
    _frame_corners.push_back(cv::Point2f(721.914, 619.454));

    // left test OK
    // processing ... calib_0079.jpg
    _frame_corners.push_back(cv::Point2f(50.748, 137.861));
    _frame_corners.push_back(cv::Point2f(681.837, 35.0509));
    _frame_corners.push_back(cv::Point2f(190.501, 676.168));
    _frame_corners.push_back(cv::Point2f(757.92, 578.624));

    // left test OK
    // processing ... calib_0080.jpg
    _frame_corners.push_back(cv::Point2f(108.927, 139.16));
    _frame_corners.push_back(cv::Point2f(742.899, 24.4459));
    _frame_corners.push_back(cv::Point2f(262.909, 655.853));
    _frame_corners.push_back(cv::Point2f(814.557, 571.601));

    // left test OK
    // processing ... calib_0081.jpg
    _frame_corners.push_back(cv::Point2f(174.701, 142.143));
    _frame_corners.push_back(cv::Point2f(818.835, 51.9449));
    _frame_corners.push_back(cv::Point2f(313.205, 647.311));
    _frame_corners.push_back(cv::Point2f(859.183, 592.001));

    // left test OK
    // processing ... calib_0082.jpg
    _frame_corners.push_back(cv::Point2f(270.518, 158.876));
    _frame_corners.push_back(cv::Point2f(925.408, 89.6277));
    _frame_corners.push_back(cv::Point2f(386.549, 656.753));
    _frame_corners.push_back(cv::Point2f(936.613, 636.164));

    // left test OK
    // processing ... calib_0083.jpg
    _frame_corners.push_back(cv::Point2f(340.505, 165.349));
    _frame_corners.push_back(cv::Point2f(973.203, 119.321));
    _frame_corners.push_back(cv::Point2f(433.72, 646.331));
    _frame_corners.push_back(cv::Point2f(972.379, 665.368));

    // left test OK
    // processing ... calib_0084.jpg
    _frame_corners.push_back(cv::Point2f(426.931, 144.051));
    _frame_corners.push_back(cv::Point2f(1068.78, 140.557));
    _frame_corners.push_back(cv::Point2f(478.901, 627.13));
    _frame_corners.push_back(cv::Point2f(1020.21, 694.645));

    // left test OK
    // processing ... calib_0085.jpg
    _frame_corners.push_back(cv::Point2f(464.777, 120.769));
    _frame_corners.push_back(cv::Point2f(1115.03, 141.354));
    _frame_corners.push_back(cv::Point2f(506.928, 602.515));
    _frame_corners.push_back(cv::Point2f(1043.68, 707.646));

    // left test OK
    // processing ... calib_0086.jpg
    _frame_corners.push_back(cv::Point2f(520.285, 85.4918));
    _frame_corners.push_back(cv::Point2f(1190.52, 121.92));
    _frame_corners.push_back(cv::Point2f(551.569, 570.188));
    _frame_corners.push_back(cv::Point2f(1092.81, 693.532));

    // left test OK
    // processing ... calib_0087.jpg
    _frame_corners.push_back(cv::Point2f(558.407, 50.4555));
    _frame_corners.push_back(cv::Point2f(1251.02, 78.5169));
    _frame_corners.push_back(cv::Point2f(589.39, 542.452));
    _frame_corners.push_back(cv::Point2f(1141.75, 662.404));

    // left test OK
    // processing ... calib_0088.jpg
    _frame_corners.push_back(cv::Point2f(485.006, 126.792));
    _frame_corners.push_back(cv::Point2f(1113.45, 30.4284));
    _frame_corners.push_back(cv::Point2f(579.386, 612.523));
    _frame_corners.push_back(cv::Point2f(1136.45, 659.648));

    // left test OK
    // processing ... calib_0089.jpg
    _frame_corners.push_back(cv::Point2f(500.125, 169.307));
    _frame_corners.push_back(cv::Point2f(1095.82, 87.5197));
    _frame_corners.push_back(cv::Point2f(580.007, 645.457));
    _frame_corners.push_back(cv::Point2f(1121.04, 714.656));

    // left test OK
    // processing ... calib_0090.jpg
    _frame_corners.push_back(cv::Point2f(543.112, 193.279));
    _frame_corners.push_back(cv::Point2f(1154.22, 152.854));
    _frame_corners.push_back(cv::Point2f(597.042, 670.552));
    _frame_corners.push_back(cv::Point2f(1142.1, 779.022));

    // left test OK
    // processing ... calib_0091.jpg
    _frame_corners.push_back(cv::Point2f(601.155, 231.612));
    _frame_corners.push_back(cv::Point2f(1222.91, 187.298));
    _frame_corners.push_back(cv::Point2f(640.042, 716.229));
    _frame_corners.push_back(cv::Point2f(1209.87, 831.696));

    // left test OK
    // processing ... calib_0092.jpg
    _frame_corners.push_back(cv::Point2f(600.113, 305.586));
    _frame_corners.push_back(cv::Point2f(1219.04, 233.349));
    _frame_corners.push_back(cv::Point2f(626.914, 809.397));
    _frame_corners.push_back(cv::Point2f(1240.14, 870.719));

    // left test OK
    // processing ... calib_0093.jpg
    _frame_corners.push_back(cv::Point2f(553.032, 298.127));
    _frame_corners.push_back(cv::Point2f(1163.2, 233.365));
    _frame_corners.push_back(cv::Point2f(573.892, 806.55));
    _frame_corners.push_back(cv::Point2f(1182.84, 860.11));

    // left test OK
    // processing ... calib_0094.jpg
    _frame_corners.push_back(cv::Point2f(510.949, 303.47));
    _frame_corners.push_back(cv::Point2f(1112.99, 235.688));
    _frame_corners.push_back(cv::Point2f(533.81, 816.984));
    _frame_corners.push_back(cv::Point2f(1142.14, 852.146));

    // left test OK
    // processing ... calib_0095.jpg
    _frame_corners.push_back(cv::Point2f(522.975, 341.766));
    _frame_corners.push_back(cv::Point2f(1114.33, 273.089));
    _frame_corners.push_back(cv::Point2f(538.876, 860.239));
    _frame_corners.push_back(cv::Point2f(1160.4, 876.271));

    // left test OK
    // processing ... calib_0096.jpg
    _frame_corners.push_back(cv::Point2f(493.134, 347.899));
    _frame_corners.push_back(cv::Point2f(1078.64, 288.478));
    _frame_corners.push_back(cv::Point2f(500.98, 873.047));
    _frame_corners.push_back(cv::Point2f(1127.9, 874.773));

    // left test OK
    // processing ... calib_0097.jpg
    _frame_corners.push_back(cv::Point2f(407.862, 316.744));
    _frame_corners.push_back(cv::Point2f(998.243, 271.569));
    _frame_corners.push_back(cv::Point2f(421.859, 854.007));
    _frame_corners.push_back(cv::Point2f(1036.45, 838.31));

    // left test OK
    // processing ... calib_0098.jpg
    _frame_corners.push_back(cv::Point2f(373.221, 383.552));
    _frame_corners.push_back(cv::Point2f(909.529, 338.715));
    _frame_corners.push_back(cv::Point2f(366.402, 896.513));
    _frame_corners.push_back(cv::Point2f(969.961, 869.369));

    // left test OK
    // processing ... calib_0099.jpg
    _frame_corners.push_back(cv::Point2f(431.745, 482.16));
    _frame_corners.push_back(cv::Point2f(902.631, 472.546));
    _frame_corners.push_back(cv::Point2f(329.598, 894.58));
    _frame_corners.push_back(cv::Point2f(946.947, 907.76));

}

void  CCalibCameraMatrixDlg::init_frame_series_50()
{
    _frame_corners.clear();


    // left test OK
    // processing ... calib_0002.jpg
    _frame_corners.push_back(cv::Point2f(544.64, 464.874));
    _frame_corners.push_back(cv::Point2f(983.757, 457.586));
    _frame_corners.push_back(cv::Point2f(536.747, 880.982));
    _frame_corners.push_back(cv::Point2f(1010.63, 881.942));

    // left test OK
    // processing ... calib_0003.jpg
    _frame_corners.push_back(cv::Point2f(265.075, 402.438));
    _frame_corners.push_back(cv::Point2f(789.448, 377.126));
    _frame_corners.push_back(cv::Point2f(278.94, 908.639));
    _frame_corners.push_back(cv::Point2f(823.358, 851.1));

    // left test OK
    // processing ... calib_0005.jpg
    _frame_corners.push_back(cv::Point2f(65.0177, 338.806));
    _frame_corners.push_back(cv::Point2f(669.658, 322.587));
    _frame_corners.push_back(cv::Point2f(81.6936, 911.28));
    _frame_corners.push_back(cv::Point2f(699.729, 846.601));
    // test failed
    // processing ... calib_0009.jpg
    _frame_corners.push_back(cv::Point2f(94.2459, 375.32));
    _frame_corners.push_back(cv::Point2f(597.812, 372.188));
    _frame_corners.push_back(cv::Point2f(124.386, 928.261));
    _frame_corners.push_back(cv::Point2f(614.865, 798.131));


    // left test OK
    // processing ... calib_0011.jpg
    _frame_corners.push_back(cv::Point2f(99.0311, 357.275));
    _frame_corners.push_back(cv::Point2f(605.95, 367.817));
    _frame_corners.push_back(cv::Point2f(115.658, 909.461));
    _frame_corners.push_back(cv::Point2f(615.341, 796.875));

    // left test OK
    // processing ... calib_0013.jpg
    _frame_corners.push_back(cv::Point2f(101.302, 346.901));
    _frame_corners.push_back(cv::Point2f(645.58, 342.532));
    _frame_corners.push_back(cv::Point2f(125.541, 879.369));
    _frame_corners.push_back(cv::Point2f(667.442, 797.443));

    // left test OK
    // processing ... calib_0015.jpg
    _frame_corners.push_back(cv::Point2f(154.96, 354.594));
    _frame_corners.push_back(cv::Point2f(703.515, 333.425));
    _frame_corners.push_back(cv::Point2f(183.805, 875.303));
    _frame_corners.push_back(cv::Point2f(735.939, 808.617));


    // left test OK
    // processing ... calib_0017.jpg
    _frame_corners.push_back(cv::Point2f(235.252, 333.054));
    _frame_corners.push_back(cv::Point2f(790.341, 303.876));
    _frame_corners.push_back(cv::Point2f(261.383, 846.298));
    _frame_corners.push_back(cv::Point2f(820.689, 806.9));


    // left test OK
    // processing ... calib_0019.jpg
    _frame_corners.push_back(cv::Point2f(327.663, 308.13));
    _frame_corners.push_back(cv::Point2f(893.824, 278.904));
    _frame_corners.push_back(cv::Point2f(342.138, 816.522));
    _frame_corners.push_back(cv::Point2f(911.661, 816.337));


    // left test OK
    // processing ... calib_0021.jpg
    _frame_corners.push_back(cv::Point2f(429.139, 304.994));
    _frame_corners.push_back(cv::Point2f(1007.71, 273.465));
    _frame_corners.push_back(cv::Point2f(430.886, 812.483));
    _frame_corners.push_back(cv::Point2f(1013.96, 852.333));


    // left test OK
    // processing ... calib_0023.jpg
    _frame_corners.push_back(cv::Point2f(517.238, 305.018));
    _frame_corners.push_back(cv::Point2f(1111.99, 258));
    _frame_corners.push_back(cv::Point2f(526.103, 811.107));
    _frame_corners.push_back(cv::Point2f(1123.96, 864.022));


    // left test OK
    // processing ... calib_0025.jpg
    _frame_corners.push_back(cv::Point2f(590.958, 270.945));
    _frame_corners.push_back(cv::Point2f(1212.26, 201.612));
    _frame_corners.push_back(cv::Point2f(614.366, 777.826));
    _frame_corners.push_back(cv::Point2f(1230.79, 827.038));

    // left test OK
    // processing ... calib_0027.jpg
    _frame_corners.push_back(cv::Point2f(580.487, 112.929));
    _frame_corners.push_back(cv::Point2f(1267.18, 54.4992));
    _frame_corners.push_back(cv::Point2f(609.909, 643.235));
    _frame_corners.push_back(cv::Point2f(1230.77, 697.34));


    // left test OK
    // processing ... calib_0029.jpg
    _frame_corners.push_back(cv::Point2f(460.575, 84.4288));
    _frame_corners.push_back(cv::Point2f(1118.58, 45.5441));
    _frame_corners.push_back(cv::Point2f(490.804, 615.064));
    _frame_corners.push_back(cv::Point2f(1081.79, 659.129));


    // left test OK
    // processing ... calib_0031.jpg
    _frame_corners.push_back(cv::Point2f(356.686, 97.6067));
    _frame_corners.push_back(cv::Point2f(1003.45, 57.6438));
    _frame_corners.push_back(cv::Point2f(406.217, 631.462));
    _frame_corners.push_back(cv::Point2f(987.3, 644.969));

    // left test OK
    // processing ... calib_0033.jpg
    _frame_corners.push_back(cv::Point2f(217.217, 85.3875));
    _frame_corners.push_back(cv::Point2f(865.289, 41.0945));
    _frame_corners.push_back(cv::Point2f(288.21, 631.099));
    _frame_corners.push_back(cv::Point2f(867.162, 618.172));


    // left test OK
    // processing ... calib_0035.jpg
    _frame_corners.push_back(cv::Point2f(106.64, 81.5606));
    _frame_corners.push_back(cv::Point2f(768.707, 46.9945));
    _frame_corners.push_back(cv::Point2f(185.805, 646.421));
    _frame_corners.push_back(cv::Point2f(777.275, 618.265));


    // left test OK
    // processing ... calib_0037.jpg
    _frame_corners.push_back(cv::Point2f(65.7111, 121.061));
    _frame_corners.push_back(cv::Point2f(689.99, 111.018));
    _frame_corners.push_back(cv::Point2f(127.62, 675.225));
    _frame_corners.push_back(cv::Point2f(698.012, 622.335));


    // left test OK
    // processing ... calib_0039.jpg
    _frame_corners.push_back(cv::Point2f(107.061, 149.703));
    _frame_corners.push_back(cv::Point2f(658.945, 168.932));
    _frame_corners.push_back(cv::Point2f(144.726, 692.101));
    _frame_corners.push_back(cv::Point2f(659.57, 616.34));

      // left test OK
    // processing ... calib_0041.jpg
    _frame_corners.push_back(cv::Point2f(156.734, 210.766));
    _frame_corners.push_back(cv::Point2f(652.022, 231.293));
    _frame_corners.push_back(cv::Point2f(195.116, 761.155));
    _frame_corners.push_back(cv::Point2f(669.639, 657.89));


    // left test OK
    // processing ... calib_0043.jpg
    _frame_corners.push_back(cv::Point2f(161.291, 209.793));
    _frame_corners.push_back(cv::Point2f(665.122, 219.756));
    _frame_corners.push_back(cv::Point2f(207.528, 759.693));
    _frame_corners.push_back(cv::Point2f(682.77, 649.42));


    // left test OK
    // processing ... calib_0045.jpg
    _frame_corners.push_back(cv::Point2f(228.338, 178.561));
    _frame_corners.push_back(cv::Point2f(740.948, 185.507));
    _frame_corners.push_back(cv::Point2f(270.24, 731.441));
    _frame_corners.push_back(cv::Point2f(745.493, 625.337));


    // left test OK
    // processing ... calib_0047.jpg
    _frame_corners.push_back(cv::Point2f(212.271, 125.427));
    _frame_corners.push_back(cv::Point2f(788.136, 91.7362));
    _frame_corners.push_back(cv::Point2f(286.771, 650.443));
    _frame_corners.push_back(cv::Point2f(777.737, 548.173));


    // left test OK
    // processing ... calib_0049.jpg
    _frame_corners.push_back(cv::Point2f(250.737, 151.48));
    _frame_corners.push_back(cv::Point2f(869.097, 81.1183));
    _frame_corners.push_back(cv::Point2f(350.838, 649.577));
    _frame_corners.push_back(cv::Point2f(852.048, 547.509));


    // left test OK
    // processing ... calib_0051.jpg
    _frame_corners.push_back(cv::Point2f(335.83, 138.291));
    _frame_corners.push_back(cv::Point2f(976.359, 82.539));
    _frame_corners.push_back(cv::Point2f(430.562, 602.221));
    _frame_corners.push_back(cv::Point2f(940.189, 555.061));


    // left test OK
    // processing ... calib_0053.jpg
    _frame_corners.push_back(cv::Point2f(419.38, 82.1278));
    _frame_corners.push_back(cv::Point2f(1113.18, 70.5701));
    _frame_corners.push_back(cv::Point2f(502.649, 543.23));
    _frame_corners.push_back(cv::Point2f(1034.69, 571.312));


    // left test OK
    // processing ... calib_0055.jpg
    _frame_corners.push_back(cv::Point2f(550.858, 49.5924));
    _frame_corners.push_back(cv::Point2f(1263.49, 105.641));
    _frame_corners.push_back(cv::Point2f(596.259, 523.344));
    _frame_corners.push_back(cv::Point2f(1137.51, 653.055));


    // left test OK
    // processing ... calib_0057.jpg
    _frame_corners.push_back(cv::Point2f(476.329, 236.648));
    _frame_corners.push_back(cv::Point2f(1046.39, 143.549));
    _frame_corners.push_back(cv::Point2f(552.591, 705.798));
    _frame_corners.push_back(cv::Point2f(1092.16, 737.766));


    // left test OK
    // processing ... calib_0059.jpg
    _frame_corners.push_back(cv::Point2f(618.249, 266.342));
    _frame_corners.push_back(cv::Point2f(1177.91, 211.875));
    _frame_corners.push_back(cv::Point2f(647.583, 726.131));
    _frame_corners.push_back(cv::Point2f(1185.05, 818.015));


    // left test OK
    // processing ... calib_0061.jpg
    _frame_corners.push_back(cv::Point2f(544.258, 348.402));
    _frame_corners.push_back(cv::Point2f(1097.12, 274.402));
    _frame_corners.push_back(cv::Point2f(558.654, 839.489));
    _frame_corners.push_back(cv::Point2f(1154.86, 835.188));


    // left test OK
    // processing ... calib_0063.jpg
    _frame_corners.push_back(cv::Point2f(546.134, 401.341));
    _frame_corners.push_back(cv::Point2f(1082.36, 342.016));
    _frame_corners.push_back(cv::Point2f(548.958, 896.125));
    _frame_corners.push_back(cv::Point2f(1159.03, 879.497));


    // left test OK
    // processing ... calib_0065.jpg
    _frame_corners.push_back(cv::Point2f(439.768, 482.575));
    _frame_corners.push_back(cv::Point2f(929.805, 454.701));
    _frame_corners.push_back(cv::Point2f(382.668, 942.382));
    _frame_corners.push_back(cv::Point2f(1008.57, 924.725));

    // left test OK
    // processing ... calib_0067.jpg
    _frame_corners.push_back(cv::Point2f(365.024, 446.325));
    _frame_corners.push_back(cv::Point2f(854.229, 452.411));
    _frame_corners.push_back(cv::Point2f(255.592, 889.013));
    _frame_corners.push_back(cv::Point2f(891.698, 909.236));


    // left test OK
    // processing ... calib_0069.jpg
    _frame_corners.push_back(cv::Point2f(310.553, 429.848));
    _frame_corners.push_back(cv::Point2f(798.698, 474.546));
    _frame_corners.push_back(cv::Point2f(159.882, 863.414));
    _frame_corners.push_back(cv::Point2f(800.851, 925.054));


    // left test OK
    // processing ... calib_0071.jpg
    _frame_corners.push_back(cv::Point2f(246.687, 407.76));
    _frame_corners.push_back(cv::Point2f(724.505, 511.788));
    _frame_corners.push_back(cv::Point2f(53.8232, 828.493));
    _frame_corners.push_back(cv::Point2f(687.175, 947.251));


    // left test OK
    // processing ... calib_0073.jpg
    _frame_corners.push_back(cv::Point2f(233.882, 304.897));
    _frame_corners.push_back(cv::Point2f(692.754, 482.557));
    _frame_corners.push_back(cv::Point2f(27.3279, 749.172));
    _frame_corners.push_back(cv::Point2f(614.484, 917.181));


    // left test OK
    // processing ... calib_0075.jpg
    _frame_corners.push_back(cv::Point2f(195.126, 172.538));
    _frame_corners.push_back(cv::Point2f(738.476, 289.623));
    _frame_corners.push_back(cv::Point2f(91.0228, 708.133));
    _frame_corners.push_back(cv::Point2f(677.46, 768.254));


    // left test OK
    // processing ... calib_0077.jpg
    _frame_corners.push_back(cv::Point2f(102.298, 128.221));
    _frame_corners.push_back(cv::Point2f(707.742, 131.889));
    _frame_corners.push_back(cv::Point2f(126.684, 683.042));
    _frame_corners.push_back(cv::Point2f(715.171, 650.747));


    // left test OK
    // processing ... calib_0079.jpg
    _frame_corners.push_back(cv::Point2f(50.748, 137.861));
    _frame_corners.push_back(cv::Point2f(681.837, 35.0509));
    _frame_corners.push_back(cv::Point2f(190.501, 676.168));
    _frame_corners.push_back(cv::Point2f(757.92, 578.624));


    // left test OK
    // processing ... calib_0081.jpg
    _frame_corners.push_back(cv::Point2f(174.701, 142.143));
    _frame_corners.push_back(cv::Point2f(818.835, 51.9449));
    _frame_corners.push_back(cv::Point2f(313.205, 647.311));
    _frame_corners.push_back(cv::Point2f(859.183, 592.001));


    // left test OK
    // processing ... calib_0083.jpg
    _frame_corners.push_back(cv::Point2f(340.505, 165.349));
    _frame_corners.push_back(cv::Point2f(973.203, 119.321));
    _frame_corners.push_back(cv::Point2f(433.72, 646.331));
    _frame_corners.push_back(cv::Point2f(972.379, 665.368));


    // left test OK
    // processing ... calib_0085.jpg
    _frame_corners.push_back(cv::Point2f(464.777, 120.769));
    _frame_corners.push_back(cv::Point2f(1115.03, 141.354));
    _frame_corners.push_back(cv::Point2f(506.928, 602.515));
    _frame_corners.push_back(cv::Point2f(1043.68, 707.646));


    // left test OK
    // processing ... calib_0087.jpg
    _frame_corners.push_back(cv::Point2f(558.407, 50.4555));
    _frame_corners.push_back(cv::Point2f(1251.02, 78.5169));
    _frame_corners.push_back(cv::Point2f(589.39, 542.452));
    _frame_corners.push_back(cv::Point2f(1141.75, 662.404));


    // left test OK
    // processing ... calib_0089.jpg
    _frame_corners.push_back(cv::Point2f(500.125, 169.307));
    _frame_corners.push_back(cv::Point2f(1095.82, 87.5197));
    _frame_corners.push_back(cv::Point2f(580.007, 645.457));
    _frame_corners.push_back(cv::Point2f(1121.04, 714.656));


    // left test OK
    // processing ... calib_0091.jpg
    _frame_corners.push_back(cv::Point2f(601.155, 231.612));
    _frame_corners.push_back(cv::Point2f(1222.91, 187.298));
    _frame_corners.push_back(cv::Point2f(640.042, 716.229));
    _frame_corners.push_back(cv::Point2f(1209.87, 831.696));

    // left test OK
    // processing ... calib_0093.jpg
    _frame_corners.push_back(cv::Point2f(553.032, 298.127));
    _frame_corners.push_back(cv::Point2f(1163.2, 233.365));
    _frame_corners.push_back(cv::Point2f(573.892, 806.55));
    _frame_corners.push_back(cv::Point2f(1182.84, 860.11));

    // left test OK
    // processing ... calib_0095.jpg
    _frame_corners.push_back(cv::Point2f(522.975, 341.766));
    _frame_corners.push_back(cv::Point2f(1114.33, 273.089));
    _frame_corners.push_back(cv::Point2f(538.876, 860.239));
    _frame_corners.push_back(cv::Point2f(1160.4, 876.271));


    // left test OK
    // processing ... calib_0097.jpg
    _frame_corners.push_back(cv::Point2f(407.862, 316.744));
    _frame_corners.push_back(cv::Point2f(998.243, 271.569));
    _frame_corners.push_back(cv::Point2f(421.859, 854.007));
    _frame_corners.push_back(cv::Point2f(1036.45, 838.31));


    // left test OK
    // processing ... calib_0099.jpg
    _frame_corners.push_back(cv::Point2f(431.745, 482.16));
    _frame_corners.push_back(cv::Point2f(902.631, 472.546));
    _frame_corners.push_back(cv::Point2f(329.598, 894.58));
    _frame_corners.push_back(cv::Point2f(946.947, 907.76));



}


/*
void CCalibCameraMatrixDlg::capture_and_proc_new_frame()
{
    cv::Mat frame;
    bool bSuccess =  _current_cam->read(frame);
    if (!bSuccess)  return single_timer_shoot();


/// int idx = ui->progressBar->value()+1;
/// QString nm = QString("./calib31jul/%1.png").arg(QString::number(idx));
/// frame = cv::imread(nm.toStdString());
/// std::cout << nm.toStdString() << std::endl;

    _frame_pix_size = frame.size();

    _captured_frames ++;

    std::vector<cv::Point2f> bPoints;
    /// TODO put into new thread
    bool found = cv::findCirclesGrid( frame, _boardSize, bPoints );

    if(found)
    {
            if((  clock() - _prev_time >  _delay ) )
            {
                _all_img_pnts.push_back(bPoints);
                _prev_time = clock();
                _blink = true;

                int v = ui->progressBar->value();
                 ui->progressBar->setValue(++v);

                 ui->lblFrame->setText(QString::number(v));

                  QString nm = QString("calib_0%1.jpg").arg(v, 3, 10, QLatin1Char('0'));
                   cv::imwrite(nm.toStdString(), frame);
            }
            else
            {
                _blink = false;
            }

        _captured_frames = 0;
        // Draw the corners.
        cv::drawChessboardCorners(frame, _boardSize, cv::Mat(bPoints), found );

        drawProjectedArea(frame);
    }


    emit update_main_preview(frame);

    if(_all_img_pnts.size() >= _max_frame_check)
        return callibrate_and_save();

    if(_captured_frames > _max_frame_count)
    {
        QString msg = QString("There are too many failed tries. Continue callibraton?");

        QMessageBox::StandardButton reply = QMessageBox::question(this, "Warning", msg,
                                        QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::No)
            close();
        else
            _captured_frames = 0;

    }

     single_timer_shoot();
}
*/
