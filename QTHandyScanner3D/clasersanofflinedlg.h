#ifndef CLASERSANOFFLINEDLG_H
#define CLASERSANOFFLINEDLG_H

#include <QDialog>

#include "graphicutils.h"
#include "opencv_aux_utils.h"

class CGLWidget;
class CLaserSanOfflineThread;
class CPointCloud;
class CTriangularMesh;
class CLaserFramePreviewDlg;

namespace Ui {
class CLaserSanOfflineDlg;
}

class CLaserSanOfflineDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CLaserSanOfflineDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CLaserSanOfflineDlg();

protected:
    void showEvent(QShowEvent * event);
    void hideEvent(QHideEvent *);

    bool _in_process;
    bool _on_apply;
    bool _is_result_valid;
    CLaserSanOfflineThread * _thread;
    CLaserFramePreviewDlg * _prevDlg;

    cv::VideoCapture * _in_avi;
    void clean_avi_acc();

    QImage * _bg_img;
    void init_bckg_image();

    std::map<int, CFrameDesc> _frame_map;
    void init_frame_map();
    float convert_to_angle(int );

    void read_ini_file(QString &);

   QString _video_name_format;
   QString _video_desc_format;
   bool _video_name_has_scan;

    QString  _scan_dir;
    QString _image_name_format;
    bool _image_name_has_scan;
    int _first_scan;
    int _last_scan;
    int _first_image;
    int _last_image;
    bool _is_difference;
    bool _has_marks;
    bool _is_round_marks;
    QString _bckg_image;
    QString _bckg_image2;
    int _marks_x;
    int _marks_y;
    int _lookfor_laser_onboard_ymax;

    bool _use_metadata_angle;
    bool _camera_laser_interchanged;
    float _meta_angle_correction;


private:
    Ui::CLaserSanOfflineDlg *ui;

    CGLWidget *_glWidget;

private slots:
    void onCancelBtn();
    void onApplyBtn();
    void onLoadINIBtn();
    void updPrevBtn();
    void init_avi_acc();

    void onMakeProgressStep(int val);
    void onCompleteProcessing();
    void onStopProcessing();
    void onWrongKMatrix();

    void close_dialog();


    void previewWindowToggle();

    void slider_frame_moved(int );
    void refresh_preview();
    void corwndsz_changed();
    void btn_frame_clicked_pos();
    void btn_frame_clicked_neg();

    void edge_thresh_changed(int );

    void onSaveCalib();
    void onLoadCalib();

};

#endif // CLASERSANOFFLINEDLG_H
