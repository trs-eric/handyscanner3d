#ifndef CCAMERAADJUSTMENTDLG_H
#define CCAMERAADJUSTMENTDLG_H

#include <QDialog>
#include <QLabel>
#include <QImage>
#include <QScrollBar>
#include <QBasicTimer>
#include <QTableWidgetItem>
#include <QMutex>

class CGLWidget;
//class CScanReconstructionThread;

namespace Ui {
class CCameraAdjustmentDlg;
}

class CCameraAdjustmentDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CCameraAdjustmentDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CCameraAdjustmentDlg();


private slots:
    void load_raw_img();
    QImage * load_raw_img_from_hdisk();
    QImage * load_raw_img_from_device();

    void zoom_in();
    void zoom_out();
    void zoom_norm();

    void current_image_changed(int);

    void test_motor();
    void test_snapshot();
    void set_auto_snap(bool);
    void save_image();

    void dev_reg_read();
    void dev_reg_write();
    void on_regvalue_changed(QTableWidgetItem *);

    ///void led1_bright_changed(int );
    /// void led2_bright_changed(int );
    void laser1_bright_changed(int );
    void laser2_bright_changed(int );
    void on_focus_edt(QString );

    void leftLaserToggled();
    void rightLaserToggled();

    ///void LED1Toggled();
    ///void LED2Toggled();

    void redgain_changed(int );
    void gregain_changed(int );
    void blugain_changed(int );

    void exposure_changed(int );
    ///void exposure_tex_changed(int );

    void exposure_changed();
    ///void exposure_tex_changed();

    void smooth_motor_rotation();
    QImage * process_single_diff_image();
    void test_process_series();

    void edge_thresh_changed(int );
    void gauss_sigma_changed(int );

    void mode_selector();
    void set_rotation_center();
    void image_label_clicked();

protected:
    void timerEvent(QTimerEvent *event);
    int ComboToChannelIdx(QString  str);
    void mousePressEvent(QMouseEvent *event);

    QPoint _mouseLastPos;
private:
    enum ComboIndices {cmbRAW=0, cmbPROCESSED, cmdDEFISHEYE, cmdEDGES};

    Ui::CCameraAdjustmentDlg *ui;

    QLabel *        _image_label;

    void clean_images();

    void adjust_scroll_bar(QScrollBar *scrollBar, float factor);
    void scale_image();
    void compute_processed_image();
    void compute_defisheye_image();
    void compute_diff_img();
    void compute_edges_image();

    float _scale_factor;

    QImage * _raw_img;
    QImage * _processed_img;
    QImage * _defisheye_img;
    QImage * _diff_img;
    QImage * _edges_img;
    QImage * _current_img;
    bool _device_busy;
    QMutex  _mutex;

    unsigned char * _buffer;

    QBasicTimer _snap_timer;

    /*
    QBasicTimer _led1_timer;
    int _led1_timer_cnt;
    void led1_on();
    void led1_off();

    QBasicTimer _led2_timer;
    int _led2_timer_cnt;
    void led2_on();
    void led2_off();
    */

    bool _auto_snap;


    bool ch_reg[256];
    bool is_device;

    CGLWidget *_glWidget;

};

#endif // CCAMERAADJUSTMENTDLG_H
