#include "ctexpreview.h"
#include "ui_ctexpreview.h"
#include <QLabel>
#include <iostream>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QFileDialog>
#include <QScrollBar>
#include <QGLWidget>

#include "messages.h"
#include "graphicutils.h"
#include "cglcompoundobject.h"
#include "img2space/edgedetection.h"

const int slid_offset = 127;
const int slid_const_offset = 127;
const int slid_vib_offset = 100;
const int slid_sat_offset = 127;
const int slid_hue_offset = 180;

/*

  rewrite this class without use f set and get pixel

*/


CTexPreview::CTexPreview(CGLCompoundObject * par,
                         QImage & tex, QString name, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CTexPreview), _parent(par), _tex_ttl(name), _texture(tex), _texture_pre(tex),
    _scale_factor(0.5)
{
    ui->setupUi(this);


    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint | Qt::WindowMinimizeButtonHint |
                   Qt::WindowMaximizeButtonHint);

    this->setModal(false);

    QString ttl = QString("Texture preview"); // + name;
    this->setWindowTitle(ttl);

    set_sliders_default();


   _tex_label = new QLabel;
   _tex_label->setBackgroundRole(QPalette::Base);
   _tex_label->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
   _tex_label->setScaledContents(true);

   ui->scrollArea->setBackgroundRole(QPalette::Dark);
   ui->scrollArea->setWidget(_tex_label);
   ui->scrollArea->setWidgetResizable(false);


//   _tex_label->setPixmap(QPixmap::fromImage(tex));
//   _tex_label->resize(_tex_label->pixmap()->size());

   QHBoxLayout *dialog_layout = new QHBoxLayout(this);
   dialog_layout->setMargin(0);
   dialog_layout->setSpacing(0);
   dialog_layout->addWidget(ui->scrollArea);
   setLayout(dialog_layout);

   connect(this, SIGNAL(finished(int)), SLOT(deleteLater()));
   connect(ui->btnSave, SIGNAL(clicked()), this, SLOT(image_save()));

   connect(ui->ZoomInBtn,   SIGNAL(clicked()), this, SLOT(zoom_in()));
   connect(ui->ZoomOutBtn,  SIGNAL(clicked()), this, SLOT(zoom_out()));
   connect(ui->ZoomNormBtn, SIGNAL(clicked()), this, SLOT(zoom_norm()));

   connect(ui->slidBri,   SIGNAL(valueChanged(int)), this, SLOT(brightness_changed(int )));
   connect(ui->slidCon,   SIGNAL(valueChanged(int)), this, SLOT(contrast_changed(int )));
   //connect(ui->slidVib,   SIGNAL(valueChanged(int)), this, SLOT(vibrance_changed(int )));

    connect(ui->slidHue,   SIGNAL(valueChanged(int)), this, SLOT(huesat_changed(int )));
    connect(ui->slidSat,   SIGNAL(valueChanged(int)), this, SLOT(huesat_changed(int )));

   connect(ui->slidDebAmount,   SIGNAL(valueChanged(int)), this, SLOT(deblur_changed(int )));
   ///connect(ui->slidDebSigma,   SIGNAL(valueChanged(int)), this, SLOT(deblur_changed(int )));
   ///connect(ui->slidDebThres,   SIGNAL(valueChanged(int)), this, SLOT(deblur_changed(int )));

   connect(ui->btnApply, SIGNAL(clicked()), this, SLOT(apply_img()));
   connect(ui->btnReset, SIGNAL(clicked()), this, SLOT(reset_img()));


   scale_image();
}

CTexPreview::~CTexPreview()
{
    delete ui;
    delete _tex_label;
}


void CTexPreview::image_save()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                tr("Save image ..."), _tex_ttl, "Image PNG (*.png)");

    if (fileName.isEmpty()) return;

    if(!_texture.save(fileName))
        QMessageBox::information(this, tr("Warning"), err_cannotSaveFile );
}

void CTexPreview::adjust_scroll_bar(QScrollBar *scrollBar, float factor)
{
    scrollBar->setValue(int(factor * scrollBar->value()
                            + ((factor - 1) * scrollBar->pageStep()/2)));
}

void CTexPreview::scale_image()
{
    _tex_label->setPixmap(QPixmap::fromImage(_texture));
    _tex_label->resize(_scale_factor * _tex_label->pixmap()->size());
    adjust_scroll_bar(ui->scrollArea->horizontalScrollBar(), _scale_factor);
    adjust_scroll_bar(ui->scrollArea->verticalScrollBar(),   _scale_factor);

    update();
}


void CTexPreview::huesat_changed(int value)
{
      modify_image();
}

void CTexPreview::deblur_changed(int value)
{
    modify_image();
}

void CTexPreview::brightness_changed(int value)
{
   modify_image();
}

void CTexPreview::contrast_changed(int value)
{
    modify_image();
}

void CTexPreview::vibrance_changed(int value)
{
    modify_image();
}

void CTexPreview::apply_img()
{
    _parent->replace_texture(&_texture);

    /////// quick fix to update main window ////////
    QWidget* m = NULL;
    foreach(QWidget *widget, qApp->topLevelWidgets())
           if(widget->inherits("QMainWindow"))
               { m = widget; break; }

    if(m!= NULL)
    {
         QList<QGLWidget *> widgets = m->findChildren<QGLWidget *>();
         if(widgets.size() != 0 )
            (*widgets.begin())->updateGL();
    }
    //////////////////////////////////////////////////


    _texture_pre = _texture.copy();

    set_sliders_default();
}


void CTexPreview::reset_img()
{
    _texture = _texture_pre.copy();
    set_sliders_default();

    scale_image();
}

void  CTexPreview::set_sliders_default()
{
    ui->slidBri->blockSignals(true);
    ui->slidCon->blockSignals(true);
    ui->slidHue->blockSignals(true);
    ui->slidSat->blockSignals(true);
    ui->slidDebAmount->blockSignals(true);

    ui->slidBri->setValue(slid_offset);
    ui->slidCon->setValue(slid_const_offset);
    ui->slidHue->setValue(slid_hue_offset);
    ui->slidSat->setValue(slid_sat_offset);
    ui->slidDebAmount->setValue(0);

    ui->slidBri->blockSignals(false);
    ui->slidCon->blockSignals(false);
    ui->slidHue->blockSignals(false);
    ui->slidSat->blockSignals(false);
    ui->slidDebAmount->blockSignals(false);
}

void CTexPreview::modify_image()
{
    ui->slidBri->blockSignals(true);
    ui->slidCon->blockSignals(true);
    ui->slidHue->blockSignals(true);
    ui->slidSat->blockSignals(true);
    ui->slidDebAmount->blockSignals(true);
/*
    ui->slidBri->setEnabled(false);
    ui->slidCon->setEnabled(false);
    ui->slidHue->setEnabled(false);
    ui->slidSat->setEnabled(false);
    ui->slidDebAmount->setEnabled(false);
*/

    /// std::cout << value - slid_offset << std::endl;

    int new_bright = ui->slidBri->value() - slid_offset;
    int new_contr = ui->slidCon->value();

    int new_hue = ui->slidHue->value() - slid_hue_offset;
    int new_sat = ui->slidSat->value() - slid_sat_offset;

    //// int new_vibr = ui->slidVib->value();

    int xs   = _texture_pre.width();
    int ys   = _texture_pre.height();

    QRgb * iYLine   = (QRgb*)_texture_pre.scanLine(0);
    QRgb * oYLine   = (QRgb*)_texture.scanLine(0);

    #pragma omp parallel for
    for(int y = 0; y < ys; y++)
    {
        for(int x =0; x < xs; x++ )
        {
            //std::cout << x << " ";

            QRgb p0 = iYLine[y*xs + x];
            int p0r = qRed(p0);
            int p0g = qGreen(p0);
            int p0b = qBlue(p0);

            int p1r = changeBrightness(p0r, new_bright);
            int p1g = changeBrightness(p0g, new_bright);
            int p1b = changeBrightness(p0b, new_bright );

            int p2r = changeContrast(p1r, new_contr);
            int p2g = changeContrast(p1g, new_contr);
            int p2b = changeContrast(p1b, new_contr);

            /// int p3r = changeVibrance(p2r, new_vibr);
            /// int p3g = changeVibrance(p2g, new_vibr);
            /// int p3b = changeVibrance(p2b, new_vibr);


            ///////////////////////////////////////////
            QColor ic = QColor(p2r, p2g, p2b, 255);
            int h, s, v;
            ic.getHsv(&h, &s, &v);
            int hn =  changeHue( h, new_hue);
            int sn = changeSaturation(s, new_sat);

            ic.setHsv(hn,sn,v,255);

            int pr, pg, pb;
            ic.getRgb(&pr, &pg, &pb);
            ///////////////////////////////////////////

            oYLine[y*xs+x] = qRgb(pr, pg, pb);

        }
    }

    unsharp_mask();
    scale_image();

    ui->slidBri->blockSignals(false);
    ui->slidCon->blockSignals(false);
    ui->slidHue->blockSignals(false);
    ui->slidSat->blockSignals(false);
    ui->slidDebAmount->blockSignals(false);
/*
    ui->slidBri->setEnabled(true);
    ui->slidCon->setEnabled(true);
    ui->slidHue->setEnabled(true);
    ui->slidSat->setEnabled(true);
    ui->slidDebAmount->setEnabled(true);
*/
}


void compute_smoothed_color_image(QImage * img, float sigma)
{


    int ksz = 0;
    float * kernel =  gauss_kernel(ksz, sigma);
    int center = ksz /2;

    int xs = img->width();
    int ys = img->height();

    float * red_sm = new float[xs * ys];
    float * blu_sm = new float[xs * ys];
    float * gre_sm = new float[xs * ys];

    QRgb * iYLine   = (QRgb*)img->scanLine(0);


    /// bluring in x-direction
    #pragma omp parallel for
    for(int r=0; r < ys; r++)
    {
        for(int c=0; c < xs; c++)
        {
            float dot_r(0);
            float dot_g(0);
            float dot_b(0);
            float sum(0);

            for(int cc=(-center); cc<=center; cc++)
            {
                if(((c+cc) >= 0) && ((c+cc) < xs))
                {

                    QRgb p0 = iYLine[r*xs + (c+cc)];//   img->pixel(c+cc,r);
                    int pr = qRed(p0);
                    int pg = qGreen(p0);
                    int pb = qBlue(p0);

                    dot_r += kernel[center+cc] * pr;
                    dot_g += kernel[center+cc] * pg;
                    dot_b += kernel[center+cc] * pb;

                    sum += kernel[center+cc];
                }
            }
            red_sm[r*xs+c] = dot_r/sum;
            gre_sm[r*xs+c] = dot_g/sum;
            blu_sm[r*xs+c] = dot_b/sum;
         }
    }

    /// bluring in y-direction
    #pragma omp parallel for
    for(int c=0; c < xs; c++)
    {
        for(int r=0; r<ys; r++)
        {
            float dot_r(0);
            float dot_g(0);
            float dot_b(0);
            float sum(0);

            for(int rr=(-center); rr<=center; rr++)
            {
                if(((r+rr) >= 0) && ((r+rr) < ys))
                {
                    dot_r += red_sm[(r+rr)*xs+c] * kernel[center+rr];
                    dot_g += gre_sm[(r+rr)*xs+c] * kernel[center+rr];
                    dot_b += blu_sm[(r+rr)*xs+c] * kernel[center+rr];

                    sum   += kernel[center+rr];
                }
            }

            int pr = (int) (dot_r/sum + 0.5);
            int pg = (int) (dot_g/sum + 0.5);
            int pb = (int) (dot_b/sum + 0.5);

            iYLine[r*xs+c]= qRgba(pr, pg, pb, 255);
         }
    }

    delete [] kernel;
    delete [] red_sm;
    delete [] blu_sm;
    delete [] gre_sm;
}

void deblurring(QImage * src, QImage * dst, QImage * res, float amount, int thres)
{
    int xs = src->width();
    int ys = src->height();

    QRgb * sYLine   = (QRgb*)src->scanLine(0);
    QRgb * dYLine   = (QRgb*)dst->scanLine(0);
    QRgb * rYLine   = (QRgb*)res->scanLine(0);


    #pragma omp parallel for
    for(int r=0; r < ys; r++)
    {
        for(int c=0; c < xs; c++)
        {

            // source values
            QRgb sp = sYLine[r*xs+c]; // src->pixel(c,r);
            int sr = qRed(sp);
            int sg = qGreen(sp);
            int sb = qBlue(sp);

            // dest values
            QRgb dp = dYLine[r*xs+c];//->pixel(c,r);
            int dr = qRed(dp);
            int dg = qGreen(dp);
            int db = qBlue(dp);


            int diff_red = sr - dr;
            int diff_gre = sg - dg;
            int diff_blu = sb - db;


            // do tresholding
            if (abs (2 * diff_red) < thres)
                        diff_red = 0;
            if (abs (2 * diff_gre) < thres)
                        diff_gre = 0;
            if (abs (2 * diff_blu) < thres)
                        diff_blu = 0;

            int vr = qBound<int>(0, sr + amount * diff_red, 255);
            int vg = qBound<int>(0, sg + amount * diff_gre, 255);
            int vb = qBound<int>(0, sb + amount * diff_blu, 255);


            rYLine[r*xs+c] = qRgb(vr, vg, vb);
        }
    }
}


void CTexPreview::unsharp_mask()
{
    float sigma = 1;    // float(ui->slidDebSigma->value())/10; /// [1 ... 5 ]
    int threshold = 0; //ui->slidDebThres->value(); /// [0 ... 255]
    float amount = float(ui->slidDebAmount->value())/30.0;  ///


    /// std::cout << sigma  << " " << amount << " " <<threshold << std::endl;

    if (amount == 0)
        return;

    QImage dest_tex = _texture.copy();

    compute_smoothed_color_image(&dest_tex, sigma);
    deblurring(&_texture, &dest_tex, &_texture, amount, threshold);
}

void CTexPreview::zoom_in()
{
    if(_scale_factor > 2.0) return;
    _scale_factor += .25;
     scale_image();
}

void CTexPreview::zoom_out()
{
    if(_scale_factor < 0.5) return;
    _scale_factor -= .25;
     scale_image();
}

void CTexPreview::zoom_norm()
{
    _scale_factor = 1.0;
     scale_image();
}
