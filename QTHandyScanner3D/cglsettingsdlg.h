#ifndef CGLSETTINGSDLG_H
#define CGLSETTINGSDLG_H

#include <QDialog>
class CGLWidget;
namespace Ui {
class CGLSettingsDlg;
}

class CGLSettingsDlg : public QDialog
{
    Q_OBJECT
private slots:
    void reset_camera_pos();
    void change_bckg_side_color();
    void change_bckg_cent_color();
    void change_surface_color();
    void change_light_amb_color();
    void change_light_dif_color();
    void change_light_posX(int );
    void change_light_posY(int );
    void change_light_posZ(int );

    void change_mat_spec(int );
    void pointsize_changed(int);
    ///void change_mat_shin(int );
    ///
    void change_cam_opacity(int );

    void dyn_change_surface_color(const QColor & color);
    void dyn_change_bckg_side_color(const QColor & color);
    void dyn_change_bckg_cent_color(const QColor & color);
    void dyn_change_light_amb_color(const QColor & color);
    void dyn_change_light_dif_color(const QColor & color);

    void enable_radiance_render(bool );
    void enhancement_changed(int );
    void transition_changed(int);
    //void invert_effect(int );
    //void loadConcave();
    //void loadConvex();

    void repaint_left_sphere();
    void change_rc_canv_color_left();
    void dyn_change_rc_canv_color_left(const QColor&);

    void change_rc_bklit_color_left();
    void dyn_change_rc_bklit_color_left(const QColor&);


    void repaint_right_sphere();
    void change_rc_canv_color_right();
    void dyn_change_rc_canv_color_right(const QColor& );

    void change_rc_bklit_color_right();
    void dyn_change_rc_bklit_color_right(const QColor& );

    void change_spheres();

    void save_presets();
    void load_presets();

    void change_table_pos(int);


protected:
    void showEvent(QShowEvent * event);
    //void changeIcon(QString path, int icon);
    void actualize_interface();

    QPixmap _pix_shad;
    QPixmap _pix_bklt;
    QPixmap _pix_tplt;
    QPixmap _pix_splt;
    QPixmap _pix_mask;
    QPixmap _pix_rmask;


    QColor _rc_bck_colorL;
    QColor _rc_bcklight_colorL;

    QColor _rc_bck_colorR;
    QColor _rc_bcklight_colorR;

    int _rc_max_sz;
    int _rc_min_sz;
    int _rc_lbl_sz;

   void block_rc_controls();
   void unblock_rc_controls();

public:
    explicit CGLSettingsDlg(CGLWidget * ,  QWidget * mw, QWidget *parent = 0);
    ~CGLSettingsDlg();

private:
    Ui::CGLSettingsDlg *ui;

    QWidget * _mw;

    CGLWidget *_glWidget;
    void update_btn_color();
    void init_interface();
};

#endif // CGLSETTINGSDLG_H
