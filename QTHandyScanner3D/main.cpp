#include "mainwindow.h"
#include <QApplication>
#include <QFile>
#include <omp.h>
#include <iostream>
#include  "settings/psettings.h"
CGlobalSettings app_settings;


int main(int argc, char *argv[])
{


    //////////////////////////////
    omp_set_nested(1);


    QApplication a(argc, argv);
    a.addLibraryPath(":/imageformats");

    QFile qssfile(":/qss/rubicon.qss");
    qssfile.open(QFile::ReadOnly);
    QString rsheet = QLatin1String(qssfile.readAll());

    a.setStyleSheet(rsheet);

    MainWindow w;

//    QIcon icon(":icons/icon_app.ico");
 //   w.setWindowIcon(icon);

    w.show();


    return a.exec();
}
/*

int frames = 550;

QString img_name_fmt("./laser_max05_2mai/scan_1_%04d.png");

QImage outimg;


for(int f = 1; f <= frames; f++ )
{
    char pattern_name_buf[2048];
    sprintf(pattern_name_buf, img_name_fmt.toStdString().c_str(), f);
    QImage simg(pattern_name_buf);

    std::cout << pattern_name_buf << std::endl;


    if(outimg.width() == 0)
    {
        outimg = simg.copy();
        continue;
    }
    unsigned width  = simg.width();
    unsigned height = simg.height();

    for (int y = 0; y < height; y++)
    {
        QRgb * oYLine   = (QRgb*)outimg.scanLine(y);
        QRgb * iYLine   = (QRgb*)simg.scanLine(y);
        for (int x = 0; x < width; x++, iYLine++, oYLine++)
        {
            QRgb vsrc  = *iYLine;
            QRgb vdest = *oYLine;

            int red   = (  qRed(vdest) >   qRed(vsrc))?  qRed(vdest)  :   qRed(vsrc) ;
            int green = (qGreen(vdest) > qGreen(vsrc))? qGreen(vdest) : qGreen(vsrc) ;
            int blue  = ( qBlue(vdest) >  qBlue(vsrc))? qBlue(vdest)  :  qBlue(vsrc) ;

            *oYLine = qRgb( red, green,  blue);
      }
    }

}
outimg.save("commulative.png");

*/
