#ifndef CSCENEEXPLORERMANAGER_H
#define CSCENEEXPLORERMANAGER_H
#include <QObject>
#include <QString>
#include <QPixmap>

#include <iostream>
#include "menu_vew_actions.h"
class CGLScene;
class CGLCompoundObject;
class QWidget;
class QTreeWidget;
class QTreeWidgetItem ;


class CSceneExplorerManager
{

public slots:
    void add_new_cobj(CGLCompoundObject * cobj);


public:
      CSceneExplorerManager(QWidget * t, CGLScene *, MenuActions *) ;
    ~CSceneExplorerManager();

    void add_unique_cobj(CGLCompoundObject * cobj);

    void update_scene_explorer();

    bool is_root_raw_pclouds(QTreeWidgetItem * item){return (item == _root_raw_pclouds);}
    bool is_root_raw_decim_surfaces(QTreeWidgetItem * item){return (item == _root_raw_decim_surfaces);}
    bool is_root_fused_surfaces(QTreeWidgetItem * item){return (item == _root_fused_surfaces);}

    void update_view_pcls(int c ) {_see_pcls[c]   = !_see_pcls[c];}
    void update_view_meshs(int c ) {_see_meshs[c] = !_see_meshs[c];}
    bool get_view_pcls(int c ) {
        return _see_pcls[c]  ;}
    bool get_view_meshs(int c ) {return _see_meshs[c]  ;}

    void set_rend_mode_wire();
    void set_rend_mode_flat();
    void set_rend_mode_smoo();
    void set_rend_mode_tex();

    void clean_all();

protected:

    QTreeWidget * _treew;
    CGLScene * _scene;

    QTreeWidgetItem * _root_raw_videos;
    QTreeWidgetItem * _root_raw_pclouds;
    QTreeWidgetItem * _root_raw_textures;
    QTreeWidgetItem * _root_raw_decim_surfaces;
    QTreeWidgetItem * _root_fused_surfaces;


    void initialize_tree();
    void update_column_width();
    QPixmap prepare_pixmap(QString , bool);
    int _obj_counter;

    std::vector<bool> _see_pcls;
    std::vector<bool> _see_meshs;

    MenuActions * _va;
    void update_field_buttons();

    int _current_btn_raw;
    int _current_btn_surf;
};

#endif // CSCENEEXPLORERMANAGER_H
