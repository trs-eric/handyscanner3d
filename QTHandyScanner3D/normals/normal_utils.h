#ifndef NORMAL_UTILS_H
#define NORMAL_UTILS_H

#include <opencv2/calib3d/calib3d.hpp>
#include "graphicutils.h"


Vector3f compute_lse_plane_normal(std::vector<cv::Point3f> & in);

#endif // NORMAL_UTILS_H
