#ifndef __VERTEXNORMAL_H__
#define __VERTEXNORMAL_H__

#include <iostream>
#include "point3.h"


class CVertex
{
public:



	typedef float ScalarType ;

    struct CoordType
	{
    public:
        CoordType(vcg::Point3<ScalarType>  p) : x(p._x), y(p._y), z(p._z) { }

        CoordType(ScalarType xx=0.0, ScalarType yy=0.0, ScalarType  zz=0.0) : x(xx), y(yy), z(zz){};

		float x, y, z;

		inline void SetZero()
		{
			x = 0;
			y = 0;
			z = 0;
		}

		inline CoordType & operator += ( CoordType  & p)
		{
			x += p.x;
			y += p.y;
			z += p.z;
			return *this;
		}

		inline CoordType & operator /= (  ScalarType s )
		{	
			x /= s;
			y /= s;
			z /= s;
			return *this;
		}
		
		inline CoordType  operator - ( CoordType  & p) 
		{
			return CoordType( x -p.x, y-p.y, z-p.z );
		}

		inline ScalarType & operator [] (  int i )
		{
			//assert(i>=0 && i<3);
			if(i == 0) return x;
			if(i == 1) return y;

			return z;
		}

        vcg::Point3<ScalarType> PP()
		{
            return vcg::Point3<ScalarType>(x, y, z);
		} 


	} _coord;

	
	struct NormalType 
	{
		float x, y, z;
	
		NormalType (ScalarType xx=0, ScalarType yy=0, ScalarType  zz=0) : x(xx), y(yy), z(zz){}
	
		inline void SetZero()
		{
			x = 0;
			y = 0;
			z = 0;
		}

		inline ScalarType & operator [] (  int i )
		{
			//assert(i>=0 && i<3);
			if(i == 0) return x;
			if(i == 1) return y;

			return z;
		}

		inline NormalType & Normalize()
		{
			ScalarType n = ScalarType(sqrt(x*x + y*y + y*y));
			if (n > ScalarType (0)) { x /= n; y /= n; z /= n; }
			return *this;
		}

		inline ScalarType operator * ( NormalType  & p ) 
		{
			return ( x*p.x + y*p.y + z*p.z );
		}
	
			inline NormalType operator * (  ScalarType s ) 
		{
			return NormalType ( x*s, y*s, z*s );
		}


		inline ScalarType dot( NormalType & p ) { return (*this) * p; }

		inline NormalType & operator *= ( const ScalarType s )
		{
			x *= s;
			y *= s;
			z *= s;
			return *this;
		}


	} _normal;

/*	struct ScalarType
	{
		float d;
	} _dist;
*/
	public:
	
    vcg::Box3<ScalarType>  P()
	{
        vcg::Box3<ScalarType> bb;
        bb.Set(vcg::Point3<ScalarType>(_coord.x, _coord.y, _coord.z));
		return bb; 
	} 


    vcg::Point3<ScalarType> PP()
	{
		// return Point3<ScalarType>(_coord.x, _coord.y, _coord.z); 
		return _coord.PP(); 
	} 


	NormalType & N() 
	{
		return _normal; 
	} 


 template<class BoxType>
  void GetBBox( BoxType & bb ) 
  {	  // bb.Set(this->P());
	 bb = this->P();
 
 }

    
  friend std::ostream & operator << (std::ostream& out,
          const CVertex & p)
  {
    out <<   p._coord.x << "\t";
    out <<   p._coord.y << "\t";
    out <<   p._coord.z << "\t";

	out <<   p._normal.x << "\t";
    out <<   p._normal.y << "\t";
    out <<   p._normal.z << "\t";

    return out;
  }

  
};


#endif 
