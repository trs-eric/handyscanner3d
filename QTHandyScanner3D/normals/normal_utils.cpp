#include "normal_utils.h"




Vector3f find_center(std::vector<cv::Point3f> & in)
{
    Vector3f sum;
    sum.x = 0.;
    sum.y = 0.;
    sum.z = 0.;

    std::vector<cv::Point3f>::iterator it = in.begin();
    for(int count = 0 ; it != in.end(); it++, count++)
    {
        sum.x += it->x;
        sum.y += it->y;
        sum.z += it->z;
    }
    sum.x /= float(count);
    sum.y /= float(count);
    sum.z /= float(count);

    return sum;
}

float find_largest_entry(cv::Mat & m)
{
    float result=0.0;

    int xs = m.cols;
    int ys = m.rows;

    for(int i = 0; i < xs; i++)
    {
        for(int j=0; j < ys; j++)
        {
            float entry=std::fabs(m.at<float>(i,j));
            result= (entry>result)?entry:result;
        }
    }
    return result;
}

Vector3f find_eigenvector_associated_with_smallest_eigenvalue(cv::Mat & m)
{
    cv::Mat E, V;
    cv::eigen(m,E,V);


 /// E.at<double>(0,i);
 ///   std::cout << "E " << E.cols << " " << E.rows << std::endl;
 ///   std::cout << "V " << V.cols << " " << V.rows << std::endl;
   /// std::cout << E << std::endl;
    //std::cout << V << std::endl;

    Vector3f res;
    res.x = V.at<double>(2,0);
    res.y = V.at<double>(2,1);
    res.z = V.at<double>(2,2);

  ///  std::cout << res.x << " " << res.y << " " << res.z << std::endl;

    return res;

}

Vector3f compute_lse_plane_normal(std::vector<cv::Point3f> & points)
{

    float sumxx = 0;
    float sumxy = 0;
    float sumxz = 0;
    float sumyy = 0;
    float sumyz = 0;
    float sumzz = 0;

   Vector3f center = find_center(points);

   ///std::cout << " center " << center.x << " " << center.y << " " << center.z << std::endl;

   std::vector<cv::Point3f>::iterator it = points.begin();
   for(int count = 0 ; it != points.end(); it++, count++)
   {
       float dx = it->x - center.x;
       float dy = it->y - center.y;
       float dz = it->z - center.z;

       sumxx += dx*dx;
       sumxy += dx*dy;
       sumxz += dx*dz;
       sumyy += dy*dy;
       sumyz += dy*dz;
       sumzz += dz*dz;
   }

   cv::Mat symmetricM = (cv::Mat_<double>(3,3) << sumxx, sumxy, sumxz,
                                                  sumxy, sumyy, sumyz,
                                                  sumxz, sumyz, sumzz);
   cv::Mat E, V;
   cv::eigen(symmetricM,E,V);

   Vector3f res;
   res.x = V.at<double>(2,0);
   res.y = V.at<double>(2,1);
   res.z = V.at<double>(2,2);

   return res;

   ///std::cout << symmetricM << " " <<  cv::determinant(symmetricM) << std::endl;


  //return find_eigenvector_associated_with_smallest_eigenvalue(symmetricM);

/*
   cv::Mat symMinv = symmetricM.inv();
   cv::Mat E, V;
   cv::eigen(symMinv,E,V);
   Vector3f res;
   res.x = V.at<double>(0,0);
   res.y = V.at<double>(0,1);
   res.z = V.at<double>(0,2);
   return res;*/
}
