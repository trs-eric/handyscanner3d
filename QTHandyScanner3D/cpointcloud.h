#ifndef CPOINTCLOUD_H
#define CPOINTCLOUD_H

#include<QString>
#include<vector>

#include "cglobject.h"
#include "graphicutils.h"
#include "ciostate.h"
#include "opencv_aux_utils.h"

class CPointCloud : public CGLObject, public CIOState
{
public:
    CPointCloud();

    void create_glList();

    bool import(QString & file);
    bool pexport(QString & file);

    int get_points_count(){return _points.size();}
    std::vector<C3DPoint> get_points_cpy(){return _points;}
    std::vector<C3DPoint> * get_points_direct_access(){return &_points;}

    void set_points(std::vector<C3DPoint> & pnts){_points = pnts;}
    void add_points(std::vector<C3DPoint> & pnts){
        _points.insert(_points.end(), pnts.begin(), pnts.end());}

    void delete_selected_points();

    void save(std::ofstream & ofile);
    void load(std::ifstream & ifile);

    void scale(float sx, float sy, float sz);
    void translate(float dx, float dy, float dz);
    void rotate(float rx, float ry, float rz);

    void turn_on_tcs(){_use_temp_color_scheme = true;}
    void turn_off_tcs(){_use_temp_color_scheme = false;}

    void set_temp_coloring_scheme(float brightness, float contrast, float gamma);
    void apply_coloring_scheme();


    enum SelectMode{DEF = 0, ADD, SUB, POLY};
    void update_section(int x, int y, int w, int h,
                        std::vector<cv::Point2f> & v, SelectMode s);
    void clear_selection();
    bool is_there_selected();

    bool _use_viewref;
    bool _use_common_color;
    bool _use_temp_color_scheme;

protected:
    bool import_from_file_xyz(std::string fname);
    bool export_xyz_pnts(std::string fname);

    void create_glList_using_greference();
    void create_glList_ignoring_greference();
    void create_glList_using_temporal_scheme();

    std::vector<C3DPoint> _points;

    void rotateX(float cos_alpha, float sin_alpha, float * x, float * y, float * z);
    void rotateY(float cos_alpha, float sin_alpha, float * x, float * y, float * z);
    void rotateZ(float cos_alpha, float sin_alpha, float * x, float * y, float * z);


    float _tmp_brightness;
    float _tmp_contrast;
    float _tmp_gamma;

};

#endif // CPOINTCLOUD_H
