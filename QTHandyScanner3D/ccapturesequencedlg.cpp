#include "ccapturesequencedlg.h"
#include "ui_ccapturesequencedlg.h"

#include <iomanip>
#include <QFileDialog>
#include <iostream>
#include "claserscanonlinedlg.h"

#include "./metadata/qexifimageheader.h"
#include "./metadata/qmetadata.h"

#include "settings/psettings.h"
#include <QMessageBox>
#include <QThread>
#include <QtTest/qtest.h>
#include "opencv_aux_utils.h"


float shift_angle_const = 270;


CCaptureSequenceDlg::CCaptureSequenceDlg(cv::VideoCapture *cam,
                                          float roll, float pitch,
                                          float rcx, float rcy, float rcz,
                                         float depth_can,
                                         PVOID dh, int eled, QWidget *parent) :
    _current_cam(cam), _dev_handle(dh), QDialog(parent),
    ui(new Ui::CCaptureSequenceDlg),
    _board_distance (0),   _timer_period(20), _capturing(false), _period_tics(0),
    _roll(roll), _pitch(pitch), _motor_cdir(false), _scan_sz(1),
    _rc_x (rcx), _rc_y(rcy), _rc_z(rcz),_max_radi_to_cancel_pnts(depth_can),
    _current_output_video (NULL), _non_add_tex(true), _current_video_desc(NULL), _eled(eled)
{
    ui->setupUi(this);

    connect(ui->btnStart, SIGNAL(clicked()), this, SLOT(start_scan()));
    connect(ui->btnStop, SIGNAL(clicked()), this, SLOT(stop()));


    ui->btnStop->setEnabled(false);

    connect(ui->btnBrowse, SIGNAL(clicked()), this, SLOT(browse()));

    ui->edtPath->setText("./scans");

    ui->lblInfo->setText("Frames: 0    FPS: 0");


    _vid_name  = "scan_%02d.avi";
    _desc_name = "scan_%02d.nfo";

    _dev_ctrl_thread = new QThread();
    _dev_ctrl = new CDevControlThread(_dev_handle);
    _dev_ctrl->moveToThread(_dev_ctrl_thread);

    connect(this, SIGNAL(dev_getLA()), _dev_ctrl_thread, SLOT(start()));
    connect(_dev_ctrl_thread, SIGNAL(started()), _dev_ctrl, SLOT(run()));
    connect(_dev_ctrl, SIGNAL(finished()), _dev_ctrl_thread, SLOT(quit()), Qt::DirectConnection);
    connect(_dev_ctrl, SIGNAL(finish_lasangle(int )), this, SLOT(new_laserangle(int)));

    ui->RadioButtonGroup->setExclusive(true);

    ui->chkOneSide->setChecked(true);
    ui->edtAnSta->setText("-10.0");
    ui->edtAnEnd->setText("10.0");


    ui->cmbMSides->setItemData(0, 4);
    ui->cmbMSides->setItemData(1, 6);
    ui->cmbMSides->setItemData(2, 8);
    ui->cmbMSides->setItemData(3, 10);

    connect(ui->btnSetStart, SIGNAL(clicked()), this, SLOT(set_start_angle()));
    connect(ui->btnSetEnd, SIGNAL(clicked()), this, SLOT(set_end_angle()));

    connect(ui->btnMotoRight, SIGNAL(pressed()),  this, SLOT(i2c_motor_right_pressed()));
    connect(ui->btnMotoRight, SIGNAL(released()), this, SLOT(i2c_motor_stop()));
    connect(ui->btnMotoLeft,  SIGNAL(pressed()),  this, SLOT(i2c_motor_left_pressed()));
    connect(ui->btnMotoLeft,  SIGNAL(released()), this, SLOT(i2c_motor_stop()));

    _tex_path = "/texture";


    ui->cmbAddTex->setItemData(0, 0);
    ui->cmbAddTex->setItemData(1, 18);
    ui->cmbAddTex->setItemData(2, 36);
}

void CCaptureSequenceDlg::single_timer_shoot()
{
    QTimer::singleShot(_timer_period, this, SLOT(capture_new_frame()));
}

void CCaptureSequenceDlg::read_and_update_lblA()
{
    int v = driv::get_laser_angle(_dev_handle);
    update_lblA(v);
}

float CCaptureSequenceDlg::convert_langle(int v)
{
    return float(v) * (360.f / 16384.f);
}

void CCaptureSequenceDlg::update_lblA(int v)
{
    float angle = convert_langle(v);
    ui->lblA->setText(QString::number(angle - shift_angle_const ));
}

void CCaptureSequenceDlg::i2c_motor_right_pressed()
{
    driv::motor_dir_change(_dev_handle,1);
    driv::motor_rotate_signal(_dev_handle, 1);
}

void CCaptureSequenceDlg::i2c_motor_left_pressed()
{
    driv::motor_dir_change(_dev_handle,2); // to the left
    driv::motor_rotate_signal(_dev_handle, 1); // start
}


void CCaptureSequenceDlg::i2c_motor_stop()
{
    driv::motor_rotate_signal(_dev_handle, 2);
}


void CCaptureSequenceDlg::set_start_angle()
{
    int v = driv::get_laser_angle(_dev_handle);
    float angle = convert_langle(v);
    ui->edtAnSta->setText(QString::number(angle - shift_angle_const ));
}

void CCaptureSequenceDlg::set_end_angle()
{

    int v = driv::get_laser_angle(_dev_handle);
    float angle = convert_langle(v);

    ui->edtAnEnd->setText(QString::number(angle - shift_angle_const ));
}

void CCaptureSequenceDlg::new_laserangle(int v)
{
    _laser_angle = v;
    _langle_ready = true;
}

void CCaptureSequenceDlg::capture_new_frame()
{

    clock_t msstart = clock();

    _langle_ready  = false;
    emit dev_getLA();

    float angle =  0;

    cv::Mat frame;
    bool bSuccess = _current_cam->read(frame);
    if (bSuccess)
    {
        int try_count = 0;
        while(!_langle_ready && (try_count < 5))
        {
            try_count ++;
            QEventLoop loop; QTimer::singleShot(5, &loop, SLOT(quit())); loop.exec();
        }

        /*if(_save_rate != 1)
        {
            if(_skipped_frames == _save_rate)
                _skipped_frames = 0;
            else
            {
                _skipped_frames++;
                if(_capturing)
                    single_timer_shoot();

                return;
            }
        }*/

        angle = convert_langle(_laser_angle);
        ui->lblA->setText(QString::number(angle - shift_angle_const ));


        QString msg =  QString("{'lpos': '%1', 'angle_': '%2', 'msec': '%3'}").arg(QString::number(_laser_angle)).arg(QString::number(angle)).arg(_total_tics);

       if(_current_output_video != NULL)
         _current_output_video->write(frame);
       if(_current_video_desc != NULL)
          (*_current_video_desc) <<  _frame_count << " f " << msg.toStdString() << std::endl;

        _frame_count++;

        int fps = _frame_count * CLOCKS_PER_SEC /(_total_tics+1);

        QString info =  QString("Frames: %1    FPS: %2").arg(QString::number(_frame_count )).arg(QString::number(fps));
         ui->lblInfo->setText(info);

         clock_t mffinish = clock();
        _total_tics  += (mffinish - msstart);
        _period_tics += (mffinish - msstart);

        if(_period_tics  > CLOCKS_PER_SEC/2)
        {
            _period_tics  = 0;
            emit update_main_preview(frame);
        }
    }

    if(_scan_sz == 1)
    {
        if(_capturing)
            single_timer_shoot();
    }
    else
    {

        bool cont_condition =   (( _motor_cdir) && (angle > _angle_min) ||
                                (!_motor_cdir) && (angle < _angle_max)) && (_frame_count < 4000)   ; // (_frame_count < 70)

        /// should we continue
        if(cont_condition)
        {
            if(_capturing)
                single_timer_shoot();
        }
        ///// done with the current scan
        else
        {
            if(_non_add_tex)
                record_texure();


            rotate_sample_table();
            if(_scan_num == _scan_sz)
                stop();
            else
                start();
        }
    }
}

void CCaptureSequenceDlg::record_additional_texture()
{
    if(_dev_handle != NULL)
    {

        driv::laser_turn_off(_dev_handle);
        driv::set_ELED_brightness(_dev_handle, 100);
        QEventLoop loop; QTimer::singleShot(3500, &loop, SLOT(quit())); loop.exec();
    }
    cv::Mat frame;
    _current_cam->read(frame);

    int tex_nums = ui->cmbAddTex->currentData().toInt();
    for(int tex_id = 0; tex_id < tex_nums; tex_id++ )
    {
        _current_cam->read(frame);
        _current_cam->read(frame);

        QString frmt = _prefix + _tex_path + "/%1.png";
        QString tname = QString(frmt).arg(QString::number(tex_id));
        cv::imwrite(tname.toStdString(), frame);

        if(_dev_handle != NULL)
        {
            emit start_main_preview();

            int angle = 360./tex_nums;
            driv::turn_table_request(_dev_handle, angle);

            driv::motor_rotate_signal(_dev_handle, 2);

            int total_delay = 3000 + 4000 /tex_nums ;
            QEventLoop loop;
            QTimer::singleShot(total_delay, &loop, SLOT(quit())); loop.exec();

            emit stop_main_preview();
        }
    }

    // turn off led
    if(_dev_handle != NULL)
        driv::set_ELED_brightness(_dev_handle, _eled);

}


void CCaptureSequenceDlg::record_texure()
{
    // turn off laser
    // turn on led
    if(_dev_handle != NULL)
    {

        driv::laser_turn_off(_dev_handle);
        driv::set_ELED_brightness(_dev_handle, 100);
        QEventLoop loop; QTimer::singleShot(2000, &loop, SLOT(quit())); loop.exec();
    }

    // record file
    cv::Mat frame;
     _current_cam->read(frame);
     _current_cam->read(frame);

    QString frmt = _prefix + _tex_path + "/%1.png";
    QString tname = QString(frmt).arg(QString::number(_scan_num));
    cv::imwrite(tname.toStdString(), frame);

    // turn off led
    if(_dev_handle != NULL)
        driv::set_ELED_brightness(_dev_handle, _eled);
}


void CCaptureSequenceDlg::rotate_sample_table()
{
    if(_dev_handle != NULL)
    {
        emit start_main_preview();

        int angle = 360./_scan_sz;
        driv::turn_table_request(_dev_handle, angle);

        driv::motor_rotate_signal(_dev_handle, 2);
        driv::laser_turn_off(_dev_handle);

        int total_delay = 2000 + 4000 /_scan_sz ;
        QEventLoop loop;
        QTimer::singleShot(total_delay, &loop, SLOT(quit())); loop.exec();

        emit stop_main_preview();
    }
}

CCaptureSequenceDlg::~CCaptureSequenceDlg()
{
    delete ui;
    clean_video_structures();
}

void CCaptureSequenceDlg::browse()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                 "./",
                                                 QFileDialog::ShowDirsOnly
                                                 | QFileDialog::DontResolveSymlinks);
    if(!dir.isEmpty())
        ui->edtPath->setText(dir);
}

void CCaptureSequenceDlg::start_scan()
{
    _scan_num = 0;

    disable_interface();

    if(ui->chkOneSide->isChecked())
    {
        _scan_sz =  1;

        // dont nead it since we have laser parking
        if(ui->cmbMotorLR->currentText() == "Left")
           _motor_cdir = false;
        else
            _motor_cdir = true;
    }
    else
    {
        _scan_sz = ui->cmbMSides->currentData().toInt();
        _motor_cdir = true;

        _angle_min = ui->edtAnSta->text().toFloat() + shift_angle_const ; // + 270 to internal conv
        _angle_max = ui->edtAnEnd->text().toFloat() + shift_angle_const ;

        move_laser_start_position();
    }

    _prefix = ui->edtPath->text();

    QDir scan_dir(_prefix);
    if(!scan_dir.exists())
    {
         QDir().mkdir(_prefix);
    }
    else
    {
        scan_dir.setNameFilters(QStringList()<<"scan_*.avi");
        QFileInfoList list = scan_dir.entryInfoList();

        if(list.size() > 0)
        {
            QString msg = "Scan data exists in <br>" + _prefix +  " Overwrite?";

           QMessageBox::StandardButton reply = QMessageBox::question(this, "Warning", msg,
                                           QMessageBox::Yes|QMessageBox::No);
            if (reply == QMessageBox::No)
            {
                stop();
                return;
            }
            foreach(QString dirFile, scan_dir.entryList())
            {
                scan_dir.remove(dirFile);
            }
         }
    }

    QDir tex_dir(_prefix + _tex_path);
    if(!tex_dir.exists())
         QDir().mkdir(_prefix + _tex_path);

    if(ui->cmbAddTex->currentData() == 0)
        _non_add_tex = true;
    else
        _non_add_tex = false;

    emit stop_main_preview();
    start();
}


void CCaptureSequenceDlg::move_laser_start_position()
{
    if(_dev_handle == NULL) return;

    int v = driv::get_laser_angle(_dev_handle);
    float angle = convert_langle(v);

    float min_angle = ui->edtAnSta->text().toFloat() + shift_angle_const;

    if(angle < min_angle) // move to right
    {
        driv::motor_dir_change(_dev_handle,1);
        driv::motor_rotate_signal(_dev_handle, 1);

        while(angle < min_angle)
        {
            v = driv::get_laser_angle(_dev_handle);
            angle = convert_langle(v);

            QEventLoop loop; QTimer::singleShot(15, &loop, SLOT(quit())); loop.exec();
        }
    }
    else // move to left
    {
        driv::motor_dir_change(_dev_handle,2); // to the left
        driv::motor_rotate_signal(_dev_handle, 1); // start
        while(angle > min_angle)
        {
            v = driv::get_laser_angle(_dev_handle);
            angle = convert_langle(v);


            QEventLoop loop; QTimer::singleShot(15, &loop, SLOT(quit())); loop.exec();
        }
    }
    driv::motor_rotate_signal(_dev_handle, 2);
}

void CCaptureSequenceDlg::disable_interface()
{
    ui->lblInfo->setText("Frames: 0    FPS: 0");
    ui->btnStart->setEnabled(false);
    ui->btnBrowse->setEnabled(false);
    ui->btnStop->setEnabled(true);
    ui->cmbMotorLR->setEnabled(false);
    ui->edtAnSta->setEnabled(false);
    ui->edtAnEnd->setEnabled(false);
    ui->chkMulSide->setEnabled(false);
    ui->chkOneSide->setEnabled(false);
    ui->cmbMSides->setEnabled(false);
    ui->btnSetStart->setEnabled(false);
    ui->btnSetEnd->setEnabled(false);
    ui->btnMotoLeft->setEnabled(false);
    ui->btnMotoRight->setEnabled(false);
    ui->cmbAddTex->setEditable(false);

}

void CCaptureSequenceDlg::enable_interface()
{
    ui->btnStart->setEnabled(true);
    ui->btnBrowse->setEnabled(true);
    ui->btnStop->setEnabled(false);
    ui->cmbMotorLR->setEnabled(true);
    ui->edtAnSta->setEnabled(true);
    ui->edtAnEnd->setEnabled(true);
    ui->chkMulSide->setEnabled(true);
    ui->chkOneSide->setEnabled(true);
    ui->cmbMSides->setEnabled(true);
    ui->btnSetStart->setEnabled(true);
    ui->btnSetEnd->setEnabled(true);
    ui->btnMotoLeft->setEnabled(true);
    ui->btnMotoRight->setEnabled(true);
    ui->cmbAddTex->setEditable(true);
}

void CCaptureSequenceDlg::clean_video_structures()
{
    if(_current_output_video != NULL)
     {
         _current_output_video->release();
          delete _current_output_video;
         _current_output_video = NULL;
     }

     if(_current_video_desc != NULL)
     {
         _current_video_desc->close();
         delete _current_video_desc;
         _current_video_desc = NULL;
     }

}

void CCaptureSequenceDlg::start()
{

    _scan_num++;
    _motor_cdir = !_motor_cdir;


    _capturing   = true;
    _frame_count = 0;
    _total_tics  = 0;

     //// turn on LEDS
    if(_dev_handle != NULL)
    {
         driv::set_M0(_dev_handle, 3); // 1 - fast, 3 - slow
         driv::set_M1(_dev_handle, 1);

         driv::set_RGBLed_X(_dev_handle, 0x21, 15, 0, 10);
         driv::set_RGBLed_X(_dev_handle, 0x22, 15, 0, 10);
         driv::set_RGBLed_X(_dev_handle, 0x23, 15, 0, 10);

         driv::laser_turn_off(_dev_handle);

        // wait until it is cleared
        QEventLoop loop; QTimer::singleShot(1500, &loop, SLOT(quit())); loop.exec();
    }


    /// setup current video-stream and corresponding description

    clean_video_structures();

    char * pattern_name_buf = new char[4096];
    std::sprintf(pattern_name_buf, _vid_name.toStdString().c_str(), _scan_num);
     QString vname = _prefix + QString("/") + QString(pattern_name_buf);

     std::cout << ":::: " << vname.toStdString() << std::endl;

    _current_output_video = new cv::VideoWriter(vname.toLocal8Bit().constData(),
                                                CV_FOURCC('D','I','V','X'),
                                                 30,
                                                cv::Size(_current_cam->get(CV_CAP_PROP_FRAME_WIDTH),
                                                         _current_cam->get(CV_CAP_PROP_FRAME_HEIGHT)),
                                                    true);
    if(!_current_output_video->isOpened())
    {
        QString msg("Cannot start video recording!");
        QMessageBox::information(this, tr("Error"), msg);
        return;
    }

    std::sprintf(pattern_name_buf, _desc_name.toStdString().c_str(), _scan_num);
    QString dname = _prefix + QString("/") + QString(pattern_name_buf);
    _current_video_desc = new std::ofstream (dname.toLocal8Bit().constData(), std::ofstream::out);

    delete [] pattern_name_buf;

    //////////////// capture background

    cv::Mat frame;
    while(!_current_cam->read(frame));
    while(!_current_cam->read(frame)); // fix: drop first bad frame

    _current_output_video->write(frame);
   (*_current_video_desc) <<  _frame_count++ << " t" << std::endl;

    _current_output_video->write(frame);
   (*_current_video_desc) <<  _frame_count++ << " b" << std::endl;


    /////////////// turn on laser
    if(_dev_handle != NULL)
    {
        driv::laser_turn_on(_dev_handle);

        if(_motor_cdir)
            driv::motor_dir_change(_dev_handle,2); // to the left
        else
            driv::motor_dir_change(_dev_handle,1); // to the right

        driv::motor_rotate_signal(_dev_handle, 1); // start
    }

    _skipped_frames = 0;

    single_timer_shoot();
}

void CCaptureSequenceDlg::stop_scan()
{
}

void CCaptureSequenceDlg::stop()
{
    if(!_non_add_tex)
        record_additional_texture();

    _capturing = false;

    std::cout << "Inside capture " << _inside_capture << std::endl;
    clean_video_structures();

    ///////// stop motor
    if(_dev_handle != NULL)
    {
        driv::motor_rotate_signal(_dev_handle, 2);

        driv::set_M0(_dev_handle, 1);
        driv::set_M1(_dev_handle, 2);

        driv::set_RGBLed_X(_dev_handle, 0x21, 0, 15, 0);
        driv::set_RGBLed_X(_dev_handle, 0x22, 0, 15, 0);
        driv::set_RGBLed_X(_dev_handle, 0x23, 0, 15, 0);
    }

    write_INI();

    if(_dev_handle != NULL)
    {
        /// flush leds
        for(int k = 0; k < 2; k++)
        {
            driv::set_ELED_brightness(_dev_handle, 100);
            QEventLoop loop; QTimer::singleShot(350, &loop, SLOT(quit())); loop.exec();
            driv::set_ELED_brightness(_dev_handle, 0);
            loop; QTimer::singleShot(350, &loop, SLOT(quit())); loop.exec();
        }

    }

    if(_dev_handle != NULL)
        driv::set_ELED_brightness(_dev_handle, _eled);

    enable_interface();

    emit start_main_preview();

    if(!ui->chkOneSide->isChecked())
    {
        QString msg("Scan Successfully Completed");
        QMessageBox::information(this, "Information", msg);
        closedlg();
    }
}

void  CCaptureSequenceDlg::write_INI()
{
     QString ini_name = _prefix  + QString("/") + QString("description.ini");
     std::ofstream inif (ini_name.toLocal8Bit().constData(), std::ofstream::out);

     inif << "video_name_format="<< _vid_name.toStdString() << std::endl;
     inif << "video_desc_format="<< _desc_name.toStdString() << std::endl;
     inif << "video_name_has_scan=1" << std::endl;
     inif << "first_scan=1" << std::endl;
     inif << "last_scan="<< _scan_sz << std::endl;
     inif << "scan_angle_step=" << 360.0/_scan_sz << std::endl;
     inif << "is_difference=0" << std::endl;
     inif << "camera_x_incline=" << _pitch << std::endl;
     inif << "camera_y_incline=" << -_roll << std::endl;
     inif << "camera_laser_dist=" << g_camera_laser_dist << std::endl;
     inif << "use_metadata_angle=1" << std::endl;
     inif << "meta_angle_correction=" << g_meta_angle_correction << std::endl;
     inif << "camera_laser_interchanged=1" << std::endl;
     inif << "laser_depth_offset=" << g_laser_depth_offset << std::endl;
     inif << "max_depth_to_cancel_pnts="<<_max_radi_to_cancel_pnts<< std::endl;
     inif << "rot_center_x=" << _rc_x << std::endl;
     inif << "rot_center_y=" << _rc_y << std::endl;
     inif << "rot_center_z=" << _rc_z << std::endl;


     inif << "texture_img_fmt=" << _tex_path.toStdString() << "/%1.png" << std::endl;
     inif << "texture_img_cnts=" <<  ((_non_add_tex)? _scan_num : ui->cmbAddTex->currentData().toInt()) << std::endl;


     std::map<int, FocusData>::iterator it =  g_callibration_map._map.find(CGlobalSettings::_webc_focus);
     if( it !=  g_callibration_map._map.end())
     {
         FocusData cam_dt = it->second;
         cv::Mat cammtx = cam_dt._cm;
         cv::Mat dcmtx = cam_dt._dc;

         inif << "is_callibrated=1"<< std::endl;

         inif << "fx="<< std::setprecision(12) <<  cammtx.at<double>(0,0) << std::endl;
         inif << "cx="<< std::setprecision(12) <<  cammtx.at<double>(0,2) << std::endl;
         inif << "fy="<< std::setprecision(12) <<  cammtx.at<double>(1,1) << std::endl;
         inif << "cy="<< std::setprecision(12) <<  cammtx.at<double>(1,2) << std::endl;

         inif << "dist1="<< std::setprecision(12) <<  dcmtx.at<double>(0,0) << std::endl;
         inif << "dist2="<< std::setprecision(12) <<  dcmtx.at<double>(0,1) << std::endl;
         inif << "dist3="<< std::setprecision(12) <<  dcmtx.at<double>(0,2) << std::endl;
         inif << "dist4="<< std::setprecision(12) <<  dcmtx.at<double>(0,3) << std::endl;
         inif << "dist5="<< std::setprecision(12) <<  dcmtx.at<double>(0,4) << std::endl;
     }
     else
     {
         inif << "is_callibrated=0"<< std::endl;
     }
     inif.close();
}


void CCaptureSequenceDlg::closedlg()
{
    _capturing = false;
    close();
}

void CCaptureSequenceDlg::hideEvent(QHideEvent * event)
{

    _capturing = false;
}

void CCaptureSequenceDlg::showEvent(QShowEvent * event)
{
    read_and_update_lblA();
    driv::laser_turn_on(_dev_handle);
}
