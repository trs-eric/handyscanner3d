#ifndef CFRAMEPREPTHREAD_H
#define CFRAMEPREPTHREAD_H

#include <QThread>
#include <QMutex>
#include <QWaitCondition>

#include "cinputdatamanager.h"

class CFramePrepThread : public QThread
{
    Q_OBJECT
public:
    explicit CFramePrepThread(QObject *parent = 0);
    void start_process(QMutex * smutex, QWaitCondition * wcond);

///signals:
///    void completed();

public slots:

protected:
     void run();

    QMutex * _syncro_mutex;
    QWaitCondition * _wait_cond_suffice_keyframes;

    CInputDataManager _input_data_manager;


};

#endif // CFRAMEPREPTHREAD_H
