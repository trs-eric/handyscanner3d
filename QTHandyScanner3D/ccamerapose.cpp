#include "ccamerapose.h"
#include "settings/psettings.h"

CCameraPose::CCameraPose(bool vis)
{
    _rx = 0.5f;
    _ry = 0.5f;
    _rz = 0.5f;

    _color[0] = 0.f;
    _color[1] = 1.f;
    _color[2] = 0.f;

    _irotx = 0.0f;
    _iroty = 90.0f;
    _irotz = 0.0f;

    isVisible = vis;
}

CCameraPose::~CCameraPose()
{

}


void CCameraPose::draw_camera()
{
    glBegin(GL_LINES);
        glColor4f(_color[0], _color[1],  _color[2], CGlobalHSSettings::gl_cam_alpha);
        glVertex3f(1, -0.5, -0.5);
        glColor4f(_color[0], _color[1],  _color[2], CGlobalHSSettings::gl_cam_alpha);
        glVertex3f(1, 0.5, -0.5);

        glColor4f(_color[0], _color[1],  _color[2], CGlobalHSSettings::gl_cam_alpha);
        glVertex3f(1, 0.5, -0.5);
        glColor4f(_color[0], _color[1],  _color[2], CGlobalHSSettings::gl_cam_alpha);
        glVertex3f(1, 0.5, 0.5);

        glColor4f(_color[0], _color[1],  _color[2], CGlobalHSSettings::gl_cam_alpha);
        glVertex3f(1, 0.5, 0.5);
        glColor4f(_color[0], _color[1],  _color[2], CGlobalHSSettings::gl_cam_alpha);
        glVertex3f(1, -0.5, 0.5);

        glColor4f(_color[0], _color[1],  _color[2], CGlobalHSSettings::gl_cam_alpha);
        glVertex3f(1, -0.5, 0.5);
        glColor4f(_color[0], _color[1],  _color[2], CGlobalHSSettings::gl_cam_alpha);
        glVertex3f(1, -0.5, -0.5);

        glColor4f(_color[0], _color[1],  _color[2], CGlobalHSSettings::gl_cam_alpha);
        glVertex3f(0, 0, 0);
        glColor4f(_color[0], _color[1],  _color[2], CGlobalHSSettings::gl_cam_alpha);
        glVertex3f(1, -0.5, -0.5);

        glColor4f(_color[0], _color[1],  _color[2], CGlobalHSSettings::gl_cam_alpha);
        glVertex3f(0, 0, 0);
        glColor4f(_color[0], _color[1],  _color[2], CGlobalHSSettings::gl_cam_alpha);
        glVertex3f(1, 0.5, -0.5);

        glColor4f(_color[0], _color[1],  _color[2], CGlobalHSSettings::gl_cam_alpha);
        glVertex3f(0, 0, 0);
        glColor4f(_color[0], _color[1],  _color[2], CGlobalHSSettings::gl_cam_alpha);
        glVertex3f(1, 0.5, 0.5);

        glColor4f(_color[0], _color[1],  _color[2], CGlobalHSSettings::gl_cam_alpha);
        glVertex3f(0, 0, 0);
        glColor4f(_color[0], _color[1],  _color[2], CGlobalHSSettings::gl_cam_alpha);
        glVertex3f(1, -0.5, 0.5);

        glColor4f(1.f, 1.f, 1.f, 1.f);

        // arror
/*		glColor3f(1, 0,  0);
        glVertex3f(0, 0, 0);
        glColor3f(1, 0,  0);
        glVertex3f(1.0f, 0, 0);*/

    glEnd();

}

void CCameraPose::draw_tri()
{
        glBegin(GL_LINES);
        // x axis
        glColor3f(1, 0,  0);
        glVertex3f(0, 0, 0);
        glColor3f(1, 0,  0);
        glVertex3f(.5, 0, 0);

        // y axis
        glColor3f(0, 1,  0);
        glVertex3f(0, 0, 0);
        glColor3f(0, 1,  0);
        glVertex3f(0, .5, 0);

        // z axis
        glColor3f(0, 0,  1);
        glVertex3f(0, 0, 0);
        glColor3f(0, 0,  1);
        glVertex3f(0, 0, -0.5);

        glEnd();

}

void CCameraPose::create_glList()
{
     delete_glLists();

     // create new display list
     _oglid = glGenLists(1);
     glNewList(_oglid, GL_COMPILE);
        glEnable( GL_BLEND );
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glDisable(GL_LIGHTING);
            draw_camera();
         glEnable(GL_LIGHTING);
         glDisable(GL_BLEND);
      glEndList();

};


