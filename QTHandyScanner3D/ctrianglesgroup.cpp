#include "ctrianglesgroup.h"
#include "cframe.h"

#include "./settings/psettings.h"

#include <set>

CTrianglesGroup::CTrianglesGroup() : _frame(NULL), _texture(NULL), _corrected_colors(false)
{
    _tex_u0i = 0;
    _tex_v0i = 0;
    _tex_u1i = 0;
    _tex_v1i = 0;

    _texture_pos_x = 0;
    _texture_pos_y = 0;
}

CTrianglesGroup::CTrianglesGroup( std::list<int> & l) : _frame(NULL), _texture(NULL),
    _corrected_colors(false)
{

    _face_group = l;
    _tex_u0i = 0;
    _tex_v0i = 0;
    _tex_u1i = 0;
    _tex_v1i = 0;

    _texture_pos_x = 0;
    _texture_pos_y = 0;
}

CTrianglesGroup::~CTrianglesGroup()
{
   if(_texture != NULL)
       delete _texture;
}

void CTrianglesGroup::merge(CTrianglesGroup * sg)
{

    /// _face_group.insert(_face_group.end(), sg->_face_group.begin(),sg->_face_group.end());

    std::list<int>::iterator itff = sg->_face_group.begin();
    for( ; itff != sg->_face_group.end();  itff++)
         _face_group.push_back(*itff);


}

void CTrianglesGroup::compute_tex_uv()
{
    if(_frame == NULL) return;
    /// find unique points in the triangular group
    std::set<int> cg_points;
    std::list<int>::iterator itff = _face_group.begin();
    for( ; itff != _face_group.end();  itff++)
        for (int p = 0; p < 3; p++)
            cg_points.insert(_mesh->_triangles[*itff].pnt_index[p]);


    std::cout << "find " << cg_points.size() << " unique points" << std::endl;

    /// find points projections and tex coords
    cv::Mat forwR = _frame->get_global_rotation_matrix_forw();

    float cam_x = _frame->_global_pos.at<double>(0,0);
    float cam_y = _frame->_global_pos.at<double>(1,0);
    float cam_z = _frame->_global_pos.at<double>(2,0);

    cv::Mat poseT = -forwR * _frame->_global_pos;
    cv::Mat P = (cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2), poseT.at<double>(0,0) ,
                                          forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2), poseT.at<double>(1,0) ,
                                          forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2), poseT.at<double>(2,0));

    cv::Mat camLK_P = CGlobalHSSettings::_camL_K * P;


   std::map<int, int> p_texcoords;
    std::set<int>::iterator itp = cg_points.begin();
    for( ; itp != cg_points.end(); itp++)
    {
        C3DPoint pp = _mesh->_points[ *itp ];
        float px = (pp._x - CGlobalHSSettings::glob_gl_offx)/   CGlobalHSSettings::glob_gl_scale;
        float py = (pp._y - CGlobalHSSettings::glob_gl_offy)/ (-CGlobalHSSettings::glob_gl_scale);
        float pz = (pp._z - CGlobalHSSettings::glob_gl_offz)/ (-CGlobalHSSettings::glob_gl_scale);

        /// get 3d point in pcl1
        cv::Mat pp_mat = (cv::Mat_<double>(4,1) << px, py, pz,1);
        cv::Mat uv = camLK_P * pp_mat;
        uv  /= uv.at<double>(2,0);

        double u = uv.at<double>(0,0);
        double v = uv.at<double>(1,0);

        double norm_u = (u - _tex_u0i)/ double( (_tex_u1i-_tex_u0i) +  1.);
       ///  std::cout << norm_u  << " ";
        norm_u  = std::max(norm_u,0.);
        norm_u  = std::min(norm_u,1.);

        double norm_v =  (v - _tex_v0i)/double( (_tex_v1i - _tex_v0i) + 1.);
       ///  std::cout << norm_v  << " ";
        norm_v  = std::max(norm_v,0.);
        norm_v  = std::min(norm_v,1.);


        Vector2f tex_coord;
        tex_coord.x = norm_u;
        tex_coord.y = norm_v;

        /// std::cout << " tex coord " <<  tex_coord.x  << " " << tex_coord.y << " " << *itp << std::endl;
        p_texcoords[*itp] = _mesh->_tex_coords.size();
        _mesh->_tex_coords.push_back(tex_coord);
    }

    /// set texture coordinates
    itff = _face_group.begin();
    for( ; itff != _face_group.end();  itff++)
        for (int p = 0; p < 3; p++)
            _mesh->_triangles[*itff].tex_index[p] = p_texcoords[ _mesh->_triangles[*itff].pnt_index[p]];

}

void CTrianglesGroup::construct_bound_groups(std::vector<CTrianglesGroup*> & group_ref)
{
    //std::map<CTrianglesGroup*, QRgb> cmap;

    std::list<int>::iterator itf =  _face_group.begin();
    for( ; itf != _face_group.end() ;itf++)
    {

        for (int p = 0; p < 3; p++)
        {
            int tidx = _mesh->_triangles[*itf].tex_index[p];
            int pidx = _mesh->_triangles[*itf].pnt_index[p];

            /// std::cout <<  tidx  << "/" << pidx <<" " << _mesh->_tex_coords.size()<< " "<<  tex_coord.x << " " << tex_coord.y << " " << _texture->width() << " " << _texture->height() << std::endl;
            bool belong_to_other_group = false;
            CTrianglesGroup* other_group = NULL;

            QRgb my_color;
            QRgb other_color;

            std::vector<int>::iterator itfm = _mesh->_points[ _mesh->_triangles[*itf].pnt_index[p] ]._faceidx.begin();
            for( ; itfm != _mesh->_points[ _mesh->_triangles[*itf].pnt_index[p] ]._faceidx.end(); itfm++)
            {
                if( (group_ref[*itfm] != this) && (group_ref[*itfm] != NULL)  )
                {
                    // find texture coordinates
                    int other_tri_id = *itfm;
                    int pp = 0;
                    for (; pp < 3; pp++)
                       if( _mesh->_triangles[other_tri_id].pnt_index[pp] == pidx)
                           break;
                    if(pp >= 3)
                        continue;

                    double pix_x =  _mesh->_tex_coords[tidx].x * double(_texture->width());
                    double pix_y =  _mesh->_tex_coords[tidx].y * double(_texture->height());
                    if(pix_x >=_texture->width() )pix_x=_texture->width() -1;
                    if(pix_y >=_texture->height() )pix_y=_texture->height() -1;
                    my_color = _texture->pixel(pix_x, pix_y);
                    other_group = group_ref[*itfm];
                    belong_to_other_group = true;


                    /*
                    QRgb valueO;
                    std::map<CTrianglesGroup*, QRgb>::iterator itm = cmap.find(other_group);
                    if(itm == cmap.end())
                    {
                        uchar red = float(rand() % 255);
                        uchar green = float(rand() % 255);
                        uchar blue = float(rand() % 255);
                        cmap[other_group] = QRgb(red,green,blue);
                        valueO = QRgb(red,green,blue);
                    }
                    else
                    {
                        valueO = itm->second;
                    }

                    _texture->setPixel(pix_x, pix_y, valueO);
                    */

                    Vector2f tex_coord_n = _mesh->_tex_coords[_mesh->_triangles[other_tri_id].tex_index[pp]];
                    double pix_x2 =  tex_coord_n.x * double(other_group->_texture->width());
                    double pix_y2 =  tex_coord_n.y * double(other_group->_texture->height());
                    if(pix_x2 >=other_group->_texture->width() )pix_x2=other_group->_texture->width() -1;
                    if(pix_y2  >=other_group->_texture->height())pix_y2=other_group->_texture->height() -1;

                    other_color = other_group->_texture->pixel(pix_x2, pix_y2);

                    //other_group->_texture->setPixel(pix_x2, pix_y2, valueO);
                    ///std::cout << tex_coord_n.x << " " << tex_coord_n.y <<" " << pix_x2 << " " << pix_y2 << std::endl;

                    break;
                }
            }

            if(belong_to_other_group)
            {
                _bound_maps[other_group].insert(std::make_pair(my_color,other_color));
            }
        }
    }

    std::cout << this << " neighbors " << _bound_maps.size() << std::endl;
}


bool vec3f_sort_dist(const Vector3f & v1, const Vector3f & v2)
{
    double v1_len = v1.x*v1.x +v1.y*v1.y+v1.z*v1.z;
    double v2_len = v2.x*v2.x +v2.y*v2.y+v2.z*v2.z;
    return v1_len < v2_len;
}

void CTrianglesGroup::make_texcolor_correction()
{

    _corrected_colors = true;

   std::map<CTrianglesGroup*, std::set<std::pair<QRgb,QRgb> > >::iterator it = _bound_maps.begin();
   for( ; it != _bound_maps.end(); it++)
   {


       if( it->first->_corrected_colors)
           continue;

       std::vector<Vector3f> color_diff;
       std::set<std::pair<QRgb,QRgb> >::iterator its = it->second.begin();
       for( ; its != it->second.end(); its++)
       {
           QRgb c1 = its->first;
           QRgb c2 = its->second;
           double rc1 = qRed(c1);
           double gc1 = qGreen(c1);
           double bc1 = qBlue(c1);

           double rc2 = qRed(c2);
           double gc2 = qGreen(c2);
           double bc2 = qBlue(c2);


           Vector3f res;
           res.x = rc1 - rc2;
           res.y = gc1 - gc2;
           res.z = bc1 - bc2;

           /// double dist = std::sqrt((rc1-rc2)*(rc1-rc2) + (gc1-gc2)*(gc1-gc2) + (bc1-bc2)*(bc1-bc2));

           color_diff.push_back(res);
       }
       std::sort(color_diff.begin(), color_diff.end(), vec3f_sort_dist);


       int vsz = color_diff.size();
       double cutoff_min = double(vsz) * 0.2;
       double cutoff_max = double(vsz) * 0.8;
       double rfix = 0;
       double gfix = 0;
       double bfix = 0;
       for(int i = cutoff_min, count = 0; i <  cutoff_max; i++, count++ )
       {
           Vector3f coldif = color_diff[i];
           rfix += coldif.x;
           gfix += coldif.y;
           bfix += coldif.z;
       }
       rfix /= count;
       gfix /= count;
       bfix /= count;
       std::cout << this << " color correction " << rfix << " " << gfix << " " << bfix << std::endl;
       correct_tex_colors(it->first, rfix, gfix, bfix);
       it->first->_corrected_colors = true;

   }

}

/*
void CTrianglesGroup::make_texcolor_correction()
{

    _corrected_colors = true;

   std::map<CTrianglesGroup*, std::set<std::pair<QRgb,QRgb> > >::iterator it = _bound_maps.begin();
   for( ; it != _bound_maps.end(); it++)
   {
       if( it->first->_corrected_colors)
           continue;

       std::vector<double> color_diff;
       std::set<std::pair<QRgb,QRgb> >::iterator its = it->second.begin();
       for( ; its != it->second.end(); its++)
       {
           QRgb c1 = its->first;
           QRgb c2 = its->second;
           double rc1 = qRed(c1);
           double gc1 = qGreen(c1);
           double bc1 = qBlue(c1);

           double br1 =  0.2989*rc1 + 0.5870*gc1 + 0.1140*bc1;

           double rc2 = qRed(c2);
           double gc2 = qGreen(c2);
           double bc2 = qBlue(c2);

           double br2 = 0.2989*rc2 + 0.5870*gc2 + 0.1140*bc2;

           color_diff.push_back(br1-br2);
       }
       std::sort(color_diff.begin(), color_diff.end());


       int vsz = color_diff.size();
       double cutoff_min = double(vsz) * 0.2;
       double cutoff_max = double(vsz) * 0.8;
       double br_av = 0;
       for(int i = cutoff_min, count = 0; i <  cutoff_max; i++, count++ )
           br_av += color_diff[i];
       br_av /= count;

       std::cout << this << " color correction " << br_av << std::endl;
       correct_tex_colors(it->first, br_av , br_av , br_av );
       it->first->_corrected_colors = true;

   }
}*/

/*void CTrianglesGroup::find_bound_points(std::vector<CTrianglesGroup*> & group_ref)
{
    std::list<int>::iterator itf =  _face_group.begin();
    for( ; itf != _face_group.end() ;itf++)
    {

        for (int p = 0; p < 3; p++)
        {
            std::vector<int>::iterator itfm = _mesh->_points[ _mesh->_triangles[*itf].pnt_index[p] ]._faceidx.begin();
            for( ; itfm != _mesh->_points[ _mesh->_triangles[*itf].pnt_index[p] ]._faceidx.end(); itfm++)
            {
               if(group_ref[*itfm] != this ) // not the same triangle group
               {
                    _bound_points.insert(_mesh->_triangles[*itf].tex_index[p]);
               }
            }
        }
    }

}*/

/*
bool cmp_colors(const QRgb & c1,const  QRgb & c2)
{
    double r1 =   qRed(c1);
    double g1 = qGreen(c1);
    double b1 =  qBlue(c1);

    double r2 =   qRed(c2);
    double g2 = qGreen(c2);
    double b2 =  qBlue(c2);

    if(r1 < r2)
        return true;
    if(r1 > r2)
        return false;

    if(g1 < g2)
         return true;
    if(g1 > g2)
         return false;

    if(b1 < b2)
         return true;
    else
        return false;
}

void CTrianglesGroup::compute_bound_points_color()
{
    std::cout << _bound_points.size() << " vs " << _face_group.size()*3 << std::endl;

    std::vector<QRgb> colors;
    std::set<int>::iterator it =  _bound_points.begin();
    for( ; it !=  _bound_points.end(); it++)
    {
           Vector2f tex_coord = _mesh->_tex_coords[*it];

           colors.push_back(_texture->pixel(tex_coord.x, tex_coord.y));
    }

    std::sort(colors.begin(), colors.end(), cmp_colors);


    int vsz = colors.size();
    double cutoff_min = double(vsz) * 0.2;
    double cutoff_max = double(vsz) * 0.8;
    _av_r = 0;
    _av_g = 0;
    _av_b = 0;

    for(int i = cutoff_min, count = 0; i <  cutoff_max; i++, count++ )
    {
        QRgb c = colors[i];

        _av_r += qRed(c);
        _av_g += qGreen(c);
        _av_b += qBlue(c);
    }
    _av_r /= count;
    _av_g /= count;
    _av_b /= count;


}

void CTrianglesGroup::correct_tex_colors(double br)
{
    double bright_curr = 0.2989 * _av_r  + 0.5870 * _av_g + 0.1140 * _av_b;
    double delta = (br - bright_curr)/1.5;
    std::cout << "correction delta " << delta << std::endl;
    int xs   = _texture->width();
    int ys   = _texture->height();

    QRgb * iYLine   = (QRgb*)_texture->scanLine(0);
    QRgb * oYLine   = (QRgb*)_texture->scanLine(0);

     for(int y = 0; y < ys; y++)
    {
        for(int x =0; x < xs; x++ )
        {
            QRgb p0 = iYLine[y*xs + x];
            int p0r = qRed(p0);
            int p0g = qGreen(p0);
            int p0b = qBlue(p0);

            int pr = changeBrightness(p0r, delta);
            int pg = changeBrightness(p0g, delta);
            int pb = changeBrightness(p0b, delta);

            oYLine[y*xs+x] = qRgb(pr, pg, pb);

        }
     }


}
*/

void CTrianglesGroup::correct_tex_colors(CTrianglesGroup * tg, double br_r, double br_g, double br_b)
{

    double delta_r = br_r;
    double delta_g = br_g;
    double delta_b = br_b;

    int xs   = tg->_texture->width();
    int ys   = tg->_texture->height();

    QRgb * iYLine   = (QRgb*)tg->_texture->scanLine(0);
    QRgb * oYLine   = (QRgb*)tg->_texture->scanLine(0);

    for(int y = 0; y < ys; y++)
    {
        for(int x =0; x < xs; x++ )
        {
            QRgb p0 = iYLine[y*xs + x];
            int p0r = qRed(p0);
            int p0g = qGreen(p0);
            int p0b = qBlue(p0);

            int pr = changeBrightness(p0r, delta_r);
            int pg = changeBrightness(p0g, delta_g);
            int pb = changeBrightness(p0b, delta_b);

            oYLine[y*xs+x] = qRgb(pr, pg, pb);

        }
     }
}


void CTrianglesGroup::recompute_uv(QImage * tex_atlas)
{
    if(_frame == NULL) return;


    std::vector<bool> tex_ready( _mesh->_tex_coords.size(), false);
     std::list<int>::iterator itff = _face_group.begin();
    for( ; itff != _face_group.end();  itff++)
    {
        for (int p = 0; p < 3; p++)
        {
            int tidx = _mesh->_triangles[*itff].tex_index[p];

            if(tex_ready[tidx]) continue;
            else tex_ready[tidx] = true;

            Vector2f tex_coord_bef = _mesh->_tex_coords[tidx ];
            /// _mesh->_triangles[*itff].tex_index[p] = p_texcoords[ _mesh->_triangles[*itff].pnt_index[p]];

            float x_loc = float(_texture->width())  * tex_coord_bef.x + _texture_pos_x;
            float y_loc = float(_texture->height()) * tex_coord_bef.y + _texture_pos_y;

            Vector2f tex_coord_aft;


            tex_coord_aft.x = x_loc/float(tex_atlas->width());
            tex_coord_aft.y = y_loc/float(tex_atlas->height());

            _mesh->_tex_coords[tidx] = tex_coord_aft;

           // std::cout << tex_coord_aft.x << " " << tex_coord_aft.y << std::endl;
        }
    }

}

inline float msign (Vector2f & p1, Vector2f & p2, Vector2f & p3) __attribute__((always_inline));
inline float msign (Vector2f & p1, Vector2f & p2, Vector2f & p3)
{
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

inline bool triangle_check(Vector2f & v1, Vector2f & v2, Vector2f &  v3, Vector2f &  pt)  __attribute__((always_inline));
inline bool triangle_check(Vector2f & v1, Vector2f & v2, Vector2f &  v3, Vector2f &  pt)
{



    bool b1 = msign(pt, v1, v2) < 0.0f;
    bool b2 = msign(pt, v2, v3) < 0.0f;
    bool b3 = msign(pt, v3, v1) < 0.0f;

    return ((b1 == b2) && (b2 == b3));
}

inline void barycentric(Vector2f & p, Vector2f & a, Vector2f & b, Vector2f & c,
                        float &u, float &v, float &w)  __attribute__((always_inline));
inline void barycentric(Vector2f & p, Vector2f & a, Vector2f & b, Vector2f & c,
                 float &u, float &v, float &w)
{
    Vector2f v0;
    v0.x = b.x  - a.x;
    v0.y = b.y  - a.y;


    Vector2f v1;
    v1.x = c.x  - a.x;
    v1.y = c.y  - a.y;

    Vector2f v2;
    v2.x = p.x  - a.x;
    v2.y = p.y  - a.y;

    float d00 = v0.x * v0.x + v0.y * v0.y;
    float d01 = v0.x * v1.x + v0.y * v1.y;
    float d11 = v1.x * v1.x + v1.y * v1.y;
    float d20 = v2.x * v0.x + v2.y * v0.y;
    float d21 = v2.x * v1.x + v2.y * v1.y;

    float denom = d00 * d11 - d01 * d01;
    v = (d11 * d20 - d01 * d21) / denom;
    w = (d00 * d21 - d01 * d20) / denom;
    u = 1.0f - v - w;
}

void CTrianglesGroup::construct_texture_color_correction(std::vector<Vector3f> & points_col)
{
    if(_frame == NULL) return;

    cv::Mat forwR = _frame->get_global_rotation_matrix_forw();

    float cam_x = _frame->_global_pos.at<double>(0,0);
    float cam_y = _frame->_global_pos.at<double>(1,0);
    float cam_z = _frame->_global_pos.at<double>(2,0);

    cv::Mat poseT = -forwR * _frame->_global_pos;
    cv::Mat P = (cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2), poseT.at<double>(0,0) ,
                                          forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2), poseT.at<double>(1,0) ,
                                          forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2), poseT.at<double>(2,0));

    cv::Mat camLK_P = CGlobalHSSettings::_camL_K * P;

     double u0 = 10e8;
     double v0 = 10e8;
     double u1 = -1;
     double v1 = -1;

     ///////////////// find flat bounding box of the group
     QImage image = _frame->get_as_image();
   image= image.convertToFormat(QImage::Format_RGB32);

     int xs = image.width();
     int ys = image.height();

     std::vector<Vector2f> flat_tri_coord(_face_group.size()*3);
     std::vector<int> flat_tri_idx(_face_group.size()*3);

     std::list<int>::iterator itf =  _face_group.begin();
     for(int g =0 ; itf != _face_group.end() ;itf++, g++)
     {

         for (int p = 0; p < 3; p++)
         {
             int pidx =  _mesh->_triangles[*itf].pnt_index[p] ;
             C3DPoint pp = _mesh->_points[pidx];

             float px = (pp._x - CGlobalHSSettings::glob_gl_offx)/   CGlobalHSSettings::glob_gl_scale;
             float py = (pp._y - CGlobalHSSettings::glob_gl_offy)/ (-CGlobalHSSettings::glob_gl_scale);
             float pz = (pp._z - CGlobalHSSettings::glob_gl_offz)/ (-CGlobalHSSettings::glob_gl_scale);

             /// get 3d point in pcl1
             cv::Mat pp_mat = (cv::Mat_<double>(4,1) << px, py, pz,1);
             cv::Mat uv = camLK_P * pp_mat;
             uv  /= uv.at<double>(2,0);

             double u = uv.at<double>(0,0);
             if(u < u0) u0 = u;
             if(u > u1) u1 = u;

             double v = uv.at<double>(1,0);
             if(v < v0) v0 = v;
             if(v > v1) v1 = v;

             if(u < 0) u =0;
             if(v < 0) v =0;
             if(u >= xs ) u = xs - 1;
             if(v >= ys ) v = ys - 1;

             Vector2f pnt2d;
             pnt2d.x = u;
             pnt2d.y = v;
            // flat_pnts[pidx] = pnt2d;
            flat_tri_coord[3*g + p] = pnt2d;
            flat_tri_idx[3*g + p] = pidx;
         }
     }



     _tex_u0i = std::max(int(u0),0); // min corner
     _tex_v0i = std::max(int(v0),0);
     _tex_u1i = std::min((int)std::ceil(u1),image.width()-1); // max corner
     _tex_v1i = std::min((int)std::ceil(v1),image.height()-1);

     ///////////// compute corrected colors

     QImage bckp_image = _frame->get_as_image();
     bckp_image= image.convertToFormat(QImage::Format_RGB32);

    QRgb * imBits = (QRgb *) image.bits();
    QRgb * imBits_bckp = (QRgb *) bckp_image.bits();

    float u = 0;
    float v = 0;
    float w = 0;

    int trisz = flat_tri_coord.size()/3;
    for(int t = 0; t < trisz ; t++)
    {
        ////std::cout << t << " from " << trisz << std::endl;

        ///// 2.1. compute bounding box of the triangle
        double u0p = 10e8;
        double v0p = 10e8;
        double u1p = -1;
        double v1p = -1;
        for (int p = 0; p < 3; p++)
        {
            Vector2f coord = flat_tri_coord[t*3 + p];

            if(coord.x < u0p) u0p = coord.x;
            if(coord.x > u1p) u1p = coord.x;

            if(coord.y < v0p) v0p = coord.y;
            if(coord.y > v1p) v1p = coord.y;
        }
        int tt_u0i = std::max(int(u0p),0); // min corner
        int tt_v0i = std::max(int(v0p),0);
        int tt_u1i = std::min((int)std::ceil(u1p),image.width()-1); // max corner
        int tt_v1i = std::min((int)std::ceil(v1p),image.height()-1);

        Vector2f coord0 = flat_tri_coord[t * 3    ];
        Vector2f coord1 = flat_tri_coord[t * 3 + 1];
        Vector2f coord2 = flat_tri_coord[t * 3 + 2];
        //std::cout << int(coord0.x) << " " << int(coord0.y) << " ";
        //std::cout << int(coord1.x) << " " << int(coord1.y) << " ";
        //std::cout << int(coord2.x) << " " << int(coord2.y) << " " << xs << " " << ys <<  std::endl;

        int idx0 = flat_tri_idx[t * 3       ];
        int idx1 = flat_tri_idx[t * 3  + 1  ];
        int idx2 = flat_tri_idx[t * 3  + 2  ];

        Vector3f col0 = points_col[idx0];
        float red0 = col0.x * 255.;
        float gre0 = col0.y * 255.;
        float blu0 = col0.z * 255.;

        QRgb fcol0 = imBits_bckp[ int(coord0.y)* xs + int(coord0.x) ];
        float fred0 = qRed(fcol0);
        float fgre0 = qGreen(fcol0);
        float fblu0 = qBlue(fcol0);

        Vector3f col1 = points_col[idx1];
        float red1 = col1.x * 255.;
        float gre1 = col1.y * 255.;
        float blu1 = col1.z * 255.;

        QRgb fcol1 = imBits_bckp[ int(coord1.y)* xs + int(coord1.x) ];
        float fred1 = qRed(fcol1);
        float fgre1 = qGreen(fcol1);
        float fblu1 = qBlue(fcol1);

        Vector3f col2 = points_col[idx2];
        float red2 = col2.x * 255.;
        float gre2 = col2.y * 255.;
        float blu2 = col2.z * 255.;


        QRgb fcol2 = imBits_bckp[ int(coord2.y)* xs + int(coord2.x) ];
        float fred2 = qRed(fcol2);
        float fgre2 = qGreen(fcol2);
        float fblu2 = qBlue(fcol2);


        /// 2. fill image within the triangle
        for (int yy = tt_v0i; yy <= tt_v1i; yy++)
        {
            int idxy = yy * xs;
            for(int xx = tt_u0i; xx <= tt_u1i; xx++)
            {
                Vector2f pt;
                pt.x = xx;
                pt.y = yy;

                bool is_corner = false;
                for (int p = 0; p < 3; p++)
                {
                    Vector2f coord = flat_tri_coord[t*3 + p];
                    int ppidx = flat_tri_idx[t*3 + p];

                    if( (int(coord.x) == xx) && (int(coord.y) == yy) )
                    {

                        //  QRgb valueO = image.pixel(u, v);
                        float red = points_col[ppidx].x * 255.;
                        float gre = points_col[ppidx].y * 255.;
                        float blu = points_col[ppidx].z * 255.;

                        imBits[idxy + xx] = qRgb(int(red), int(gre), int(blu));
                        /// imBits[idxy + xx] = qRgb(255, 0, 0);

                        is_corner = true;
                        break;
                    }

                }
                if(is_corner) continue;

                bool inside = triangle_check(coord0, coord1, coord2, pt);
                if(inside)
                {

                      barycentric(pt, coord0, coord1, coord2, u,  v,  w);

                       //QRgb valueR =qRgb(255, 0, 255);
                       //image.setPixel(xx, yy, valueR);

                      QRgb pcolor0 =  imBits[idxy + xx];
                      float pcolor0_red = qRed(pcolor0);
                      float pcolor0_gre = qGreen(pcolor0);
                      float pcolor0_blu = qBlue(pcolor0);

                    float corrected_red =   pcolor0_red + u * (red0 - fred0) + v * (red1 - fred1) + w * (red2 - fred2);

                    if(corrected_red < 0)   corrected_red =0;
                    if(corrected_red > 255) corrected_red =255;

                    float corrected_green =   pcolor0_gre + u * (gre0 - fgre0) + v * (gre1 - fgre1) + w * (gre2 - fgre2);

                    if(corrected_green < 0)   corrected_green = 0;
                    if(corrected_green > 255) corrected_green = 255;


                    float corrected_blue =   pcolor0_blu + u * (blu0 - fblu0) + v * (blu1 - fblu1) + w * (blu2 - fblu2);

                    if(corrected_blue < 0)   corrected_blue = 0;
                    if(corrected_blue > 255) corrected_blue = 255;


                    imBits[idxy + xx] = qRgb(corrected_red, corrected_green, corrected_blue);



                 }
            }
        }

    }

      /*std::map<int, Vector2f>::iterator itt = flat_pnts.begin();
      for(; itt != flat_pnts.end(); itt++)
      {
          Vector2f coord = itt->second;
          /// if( (int(coord.x) == u) && (int(coord.y) == v) )
          {

              //  QRgb valueO = image.pixel(u, v);
              float red = points_col[itt->first].x * 255.;
              float gre = points_col[itt->first].y * 255.;
              float blu = points_col[itt->first].z * 255.;


              ///QRgb valueR = qRgb(red, gre, blu);
              QRgb valueR = qRgb(0, 255, 0);
              image.setPixel(int(coord.x), int(coord.y), valueR);

              //std::cout << red << " " << gre << " " << blu << std::endl;

          }
      }*/

     QImage impart = image.copy(_tex_u0i, _tex_v0i, (_tex_u1i-_tex_u0i)+1, (_tex_v1i-_tex_v0i)+1);
     _texture = new QImage(impart );

}

void CTrianglesGroup::construct_texture()
{
  // _frame->load_LR_images_from_hd();

    //std::cout << "frame ref" << _frame << std::endl;
  // std::cout << _frame->_global_pos.t() << std::endl;

   cv::Mat forwR = _frame->get_global_rotation_matrix_forw();

   float cam_x = _frame->_global_pos.at<double>(0,0);
   float cam_y = _frame->_global_pos.at<double>(1,0);
   float cam_z = _frame->_global_pos.at<double>(2,0);

   cv::Mat poseT = -forwR * _frame->_global_pos;
   cv::Mat P = (cv::Mat_<double>(3,4) << forwR.at<double>(0,0), forwR.at<double>(0,1), forwR.at<double>(0,2), poseT.at<double>(0,0) ,
                                         forwR.at<double>(1,0), forwR.at<double>(1,1), forwR.at<double>(1,2), poseT.at<double>(1,0) ,
                                         forwR.at<double>(2,0), forwR.at<double>(2,1), forwR.at<double>(2,2), poseT.at<double>(2,0));

   cv::Mat camLK_P = CGlobalHSSettings::_camL_K * P;
   //int xs = _frame->_img_L.cols;
   //int ys = _frame->_img_L.rows;

    double u0 = 10e8;
    double v0 = 10e8;
    double u1 = -1;
    double v1 = -1;

    std::list<int>::iterator itf =  _face_group.begin();
    for( ; itf != _face_group.end() ;itf++)
    {

        for (int p = 0; p < 3; p++)
        {
            C3DPoint pp = _mesh->_points[ _mesh->_triangles[*itf].pnt_index[p] ];

            float px = (pp._x - CGlobalHSSettings::glob_gl_offx)/   CGlobalHSSettings::glob_gl_scale;
            float py = (pp._y - CGlobalHSSettings::glob_gl_offy)/ (-CGlobalHSSettings::glob_gl_scale);
            float pz = (pp._z - CGlobalHSSettings::glob_gl_offz)/ (-CGlobalHSSettings::glob_gl_scale);

            /// get 3d point in pcl1
            cv::Mat pp_mat = (cv::Mat_<double>(4,1) << px, py, pz,1);
            cv::Mat uv = camLK_P * pp_mat;
            uv  /= uv.at<double>(2,0);

            double u = uv.at<double>(0,0);
            if(u < u0) u0 = u;
            if(u > u1) u1 = u;

            double v = uv.at<double>(1,0);
            if(v < v0) v0 = v;
            if(v > v1) v1 = v;

            ///std::cout << "uv" <<  u << " " << v  << " " << u0 << " " << v0 << std::endl;

        }
    }

    std::cout << u0 << " " << v0 << " " << u1 << " " << v1 << std::endl;

    QImage image = _frame->get_as_image();

    _tex_u0i = std::max(int(u0),0); // min corner
    _tex_v0i = std::max(int(v0),0);
    _tex_u1i = std::min((int)std::ceil(u1),image.width()-1); // max corner
    _tex_v1i = std::min((int)std::ceil(v1),image.height()-1);

    std::cout << "copy part "<< _tex_u0i<< " " << _tex_v0i << " " << (_tex_u1i-_tex_u0i)+1 << " "<< (_tex_v1i-_tex_v0i)+1<< std::endl;
   // QRect rect(_tex_u0i, _tex_v0i , (_tex_u1i-_tex_u0i)+1 , (_tex_v1i-_tex_v0i)+1);
    //*_texture =  image.copy(_tex_u0i, _tex_v0i , (_tex_u1i-_tex_u0i)+1 , (_tex_v1i-_tex_v0i)+1);

    QImage impart = image.copy(_tex_u0i, _tex_v0i , (_tex_u1i-_tex_u0i)+1 , (_tex_v1i-_tex_v0i)+1);
    _texture = new QImage(impart );


      ///std::cout << "save it "<< std::endl;
   ///_texture->save("cut_texrure.png");
}

bool cmp_face_groups_desc(CTrianglesGroup * t1, CTrianglesGroup * t2)
{
    return ((t1->_face_group.size()) > (t2->_face_group.size()));
}

bool cmp_face_groups_asc(CTrianglesGroup * t1, CTrianglesGroup * t2)
{
    return ((t1->_face_group.size()) < (t2->_face_group.size()));
}

bool cmp_face_groups_desc_tex(CTrianglesGroup * t1, CTrianglesGroup * t2)
{

    return (((t1->_texture != NULL)?t1->_texture->height():0) > ((t2->_texture != NULL)?t2->_texture->height():0));
}

bool cmp_face_groups_desc_texarea(CTrianglesGroup *t1 , CTrianglesGroup *t2)
{
return ((t1->_texture->height()*t1->_texture->width()) > (t2->_texture->height()*t2->_texture->width()));
}
