#ifndef CLASERSANOFFLINETHREAD_H
#define CLASERSANOFFLINETHREAD_H

#include <QThread>
#include <QMutex>

#include "graphicutils.h"
#include "c3dcorrector.h"
#include "opencv_aux_utils.h"

#include "runetag/runetag.h"

class CPointCloud;
class CTriangularMesh;
struct aux_pix_data;
class CFrame;

class CLaserSanOfflineThread : public QThread
{
    Q_OBJECT
public:
    explicit CLaserSanOfflineThread(QObject *parent = 0);

    ~CLaserSanOfflineThread();
    void start_process(QString & ininame,
                       bool rand_lcol,
                       int decim_pnt,
                       float side_len,
                       bool usecorr,
                       bool ismesh,
                       bool isCalibMode,
                       bool useADeform);

    void start_process(QString & ininame,
                       int decim_pnt,
                       bool usecorr);

    bool is_abort(){return _abort;}
    void emit_send_back(int i);

signals:
    void send_back(const int val);
    void send_result(std::vector<C3DPoint> * cpoints);
    void send_mesh(CTriangularMesh *);
    void send_data_for_cglobject(std::vector<C3DPoint> * , CTriangularMesh *, float r, float g, float b);
    void send_result_fuse(CTriangularMesh *);
    void send_clean_scan();
    void send_delete_scan();
    void send_wrong_K_matrix();
    void send_new_cam();
    void valid_result();

public slots:

protected:
    void run_old();
    void run();

    QString construct_file_name(char * pattern_name_buf, QString & fformat, int scan );

    QImage CLaserSanOfflineThread::extract_particular_frame(cv::VideoCapture & vid,
                                    std::map<int, CFrameDesc> & frame_map,
                                    CFrameDesc::CFrameType typ, int & fidx);


    QImage  read_as_undistorted_image(QString & fname);
    void undistort_image(QImage & q_input_image);


    std::vector<cv::Point2f>  find_calibration_board(int marks_x, int marks_y, QString imgname);
    void construct_board_plain( CArray2D<aux_pix_data *>  * mdata, float &A, float & B, float &C, float &D);

    CArray2D<cv::Point3f>  construct_deform_pattern( std::vector<CArray2D<aux_pix_data *> *> & merged_data,
                                   std::vector<cv::Point2f> corners, int marks_x, int marks_y,
                                                     float cz,
                                                     board_corners & bc);

    CArray2D<int > contruct_label_map(int xs, int ys, int marks_x, int marks_y,
                                                              std::vector<cv::Point2f> & corners);


    void construct_and_add_new_cframe(int s, float rxc, float ryc, QString & aviname, int frame_idx);
    void compute_camera_matrix_K(CArray2D<aux_pix_data *> * mdata);
    void set_camera_matrix_for_texturing();


    void construct_merged_data(QImage & bckg, CArray2D<aux_pix_data *> & merged_data);
    //void construct_merged_data_with_pcl(QImage & bckg, CArray2D<aux_pix_data *> & merged_data);

    std::vector<cv::Point3f> get_neighbour_3d_glpoints(std::vector<C3DPoint*> & gl_points,
                                                         CArray2D<aux_pix_data *> * _merged_data,
                                                          int x, int y, int xs, int ys,
                                                           int decimstep, int nsz);

    std::vector<C3DPoint> * prepare_pcl(CArray2D<aux_pix_data *> * _merged_data,
                                               int cx, int cy, int cz, int s,
                                               float gl_red,
                                               float gl_gre,
                                               float gl_blu,
                                               int decim,
                                               QImage & tex);

    std::vector<C3DPoint> * prepare_pcl_with_norms(CArray2D<aux_pix_data *> * _merged_data,
                                                int s,
                                               float gl_red,
                                               float gl_gre,
                                               float gl_blu,
                                               int decim,
                                               QImage & tex);

   void construct_pcl(CArray2D<aux_pix_data *> * , int cx, int cy, int cz, int,
                       float gl_red,
                       float gl_gre,
                       float gl_blu,
                       int decim = 1);


    void fill_gaps_in_merged_data(CArray2D<aux_pix_data *> * merged_data);
    void fill_gaps_in_merged_data_triv(CArray2D<aux_pix_data *> & merged_data);

    ///void construct_pcl(CArray2D<aux_pix_data *> & merged_data, std::vector<C3DPoint> * pcl_pnts);

    CTriMesh * construct_mesh_from_single_shot(CArray2D<aux_pix_data *> & merged_data,
                                               int decimstep);

   void construct_texture(CArray2D<aux_pix_data *> & merged_data,
                          CTriMesh * rmesh,  QImage & teximg);


   void compute_polar_map_for_fused_mesh( std::vector<CArray2D<aux_pix_data *> *> & m_scans, float rxc, float ryc);
   CArray2D<aux_pix_data *> *  simplify_polar_map(CArray2D< std::list<aux_pix_data *> > & );
   CArray2D<aux_pix_data *> *  simplify_polar_map_ransac(CArray2D< std::list<aux_pix_data *> > & );

public slots:
    void stop_process();

private:
    bool    _abort;

    QMutex  _mutex;

    QString _video_name_format;
    QString _video_desc_format;
    bool _video_name_has_scan;

    QString _path;
    bool _has_several_scans;
    int _scan_first;
    int _scan_last;
    QString _image_name_format;
    QString _tex_name_format;
    int _img_beg_idx;
    int _img_end_idx;
    bool _is_difference;
    bool _has_marks;
    bool _is_round_marks;
    QString _bckg_image;
    QString _bckg_image2;
    int _marks_x;
    int _marks_y;

    int _max_scan_lines;
    int _scan_cnt;
    float _sca_angle;

    bool _compute_laser_pos;
    float _las_betta_beg;
    float _las_betta_end;
    float _las_betta_step;
    float _fov_horiz;
    float _fov_vert;
    float _camera_laser_dist;
    float _camera_x_incline;
    float _camera_y_incline;
    float _rot_center_x;
    float _rot_center_y;
    float _rot_center_z;

    float _max_depth_to_cancel;
    float _min_depth_to_cancel;

    bool _use_metadata_angle;
    bool _camera_laser_interchanged;
    float _meta_angle_correction;

    float _laser_depth_offset;

    bool _do_mesh;
    int _decim_pnt;
    float _max_side_len;

    bool _random_line_color;


    int _laser_onboard_ymin;
    int _laser_onboard_ymax;
    float _single_cell_width;
    float _camera_obj_dist;

    bool _is_calib_mode;
    bool _use_adeform;

    bool _use_corr;
    std::vector<CIPixel> extract_only_grid_laser_pnts(std::vector<CIPixel>  &);
    bool convert_pix_to_laser_angle(std::vector<CIPixel> & grid_pix, int xs,
                                    float & laser_angle);

    void extract_grid_points(QString bckg_name);
    std::vector<cv::Point2f> _corners;


    QString construct_image_name(char * pattern_name_buf, int scan, int idx);
    QString construct_tex_name(char * pattern_name_buf, int scan);
    QString construct_bckg_name(char * pattern_name_buf, int scan);


    std::map<int,float> _dummy_angles;



    cv::runetag::MarkerDetector  *  _pat_detector;
    cv::runetag::EllipseDetector *  _ellipseDetector;
    bool do_find_runetag_in_qimage(QImage & img, cv::runetag::Pose & RT );


};

#endif // CLASERSANOFFLINETHREAD_H
