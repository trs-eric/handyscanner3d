#include "cphotogrammetrythread.h"

// start qt process
// http://stackoverflow.com/questions/2622864/start-a-process-using-qprocess
// http://qt-project.org/forums/viewthread/30146
// http://forum.sources.ru/index.php?showtopic=291048
// https://qt-project.org/doc/qt-4.7/qprocess.html
// redirect output
// http://www.qtforum.org/article/3079/howto-start-an-external-program-from-a-qt-application.html
CPhotogrammetryThread::CPhotogrammetryThread(QObject *parent) :
    QThread(parent), _abort(false)
{
}

CPhotogrammetryThread::~CPhotogrammetryThread()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
    wait();
}

void CPhotogrammetryThread::start_process()
{
    _abort = false;
    start();
}

void CPhotogrammetryThread::stop_process()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
}

void CPhotogrammetryThread::emit_send_back(int i)
{
     emit send_back(i);
}

void CPhotogrammetryThread::run()
{
    // emit send_result(ncld);
    emit send_back(100);
}
