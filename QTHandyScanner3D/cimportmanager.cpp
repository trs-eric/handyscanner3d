#include "cimportmanager.h"
#include <sstream>
#include <QFile>
#include <QFileInfo>

const QString CImportManager::importFileTypes = "XYZ Point Cloud (*.xyz);; Textured Mesh (*.obj)";

CImportManager::CImportManager()
{
}
////////////////////////////// obj loader part ////////////////////////////


#define TOKEN_VERTEX_POS "v"
#define TOKEN_VERTEX_NOR "vn"
#define TOKEN_VERTEX_TEX "vt"
#define TOKEN_FACE "f"
#define TOKEN_MTL "mtllib"
#define TOKEN_KD "map_Kd"
#define TOKEN_KA "map_Ka"

std::string DirName(std::string source)
{
    source.erase(std::find(source.rbegin(), source.rend(), '/').base(), source.end());
    return source;
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

struct ObjMeshVertex
{
    Vector3f pos;
    Vector2f texcoord;
    Vector3f normal;
};

struct ObjMeshFace
{
    ObjMeshVertex vertices[3];
};

struct _ObjMeshFaceIndex
{
    int pos_index[3];
    int tex_index[3];
    int nor_index[3];
};

std::string findout_texture_file(std::ifstream & mtlfilestream)
{
    std::string texture;
    std::string line_stream;

    while(std::getline(mtlfilestream, line_stream))
    {
        std::stringstream str_stream(line_stream);
        std::string key;
        str_stream >> key;
        if(key == TOKEN_KD || key == TOKEN_KA)
            str_stream >> texture;
    }
    return texture;
}

CTriMesh * load_obj_mesh(std::string fname)
{

    ///
    /// ofile.open(fname.toLocal8Bit().data(), std::ios::out );


   ///
    QString qfname(fname.c_str());
    QFileInfo info_fname(qfname);
    QString working_dir = info_fname.absolutePath();
    ///std::cout << qfname.toLocal8Bit().data() <<
    ///             std::endl << working_dir.toLocal8Bit().data() << std::endl;

    /// std::string fbase = DirName(fname);

    std::vector<C3DPoint>  points;

    std::vector<Vector3f>  positions;
    bool is_textured = false;
    bool is_tex_coords = false;
    std::vector<Vector2f>  texcoords;
    bool is_norm_coords = false;
    std::vector<Vector3f>  normals;


    std::vector<_ObjMeshFaceIndex>  faces;

    std::ifstream filestream;
    filestream.open(qfname.toLocal8Bit().data());
    if(!filestream.is_open())
        return NULL;

    std::string line_stream;
    std::string mtlfile;
    bool is_face_format_determined = false;
    while(std::getline(filestream, line_stream))
    {
        std::stringstream str_stream(line_stream);
        std::string type_str;
        str_stream >> type_str;
        if(type_str == TOKEN_MTL)
        {
            str_stream >> mtlfile;
        }
        else if(type_str == TOKEN_VERTEX_POS)
        {
            Vector3f pos;
            str_stream >> pos.x >> pos.y >> pos.z;
            positions.push_back(pos);

            C3DPoint pnt;
            pnt._x = pos.x;
            pnt._y = pos.y;
            pnt._z = pos.z;

            points.push_back(pnt);
        }
        else if(type_str == TOKEN_VERTEX_TEX)
        {
            Vector2f tex;
            str_stream >> tex.x >> tex.y;
            texcoords.push_back(tex);
        }
        else if(type_str == TOKEN_VERTEX_NOR)
        {
            Vector3f nor;
            str_stream >> nor.x >> nor.y >> nor.z;
            normals.push_back(nor);
        }
        else if(type_str == TOKEN_FACE)
        {

            std::vector<std::string> tokens = split(line_stream, ' ');

            if(!is_face_format_determined)
            {
                std::vector<std::string> face_format = split(tokens[1], '/');
                if(face_format.size() == 3 ) // v/vt/vn
                {
                    is_tex_coords  = true;
                    is_norm_coords = true;
                    // std::cout << "true true" << std::ends;

                }
                else if(face_format.size() == 1) // v
                {
                    is_tex_coords  = false;
                    is_norm_coords = false;

                    // std::cout << "false false " << std::ends;
                }
                else if(face_format.size() == 2)
                {
                    if(tokens[0].find_first_of("//") != std::string::npos) // v//vn
                    {
                        is_tex_coords  = false;
                        is_norm_coords = true;

                        // std::cout << "false true" << std::ends;
                    }
                    else // v/vt
                    {
                        is_tex_coords  = true;
                        is_norm_coords = false;

                        // std::cout << "true false" << std::ends;
                    }
                }
                is_face_format_determined = true;
            }
            // classical triangle
            if   ( tokens.size() == 4 )
            {
                //std::cout << "3: " << tokens[0] << " " << tokens[1] << std::endl;
                 _ObjMeshFaceIndex face_index;
                for(int i = 0; i < 3; ++i)
                {
                    std::vector<std::string> face_content = split(tokens[i+1], '/');

                    face_index.pos_index[i] = atoi(face_content[0].c_str()) - 1;

                    if(is_tex_coords)
                        face_index.tex_index[i] = atoi(face_content[1].c_str()) - 1;

                    if(is_norm_coords && is_tex_coords)
                        face_index.nor_index[i] = atoi(face_content[2].c_str()) - 1;
                    else if(is_norm_coords && !is_tex_coords)
                        face_index.nor_index[i] = atoi(face_content[1].c_str()) - 1;
                }
                //std::cout << std::endl;
                faces.push_back(face_index);
            }
            // we split quad into 2 triangles
            else if ( tokens.size() == 5 )
            {
                _ObjMeshFaceIndex face_index1;
                _ObjMeshFaceIndex face_index2;
                for(int i = 0; i < 4; ++i)
                {
                    int pos = 0 , tex = 0, nor = 0;

                    std::vector<std::string> face_content = split(tokens[i+1], '/');

                    pos = atoi(face_content[0].c_str()) - 1;
                    if(is_tex_coords)
                        tex = atoi(face_content[1].c_str()) - 1;
                    if(is_norm_coords && is_tex_coords)
                        nor = atoi(face_content[2].c_str()) - 1;
                    else if(is_norm_coords && !is_tex_coords)
                        nor = atoi(face_content[1].c_str()) - 1;

                    if(i < 3)
                    {
                        face_index1.pos_index[i] = pos;
                        face_index1.tex_index[i] = tex;
                        face_index1.nor_index[i] = nor;
                    }

                    if(i != 1 )
                    {
                        face_index2.pos_index[(i + 2)%4] = pos;
                        face_index2.tex_index[(i + 2)%4] = tex;
                        face_index2.nor_index[(i + 2)%4] = nor;
                    }
                }

                faces.push_back(face_index1);
                faces.push_back(face_index2);

            }
        }
    }
    filestream.close();

    ///// find out name of the texture /////////////////
    QString tex_file;
    QString texture_name;
    std::ifstream mtlfilestream;
    mtlfile = std::string (working_dir.toLocal8Bit().data()) + '/'  + mtlfile;

    //// std::cout << mtlfile << std::endl;

    mtlfilestream.open(mtlfile.c_str());
    if(mtlfilestream.is_open())
    {
        tex_file = QString(findout_texture_file(mtlfilestream).c_str());
        texture_name = tex_file;
        if(!tex_file.isEmpty())
        {
            tex_file = working_dir + '/' + tex_file;

            //// std::cout << tex_file.toLocal8Bit().data() << std::endl;

            QFile tex_file_e(tex_file);
            if(tex_file_e.exists())
                is_textured = true;

        }
    }
    /////////////////////////////////////////////////////////

    std::vector<C3DPointIdx> triangles;
    std::vector<_ObjMeshFaceIndex>::iterator itf = faces.begin();
    for(int f =0 ; itf != faces.end(); itf++, f++)
    {
        C3DPointIdx triangle;
        for(int p = 0; p < 3; p++)
        {
            int point_num = itf->pos_index[p];

            if(is_norm_coords)
            {
                points[point_num]._nx = normals[itf->nor_index[p]].x;
                points[point_num]._ny = normals[itf->nor_index[p]].y;
                points[point_num]._nz = normals[itf->nor_index[p]].z;
            }

            if(is_textured)
                triangle.tex_index[p] = itf->tex_index[p];

            points[point_num]._faceidx.push_back(f);

            triangle.pnt_index[p] = point_num;


        }

        C3DPoint pnt0 = points[ triangle.pnt_index[0]];
        C3DPoint pnt1 = points[ triangle.pnt_index[1]];
        C3DPoint pnt2 = points[ triangle.pnt_index[2]];

        triangle.tri_normal = compute_normal( pnt2, pnt1, pnt0);
        triangles.push_back(triangle);
    }


    CTriMesh * mesh = new CTriMesh;
    mesh->_points       = points;
    mesh->_triangles    = triangles;

    if(is_textured)
    {
        mesh->_texture = new QImage(tex_file);
        mesh->_texture_name = texture_name.toStdString();
        mesh->_tex_coords   = texcoords;
    }
    else
    {
        mesh->_texture = NULL;
        mesh->_texture_name = "";
    }

    return mesh;
}
