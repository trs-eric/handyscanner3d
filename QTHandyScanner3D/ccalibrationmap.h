#ifndef CCALIBRATIONMAP_H
#define CCALIBRATIONMAP_H

#include "opencv_aux_utils.h"

struct FocusData
{
    int   _focus;
    cv::Mat _cm;
    cv::Mat _dc;

    FocusData(){}

    FocusData(int focus, cv::Mat & cm, cv::Mat & dc)
    {
        _focus = focus;
        _cm = cm;
        _dc = dc;
    }

    FocusData(const FocusData & c )
    {
        _focus = c._focus;
        _cm = c._cm;
        _dc = c._dc;
    }

    void save(cv::FileStorage & fs);
};

class CCalibrationMap
{
public:
    CCalibrationMap();
    void set_callibration_data(int focus, cv::Mat & cm, cv::Mat & dc);
    bool set_globals_for_focus(int focus);
    int set_globals_for_num(int);
    int which_focus_indx(int focus);

    void save();
    void load(QString & f);
    bool settings_for_focus(int focus);
    bool empty();

    std::map<int, FocusData> _map;
};

#endif // CCALIBRATIONMAP_H
