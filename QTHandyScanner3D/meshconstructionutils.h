#ifndef MESHCONSTRUCTIONUTILS_H
#define MESHCONSTRUCTIONUTILS_H

#include "graphicutils.h"

void compute_normals(std::vector<C3DPoint> * points, int neighbors, bool flip);
void compute_normals_meshlab(std::vector<C3DPoint> * points, int neighbors, bool flip);

typedef bool CallBackPos(const int pos, const char * str );

void compute_normals_meshlab(std::list<C3DPoint> * points, int neighbors, int iter,
                     bool flip=false,  CallBackPos info=NULL);

#endif // MESHCONSTRUCTIONUTILS_H
