#ifndef CUSBCAMERASETTINGS_H
#define CUSBCAMERASETTINGS_H

#include <QDialog>
#include <vector>
#include <QTimer>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

namespace Ui {
class CUSBCameraSettings;
}

class CUSBCameraSettings : public QDialog
{
    Q_OBJECT

public:
    explicit CUSBCameraSettings(cv::VideoCapture *,  QWidget *parent = 0);
    ~CUSBCameraSettings();

private slots:
    void resolution_changed(int);
    void change_exposure();
    void change_focus();
    void change_brightness();
    void change_contrast();
    void change_gamma();
    void change_gain();
    void change_sharpness();
    void change_hue();
    void change_saturation();
    void change_wb_blue();
    void change_wb_red();
    void change_wb_auto();
    void set_defaults();
    void change_calib(bool);

private:
    Ui::CUSBCameraSettings *ui;


    std::vector<int> _resol_list;
    int _resol_idx;

    cv::VideoCapture * _current_cam;

    int _val_exposure;
    int _val_focus;
    int _val_brightness;
    int _val_contrast;
    int _val_gamma;
    int _val_gain;
    int _val_sharp;
    int _val_hue;
    int _val_satu;
    int _val_wb_red;
    int _val_wb_blue;




};

#endif // CUSBCAMERASETTINGS_H
