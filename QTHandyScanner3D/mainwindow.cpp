#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<QPushButton>
#include<QLineEdit>
#include<QFormLayout>
#include<QToolBar>
#include<QMenuBar>
#include<QMessageBox>
#include<QFileDialog>
#include <QLabel>
#include <QCloseEvent>
#include <QThread>

#include "cglwidget.h"
#include "messages.h"

#include "cimportmanager.h"
#include "cexportmanager.h"

#include "cdisparitycompdlg.h"
#include "cdisparitycompclassicdlg.h"
#include "ccalibstereodlg.h"
#include "ccomputesparsepcldlg.h"
#include "cdpclconstructiondlg.h"
#include "cmcubesmeshdlg.h"
#include "cmeshconstructiondlg.h"
#include "cfusemeshdlg.h"

#include "ctexturingdlg.h"
// #include "claplaciansmdlg.h"
//#include "cqecdecimdlg.h"
#include "cdecismoothdlg.h"
#include "clasersanofflinedlg.h"
#include "claserscanonlinedlg.h"
#include "cpclcoloringdlg.h"
#include "cselectpclhelpdlg.h"
#include "cmeshartifremovedlg.h"

#include <QPushButton>
#include <QLayout>
#include <QSplashScreen>
#include <QTimer>

#include "menu_vew_actions.h"
#include "cglsettingsdlg.h"
#include "settings/psettings.h"

#include "csceneexplorermanager.h"

#include "cprojectio.h"
#include "iowaitdlg.h"
#include "cglalignmentclouddlg.h"



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), _is_actul_scene(true)
{
    createActions();
    createMenus();
    //createToolBars();

    ui->setupUi(this);
    createStatusBar();


    MenuActions * va = new  MenuActions;

    va->_main_window = this;

    va->view_raw_point_Act = viewrpAct;
    va->view_mesh_wire_Act = viewwrAct;
    va->view_mesh_flat_Act = viewflAct;
    va->view_mesh_smoo_Act = viewsmAct;
    va->view_mesh_tex_Act  = viewtxAct;
    va->view_shad_pnt_Act  = viewshadpAct;
    va->view_camara_path_Act = viewcamAct;

    glWidget = new CGLWidget(ui->treeWidget, va);
    setCentralWidget(glWidget);



    createFieldButtons();
    va->fbtn_vw_wr = btn_vw_wr;
    va->fbtn_vw_fl = btn_vw_fl;
    va->fbtn_vw_sm = btn_vw_sm;
    va->fbtn_vw_tx = btn_vw_tx;

    createAuxDialogs();

    int bits = SYSBITS;

    _appName            = QString("Rubicon 3D Scanner (%1-bits)").arg(QString::number(bits));
    _current_project    = "";
    _title              = _appName;
    this->setWindowTitle(_title);

    ///////////////////////////////////////////////////

    ui->dockWidget->setWidget(ui->treeWidget); //fill central widget

    ui->treeWidget->setItemsExpandable(false); // donot expand at all
    ui->treeWidget->setRootIsDecorated(false); // hide [-] sign

    //ui->treeWidget->set
    //ui->treeWidget->setStyleSheet( "QTreeView::branch {  border-image: url(none.png); }" );
    ui->treeWidget->setColumnCount(5);
    //ui->treeWidget->header()->setStretchLastSection(false);

    ui->treeWidget->setColumnWidth(0, 20);
    //ui->treeWidget->header()->setSectionResizeMode(4,  QHeaderView::Stretch);
    ui->treeWidget->setColumnWidth(1, 20);
    //ui->treeWidget->header()->setSectionResizeMode(1,  QHeaderView::Interactive);
    ui->treeWidget->setColumnWidth(2, 20);
    //ui->treeWidget->header()->setSectionResizeMode(2,  QHeaderView::Interactive);
    ui->treeWidget->setColumnWidth(3, 20);
    //ui->treeWidget->header()->setSectionResizeMode(3,  QHeaderView::Interactive);
    ui->treeWidget->setColumnWidth(4, 20);
    //ui->treeWidget->header()->setSectionResizeMode(4,  QHeaderView::Interactive);


    ui->treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->treeWidget->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->treeWidget->setHeaderHidden(true);


    ui->treeWidget->expandAll();
    //////////////////////////////////////////////////

    connect(ui->treeWidget,
            SIGNAL(itemDoubleClicked(QTreeWidgetItem *, int)),
            glWidget, SLOT(on_double_click_treew_item(QTreeWidgetItem *, int)));
    connect(ui->treeWidget,
            SIGNAL(itemClicked(QTreeWidgetItem *, int)),
            glWidget, SLOT(on_single_click_treew_item(QTreeWidgetItem *, int)));


    connect(ui->dockWidget,
            SIGNAL(dockLocationChanged(Qt::DockWidgetArea)),
            this, SLOT(dockLocationChanged(Qt::DockWidgetArea)));
    /*
    glWidget->resizeEvent();

    connect(glWidget,
            SIGNAL(dockLocationChanged(Qt::DockWidgetArea)),
            this, SLOT(dockLocationChanged(Qt::DockWidgetArea)));
    */

    ui->dockWidget->setVisible(true);


    ///// save and load part

    _proj_io_thread = new QThread();
    _proj_io = new CProjectIO();

    _proj_io->moveToThread(_proj_io_thread);

    connect(_proj_io , SIGNAL(save_requested()), _proj_io_thread, SLOT(start()));
    connect(_proj_io , SIGNAL(load_requested()), _proj_io_thread, SLOT(start()));
    connect(_proj_io , SIGNAL(export_requested()), _proj_io_thread, SLOT(start()));

    connect(_proj_io_thread, SIGNAL(started()), _proj_io, SLOT(do_io()));

    connect(_proj_io, SIGNAL(finished_save_proj()), _proj_io_thread, SLOT(quit()), Qt::DirectConnection);
    connect(_proj_io, SIGNAL(finished_save_proj()), this, SLOT(proj_save_finished()));

    connect(_proj_io, SIGNAL(finished_load_proj(bool)), _proj_io_thread, SLOT(quit()), Qt::DirectConnection);
    connect(_proj_io, SIGNAL(finished_load_proj(bool)), this, SLOT(proj_load_finished(bool)));

    connect(_proj_io, SIGNAL(finished_export()), _proj_io_thread, SLOT(quit()), Qt::DirectConnection);
    connect(_proj_io, SIGNAL(finished_export()), this, SLOT(export_finished()));


    _iowaitDlg = new IOWaitDlg;

    //////////////////////////

}

void MainWindow::text_clicked()
{
    QMessageBox::about(this, tr("Warning"),
             tr("The button was pressed!"));
}
MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::dockLocationChanged(Qt::DockWidgetArea area)
{
     updateFieldButton();
}

void MainWindow::resizeEvent(QResizeEvent * event)
{
    updateFieldButton();
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    switch(event->key())
    {
       /*  case Qt::Key_Escape:
           if(_martifRemDlg != NULL) _martifRemDlg->hide();
           break;
           glWidget->set_view_scene_mode(CGLWidget::DEF_VIEW_MODE);
            _selPCLHelpDlg->hide();
            break;
*/

        case Qt::Key_Delete:
            glWidget->delete_selected_points();
            break;
    }
}


void MainWindow::closeEvent(QCloseEvent *event)
{
    /*
    if(!_is_actul_scene)
    {
          QMessageBox::StandardButton reply =
                  QMessageBox::warning(this, tr("Warning"),
                                       wrn_DiscardSceneChanges, QMessageBox::Yes|QMessageBox::No);
           if (reply == QMessageBox::No)
           {
                event->ignore();
                return;
           }
    }
    */
    delete _dispDlg;
   /// delete _dispClasDlg;
    delete _calibDlg;
    delete _compSPCLDlg;
    delete _compDPCLDlg;
    //// delete _compMCubeMeshDlg;
    delete _meshCDlg;
    delete _fusemeshDlg;
    delete _texDlg;
   // delete _lapSmDlg;
   // delete _qecDecDlg;

    delete _decsmDlg;
    delete _glSetDlg;
    delete _lasScanOffDlg;
    delete _lasScanOnlDlg;
    delete _pclColorDlg;
    delete _selPCLHelpDlg;

    delete _alignCldDlg;
    delete _martifRemDlg;

    QMainWindow::closeEvent(event);
}

void MainWindow::createAuxDialogs()
{
    /*
    _poptDlg  = new CPointOptimizeDlg(glWidget);
    _normeDlg = new CNormalEstimationDlg(glWidget);
     _cvxMeshCDlg = new CConvexMeshConstructionDlg(glWidget);
    _scanDlg = new CScanReconstructionDlg(glWidget);
    _cropDlg = new CCropDiskDlg(glWidget);
    _clDesimDlg = new CClusteringDecimDlg(glWidget);
    _mrsDlg = new CMovRotScaDlg(glWidget);
*/
    _dispDlg = new CDisparityCompDlg(glWidget);
   /// _dispClasDlg = new CDisparityCompClassicDlg(glWidget);
    _calibDlg = new CCalibStereoDlg();
    _compSPCLDlg = new CComputeSparsePCLDlg(glWidget);
    _compDPCLDlg = new CDPCLConstructionDlg(glWidget);

   /// _compMCubeMeshDlg = new CMCubesMeshDlg(glWidget);
    _meshCDlg = new CMeshConstructionDlg(glWidget);
    _fusemeshDlg = new CFuseMeshDlg(glWidget);
    _texDlg = new CTexturingDlg(glWidget) ;
   // _lapSmDlg = new CLaplacianSmDlg(glWidget) ;
   // _qecDecDlg = new CQECDecimDlg(glWidget);
    _decsmDlg = new CDeciSmoothDlg(glWidget);
    _glSetDlg = new CGLSettingsDlg(glWidget, this);
    _lasScanOffDlg = new CLaserSanOfflineDlg(glWidget);
    _lasScanOnlDlg = new CLaserScanOnlineDlg(glWidget);
    _pclColorDlg = new CPCLColoringDlg(glWidget);
    _selPCLHelpDlg = new CSelectPCLHelpDlg(glWidget, this);
    connect(glWidget, SIGNAL(def_vmode()), _selPCLHelpDlg, SLOT(hide()));

    _alignCldDlg = new CGLAlignmentCloudDlg(glWidget) ;
    _martifRemDlg = new CMeshArtifRemoveDlg (glWidget,this);
    connect(glWidget, SIGNAL(def_vmode()), _martifRemDlg, SLOT(hide()));


}


void MainWindow::createActions()
{

    scanOffAct= new QAction(QIcon(":/images/icons/ico_scan.png"), tr("Process photos"), this);
    scanOffAct->setStatusTip(tr("Process photoseries offline"));
    connect(scanOffAct, SIGNAL(triggered()), this, SLOT(computeSparcePCLDlg()));


    dpclConstAct = new QAction(QIcon(":/images/icons/ico_scan.png"), tr("DPCL Construction"), this);
    dpclConstAct-> setStatusTip(tr("Process photos to construct dense PCL"));
    connect(dpclConstAct, SIGNAL(triggered()), this, SLOT(computeDensePCLDlg()));


    /*
    mcubemeshAct = new QAction(QIcon(":/images/icons/ico_mesh.png"), tr("Compute mesh with marching cubes"), this);
    mcubemeshAct->setStatusTip(tr("Computing mesh with marching cubes"));
    connect(mcubemeshAct, SIGNAL(triggered()), this, SLOT(computeMCubeMeshDlg()));
    */


    newAct = new QAction(QIcon(":/images/icons/ico_new.png"), tr("&New"), this);
    newAct->setShortcuts(QKeySequence::New);
    newAct->setStatusTip(msg_newScene);
    connect(newAct, SIGNAL(triggered()), this, SLOT(new_scene()));

    openAct = new QAction(QIcon(":/images/icons/ico_open.png"), tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing file"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(load_project()));

    saveAct = new QAction(QIcon(":/images/icons/ico_save.png"), tr("&Save"), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip(tr("Save the document to disk"));
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save_project()));

    saveAsAct = new QAction(tr("Save &As..."), this);
    saveAsAct->setShortcuts(QKeySequence::SaveAs);
    saveAsAct->setStatusTip(tr("Save the document under a new name"));
    connect(saveAsAct, SIGNAL(triggered()), this, SLOT(save_as_project()));

    exportAct = new QAction(tr("Export..."), this);
    exportAct->setStatusTip(tr("Export geometry into an extrenal file"));
    connect(exportAct, SIGNAL(triggered()), this, SLOT(export_scene()));

    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Exit the application"));
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));


    calibAct= new QAction(QIcon(":/images/icons/ico_calib.png"), tr("Calibrate"), this);
    calibAct->setStatusTip(tr("Stereo pair calibration"));
    connect(calibAct, SIGNAL(triggered()), this, SLOT(calibrationStereoDlg()));


    meshAct= new QAction(QIcon(":/images/icons/ico_mesh.png"), tr("Mesh reconstruction"), this);
    meshAct->setStatusTip(tr("Mesh recontsruction with the Poisson algorithm"));
    connect(meshAct, SIGNAL(triggered()), this, SLOT(meshConstDlg()));

    fuseMeshAct= new QAction(QIcon(":/images/icons/ico_mesh.png"), tr("Mesh fusion"), this);
    fuseMeshAct->setStatusTip(tr("Mesh fusion from separate scans"));
    connect(fuseMeshAct, SIGNAL(triggered()), this, SLOT(fusemeshDlg()));

    decismAct = new QAction(QIcon(":/images/icons/ico_opt.png"), tr("DeciSmooth"), this);
    decismAct->setStatusTip(tr("Decimation and smoothing for the mesh"));
    connect(decismAct, SIGNAL(triggered()), this, SLOT(computeDeciSmooth()));

    texAct= new QAction(QIcon(":/images/icons/ico_texture.png"), tr("Texture"), this);
    // texAct->setShortcuts(QKeySequence::New);
    texAct->setStatusTip(tr("Texture construction"));
    connect(texAct, SIGNAL(triggered()), this, SLOT(computeTexture()));

    aboutAct = new QAction(tr("&About ..."), this);
    aboutAct->setStatusTip(tr("Show the application's About box"));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));


    //
    viewrpAct = new QAction(QIcon(":/images/icons/ico_opt.png"),tr("Raw points"), this);
    viewrpAct->setCheckable(true);
    viewrpAct->setStatusTip(tr("Show raw points only"));
    connect(viewrpAct, SIGNAL(triggered()), this, SLOT(set_rend_mode_rawp()));

    //
    viewwrAct = new QAction(QIcon(":/images/icons/surface_wire_1.png"),tr("Wireframe"), this);
    viewwrAct->setCheckable(true);
    viewwrAct->setStatusTip(tr("Show mesh as wires"));
    //connect(viewwrAct, SIGNAL(triggered()), this, SLOT(set_rend_mode_mesh_wire()));

    //
    viewflAct = new QAction(QIcon(":/images/icons/surface_flat_1.png"),tr("Flat"), this);
    viewflAct->setCheckable(true);
    viewflAct ->setStatusTip(tr("Show mesh as flat surface"));
    //connect(viewflAct, SIGNAL(triggered()), this, SLOT(set_rend_mode_mesh_flat()));

    //
    viewsmAct = new QAction(QIcon(":/images/icons/surface_smooth_1.png"),tr("Smooth"), this);
    viewsmAct->setCheckable(true);
    viewsmAct->setStatusTip(tr("Show mesh as smooth surface"));
    //connect(viewsmAct, SIGNAL(triggered()), this, SLOT(set_rend_mode_mesh_smoo()));

    //
    viewtxAct = new QAction(QIcon(":/images/icons/surface_tex_1.png"), tr("Textured"), this);
    viewtxAct ->setCheckable(true);
    viewtxAct->setStatusTip(tr("Show mesh as textured surface"));
    //connect(viewtxAct, SIGNAL(triggered()), this, SLOT(set_rend_mode_mesh_tex()));

    visGroup = new QActionGroup(this);
    visGroup->setExclusive(true);
    // visGroup->setExclusive(true);
    visGroup ->addAction(viewrpAct);
    visGroup ->addAction(viewwrAct);
    visGroup ->addAction(viewflAct);
    visGroup ->addAction(viewsmAct);
    visGroup ->addAction(viewtxAct);
    viewrpAct->setChecked(true);

    viewshadpAct = new QAction(tr("Points Shadowing"), this);
    viewshadpAct ->setCheckable(true);
    viewshadpAct ->setStatusTip(tr("Show shadowed points"));
    connect(viewshadpAct, SIGNAL(triggered()), this, SLOT(set_view_shad_points()));

    viewcamAct = new QAction(tr("Show Cameras"), this);
    viewcamAct ->setCheckable(true);
    viewcamAct ->setStatusTip(tr("Show cameras in the scene"));
    connect(viewcamAct , SIGNAL(triggered()), this, SLOT(set_view_camera()));

    viewGLSet = new QAction(tr("Scene Settings"), this);
    viewGLSet->setStatusTip(tr("Change scene settings"));
    connect(viewGLSet, SIGNAL(triggered()), this, SLOT(change_gl_settings()));

    viewSelMode= new QAction(tr("Select vetrtecies"), this);
    viewSelMode->setStatusTip(tr("Selecting vertecies mode"));
    connect(viewSelMode, SIGNAL(triggered()), this, SLOT(change_view_scene_mode()));

    viewRemAct= new QAction(tr("Select mesh faces"), this);
    viewRemAct->setStatusTip(tr("Selecting mesh faces mode for further artifact removal"));
    connect(viewRemAct, SIGNAL(triggered()), this, SLOT(meshRemoveDlg()));


    _martifRemDlg = new CMeshArtifRemoveDlg (glWidget);

    dispAct= new QAction(QIcon(":/images/icons/ico_photo.png"), tr("Single photo PCL demo"), this);
    dispAct->setStatusTip(tr("Compute point cloud from single photo"));
    connect(dispAct, SIGNAL(triggered()), this, SLOT(disparityCompDlg()));

    alignAct = new QAction(QIcon(":/images/icons/tool_move.png"), tr("Cloud alignment"), this);
    alignAct->setStatusTip(tr("Cloud alignment wrt rotation center"));
    connect(alignAct, SIGNAL(triggered()), this, SLOT(alignCloudDlg()));

    laserScanOffAct= new QAction(QIcon(":/images/icons/ico_photo.png"), tr("Reconstruction from Video"), this);
    laserScanOffAct->setStatusTip(tr("Compute point cloud and mesh from single photo using laser line scnner"));
    connect(laserScanOffAct, SIGNAL(triggered()), this, SLOT(laserScanOfflineDlg()));

    laserScanOnlAct= new QAction(QIcon(":/images/icons/ico_scan.png"), tr("Scan to Video"), this);
    laserScanOnlAct->setStatusTip(tr("Compute point cloud from real data"));
    connect(laserScanOnlAct, SIGNAL(triggered()), this, SLOT(laserScanOnlineDlg()));

    pclColAct= new QAction(QIcon(":/images/icons/ico_opt.png"), tr("PCL Coloring"), this);
    pclColAct->setStatusTip(tr("Changin point cloud colors"));
    connect(pclColAct, SIGNAL(triggered()), this, SLOT(pclColorDlg()));

    /*
     *
    lapSmAct = new QAction(QIcon(":/images/icons/ico_opt.png"), tr("Laplacian Smooth"), this);
    lapSmAct->setStatusTip(tr("Laplacian smooth (surface preserve)"));
    connect(lapSmAct, SIGNAL(triggered()), this, SLOT(computeLaplacianSmooth()));

    qecDecAct = new QAction(QIcon(":/images/icons/ico_opt.png"), tr("QEC Decimation"), this);
    qecDecAct->setStatusTip(tr("Quadric Edge Collapse Decimation"));
    connect(qecDecAct, SIGNAL(triggered()), this, SLOT(computeQECDecimation()));


    dispClasAct= new QAction(QIcon(":/images/icons/ico_norm.png"), tr("Disparity Classic"), this);
    dispClasAct->setStatusTip(tr("Compute disparity map with classic method"));
    connect(dispClasAct, SIGNAL(triggered()), this, SLOT(disparityCompClassicDlg()));
    */

    /*

    importAct = new QAction(tr("Import..."), this);
    //importAct->setShortcuts(QKeySequence::SaveAs);
    importAct->setStatusTip(tr("Import geometry into the scene"));
    connect(importAct, SIGNAL(triggered()), this, SLOT(import_scene()));


    pptAct = new QAction(tr("Import images ..."), this);
    //ppt->setShortcuts(QKeySequence::SaveAs);
    pptAct->setStatusTip(tr("Construct point cloud for a series of images"));
    connect(pptAct, SIGNAL(triggered()), this, SLOT(import_photogrammetry()));

    scanAct= new QAction(QIcon(":/images/icons/ico_scan.png"), tr("Scan"), this);
    // scanAct->setShortcuts(QKeySequence::New);
    scanAct->setStatusTip(tr("Scanning"));
    connect(scanAct, SIGNAL(triggered()), this, SLOT(scanning()));

    optimAct= new QAction(QIcon(":/images/icons/ico_opt.png"), tr("Optimize"), this);
    // optimAct->setShortcuts(QKeySequence::New);
    optimAct->setStatusTip(tr("Point clouds optimization"));
    connect(optimAct, SIGNAL(triggered()), this, SLOT(optimizePntsDlg()));

    pnormAct= new QAction(QIcon(":/images/icons/ico_norm.png"), tr("Normals"), this);
    // pnormAct->setShortcuts(QKeySequence::New);
    pnormAct->setStatusTip(tr("Point clouds normal estimation"));
    connect(pnormAct, SIGNAL(triggered()), this, SLOT(normalsEstDlg()));

    meshActS= new QAction(QIcon(":/images/icons/ico_mesh.png"), tr("Convex Mesh"), this);
    // meshAct->setShortcuts(QKeySequence::New);
    meshActS->setStatusTip(tr("Mesh convex recontsruction (simple)"));
    connect(meshActS, SIGNAL(triggered()), this, SLOT(delaunay_mesh_test()));


    meshActD = new QAction(QIcon(":/images/icons/ico_mesh.png"), tr("Optim Mesh"), this);
    meshActD->setStatusTip(tr("Mesh optimization via decimation"));
    connect(meshActD, SIGNAL(triggered()), this, SLOT(mesh_decimation()));

    meshOpt = new QAction(tr("Mesh optimization"), this);
    meshOpt->setStatusTip(tr("Mesh optimization with clustering decimation"));
    connect(meshOpt, SIGNAL(triggered()), this, SLOT(tobeimplented()));

    meshLSmooth = new QAction(tr("Mesh smoothing"), this);
    meshLSmooth->setStatusTip(tr("Mesh smoothing with Laplacian operator"));
    connect(meshLSmooth, SIGNAL(triggered()), this, SLOT(tobeimplented()));


    movAct = new QAction(QIcon(":/images/icons/tool_move.png"), tr("Move, Rotate, Scale"), this);
    movAct->setStatusTip(tr("Move, rotate, scale of mesh"));
    connect(movAct, SIGNAL(triggered()), this, SLOT(transform_mesh()));

    rotAct= new QAction(QIcon(":/images/icons/tool_rotate.png"), tr("Rotate"), this);
    rotAct->setStatusTip(tr("Rotate point cloud"));
    connect(rotAct, SIGNAL(triggered()), this, SLOT(tobeimplented()));

    cropAct = new QAction(QIcon(":/images/icons/tool_crop.png"), tr("Trim"), this);
    cropAct->setStatusTip(tr("Crop"));
    connect(cropAct, SIGNAL(triggered()), this, SLOT(cropDiskDlg()));

    erasAct = new QAction(QIcon(":/images/icons/tool_erase.png"), tr("Erase"), this);
    erasAct->setStatusTip(tr("Erase"));
    connect(erasAct, SIGNAL(triggered()), this, SLOT(tobeimplented()));

    aligAct = new QAction(QIcon(":/images/icons/tool_magner.png"), tr("Align"), this);
    aligAct->setStatusTip(tr("Align"));
    connect(aligAct, SIGNAL(triggered()), this, SLOT(tobeimplented()));
*/
}

void MainWindow::new_scene()
{
    if(!_is_actul_scene)
    {
          QMessageBox::StandardButton reply =
                  QMessageBox::warning(this, tr("Warning"),
                                       wrn_DiscardSceneChanges, QMessageBox::Yes|QMessageBox::No);
           if (reply == QMessageBox::No)
                        return;
    }

    _selPCLHelpDlg->hide();
    glWidget->set_view_scene_mode(CGLWidget::DEF_VIEW_MODE);
    glWidget->make_new_scene();

    _is_actul_scene = true;
    _title = _appName;
    _current_project = "";
    this->setWindowTitle(_title);
    sbSceneMessageLbl->setText("");
    myinitMenuItems();
    myinitFieldButtons();

}

void MainWindow::change_view_scene_mode()
{

    glWidget->set_view_scene_mode(CGLWidget::SEL_VIEW_MODE);
    _selPCLHelpDlg->show();

}

void MainWindow::meshRemoveDlg()
{
    glWidget->set_view_scene_mode(CGLWidget::SEL_VIEW_MODE);
    _martifRemDlg->show();
}

void MainWindow::set_view_shad_points()
{
    glWidget->set_view_shad_points(viewshadpAct->isChecked());
}

void MainWindow::set_view_camera()
{
    glWidget->set_view_camera_path(viewcamAct->isChecked());
}

void MainWindow::set_rend_mode_rawp()
{
   glWidget->chainge_view_mode_of_current_object(CGLCompoundObject::RAW_POINTS);
   glWidget->udate_gl();
}

void MainWindow::set_rend_mode_wire()
{
   glWidget->get_scene()->get_se_manager()->set_rend_mode_wire();
   glWidget->udate_gl();
}

void MainWindow::set_rend_mode_flat()
{
    glWidget->get_scene()->get_se_manager()->set_rend_mode_flat();
    glWidget->udate_gl();
}

void MainWindow::set_rend_mode_smoo()
{
   glWidget->get_scene()->get_se_manager()->set_rend_mode_smoo();
   glWidget->udate_gl();
}

void MainWindow::set_rend_mode_tex()
{
    glWidget->get_scene()->get_se_manager()->set_rend_mode_tex();
    glWidget->udate_gl();
}


void MainWindow::device_calibration()
{
  //  CCameraAdjustmentDlg dlg(glWidget);
  //  dlg.exec();
}
void MainWindow::scanning()
{
   // _is_actul_scene = false;
  // if(_scanDlg)
   //    _scanDlg->show();
}

void MainWindow::load_project()
{
    if(!_is_actul_scene)
    {
          QMessageBox::StandardButton reply =
                  QMessageBox::warning(this, tr("Warning"),
                                       wrn_DiscardSceneChanges, QMessageBox::Yes|QMessageBox::No);
           if (reply == QMessageBox::No)
                        return;
    }

   QString fileName = QFileDialog::getOpenFileName(this,
         tr("Open project ..."), "", "Rubicon3D project (*.3dscan)");

   if (fileName.isEmpty()) return;

   _current_project = fileName;

   _selPCLHelpDlg->hide();
   glWidget->set_view_scene_mode(CGLWidget::DEF_VIEW_MODE);
   glWidget->make_new_scene();
   _is_actul_scene = true;
   _title = _appName + " - " + _current_project;
   setWindowTitle(_title);
   myinitMenuItems();
   sbSceneMessageLbl->setText("");



     /// glWidget->load_project(_current_project);

    _iowaitDlg->set_caption("Loading, please wait...");
     _iowaitDlg->show();
     _proj_io->request_load(glWidget, _current_project);
}

void MainWindow::proj_load_finished(bool r)
{
   _iowaitDlg->hide();
   if(!r)
       QMessageBox::warning(this, tr("Warning"), err_cannotLoadFile);
   else
        glWidget->finilize_loading_from_rub();
    std::cout << " proj_load_finished " << r << std::endl;

}


void MainWindow::save_project()
{
    if(_current_project.isEmpty())
    {
        QString fileName = QFileDialog::getSaveFileName(this,
             tr("Save project ..."), "", "Rubicon3D project (*.3dscan)");

        if (fileName.isEmpty())
                              return;
        _current_project = fileName;
    }

    /// glWidget->save_project(_current_project);


    _iowaitDlg->set_caption("Saving, please wait...");
    _iowaitDlg->show();
    _proj_io->request_save(glWidget, _current_project);

    _is_actul_scene = true;
    _title = _appName + " - " + _current_project;
    setWindowTitle(_title);
}

void MainWindow::save_as_project()
{


    QString fileName = QFileDialog::getSaveFileName(this,
             tr("Save project as ..."), "", "Rubicon3D project (*.3dscan)");

    if (fileName.isEmpty()) return;

    _current_project = fileName;

    /// glWidget->save_project(_current_project);

    _iowaitDlg->set_caption("Saving, please wait...");
    _iowaitDlg->show();
    _proj_io->request_save(glWidget, fileName);

    _is_actul_scene = true;
    _title = _appName + " - " + _current_project;
    setWindowTitle(_title);
}

void MainWindow::proj_save_finished()
{
    std::cout << " proj_save_finished " << std::endl;
    _iowaitDlg->hide();
}



void MainWindow::optimizePntsDlg()
{
    /*
    _is_actul_scene = false;
    if(glWidget->get_raw_points_of_current_object() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NothingOptim);
        return;
    }
    if(_poptDlg)
        _poptDlg->show();*/
}

void MainWindow::cropDiskDlg()
{
  /*  if(glWidget->get_raw_points_of_current_object() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NoRawPoints);
        return;
    }

    _is_actul_scene = false;
    if(_cropDlg)
        _cropDlg->show();*/
}

void MainWindow::import_photogrammetry()
{
  //  CPhotogrammetryDlg pptDlg;
  //  pptDlg.exec();

}
void MainWindow::normalsEstDlg()
{
    /*
     _is_actul_scene = false;
    if(glWidget->get_optim_points_of_current_object() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NothingNormal);
        return;
    }
    if(_normeDlg)
        _normeDlg->show();
*/
}

void MainWindow::meshConstDlg()
{
    _is_actul_scene = false;
    /*if(glWidget->get_dpcl_of_current_object() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NoDPCL);
        return;
    }*/

    if(_meshCDlg)
        _meshCDlg->show();

}

void MainWindow::fusemeshDlg()
{
    _is_actul_scene = false;

    if(_fusemeshDlg)
        _fusemeshDlg->show();
}


void MainWindow::myinitFieldButtons()
{
    btn_vw_wr ->setEnabled(false);
    btn_vw_tx ->setEnabled(false);
    btn_vw_sm ->setEnabled(false);
    btn_vw_fl ->setEnabled(false);

    updateFieldButton();
}

void MainWindow::field_btn_sound()
{
    _splay->play();
}

void MainWindow::initButtonView(QString file, QPushButton * btn, int icosz)
{
    QPixmap selectedPixmap(file);

    QPixmap disabledPixmap(selectedPixmap.size());
    disabledPixmap.fill(Qt::transparent);
    QPainter pd(&disabledPixmap);
    pd.setBackgroundMode(Qt::TransparentMode);
    pd.setBackground(QBrush(Qt::transparent));
    pd.eraseRect(selectedPixmap.rect());
    pd.setOpacity(0.15);
    pd.drawPixmap(0, 0, selectedPixmap);
    pd.end();

    QPixmap normalPixmap(selectedPixmap.size());
    normalPixmap.fill(Qt::transparent);
    QPainter pn(&normalPixmap);
    pn.setBackgroundMode(Qt::TransparentMode);
    pn.setBackground(QBrush(Qt::transparent));
    pn.eraseRect(selectedPixmap.rect());
    pn.setOpacity(0.4);
    pn.drawPixmap(0, 0, selectedPixmap);
    pn.end();


    QIcon icon;

    icon.addPixmap(selectedPixmap, QIcon::Normal, QIcon::On);
    icon.addPixmap(normalPixmap, QIcon::Normal, QIcon::Off);
    icon.addPixmap(disabledPixmap, QIcon::Disabled, QIcon::Off);
    icon.addPixmap(disabledPixmap, QIcon::Disabled, QIcon::On);

    btn->setIcon(icon);
    btn->setIconSize(QSize(icosz,icosz));
}

void MainWindow::updateFieldButton()
{
    int mwidth = this->glWidget->size().width();

    int offxL  = 10;
    int offy   = 25;
    int spacex = 5;
    int bsize = 32;

    QSize bsz(bsize, bsize);

    btn_vw_wr->setGeometry(QRect(QPoint(mwidth - offxL - 4 * bsize - 3 * spacex, offy), bsz));
    QString btn_vw_wr_style = _field_btn_style.arg(QString::number(CGlobalHSSettings::gcolor_sr*255)).arg(QString::number(CGlobalHSSettings::gcolor_sg*255)).arg(QString::number(CGlobalHSSettings::gcolor_sb*255));
    btn_vw_wr->setStyleSheet(btn_vw_wr_style);
    btn_vw_wr->repaint();

    btn_vw_tx->setGeometry(QRect(QPoint(mwidth - offxL - 1 * bsize - 0 * spacex, offy), bsz));
    QString btn_vw_tx_style = _field_btn_style.arg(QString::number(CGlobalHSSettings::gcolor_sr*255)).arg(QString::number(CGlobalHSSettings::gcolor_sg*255)).arg(QString::number(CGlobalHSSettings::gcolor_sb*255));
    btn_vw_tx->setStyleSheet(btn_vw_tx_style);
    btn_vw_tx->repaint();

    btn_vw_fl->setGeometry(QRect(QPoint(mwidth - offxL - 3 * bsize - 2 * spacex, offy), bsz));
    QString btn_vw_fl_style = _field_btn_style.arg(QString::number(CGlobalHSSettings::gcolor_sr*255)).arg(QString::number(CGlobalHSSettings::gcolor_sg*255)).arg(QString::number(CGlobalHSSettings::gcolor_sb*255));
    btn_vw_fl->setStyleSheet(btn_vw_fl_style);
    btn_vw_fl->repaint();


    btn_vw_sm->setGeometry(QRect(QPoint(mwidth - offxL - 2 * bsize - 1 * spacex, offy), bsz));
    QString btn_vw_sm_style = _field_btn_style.arg(QString::number(CGlobalHSSettings::gcolor_sr*255)).arg(QString::number(CGlobalHSSettings::gcolor_sg*255)).arg(QString::number(CGlobalHSSettings::gcolor_sb*255));
    btn_vw_sm->setStyleSheet(btn_vw_sm_style);
    btn_vw_sm->repaint();

}


void MainWindow::createFieldButtons()
{

    _splay = new  QSound(":sounds/sounds/click_s.wav");

    _field_btn_style = QString(" background-color: rgb(%1,%2,%3); \
                            border-top: 0px ; \
                            border-bottom: 0px ; \
                            border-right: 0px ; \
                            border-left: 0px ; \
                            border-radius: 0; ");

    int bsize = 32;

    btn_vw_wr =  new QPushButton();
    btn_vw_wr->setCheckable(true);
    initButtonView(QString(":/images/icons/n_surface_wire_1.png"), btn_vw_wr, bsize);

    this->layout()->addWidget(btn_vw_wr);
    connect(btn_vw_wr,   SIGNAL(clicked()), this, SLOT(field_btn_sound()));
    connect(btn_vw_wr,   SIGNAL(clicked()),this, SLOT(set_rend_mode_wire()));

    btn_vw_tx =  new QPushButton;
    btn_vw_tx->setCheckable(true);
    initButtonView(QString(":/images/icons/n_surface_tex_1.png"), btn_vw_tx, bsize);
    this->layout()->addWidget(btn_vw_tx);
    connect(btn_vw_tx,   SIGNAL(clicked()), this, SLOT(field_btn_sound()));
    connect(btn_vw_tx,   SIGNAL(clicked()), this, SLOT(set_rend_mode_tex()));

    btn_vw_fl =  new QPushButton;
    btn_vw_fl->setCheckable(true);
    initButtonView(QString(":/images/icons/n_surface_flat_1.png"), btn_vw_fl, bsize);
    this->layout()->addWidget(btn_vw_fl);
    connect(btn_vw_fl,   SIGNAL(clicked()), this, SLOT(field_btn_sound()));
   connect(btn_vw_fl,   SIGNAL(clicked()), this, SLOT(set_rend_mode_flat()));

    btn_vw_sm =  new QPushButton;
    btn_vw_sm->setCheckable(true);
    initButtonView(QString(":/images/icons/n_surface_smooth_1.png"), btn_vw_sm, bsize);
    this->layout()->addWidget(btn_vw_sm);
    connect(btn_vw_sm,   SIGNAL(clicked()), this, SLOT(field_btn_sound()));
    connect(btn_vw_sm,   SIGNAL(clicked()), this, SLOT(set_rend_mode_smoo()));


    myinitFieldButtons();

}

void MainWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(newAct);
    fileMenu->addAction(openAct);
    fileMenu->addAction(saveAct);
    fileMenu->addAction(saveAsAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exportAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    workMenu = menuBar()->addMenu(tr("&Workflow"));
    workMenu->addAction(laserScanOnlAct);
    workMenu->addAction(laserScanOffAct);
   /// workMenu->addAction(dispAct);
   /// workMenu->addAction(scanOffAct);
   /// workMenu->addSeparator();
   /// workMenu->addAction(dpclConstAct);
 ///   workMenu->addAction(fuseMeshAct);
    workMenu->addAction(alignAct);
    workMenu->addAction(meshAct);
    workMenu->addSeparator();

    workMenu->addAction(decismAct);
    workMenu->addAction(texAct);

    viewMenu = menuBar()->addMenu(tr("&View"));
    viewMenu->addAction(viewGLSet);
    viewMenu->addAction(pclColAct);
    viewMenu->addAction(viewSelMode);
    viewMenu->addAction(viewRemAct);




   // viewMenu->addAction(viewwrAct);
   // viewMenu->addAction(viewflAct);
   // viewMenu->addAction(viewsmAct);
   // viewMenu->addAction(viewtxAct);
  //  viewMenu->addSeparator();
   // viewMenu->addAction(viewshadpAct);
    //viewMenu->addAction(viewcamAct);


    //viewMenu->addActions(alignmentGroup);

    helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAct);

    myinitMenuItems();

    /*
    fileMenu = menuBar()->addMenu(tr("&File"));

    fileMenu->addAction(importAct);
    //fileMenu->addSeparator();
    //fileMenu->addAction(pptAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    actMenu = menuBar()->addMenu(tr("&Action"));
    actMenu->addAction(scanAct);
    actMenu->addAction(cropAct);
    actMenu->addAction(meshActS);
    actMenu->addAction(meshActD);
    //actMenu->addAction(aligAct);
    //actMenu->addAction(meshOpt);
    //actMenu->addAction(meshLSmooth);
    actMenu->addAction(movAct);
    actMenu->addAction(meshAct);

    viewMenu = menuBar()->addMenu(tr("&View"));
    //viewMenu->addAction(newAct);
    //viewMenu->addAction(copyAct);

    menuBar()->addSeparator();

    helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAct);
    //helpMenu->addAction(aboutQtAct);*/
}

void MainWindow::myinitMenuItems()
{
    viewrpAct->setChecked(true);
    viewrpAct->setEnabled(false);
    viewwrAct->setEnabled(false);
    viewflAct->setEnabled(false);
    viewsmAct->setEnabled(false);
    viewtxAct->setEnabled(false);
    viewshadpAct->setEnabled(false);
    viewshadpAct->setChecked(true);
    viewcamAct->setEnabled(false);
    viewcamAct->setChecked(true);
}

void MainWindow::createToolBars()
{
    toolsToolBar = addToolBar(tr("Tools"));
    ///toolsToolBar->addAction(calibAct);
    ///toolsToolBar->addAction(dispClasAct);

    ///toolsToolBar->addAction(dispAct);
    toolsToolBar->addAction(scanOffAct);
    ///toolsToolBar->addAction(mcubemeshAct);

    toolsToolBar->addAction(meshAct);
    toolsToolBar->addAction(decismAct);
    //toolsToolBar->addAction(qecDecAct);
    //toolsToolBar->addAction(lapSmAct);
    toolsToolBar->addAction(texAct);

   /*   mainToolBar = addToolBar(tr("Main"));
   mainToolBar->addAction(newAct);
    mainToolBar->addAction(openAct);
    mainToolBar->addAction(saveAct);


    toolsToolBar = addToolBar(tr("Tools"));
    toolsToolBar->addAction(scanAct);
    ///toolsToolBar->addAction(optimAct);
    ///toolsToolBar->addAction(pnormAct);

    toolsToolBar->addAction(meshActS);
    //toolsToolBar->addAction(meshActD);
    ///toolsToolBar->addAction(texAct);
    toolsToolBar->addAction(calibAct);

    //toolsAuxToolBar = addToolBar(tr("Individual tools"));
    //toolsAuxToolBar->addAction(movAct);
    //toolsAuxToolBar->addAction(rotAct);
    //toolsAuxToolBar->addAction(cropAct);
    //toolsAuxToolBar->addAction(erasAct);
    //toolsAuxToolBar->addAction(aligAct);*/


}

void MainWindow::createStatusBar()
{
    ui->statusBar->setStyleSheet("QStatusBar::item { border: 0px solid black }; ");
    sbSceneMessageLbl= new QLabel(this);
     ui->statusBar->addWidget(sbSceneMessageLbl);
}

void MainWindow::setStatusBarInfo(QString msg)
{
    sbSceneMessageLbl->setText(msg);
}

void MainWindow::disparityCompDlg()
{
   if(_dispDlg)
        _dispDlg->show();
}

void MainWindow::disparityCompClassicDlg()
{
    //if(_dispClasDlg)
    //    _dispClasDlg->show();
}

void MainWindow::computeSparcePCLDlg()
{
    _is_actul_scene = false;
    if(_compSPCLDlg)
        _compSPCLDlg->show();
}
void MainWindow::pclColorDlg()
{
    _is_actul_scene = false;
    if(_pclColorDlg)
        _pclColorDlg->show();
}


void MainWindow::laserScanOfflineDlg()
{
    _is_actul_scene = false;
    if(_lasScanOffDlg)
        _lasScanOffDlg->show();
}

void MainWindow::laserScanOnlineDlg()
{
    _is_actul_scene = false;
    if(_lasScanOnlDlg)
       _lasScanOnlDlg->show();
}

void  MainWindow::computeDensePCLDlg()
{
    _is_actul_scene = false;
    if(_compDPCLDlg)
        _compDPCLDlg->show();
}

void MainWindow::computeMCubeMeshDlg()
{
    /*
    if(glWidget->get_raw_points_of_current_object() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NoRawPoints);
        return;
    }


    if(_compMCubeMeshDlg)
       _compMCubeMeshDlg->show();*/
}

void MainWindow::computeTexture()
{
    if(glWidget->get_fused_mesh() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NoFusedMesh);
        return;
    }

    _is_actul_scene = false;
    if(_texDlg)
       _texDlg->show();
}

void MainWindow::alignCloudDlg()
{
    _is_actul_scene = false;

    if(_alignCldDlg)
        _alignCldDlg->show();
}

void MainWindow::computeLaplacianSmooth()
{
/*
    if(glWidget->get_mesh_of_current_object() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NoMesh);
        return;
    }

    _is_actul_scene = false;
    if(_lapSmDlg)
       _lapSmDlg->show();
*/
}

void MainWindow::computeQECDecimation()
{
/*
    if(glWidget->get_mesh_of_current_object() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NoMesh);
        return;
    }

    _is_actul_scene = false;
    if(_qecDecDlg)
       _qecDecDlg->show();
*/
}

void MainWindow::computeDeciSmooth()
{
    if(glWidget->get_fused_mesh() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NoMesh);
        return;
    }

    _is_actul_scene = false;
    if(_decsmDlg)
       _decsmDlg->show();
}

void MainWindow::calibrationStereoDlg()
{
    if(_calibDlg)
        _calibDlg->show();
}

void MainWindow::about()
{
    int bits = SYSBITS;
    QString txt = QString("<b>Rubicon 3D Scanner (%1-bits)</b> ").arg(QString::number(bits)) + g_app_version + QString(" <br><br> Copyright (C) Rubicon Technologies, 2017 <br><br> <a href=\"www.rubitech.org\">www.rubitech.org</a>");
   QMessageBox::about(this, tr("About Rubicon 3D Scanner"),
             txt );
}


void MainWindow::tobeimplented()
{
    QMessageBox::about(this, tr("Warning"),
             tr("This feature will be implemented soon"));
}


void MainWindow::import_scene()
{
    /*
    _is_actul_scene = false;
    QString fileName = QFileDialog::getOpenFileName(this,
         tr("Import data ..."), "", CImportManager::importFileTypes);

    if (!fileName.isEmpty())
        glWidget->import_scene(fileName);
*/
}

void MainWindow::export_scene()
{
    QString fileName = QFileDialog::getSaveFileName(this,
         tr("Export data ..."), "", CExportManager::exportFileTypes);

   if (!fileName.isEmpty())
   {
       _iowaitDlg->set_caption("Exporting, please wait...");
       _iowaitDlg->show();
       _proj_io->request_export(glWidget, fileName);

   }
}


void MainWindow::export_finished()
{
    std::cout << "export_finished " << std::endl;
    _iowaitDlg->hide();
}


void MainWindow::delaunay_mesh_test()
{
    /*

    if(glWidget->get_raw_points_of_current_object() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NoRawPoints);
        return;
    }

    _is_actul_scene = false;
    if(_cvxMeshCDlg)
        _cvxMeshCDlg->show();
*/
}


void MainWindow::mesh_decimation()
{
/*
    if(glWidget->get_mesh_of_current_object() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NoMesh);
        return;
    }

    _is_actul_scene = false;
    if(_clDesimDlg)
        _clDesimDlg->show();

*/
}

void MainWindow::transform_mesh()
{
    /*
    if(glWidget->get_mesh_of_current_object() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NoMesh);
        return;
    }

    _is_actul_scene = false;
    if(_mrsDlg)
        _mrsDlg->show();*/
}

void MainWindow::change_gl_settings()
{
    if(_glSetDlg)
        _glSetDlg->show();
}
