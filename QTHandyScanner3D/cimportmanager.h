#ifndef CIMPORTMANAGER_H
#define CIMPORTMANAGER_H

#include <QString>
#include "graphicutils.h"

class CImportManager
{
public:
    CImportManager();

    static const QString importFileTypes;
};

CTriMesh * load_obj_mesh(std::string fname);
std::vector<std::string> split(const std::string &s, char delim);

#endif // CIMPORTMANAGER_H
