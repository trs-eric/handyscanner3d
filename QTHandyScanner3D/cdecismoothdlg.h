#ifndef CDECISMOOTHDLG_H
#define CDECISMOOTHDLG_H

#include <QDialog>

#include <QMessageBox>


class CGLWidget;
class CTriangularMesh;
class CDecimSmoothThread;
class CMeshDumpThread;

namespace Ui {
class CDeciSmoothDlg;
}

class CDeciSmoothDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CDeciSmoothDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CDeciSmoothDlg();
private slots:
    void decimproc_changed(int );
    void iterations_changed(int );
    void smoothdeg_changed(int );

protected:
    void showEvent(QShowEvent * event);
    CTriangularMesh * restore_mesh();
    void start_process_loc();

private:
    Ui::CDeciSmoothDlg *ui;
    CGLWidget *_glWidget;

    bool _in_process;
    bool _on_apply;
    bool _is_result_valid;

    CDecimSmoothThread * _thread;
    QMessageBox * _info;
    CMeshDumpThread * _ithread;

    int _init_tri_cnt;
    bool _very_first_use;

private slots:
    void onCancelBtn();
    void onPreviewBtn();
    void onApplyBtn();
    void onFinishDumping();

    void onMakeProgressStep(int val);
    void onCompleteProcessing(CTriangularMesh * );
    void onStopProcessing();

    void close_dialog();
};

#endif // CDECISMOOTHDLG_H
