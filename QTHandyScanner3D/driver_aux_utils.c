#include "driver_aux_utils.h"

static void sdramTest(void)
{
  u32 offs;
  u16 data;
  int i;

  offs = 0x5555;
  for (i = 0; i < 1000; i+= 2)
    writeMemory(getFtHandler(), i, i + offs);
  for (i = 0; i < 1000; i+= 2)
  {
    if (i % 64 == 0)
      printf("%d\n", i);
    readMemory(getFtHandler(), i, &data);
    if (data != (i + offs))
    {
      printf("Error reading address 0x%06x   write data 0x%04x  read data 0x%04x\n",
        i, i + offs, data);
//      break;
    }
  }
}

u8 block_buffer[BLOCK_SIZE*2];

static void blockReadTest(u8* buffer)
{
  int i;
  u16 data;
  int size = BLOCK_SIZE;

  for (i = 0; i < size; i+= 2)
  {
    data = (i & 0xFF) | (((i + 1) & 0xFF) << 8);
    writeMemory(getFtHandler(), i, data);
  }

  for (i = 0; i < BLOCK_SIZE*2; i++)
    buffer[i] = 0xFF;
  readBlock(getFtHandler(), 0, buffer, 0);
  for (i = 0; i < size+ 16; i++)
  {
    printf("%02x ", buffer[i]);
    if (i % 16 == 15)
      printf("\n");
  }
  printf("\n");
}

u16 getSnapshotPixelValue(const u8* buffer, int x, int y, int width)
{
  // 2 12-bit pixels -> 3 consecutive bytes in memory
  int pixel_address = (3*width/ 2)* y + (3* x/ 2);

  if ((x & 1) == 0)
  {
    return buffer[pixel_address] | ((buffer[pixel_address + 1] & 0xF) << 8);
  }
  else
  {
    return ((buffer[pixel_address] >> 4) & 0xF) | (buffer[pixel_address + 1] << 4);
  }
}

void convertSnapshotToPPM(const u8* buffer, const char* pFilename)
{
  FILE* f;
  int width = getSnapshotWidth();
  int height = getSnapshotHeight();
  int out_width = 1250;
  int out_height = 950;
  int x;
  int y;

  f = fopen(pFilename, "w+");
  if (f == NULL)
  {
    printf("Can't open file %s\n", pFilename);
    return;
  }
  fprintf(f, "P3\n");
  fprintf(f, "%d %d\n", out_width, out_height);
  fprintf(f, "%d\n", 255);

  for (y = 0; y < out_height* 2; y+= 2)
  {
    for (x = 0; x < out_width* 2; x+= 2)
    {
      // pixel pattern :
      //
      //   G   R
      //
      //   B   G
      u16 g1 = getSnapshotPixelValue(buffer, x, y, width);
      u16 r =  getSnapshotPixelValue(buffer, x + 1, y, width);
      u16 b =  getSnapshotPixelValue(buffer, x, y + 1, width);
      u16 g2 = getSnapshotPixelValue(buffer, x + 1, y + 1, width);

      // simple conversion to 8 bit
      u16 r8 = r >> 4;
//      r8 = 220 * r8 / 256;
      u16 g8 = (g1 + g2) >> 5;
//      g8 = 220 * g8 / 256;
      u16 b8 = b >> 4;

      fprintf(f, "%d %d %d  ", r8, g8, b8);
    }
    fprintf(f, "\n");
  }

  fflush(f);
  fclose(f);
}

void testSnapshot(void)
{
  int size;
  u8* buffer;

  size = getSnapshotBufferSize() + 16;
  buffer = (u8*)calloc(size, 1);

  doSnapshot(buffer);
//  convertSnapshotToJpeg(buffer, "c:/tmp/test.jpg");
  convertSnapshotToPPM(buffer, "c:/test.ppm");

  printf("snaphot data converted\n");
  free(buffer);
}

// 4 pin step function for half stepper
// This is passed the current step number (0 to 7)
// Subclasses can override
void step4(u8 step)
{
  switch (step & 0x3)
  {
    case 0:  // 1010
//      stepMotorOut(0xA);
      break;
    case 1:  // 0110
//      stepMotorOut(0x6);
      break;
    case 2:  //0101
//      stepMotorOut(0x5);
      break;
    case 3:  //1001
//      stepMotorOut(0x9);
      break;
  }
}

void stepMotorTest(void)
{
  u8 step;
  int i;

  step = 0;
  for (i = 0; i < 500; i++)
  {
    step4(step);
    usleep(10000);
    step = (step + 1) & 0x3;
  }
  for (i = 0; i < 500; i++)
  {
    step4(step);
    usleep(10000);
    step = (step - 1) & 0x3;
  }
//  stepMotorOut(0);
}
