#ifndef CCAMERAPOSE_H
#define CCAMERAPOSE_H

#include "cglobject.h"

class CCameraPose : public CGLObject
{
public:
    CCameraPose(bool vis = false);
    virtual ~CCameraPose();

    void draw_tri();
    void draw_camera();


protected:
    virtual void create_glList();


};

#endif // CCAMERAPOSE_H
